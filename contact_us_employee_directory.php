<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="robots" content="noindex"/>
<title>ETTER Engineering - Employee Directory</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com/"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Package Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
			<li><a href="packaged-burners.php">Package Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="accolades.php">Accolades</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Case Studies</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id= "RedBrowseBar"></div>
<div id="AddressETC">
<font color="#000000" face="Tahoma" size="2"><b>210 Century Drive</b></font>
<br/>
<font color="#D21D1F" face="Tahoma" size="2"><b>Bristol, Ct 06010</b></font>
</div>
<div id="PhoneNumber">
Tel: <font face="Tahoma" size="2">860-584-8842</font>
<br/>
Fax: <font face="Tahoma" size="2">860-584-8612</font>
<br/>
Toll Free: <font face="Tahoma" size="2">1-800-444-1962</font>
</div>
<a href="contact_us.php" id="BackMap"><b>Back to Map</b></a>
<div id="EmployeeDirectory">
<br/>
<font size="2"><b>Tom Etter</b><br/>
<font color="#D21D1F">President</font><br/>
<a href="mailto:tetter@etterengineering.com?subject=Contact%20Us"><font color="#000000">tetter@etterengineering.com</font></a><br/>
Ext. 202<br/>
<br/>
<b>Jon G. Moore</b><br/>
<font color="#D21D1F">V.P., Engineering Sales</font><br/>
<a href="mailto:jmoore@etterengineering.com?subject=Contact%20Us"><font color="#000000">jmoore@etterengineering.com</font></a><br/>
Ext. 201<br/>
<br/>
<b>Brenda J. Wazocha</b><br/>
<font color="#D21D1F">Purchasing Agent</font><br/>
<a href="mailto:bwazocha@etterengineering.com?subject=Contact%20Us"><font color="#000000">bwazocha@etterengineering.com</font></a><br/>
Ext. 208<br/>
<br/>
<b>Brett Bernier</b><br/>
<font color="#D21D1F">Controls Technician</font><br/>
<a href="mailto:bbernier@etterengineering.com?subject=Contact%20Us"><font color="#000000">bbernier@etterengineering.com</font></a><br/>
Ext. 216<br/>
<br/>
<b>Christopher Del Sole</b><br/>
<font color="#D21D1F">Marketing Director</font><br/>
<a href="mailto:cdelsole@etterengineering.com?subject=Contact%20Us"><font color="#000000">cdelsole@etterengineering.com</font></a><br/>
Ext. 205<br/>
<br/>
<b>Daryl Ritone</b><br/>
<font color="#D21D1F">Controls Technician</font><br/>
<a href="mailto:dritone@etterengineering.com?subject=Contact%20Us"><font color="#000000">dritone@etterengineering.com</font></a><br/>
Ext. 206<br/>
<br/>
<b>Jamie Feagain</b><br/>
<font color="#D21D1F">Project Services</font><br/>
<a href="mailto:jfeagain@etterengineering.com?subject=Contact%20Us"><font color="#000000">jfeagain@etterengineering.com</font></a><br/>
Ext. 214<br/>
<br/>
<b>Ren Malcolm</b><br/>
<font color="#D21D1F">Design Engineer</font><br/>
<a href="mailto:rmalcolm@etterengineering.com?subject=Contact%20Us"><font color="#000000">rmalcolm@etterengineering.com</font></a><br/>
Ext. 215<br/>
<br/>
<b>Kevin Foster</b><br/>
<font color="#D21D1F">Controls Engineer</font><br/>
<a href="mailto:foster@etterengineering.com?subject=Contact%20Us"><font color="#000000">foster@etterengineering.com</font></a><br/>
Ext. 213<br/>
<br/>
<b>Norm Myers</b><br/>
<font color="#D21D1F">Sales and Service</font><br/>
<a href="mailto:nmyers@etterengineering.com?subject=Contact%20Us"><font color="#000000">nmyers@etterengineering.com</font></a><br/>
Ext. 212<br/>
<br/>
<b>Ryan Smart</b><br/>
<font color="#D21D1F">Product Manager</font><br/>
<a href="mailto:rsmart@etterengineering.com?subject=Contact%20Us"><font color="#000000">rsmart@etterengineering.com</font></a><br/>
Ext. 207<br/>
<br/>
<b>Paul Lavoie</b><br/>
<font color="#D21D1F">VP, Sales & Marketing</font><br/>
<a href="mailto:rzimnoch@etterengineering.com?subject=Contact%20Us"><font color="#000000">plavoie@etterengineering.com</font></a><br/>
Ext. 215<br/>
<br/>
<b>Steve Breton</b><br/>
<font color="#D21D1F">Inside Technical Sales</font><br/>
<a href="mailto:sbreton@etterengineering.com?subject=Contact%20Us"><font color="#000000">sbreton@etterengineering.com</font></a><br/>
Ext. 204<br/>
<br/>
<b>Theresa P. Brownell</b><br/>
<font color="#D21D1F">Office Manager</font><br/>
<a href="mailto:tbrownell@etterengineering.com?subject=Contact%20Us"><font color="#000000">tbrownell@etterengineering.com</font></a><br/>
Ext. 200<br/>
<br/>
<b>Terry Wakeman</b><br/>
<font color="#D21D1F">Boiler/Burner Specialist</font><br/>
<a href="mailto:twakeman@etterengineering.com?subject=Contact%20Us"><font color="#000000">twakeman@etterengineering.com</font></a><br/>
Ext. 219<br/>
<b>Laldeo Lilman</b><br/>
<font color="#D21D1F">Electrical Controls Technician</font><br/>
<a href="mailto:llilman@etterengineering.com?subject=Contact%20Us"><font color="#000000">llilman@etterengineering.com</font></a><br/>
Ext. 217<br/>
<b>Jim Higley</b><br/>
<font color="#D21D1F">Lead Controls Engineer</font><br/>
<a href="mailto:jhigley@etterengineering.com?subject=Contact%20Us"><font color="#000000">jhigley@etterengineering.com</font></a><br/>
<br/>
<b>Michel Theodore</b><br/>
<font color="#D21D1F">Quality Control Engineer</font><br/>
<a href="mailto:mtheodore@etterengineering.com?subject=Contact%20Us"><font color="#000000">mtheodore@etterengineering.com</font></a><br/>
Ext. 226<br/>
<br/>
<br/></font>
</div>
<div id="ContactForm">
<form id="emf-form" enctype="multipart/form-data" method="post" action="http://www.emailmeform.com/builder/form/LHMjmv41q3ey4" name="emf-form">
  <table style="text-align:left;" cellpadding="2" cellspacing="0" border="0" bgcolor="transparent">
    <tr>
      <td colspan="2">
        <font face="Verdana" size="1" color="#000000"><br />
        <br /></font>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <font face="Verdana" size="1" color="#000000"><b>Your Name</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td>
        <input id="element_0" name="element_0" size="30" class="validate[required]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <font face="Verdana" size="1" color="#000000"><b>Your Company</b></font>
      </td>
    </tr>
    <tr>
      <td>
        <input id="element_1" name="element_1" size="30" class="validate[optional]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <font face="Verdana" size="1" color="#000000"><b>Your Email</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td>
        <input id="element_2" name="element_2" class="validate[required,custom[email]]" size="30" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <font face="Verdana" size="1" color="#000000"><b>Subject</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td>
        <input id="element_3" name="element_3" size="30" class="validate[required]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td>
        <font face="Verdana" size="1" color="#000000"><b>Message</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td>
        <textarea id="element_4" name="element_4" cols="30" rows="5" class="validate[required]">
</textarea>
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">
        <input name="element_counts" value="5" type="hidden" /> <input name="embed" value="forms" type="hidden" /><input value="Send" type="submit" /><input value="Clear" type="reset" />
      </td>
    </tr>
  </table>
</form>
<div>
  <font face="Verdana" size="1" color="#000000"></font><span style="position: relative; padding-left: 3px; bottom: -5px;"></span><font face="Verdana" size="1" color="#000000"></font> <a style="text-decoration:none;" href="http://www.emailmeform.com"
  target="_blank"><font face="Verdana" size="1" color="#000000"></font></a>
</div><a style="line-height:20px;font-size:70%;text-decoration:none;" href="http://www.emailmeform.com/report-abuse.html?http://www.emailmeform.com/builder/form/LHMjmv41q3ey4" target=
"_blank"><font face="Verdana" size="1" color="#000000"></font></a>
</div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>