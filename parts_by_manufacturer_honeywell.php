<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
$(function() {
$('a.SolidVideo').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGBSolid"><img border="0" src="closeXsmall.gif" class="btn_close_ENGBSolid" title="Close Window" alt="Close" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn(); 
return false;
});
$('a.close_ENGBSolid, #fade').live('click', function() {
    $('#fade , .ENGBSolid_block').fadeOut(function() {
        $('#fade, a.close_ENGBSolid').remove(); 
    });
    return false;
});
});
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color=#494A4A><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script>
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>

<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php"div id="Logo"></a>
<a href="index.php"div id="Tagline">to ALL your process heating & combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<div id="AscoLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<div id="KromSchroderLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<div id="MaxitrolLogo" title="Manufacturer Available Online Soon"></div>
<div id="ElsterLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<div id="ProtectionLogo" title="Manufacturer Available Online Soon"></div>
<div id="EclipseLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<div id="EmonLogo" title="Manufacturer Available Online Soon"></div>
<div id="DuragLogo" title="Manufacturer Available Online Soon"></div>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>

<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="HoneywellLogoLarge"></div>
<div id="SensusText">Multi-Division Company that is an industry leader 
in communication and building automation technology. We are a Siemens ICPI 
(Industrial Combustion Products Integrator) fo the Building Technologies 
division which enables us to buy at a deep discount. This division offers 
various combustion products such as gas safety shut-off valves, actuators, 
butterfly valves, motors for gas modulation, flame safety products, sensors, 
scanners and more.</div>
<div id="ThumbnailBackground"></div>
<ul id="slideshow">
		<li>
			<h3>Auto Adjust Meters</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Auto_Adjust_Meters.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Auto_Adjust_Meters_Thumbnail.gif" alt="blue" /></a>
		</li>
		<li>
			<h3>Combustion Boiler Regulators</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Combustion_Boiler_Regulators.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Combustion_Boiler_Regulators_Thumbnail.gif" alt="cyan" />
		</li>
		<li>
			<h3>Commercial Industrial</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Commercial_Industrial.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Commercial_Industrial_Thumbnail.gif" alt="green" /></a>
		</li>
		<li>
			<h3>Commercial 243</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Commercial_243.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Commercial_243_Thumbnail.gif" alt="orange" /></a>
		</li>
		<li>
			<h3>Diaphragm Meters</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Diaphragm_Meters.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Diaphragm_Meters_Thumbnail.gif" alt="pink" />
		</li>
		<li>
			<h3>Domestic 143 80</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Domestic_143_80.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Domestic_143_80_Thumbnail.gif" alt="purple" /></a>
		</li>
		<li>
			<h3>Domestic to 500 CFH</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Domestic_to_500_CFH.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="thumbnails/Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Domestic_to_500_CFH_Thumbnail.gif" alt="red" /></a>
		</li>
		<li>
			<h3>High Capacity 441 VPC</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/High_Capacity_441_VPC.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Capacity_441_VPC_Thumbnail.gif" alt="yellow" /></a>
		</li>
		<li>
			<h3>High Pressure</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/High_Pressure.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Pressure_Thumbnail.gif" alt="blue" /></a>
		</li>
		<li>
			<h3>High Pressure 046</h3>
			<span>Parts_by_Man_OK_By_Jon/High_Pressure_046.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Pressure_046_Thumbnail.gif" alt="cyan" />
		</li>
		<li>
			<h3>Intermediate Capacity</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Intermediate_Capacity.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Intermediate_Capacity_Thumbnail.gif" alt="green" /></a>
		</li>
		<li>
			<h3>Lower Pressure</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Lower_Pressure.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Lower_Pressure_Thumbnail.gif" alt="orange" /></a>
		</li>
		<li>
			<h3>Mark II Meters</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Mark_II_Meters.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Mark_II_Meters_Thumbnail.gif" alt="pink" />
		</li>
		<li>
			<h3>Medium Pressure</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Medium_Pressure.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Medium_Pressure_Thumbnail.gif" alt="purple" /></a>
		</li>
		<li>
			<h3>Pilot Operated Regulators</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Pilot_Operated_Regulators.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="thumbnails/Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Pilot_Operated_Regulators_Thumbnail.gif" alt="red" /></a>
		</li>
		<li>
			<h3>Service 243 RPC</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Service_243_RPC.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Service_243_RPC_Thumbnail.gif" alt="yellow" /></a>
		</li>
		<li>
			<h3>Service Regulators Domestic 496</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Service_Regulators_Domestic_496.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Service_Regulators_Domestic_496_Thumbnail.gif" alt="blue" /></a>
		</li>
		<li>
			<h3>Sonix Ultrasonic Meters</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Sonix_Ultrasonic_Meters.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Sonix_Ultrasonic_Meters_Thumbnail.gif" alt="cyan" />
		</li>
		<li>
			<h3>TPL 9 and T 10</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/TPL_9_and_T_10.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/TPL_9_and_T_10_Thumbnail.gif" alt="green" /></a>
		</li>
		<li>
			<h3>Turbine Meters</h3>
			<span>Parts_by_Man_OK_By_Jon/Sensus/Turbine_Meters.gif</span>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam ut urna. Mauris nulla. Donec nec mauris. Proin nulla dolor, bibendum et, dapibus in, euismod ut, felis.</p>
			<a href="#"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Turbine_Meters_Thumbnail.gif" alt="orange" /></a>
		</li>
	</ul>
	<div id="wrapper">
		<div id="fullsize">
			<div id="imgprev" class="imgnav" title="Previous Image"></div>
			<div id="imglink"></div>
			<div id="imgnext" class="imgnav" title="Next Image"></div>
			<div id="image"></div>
			<div id="information">
				<h3></h3>
				<p></p>
			</div>
		</div>
		<div id="thumbnails">
			<div id="slideleft" title="Slide Left"></div>
			<div id="slidearea">
				<div id="slider"></div>
			</div>
			<div id="slideright" title="Slide Right"></div>
		</div>
	</div>
<script type="text/javascript" src="compressed.js"></script>
<script type="text/javascript">
	$('slideshow').style.display='none';
	$('wrapper').style.display='block';
	var slideshow=new TINY.slideshow("slideshow");
	window.onload=function(){
		slideshow.auto=true;
		slideshow.speed=5;
		slideshow.info="information";
		slideshow.thumbs="slider";
		slideshow.left="slideleft";
		slideshow.right="slideright";
		slideshow.scrollSpeed=4;
		slideshow.spacing=5;
		slideshow.active="#fff";
		slideshow.init("slideshow","image");
	}
</script>

<div id="ManBioWhtBkg"></div>

<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LrgProductDropShadow"></div>
<div id="LogoNavWhtBkg"></div>


<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>

<a href="privacy_policy.html" div id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.html" div id="TermsofService">Terms of Service</a>
<a href="site_map.html" div id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"></div>
<div id="LearnMoreFooterText"></div>
<div id="ENGBFooterSolidVideoBTN"></div>
<div id="ENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf">
        <param name='quality' value="high">
        <param name='bgcolor' value='#FFFFFF'>
        <param name='loop' value="true">
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;">
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" "value="" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;">
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;">
<input type="hidden" name="llr" value="qksvr8cab"> <input type="hidden" name="m" value="1102583613776"> 
<input type="hidden" name="p" value="oi"> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
 
</div>
</body>
</html>
