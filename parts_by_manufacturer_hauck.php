<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Hauck combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Hauck,AIG High Pressure Gas-Air Inspirators,BBG Gas Beta Burner,BIC ZIC Gas Burner,Ecomax Direct-Fired Self-Recuperative Metallic Gas Burner,ISER Single Ended Recuperative Burner for Immersion Firing,JAG Jet Air Gas Burner,NMG Nozzle Mix Gas Burner,Hauck PBG Packaged Gas Burner,Hauck Radimax Radiant Tube Plug-In Recuperator,Retain-A-Flame Gas Burner Nozzles,RFG - RadiFlame Radiant Tube Gas Burner,RKG Radiant Cone Gas Burner,SVG Super Versatile Gas Burner,WHG Hugger Gas Burner,WHG Package Wall Hugger Gas Fired Burner,WHI Wall Hugger Low NOx Burner,BBC Gas/Oil Combination Beta Burner,EJC EnerJet High Velocity Gas/Oil Combination Burners,ECO-Star II Packaged Low NOx Multi Fuel Burner,NMC Nozzle Mix Gas/Oil Combination Burner,NovaStar Ultra Low NOx Burner for Aggregate Drying,STARJET Open Fired Multi Fuel Burner,SVC Super Versatile Combination High Velocity Square Refractory Tile,WRC/WRO Wide Range Burners,780 PAC-L Series Self-Proportioning Combination Gas Oil Burners,FVA-FVS Adjustable Flow Valves for Clean Gas and Air,PVS Adjustiable Port Valves,BVA Butterfly Valves,Hauck LVG Gas Limiting Orifice Valve,Valvario Modular Solenoid Valves,Hauck WBV-H Wafer Butterfly Valves Hot Air,RRG Ratio Regulator Gas,OMG Orifice Meters,Hauck IC Ignition Wires/Ignitor and Transformer Terminal" />
<title>ETTER Engineering - Hauck Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="HauckLogoLarge"></div>
<div id="SensusText">Hauck Manufacturing Company, a subsidiary of the Elster Group, supplies knowledge, equipment, and controls for industrial combustion. Since 1888 Hauck has manufactured the most reliable industrial burners for global combustion applications including solutions for the asphalt, glass, brick, metal, and drying industries. Our control over a complete line of burners, a knowledgeable staff of engineers, and an innovative line of controls means a tightly integrated combustion solution to match your needs. Talk to Hauck Manufacturing Company experts today to see how our oil, gas, and combination burners can reduce your fuel costs and make your combustion process more efficient.</div>
<div id="ThumbnailBackground"></div>   
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/AIG_High_Pressure_Gas_Air_Inspirators.jpg" alt="Hauck AIG High Pressure Gas-Air Inspirators" title="Hauck AIG High Pressure Gas-Air Inspirators"/></div>
			<div id="PartsContent"><h3>AIG
			<br/><font color="#50658D">High Pressure Gas-Air Inspirators</font></h3>
			<p><br/>Hauck High Pressure AIG Inspirators Make it Possible to utilize the energy of the gas to inspirate the necessary air for combustion. The AIG inspirators may be used wherever high pressure gas form 1 lb. to 30 lb. is available. The AIG automatically maintains the gas-air ratio. No blower or combustion air. A single control valve is used for manual or automatic operation.
			<br/><br/>&#149; Maximum Air Entrainment
   			<br/>&#149; Requires No Blower or Compressor
   			<br/>&#149; Automatically Maintains Gas-air Ratio
   			<br/>&#149; Single Valve Control for Manual or 
			<br/>&nbsp;&nbsp; Automatic Operation
   			<br/>&#149; Easy to Adjust
   			<br/>&#149; Efficient - Simple - Rugged
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/aig-1.pdf" target="_blank"><font color="#ACB0C3"><b>AIG Fact Sheet</b></font></a>
			</p>
			</div></div>    
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/BBG_Gas_Beta_Burner.jpg" alt="Hauck BBG Gas Beta Burner" title="Hauck BBG Gas Beta Burner"/></div>
			<div id="PartsContent"><h3>BBG
			<br/><font color="#50658D">Gas Beta Burner</font></h3><p><br/>Hauck's BBG Gas Beta Burner is designed for exceptionally long life in continuous or intermittent operation in furnace, 
			kiln and dryer environments. 
			<br/><br/>The BBG is well suited for applications operating under backpressure. Burner capacities range 
			from 2.2 million to more than 87 million Btu/hr (645 to 25,500 kW). Preheated air versions are available up to 900&#176;F 
			(480&#176;C). 
			<br/><br/>The BBG is designed to operate on any clean industrial fuel gas with a higher heating value of 500 Btu/scf 
			(19.7MJ/nm&#179;) or greater with ambient or preheated combustion air. Available with a converging alloy tile for 
			ambient combustion air only for temperatures up to 1800&#176;F (980&#176;C), and converging or diverging refractory 
			tiles for temperatures up to 2800&#176;F (1540&#176;C).
			<br/><br/><b>Features</b>
   			<br/><br/>&#149; Designed for low pressure air operation
   			<br/>&#149; Rugged heavy gauge steel construction
   			<br/>&#149; Available with alloy or refractory tile
   			<br/>&#149; Direct spark or pilot ignition
   			<br/>&#149; UV flame supervision
   			<br/>&#149; Packaged versions available with air and 
			<br/>&nbsp;&nbsp; fuel manifolds
   			<br/>&#149; Preheated air up to 900&#176;F (480&#176;C)
			<br/><br/><b>Benefits</b>
   			<br/><br/>&#149; Supports a wide variety of applications
   			<br/>&#149; Highly reliable, durable design
   			<br/>&#149; Broad range of air/fuel ratios
   			<br/>&#149; Tile options allow burner to be tailored to 
			<br/>&nbsp;&nbsp; application
			<br/><br/><b>Advantages</b>
			<br/><br/>Beta burners are baffle type burners designed for low air pressure operation in a 
			wide range of applications including steel reheat furnaces, rotary kilns, gypsum kettles, 
			air heaters and incinerators. Available with alloy tile for ambient combustion air only 
			for temperatures up to 1800&#176;F (980&#176;C), and refractory tile for temperatures up to 2800&#176;F 
			(1540&#176;C). With a refractory tile, the 1000 and 2000 series can operate with air preheat 
			temperatures up to 600&#176;F (315&#176;C), and the 3000 series can operate with air preheat temperatures 
			up to 900&#176;F (480&#176;C).
			<br/><br/>The BBG burner will fire any clean industrial fuel gas with a higher heating value of 
			500 Btu/scf (19.7MJ/nm3) or greater with ambient or preheated combustion air. Capacities range 
			from 2.2 million to more than 87 million Btu/hr (645 to 25,500 kW).
			<br/><br/>On ratio turndown is approximately 8:1 with higher thermal turndown possible with 
			excess air firing. Nominal burner air supply pressure is 8 osig (3450 Pa). Nominal gas supply 
			pressure is 4 osig (1725 Pa). Hauck recommends the use of a self-supporting refractory or alloy 
			tile for soft and hard wall applications. An optional mandrel for field pouring of a burner port 
			can be supplied. Consult Hauck for mounting options.
			<br/><br/>Direct spark ignition is available on the BBG_ 204 through _212 burners, while gas pilot 
			ignition is available on all size burners. The BBG burner can utilize UV flame supervision.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/bbg-1.pdf" target="_blank"><font color="#ACB0C3"><b>BBG Fact Sheet</b></font></a>
			</p>
			</div></div>       
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/BIC_ZIC_Gas_Burner.jpg" alt="Hauck BIC ZIC Gas Burner" title="Hauck BIC ZIC Gas Burner"/></div>
			<div id="PartsContent"><h3>BIC ZIC
			<br/><font color="#50658D">Gas Burner</font></h3>
			<p><br/>The BIC and ZIC burners are suitable for use on gas fired industrial furnaces, ceramic kilns and ovens. 
			Both are appropriate in the iron and steel, non-ferrous metals, precious metals, ceramic, glass and food industries. 
			<br/><br/>The BIC and ZIC burners utilize a lightweight tube manufactured from a high quality SiC silicon carbide. 
			The silicon carbide tube is well-suited for installations using ceramic fiber insulation and brick linings.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.hauckburner.com/pdf/bic_zic.pdf" target="_blank"><font color="#ACB0C3"><b>BIC ZIC Literature</b></font></a>
			</p>
			</div></div>       
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/Ecomax_Direct_Fired_Self_Recuperative_Metallic_Gas_Burner.jpg" alt="Hauck Ecomax Direct-Fired Self-Recuperative Metallic Gas Burner" title="Hauck Ecomax Direct-Fired Self-Recuperative Metallic Gas Burner"/></div>
			<div id="PartsContent"><h3>Ecomax&#174;
			<br/><font color="#50658D">Direct-Fired
			<br/>Self-Recuperative 
			<br/>Metallic Gas Burner</font></h3>
			<p><br/>Hauck and LBE are jointly offering the Ecomax&#174; direct-fired self-recuperative metallic gas burners for high 
			temperature furnace applications in the US market. Equipped with an eductor system, the burner is capable of 100&#37; 
			exhaust gas removal. 
			<br/><br/>The high temperature alloy recuperator is designed to allow for maximum combustion air preheat which results 
			in efficiencies up to 70&#37; for maximum fuel savings. Capable of firing any clean industrial fuel gas with a higher 
			heating value of 500 Btu/scf (19.7 MJ/nm&#179;) or greater, the Ecomax&#174; is available in five sizes with capacities 
			from 57,000 to 945,000 Btu/hr (15 to 250 kW) for process temperatures up to 2100&#176;F (1150&#176;C). 
			<br/><br/>Higher temperature and low NOx emissions versions are available.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Self-recuperative design
			<br/>&#149; Heat resistant silicon carbide combustor 
			<br/>&nbsp;&nbsp; and cast alloy recuperator
			<br/>&#149; Various recuperator lengths available
			<br/>&#149; Insulated cast aluminum body
			<br/>&#149; Multiple air/gas inlet and exhaust gas 
			<br/>&nbsp;&nbsp; outlet orientations
			<br/>&#149; Integral air and gas orifices
			<br/>&#149; High exit velocities up to 525 ft/sec 
			<br/>&nbsp;&nbsp; (160 m/sec)
			<br/>&#149; Direct spark igniter/flame rod; UV flame 
			<br/>&nbsp;&nbsp; supervision option
			<br/>&#149; Low NOx version available
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Eliminates the need for separate 
			<br/>&nbsp;&nbsp; centralized recuperator system
			<br/>&#149; Efficiencies up to 70% for maximum fuel 
			<br/>&nbsp;&nbsp; savings
			<br/>&#149; Air preheat up to 1490&#176;F (810&#176;C)
			<br/>&#149; Eductor capable of 100% exhaust gas 
			<br/>&nbsp;&nbsp; removal
			<br/>&#149; Furnace temperature uniformity via pulse 
			<br/>&nbsp;&nbsp; firing and high exit velocity
			<br/>&#149; Low emissions option to achieve 
			<br/>&nbsp;&nbsp; regulatory agency requirements
			<br/><br/><b>Advantages</b>
			<br/><br/>The Ecomax&#174; burner is widely used in the heat treating industry for direct heated roller hearth 
			and batch furnaces. It utilizes preheated combustion air from the furnace exhaust gas allowing the burner 
			to achieve a maximum combustion air temperature of 1490&#176;F (810&#176;C). The energy savings coupled with the 
			burner's simple design and easy installation result in cost effective payback periods.
			<br/><br/>The burner is constructed with a multifunctional housing through which it is mounted, supplied 
			with air, cooled and exhaust gas is discharged. This makes the burner easy to assemble and install. The 
			housing is constructed of insulated cast aluminum and is double-walled for durability.
			<br/><br/>The Ecomax&#174; is self-recuperative utilizing hot furnace exhaust gas for preheating the combustion 
			air. The heat resistant alloy recuperator and silicon carbide combustor allow the burner to stand up to the 
			high temperature environment.
			<br/><br/>The burner's eductor system allows up to 100% of exhaust gas removal. This eliminates the need for 
			a complicated, costly centralized recuperator, insulated hot air piping and the inherent maintenance issues.
			<br/><br/>The burner is designed for process temperatures up to 2100&#176;F (1150&#176;C) with higher temperature versions 
			available. High exit velocities up to 525 ft/sec (160 m/sec) are achieved resulting in excellent furnace temperature 
			uniformity.
			<br/><br/>The Ecomax&#174; is designed to operate on any clean industrial fuel gas with a higher heating value of 500 
			Btu/scf (19.7 MJ/nm�) or greater. The burner is available in five sizes with capacities from 57,000 to 945,000 
			Btu/hr (15 to 250 kW).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/ecomax-1.pdf" target="_blank"><font color="#ACB0C3"><b>EcoMax Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ecomax.pdf" target="_blank"><font color="#ACB0C3"><b>EcoMax Specifications</b></font></a></p>
			</p>
			</div></div>       
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/ISER_Single_Ended_Recuperative_Burner_for_Immersion_Firing.jpg" alt="Hauck ISER Single Ended Recuperative Burner for Immersion Firing" title="Hauck ISER Single Ended Recuperative Burner for Immersion Firing"/></div>
			<div id="PartsContent"><h3>ISER
			<br/><font color="#50658D">Single Ended Recuperative Burner for Immersion Firing</font></h3>
			<p><br/>Hauck's ISER immersion burner represents the latest burner technology for liquid aluminum and zinc heating with high thermal efficiency, durability, and reduced NOx emissions. The design incorporates Hauck's highly successful SVG burner technology for excellent turndown capabilities, proven performance, and reliable direct spark ignition. The burner combustor/recuperator is made of silicon carbide, providing long component life.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Designed for galvanizing and liquid 
			<br/>&nbsp;&nbsp; aluminum and zinc applications
			<br/>&#149; Compact design
			<br/>&#149; Self-recuperative
			<br/>&#149; Silicon carbide combustor/recuperator 
			<br/>&nbsp;&nbsp; tube
			<br/>&#149; Insulated exhaust gas body
			<br/>&#149; Rugged ceramic immersion tube
			<br/>&#149; Direct spark ignition
			<br/>&#149; UV or flame rod flame supervision
			<br/>&#149; Excellent turndown capability
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Excellent temperature uniformity of metal 
			<br/>&nbsp;&nbsp; bath
			<br/>&#149; High heat transfer to liquid metal
			<br/>&#149; Efficiencies up to 60% for maximum 
			<br/>&nbsp;&nbsp; energy savings
			<br/>&#149; Reduced surface oxidation and dross 
			<br/>&nbsp;&nbsp; losses
			<br/>&#149; Low NOx emissions via internal exhaust 
			<br/>&nbsp;&nbsp; gas recirculation
			<br/><br/><b>Advantages</b>
			<br/><br/>&#149; High Thermal Efficiencies
			<br/><br/>The ISER was designed for the unique operating characteristics required for liquid aluminum and zinc applications. It operates by conducting the heat of combustion through the wall of the ceramic immersion tube. Efficient transfer of the available heat is assured through the thermal properties of the liquid metal in the bath where the tube is placed.
			<br/><br/>New galvanizing baths using an ISER burner can be more compact, more efficient, and allow more accurate process control than comparative systems. Conventional systems can be redesigned to take advantage of this compact heat source.
			<br/><br/>Thermal shock resistant silicon carbide combustor/recuperator tubes permit long life of ISER burners used in aluminum filter boxes, die casting holding furnaces, and other immersion applications.
			<br/><br/>Careful design of bath equipment can yield surface area heat inputs up to 10 times greater than with conventional direct fired equipment. This leads to smaller baths and reduced structural heat loss.
			<br/><br/>The use of the immersion burner design assures that products of combustion are prevented from coming in contact with the metal. This results in significant reduction of surface oxidation, dross losses, and hydrogen absorption in the bath.
			<br/><br/>It is possible to achieve very accurate bath temperature control during processing. This ensures repeatability and product quality control during the process. Because they are self-recuperative, ISER burners operate at thermal efficiencies up to 60%, contributing to significant energy savings over direct fired burners.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.hauckburner.com/pdf/iser.pdf" target="_blank"><font color="#ACB0C3"><b>ISER Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/iser-6.pdf" target="_blank"><font color="#ACB0C3"><b>ISER Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/iser-9.pdf" target="_blank"><font color="#ACB0C3"><b>ISER Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/iser-1.pdf" target="_blank"><font color="#ACB0C3"><b>ISER Fact Sheet</b></font></a></p>
			</p>
			</div></div>       
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/JAG_Jet_Air_Gas_Burner.jpg" alt="Hauck JAG Jet Air Gas Burner" title="Hauck JAG Jet Air Gas Burner"/></div>
			<div id="PartsContent"><h3>JAG 
			<br/><font color="#50658D">Jet Air Gas Burner</font></h3>
			<p><br/>The Hauck JAG Jet Air Gas Burner is specifically designed for homogenizing and solution heat treating applications. At the heart of the JAG design is the proven reliability of the SVG burner family which provides excellent stability and performance. The integral firing tube provides the flame with complete  protection from recirculation air cross velocities typically present in homogenizing and heat treat applications which can result in high carbon monoxide (CO) emissions. In combination with the high exit velocity of the JAG burner, the furnace gas recirculation and flame protection features of the firing tube provide low NOx and CO emissions.  The JAG is direct spark ignited and can be flame supervised with a flame rod or UV scanner.  The burner's combustor, firing and recirculation tubes are constructed from temperature resistant stainless steel alloys. Additionally, the modular construction allows for easy installation.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/jag-1.pdf" target="_blank"><font color="#ACB0C3"><b>JAG Fact Sheet</b></font></a>
			</p>
			</div></div>       
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/NMG_Nozzle_Mix_Gas_Burner.jpg" alt="Hauck NMG Nozzle Mix Gas Burner" title="Hauck NMG Nozzle Mix Gas Burner"/></div>
			<div id="PartsContent"><h3>NMG 
			<br/><font color="#50658D">Nozzle Mix Gas Burner</font></h3>
			<p><br/>Designed for applications requiring a general purpose long life, low maintenance gas burner. Performs equally well firing on-ratio, with excess fuel or with excess air. NMG-H model utilizes preheated air to 8000&#176;F. All models can be converted for oil and gas firing. Mounting plate and tile included with burner. NMG capacity range: 224,000 to 25 million Btu/hr &#64; 16 oz.
			<br/><br/><b>Operation.</b> Combustion air and fuel are channeled into the burner nozzle separately. Since the burner nozzle is sealed into the refractory tile, all combustion air is supplied through the burner. The NMG burners couple a uniform flame front with flame stability over the entire operating range.
			<br/><br/>NMG burners can be controlled manually or automatically. Automatic control normally employs a Hauck Ratio Regulator for each control zone to maintain air-fuel ratio. An alternate system uses control valves in each of the fuel and air lines, linking the valves to a single motor controller.
			<br/><br/>Hauck's hot air Nozzle Mix gas burner (NMG-H) provides fuel savings by utilizing preheated combustion air (to 800&#176;F). Application of the burner in recuperation systems results in an economical approach to energy saving.
			<br/><br/><b>Construction.</b> With no moving parts, the burner is virtually maintenance free. Parts subject to heat are either of heat resisting cast iron or stainless steel. The tile is manufactured, dried and cured by Hauck. Special refractory materials and jacketed tiles can be supplied upon request.
			<br/><br/><b>Mounting.</b> The burner, mounting plate, and refractory tile are shipped as an assembled unit. The burner can be mounted to fire in any position - horizontal, vertical up, or vertical down. The air connection can be rotated to any of three other positions. The gas inlet is through the back of the burner.
			<br/><br/>If the pilot and secondary air are to be in line, a 45&#176; No. 1 spark ignited gas pilot, Model No. IPG2411, must be used with NMG 210 through 230 burners to avoid interference. (Alternately, if a 9 o'clock secondary air position is required and it is desired to use the standard No. 1 pilot, Model IPG1411A, the mounting plate may be rotated so as to position the pilot at either the 12 o'clock or 3 o'clock position. Each NMG is equipped with companion flanges on the main air connections to permit easy burner installation and removal and allow the mounting of an orifice plate for reduced air flows, if required.
			<br/><br/><b>Pilot.</b> The burner mounting plate includes a port for direct spark or gas pilot ignition system. The pilot is required for initial burner ignition only and is not required to maintain ignition.
			<br/><br/><b>Flame Supervision.</b>The NMG mounting plate is provided with a port for monitoring the pilot and main flame, using a UV scanner or other suitable device.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/nmg-1.pdf" target="_blank"><font color="#ACB0C3"><b>NMG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmg.pdf" target="_blank"><font color="#ACB0C3"><b>NMG Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmg-6.pdf" target="_blank"><font color="#ACB0C3"><b>NMG Part List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmg-9.pdf" target="_blank"><font color="#ACB0C3"><b>NMG Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/PBG_Packaged_Gas_Burner.jpg" alt="Hauck PBG Packaged Gas Burner" title="Hauck PBG Packaged Gas Burner"/></div>
			<div id="PartsContent"><h3>PBG 
			<br/><font color="#50658D">Packaged Gas Burner</font></h3>
			<p><br/>Designed for simple installation and factory set for ease of operation, the Hauck packaged gas burner can be used for many process heating applications. With a nominal 40 to 1 thermal turndown capability, it is an ideal choice for heat processes that require rapid heat ramp rates with no temperature overshoot. The PBG has all of the necessary components for a high turndown combustion system in one assembly.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Direct spark ignition
			<br/>&#149; Available in three combustor styles: 
			<br/>&nbsp;&nbsp; straight, elbow alloy tube, or  
			<br/>&nbsp;&nbsp; refractory tile
			<br/>&#149; Choice of flame rod or UV scanner flame 
			<br/>&nbsp;&nbsp; supervision
 			<br/>&#149; Available in basic burner, configurable, or 
			<br/>&nbsp;&nbsp; custom engineered package
			<br/>&#149; Operates on any clean industrial gas
			<br/>&#149; NFPA compliant packages available
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; High thermal turndown
			<br/>&#149; Excellent excess air limits
			<br/>&#149; Factory pre-set for ease of start-up and 
			<br/>&nbsp;&nbsp; operation
			<br/>&#149; Gas ratio regulator automatically 
			<br/>&nbsp;&nbsp; compensates for varying pressures
  			<br/>&#149; Low CO and NOx emissions
    			<br/>&#149; Self-cleaning fan blade reduces 
			<br/>&nbsp;&nbsp; maintenance costs
			<br/><br/><b>Features</b>
			<br/><br/>The Hauck PBG Packaged Gas Burner is designed for a wide variety of process heating applications such as paint baking ovens, indirect heat exchangers, meat smokehouses, paper drying systems, glass tempering, textile dryers, and many other heat processes.
			<br/><br/>Seven different model PBG burners are available with maximum capacity of 440,000 to 4,660,000 Btu/hr (LHV of 100 to 1030 kW at 50 Hz operation).
			<br/><br/>The PBG burner can be operated with any clean industrial fuel gas with a calorific value of 2500 Btu per cubic foot (LHV of 91.2 MJ/nm3) or less. Cross connection of the air pressure to the gas ratio regulator allows for low excess air at high fire capacity, while use of a bypass valve around the regulator enables high excess air at low fire capacity - the result being high thermal turndown.
			<br/><br/>For applications up to 1850&#176;F (1010&#176;C), high temperature alloy combustor tubes can be selected; a refractory tile option is also available for temperatures up to 2300&#176;F (1260&#176;C).
			<br/><br/>Each PBG is factory pre-set for ease of start-up and operation. All you have to do is pipe, wire, and fire. As with all Hauck products, the PBG is backed by our nationally recognized service department.
			</p>
			</div></div>       
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/Radimax_Radiant_Tube_Plug_In_Recuperator.jpg" alt="Hauck Radimax Radiant Tube Plug-In Recuperator" title="Hauck Radimax Radiant Tube Plug-In Recuperator"/></div>
			<div id="PartsContent"><h3>Radimax 
			<br/><font color="#50658D">Radiant Tube Plug-In Recuperator</font></h3>
			<p><br/>The Radimax plug-in recuperator is designed for maximum heat transfer and fuel efficiency from radiant tube combustion systems. For use with 6 inch ID or larger tubes, the product can be installed in U tubes, W tubes and Trident&reg; tubes. Supports exhaust temperatures to 19000&#176;F (10380&#176;C). Heavy-duty cast construction.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/rad-1.pdf" target="_blank"><font color="#ACB0C3"><b>RAD Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rad.pdf" target="_blank"><font color="#ACB0C3"><b>RAD Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rad-6.pdf" target="_blank"><font color="#ACB0C3"><b>RAD Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rad-9.pdf" target="_blank"><font color="#ACB0C3"><b>RAD Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/Retain_A_Flame_Gas_Burner_Nozzles.jpg" alt="Hauck Retain-A-Flame Gas Burner Nozzles" title="Hauck Retain-A-Flame Gas Burner Nozzles"/></div>
			<div id="PartsContent"><h3>Retain-A-Flame 
			<br/><font color="#50658D">Gas Burner Nozzles</font></h3>
			<p><br/>Hauck's 'Retain-a-Flame' burner nozzles are recommended for firing of gas-air mixtures where a small amount of excess air induced around the nozzle is allowable. These nozzles are ideal for firing either into the open ports of furnaces, or into the open without a combustion chamber. They have been successfully used for firing kilns, heat treating and melting furnaces, ovens, air heaters, boilers, kettles, immersion tubes and for ladle heating or drying.
			<br/><br/><b>Features</b>
    			<br/><br/>&#149; Designed to produce long cylindrical 
			<br/>&nbsp;&nbsp; blast flame
    			<br/>&#149; Manual or spark ignition option
    			<br/>&#149; Available with threaded or flanged 
			<br/>&nbsp;&nbsp; connections
     			<br/>&#149; Available in straight or elbow options
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Produces long flame for greater heat 
			<br/>&nbsp;&nbsp; distribution
   			<br/>&#149; Excellent flame stability and retention
    			<br/>&#149; Minimizes possibility of flashback on low 
			<br/>&nbsp;&nbsp; fire
			<br/><br/><b>Advantages</b>
			<br/><br/>Retain-a-Flame nozzles are available in two versions: the Series 1100 RFS straight nozzle and the Series 1200 RFE elbow nozzle. They are available in sizes ranging from 40,000 to 2 million Btu/hr (12 to 586 kW).
			<br/><br/>These nozzles will retain the flame at their tips under usual operating conditions. Flame retention is achieved by removing part of the air-gas supply and burning it in a recess around the main burner opening. The nozzle design permits a wider range of mixture pressures without blowing the flame off the burner tip or backfiring. Little turbulence is caused in the main stream of gas mixture, with a resulting long, cylindrical flame of good stability.
			<br/><br/>All RFS straight nozzles are designed to produce a long cylindrical, torch type blast flame of unusually good stability. These flame characteristics provide heat distribution over a greater distance. These nozzles may be ignited manually and, if properly equipped, by a spark ignition system.
			<br/><br/>The RFE elbow and straight nozzles combine, in one unit, an excellent flame retention capability and the ease of mounting associated with flanged units. These highly functional units have many advantages. Their unique design not only permits a wider range of operation but also ensures the proper delivery of the mixture to the nozzle, thus minimizing the possibility of flashback on low fire.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/raf-1.pdf" target="_blank"><font color="#ACB0C3"><b>RAF Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/raf.pdf" target="_blank"><font color="#ACB0C3"><b>RAF Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/raf-9.pdf" target="_blank"><font color="#ACB0C3"><b>RAF Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/RFG_RadiFlame_Radiant_Tube_Gas_Burner.jpg" alt="Hauck RFG - RadiFlame Radiant Tube Gas Burner" title="Hauck RFG - RadiFlame Radiant Tube Gas Burner"/></div>
			<div id="PartsContent"><h3>RFG - RadiFlame 
			<br/><font color="#50658D">Radiant Tube Gas Burner</font></h3>
			<p><br/>Hauck's RFG radiant tube burners provide reliable ignition, flame stability and uniform heat distribution in all radiant tubes.  The fixed spin plate construction allows for specific and repeatable air flows with a standard flame length relative to the burner's capacity.  RFG burners are available with capacities from 200,000 to 1,200,000 Btu/hr (58 to 317 kW).  The RFG fires any clean industrial fuel gas with a higher heating value of 500 Btu per cubic foot (19.7 MJ/nm&#179;) or greater with ambient or preheated combustion air.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.hauckburner.com/pdf/rfg-1.pdf" target="_blank"><font color="#ACB0C3"><b>RFG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rfg-9.pdf" target="_blank"><font color="#ACB0C3"><b>RFG Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rfg-6.pdf" target="_blank"><font color="#ACB0C3"><b>RFG Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rfg.pdf" target="_blank"><font color="#ACB0C3"><b>RFG Specifications</b></font></a></p>
			</p>
			</div></div>       
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/RKG_Radiant_Cone_Gas_Burner.jpg" alt="Hauck RKG Radiant Cone Gas Burner" title="Hauck RKG Radiant Cone Gas Burner"/></div>
			<div id="PartsContent"><h3>RKG 
			<br/><font color="#50658D">Radiant Cone Gas Burner</font></h3>
			<p><br/>The Hauck RKG radiant cone burner is designed for applications requiring even heat distribution with a short flame length. The short flame allows the burner to be placed close to the load. The RKG burner is available in six sizes, with a capacity range of 177,000 to 2.3 million Btu/hr (47 to 611 kW). RKG hot air versions are available for preheated air up to 800&#176;F (425&#176;C).
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Compact ball-shaped flame
    			<br/>&#149; Direct spark or gas pilot ignition
    			<br/>&#149; Stable operation at ratio in cold furnace
    			<br/>&#149; Roof or sidewall mounting
    			<br/>&#149; UV or flame rod flame supervision
    			<br/>&#149; Preheated air up to 800&#176;F (425&#176;C)
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Compact flame produces even heating.
    			<br/>&#149; Immediate ramping to high fire reduces 
			<br/>&nbsp;&nbsp; furnace heat-up time.
    			<br/>&#149; Minimum distance requirement from 
			<br/>&nbsp;&nbsp; burner to load.
			<br/><br/><b>Advantages</b>
			<br/><br/>The RKG compact flame pattern promotes even heating by radiation from the furnace walls and roof. The short flame allows the burner to be placed close to the load without flame impingement.
			<br/><br/>The RKG incorporates a port for monitoring the pilot and main flames with either a UV scanner or optional flame rod. An optional spark igniter kit is available. The burner, mounting plate, and refractory tile are shipped as an assembled unit ready for mounting on the furnace. The RKG can be installed to fire in any position.
			<br/><br/>The RKG can be ignited and brought to high fire immediately, even in a cold, tight furnace. This reduces furnace heat-up.
			<br/><br/>The RKG performs equally well when firing on-ratio, with excess fuel up to 20%, or with excess air limits ranging from 280 to 400% at 16 osig (6900 Pa) inlet air pressure.
			<br/><br/>The RKG may be operated in furnaces with chamber temperatures up to 2500&#176;F (1370&#176;C).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/rkg-1.pdf" target="_blank"><font color="#ACB0C3"><b>RKG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rkg.pdf" target="_blank"><font color="#ACB0C3"><b>RKG Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rkg-6.pdf" target="_blank"><font color="#ACB0C3"><b>RKG Part List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rkg-9.pdf" target="_blank"><font color="#ACB0C3"><b>RKG Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/SVG_Super_Versa_Tile_Gas_Burner.jpg" alt="Hauck SVG Super Versatile Gas Burner" title="Hauck SVG Super Versatile Gas Burner"/></div>
			<div id="PartsContent"><h3>SVG 
			<br/><font color="#50658D">Super Versatile Gas Burner</font></h3>
			<p><br/>Hauck's modern SVG Super Versatile Gas Burners are designed for applications that benefit from intensive combustion gas recirculation, increased efficiency, improved temperature uniformity, and substantially reduced emissions. Offered in three separate series. The 100 series has full capacity at 16 osig (6,900 Pa) while the 200 series reaches the same capacity at an air inlet pressure of 8 osig (3,450 Pa). The 'E' series has the rating of the 200 series with metric air and gas connections. Available in a variety of tile materials and configurations, the SVG burner family includes medium and high velocity models. SVG burners reliably and dependably fire any clean industrial fuel gas over a wide operational range from excess air to excess fuel.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Alloy encased refractory tile
  			<br/>&#149; Available with flange, lugs or slip-in 
			<br/>&nbsp;&nbsp; mounting
  			<br/>&#149; Reliable gas pilot ignition
  			<br/>&#149; Integral air orifice
  			<br/>&#149; Equipped with Hauck self- cleaning micro 
			<br/>&nbsp;&nbsp; oil valve
  			<br/>&#149; UV flame supervision option
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Combination gas/oil flexibility without 
			<br/>&nbsp;&nbsp; loss of performance
  			<br/>&#149; High exit velocity for temperature 
			<br/>&nbsp;&nbsp; uniformity
  			<br/>&#149; Easy installation
  			<br/>&#149; Excellent stability firing gas or oil
			<br/><br/><b>Advantages</b>
			<br/><br/>&#149; Improved temperature uniformity 
			<br/>&nbsp;&nbsp; throughout
  			<br/>&#149; High exhaust gas velocity
  			<br/>&#149; Alloy encased self-supporting 
			<br/>&nbsp;&nbsp; refractory tile
  			<br/>&#149; Excellent stability firing gas or oil over 
			<br/>&nbsp;&nbsp; entire operating range
			<br/><br/>The SVC burners offer a rugged design with no reduction in performance or efficiency seen in conventional dual fuel high velocity burners. The alloy encased refractory tile ensures that the burner is self-supporting even in a soft wall application. Mounting options available include a flange, lugs, or slip-in arrangement. Each burner also includes a Hauck self-cleaning micro oil valve that functions as an oil limiting valve.
			<br/><br/>The SVC will fire any clean industrial fuel gas with a higher heating value of 500 Btu/scf (19.7 MJ/nm3) or greater or light fuel oil through No. 2. The air staging design results in reduced NOx emissions compared to conventional dual fuel high velocity burners.
			<br/><br/>The SVC will operate over a very wide range of air pressures and air/fuel ratios. Firing gas, excess air limits at 16 osig (6900 Pa) range from 1200 to 4000%. Firing No. 2 fuel oil, excess air limits at 16 osig (6900 Pa) range from 240 to 390%.
			<br/><br/>SVC burners can be operated in low or high temperature furnaces up to 2400&#176;F (1320&#176;C). The burner is designed for ignition using a gas pilot. A UV flame scanner can be accommodated on the burner backplate.
			<br/><br/>Burner control firing No. 2 oil can be achieved by cross-connected ratio control. Gas firing can be by pulse firing, cross-connected ratio or fuel only control.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/svg-1.pdf" target="_blank"><font color="#ACB0C3"><b>SVG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/svg.pdf" target="_blank"><font color="#ACB0C3"><b>SVG Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/svg-6.pdf" target="_blank"><font color="#ACB0C3"><b>SVG Part List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/svg-9.pdf" target="_blank"><font color="#ACB0C3"><b>SVG Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/WHG_Hugger_Gas_Burner.jpg" alt="Hauck WHG Hugger Gas Burner" title="Hauck WHG Hugger Gas Burner"/></div>
			<div id="PartsContent"><h3>WHG 
			<br/><font color="#50658D">Hugger Gas Burner</font></h3>
			<p><br/>The Hauck WHG wall hugger burner is designed for applications requiring even heat distribution with no flame impingement. The flame hugs the wall, allowing the burner to be placed close to the load. The WHG burner is available in six sizes, with a 16 osig (6,900 Pa) capacity range of 175,000 to 2.3 million Btu/hr (47 to 611 kW). WHG hot air versions are available for preheated air up to 800&#176;F (425&#176;C).
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Flat flame profile throughout firing 
			<br/>&nbsp;&nbsp; range
  			<br/>&#149; Direct spark or gas pilot ignition
  			<br/>&#149; Stable operation at ratio in cold furnace
  			<br/>&#149; UV or flame rod flame supervision
  			<br/>&#149; Preheated air up to 800&#176;F (425&#176;C)
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Flat flame produces even heating
  			<br/>&#149; Immediate ramping to high fire reduces 
			<br/>&nbsp;&nbsp; furnace heat-up time
  			<br/>&#149; Even heat distribution with no flame 
			<br/>&nbsp;&nbsp; impingement
			<br/><br/><b>Advantages</b>
			<br/><br/>The WHG flat flame pattern promotes even heating by radiation from the furnace walls and roof. The wall-hugging flame allows the burner to be placed close to the load without flame impingement.
			<br/><br/>The WHG incorporates a port for monitoring the pilot and main flames with either a UV scanner or flame rod. Direct spark or gas pilot ignition is available. The burner, mounting plate, and refractory tile are shipped as an assembled unit ready for mounting on the furnace. The WHG can be installed to fire in any position.
	 		<br/><br/>The WHG can be ignited and brought to high fire immediately, even in a cold, tight furnace. This reduces furnace heat-up time.
			<br/><br/>The WHG performs equally well when firing on-ratio, or with excess air limits ranging from 175 to 400% at 16 osig (6900 Pa) inlet air pressure.
			<br/><br/>The WHG may be operated in furnaces with chamber temperatures up to 2500&#176;F (1370&#176;C).
			<br/><br/>Also available as a complete packaged combustion system as the WHG Packaged Burner.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/whg-1.pdf" target="_blank"><font color="#ACB0C3"><b>WHG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whg.pdf" target="_blank"><font color="#ACB0C3"><b>WHG Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whg-6.pdf" target="_blank"><font color="#ACB0C3"><b>WHG Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whg-9.pdf" target="_blank"><font color="#ACB0C3"><b>WHG Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/WHG_Package_Wall_Hugger_Gas_Fired_Burner.jpg" alt="Hauck WHG Package Wall Hugger Gas Fired Burner" title="Hauck WHG Package Wall Hugger Gas Fired Burner"/></div>
			<div id="PartsContent"><h3>WHG 
			<br/><font color="#50658D">Package Wall Hugger 
			<br/>Gas Fired Burner</font></h3>
			<p><br/>Specially designed for single burner roof fired applications, the WHG Package has all the benefits of the Hauck WHG burner in a complete combustion unit. Particularly well-suited to replace electric heating elements in small aluminum holding furnaces. Capacities range from 333,000 to 1,063,000 Btu/hr (88 kW to 281 kW). Capacities &#64; 10&#37; excess air and blower motor operation &#64; 75Hz.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Complete combustion system
  			<br/>&#149; Durable WHG burner designed for direct 
			<br/>&nbsp;&nbsp; fired applications
  			<br/>&#149; Burner flame that 'hugs' the wall
  			<br/>&#149; Pre-piped, pre-wired and skid mounted
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Complete packaged combustion system 
			<br/>&nbsp;&nbsp; for ease of installation
  			<br/>&#149; Flat flame produces even heating
  			<br/>&#149; Ideal for smaller furnaces
  			<br/>&#149; Immediate ramping to high fire reduces 
			<br/>&nbsp;&nbsp; furnace heat-up
			<br/><br/><b>Advantages</b>
			<br/><br/>The WHG Package provides the operational benefits of Hauck's WHG burner within the convenience of a complete packaged combustion system.
			<br/><br/>The WHG burner is designed for direct fired applications, or for replacing electrical heating systems, requiring even heat distribution and no flame impingement. The flame hugs the wall, allowing the burner to be place close to the load. The flat flame pattern promotes even heating by radiation from the furnace walls and roof. The baffle walls and chambers necessary for the firing of conventional burners are not necessary with the WHG. This permits the burner to be installed in smaller furnaces and simplified interiors.
			<br/><br/>The WHG can be ignited and ramped to high fire immediately, even in a cold, tight furnace. This reduces furnace heat-up time.
			<br/><br/>WHG Package Includes:
			<br/><br/>&#149; Burner Unit with Spark Ignition
  			<br/>&#149; UV Scanner
  			<br/>&#149; Spark Generator
  			<br/>&#149; Blower Assembly with TEFC Motor
  			<br/>&#149; Gas Orifice Meter
  			<br/>&#149; Gas Filter
  			<br/>&#149; Air and Gas Pressure Switches
  			<br/>&#149; Gas Safety Shutoff Valves
  			<br/>&#149; Gas Vent Valve
  			<br/>&#149; Gas/Air Ratio Regulator
  			<br/>&#149; Limiting Gas Valve
  			<br/>&#149; Mounting Skid
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/whgp-1.pdf" target="_blank"><font color="#ACB0C3"><b>WHG-P Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whgp.pdf" target="_blank"><font color="#ACB0C3"><b>WHG-P Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whgp-6.pdf" target="_blank"><font color="#ACB0C3"><b>WHG-P Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whgp-9.pdf" target="_blank"><font color="#ACB0C3"><b>WHG-P Instructions</b></font></a></p>
			</p>
			</div></div>     
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/WHI_Wall_Hugger_Low_NOx_Burner.jpg" alt="Hauck WHI Wall Hugger Low NOx Burner" title="Hauck WHI Wall Hugger Low NOx Burner"/></div>
			<div id="PartsContent"><h3>WHI 
			<br/><font color="#50658D">Wall Hugger 
			<br/>Low NOx Burner</font></h3>
			<p><br/>Hauck's WHI Wall Hugger Invisiflame&reg; burner is designed for applications requiring even heat distribution with no flame impingement AND low NOx emissions. The flame hugs the wall, even at high turndown ratios, allowing the burner to be placed close to the load. The WHI's three-staged air injection design maximizes production efficiency while minimizing NOx and CO emissions.  Low excess air operation (5&#37;) results in outstanding fuel efficiency. The WHI fires any clean industrial fuel gas with a higher heating value of 500 Btu per cubic foot (19.7 MJ/nm&#179;) or greater with ambient or preheated combustion air.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.hauckburner.com/pdf/whi-1.pdf" target="_blank"><font color="#ACB0C3"><b>WHI Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whi-9.pdf" target="_blank"><font color="#ACB0C3"><b>WHI Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whi-6.pdf" target="_blank"><font color="#ACB0C3"><b>WHI Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/whi.pdf" target="_blank"><font color="#ACB0C3"><b>WHI Specificaions</b></font></a></p>
			</p>
			</div></div>       
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/BBC_Gas_Oil_Combination_Beta_Burner.jpg" alt="Hauck BBC Gas/Oil Combination Beta Burner" title="Hauck BBC Gas/Oil Combination Beta Burner"/></div>
			<div id="PartsContent"><h3>BBC 
			<br/><font color="#50658D">Gas/Oil Combination 
			<br/>Beta Burner</font></h3>
			<p><br/>Hauck's BBC Gas/Oil Combination Beta Burner is designed for exceptionally long life in continuous or intermittent operation in furnace, kiln and dryer environments. The BBC is well suited for applications operating under back pressure. Burner capacities range from 2.5 million to more than 100 million Btu/hr (730 to 29,300 kW). Preheated air versions are available up to 900&#176;F (480&#176;C). The BBC is designed to operate with all types of industrial fuel oils, including heavy oils, and with all industrial fuel gasses with or without preheat. The burner can fire with diverging tiles or converging tiles for shorter, higher velocity flames.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Designed for low pressure air operation
   			<br/>&#149; Spark ignited pilot
   			<br/>&#149; Available with stainless steel or refractory
			<br/>&nbsp;&nbsp; baffle
   			<br/>&#149; UV flame supervision
   			<br/>&#149; Partial or complete packaged versions available with air and fuel manifolds
   			<br/>&#149; Rugged heavy gauge steel construction
   			<br/>&#149; Preheated air versions to 900&#176;F (480&#176;C)
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Supports a wide variety of applications
   			<br/>&#149; Highly reliable, durable design
   			<br/>&#149; Broad range of air/fuel ratios
   			<br/>&#149; Convergent tile provides higher velocity
			<br/>&nbsp;&nbsp; flames for higher convective heat
			<br/>&nbsp;&nbsp; transfer
   			<br/>&#149; Diverging tile permits ignition from hot
			<br/>&nbsp;&nbsp; furnace
			<br/><br/><b>Advantages</b>
			<br/><br/>Beta burners are baffle type burners designed for low air pressure operation in a wide range of applications
			including steel reheat furnaces, rotary kilns, gypsum kettles, air heaters and incinerators. Available with stainless 
			steel or refractory baffle with application temperatures of 2000&#176;F (1090&#176;C) and 2800&#176;F (1540&#176;C) respectively. The 1100 
			and 2100 series can operate with air preheat temperatures up to 600&#176;F (315&#176;C). The 3100 series can operate with air 
			preheat temperatures up to 900&#176;F (480&#176;C).
   			<br/><br/>The BBC is designed to operate with all types of industrial fuel oils, including heavy oils, and with any clean 
			industrial fuel gas with a gross calorific value of 500 Btu/scf (4700 Kcal/nm3) or greater.
   			<br/><br/>Capacities range from 2.5 million to more than 100 million Btu/hr (730 to 29,300 kW). The Beta burner's flame 
			shapes are well defined throughout the burner's operating range.
			<br/><br/>Fuel turndown is 8:1 with higher thermal turndown possible with excess air firing.
   			<br/><br/>Nominal burner air supply pressure is 8 osig (3450 Pa). Nominal gas supply pressure is 4 osig (1725 Pa).
   			<br/><br/>The burner can be supplied with a self supporting tile for soft wall applications. For hard refractory installations, 
			a self supporting tile or mandrel for field pouring of a burner port can be supplied. Hauck for mounting options.
   			<br/>All burners are ignited by a spark ignited gas pilot and can utilize UV flame supervision. Diverging tile designs 
			can be ignited from a hot furnace.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/bbc-1.pdf" target="_blank"><font color="#ACB0C3"><b>BBC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bbc.pdf" target="_blank"><font color="#ACB0C3"><b>BBC Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bbc-6.pdf" target="_blank"><font color="#ACB0C3"><b>BBC Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bbc-9.pdf" target="_blank"><font color="#ACB0C3"><b>BBC Instructions</b></font></a></p>
			</div></div>       
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/EJC_EnerJet_High_Velocity_Gas_Oil_Combination_Burners.jpg" alt="Hauck EJC EnerJet High Velocity Gas/Oil Combination Burners" title="Hauck EJC EnerJet High Velocity Gas/Oil Combination Burners"/></div>
			<div id="PartsContent"><h3>EJC EnerJet 
			<br/><font color="#50658D">High Velocity Gas/Oil Combination Burners</font></h3>
			<p><br/>The Hauck EJC burner promotes high velocity recirculation of heated gases for temperature uniformity and efficiency. It will burn any clean industrial fuel gas, No. 2 fuel oil, or a combination of gas and oil with the same performance characteristics on oil as on gas. Turndown, excess air and excess fuel are excellent on either fuel. The EJC is available in four sizes from 370,000 to 3,700,000 Btu/hr (108 to 1,080 kW). Also available in preheated air versions to 800&#176;F (425&#176;C), and in medium velocity versions.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Built-in flow metering of gas and air
			<br/>&#149; Excess air and fuel capability
			<br/>&#149; Manual or automatic control
			<br/>&#149; Operates on gas, oil or a combination of 
			<br/>&nbsp;&nbsp; fuels
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Simple design for low maintenance 
			<br/>&nbsp;&nbsp; operation
			<br/>&#149; Wide turndown
			<br/>&#149; High velocity recirculation of furnace 
			<br/>&nbsp;&nbsp; gases for maximum heat transfer 
			<br/>&nbsp;&nbsp; and temperature uniformity
			<br/><br/><b>Advantages</b>
			<br/><br/>Hauck's EnerJet (EJC) series high velocity combination burners have been specially designed for 
			application where high velocity recirculation of heated gases is desired for temperature uniformity and 
			efficiency. The EJC's fast, efficient high velocity recirculation reduces the need for installing expensive 
			high temperature recirculating fans or diluting flame temperatures with costly excess air. The EJC's high 
			velocity 'jet energy' entrains and recirculates gases for maximum convection heat transfer and temperature 
			uniformity - resulting in increased production at reduced fuel consumption and cost.
			<br/><br/><b>Efficient Operation.</b> The EJC is stable over its entire operating range and will burn any 
			clean industrial fuel gas, No. 2 fuel oil, or a combination of gas and oil.
			<br/><br/>A noteworthy feature of the DC is its ability when firing on oil to maintain a similar range 
			of performance characteristics experienced on gas firing in terms of turndown, excess air, and excess fuel operation.
			<br/><br/>The EJC can be controlled manually or automatically. A typical gas control system employs a 
			Hauck Ratio Regulator for each control zone to properly maintain the air-to-fuel ratio. For oil firing, 
			Hauck recommends an MRO regulator for each burner to properly maintain air-to-fuel ratio and simplify 
			burner setup.
			<br/><br/><b>Construction.</b> A simple design, with no moving parts, makes this burner virtually 
			maintenance free. Parts subjected to heat are either of heat resistant cast iron or stainless steel. 
			A special backplate/tile sealing arrangement completely eliminates overheating. The EJC's tile is 
			made of a low cement castable refractory and runs at lower temperatures than typical combination fired 
			pressurized tiles. Series 100 and 200 utilize a square tile design. Series 300 and 400 use a round tile.
			<br/><br/><b>Air-Fuel Metering.</b> Flow metering orifices in the EJC air and gas supply connections 
			give accurate readings of air and gas volumes entering the burner. This built-in flow metering provides 
			the means for 'fine- tuning' and properly setting flow rates to achieve optimum performance with desired 
			flame condition and at reduced cost. Optional oil flow meters are available to 'fine tune' the burner 
			when firing No. 2 fuel oil.
			<br/><br/><b>Piloting/Flame Supervision.</b>Provision is made for a spark ignited gas pilot. An ultraviolet 
			(UV) type flame detector system - with constant purge - is available for use with gas, oil, or combination fuels.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/ejc-1.pdf" target="_blank"><font color="#ACB0C3"><b>EJC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ejc.pdf" target="_blank"><font color="#ACB0C3"><b>EJC Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ejc-6.pdf" target="_blank"><font color="#ACB0C3"><b>EJC Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ejc-9.pdf" target="_blank"><font color="#ACB0C3"><b>EJC Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/ECO_Star_II_Packaged_Low_NOx_Multi_Fuel_Burner.jpg" alt="Hauck ECO-Star II Packaged Low NOx Multi Fuel Burner" title="Hauck ECO-Star II Packaged Low NOx Multi Fuel Burner"/></div>
			<div id="PartsContent"><h3>ECO-Star II 
			<br/><font color="#50658D">Packaged Low NOx Multi Fuel Burner</font></h3>
			<p><br/>The Eco-Star II continues Hauck's high quality commitment to the latest generation of combustion alternatives for 
			industry. Maintaining the same excellent burner stability as its EcoStar predecessors, the Eco-Star II offers design and 
			performance advantages while still being service accessible and simple to install. This burner supports a variety of liquid 
			and gaseous fuels offering flexibility for optimizing fuel costs. 
			<br/><br/>The EcoStar II is available with low pressure atomization for light fuel oil or LP, or high pressure compressed 
			air atomization for heavy fuel oils or installations located at high elevations. Emissions of NOx, CO and VOCs are minimized 
			with proven, state of the art technologies.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Compact, packaged design
    			<br/>&#149; Burner skid-mounted fuel manifolds
    			<br/>&#149; Multi-fuel capability: natural gas, No. 2 - 
			<br/>&nbsp;&nbsp; No. 6 fuel oils, liquid propane, landfill 
			<br/>&nbsp;&nbsp; gas
    			<br/>&#149; 1800 rpm impellers for low wear
    			<br/>&#149; Single pilot/main flame scanner
    			<br/>&#149; Total air design
    			<br/>&#149; Compressed air/oil firing optional
    			<br/>&#149; Low noise
    			<br/>&#149; Independent motors for fuel and air
    			<br/>&#149; Ultra low NOx design for < 35 ppm available
			<br/><br/><b>Benefits</b>
    			<br/><br/>&#149; Easy installation, setup and maintenance
    			<br/>&#149; Fast mixing/burning - compact flames
    			<br/>&#149; Improved recycled and heavy oil 
			<br/>&nbsp;&nbsp; atomization
    			<br/>&#149; Fuel efficient
    			<br/>&#149; Variable flame shaping
    			<br/>&#149; Low CO, NOx and VOC emissions
    			<br/>&#149; Valves can be characterized to maintain 
			<br/>&nbsp;&nbsp; air/fuel ratio over entire operating 
			<br/>&nbsp;&nbsp; range
			<br/><br/><b>Advantages</b>
			<br/><br/>&#149; Low Noise
    			<br/>&#149; Compact Design
    			<br/>&#149; Flame Adjustability
    			<br/>&#149; Improved Fuel Efficiency
    			<br/>&#149; Low Emissions
			<br/><br/>Available in sizes ranging from a nominal 75 to 200 million Btu/hr (19,800 to 52,900 kW), 
			the Eco-Star II is ready to meet your production needs. Capable of supporting a variety of gaseous 
			and liquid fuels, this burner offers the ultimate flexibility in dealing with today's highly volatile 
			fuel pricing. The standard design can be applied to applications up to 1500&#176;F (815&#176;C).
			<br/><br/>The Eco-Star II precisely controls air/fuel ratio over its entire operating range with a 100% 
			total air design. This proven technology reduces inefficient excess air, lowering fuel costs and improving 
			production. The burner's compact, skid-mounted package adapts easily to most installations.
			<br/><br/>The Eco-Star II is designed to be a uniquely reliable low emission burner.
			<br/><br/>The burner's wide range of flame shaping ability allows the flame to be adapted to specific 
			process requirements. All combustion is completed within the recommended combustion zone, further reducing 
			emissions by eliminating flame quenching from process materials.
			<br/><br/>The Eco-Star ll's sealed-in construction, coupled with its high efficiency aerodynamic design, 
			significantly reduces operational noise. As with all Hauck products, the Eco-Star II is backed by our 
			internationally recognized service department and our more than 50 years of experience as a leader and 
			partner to the asphalt industry.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/ESII-1.pdf" target="_blank"><font color="#ACB0C3"><b>ESII Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/esII.pdf" target="_blank"><font color="#ACB0C3"><b>ESII Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/es.pdf" target="_blank"><font color="#ACB0C3"><b>ES 25-50 Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/esII-6.pdf" target="_blank"><font color="#ACB0C3"><b>ESII Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ESII-9.1 B Model.pdf" target="_blank"><font color="#ACB0C3"><b>ES 25-50 Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/ESII-9.1 B Model.pdf" target="_blank"><font color="#ACB0C3"><b>ESII B Model Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/es-9.pdf" target="_blank"><font color="#ACB0C3"><b>ES 25-50 Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/NMC_Nozzle_Mix_Gas_Oil_Combination_Burners.jpg" alt="Hauck NMC Nozzle Mix Gas/Oil Combination Burner" title="Hauck NMC Nozzle Mix Gas/Oil Combination Burner"/></div>
			<div id="PartsContent"><h3>NMC Nozzle Mix 
			<br/><font color="#50658D">Gas/Oil Combination Burner</font></h3>
			<p><br/>The Hauck NM nozzle mix burners are designed for applications requiring a general purpose, long life, low maintenance burner. Fires most gaseous fuels, No. 2 oil or gas/oil combination. Preheated air models to 8000&#176;F (4250&#176;C).
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Burns most gaseous fuels and No. 2 oil
			<br/>&#149; Sealed-in capability
			<br/>&#149; Preheated air to 800&#176;F (425&#176;C)
			<br/>&#149; Available in 'swing-away' versions
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Stable flame over entire operating range
			<br/>&#149; Wide turndown
			<br/>&#149; Low maintenance
			<br/><br/><b>Advantages</b>
			<br/><br/><b>Operation.</b> Combustion air and fuel are channeled into the burner nozzle separately, permitting wide turn down. Since the burner nozzle is sealed into the refractory tile, all combustion air is supplied through the burner. The NM series burners couple a uniform flame front with flame stability over the entire operating range.
			<br/><br/>NM burners are available for preheated air operation up to 800&#176;F (425&#176;C) for high temperature furnace applications. Atomizing air is maintained at ambient conditions during preheated air operation.
			<br/><br/>NM burners can be controlled manually or automatically. Automatic control normally employs a Hauck Ratio Regulator for each control zone or burner to maintain air-fuel ratio. An alternate system uses control valves in each of the fuel and air lines, linking the valves to a single motor controller.
			<br/><br/>Use of the burner in recuperative systems results in an economical approach to energy savings.
			<br/><br/><b>Construction.</b> With no moving parts, the burner is virtually maintenance free. Parts subject to heat are constructed of either heat resistant cast iron or stainless steel. The tile is manufactured, dried and cured by Hauck. Special refractory materials and jacketed tiles can be supplied upon request.
			<br/><br/><b>Mounting.</b> The burner, mounting plate and refractory tile are shipped as an assembled unit. The burner can be mounted to fire in any position - horizontal, vertical up or vertical down. The air connection can be rotated to any of three other positions.
			<br/><br/>Each NM burner is equipped with companion flanges on the main air connections to permit easy burner installation and removal and allow the mounting of an orifice plate for reduced air flows, if required.
			<br/><br/><b>Pilot.</b> The burner mounting plate includes a port for direct spark or gas pilot ignition system. Direct spark ignition is available for gas operation only. The pilot is required for initial burner ignition only and is not required to maintain ignition.
			<br/><br/><b>Flame Supervision.</b> The NM mounting plate is provided with a port for monitoring the pilot and main flame, using a UV scanner or other suitable device.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/nmc-1.pdf" target="_blank"><font color="#ACB0C3"><b>NMC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmc.pdf" target="_blank"><font color="#ACB0C3"><b>NMC Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmc-6.pdf" target="_blank"><font color="#ACB0C3"><b>NMC Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/nmc-9.1.pdf" target="_blank"><font color="#ACB0C3"><b>NMC Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/NovaStar.jpg" alt="Hauck NovaStar Ultra Low NOx Burner for Aggregate Drying" title="Hauck NovaStar Ultra Low NOx Burner for Aggregate Drying"/></div>
			<div id="PartsContent"><h3>NovaStar
			<br/><font color="#50658D">Ultra Low NOx Burner for Aggregate Drying</font></h3>
			<p><br/>Hauck's ultra low NOx NovaStar burner continues the company's high quality commitment to state of the art advancements for theaggregate drying industry.Utilizing the latest lean burn premix technologies (patent pending), the NovaStar offers design and performance advantages with service accessibility and ease of installation.
			<br/><br/><b>Features</b> 	 
			<br/><br/>&#149; Ultra low NOx emissions of < 20 ppm 
			<br/>&nbsp;&nbsp; without FGR(corrected to 3% O2) 
			<br/>&nbsp;&nbsp; on natural gas and vaporized propane
			<br/>&#149; Easily adaptable to long-nose variations
			<br/>&#149; Easy to install, low maintenance 
			<br/>&nbsp;&nbsp; construction
			<br/>&#149; Precise air flow control via VFD and low 
			<br/>&nbsp;&nbsp; horsepower design offers significant 
			<br/>&nbsp;&nbsp; energy savings with no inlet or 
			<br/>&nbsp;&nbsp; outlet damper required
			<br/>&#149; Compact modular design suitable for 
			<br/>&nbsp;&nbsp; stationary or portable plants
			<br/>&#149; NS-1Edition 09-08Sealed-in construction 
			<br/>&nbsp;&nbsp; for quiet operation
			</p>
			</div></div>       
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/STARJET_Open_Fired_Multi_Fuel_Burner.jpg" alt="Hauck STARJET Open Fired Multi Fuel Burner" title="Hauck STARJET Open Fired Multi Fuel Burner"/></div>
			<div id="PartsContent"><h3>STARJET 
			<br/><font color="#50658D">Open Fired Multi Fuel Burner</font></h3>
			<p><br/>The StarJet burner is one of Hauck's most durable and efficient burners. The StarJet provides flame stability over its wide operating range eliminating the need for refractory ignition tiles or combustion chambers. The burner fires all commercial grades of fuel oil, natural gas, or LP gas. Liquid fuels require the use of a 36 osig blower. As a combination burner, the StarJet will burn any two fuels in combination-with the exception of liquid LP gas and fuel oil. With fuel efficiency and superior performance, the StarJet burner delivers.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Multi-fuel capability: natural gas, 
			<br/>&nbsp;&nbsp; No. 2-No. 6 oils, liquid propane gas, 
			<br/>&nbsp;&nbsp; butane gas
  			<br/>&#149; Flexible mounting capability
  			<br/>&#149; Superior oil atomization
  			<br/>&#149; Stainless steel ignition cone
  			<br/>&#149; Easily removable oil nozzle
  			<br/>&#149; Dual scanner tubes
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Easy installation, setup, and 
			<br/>&nbsp;&nbsp; maintenance
  			<br/>&#149; Compact, fast mixing flame
  			<br/>&#149; Improved waste/heavy oil atomization
  			<br/>&#149; Fuel efficient
  			<br/>&#149; Variable flame shaping
  			<br/>&#149; Adaptable to existing blower
			<br/><br/><b>Advantages</b>
			<br/><br/>&#149; Superior Oil Atomization
  			<br/>&#149; Fuel Efficient
  			<br/>&#149; Compact Flames
	   		<br/><br/>The StarJet fires all commercial grades of fuel oil, natural gas, or LP gas. The unique 'Swirl Generator' oil nozzle efficiently atomizes even the most difficult heavy oil and waste oil resulting in rapid combustion and a short bushy flame. The flame is aerodynamically stabilized, eliminating the need for an ignition tile.
	   		<br/><br/>Only the StarJet has two modes of flame shaping to meet the requirements of any process: spin vane adjustment and secondary air sleeve adjustment. Flame shaping is achieved by moving the adjustment lever at the side of the burner that is directly linked to the adjustable spin vanes. This feature greatly reduces setup time at initial start-up.
	   		<br/><br/>For heavy fuel applications, 36 ounce blowers are required to deliver superior fuel atomization and combustion intensity. Atomizing air is supplied by the burner blower-there is no expensive compressed air requirement.
	   		<br/><br/>Dual scanner tubes permit monitoring pilot and main flames individually. The scanner tubes are connected to an air purge, which minimizes dirt infiltration and provides cooling air.
	   		<br/><br/>As with all Hauck products, the StarJet is backed by our nationally recognized service department.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/sj-asphalt--1.pdf" target="_blank"><font color="#ACB0C3"><b>StarJet Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/sj.pdf" target="_blank"><font color="#ACB0C3"><b>StarJet Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/sj-6.pdf" target="_blank"><font color="#ACB0C3"><b>StarJet Part List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/sj-9.pdf" target="_blank"><font color="#ACB0C3"><b>StarJet Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/sj-9.2.pdf" target="_blank"><font color="#ACB0C3"><b>StarJet Heavy Oil Insert Heater</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/lnsj-9.pdf" target="_blank"><font color="#ACB0C3"><b>Long Nose StarJet Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/SVC.jpg" alt="Hauck SVC Super Versatile Combination High Velocity Square Refractory Tile" title="Hauck SVC Super Versatile Combination High Velocity Square Refractory Tile"/></div>
			<div id="PartsContent"><h3>SVC 
			<br/><font color="#50658D">Super Versatile 
			<br/>Combination High Velocity 
			<br/>Square Refractory Tile</font></h3>
			<p><br/>The SVC Super Versatile Combination burner is designed for applications that require dual fuel flexibility without sacrificing the advantages of combustion gas recirculation, increased efficiency and improved temperature uniformity resulting from high exhaust gas exit velocity. Designed to fire on light fuel oil through No. 2 or any clean industrial fuel gas, the SVC is available in sizes ranging from 400,000 to more than 2,200,000 Btu/hr (117 to 645 kW).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/svc-1.pdf" target="_blank"><font color="#ACB0C3"><b>SVC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/svc.pdf" target="_blank"><font color="#ACB0C3"><b>SVC Specifications</b></font></a></p>
			</p>
			</div></div>       
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/WRC_WRO_Wide_Range_Burners.jpg" alt="Hauck WRC/WRO Wide Range Burners" title="Hauck WRC/WRO Wide Range Burners"/></div>
			<div id="PartsContent"><h3>WRC/WRO 
			<br/><font color="#50658D">Wide Range Burners</font></h3>
			<p><br/>Wide Range Burner Units designed for oil or gas/oil firing and both pressurized and nonpressurized applications. The output capacity is twice that of a 780 series burner and turndown range is greatly increased. These advantages are accomplished by supplying controlled combustion air through the burner and around the burner nose. Air flows are separately valved but jointly controlled. Where there is much excess air, such as in air heater, the fuel capacities can be increased by as much as 2.5 times sealed-in stoichiometric capacity.
			<br/><br/><b>Advantages</b>
			<br/><br/>Hauck's Wide Range Burner permits precise adjustment at all firing rates throughout its range. Combustion ratios can be precisely set and maintained at any constant value from high to low fire. When burning oil, atomization is excellent even with the wide turndown available. The atomizing air is delivered at full blower pressure from high to low and throttled at the point of atomization. The oil is introduced into the atomizing stream with zero forward velocity at the point of maximum air velocity, assuring complete atomization.
			<br/><br/>The burner is designed for single lever control, driven either manually or by means of a control motor. All operating levers are linked together: movement of one control lever adjusts all the valves.
			<br/><br/>The Wide Range Burner is provided with a spark ignited gas pilot, two observation ports, drain plug, a burner-mounted low fire micro switch. Provision has been made to permit monitoring either the main flame or both the pilot and main flames using UV scanners.
			<br/><br/>The basic burner unit consists of an oil or combination gas/oil Hauck 780 series burner and an adjustable flow valve mounted on a circular, metal air plenum. (On the smaller burner units, the plenum air control valve is mounted on the chamber itself.) Series 100 units include a separate ignition tile. The ignition chamber is constructed by the customer. Series 500 units include an Ignition Chamber Unit (ICU).The last six inches of the ICU are constructed of stainless steel.
			<br/><br/>The burner and plenum valves are controlled by the movement of one lever, which can be operated manually or automatically, providing accurate proportioning of fuel and air. For automatic operation, the lever is connected to a control motor which is usually activated by a temperature control instrument.
			<br/><br/>Use of a 20 osig blower is recommended for light oils or a combination of gas and light oil. Use of a 24 osig blower is recommended for heavy fuel oils or a combination of gas and heavy oil. Heavy oil firing is not recommended below 24 osig air pressure; light oil firing is not recommended below 16 osig air pressure. The type of oil and ambient conditions can affect burner and blower selections. The burner requires constant pressure and air piping should be properly sized to avoid excessive pressure drops in the system.
			<br/><br/>The burner must be supplied with a pressure regulated oil supply. The oil supply line pressure can range up to 50 psi at the inlet of the regulator.
			<br/><br/>The oil set-up assembly consists of a PRO oil regulator, edge plate filter, and shutoff valve. All components are prepiped into a single unit for easy installation and each unit is pre-sized to handle the burner requirements. The PRO functions as a self-contained pressure reducing valve, eliminating the need for external actuators or control lines.
			<br/><br/>A pressure regulated gas supply line must be provided to the burner. The required pressure normally ranges from 8" wc to 2 psi. The actual pressure required is determined by the Btu value of the gas, the burner size, the application, and the capacity required. The total pressure necessary is the sum of the pressure drops across the gas flow valve, the gas nozzle and various fittings, and a pressure head allowance to overcome any combustion chamber back-pressure. For specific requirements, Hauck representative or the factory.
			<br/><br/>The Wide Range Burner utilizes Hauck's IPG blast type gas pilot designed to provide flame stability and long life even under adverse conditions. The pilot assembly consists of a flame retaining nozzle with stainless steel tip, an air-gas mixer, an air cock, a gas pressure regulator, spark plug assembly, and flexible pipe nipple. The pilot requires a constant air pressure in the 8 to 32 ounce range. The pilot will burn any clean, commercial gas. Gas pressure, at the pilot regulator inlet, must be between 1/2 and 1 psi.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/wrc-1.pdf" target="_blank"><font color="#ACB0C3"><b>WRC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/wrc.pdf" target="_blank"><font color="#ACB0C3"><b>WRC Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/wrc-6.pdf" target="_blank"><font color="#ACB0C3"><b>WRC Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/wro-6.pdf" target="_blank"><font color="#ACB0C3"><b>WRO Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/wrc-9.pdf" target="_blank"><font color="#ACB0C3"><b>WRC Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/780_PAC_L_Series_Self_Proportioning_Combination_Gas_Oil_Burners.jpg" alt="Hauck 780 PAC-L Series Self-Proportioning Combination Gas Oil Burners" title="Hauck 780 PAC-L Series Self-Proportioning Combination Gas Oil Burners"/></div>
			<div id="PartsContent"><h3>780 PAC-L Series  
			<br/><font color="#50658D">Self-Proportioning 
			<br/>Combination Gas Oil Burners</font></h3>
			<p><br/>The Hauck 780 PAC-L Series Burners are self-proportioning, Combination gas-oil fired burners designed for highly efficient operation using low pressure atomizing air. The 780 PAC series can be fired on No. 2 through No. 6 fuel oil or any clean commercial fuel gas with sealed-in capacities from 200,000 Btu/hr to 14,000,000 Btu/hr. Higher capacities available with induced air.
			<br/><br/>&#149; Accurately proportions fuel and air
    			<br/>&#149; Single lever control
    			<br/>&#149; Switch or mix fuels instantly
    			<br/>&#149; Burns heavy oil, light oil and gas
    			<br/>&#149; Low pressure air
    			<br/>&#149; Highly reliable
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/780pac-1.pdf" target="_blank"><font color="#ACB0C3"><b>780PAC Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/780PAC.pdf" target="_blank"><font color="#ACB0C3"><b>780PAC Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/780-6.pdf" target="_blank"><font color="#ACB0C3"><b>780PAC Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/780PAC-9.pdf" target="_blank"><font color="#ACB0C3"><b>780P Instructions</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/780PAC-9.1.pdf" target="_blank"><font color="#ACB0C3"><b>780PAC Gas Nozzle Installation</b></font></a></p>
			</div></div>       
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/FVA_FVS.jpg" alt="Hauck FVA-FVS Adjustable Flow Valves for Clean Gas and Air" title="Hauck FVA-FVS Adjustable Flow Valves for Clean Gas and Air"/></div>
			<div id="PartsContent"><h3>FVA-FVS 
			<br/><font color="#50658D">Adjustable Flow Valves
			<br/>for Clean Gas and Air</font></h3>
			<p><br/>Operating conditions encountered in the automatic controlling of gas and air flows often require valves with an adjustable flow curve which can be altered at various points of opening to obtain the desired amount of flow. With Hauck adjustable flow valves, the flow rate can be easily changed at any of the 10 or more adjusting points provided. Therefore, the valve flow curve can be characterized to suit different constants in pressure, flow suction, or discharge resistance occuring at the minimum, maximum or in-between capacities.
			<br/><br/><b>Details</b>
			<br/><br/>Operating conditions encountered in the automatic controlling of gas and air flows often require valves with an adjustable flow curve which can be altered at various points of opening to obtain the desired amount of flow. Such flexibility is not obtainable with ordinary adjustable port valves having a fixed height of port opening over the entire range of operation.
			<br/><br/>With Hauck adjustable Flow Valves, the flow rate can be easily changed at any of the 10 or more adjusting points provided. Therefore, the valve flow curve can be characterized to suit different constants in pressure, flow suction, or discharge resistance occuring at the minimum, maximum or in-between capacities. Infinite flexibility of flow characteristics is thus available. These valves are designed for use as control valves and they should not be used as shutoff valves.
			<br/><br/>The adjustable Flow Valve can easily be linked to a Hauck Micro Oil Valve and Hauck adjustable Port Valve for manual or automatic control of gas, oil, and air flow to burners.
			<br/><br/>For the 1" thru 4" valves, the valve body and piston are made of cast iron. For the 6" valve, the body is fabricated (welded) steel and the piston is fabricated aluminum. The valve spring and cam spring are of steel and other internal operating parts are of brass. All valves have four pads on each side which are drilled and tapped to facilitate mounting.
			<br/><br/><b>Operation.</b> The valve consists of a cylindrical piston with a rectangular port. The rectangular port both rotates and reciprocates within the valve body, which also has a rectangular opening. When the valve lever is moved from low (1) to high (10) position, the rectangular opening in the body is uncovered for flow. The height of the rectangular port is adjustable. The position of the cam spring affects the position of the rocker arm, which in turn controls the height of the port. The cam spring can be adjusted at 10 or more independent points (the number of points is dependent on the valve size) by rotating the adjusting screws located under the valve dial.
			<br/><br/>If more or less flow is desired at any position, the adjusting screw under the pointer is turned in to increase, or out to decrease, the height of the port opening and thus change the overall port area at that point. Great flexibility of flow characteristics is provided by the adjusting screws which can be set for either uniform or varying increments of change in capacity at the 10 valve dial positions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/afv-1.pdf" target="_blank"><font color="#ACB0C3"><b>AFV Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/afv.pdf" target="_blank"><font color="#ACB0C3"><b>AFV Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/afv-6.pdf" target="_blank"><font color="#ACB0C3"><b>AFV Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/afv-9.pdf" target="_blank"><font color="#ACB0C3"><b>AFV Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/PVS_Adjustable_Port_Valves.jpg" alt="Hauck PVS Adjustiable Port Valves" title="Hauck PVS Adjustiable Port Valves"/></div>
			<div id="PartsContent"><h3>PVS 
			<br/><font color="#50658D">Adjustiable Port Valves</font></h3>
			<p><br/>Hauck valves are a dependable and accurate means of controlling the flow or air or gas to burners on furnaces, ovens, kilns, dryers and for other industrial control applications. Hauck rectangular port valves feature an easy, single adjustment and are designed to hold the desired setting in either automatic or manual operation systems.
			<br/><br/>&#149; Single adjustment-positive locking
  			<br/>&#149; Adjustable limit stops
  			<br/>&#149; May be installed in any position
  			<br/>&#149; Mounting pads for control motor/other 
			<br/>&nbsp;&nbsp; valves
  			<br/>&#149; Low torque
			<br/><br/>Valve flow rate is controlled by operating the lever to rotate the cylindrical piston for desired amount of port opening. When necessary for conditions of application, the height of the rectangular port opening can be adjusted for the entire range of lever travel, by merely turning a single knurled knob. This knob has "close" and "open" direction arrows, and a socket screw for positive locking of position.
			<br/><br/>Full control range of the valve is obtained by 90 degrees rotation of lever. The adjustable radius and positioning features of the lever permit easy linkage to a control motor or drive mechanism. A ball joint snap connection on the lever eliminates friction from misalignment and allows the valve to be instantly disconnected from a control system, for manual operation or readjustment. Adjustable limit stop screws for the valve pointer are vibration-proof.
			<br/><br/>A separate locking screw, friction type and vibration-proof, is provided to lock any manual setting of valve pointer. Valve piston shaft has a ball thrust bearing and take-up screw which eliminates cylinder end play and related flow variations.
			<br/><br/>Designed for efficient controlling, these units are not intended to be used as a shut-off valve.
			<br/><br/>Hauck Adjustable Port Valves have mounting pads drilled and tapped on both sides. They can be provided with connecting linkage and brackets for automatic control, and in multiple valve units. Units combining other Hauck valves are also available.
			<br/><br/>Entire valve internals with valve cover assembly are removable from valve body by unscrewing the cover plate screws.
			<br/><br/>All valve bodies through 4 inch size are cast iron. Larger sizes are of steel. Valve pistons are of machined aluminum alloy in all sizes. Valve covers are aluminum alloy in sizes 1 inch through 4 inch, and steel in the 6, 8, and 10 inch sizes.
			<br/><br/>Valve sizes 1 inch through 2 inch have one- piece bodies with screwed connections. Valves sizes 3 inch and 4 inch have flanged type screw connections. The 6, 8, and 10 inch valves are flanged for 125 lb. American Standard Flange size.
			<br/><br/>Valve sizes 1 inch through 4 inch are designed for operating pressures up to 15 psig. The 6, 8, and 10 inch valves are rated for pressures up to 5 psig. PVS valves may be used with temperatures up to 300&#176; F.
			<br/><br/>Manual or automatic control is by moving the Adjustable Radius Control Lever. The piston is thereby rotated to proportionally uncover the rectangular port in the valve body. The height of the port is adjusted by a single adjusting knob. When the height adjustment is made to satisfy maximum flow conditions, this establishes the port height for all rotational valve positions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/apv-1.pdf" target="_blank"><font color="#ACB0C3"><b>APV Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/apv.pdf" target="_blank"><font color="#ACB0C3"><b>APV Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/apv-6.pdf" target="_blank"><font color="#ACB0C3"><b>APV Part List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/apv-9.pdf" target="_blank"><font color="#ACB0C3"><b>APV Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/BVA_Butterfly_Valves.jpg" alt="Hauck BVA Butterfly Valves" title="Hauck BVA Butterfly Valves"/></div>
			<div id="PartsContent"><h3>BVA 
			<br/><font color="#50658D">Butterfly Valves</font></h3>
			<p><br/>Hauck butterfly valves control the flow of low pressure combustion air or fuel gas to burner systems. 
			Two versions of the valve are available; the BVA series for ambient air service and most fuel gases; and the BVA-H series 
			for preheated air applications up to 800&#176;F (425&#176;C). Each valve series is available in manual or automatic 
			control mode of operation. 
			<br/><br/>BVA valves are ideal for individual burner or zone control and can be used as a trim valve to balance 
			individual burners.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Automatic or manual control
    			<br/>&#149; Wide flow range
    			<br/>&#149; Available in 11 sizes for air, hot air, 
			<br/>&nbsp;&nbsp; or fuel gas
    			<br/>&#149; Easy to read valve position dial plate
			<br/><br/><b>Benefits</b>
    			<br/><br/>&#149; Cast iron body for long life
    			<br/>&#149; Accurate and repeatable for quick flow 
			<br/>&nbsp;&nbsp; control
    			<br/>&#149; Easy to read valve position dial plate
    			<br/>&#149; Ideal for individual burner or zone control
			<br/><br/><b>Advantages</b>
    			<br/><br/>Hauck Butterfly Valves utilize maximum port openings for high flow rates. An easily read dial plate helps 
			the operator maintain settings from burner to burner. Settings can be duplicated allowing quick return to previous flow rates.
			<br/><br/>The BVA is available for automatic or manual control. The manual valve, 100 series, is equipped with a 
			short control lever and a thumb screw to easily set and lock valve position. The automatic series valves come with 
			tapped and threaded bolt locations for the addition of a control motor.
			<br/><br/>BVA valves are NPT threaded in the 1" to 6" sizes. In addition, there are 6", 8", and 10" flanged valves 
			with RPM flanges. The 1000 series is a manual valve, while the 2000 series is an automatic valve.
			<br/><br/>BVA 100 and 400 series are constructed of a cast iron body with a steel valve disc, brass shaft and viton shaft seals.
			<br/><br/>BVA-H 100 and 400 series are constructed of a cast iron body, stainless steel valve disc and stainless 
			steel shaft. A zinc plated steel retaining nut and packing ensure a high temperature shaft seal.
			<br/><br/>BVA 1000 and 2000 flanged series are constructed of a welded steel body, steel valve disc and brass shaft.
			<br/><br/>Hauck Manufacturing can provide a bracket and linkage for the BVA to be used with most any control motor 
			or actuator.
			<br/><br/>Wafer butterfly valves from 6" to 22" are available.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/bva-1.pdf" target="_blank"><font color="#ACB0C3"><b>BVA Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bva.pdf" target="_blank"><font color="#ACB0C3"><b>BVA Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bva-6.pdf" target="_blank"><font color="#ACB0C3"><b>BVA Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/bva-9.pdf" target="_blank"><font color="#ACB0C3"><b>BVA Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/LVG_Gas_Limiting_Orifice_Valve.jpg" alt="Hauck LVG Gas Limiting Orifice Valve" title="Hauck LVG Gas Limiting Orifice Valve"/></div>
			<div id="PartsContent"><h3>LVG 
			<br/><font color="#50658D">Gas Limiting Orifice Valve</font></h3>
			<p><br/>The Hauck LVG Series Limiting Orifice Gas Valves are designed to allow precise setting of high fire gas flow. They are used for single or multiple burner systems. The standard LVG series valves can be used as straight through type valves, and angle type valves are available to meet burner piping system requirements. These valves are designed for use with any commercial fuel gas; coke oven gas versions are also available.
			<br/><br/>There are nine LVG valve sizes from 1/2 NPT to 4" ANSI flange. Valves with metric connections are also available. Capacities for natural gas range from 760 to 24,640 scfh (20 to 660 nm&#179;/hr) at a pressure drop of 6.9" wc (1,720 Pa).
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Precision machined valve flow piston
			<br/>&#149; Rugged cast iron body
			<br/>&#149; Replaceable cartridge assembly for LVG 
			<br/>&nbsp;&nbsp; 505-520
			<br/>&#149; Straight through valve design; right angle 
			<br/>&nbsp;&nbsp; valve option
			<br/>&#149; Pressure rated to 25 psig (172 kPa)
			<br/><br/><b>Benefits</b>
			<br/><br/>&#149; Fine flow adjustment
			<br/>&#149; Maintenance free operation
			<br/>&#149; Vibration proof flow setting
			<br/><br/><b>Advantages</b>
			<br/><br/>&#149; Precise Flow Adjustment
			<br/><br/>LVG series valves are designed for use as straight through type valves to set high fire on single or multiple burner systems. Angle type valves are available to meet burner piping system requirements. Valves are also available with metric connections or for coke oven gas.
			<br/><br/>Installation should be made as close to the burner as possible to minimize burner response time to changes in gas flow, and downstream of a shutoff valve. Though the valves are designed to provide minimal leakage flow when fully closed, they can not be used as shutoff valves.
			<br/><br/>Precise adjustment is accomplished by rotating the adjusting screw to increase or decrease the gas flow through the valve. After the valve is properly adjusted, the hex screw cap should be replaced to protect the setting. The seal design of the cartridge stem permits operating pressures up to 25 psig (172 kPa).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/lvg-1.pdf" target="_blank"><font color="#ACB0C3"><b>LVG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/lvg.pdf" target="_blank"><font color="#ACB0C3"><b>LVG Specifiations</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/lvg-6.pdf"><font color="#ACB0C3"><b>LVG Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/lvg-9.pdf" target="_blank"><font color="#ACB0C3"><b>LVG Instructions</b></font></a></p>
			</p>
			</div></div>      
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/Valvario_Modular_Solenoid_Valves.jpg" alt="Hauck Valvario Modular Solenoid Valves" title="Hauck Valvario Modular Solenoid Valves"/></div>
			<div id="PartsContent"><h3>Valvario
			<br/><font color="#50658D">Modular Solenoid Valves</font></h3>
			<p><br/>Solenoid valves for gas VAS and double solenoid valves VCS for safeguarding and controlling the air and gas supply to gas burners and gas appliances. For use in gas control and safety systems in all sectors of the iron, steel, glass, and ceramics industries, also in commercial heat generation, such as the packaging, paper and food industries.
			</p>
			</div></div>       
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/WBV_H_Wafer_Butterfly_Valves_Hot_Air.jpg" alt="Hauck WBV-H Wafer Butterfly Valves Hot Air" title="Hauck WBV-H Wafer Butterfly Valves Hot Air"/></div>
			<div id="PartsContent"><h3>WBV-H 
			<br/><font color="#50658D">Wafer Butterfly Valves Hot Air</font></h3>
			<p><br/>Hauck Wafer Butterfly Valves are used to trim or control ambient or preheated air. The valve can be ordered with a hand lever or can be supplied with a lever and linkage for automatic control. When sizing wafer butterfly valves for automatic control, normal practice is to size for a valve differential pressure (&#916;p) of 10-20&#37; of total available air pressure. When ordering, specify temperature service desired: ambient air to 400&#176;F, or preheated air to 1200&#176;F.
			<br/><br/>Valves should be selected for maximum flow at no more than 70&#176; open. Full ported wafer valves installed in lines of the same size offer increased flow for a fixed pressure drop up to 70&#176; open. At greater openings, flow does not increase appreciably although pressure drop across the valve falls off rapidly. At 90&#176; open, resistance is very low -pressure drop is practically zero.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/wbvh-1.pdf" target="_blank"><font color="#ACB0C3"><b>WBV-H Fact Sheet</b></font></a>
			</p>
			</div></div>       
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/RRG_Ratio_Regulator_Gas.jpg" alt="Hauck RRG Ratio Regulator Gas" title="Hauck RRG Ratio Regulator Gas"/></div>
			<div id="PartsContent"><h3>RRG 
			<br/><font color="#50658D">Ratio Regulator Gas</font></h3>
			<p><br/>Hauck RRG regulators are carefully factory balanced to maintain a precise gas-air ratio for nozzle 
			mix burners which use low pressure air and gas. When cross-connected to a control air line (see typical piping schematic),
			these regulators automatically proportion the gas delivery pressure in response to varying air pressure in the air
			control zone, enabling single valve firing control.
			<br/><br/>The proper operation of a cross-connected gas regulator requires that the gas supply pressure be equal to 
			the sum of the maximum control air line pressure plus the gas pressure drop through the regulator at high fire. 
			If the gas pressure is high enough, an air bleeder valve is not required. If the gas pressure is too low or the 
			air pressure is too high, an air bleeder valve is required in the air control line.
			<br/><br/>A gas tight cock or valve should be installed upstream of the RRG regulator in the gas line.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Sensitive & Accurate
    			<br/>&#149; Precise Gas Flow Control
    			<br/>&#149; Temperature Range up to 150&#176;F
    			<br/>&#149; Factory Balanced
    			<br/>&#149; Single Valve Control
    			<br/>&#149; Dependable, Consistent Performance
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.hauckburner.com/pdf/rrg-1.pdf" target="_blank"><font color="#ACB0C3"><b>RRG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/rrg-9.pdf" target="_blank"><font color="#ACB0C3"><b>RRG Instructions</b></font></a></p>
			</p>
			</div></div>       
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/OMG_Ofifice_Meters.jpg" alt="Hauck OMG Orifice Meters" title="Hauck OMG Orifice Meters"/></div>
			<div id="PartsContent"><h3>OMG 
			<br/><font color="#50658D">Orifice Meters</font></h3>
			<p><br/>Hauck Orifice Meters are a simple, economical method of measuring air and gas flow on industrial combustion applications. OMG assemblies are available in threaded or weld-in designs. The threaded 100 series is suitable for gas or air service in 3/8" to 6" NPT. The weld-in 200 series is suitable for gas or air service in 3/4" to 6" pipe. Additionally, the 2000 series weld-in design is suitable for air service in 8" to 28" sizes. A wide range of standard orifice plates enables flow measurement of natural gas from a nominal 30 scfh to over 52000 scfh, and of air from a nominal 24 scfh to over 850,000 scfh. Customized orifice plates are available upon request.
			<br/><br/>&#149; Threaded or welded design
			<br/>&#149; Heavy duty construction
			<br/>&#149; Simple one bolt plate removal
			<br/>&#149; Economical flow metering
			<br/>&#149; Easy to use
			<br/>&#149; Highly accurate results
			<br/><br/>The Hauck OMG series allows simple measurement of air and gas flow in industrial combustion systems. The OMG is designed for use in the most demanding and harsh installations. A wide selection of connection options and an extensive variety of orifice bore sizes make the OMG ideal for individual burner metering and total system metering. For example, on large multi-burner installations, an OMG can be installed at each burner's air and gas connection and in the main air header or gas manifold for the furnace.
			<br/><br/>The heavy-duty construction of the OMG series allows pressure up to 25 psig for the 3/8" to 4" size. The 6" through 28" sizes are suitable for 10 psig service. All OMG components are suitable for temperatures up to 250&#176;F. Higher temperature materials are available upon request. factory for higher pressure applications.
			<br/><br/>OMG assemblies not only provide accurate flow measurement, but also offer the convenience of serving as an auxiliary union connection in the piping. An OMG may be installed in any position provided the location chosen ensures easy access to both pressure cocks and sufficient clearance exists to remove the orifice plate. For maximum accuracy, use at least 6 pipe diameters upstream and 4 pipe diameters downstream of the OMG.
			<br/><br/>To assist in proper orifice meter and plate selection. The Hauck Orifice Calculator will calculate the flow of a given gas (Natural Gas, Propane, Butane, Coke Oven Gas or Air) under varying conditions. To properly select an orifice plate, the following information is needed: gas temperature, upstream pressure, gas type, and pipe size. The Orifice Calculator determines gas flow at a given differential pressure across the orifice plate.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://hauckburner.thomasnet.com/Asset/omg-1.pdf" target="_blank"><font color="#ACB0C3"><b>OMG Fact Sheet</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/omg.pdf" target="_blank"><font color="#ACB0C3"><b>OMG Specifications</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/omg-6.pdf" target="_blank"><font color="#ACB0C3"><b>OMG Parts List</b></font></a>
			<br/><a href="http://www.hauckburner.com/pdf/omg-9.pdf" target="_blank"><font color="#ACB0C3"><b>OMG Instructions</b></font></a></p>
			</p>
			</div></div>      
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/IC_Ignition_Wires_Ignitor_and_Transformer_Terminal.jpg" alt="Hauck IC Ignition Wires/Ignitor and Transformer Terminal" title="Hauck IC Ignition Wires/Ignitor and Transformer Terminal"/></div>
			<div id="PartsContent"><h3>IC 
			<br/><font color="#50658D">Ignition Wires/Ignitor and Transformer Terminal</font></h3>
			<p><br/>The Q624A is a half-wave solid state ignition transformer that is suitable for the spark ignition of most industrial burners and pilots and prevents detection of the ignition spark by the UV flame detectors (C7027A-1049-UV or C7035A-1023-UV). The E05-SA6 is a standard coil ignition transformer that is also suitable for most spark ignition applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.hauckburner.com/pdf/ic.pdf" target="_blank"><font color="#ACB0C3"><b>IC Specifications</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Hauck/AIG_High_Pressure_Gas_Air_Inspirators.jpg" alt="Hauck AIG High Pressure Gas-Air Inspirators" title="Hauck AIG High Pressure Gas-Air Inspirators"/></div>
			<div id="PartsContent"><h3>AIG
			<br/><font color="#50658D">High Pressure Gas-Air Inspirators</font></h3>
			<p><br/>Hauck High Pressure AIG Inspirators Make it Possible to utilize the energy of the gas to inspirate the necessary air for combustion. The AIG inspirators may be used wherever high pressure gas form 1 lb. to 30 lb. is available. The AIG automatically maintains the gas-air ratio. No blower or combustion air. A single control valve is used for manual or automatic operation.
			<br/><br/>&#149; Maximum Air Entrainment
   			<br/>&#149; Requires No Blower or Compressor
   			<br/>&#149; Automatically Maintains Gas-air Ratio
   			<br/>&#149; Single Valve Control for Manual or 
			<br/>&nbsp;&nbsp; Automatic Operation
   			<br/>&#149; Easy to Adjust
   			<br/>&#149; Efficient - Simple - Rugged
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://hauckburner.thomasnet.com/Asset/aig-1.pdf" target="_blank"><font color="#ACB0C3"><b>AIG Fact Sheet</b></font></a>
			</p>
			</div></div>  
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/AIG_High_Pressure_Gas_Air_Inspirators_Thumbnail.gif" border="0" alt="Hauck AIG High Pressure Gas-Air Inspirators" title="Hauck AIG High Pressure Gas-Air Inspirators" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/BBG_Gas_Beta_Burner_Thumbnail.gif" border="0" alt="Hauck BBG Gas Beta Burner" title="Hauck BBG Gas Beta Burner" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/BIC_ZIC_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck BIC ZIC Gas Burner" title="Hauck BIC ZIC Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/Ecomax_Direct_Fired_Self_Recuperative_Metallic_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck Ecomax Direct-Fired Self-Recuperative Metallic Gas Burner" title="Hauck Ecomax Direct-Fired Self-Recuperative Metallic Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/ISER_Single_Ended_Recuperative_Burner_for_Immersion_Firing_Thumbnail.gif" border="0" alt="Hauck ISER Single Ended Recuperative Burner for Immersion Firing" title="Hauck ISER Single Ended Recuperative Burner for Immersion Firing" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/JAG_Jet_Air_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck JAG Jet Air Gas Burner" title="Hauck JAG Jet Air Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/NMG_Nozzle_Mix_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck NMG Nozzle Mix Gas Burner" title="Hauck NMG Nozzle Mix Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/PBG_Packaged_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck PBG Packaged Gas Burner" title="Hauck PBG Packaged Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/Radimax_Radiant_Tube_Plug_In_Recuperator_Thumbnail.gif" border="0" alt="Hauck Radimax Radiant Tube Plug-In Recuperator" title="Hauck Radimax Radiant Tube Plug-In Recuperator" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/Retain_A_Flame_Gas_Burner_Nozzles_Thumbnail.gif" border="0" alt="Hauck Retain-A-Flame Gas Burner Nozzles" title="Hauck Retain-A-Flame Gas Burner Nozzles" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/RFG_RadiFlame_Radiant_Tube_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck RFG - RadiFlame Radiant Tube Gas Burner" title="Hauck RFG - RadiFlame Radiant Tube Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/RKG_Radiant_Cone_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck RKG Radiant Cone Gas Burner" title="Hauck RKG Radiant Cone Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/SVG_Super_Versatile_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck SVG Super Versatile Gas Burner" title="Hauck SVG Super Versatile Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/WHG_Hugger_Gas_Burner_Thumbnail.gif" border="0" alt="Hauck WHG Hugger Gas Burner" title="Hauck WHG Hugger Gas Burner" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/WHG_Package_Wall_Hugger_Gas_Fired_Burner_Thumbnail.gif" border="0" alt="Hauck WHG Package Wall Hugger Gas Fired Burner" title="Hauck WHG Package Wall Hugger Gas Fired Burner" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/WHI_Wall_Hugger_Low_NOx_Burner_Thumbnail.gif" border="0" alt="Hauck WHI Wall Hugger Low NOx Burner" title="Hauck WHI Wall Hugger Low NOx Burner" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/BBC_Gas_Oil_Combination_Beta_Burner_Thumbnail.gif" border="0" alt="Hauck BBC Gas/Oil Combination Beta Burner" title="Hauck BBC Gas/Oil Combination Beta Burner"/></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/EJC_EnerJet_High_Velocity_Gas_Oil_Combination_Burners_Thumbnail.gif" border="0" alt="Hauck EJC EnerJet High Velocity Gas/Oil Combination Burners" title="Hauck EJC EnerJet High Velocity Gas/Oil Combination Burners" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/ECO_Star_II_Packaged_Low_NOx_Multi_Fuel_Burner_Thumbnail.gif" border="0" alt="Hauck ECO-Star II Packaged Low NOx Multi Fuel Burner" title="Hauck ECO-Star II Packaged Low NOx Multi Fuel Burner" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/NMC_Nozzle_Mix_Gas_Oil_Combination_Burners_Thumbnail.gif" border="0" alt="Hauck NMC Nozzle Mix Gas/Oil Combination Burner" title="Hauck NMC Nozzle Mix Gas/Oil Combination Burner" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/NovaStar_Thumbnail.gif" border="0" alt="Hauck NovaStar Ultra Low NOx Burner for Aggregate Drying" title="Hauck NovaStar Ultra Low NOx Burner for Aggregate Drying" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/STARJET_Open_Fired_Multi_Fuel_Burner_Thumbnail.gif" border="0" alt="Hauck STARJET Open Fired Multi Fuel Burner" title="Hauck STARJET Open Fired Multi Fuel Burner" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/SVC_Super_Versatile_Combination_Burner_High_Velocity_Square_Refractory_Tile_Thumbnail.gif" border="0" alt="Hauck SVC Super Versatile Combination High Velocity Square Refractory Tile" title="Hauck SVC Super Versatile Combination High Velocity Square Refractory Tile" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/WRC_WRO_Wide_Range_Burners_Thumbnail.gif" border="0" alt="Hauck WRC/WRO Wide Range Burners" title="Hauck WRC/WRO Wide Range Burners" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/780_PAC_L_Series_Self_Proportioning_Combination_Gas_Oil_Burners_Thumbnail.gif" border="0" alt="Hauck 780 PAC-L Series Self-Proportioning Combination Gas Oil Burners" title="Hauck 780 PAC-L Series Self-Proportioning Combination Gas Oil Burners" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/FVA_FVS_Adjustable_Flow_Valves_for_Clean_Gas_and_Air_Thumbnail.gif" border="0" alt="Hauck FVA-FVS Adjustable Flow Valves for Clean Gas and Air" title="Hauck FVA-FVS Adjustable Flow Valves for Clean Gas and Air" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/PVS_Adjustable_Port_Valves_Thumbnail.gif" border="0" alt="Hauck PVS Adjustiable Port Valves" title="Hauck PVS Adjustiable Port Valves" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/BVA_Butterfly_Valves_Thumbnail.gif" border="0" alt="Hauck BVA Butterfly Valves" title="Hauck BVA Butterfly Valves" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/LVG_Gas_Limiting_Orifice_Valve_Thumbnail.gif" border="0" alt="Hauck LVG Gas Limiting Orifice Valve" title="Hauck LVG Gas Limiting Orifice Valve" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/Valvario_Modular_Solenoid_Valves_Thumbnail.gif" border="0" alt="Hauck Valvario Modular Solenoid Valves" title="Hauck Valvario Modular Solenoid Valves"/></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/WBV_H_Wafer_Butterfly_Valves_Hot_Air_Thumbnail.gif" border="0" alt="Hauck WBV-H Wafer Butterfly Valves Hot Air" title="Hauck WBV-H Wafer Butterfly Valves Hot Air" /></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/RRG_Ratio_Regulator_Gas_Thumbnail.gif" border="0" alt="Hauck Combustion Parts" title="Hauck RRG Ratio Regulator Gas" title="Hauck RRG Ratio Regulator Gas" /></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/OMG_Orifice_Meters_Thumbnail.gif" border="0" alt="Hauck OMG Orifice Meters" title="Hauck OMG Orifice Meters" /></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/Hauck/thumbnails/IC_Ignition_Wires_Ignitor_and_Transformer_Terminal_Thumbnail.gif" border="0" alt="Hauck IC Ignition Wires/Ignitor and Transformer Terminal" title="Hauck IC Ignition Wires/Ignitor and Transformer Terminal" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>


