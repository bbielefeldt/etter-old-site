<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of SIEMENS combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Siemens,Actuator SKP15,Actuator SKP25,Gas Valve Actuators SKP55 SKP75,Double Gas Valve VGD20,Double Gas Valves VGD40 VGD41,Linkage-less Burner Management System LMV5,Linkage-less Burner Management System LMV3,Control Monitors SQM5,Control Motors SQN7,Temperature Pressure Controller RWF40,Flame Safeguards LFL,Valve Proving Control LDU,Butterfly Valves VKF,Gas Pressure Switches QP,Flame Detectors QRA,Flame Detectors QRI,Pressure Sensor 7MF,Temperature Sensor QAE,Valve Proving System for automatic Shutoff Valves LDU11,Burner Controls LFL1,Burner Controls LFE1,Flame Safeguards LFE10,UV Flame Safeguard LFE50,Gas Burner Controls LGA,Burner Controls LGB,Gas Burner Controls LGC22,Burner Controls LGI16,Burner Controls LOK16 and LGK16,Burner Controls LMG,Pressure Switches QPLx5,Flame Detector QRA,Flame Detector QRA4,Flame Detector QRA10,Flame Detector QRA53 and QRA55 with clamp,Photo Resistive QRB1 A with small flange and clamp,Photo Resistive Flame Detectors QRB1_B with plug,Flame Detectors QRB3 with flange and clamp,Blue Flame Detectors QRC1,Selenium Photocell Detectors RAR,Gas Valve Actuators SKP15,Actuator for Gas Valves SKP25-SKL25,Actuator for Gas Valves SKP55,Siemens Actuator SKP75,Siemens Actuators SQM1 and SQM2,Siemens Actuators SQN3 and SQN4,Siemens Damper Actuators SQN9,Gas Valves VGG,Gas Valves VGF,Siemens Gas Valves VGH" />
<title>ETTER Engineering - SIEMENS Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="SiemensLogoLarge"></div>
<div id="SensusText">Multi-Division Company that is an industry leader 
in communication and building automation technology. We are a SIEMENS ICPI 
(Industrial Combustion Products Integrator) for the Building Technologies 
division which enables us to buy at a deep discount. This division offers 
various combustion products such as gas safety shut-off valves, actuators, 
butterfly valves, motors for gas modulation, flame safety products, sensors, 
scanners and more.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/skp15.gif" alt="Siemens Actuator SKP15" title="Siemens Actuator SKP15"/></div>
			<div id="PartsContent"><h3>Actuator
			<br/><font size="2" color="#50658D">SKP15</font></h3>
			<p><br/>All of the SKP Series electro-hydraulic actuators can be used in combination with any 
			VG Series gas valve. This modular approach creates a matrix of valve combinations, allowing 
			manufacturers to meet varied capacity and pressure regulation requirements with minimal inventory.
			<br/><br/>&#149; SKP Actuators fit all valve sizes from &#189;"
			<br/>&nbsp;&nbsp;    to 6"
			<br/>&#149; Visual stroke position indicator
			<br/>&#149; Optional proof-of-closure overtravel
			<br/>&nbsp;&nbsp;    switch
			<br/>&#149; Optional auxiliary position switches 
			<br/><br/>SKP15 actuators provide gas safety shutoff control.
			<br/><br/>&#149; Safety shut-off control
			<br/>&#149; Optional NEMA 4 protection
			<br/>&#149; Quick connect wiring terminals
			<br/>&#149; Visual position indicator
			<br/>&#149; Low 13.5 VA power consumption
			<br/>&#149; Optional Proof-of-Closure (POC)
			<br/>&nbsp;&nbsp;    over-travel switch
			<br/>&#149; Optional auxiliary switch
			<br/><br/>The Sensus Model 496 was previously manufactured as the Sensus Model 043-B.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP15155751Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP15 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP15.DWG" target="_blank"><font color="#ACB0C3"><b>SKP15 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/skp25.gif"  alt="Siemens Actuator SKP25" title="Siemens Actuator SKP25"/></div>
			<div id="PartsContent"><h3>Actuator
			<br/><font size="2" color="#50658D">SKP25</font></h3>
			<p><br/>TThe SKP25 actuators perform two functions: safety shutoff and gas pressure regulation.  The SKP25 eliminates the need for a separately piped pressure regulator.
			<br/><br/>&#149; Certified as a ventless pressure regulator
			<br/>&#149; Safety shut-off control and pressure 
			<br/>&nbsp;&nbsp;   regulation in one compact unit
			<br/>&#149; Pressure regulation to 20 PSI outlet
			<br/>&#149; Optional Proof-of-Closure (POC) 
			<br/>&nbsp;&nbsp;   over-travel switch.
			<br/>&#149; Optional NEMA 4 protection
			<br/>&#149; Applicable as a 1:1 air/gas ratio control
 			<br/>&#149; Excellent pressure control characteristics
			<br/>&nbsp;&nbsp;   with zero regulator droop.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP25155752Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP25 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP25.DWG" target="_blank"><font color="#ACB0C3"><b>SKP25 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/skp55_and_SKP75.gif" alt="Siemens Gas Valve Actuators SKP55 and SKP75" title="Siemens Gas Valve Actuators SKP55 and SKP75"/></div>
			<div id="PartsContent"><h3>Gas Valve Actuators
			<br/><font size="2" color="#50658D">SKP55 and SKP75</font></h3>
			<p><br/>All of the SKP Series electro-hydraulic actuators can be used in combination with any VG Series gas valve. This modular approach creates a matrix of valve combinations, allowing manufacturers to meet varied capacity and pressure regulation requirements with minimal inventory.
			<br/><br/>&#149; SKP Actuators fit all valve  
			<br/>&nbsp;&nbsp; sizes from &#189;"to 6"
			<br/>&#149;  Visual stroke position indicator
			<br/>&#149; Optional proof-of-closure over   
			<br/>&nbsp;&nbsp; travel switch
			<br/>&#149; Optional auxiliary position switches
			<br/><br/>SKP55 and SKP75 perform three functions: safety shut-off, pressure regulation, and air/gas ration control.  Both eliminate the need for mechanical cams and linkage while improving burner performance.
			<br/><br/>&#149; Excellent control for recuperative 
			<br/>&nbsp;&nbsp;  burners, burners utilizing preheated 
			<br/>&nbsp;&nbsp;  combustion air, and variable   
			<br/>&nbsp;&nbsp;  orifice/flow regulating burners
			<br/>&#149; Air/gas ratio maintained even  
			<br/>&nbsp;&nbsp;  when air flow is disrupted
			<br/>&#149; Compensates for variations in 
			<br/>&nbsp;&nbsp;  combustion chamber back pressure
			<br/>&#149; Low-fire, excess-air biasing 
			<br/>&nbsp;&nbsp;  adjustment
			<br/>&#149; Simplified commissioning of burner  
			<br/>&nbsp;&nbsp;  and reduced start-up time
			<br/>&#149; Optimal solution for premix burner 
			<br/>&nbsp;&nbsp;  applications
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP55155753Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP55 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP75155754Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP75 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP55.DWG" target="_blank"><font color="#ACB0C3"><b>SKP55 CAD Drawing</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP75.DWG" target="_blank"><font color="#ACB0C3"><b>SKP75 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Double_Gas_Valves_VGD20.gif" alt="Siemens Double Gas Valve VGD20" title="Siemens Double Gas Valve VGD20"/></div>
			<div id="PartsContent"><h3>Double Gas Valve
			<br/><font size="2" color="#50658D">VGD20</font></h3>
			<p><br/>&#149; Double gas valves of class for integration 
			<br/>&nbsp;&nbsp;  into gas trains
			<br/>&#149; Safety shutoff valves conforming to EN 
			<br/>&nbsp;&nbsp;  161 in connection with SKP actuators
			<br/>&#149; Suited for use with gases of gas families 
			<br/>&nbsp;&nbsp;  I, III
			<br/>&#149; Double gas valves in connection with SKP  
			<br/>&nbsp;&nbsp; actuators open slowly and close rapidly
			<br/>&#149; 2-port valves of the normally closed type
			<br/>&#149; Sizes 1 &#189;" DN150
			<br/>&#149; The double gas valves are designed for  
			<br/>&nbsp;&nbsp;  combination with 2 actuators
			<br/><br/>The double gas valves are used primarily:
			<br/><br/>&#149; On gas-fired combustion plant
			<br/>&#149; In gas trains in connection with forced 
			<br/>&nbsp;&nbsp;  draft gas burners
			<br/>&#149; They serve as:
			<br/>&#149; Shutoff valves (in connection with SKP1 
			<br/>&nbsp;&nbsp;  actuators)
			<br/>&#149; Control valves with shutoff feature (in  
			<br/>&nbsp;&nbsp;  connection with SKP2, SKP5 or SKP7 
			<br/>&nbsp;&nbsp;  actuators)
			<br/><br/>All types of double gas valves can be combined with any type of SKP actuator.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/VGDn7631en14092006.pdf" target="_blank"><font color="#ACB0C3"><b>VGD PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Double_Gas_Valves_VGD40-VGD41.gif" alt="Siemens Double Gas Valves VGD40 and VGD41" title="Siemens Double Gas Valves VGD40 and VGD41"/></div>
			<div id="PartsContent"><h3>Double Gas Valves
			<br/><font size="2" color="#50658D">VGD40 and VGD41</font></h3>
			<p><br/>&#149; Double gas valves of class A for 
			<br/>&nbsp;&nbsp;  integration into gas trains
			<br/>&#149; Safety shutoff valves conforming to EN 
			<br/>&nbsp;&nbsp;  161 in connection with SKP actuators
			<br/>&#149; Suited for use with gases of gas families 
			<br/>&nbsp;&nbsp;  I, III
			<br/>&#149; Double gas valves in connection with SKP 
			<br/>&nbsp;&nbsp;  actuators open slowly and close rapidly
			<br/>&#149; 2-port valves of the normally closed type
			<br/>&#149; Sizes 1 &#189;" DN150
			<br/>&#149; The double gas valves are designed for 
			<br/>&nbsp;&nbsp;  combination with 2 actuators
			<br/><br/>The double gas valves are used primarily:
			<br/>&#149; On gas-fired combustion plant
			<br/>&#149; In gas trains in connection with forced 
			<br/>&nbsp;&nbsp;  draft gas burners
			<br/>&#149; They serve as:
			<br/>&#149; Shutoff valves (in connection with SKP1 
			<br/>&nbsp;&nbsp;  actuators)
			<br/>&#149; Control valves with shutoff feature (in 
			<br/>&nbsp;&nbsp;  connection with SKP2, SKP5 or SKP7 
			<br/>&nbsp;&nbsp;  actuators)
			<br/><br/>All types of double gas valves can be combined with any type of SKP actuator.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/VGDn7631en14092006.pdf" target="_blank"><font color="#ACB0C3"><b>VGD PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/lmv5.gif" alt="Siemens Linkage-less Burner Management System LMV5" title="Siemens Linkage-less Burner Management System LMV5"/></div>
			<div id="PartsContent"><h3>Linkage-less Burner Management System
			<br/><font size="2" color="#50658D">LMV5</font></h3>
			<p><br/>The LMV5 Linkageless Burner Management System sets the standard high - from easy installation, programming and commissioning to reliable, proven control. With the LMV, SCC forges new ground delivering a fully integrated system that is not only easy to install and use, but also provides improved burner performance and efficiency and ensures safe operation.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_01_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 01 Overview</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_02_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 02 Mounting</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_03_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 03 Wiring</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_04_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 04 Parameters</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_05_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 05 Troubleshooting</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_06_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 06 O2 Trim</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_07_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 07 Variable Speed Drive</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_08_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 08 Modbus</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/QSGSection_09_2010.pdf" target="_blank"><font color="#ACB0C3"><b>QSG Section 09 ACS450 Software</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/lmv3.gif" alt="Siemens Linkage-less Burner Management System LMV3" title="Siemens Linkage-less Burner Management System LMV3"/></div>
			<div id="PartsContent"><h3>Linkage-less Burner Management System
			<br/><font size="2" color="#50658D">LMV3</font></h3>
			<p><br/>The LMV3 Linkageless Burner Management System sets the standard high - from easy installation,
			 programming and commissioning to reliable, proven control. With the LMV, SCC forges new ground delivering 
			a fully integrated system that is not only easy to install and use, but also provides improved burner 
			performance and efficiency and ensures safe operation.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/LMV37 n7546en.pdf" target="_blank"><font color="#ACB0C3"><b>LMV37 Basic Documentation</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/LMV36 p7544en.pdf" target="_blank"><font color="#ACB0C3"><b>LMV36 Basic Documentation</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/SQM33 Original n7813en.pdf" target="_blank"><font color="#ACB0C3"><b>SQM33</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/OCI430 n7635en.pdf" target="_blank"><font color="#ACB0C3"><b>OCI</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/AZL2 Displays.pdf" target="_blank"><font color="#ACB0C3"><b>AZL</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/LMV2-3 q7541en Overview 27-05-2010.pdf" target="_blank"><font color="#ACB0C3"><b>LMV3 Overview</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv3_linkage-less_burner_control_system.aspx_files/LMV2-3 q7541en Overview 27-05-2010.pdf" target="_blank"><font color="#ACB0C3"><b>LMV3 Overview</b></font></a>
			<br/><a href="http://www.scccombustion.com/lmv5_linkage-less_burner_control_system.aspx_files/acs410.zip" target="_blank"><font color="#ACB0C3"><b>Download ACS410 Software</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/SIEMENS_LME7.gif" alt="Siemens Linkage-less Burner Management System LME7" title="Siemens Linkage-less Burner Management System LME7"/></div>
			<div id="PartsContent"><h3>Linkage-less Burner Management System
			<br/><font size="2" color="#50658D">LME7</font></h3>
			<p><br/>The SIEMENS LME7 Linkageless Burner Management System is a microprocessor-based burner control with matching system components for the control and supervision of forced draft burners of medium to high capacity.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="pdfs/LME7 - p7105en - Overview.pdf" target="_blank"><font color="#ACB0C3"><b>LME7 Overview</b></font></a>
			<br/><a href="pdfs/LME7 - p7105en - full technical specifications.pdf" target="_blank"><font color="#ACB0C3"><b>LME7 Full Technical Specifications</b></font></a>
			</p>
			</div></div>            
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/sqm5.gif" alt="Siemens Control Monitors SQM5" title="Siemens Control Monitors SQM5"/></div>
			<div id="PartsContent"><h3>Control Monitors
			<br/><font size="2" color="#50658D">SQM5</font></h3>
			<p><br/>Siemens control motors are highly versatile modulating actuators used for the positioning of flow control valves, butterfly valves, dampers, or any application requiring rotary motion.
 			<br/><br/>The SQM5 is available in a wide range of torque ratings from 90 to 400 in-lbs. and running times of 8 to 50 seconds. A selection of exchangeable circuit boards provide a variety of functions including zero and span adjustment, parallel or master/slave operation, split range control, input signal override and selectable electronic linearization.
			<br/><br/>&#149; Six easily accessible internal switches
			<br/>&#149; Auto/manual switch allows easy
			<br/>&nbsp;&nbsp;    adjustments
			<br/>&#149; Externally visible shaft position indicator
			<br/>&#149; Highly modulating accuracy/rotational
			<br/>&nbsp;&nbsp;    resolution
			<br/>&#149; Low hysteresis actuator with double-offset
			<br/>&nbsp;&nbsp;    drive and potentiometer gearing 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/sqm5-sqn7_control_motors.aspx_files/SQM5_Data_Sheet.pdf" target="_blank"><font color="#ACB0C3"><b>SQM5 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/sqm5-sqn7_control_motors.aspx_files/SQM5.dwg" target="_blank"><font color="#ACB0C3"><b>SQM50 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/SQM4_Synchro_WBG_9W.gif" alt="Siemens Control Motors SQM4" title="Siemens Control Motors SQM4"/></div>
			<div id="PartsContent"><h3>Control Motors
			<br/><font size="2" color="#50658D">SQM4</font></h3>
			<p><br/>Siemens control motors are highly versatile modulating actuators used for the positioning of flow control valves, butterfly valves, dampers, or any application requiring rotary motion. 
			<br/><br/>The SQM4 Synchro is an economical modulating actuator, the SQM4 Synchro has versions up to 90 in-lbs, with 12 or 25 second drive times for 90 degrees. Some of the benefits of the SQM4 Synchro actuator include:
			<br/><br/>&#149; NEMA 4 out of the box - No need for
			<br/>&nbsp;&nbsp;     weather shields
			<br/><br/>&#149; No restrictions on mounting orientation -
			<br/>&nbsp;&nbsp;     No more elaborate linkages to keep the
			<br/>&nbsp;&nbsp;     shaft horizontal!
			<br/><br/>&#149; The modulating input version accepts
			<br/>&nbsp;&nbsp;     2-10 Vdc, 4-20 mA or 0-135 ohm control
			<br/>&nbsp;&nbsp;     signals - one motor
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/sqm5-sqn7_control_motors.aspx_files/SQM40&41 US LIT REV2 100710.pdf" target="_blank"><font color="#ACB0C3"><b>SQM4 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/sqn7.gif" alt="Siemens Control Motors SQN7" title="Siemens Control Motors SQN7"/></div>
			<div id="PartsContent"><h3>Control Motors
			<br/><font size="2" color="#50658D">SQN7</font></h3>
			<p><br/>Siemens control motors are highly versatile modulating actuators used for the positioning of flow control valves, butterfly valves, dampers, or any application requiring rotary motion. 
			<br/><br/>SQN7 control motors are designed to drive gas and air dampers for burners with small to medium capacities.
			<br/><br/>&#149; Line Voltage floating or proportional
			<br/>&nbsp;&nbsp;    control
			<br/>&#149; Optional feedback potentiometer
			<br/>&#149; Up to 22 in-lb torque
			<br/>&#149; Auxiliary switch
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/sqm5-sqn7_control_motors.aspx_files/SQN7155714P25.pdf" target="_blank"><font color="#ACB0C3"><b>SQN7 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/RWF40_Temperature-PressureController.gif" alt="Siemens Temperature/Pressure Controller RWF40" title="Siemens Temperature/Pressure Controller RWF40"/></div>
			<div id="PartsContent"><h3>Temperature/Pressure Controller 
			<br/><font size="2" color="#50658D">RWF40</font></h3>
			<p><br/>The RWF40 single loop temperature/pressure control accepts a variety of input signals. The floating control option eliminates the need for electronics in control motors, reducing overall component and installation costs.
			<br/><br/><b>Product features:</b>
			<br/>&#149; Three analog and two binary inputs.
			<br/>&#149; "Auto Tune" PID output control.
			<br/>&#149; Optional MODBUS interface.
			<br/>&#149; Dual set point and set point limits.
			<br/>&#149; Three-level access/locking feature.
			<br/>&#149; Auto/Manual control feature
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/rwf40_temperature-pressure_controller.aspx_files/3116_1130.pdf" target="_blank"><font color="#ACB0C3"><b>RWF PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/rwf40_temperature-pressure_controller.aspx_files/3117_1131.pdf" target="_blank"><font color="#ACB0C3"><b>RWF PDF User Manual</b></font></a>
			<br/><a href="http://www.scccombustion.com/rwf40_temperature-pressure_controller.aspx_files/3114_1128.pdf" target="_blank"><font color="#ACB0C3"><b>RWF40 PDF Hints</b></font></a>
			<br/><a href="shttp://www.scccombustion.com/rwf40_temperature-pressure_controller.aspx_files/3115_1129.pdf" target="_blank"><font color="#ACB0C3"><b>RWF40 PDF Modbus Interface</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/LFL_Flame_safeguards.gif" alt="Siemens Flame Safeguards LFL" title="Siemens Flame Safeguards LFL"/></div>
			<div id="PartsContent"><h3>Flame Safeguards
			<br/><font size="2" color="#50658D">LFL</font></h3>
			<p><br/>SCC LFL and LAL burner controls provide burner sequencing and flame monitoring for 
			the supervision of individual gas, oil, or dual fueled burners. With an incomparable record 
			for safety and reliability, the economical LFL continues to be one of the most popular, globally 
			approved flame safeguard controls.
			<br/><br/>&#149; 24 terminals provide the functionality of
			<br/>&nbsp;&nbsp;    high-priced competitive products
			<br/>&#149; Built-in flame amplifier for both UV 
			<br/>&nbsp;&nbsp;    scanner or flame rod for the LFL
			<br/>&#149; Various timings available to meet
			<br/>&nbsp;&nbsp;    application requirements
			<br/>&#149; Globally approved - cULus, FM, CSA, CE
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/lfl_flame_safeguards.aspx_files/LFLTechInstrucitons.pdf" target="_blank"><font color="#ACB0C3"><b>LFL PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/LDU_Valve_Proving_Control.gif" alt="Siemens Valve Proving Control LDU" title="Siemens Valve Proving Control LDU"/></div>
			<div id="PartsContent"><h3>Valve Proving Control
			<br/><font size="2" color="#50658D">LDU</font></h3>
			<p><br/>The LDU11 gas valve leak test control checks for leakage through the shutoff valves prior to burner start-up and/or immediately after burner shutdown. The cost effective, easy to install LDU11 adds a substantial degree of safety to any combustion system.
			<br/><br/>&#149; Checks both shutoff valves for leakage
			<br/>&#149; Improves system safety
			<br/>&#149; No inlet gas pressure limatations
			<br/>&#149; Globally approved - cULus, FM, CSA, CE
			<br/>&#149; Easy-to-read dial indicates progress of test 
			<br/>&nbsp;&nbsp;   program
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/ldu_valve_proving_control.aspx_files/3096_1110.pdf" target="_blank"><font color="#ACB0C3"><b>LDU PDF Data sheet - US</b></font></a>
			<br/><a href="http://www.scccombustion.com/ldu_valve_proving_control.aspx_files/LDU11n7696en.pdf" target="_blank"><font color="#ACB0C3"><b>LDU PDF Data sheet - CE</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/VKF_Butterfly_valves.gif" alt="Siemens Butterfly Valves VKF"  title="Siemens Butterfly Valves VKF"/></div>
			<div id="PartsContent"><h3>Butterfly Valves
			<br/><font color="#50658D">VKF</font></h3>
			<p><br/>Butterfly valves designed for fitting between counter-flanges, for integration into gas trains
			<br/><br/>&#149; Disk with metalically tight shutoff
			<br/>&#149; DN40, DN2000
			<br/>&#149; Angle of rotation 85&#176;
			<br/>&#149; No maintenance required
			<br/>&#149; Suited for use with air, natural gas, or 
			<br/>&nbsp;&nbsp;    other clean gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/vkf_butterfly_valves.aspx_files/VKFn7632en.pdf" target="_blank"><font color="#ACB0C3"><b>VKF PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/vkf_butterfly_valves.aspx_files/VKFallsizes.dwg" target="_blank"><font color="#ACB0C3"><b>VKF PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/QP-Gas-Pressure-Switches.gif" alt="Siemens Gas Pressure Switches QP" title="Siemens Gas Pressure Switches QP"/></div>
			<div id="PartsContent"><h3>Gas Pressure Switches
			<br/><font size="2" color="50658D">QP</font></h3>
			<p><br/>SCC now offers Series QP pressure switches for gas and air pressure supervision on industrial and commercial burner applications. Automatic and manual reset versions, in a wide range of available pressures add to SCC extensive combustion control line.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/qp_gas_pressure_switches.aspx_files/3111_1125.pdf" target="_blank"><font color="#ACB0C3"><b>QP PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Flame-Detectors_QRA2_with_clamp.gif" alt="Siemens Flame Detectors QRA" title="Siemens Flame Detectors QRA"/></div>
			<div id="PartsContent"><h3>Flame Detectors
			<br/><font size="2" color="#50658D">QRA</font></h3>
			<p><br/>Flame detectors for use with burner controls from SCC Building Technologies for the supervision of gas or oil flames
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRAn7712en20072007.pdf" target="_blank"><font color="#ACB0C3"><b>QRA PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/QRI_Flame_Detectors.gif" alt="Siemens Flame Detectors QRI" title="Siemens Flame Detectors QRI"/></div>
			<div id="PartsContent"><h3>Flame Detectors
			<br/><font size="2" color="#50658D">QRI</font></h3>
			<p><br/>Infrared flame detectors for use with SCC burner controls, for the supervision of gas, oil and other flames that emit infrared light.  The QRI are suited for burners of any capacity, either in continuous or intermittent operation.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/qra-qri_flame_detectors.aspx_files/QRIn7719en.pdf" target="_blank"><font color="#ACB0C3"><b>QRI PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/7MF-QAE_Temperature-Pressure_Switches.gif" alt="Siemens Pressure Sensor 7MF" title="Siemens Pressure Sensor 7MF"/></div>
			<div id="PartsContent"><h3>Pressure Sensor
			<br/><font size="2" color="#50658D">7MF</font></h3>
			<p><br/>&#149; Pressure sensor with range of 0-15psi
			<br/>&nbsp;&nbsp;     through 0-300psi
			<br/>&#149; Output options 4-20mA & 0-10v
			<br/>&#149; High measuring accuracy, 0.25% of full
			<br/>&nbsp;&nbsp;     scale typical
			<br/>&#149; Aggressive and non-aggressive media
			<br/>&#149; Measures the pressure of liquids, gasses
			<br/>&nbsp;&nbsp;     and vapor
			<br/>&#149; Compact design
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/7mf-qae_temperature-pressure_switches.aspx_files/7MF1564datasheet.pdf" target="_blank"><font color="#ACB0C3"><b>7MF PDF Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/QAE_Temperature_Sensor.gif" alt="Siemens Temperature Sensor QAE" title="Siemens Temperature Sensor QAE""/></div>
			<div id="PartsContent"><h3>Temperature Sensor
			<br/><font color="#50658D">QAE</font></h3>
			<p><br/>The 1000 ohm Nickel temperature sensors a room, outside air, duct, or liquid temperature.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/7mf-qae_temperature-pressure_switches.aspx_files/1000ohmNiRTD155330p2510042007.pdf" target="_blank"><font color="#ACB0C3"><b>1000 ohm Ni RTD PDF Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Valve_Proving_System_for_automatic_Shutoff_Valves_LDU11.gif" alt="Siemens Valve Proving System for automatic Shutoff Valves LDU11" title="Siemens Valve Proving System for automatic Shutoff Valves LDU11"/></div>
			<div id="PartsContent"><h3>Valve Proving System for automatic Shutoff Valves
			<br/><font size="2" color="#50658D">LDU11</font></h3>
			<p><br/>The LDU11 valve proving system is designed for use with shutoff valves in connection
			with gas burners and gas appliances. In the event of inadmissible leakage,
			the system prevents the burner from starting up.
			<br/><br/>
			The LDU11 system conforms to the requirements of EN 1643 covering automatic
			shutoff valves for use with gas burners and gas appliances to EN 161.
			<br/><br/>
			The LDU11 and this Data Sheet are intended for use by OEMs which integrate the
			valve proving system in their products.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LDU11EUn7696en25042005.pdf" target="_blank"><font color="#ACB0C3"><b>LDU11 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LFL1.gif" alt="Siemens Burner Controls LFL1" title="Siemens Burner Controls LFL1"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LFL1</font></h3>
			<p><br/>&#149; For gas, oil or dual-fuel forced draft 
			<br/>&nbsp;&nbsp;    burners of medium to high capacity
			<br/>&#149; For multistage or modulating burners in
			<br/>&nbsp;&nbsp;    intermittent operation
			<br/>&#149; With checked air damper control
			<br/>&#149; Flame supervision
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&#149; with UV detectors QRA
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&#149; and ionization probe
			<br/>&#149; Control and supervision of forced draft
			<br/>&nbsp;&nbsp;    burners of expanding flame or
			<br/>&nbsp;&nbsp;    interrupted pilot construction
			<br/>&#149; For medium to high capacity
			<br/>&#149; For intermittent operation (at least one
			<br/>&nbsp;&nbsp;    controlled shutdown every 24 hours)
			<br/>&#149; For universal use with multistage or
			<br/>&nbsp;&nbsp;    modulating burners
			<br/>&#149; For use with dual-fuel burners
			<br/>&#149; For use with stationary air heaters
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LFL1n7451en13122006.pdf" target="_blank"><font color="#ACB0C3"><b>Model 1100 Bulletin (R-1341)</b></font></a>
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LFE1.gif" alt="Siemens Burner Controls LFE1" title="Siemens Burner Controls LFE1"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LFE1</font></h3>
			<p><br/><br/>The LFE1 is designed for the fully automatic control and supervision of single-stage, multi-stage or modulating 
			gas or dual-fuel burners. It is suited for use with expanding flame and interrupted pilot type gas burners.
			<br/><br/>Flame supervision is ensured by means of an ionization probe or a QRA UV flame detector. Ignition spark proving with a 
			UV flame detector is also possible.
			<br/><br/>When used in connection with a gas valve proving system LDU11, the control sequence of the LFE1 can be extended to include automatic
			gas valve proving.
			<br/>All types of burner controls comply with the relevant European standards for gas and oil
			burners of any capacity.
			<br/><br/>The LFE1 can control the following burner plant components:
			<br/><br/>&#149; fan motor
			<br/>&#149; flue gas fan
			<br/>&#149; air damper
			<br/>&#149; ignition transformer
			<br/>&#149; 1 to 3 fuel valves
			<br/>&#149; load controller
			<br/>&#149; external lockout warning device.	
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LFL1n7451en13122006.pdf" target="_blank"><font color="#ACB0C3"><b>LFL1 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Flame_Safeguards_LAE10_and_LFE10.gif" alt="Siemens Flame Safeguards LFE10" title="Siemens Flame Safeguards LFE10"/></div>
			<div id="PartsContent"><h3>Flame Safeguards
			<br/><font size="2" color="#50658D">LFE10</font></h3>
			<p><br/>For supervision of gas flames and luminous or blue-burning oil flames
			<br/><br/>Supervision in connection with flame detector QRA or ionization probe
			<br/><br/>Flame safeguards are used primarily in conjunction with LEC1 burner
			controls on the following applications:
			<br/><br/>&#149; Dual-supervision of burners / supervision
			<br/>&nbsp;&nbsp;    of the main flame or of the pilot and
			<br/>&nbsp;&nbsp;    main flame by 2 identical or different 
			<br/>&nbsp;&nbsp;    types of flame detectors.
			<br/><br/>&#149; Supervision of forced draft oil / gas
			<br/>&nbsp;&nbsp;    burners / supervision of the flame with
			<br/>&nbsp;&nbsp;    different types of detectors, depending 
			<br/>&nbsp;&nbsp;    on the operating mode.
			<br/><br/>&#149; Multiflame supervision / plants with
			<br/>&nbsp;&nbsp;    several burners whose flames must be
			<br/>&nbsp;&nbsp;    supervised individually by one or several
			<br/>&nbsp;&nbsp;    detectors, whose startup and 
			<br/>&nbsp;&nbsp;    supervision, however, should or must be 
			<br/>&nbsp;&nbsp;    carried out centrally and simultaneously 
			<br/>&nbsp;&nbsp;    by only 1 burner control.
			<br/><br/>&#149; The flame safeguards can also be used
			<br/>&nbsp;&nbsp;    in connection with other types of burner 
			<br/>&nbsp;&nbsp;    controls provided the given combination
			<br/>&nbsp;&nbsp;    and selected circuitry do not impair the
			<br/>&nbsp;&nbsp;    burner control's safety functions.
			<br/><br/>&#149; The flame safeguards are also used as 
			<br/>&nbsp;&nbsp;    flame indication units in combustion 
			<br/>&nbsp;&nbsp;    plant with manual startup
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LxE10n7781en20042006.pdf" target="_blank"><font color="#ACB0C3"><b>LxE10 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/UV_Flame_Safeguard_LFE50.gif" alt="Siemens UV Flame Safeguard LFE50" title="Siemens UV Flame Safeguard LFE50"/></div>
			<div id="PartsContent"><h3>UV Flame Safeguard
			<br/><font size="2" color="#50658D">LFE50</font></h3>
			<p><br/>The DETACTOGYR flame supervision system consists of the UV flame safeguard
			LFE50 and the flame detectors QRA50/ QRA51 It is designed for UV supervision
			and can therefore be used in connection with oil burners, gas burners and gas / oil
			burners. Automatic startup and control of these burners must be ensured with a control
			unit LEC1 (refer to Data Sheet N7761). The system is also suited for the supervision
			of manually operated burners.
			<br/><br/>Prerequisite for the use of the DETACTOGYR system is sufficiently high UV radiation
			at the flame detector's mounting location.
			<br/><br/>For combustion plant where heat generation must also be ensured in the event the
			flame supervision system fails, active redundancy circuitry can be accomplished by
			using 2 DETACTOGYR systems.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LFE50n7783en09052007.pdf" target="_blank"><font color="#ACB0C3"><b>LFE50 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Gas_Burner_Controls_LGA.gif" alt="Siemens Gas Burner Controls LGA" title="Siemens Gas Burner Controls LGA"/></div>
			<div id="PartsContent"><h3>Gas Burner Controls
			<br/><font size="2" color="#50658D">LGA</font></h3>
			<p><br/>The LGA are used for the startup and supervision of atmospheric gas burners
			of small to medium capacity (without fan) in intermittent operation.
			<br/><br/>
			The LGA and this Data Sheet are intended for use by OEMs which integrate the
			gas burner controls in their products.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LGAn7418en14032006.pdf" target="_blank"><font color="#ACB0C3"><b>LGA PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LGB.gif" alt="Siemens Burner Controls LGB" title="Siemens Burner Controls LGB"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LGB</font></h3>
			<p><br/>The LGB burner controls are used for the startup and supervision of 1- or 2-stage gas
			or gas / oil burners in intermittent operation.
			<br/><br/>
			Depending on the type of burner control used, the flame is supervised either by an
			ionization probe, a blue-flame detector QRC1 for forced draft gas / oil burners, or a
			UV detector QRA (with auxiliary unit AGQ1,A27).
			<br/><br/>
			In connection with the respective adapters, the LGB burner controls replace their
			predecessor types LFI7 and LFM1
			<br/><br/>&#149; Automatic forced draft burners for
			<br/>&nbsp;&nbsp;    gaseous fuels to EN 676
			<br/>&#149; Gas burner controls to EN 298
			<br/>&#149; Undervoltage detection
			<br/>&#149; Air pressure supervision with function
			<br/>&nbsp;&nbsp;    check of the air pressure switch during
			<br/>&nbsp;&nbsp;    startup and operation
			<br/>&#149; Electrical remote reset facility
			<br/>&#149; LGB41 for use with atmospheric gas
			<br/>&nbsp;&nbsp;    burners
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LGBn7435en11012007.pdf" target="_blank"><font color="#ACB0C3"><b>LGB PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Gas_Burner_Controls_LGC22.gif" alt="Siemens Gas Burner Controls LGC22" title="Siemens Gas Burner Controls LGC22"/></div>
			<div id="PartsContent"><h3>Gas Burner Controls
			<br/><font size="2" color="#50658D">LGC22</font></h3>
			<p><br/>Gas burner controls for the startup, control and supervision of single-stage atmospheric
			gas burners in intermittent operation, with pilot burners = 250 W and
			flue gas supervision conforming to EN 297.
			<br/><br/>Typical field of use:
			<br/><br/>&#149; Gas-fired heating boilers with or without
			<br/>&nbsp;&nbsp;    d.h.w. heating conforming to EN 297
			<br/><br/>Flame supervision is accomplished with an ionization probe. The demand for heat is visually indicated (on the top of the unit).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LGC22n7620en27112001.pdf" target="_blank"><font color="#ACB0C3"><b>LGC22 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LGI16.gif" alt="Siemens Burner Controls LGI16" title="Siemens Burner Controls LGI16"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LGI16</font></h3>
			<p><br/>&#149; For use on industrial furnaces
			<br/>&#149; For burners in continuous operation
			<br/>&#149; Without fan control and air pressure
			<br/>&nbsp;&nbsp;    supervision
			<br/>&#149; Flame supervision
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&#149; with QRA5, flame detector
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&#149; with ionizations probe
			<br/>&#149; With self-supervising flame signal
			<br/>&nbsp;&nbsp;    amplifier
			<br/>&#149; Quick startup
			<br/>&#149; 1-stage operation with interrupted pilot
			<br/>&nbsp;&nbsp;    burner or 2-stage operation
			<br/>&#149; Common or separate ionization probe
			<br/>&nbsp;&nbsp;    and ignition electrode (single- or
			<br/>&nbsp;&nbsp;    double- electrode operation)
			<br/>&#149; Automatic restart (repetition) or lockout
			<br/>&nbsp;&nbsp;    after loss of flame during operation
			<br/>&#149; Indication of program sequence
			<br/>&#149; Remote reset facility
			<br/>&#149; Programming mechanism in
			<br/>&nbsp;&nbsp;    plastic housing, plugs into the base
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LGIn7786en18082005.pdf" target="_blank"><font color="#ACB0C3"><b>LGI PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LOK16_and-LGK16.gif" alt="Siemens Burner Controls LOK16 and LGK16" title="Siemens Burner Controls LOK16 and LGK16"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LOK16 and LGK16</font></h3>
			<p><br/>&#149; With self-checking flame signal amplifier
			<br/>&#149; For continuously operating multistage or
			<br/>&nbsp;&nbsp;    modulating oil or gas burners of medium 
			<br/>&nbsp;&nbsp;    to high capacity
			<br/>&#149; With air pressure supervision for checked
			<br/>&nbsp;&nbsp;    air damper control.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LGKn7785en18082005.pdf" target="_blank"><font color="#ACB0C3"><b>LGK PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Burner_Controls_LMG.gif" alt="Siemens Burner Controls LMG" title="Siemens Burner Controls LMG"/></div>
			<div id="PartsContent"><h3>Burner Controls
			<br/><font size="2" color="#50658D">LMG</font></h3>
			<p><br/>LMG are designed for the startup and supervision of 1- or 2-stage gas or forced draft
			gas / oil burners in intermittent operation. The flame is supervised with an ionization
			probe or a UV flame detector QRA. (with ancillary unit AGQ2 A27). LMG21 /
			LMG22 in the same housing replace burner controls LGB21 / LGB22 and - with the help of 
			the relevant adapters - burner controls LFI7 and LFM1.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/LMGn7422en21122004.pdf" target="_blank"><font color="#ACB0C3"><b>LMG PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Pressure_Switches_QPLx5.gif" alt="Siemens Pressure Switches QPLx5" title="Siemens Pressure Switches QPLx5"/></div>
			<div id="PartsContent"><h3>Pressure Switches 
			<br/><font size="2" color="#50658D">QPLx5</font></h3>
			<p><br/>The pressure switches are used for monitoring gas or air pressures. When the pressure falls below or exceeds the adjusted switching point, the respective electrical circuit will be opened or changes over.
			<br/><br/>&#149; For the supervision of air or gas pressures 
			<br/>&nbsp;&nbsp;   in gas trains of gas-fired equipment 
			<br/>&nbsp;&nbsp;   (gas burners)
			<br/>&#149; The QPLx5 are suitable as pressure 
			<br/>&nbsp;&nbsp;   switches for minimum or maximum 
			<br/>&nbsp;&nbsp;   pressure
			<br/>&#149; Adjustable working pressure range up to 
			<br/>&nbsp;&nbsp;    500 mbar
			<br/>&#149; Able for a permanent operation pressure 
			<br/>&nbsp;&nbsp;    up to 690 mbar
			<br/>&#149; Suited for gases of gas families 1, 2 and 3 
			<br/>&nbsp;&nbsp;   and other neutral gaseous media 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QPLn7221en03042007.pdf" target="_blank"><font color="#ACB0C3"><b>QPL PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/QRA_Flame_Detectors.gif" alt="Siemens Flame Detector QRA" title="Siemens Flame Detector QRA"/></div>
			<div id="PartsContent"><h3>Flame Detector
			<br/><font size="2" color="#50658D">QRA</font></h3>
			<p><br/>Flame detectors for use with burner controls from SCC Building Technologies for the supervision of gas or oil flames
			<br/><br/>
			The flame detectors are used for the supervision of gas flames, yellow- or blue-burning
			oil flames and for ignition spark proving.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/qra-qri_flame_detectors.aspx_files/QRA4n7711en.pdf" target="_blank"><font color="#ACB0C3"><b>QRA4 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Flame_DetectorQRA4.gif" alt="Siemens Flame Detector QRA4" title="Siemens Flame Detector QRA4"/></div>
			<div id="PartsContent"><h3>Flame Detector
			<br/><font size="2" color="#50658D">QRA4</font></h3>
			<p><br/>The flame detector is used for the supervision of gas flames, yellow- or blue-burning oil
			flames and for ignition spark proving.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRA4UUSn7711en16052003.pdf" target="_blank"><font color="#ACB0C3"><b>QRA4 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="35" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Flame_Detector_QRA10.gif" alt="Siemens Flame Detector QRA10" title="Siemens Flame Detector QRA10"/></div>
			<div id="PartsContent"><h3>Flame Detector
			<br/><font size="2" color="#50658D">QRA10</font></h3>
			<p><br/>The UV flame detectors are designed for use with Siemens burner controls, for
			the supervision of gas or oil flames.
			<br/><br/>
			The flame detectors are used for the supervision of gas flames, yellow- or blue-burning
			oil flames and for ignition spark proving.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRAn7712en20072007.pdf" target="_blank"><font color="#ACB0C3"><b>QRA PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="36" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Flame_Detector_QRA53_and_QRA55_with_clamp.gif" alt="Siemens Flame Detector QRA53 and QRA55 with clamp" title="Siemens Flame Detector QRA53 and QRA55 with clamp"/></div>
			<div id="PartsContent"><h3>Flame Detector
			<br/><font size="2" color="#50658D">QRA53 and QRA55 with clamp</font></h3>
			<p><br/>The UV flame detectors are designed for use with Siemens burner controls, for
			the supervision of gas or oil flames.
			<br/><br/>The QRA are intended for use by OEMs which integrate the
			flame detectors in their products.
			<br/><br/>The flame detectors are used for the supervision of gas flames, yellow- or blue-burning
			oil flames and for ignition spark proving.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRAn7712en20072007.pdf" target="_blank"><font color="#ACB0C3"><b>QRA PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="37" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Photo_Resistive_Flame_Detectors_QRB1_A_with_small_flange_and_clamp.gif" alt="Siemens Photo Resistive QRB1 A with small flange and clamp" title="Siemens Photo Resistive QRB1 A with small flange and clamp"/></div>
			<div id="PartsContent"><h3>Photo Resistive
			<br/><font size="2" color="#50658D">QRB1 A with small flange and clamp</font></h3>
			<p><br/>The QRB are designed for the supervision of yellow-burning oil flames in connection
			with burner controls type LAL, LME7, LMO, LMV and LOA
			They are suited for frontal or lateral (90&#176;) illumination.
			<br/><br/>The maximum spectral sensitivity of the QRB is about 600 nm, thus giving full consideration
			to the maximum level of visible light radiation of yellow-burning oil flames.
			Since the QRB also acquires certain parts of the radiation spectrum of other light
			sources (boiler room lighting, solar radiation, etc.), the standard regulations regarding
			safety in connection with extraneous light still apply.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRBn7714en24042008.pdf" target="_blank"><font color="#ACB0C3"><b>QRB PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="38" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Photo_Resistive_Flame_Detectors_QRB1_B_with_plug.gif" alt="Siemens Photo Resistive Flame Detectors QRB1_B with plug" title="Siemens Photo Resistive Flame Detectors QRB1_B with plug"/></div>
			<div id="PartsContent"><h3>Photo Resistive Flame Detectors
			<br/><font size="2" color="#50658D">QRB1_B with plug</font></h3>
			<p><br/>Photo resistive detectors for use with Siemens burner controls, for the supervision of oil flames in the visible light spectrum. The QRB are used primarily in connection with oil burner controls in intermittent operation.
			The QRB are designed for the supervision of yellow-burning oil flames in connection
			with burner controls type LAL, LME7, LMO, LMV and LOA
			They are suited for frontal or lateral (90&#176;) illumination.
			<br/><br/>The maximum spectral sensitivity of the QRB is about 600 nm, thus giving full consideration
			to the maximum level of visible light radiation of yellow-burning oil flames.
			Since the QRB also acquires certain parts of the radiation spectrum of other light
			sources (boiler room lighting, solar radiation, etc.), the standard regulations regarding
			safety in connection with extraneous light still apply.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRBn7714en24042008.pdf" target="_blank"><font color="#ACB0C3"><b>QRB PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="39" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Photo_Resistive_Flame_Detectors_QRB3_with_flange_and_clamp.gif" alt="Siemens Flame Detectors QRB3 with flange and clamp" title="Siemens Flame Detectors QRB3 with flange and clamp"/></div>
			<div id="PartsContent"><h3>Flame Detectors
			<br/><font size="2" color="#50658D">QRB3 with flange and clamp</font></h3>
			<p><br/>The QRB are designed for the supervision of yellow-burning oil flames in connection
			with burner controls type LAL, LME7, LMO, LMV and LOA 
			They are suited for frontal or lateral (90&#176;) illumination.
			The maximum spectral sensitivity of the QRB is about 600 nm, thus giving full consideration
			to the maximum level of visible light radiation of yellow-burning oil flames.
			Since the QRB also acquires certain parts of the radiation spectrum of other light
			sources (boiler room lighting, solar radiation, etc.), the standard regulations regarding
			safety in connection with extraneous light still apply.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRBn7714en24042008.pdf" target="_blank"><font color="#ACB0C3"><b>QRB PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="40" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Blue-flame_Detectors_QRC1_for_lateral_illumination.gif" alt="Siemens Blue Flame Detectors QRC1" title="Siemens Blue Flame Detectors QRC1"/></div>
			<div id="PartsContent"><h3>Blue Flame Detectors
			<br/><font size="2" color="#50658D">QRC1</font></h3>
			<p><br/>Blue-flame detectors for the supervision of blue- or yellow-burning oil or gas flames.
			<br/><br/>Blue-flame detectors are used primarily in connection with burner controls for
			small-capacity burners in intermittent operation.
			<br/><br/>The QRC1  are intended for use by OEMs which integrate
			the flame detectors in their products!
			<br/><br/>The QRC1 is a compact UV-sensitive blue-flame detector with an integrated preamplifier.
			It is designed for frontal and lateral (90&#176;) illumination.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/QRCn7716en24042008.pdf" target="_blank"><font color="#ACB0C3"><b>QRC PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="41" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Selenium_Photocell_Detectors_RAR.gif" alt="Siemens Selenium Photocell Detectors RAR" title="Siemens Selenium Photocell Detectors RAR"/></div>
			<div id="PartsContent"><h3>Selenium Photocell Detectors
			<br/><font size="2" color="#50658D">RAR</font></h3>
			<p><br/>The RAR flame detectors are used for the supervision of yellow-burning oil flames.
			<br/><br/>They are designed for use with the following types of burner controls: LAL, LAE1,
			LOK16 and LAE10
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/RARn7713en20022007.pdf" target="_blank"><font color="#ACB0C3"><b>RAR PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="42" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuators_for_Gas_Valves_SKP15.gif" alt="Siemens Gas Valve Actuators SKP15" title="Siemens Gas Valve Actuators SKP15"/></div>
			<div id="PartsContent"><h3>Gas Valve Actuators
			<br/><font size="2" color="#50658D">SKP15</font></h3>
			<p><br/>All of the SKP Series electro-hydraulic actuators can be used in combination with any VG Series gas valve. This modular approach creates a matrix of valve combinations, allowing manufacturers to meet varied capacity and pressure regulation requirements with minimal inventory.
			<br/><br/>&#149; SKP Actuators fit all valve  
			<br/>&nbsp;&nbsp; sizes from &#189;"to 6"
			<br/>&#149;  Visual stroke position indicator
			<br/>&#149; Optional proof-of-closure over   
			<br/>&nbsp;&nbsp; travel switch
			<br/>&#149; Optional auxiliary position switches
			<br/><br/>SKP15 actuators provide gas safety shutoff control.
			<br/><br/>&#149; Safety shut-off control
			<br/>&#149; Optional NEMA 4 protection
			<br/>&#149; Quick connect wiring terminals
			<br/>&#149; Visual position indicator
			<br/>&#149; Low 13.5 VA power consumption
			<br/>&#149; Optional Proof-of-Closure (POC) 
			<br/>&nbsp;&nbsp; over-travel switch
			<br/>&#149; Optional auxiliary switch
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP15155751Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP15 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP15.DWG" target="_blank"><font color="#ACB0C3"><b>SKP15 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div id="43" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuators_for_Gas_Valves_SKP25-SKL25.gif" alt="Siemens Actuator for Gas Valves SKP25-SKL25" title="Siemens Actuator for Gas Valves SKP25-SKL25"/></div>
			<div id="PartsContent"><h3>Actuator for Gas Valves 
			<br/><font size="2" color="#50658D">SKP25-SKL25</font></h3>
			<p><br/>The SKP25 operates with a gas pressure governor and controls the gas pressure
			according to the setpoint preselected with the setpoint spring or air pressure signal.
			<br/><br/>Its field of use are primarily forced draft gas burners
			<br/><br/>&#149; with mechanical air / fuel ratio control 
			<br/>&nbsp;&nbsp;   (SKP25.0)
			<br/>&#149; with electronic air / fuel ratio control
			<br/>&nbsp;&nbsp;   (SKP25.0)
			<br/>&#149; with 2-stage setpoint changeover 
			<br/>&nbsp;&nbsp;   (SKP25.2)
			<br/>&#149; with zero governor (SKP25.3)
			<br/>&#149; with constant pressure governor and 
			<br/>&nbsp;&nbsp;   electric adjustment of the setpoint spring
			<br/>&nbsp;&nbsp;   (SKP25.7)
			<br/><br/>The SKL25 actuators are of the same design as the SKP25, but close more slowly
			(3 to 6 seconds).
			<br/><br/>The SKL25 do not conform to the standards for gas applications and, for this reason,
			are only suited for use with air.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SKPx5EUn7643en03042007.pdf" target="_blank"><font color="#ACB0C3"><b>SKPx5 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="44" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuators_for_Gas_Valves_SKP55.gif" alt="Siemens Actuator for Gas Valves SKP55" title="Siemens Actuator for Gas Valves SKP55"/></div>
			<div id="PartsContent"><h3>Actuator for Gas Valves 
			<br/><font size="2" color="#50658D">SKP55</font></h3>
			<p><br/>The SKP55 operates with a differential gas pressure governor and controls a differential
			gas pressure according to a differential air pressure. The ratio of the differential
			pressures is 1-to-1 and constant across the entire air range.
			<br/><br/>Its field of use are predominantly:
			<br/>&#149; combustion plant with combined heat 
			<br/>&nbsp;&nbsp;   recovery systems
			<br/>&#149; plant where pressure conditions in the 
			<br/>&nbsp;&nbsp;   burner and combustion chamber do not
			<br/>&nbsp;&nbsp;   change in proportion to load changes
			<br/>&#149; burners with adjustable air / fuel mixing 
			<br/>&nbsp;&nbsp;   devices in the burner head
			<br/>&#149; plant with negative pressure levels on the 
			<br/>&nbsp;&nbsp;   gas or air side
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SKPx5EUn7643en03042007.pdf" target="_blank"><font color="#ACB0C3"><b>SKPx5 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="45" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuators_for_Gas_Valves_SKP75.gif" alt="Siemens Actuator SKP75" title="Siemens Actuator SKP75"/></div>
			<div id="PartsContent"><h3>Actuator
			<br/><font color="#50658D">SKP75 High Pressure</font></h3>
			<p><br/>&#149; On / Off safety shutoff feature conforming 
			<br/>&nbsp;&nbsp;    to EN161 in connection with SKPx5 
			<br/>&nbsp;&nbsp;    actuators and gas valves supplied 
			<br/>&nbsp;&nbsp;    by Siemens
			<br/>&#149; Damped opening (rapid closing)
			<br/>&#149; Very low power consumption
			<br/>&#149; Suitable for gases of gas families I, III
			<br/>&#149; Optionally with / without end switch
			<br/>&nbsp;&nbsp;    (factory-set)
			<br/>&#149; Plug-in connection facility
			<br/>&#149; Electrical indication of operation
			<br/>&#149; Stroke indication
			<br/>&#149; Supplementary Data Sheets (refer to the
			<br/>&nbsp;&nbsp;    Data Sheets on gas and air valves)
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SKPx5EUn7643en03042007.pdf" target="_blank"><font color="#ACB0C3"><b>SKPx5 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="46" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuactors_SQM1_and_SQM2.gif" alt="Siemens Actuators SQM1 and SQM2" title="Siemens Actuators SQM1 and SQM2"/></div>
			<div id="PartsContent"><h3>Actuators
			<br/><font size="2" color="#50658D">SQM1 and SQM2</font></h3>
			<p><br/>Reversible electromotoric actuators
			<br/><br/>&#149; Torques:&nbsp;&nbsp;&nbsp;&nbsp; - SQM1 up to 10 Nm
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - SQM2 up to 20 Nm
			<br/>&#149; Run times:&nbsp; - SQM1, 14, 100 s
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - SQM2, 29, 66 s
			<br/>&#149; Versions:&nbsp;&nbsp;&nbsp;&nbsp; - Clockwise or 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; counterclockwise 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; rotation
			<br/><br/>The SQM and this Data Sheet are intended for use by OEMs which integrate the
			actuators in their products!
			<br/><br/>The reversible actuators of the SQM range are for use in
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SQM12n7812en01062007.pdf" target="_blank"><font color="#ACB0C3"><b>SQM 1-2 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="47" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Actuators_SQN3_and_SQN4.gif" alt="Siemens Actuators SQN3 and SQN4" title="Siemens Actuators SQN3 and SQN4"/></div>
			<div id="PartsContent"><h3>Actuators
			<br/><font size="2" color="#50658D">SQN3 and SQN4</font></h3>
			<p><br/>The SQN3/ SQN4 actuators are designed for driving gas or air dampers of oil or
			gas burners of small to medium capacity, or for load-dependent control of the fuel or
			combustion air volume:
			<br/><br/>&#149;  In connection with P-PI or PID controllers,
			<br/>&nbsp;&nbsp;     such as the RWF40
			<br/>&#149; Directly via the different types of burner
			<br/>&nbsp;&nbsp;     controls, such as LOA, LMO, LMG or LFL
			<br/>&#149; In connection with 1- or 2-wire control or
			<br/>&nbsp;&nbsp;     3-position controllers
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SQN3SQN4n7808en15022005.pdf" target="_blank"><font color="#ACB0C3"><b>SQN 3-4 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="48" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Damper_Actuators_SQN9.gif" alt="Siemens Damper Actuators SQN9" title="Siemens Damper Actuators SQN9"/></div>
			<div id="PartsContent"><h3>Damper Actuators
			<br/><font size="2" color="#50658D">SQN9</font></h3>
			<p><br/>Reversible electromotoric actuators for air dampers and valves of oil or gas burners
			of small to medium capacity.
			<br/><br/>The SQN9 actuators are designed for driving gas or air dampers of oil or gas burners
			of small to medium capacity, for load-dependent control of the fuel and combustion air volume:
			<br/>&#149; In connection with P-PI or PID controllers, such as the RWF40
			<br/>&#149; Directly via the different types of burner controls, such as LOA, LMO, LMG, LFL
			<br/>&#149; In connection with 1- or 2-wire control or 3-position controllers
			<br/><br/>&#149; Holding torque: 
			<br/> 0.8, 2.4 Nm
			<br/><br/>&#149; Running time: 
			<br/> 4, 24 s
			<br/><br/>&#149; Direction of rotation: 
			<br/>&#149; SQN90 
			<br/>counterclockwise
			<br/>&#149; SQN91 
			<br/>clockwise
			<br/><br/>&#149; SQN9 	
			<br/> Fixing holes and cable entries
			<br/> Equivalent to actuators of the same category made by Conectron and Berger
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/SQN9EUn7806en10112005.pdf" target="_blank"><font color="#ACB0C3"><b>SQN 9 PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="49" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Gas_Valves_VGG.gif" alt="Siemens Gas Valves VGG" title="Siemens Gas Valves VGG"/></div>
			<div id="PartsContent"><h3>Gas Valves
			<br/><font size="2" color="#50658D">VGG</font></h3>
			<p><br/>&#149; Single valves of class A for installation
			<br/>&nbsp;&nbsp;   in gas trains
			<br/>&#149; Safety shutoff valves conforming to
			<br/>&nbsp;&nbsp;    EN161 in connection with SKP actuators
			<br/>&#149; Suitable for use with gases of gas
			<br/>&nbsp;&nbsp;    families I, III
			<br/>&#149; Valves in connection with SKP actuators
			<br/>&nbsp;&nbsp;    open slowly and close rapidly
			<br/>&#149; 2-port valves of the normally closed type
			<br/>&#149; 1 &#189;" DN125
			<br/>&#149; The gas valves are used in connection
			<br/>&nbsp;&nbsp;    with electrohydraulic SKP actuators
			<br/>&#149; As a control valve in connection with SQX
			<br/>&nbsp;&nbsp;    actuators and an AGA60 adapter (not as
			<br/>&nbsp;&nbsp;     a safety shutoff valve)
			<br/>&#149; Supplementary Data Sheets on actuators 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/VGGVGFVGHn7636en18032005.pdf" target="_blank"><font color="#ACB0C3"><b>VGG PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="50" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Gas_Valves_VGF.gif" alt="Siemens Gas Valves VGF" title="Siemens Gas Valves VGF"/></div>
			<div id="PartsContent"><h3>Gas Valves
			<br/><font size="2" color="#50658D">VGF</font></h3>
			<p><br/>&#149; Single valves of class A for installation
			<br/>&nbsp;&nbsp;    in gas trains
			<br/>&#149; Safety shutoff valves conforming to EN161 
			<br/>&nbsp;&nbsp;    in connection with SKP actuators
			<br/>&#149; Suitable for use with gases of gas families
			<br/>&nbsp;&nbsp;    I,III
			<br/>&#149; Valves in connection with SKP  actuators 
			<br/>&nbsp;&nbsp;    open slowly and close rapidly
			<br/>&#149; 2-port valves of the normally closed type
			<br/>&#149; 1 &#189;" DN125
			<br/>&#149; The gas valves are used in connection with
			<br/>&nbsp;&nbsp;    electrohydraulic SKP actuators
			<br/>&#149; As a control valve in connection with SQX
			<br/>&nbsp;&nbsp;    actuators and an AGA60 adapter (not as
			<br/>&nbsp;&nbsp;    a safety shutoff valve)
			<br/>&#149; Supplementary Data Sheets on actuators 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/VGGVGFVGHn7636en18032005.pdf" target="_blank"><font color="#ACB0C3"><b>VGF PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="51" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/Gas_Valves_VGH.gif" alt="Siemens Gas Valves VGH" title="Siemens Gas Valves VGH"/></div>
			<div id="PartsContent"><h3>Gas Valves
			<br/><font size="2" color="#50658D">VGH</font></h3>
			<p><br/>&#149; Single valves of class A for installation
			<br/>&nbsp;&nbsp;    in gas trains
			<br/>&#149; Safety shutoff valves conforming to EN161 
			<br/>&nbsp;&nbsp;    in connection with SKP actuators
			<br/>&#149; Suitable for use with gases of gas families
			<br/>&nbsp;&nbsp;    I,III
			<br/>&#149; Valves in connection with SKP  actuators 
			<br/>&nbsp;&nbsp;    open slowly and close rapidly
			<br/>&#149; 2-port valves of the normally closed type
			<br/>&#149; 1 &#189;" DN125
			<br/>&#149; The gas valves are used in connection with
			<br/>&nbsp;&nbsp;    electrohydraulic SKP actuators
			<br/>&#149; As a control valve in connection with SQX
			<br/>&nbsp;&nbsp;    actuators and an AGA60 adapter (not as
			<br/>&nbsp;&nbsp;    a safety shutoff valve)
			<br/>&#149; Supplementary Data Sheets on actuators 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/eu-ce_european_products.aspx_files/VGGVGFVGHn7636en18032005.pdf" target="_blank"><font color="#ACB0C3"><b>VGH PDF Data sheet</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Siemens/skp15.gif" alt="Siemens Actuator SKP15" title="Siemens Actuator SKP15"/></div>
			<div id="PartsContent"><h3>Actuator
			<br/><font size="2" color="#50658D">SKP15</font></h3>
			<p><br/>All of the SKP Series electro-hydraulic actuators can be used in combination with any 
			VG Series gas valve. This modular approach creates a matrix of valve combinations, allowing 
			manufacturers to meet varied capacity and pressure regulation requirements with minimal inventory.
			<br/><br/>&#149; SKP Actuators fit all valve sizes from &#189;"
			<br/>&nbsp;&nbsp;    to 6"
			<br/>&#149; Visual stroke position indicator
			<br/>&#149; Optional proof-of-closure overtravel
			<br/>&nbsp;&nbsp;    switch
			<br/>&#149; Optional auxiliary position switches 
			<br/><br/>SKP15 actuators provide gas safety shutoff control.
			<br/><br/>&#149; Safety shut-off control
			<br/>&#149; Optional NEMA 4 protection
			<br/>&#149; Quick connect wiring terminals
			<br/>&#149; Visual position indicator
			<br/>&#149; Low 13.5 VA power consumption
			<br/>&#149; Optional Proof-of-Closure (POC)
			<br/>&nbsp;&nbsp;    over-travel switch
			<br/>&#149; Optional auxiliary switch
			<br/><br/>The Sensus Model 496 was previously manufactured as the Sensus Model 043-B.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.scccombustion.com/skp_gas_valve_actuators.aspx_files/SKP15155751Tech.pdf" target="_blank"><font color="#ACB0C3"><b>SKP15 PDF Data sheet</b></font></a>
			<br/><a href="http://www.scccombustion.com/Documents/SKP15.DWG" target="_blank"><font color="#ACB0C3"><b>SKP15 CAD Drawing</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=600" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/skp15_Thumbnail.gif" border="0"  alt="Siemens Actuator SKP15" title="Siemens Actuator SKP15" /></a></li>
		<li><a href="#?w=600" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/skp25_Thumbnail.gif" border="0"  alt="Siemens Actuator SKP25" title="Siemens Actuator SKP25" /></a></li>
		<li><a href="#?w=600" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/skp55_and_SKP75_Thumbnail.gif" border="0"  alt="Siemens Gas Valve Actuators SKP55 and SKP75" title="Siemens Gas Valve Actuators SKP55 and SKP75" /></a></li>
		<li><a href="#?w=600" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Double_Gas_Valves_VGD20_Thumbnail.gif" border="0"  alt="Siemens Double Gas Valve VGD20" title="Siemens Double Gas Valve VGD20" /></a></li>
		<li><a href="#?w=600" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Double_Gas_Valves_VGD40-VGD41_Thumbnail.gif" border="0"  alt="Siemens Double Gas Valves VGD40 and VGD41" title="Siemens Double Gas Valves VGD40 and VGD41"/></a></li>
		<li><a href="#?w=600" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/lmv5_Thumbnail.gif" border="0"  alt="Siemens Linkage-less Burner Management System LMV5" title="Siemens Linkage-less Burner Management System LMV5"/></a></li>
		<li><a href="#?w=600" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/lmv3_Thumbnail.gif" border="0"  alt="Siemens Linkage-less Burner Management System LMV3" title="Siemens Linkage-less Burner Management System LMV3" /></a></li>        
        <li><a href="#?w=600" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/SIEMENS_LME7_thumbnail.gif" border="0"  alt="Siemens Linkage-less Burner Management System LME7" title="Siemens Linkage-less Burner Management System LME7" /></a></li>        
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/sqm5_Thumbnail.gif" border="0"  alt="Siemens Control Monitors SQM5" title="Siemens Control Monitors SQM5"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/SQM4_Synchro_WBG_9W_Thumbnail.gif" border="0"  alt="Siemens Control Motors SQM4" title="Siemens Control Motors SQM4" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/sqn7_Thumbnail.gif" border="0"  alt="Siemens Control Motors SQN7" title="Siemens Control Motors SQN7" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/RWF40_Temperature-PressureController_Thumbnail.gif" border="0"  alt="Siemens Temperature/Pressure Controller RWF40" title="Siemens Temperature/Pressure Controller RWF40"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/LFL_Flame_safeguards_Thumbnail.gif" border="0"  alt="Siemens Flame Safeguards LFL" title="Siemens Flame Safeguards LFL" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/LDU_Valve_Proving_Control_Thumbnail.gif" border="0"  alt="Siemens Valve Proving Control LDU" title="Siemens Valve Proving Control LDU" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/VKF_Butterfly_valves_Thumbnail.gif" border="0"  alt="Siemens Butterfly Valves VKF" title="Siemens Butterfly Valves VKF"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/QP-Gas-Pressure-Switches_Thumbnail.gif" border="0"  alt="Siemens Gas Pressure Switches QP" title="Siemens Gas Pressure Switches QP" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Flame-Detectors_QRA2_with_clamp_Thumbnail.gif" border="0"  alt="Siemens Flame Detectors QRA" title="Siemens Flame Detectors QRA" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/QRI_Flame_Detectors_Thumbnail.gif" border="0"  alt="Siemens Flame Detectors QRI" title="Siemens Flame Detectors QRI" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/7MF-QAE_Temperature-Pressure_Switches_Thumbnail.gif" border="0"  alt="Siemens Pressure Sensor 7MF" title="Siemens Pressure Sensor 7MF" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/QAE_Temperature_Sensor_Thumbnail.gif" border="0"  alt="Siemens Temperature Sensor QAE" title="Siemens Temperature Sensor QAE" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Valve_Proving_System_for_automatic_Shutoff_Valves_LDU11_Thumbnail.gif" border="0"  alt="Siemens Valve Proving System for automatic Shutoff Valves LDU11" title="Siemens Valve Proving System for automatic Shutoff Valves LDU11" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LFL1_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LFL1" title="Siemens Burner Controls LFL1" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LFE1_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LFE1" title="Siemens Burner Controls LFE1" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Flame_Safeguards_LAE10_and_LFE10_Thumbnail.gif" border="0"  alt="Siemens Flame Safeguards LFE10" title="Siemens Flame Safeguards LFE10 " /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/UV_Flame_Safeguard_LFE50_Thumbnail.gif" border="0"  alt="Siemens UV Flame Safeguard LFE50" title="Siemens UV Flame Safeguard LFE50" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Gas_Burner_Controls_LGA_Thumbnail.gif" border="0"  alt="Siemens Gas Burner Controls LGA" title="Siemens Gas Burner Controls LGA" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LGB_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LGB" title="Siemens Burner Controls LGB" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Gas_Burner_Controls_LGC22_Thumbnail.gif" border="0"  alt="Siemens Gas Burner Controls LGC22" title="Siemens Gas Burner Controls LGC22" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LGI16_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LGI16" title="Siemens Burner Controls LGI16" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LOK16_and-LGK16_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LOK16 and LGK16" title="Siemens Burner Controls LOK16 and LGK16" /></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Burner_Controls_LMG_Thumbnail.gif" border="0"  alt="Siemens Burner Controls LMG" title="Siemens Burner Controls LMG" /></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Pressure_Switches_QPLx5_Thumbnail.gif" border="0"  alt="Siemens Pressure Switches QPLx5" title="Siemens Pressure Switches QPLx5" /></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/QRA_Flame_Detectors_Thumbnail.gif" border="0"  alt="Siemens Flame Detector QRA" title="Siemens Flame Detector QRA" /></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Flame_DetectorQRA4_Thumbnail.gif" border="0"  alt="Siemens Flame Detector QRA4" title="Siemens Flame Detector QRA4" /></a></li>
		<li><a href="#?w=400" rel="35" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Flame_Detector_QRA10_Thumbnail.gif" border="0" title="Siemens Flame Detector QRA10"  alt="Siemens Flame Detector QRA10" /></a></li>
		<li><a href="#?w=400" rel="36" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Flame_Detector_QRA53_and_QRA55_with_clamp_Thumbnail.gif" border="0"  alt="Siemens Flame Detector QRA53 and QRA55 with clamp" title="Siemens Flame Detector QRA53 and QRA55 with clamp"/></a></li>
		<li><a href="#?w=400" rel="37" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Photo_Resistive_Flame_Detectors_QRB1_A_with_small_flange_and_clamp_Thumbnail.gif" border="0"  alt="Siemens Photo Resistive QRB1 A with small flange and clamp" title="Siemens Photo Resistive QRB1 A with small flange and clamp" /></a></li>
		<li><a href="#?w=400" rel="38" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Photo_Resistive_Flame_Detectors_QRB1_B_with_plug_Thumbnail.gif" border="0"  alt="Siemens Photo Resistive Flame Detectors QRB1_B with plug" title="Siemens Photo Resistive Flame Detectors QRB1_B with plug" /></a></li>
		<li><a href="#?w=400" rel="39" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Photo_Resistive_Flame_Detectors_QRB3_with_flange_and_clamp_Thumbnail.gif" border="0"  alt="Siemens Flame Detectors QRB3 with flange and clamp" title="Siemens Flame Detectors QRB3 with flange and clamp" /></a></li>
		<li><a href="#?w=400" rel="40" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Blue-flame_Detectors_QRC1_for_lateral_illumination_Thumbnail.gif" border="0"  alt="Siemens Blue Flame Detectors QRC1" title="Siemens Blue Flame Detectors QRC1" /></a></li>
		<li><a href="#?w=400" rel="41" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Selenium_Photocell_Detectors_RAR_Thumbnail.gif" border="0"  alt="Siemens Selenium Photocell Detectors RAR" title="Siemens Selenium Photocell Detectors RAR" /></a></li>
		<li><a href="#?w=400" rel="42" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuators_for_Gas_Valves_SKP15_Thumbnail.gif" border="0"  alt="Siemens Gas Valve Actuators SKP15" title="Siemens Gas Valve Actuators SKP15" /></a></li>
		<li><a href="#?w=400" rel="43" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuators_for_Gas_Valves_SKP25-SKL25_Thumbnail.gif" border="0"  alt="Siemens Actuator for Gas Valves SKP25-SKL25" title="Siemens Actuator for Gas Valves SKP25-SKL25" /></a></li>
		<li><a href="#?w=400" rel="44" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuators_for_Gas_Valves_SKP55_Thumbnail.gif" border="0" alt="Siemens Actuator for Gas Valves SKP55" title="Siemens Actuator for Gas Valves SKP55" /></a></li>
		<li><a href="#?w=400" rel="45" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuators_for_Gas_Valves_SKP75_Thumbnail.gif" border="0"  alt="Siemens Actuator SKP75" title="Siemens Actuator SKP75" /></a></li>
		<li><a href="#?w=400" rel="46" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuactors_SQM1_and_SQM2_Thumbnail.gif" border="0"  alt="Siemens Actuators SQM1 and SQM2" title="Siemens Actuators SQM1 and SQM2" /></a></li>
		<li><a href="#?w=400" rel="47" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Actuators_SQN3_and_SQN4_Thumbnail.gif" border="0"  alt="Siemens Actuators SQN3 and SQN4" title="Siemens Actuators SQN3 and SQN4" /></a></li>
		<li><a href="#?w=400" rel="48" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Damper_Actuators_SQN9_Thumbnail.gif" border="0"  alt="Siemens Damper Actuators SQN9" title="Siemens Damper Actuators SQN9" /></a></li>
		<li><a href="#?w=400" rel="49" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Gas_Valves_VGG_Thumbnail.gif" border="0"  alt="Siemens Gas Valves VGG" title="Siemens Gas Valves VGG" /></a></li>
		<li><a href="#?w=400" rel="50" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Gas_Valves_VGF_Thumbnail.gif" border="0"  alt="Siemens Gas Valves VGF" title="Siemens Gas Valves VGF" /></a></li>
		<li><a href="#?w=400" rel="51" class="Product"><img src="Parts_by_Man_OK_By_Jon/Siemens/thumbnails/Gas_Valves_VGH_Thumbnail.gif" border="0"  alt="Siemens Gas Valves VGH" title="Siemens Gas Valves VGH" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

