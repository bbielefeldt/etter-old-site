<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Eclipse combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Eclipse,WTPUG,WRASP-DI,Winnox,WGD,Vortometric,Vari-Set Mixers,Tubular,ThermJet Self-Recuperative Burner,ThermJet Liquid,ThermJet,ThermAir,TFB,Open Burner Nozzles,Standard Filters for SMJ Blowers,Sinusoidal Plate Heat Exchangers,Soleniod Valves,Socket Plates,SMJ Blowers,Single-Ended Radiant Tube Burners,RHT Indirect Air Heaters,Retraction Mechanism,Rectangular Filters for SMJ Blowers,RatioStar,RatioMatic,RatioAir,European RatioAir HeatPak,P-Tube,PrimeFire Forehearth,PrimeFire 400,PrimeFire 100,Proportional Mixers,Pilot Mixers,Mounting Brackets,Mixing Tees,Minnox,Minimixers,Mini Stainless,Mark IV,Locktite Manual Reset Gas Shut-Off Valves,Line Burners,InciniFume,Incini-Cone,ImmersoPak,ImmersoJet,Heat-Up Burner,GT Underport Burner,GT Bracket,GT/CPA,GT/NG-DI,Furnnox,PrimeFire 300,Forehearth Burners,FlueFire,Extern-a-Therm Recuperator,High ExtensoJet,ER Indirect Air Heater,E-Jector,EH Series Lance Burner,Float Fire,Dimple Insulated,Dimple Plate Heat Exchangers,Deep Spiral Flame (DSF/DSF-CGO),Cross Flow Recuperators,BV-Pak Packaged Butterfly Valves,Butterfly Valves,VAC Burner,BrightFire,Blast Tips,Blast Gate Valve,Bayonet-Ultra Recuperator,Bayonet Recuperator,AutoTite Gas Shut-Off Valves,Automotive Filters for Eclipse Blowers,Automatic Operated Valves,Atmospheric Injectors,Aluminum Air-to-Air,Airjectors,AirHeat,Air Heat Side Plates,Air Heat Side Plates,Air Heat Duct Units,Adjustable Orifice Gas Cocks,Adjustable Limiting Orifice,0400(04V),03V,03FA,03F,Linnox" />
<title>ETTER Engineering - Eclipse Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="EclipseLogoLarge"></div>
<div id="SensusText">Eclipse, Inc. is a worldwide manufacturer of products and systems for industrial heating and drying applications. We design and manufacture a wide variety of gas and oil burners, recuperators and heat exchangers, complete combustion systems, and accessories for combustion systems.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/WTPUG.gif" alt="Eclipse WTPUG" title="Eclipse WTPUG"/></div>
			<div id="PartsContent"><h3>WTPUG</h3>
			<p><b>Water cooled throughport burners for regenerative glass furnaces.</b>
			<br/><br/>Eclipse WTPUG burners are compact, water-cooled burners designed to be inserted into the 
			port neck of a regenerative glass furnace. The burner is fitted to the Eclipse Retraction 
			Mechanism to withdraw the burner from the furnace when not firing. (See Bulletin 1140C for 
			further information.) Use of the Retraction Mechanism ensures consistent combustion quality, 
			which is essential to achieve high glass quality. The burner produces a conical flame, which 
			is ideal for low-flow ports in a glass furnace.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Low maintenance
			<br/>&#149; Fits existing retraction gear
			<br/>&#149; Ideal for low flow ports
			<br/>&#149; Compact design 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=276874.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>      
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/WRASP_DI.gif" alt="Eclipse WRASP-DI" title="Eclipse WRASP-DI"/></div>
			<div id="PartsContent"><h3>WRASP-DI</h3>
			<p><i>Low NOx side of port gas firing system.</i>
			<br/><br/>Side of port firing allows a high degree of flame coverage using burners mounted at 
			the port neck, providing easy access for adjustment and maintenance. In general, side of port 
			firing provides high thermal efficiency. The Dual Impulse burner uses coaxial gas jets in which 
			the center jet flow is adjustable by means of a special calibrated metering valve attached to 
			the burner.
			<br/><br/>By altering the gas to the center jet, the thrust of the burner can be controlled 
			allowing the flame length to be varied by 30% without changing the nozzle. This allows for 
			optimization of the flame. The arrangement also increases the flame luminosity, thus providing 
			greater heat transfer to the glass and lower NOx. A water cooled sealing ring eliminates cold 
			induced air around the burner, increasing efficiency, minimizing NOx as well as prolonging 
			nozzle and burner block life. When the sealing ring is used, the WRASP-DI burner does not 
			require any compressed air for cooling.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=270885.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Winnoxv2_2010.gif" alt="Eclipse Winnox" title="Eclipse Winnox"/></div>
			<div id="PartsContent"><h3>Winnox</h3>
			<p>Efficient and Simple, Winnox Burners Meet Tomorrow's Emissions Mandates Today
			<br/><br/>Designed to comply with the global emissions mandates of the 21st century, Winnox 
			burners are also simple to set up and operate.
			<br/><br/>Ideal for air heating and oven applications, Winnox burners incorporate direct-spark 
			ignition and an air/gas regulator to fire efficiently over a wide gas turndown range at a 
			controlled ratio.
			<br/><br/>The patented nozzle design allows for intense mixing of air and fuel, resulting in 
			extremely low emissions. The short flame is completely contained within the firing tube thanks 
			to the intense swirl generated by the patented nozzle.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161487.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/WGD.gif" alt="Eclipse WGD" title="Eclipse WGD"/></div>
			<div id="PartsContent"><h3>WGD</h3>
			<p><i>Water cooled throughport burners for regenerative glass furnaces.</i>
			<br/><br/>Eclipse WGD burners are compact water cooled burners designed to be inserted into 
			the port neck of a regenerative glass furnace. The burner is fitted to the Eclipse retraction 
			mechanism to withdraw the burner from the furnace when not firing. Using throughport burners 
			with the retraction mechanism ensures the lowest possible maintenance and therefore consistent 
			combustion essential for high glass quality.
			<br/><br/>The burner produces a flat, fan shaped luminous flame using a unique arrangement of 
			converging flat jet nozzles. Only one burner is to be used in each port. Excellent flame coverage 
			of the glass is ensured with this type of burner and flames are extremely luminous ensuring high 
			heat transfer and low NOx. The flame location is very easily adjusted within the furnace ensuring 
			that combustion is taking place at the optimum position.
			<br/><br/><b>Port Design:</b> The correct port design is essential for this burner. Contact your 
			Eclipse representative to provide the design information for each specific application.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=270517.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Vortometric.gif" alt="Eclipse Vortometric" title="Eclipse Vortometric"/></div>
			<div id="PartsContent"><h3>Vortometric</h3>
			<p>Designed to deliver rugged dependability, high outputs and low emissions.
			<br/><br/>Vortometric burners are built to fire light and heavy fuel oils and almost any industrial 
			gas -- including propane, butane, manufactured and mixed gases -- to extremely high outputs. They 
			feature a high combustion air swirl rate that produces a stable flame with high turndown capabilities 
			and very low levels of NOx and CO emissions.
			<br/><br/>Submit worksheet to Eclipse with process conditions and requirements to ensure optimum 
			performance in your application. Worksheets are available in the Business Forms section of the 
			Technical Literature web page (see link below right).
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161864.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Vari-Set-Mixers.gif" alt="Eclipse Vari-Set Mixers" title="Eclipse Vari-Set Mixers"/></div>
			<div id="PartsContent"><h3>Vari-Set Mixers</h3>
			<p>Eclipse Vari-Set Proportional Mixers are used to mix low-pressure air (2 osig to 4 psig) and any 
			commercially available fuel gas at low pressure (4" w.c. to 16 osig) and deliver the mixture under 
			pressure to open or sealed premix-type gas burners.
			<br/><br/>By providing single valve control, the proportional mixer makes temperature control adaptation 
			very simple. The gas is automatically entrained and mixed with the air in the correct air/gas ratio over 
			the entire range of operation. The amount of air pressure that should be provided from the combustion 
			air blower will depend on the turndown range required, and this will vary according to the application.
			<br/><br/>The single-valve control provided by the LP Mixer eliminates the need for the operator to 
			regulate both the gas and air ratios as the heat requirements change. Efficiency losses are inevitable 
			when both the gas and air valves on a burner have to be adjusted manually. When these losses are eliminated 
			by a Series VS Mixer, the investment in equipment is soon repaid in fuel savings. Vari-Set mixers can also 
			be used with the compressed air by using an Eclipse Airjector.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162611.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/TubularPhoto2.gif" alt="Eclipse Tubular" title="Eclipse Tubular"/></div>
			<div id="PartsContent"><h3>Tubular</h3>
			<p>Exothermics Air to Air Industrial Tubular heat exchangers are heat recovery units that effectively reclaim 
			heat from catalytic incinerators, furnaces, thermal oxidizers and many other high temperature process and 
			environmental applications.
			<br/><br/>They help lower energy costs, easily and effectively control process air temperatures, and reclaim 
			a fast return on investment.
			<br/><br/>No other company manufactures a more effective Tubular Heat Recuperator than Exothermics. Our units 
			are installed in hundreds of sites around the world, and we are quickly becoming the preferred choice for high 
			temperature heat recovery equipment.
			<br/><br/>Our Tubular Heat Recuperators are accepted and endorsed worldwide because they simply perform better. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=287107.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ThermJet-Self-Recuperative-Burner.gif" alt="Eclipse ThermJet Self-Recuperative Burner" title="Eclipse ThermJet Self-Recuperative Burner"/></div>
			<div id="PartsContent"><h3>ThermJet Self-Recuperative Burner</h3>
			<p>The Eclipse TJSR V5 is a direct fired, self-recuperative burner based on our industry proven ThermJet 
			technology. This advanced burner design combines a high velocity flame with fuel saving recuperation. A space 
			saving, integral eductor pulls the furnace exhaust through an internal ceramic recuperator. The recuperator 
			preheats the incoming combustion air to very high levels, which improves furnace operating efficiency to 
			reduce fuel usage by as much as 50% over typical ambient air burners.
			<br/><br/>The TJSR V5 design eliminates the need for hot air ductwork required by external recuperators, 
			providing savings in hardware and installation. The internally insulated heat exchanger section and exhaust 
			housing hold heat in the recuperative section, adding to the heat recovery efficiency. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=271290.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ThermJet-Liquid.gif" alt="Eclipse ThermJet Liquid" title="Eclipse ThermJet Liquid"/></div>
			<div id="PartsContent"><h3>ThermJet Liquid</h3>
			<p>The ThermJet Liquid burners are nozzle mix burners for light or heavy oil. The burners are designed to 
			operate on low pressure atomizing air and will operate efficiently throughout a wide operating range. Typical 
			applications include high temperature kilns and forge and melting furnaces.
			<br/><br/>The ThermJet Liquid burners are available in three sizes with maximum capacities from 300,000 BTU/hr 
			to 1,500,000 BTU/hr. All burner models can be controlled on-ratio with a cross-connected fuel regulator to 
			maximize efficiency, or with fixed air to improve temperature uniformity in the furnace. The standard burner 
			configuration includes a manual adjustable oil flow metering valve. The burner can be supplied with an optional
			oil pressure gauge to simplify piping, set-up and adjustment.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=269458.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ThermJet.gif" alt="Eclipse ThermJet" title="Eclipse ThermJet"/></div>
			<div id="PartsContent"><h3>ThermJet</h3>
			<p>Designed to deliver more, ThermJet burners represent a technological leap forward in every area of design 
			and performance that counts. ThermJet burners set new standards for velocity, emissions and flexibility.
			<br/><br/>&#149; ThermJet has the highest operating 
			<br/>&nbsp;&nbsp;velocity of any burner available.
			<br/>&#149; Comparison tests with competitive models 
			<br/>&nbsp;&nbsp;prove ThermJet delivers the lowest 
			<br/>&nbsp;&nbsp;emissions.
			<br/>&#149; Integrated gas and air orifices simplify 
			<br/>&nbsp;&nbsp;burner piping, set-up and adjustment.
			<br/>&#149; Air and gas inlets are independently 
			<br/>&nbsp;&nbsp;adjustable in 90 degree increments to suit 
			<br/>&nbsp;&nbsp;a variety of piping alternatives.
			<br/>&#149; Installation, operation, and maintenance 
			<br/>&nbsp;&nbsp;are simplified and less costly.
			<br/>&#149; Made to last -- rugged Eclipse Combustion 
			<br/>&nbsp;&nbsp;dependability and reliability are built in.
			<br/>&#149; Available in fourteen sizes with maxmum 
			<br/>&nbsp;&nbsp;capacities from 150,000 to 20,000,000 
			<br/>&nbsp;&nbsp;Btu/hr. All models are available for use with 
			<br/>&nbsp;&nbsp;preheated combustion air. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161936.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ThermAir-web.gif" alt="Eclipse ThermAir" title="Eclipse ThermAir"/></div>
			<div id="PartsContent"><h3>ThermAir</h3>
			<p>The Eclipse ThermAir burner is a nozzle-mixing burner with packaged air blower that's designed to fire with 
			fixed combustion air over a wide gas turndown range. Fixed air operation plus integral air and gas orifices 
			make the ThermAir one of the simplest burners ever to quickly setup and adjust.
			<br/><br/>ThermAir burners are ideal on heaters, textile ovens and in situations where the fuel is highly 
			variable (800 Btu/cf to 3200 Btu/cf). Plus they're perfect for ovens needing additional air to carry moisture 
			away from the product being heated.
			<br/><br/>Check out these quality construction and performance features:
			<br/><br/>&#149; Easy to install, operate and maintain. 
			<br/>&nbsp;&nbsp;Right or left hand piping; readily adapts to 
			<br/>&nbsp;&nbsp;your present system. Burner can be 
			<br/>&nbsp;&nbsp;disassembled from the rear.
			<br/>&#149; Global design: metric/English gas 
			<br/>&nbsp;&nbsp;interface.
			<br/>&#149; 30:1 turndown with UV scanner or 
			<br/>&nbsp;&nbsp;Flame Rod.* 
			<br/><br/><i>*Flame Rod not available on all models.</i>
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=298208.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/TFB.gif" alt="Eclipse TFB" title="Eclipse TFB"/></div>
			<div id="PartsContent"><h3>TFB</h3>
			<p>The Eclipse Tube Firing Burner is designed to fire radiant and immersion tubes with inputs from 25,000 to 
			2,000,000 Btu/hr. The TFB comes equipped with the Eclipse ThermThief system. A unique nozzle creates an 
			intense adjustable-length flame with a vigorous spinning action. The long, spiraling flame results in cleaner 
			combustion, efficient heat transfer and more uniform tube temperatures. The flame scrubs the inside of the 
			fire tubes to remove the gas film boundary layer and increase heat-transfer effectiveness with outstanding 
			temperature uniformity. Not only do these factors contribute to longer tube life, they also ensure consistent 
			uniform product processing.
			<br/><br/>Additional benefits include:
			<br/><br/>&#149; 3Low NOx and low CO.
			<br/>&#149; 3Low excess air capability for high 
			<br/>&nbsp;&nbsp;efficiency.
			<br/>&#149; 3Low air and gas pressure requirements.
			<br/>&#149; 3High heat transfer at low noise levels.
			<br/>&#149; 3Reliable ignition at a wide range of firing 
			<br/>&nbsp;&nbsp;rates.
			<br/>&#149; 3Can be fitted with Eclipse Bayonet-Ultra 
			<br/>&nbsp;&nbsp;recuperators for maximum system 
			<br/>&nbsp;&nbsp;efficiency. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162017.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Sticktite.gif" alt="Eclipse Open Burner Nozzles" title="Eclipse Open Burner Nozzles"/></div>
			<div id="PartsContent"><h3>Open Burner Nozzles</h3>
			<p>Eclipse Sticktite and Ferrofix nozzles can be used wherever an open-type nozzle is applicable. Combustion 
			chambers must have balanced or negative pressure. Typical applications include boiler conversion, kilns, 
			ovens, air heaters, furnaces, immersion heating and other applications where the slight excess air introduced 
			into the combustion chamber around the burner is acceptable.
			<br/><br/>Eclipse Open Burner Nozzles require a quote from our Mod shop. Contact your Eclipse office for 
			additional information.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=164479.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/StandardFilter-for-SMJ-Blower.gif" alt="Eclipse Standard Filters for SMJ Blowers" title="Eclipse Standard Filters for SMJ Blowers"/></div>
			<div id="PartsContent"><h3>Standard Filters for SMJ Blowers</h3>
			<p>Eclipse standard filters can be mounted in place of the standard grill on all Eclipse SMJ Blowers to clean 
			excess dust and dirt from the combustion air and keep the blower rotor clean.
			<br/><br/>They have a wide variety of applications wherever SMJ blowers are used, making them a great addition 
			to any systems package. They are also replaceable and easy to clean.
			<br/><br/>Add an Eclipse Standard Filter to your system and start realizing the benefits.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162572.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/SPMT.gif" alt="Eclipse Sinusoidal Plate Heat Exchangers" title="Eclipse Sinusoidal Plate Heat Exchangers"/></div>
			<div id="PartsContent"><h3>Sinusoidal Plate Heat Exchangers</h3>
			<p>Sinusoidal Plate Heat Exchangers are the flagship of the Exothermics line. Our best-in-class sinusoidal plate 
			design provides maximum effectiveness and the highest performance available on the market today, bar none.
			<br/><br/>High heat transfer effectiveness is produced by corrugating the heat transfer plates to achieve our 
			proprietary sinusoidal wave pattern. The corrugated surface creates air flow turbulence as the air passes through 
			the unit. Heat transfer is further enhanced by using a counterflow design.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=287113.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Soleniod_Valves.gif" alt="Eclipse Soleniod Valves" title="Eclipse Soleniod Valves"/></div>
			<div id="PartsContent"><h3>Soleniod Valves</h3>
			<p><b>Two Way, Normally Open or Normally Closed, Aluminum Body</b>
			<br/><br/>These valves are offered as Model 8214 vent valves from 3/4" to 2-1/2", Model 8040 pilot gas valves 
			in 1/4" to 3/8", Model 8214 direct operating valves from 3/8" to 3/4", Model 8214 internal pilot operated 
			valves from 1/2" to 3", Model 8214VI valves with visual indication from 3/4" to 3", and Model 8214C valves 
			with proof of closure and indication from 3/4" to 3". 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162904.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/socket-plate.gif" alt="Eclipse Socket Plates" title="Eclipse Socket Plates"/></div>
			<div id="PartsContent"><h3>Socket Plates</h3>
			<p>A good seal will be tight, will permit easy placement and removal of the burner, will be long lasting, 
			will be easily maintained, and will not be heat absorbent (i.e., water cooled). Eclipse Inc. brings you a 
			ball end nozzle and ribbed socket place with a properly designed tile which meets all of these requirements.
			<br/><br/>SOCKET FEATURES
			<br/><br/>&#149; For regenerative furnace systems
			<br/>&#149; Seals burner to burner block
			<br/>&#149; Elimination of inspirated air around burner 
			<br/>&nbsp;&nbsp;nozzle results in average 5% fuel savings
			<br/>&#149; Optional water-cooled model available 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258158.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/SMJ-Blowers.gif" alt="Eclipse "SMJ" Blowers" title="Eclipse "SMJ" Blowers"/></div>
			<div id="PartsContent"><h3>"SMJ" Blowers</h3>
			<p>The Eclipse SMJ Blower is the most versatile and user-friendly blower on the market. They can be used for 
			cooling, conveying, drying, liquid agitation, smoke abatement, vacuum cleaning, flume and dust exhausting, and 
			other low-temperature applications.*
			<br/><br/>What makes the SMJ Blower more versatile than its competition is its improved design, which features 
			a heavy-gauge steel base and housing, aluminum impellers that are balanced statically and dynamically, and a 
			revamped fan housing that allows the end-user to reconfigure the blower in the field by changing the direction 
			of the fan's rotation. The outlet couplings on this blower model have also been replaced with flanges to make 
			the blower easier to handle during installation.
			<br/><br/>*Under 220 degrees Fahrenheit (104 degrees Celcius)
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162565.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Single-Ended-Radiant-Tube-Burner.gif" alt="Eclipse Single-Ended Radiant Tube Burners" title="Eclipse Single-Ended Radiant Tube Burners"/></div>
			<div id="PartsContent"><h3>Single-Ended Radiant Tube Burners</h3>
			<p>The SER V5 burner delivers the best fuel efficiency of any burner in its class.
			<br/><br/>With the burner and enhanced performance recuperator coaxially mounted inside a radiant tube, the SER 
			V5 internally transfers combustion exhaust heat back to the burner air supply in a counterflow pattern, providing 
			high preheat temperatures.
			<br/><br/>&#149; The new, high efficiency recuperator design 
			<br/>&nbsp;&nbsp;incorporates a unique dual finned 
			<br/>&nbsp;&nbsp;combustor to provide an increased heat 
			<br/>&nbsp;&nbsp;transfer surface for greater fuel savings.
			<br/>&#149; SER v5 delivers exceptional heat flux and 
			<br/>&nbsp;&nbsp;temperature uniformity. SER V5 cuts fuel 
			<br/>&nbsp;&nbsp;costs from 35% to 55% over sealed 
			<br/>&nbsp;&nbsp;ambient air burners and even more when 
			<br/>&nbsp;&nbsp;replacing atmospheric type burners.
			<br/>&#149; NOx emissions are reduced by more than 
			<br/>&nbsp;&nbsp;50% over conventional recuperative 
			<br/>&nbsp;&nbsp;burners.
			<br/>&#149; Self-recuperative design eliminates the 
			<br/>&nbsp;&nbsp;need for external recuperators and 
			<br/>&nbsp;&nbsp;insulated piping.
			<br/>&#149; The insulated mounting flange and 
			<br/>&nbsp;&nbsp;exhaust housing improve heat recovery 
			<br/>&nbsp;&nbsp;while creating a much cooler working 
			<br/>&nbsp;&nbsp;environment.
			<br/>&#149; Burners can be installed either vertically or 
			<br/>&nbsp;&nbsp;horizontally in nearly any furnace, either 
			<br/>&nbsp;&nbsp;gas or electric.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=291094.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RHT.gif" alt="Eclipse RHT Indirect Air Heaters" title="Eclipse RHT Indirect Air Heaters"/></div>
			<div id="PartsContent"><h3>RHT<br/><font color="#445679" size="2">Indirect Air Heaters</font></h3>
			<p>Eclipse Recirculating High Temperature (RHT) indirect air heating burners are designed to heat recirculating
			ovens and dryers where the products of combustion must be isolated from the process air stream. They are also 
			excellent for use in industrial space heating systems. These burners feature an industry-proven burner, firing 
			chamber and exhaust tubes assembled as a single unit for easy installation and optimum performance.blank">
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RetractionMech-1140.gif" alt="Eclipse Retraction Mechanism" title="Eclipse Retraction Mechanism"/></div>
			<div id="PartsContent"><h3>Retraction Mechanism</h3>
			<p>Retraction mechanism for water cooled throughport burners.
			<br/><br/>For use with all types of Eclipse watercooled throughport burners. The mechanism is intended for 
			installation under the ports with the burners inserted through holes in the bottom of the port. Each mechanism 
			is designed to operate in a hot dusty environment for long periods with minimun maintenance.
			<br/><br/>Retracting burners from the furnace when not firing minimizes the need for frequent nozzle cleaning thus 
			ensuring repeatable firing for long periods of time. This results in higher glass quality and improved thermal 
			efficiency. The recommended burner cleaning interval is one month, although experience shows longer intervals are possible.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=270869.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RectangularFilters-For-SMJ-Blowers.gif" alt="Eclipse Rectangular Filters for SMJ Blowers" title="Eclipse Rectangular Filters for SMJ Blowers"/></div>
			<div id="PartsContent"><h3>Rectangular Filters for SMJ Blowers</h3>
			<p>Eclipse rectangular filters can be mounted in place of the standard grill on all Eclipse SMJ Blowers to clean excess 
			dust and dirt from the combustion air and keep the blower rotor clean.
			<br/><br/>They have a wide variety of applications wherever SMJ blowers are used, making them a great addition to 
			any systems package. They are also replaceable and easy to clean.
			<br/><br/>Add an Eclipse Rectangular Filter to your system and start realizing the benefits.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=269644.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RatioStar-duct.gif" alt="Eclipse RatioStar" title="Eclipse RatioStar"/></div>
			<div id="PartsContent"><h3>RatioStar</h3>
			<p>RatioStar is a modular duct burner designed for direct fired air heating applications. Burners are configured in rows of 
			up to 24 modules. Propagation modules connect individual burner rows to provide cross ignition. This modular design allows 
			for a wide range of burner matrix configurations. Burner modules are built with high quality stainless steel. Eclipse 
			engineers can design a complete RatioStar duct system with gas train to meet specific customer requirements. 
			<br/><br/>Typical applications include:
			<br/><br/>&#149; Air heating processes that require 
			<br/>&nbsp;&nbsp;optimum heat distribution in process 
			<br/>&nbsp;&nbsp;air flows.
			<br/>&#149; Air heating processes that requires 
			<br/>&nbsp;&nbsp;optimum combustion efficiency 
			<br/>&nbsp;&nbsp;(On ratio control).
			<br/>&#149; Low Oxygen process air flows that require 
			<br/>&nbsp;&nbsp;additional heat.
			<br/>&#149; Air flows with high inlet temperatures up to 
			<br/>&nbsp;&nbsp;750&deg;C, where standard air heating burners 
			<br/>&nbsp;&nbsp;cannot be used. 
			<br/>&#149; Processes that require low emissions for 
			<br/>&nbsp;&nbsp;CO, NOx and unburned Hydro Carbons 
			</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Ratiomatic.gif" alt="Eclipse RatioMatic" title="Eclipse RatioMatic"/></div>
			<div id="PartsContent"><h3>RatioMatic</h3>
			<p>The world's most reliable air heating burner is now even safer and simpler.
			<br/><br/>New design takes the guesswork out of start-up and adjustment.
			<br/><br/>Compare these user-preferred features of RatioMatic to any competitive model:
			<br/><br/>&#149; Ratio regulator automatically compensates 
			<br/>&nbsp;&nbsp;for changes in operating conditions --such 
			<br/>&nbsp;&nbsp;as dirty air filters-- which can upset the 
			<br/>&nbsp;&nbsp;air/gas ratio in competitive linked valve 
			<br/>&nbsp;&nbsp;systems.
			<br/>&#149; Turndown range of up to 100:1.
			<br/>&#149; Direct-drive air butterfly eliminates 
			<br/>&nbsp;&nbsp;linkages which can slip or jam 
			<br/>&nbsp;&nbsp;during operation.
			<br/>&#149; Fast-mixing nozzle provides clean stable 
			<br/>&nbsp;&nbsp;flame at all firing rates and releases heat 
			<br/>&nbsp;&nbsp;into the chamber, not the oven wall.
			<br/>&#149; Ideal for emission-sensitive processes due 
			<br/>&nbsp;&nbsp;to low NOx, CO and aldehydes.
			<br/>&#149; Capacities from 500,000 to 25,000,000 
			<br/>&nbsp;&nbsp;Btu/hr. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161411.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RatioAir.gif" alt="Eclipse RatioAir" title="Eclipse RatioAir"/></div>
			<div id="PartsContent"><h3>RatioAir</h3>
			<p>Available with three different outlet tube velocity characteristics (standard, medium and high velocity), RatioAir burners are capable of flame speeds of up to 500 feet per second. This results in improved temperature uniformity, product quality and system efficiency. A ratio regulator automatically compensates for changes in operating conditions and assures ratio controlled operation over a turndown range in excess of 50:1. A direct drive air butterfly eliminates linkages which can slip or jam during operation.
			<br/><br/>Designed for application versatility and simplicity of operation. The Eclipse RatioAir is a premium air heating burner ideally suited for applications that require:
			<br/><br/>&#149; A velocity burner with integral packaged 
			<br/>&nbsp;&nbsp;blower.
			<br/>&#149; High excess air and ratio control.
			<br/>&#149; Use of unusual fuels (e.g. low Btu fuels).
			<br/>&#149; High chamber temperatures 
			<br/>&nbsp;&nbsp;(up to 2500o F - 1370o C).
			<br/>&#149; Confined firing chambers. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=296003.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/RA-Euro-HP.gif" alt="Eclipse European RatioAir HeatPak" title="Eclipse European RatioAir HeatPak"/></div>
			<div id="PartsContent"><h3>European RatioAir HeatPak</h3>
			<p>RAHP burners comply with European Standard EN746-2: 2006 and are available with straight and medium velocity combustor 
			options for optimal application integration. A ratio regulator automatically compensates for changes in operating conditions 
			and assures ratio controlled operation over a turndown range in excess of 20:1. A direct drive air butterfly valve eliminates 
			linkages which can slip or jam during operation.
			<br/><br/>Designed for application versatility and simplicity of operation, the Eclipse RatioAir is a premium air heating 
			burner ideally suited for applications that require:
			<br/><br/>&#149; A velocity burner with integral packaged 
			<br/>&nbsp;&nbsp;blower.
			<br/>&#149; Precise ratio control.
			<br/>&#149; Use of unusual fuels (e.g. low Btu fuels).
			<br/>&#149; High chamber temperatures 
			<br/>&nbsp;&nbsp;(up to 1370&deg;C / 2500&deg;F).
			<br/>&#149; Confined firing chambers.
			<br/>&#149; Capacities from 200 to 900 kW. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=297457.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/P-Tube.gif" alt="Eclipse P-Tube" title="Eclipse P-Tube"/></div>
			<div id="PartsContent"><h3>P-Tube</h3>
			<p>The P-Tube Burner is a radiant tube fire burner for use in P-Tubes. P-Tube Burner combines the benefits of the SER v3.0 
			with the surface area of a U-Tube, meaning less burners and less cost for the customer. The burners are designed to fire up 
			to 350 KBTU/hr into a 6" or 7.5" P-Tube. The burner is self-recuperative and utilizes flue gas recirculation providing the 
			following benefits:
			<br/><br/>&#149; Cuts fuel costs up to 50%
			<br/>&#149; Higher efficiencies than U-Tube without 
			<br/>&nbsp;&nbsp;recuperation
			<br/>&#149; Better tube temperature uniformity than 
			<br/>&nbsp;&nbsp;U-Tubes with recuperation
			<br/>&#149; Lower NOX emissions than conventional 
			<br/>&nbsp;&nbsp;recuperation
			<br/>&#149; Lower system costs than multiple burner 
			<br/>&nbsp;&nbsp;systems
			<br/>&#149; Can be fired on/off, high/low, proportional, 
			<br/>&nbsp;&nbsp;and pulse
			<br/>&#149; "Jet" velocity nozzle design
			<br/>&#149; Can be fitted to existing gas or electric 
			<br/>&nbsp;&nbsp;furnaces
			<br/>&#149; Eliminates hot air pipework 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=273257.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/PrimefireForehearth.gif" alt="Eclipse PrimeFire Forehearth" title="Eclipse PrimeFire Forehearth"/></div>
			<div id="PartsContent"><h3>PrimeFire Forehearth</h3>
			<p>The patented Primefire Forehearth burner from Eclipse, Inc. delivers significant reductions in fuel consumption and emissions 
			in glass forehearth operations. It incorporates a nozzle-mix design which eliminates the equipment costs associated with pre-mix 
			style burners. The burner design also simplifies set-up and operation. It can fire natural gas, propane, butane or mixed fuels.
			<br/><br/>Oxygen-fuel fired burners are now accepted as a safe, reliable and efficient method for precisely controlling the 
			energy input in glass forehearths This field proven combustion technology can reduce fuel consumption by over 60%, lower NOx 
			emissions by over 70% and deliver a higher glass yield. The efficiencies gained are a result of the higher heat flux from the 
			oxygen-fuel flame with a temperature of up to 4,900&deg;F. The oxy-fuel flame improves temperature distribution and heat uniformity. 
			Improved glass quality is also achieved due to the lower surface tension created with oxygen-fuel combustion. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=270906.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div> 
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Primefire400.gif" alt="Eclipse PrimeFire 400" title="Eclipse PrimeFire 400"/></div>
			<div id="PartsContent"><h3>PrimeFire 400</h3>
			<p>The Eclipse Inc. PrimeFire 400 Series oxygen-fuel burner is an advanced, non-water cooled burner featuring a flat flame 
			geometry which provides enhanced flame coverage over the load. This flat flame shape also provides uniform heat distribution 
			resulting in improved product quality and extended furnace refractory life.
			<br/><br/>The Primefire Series 400 oxygen-fuel burner incorporates a pre-combustor at the back of the burner which mixes a 
			portion of the combustion oxygen with the fuel stream. This causes gas cracking which produces soot or free carbon particles. 
			These free carbon particles increase luminosity and improve the radiant heat transfer to the glass load. Overall furnace 
			efficiency is improved, peak flame temperatures are reduced and NOx formation is lowered.
			<br/><br/>Primefire Series 400 oxygen-fuel burners are available in four sizes with maximum capacities of 2MM, 4MM, 10MM and 
			20MM Btu/hr. All models are capable of 4:1 turndown and can be fired using natural gas or fuel oil.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258095.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>      
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Primefire100.gif" alt="Eclipse PrimeFire 100" title="Eclipse PrimeFire 100"/></div>
			<div id="PartsContent"><h3>PrimeFire 100</h3>
			<p>The PrimeFire 100 is our oxygen-fuel burner. It has become the industry standard--providing greater flexibility, extended 
			fuel firing capability, increased melter efficiency, improved refractory life, and reduced melting cost! The Primefire 100 
			oxygen-fuel burner produces a conventional shape flame and has multiple-fuel capabilities (natural gas and light/heavy oils). 
			The Primefire 100 burner is currently in use at many glass and high temperature furnaces worldwide.
			<br/><br/>The adjustable control on the burner allows variation in flame coverage to suit melter size and temperature profile. 
			The higher firing range burner can be used in the float glass furnaces. Many major float companies have used the Primefire 100 
			successfully for up to a five percent increase in overall production.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258092.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Porportional-Mixers.gif" alt="Eclipse Proportional Mixers" title="Eclipse Proportional Mixers"/></div>
			<div id="PartsContent"><h3>Proportional Mixers</h3>
			<p>Low Pressure Proportion Mixers are used to mix air and any commercially available fuel gas at low pressure and deliver the 
			mixture under pressure to open or sealed premix type gas burners. (2 oz. to 4 psi air and 4" w.c. to 16 oz gas.)
			<br/><br/>The single-valve control provided by the LP Mixer eliminates the need for the operator to regulate both the gas and 
			air ratios as the heat requirements change. Efficiency losses are inevitable when both the gas and air valves on a burner have 
			to be adjusted manually, and even a slight excess in air or gas can cause serious losses in efficiency and extend the length of 
			your heat-up time. When these losses are eliminated by a Series LP Mixer, the investment in equipment is soon repaid in fuel 
			savings. LP mixers can also be used with compressed with the help of an Eclipse Airjector.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162601.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Pilot-Mixers.gif" alt="Eclipse Pilot Mixers" title="Eclipse Pilot Mixers"/></div>
			<div id="PartsContent"><h3>Pilot Mixers</h3>
			<p>Eclipse High Pressure Atmospheric Pilot Mixers operate on the same principle as the Low Pressure Atmospheric Pilot Mixers. 
			However, the kinetic energy of the high pressure gas is sufficient to entrain all the air required for combustion. This feature 
			makes it possible to use high pressure pilot mixers on sealed burner applications as there is no requirement for secondary air 
			at the pilot nozzle.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=163387.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/MountingBrackets.gif" alt="Eclipse Mounting Brackets" title="Eclipse Mounting Brackets"/></div>
			<div id="PartsContent"><h3>Mounting Brackets</h3>
			<p>Good flame coverage and heat transfer to the glass without impingement to furnace refractories is essential, and the Eclipse Inc. 
			gimbal arm bracket meets all of the requirements to promote effective burner handling and firing conditions. The arm gimbal bracket 
			has also been specifically designed to allow angular adjustment of the burner about the horizontal and vertical axis without repositioning 
			the bracket while maintaining a sealed-in firing condition utilizing the gimbal privot point principle.
			<br/><br/>Uniform bolt sizes throughout the bracket permit easy adjustment with two ratchet wrenches. Oversized taps prevent thread 
			seizures and brass locking bolts with spring washers hold positions firm against vibration. Assembly bolts can be tightened to resist 
			stress placed upon the bracket by various connecting lines, especially a stiff gas hose.
			<br/><br/>BURNER BRACKET FEATURES
			<br/><br/>&#149; Regenerative furnace - sideport firing
			<br/>&#149; Aiming adjustment pivots burner ball in 
			<br/>&nbsp;&nbsp;the socket plate to retain seal
			<br/>&#149; Designed for use in hot, dirty firing 
			<br/>&nbsp;&nbsp;operations
			<br/>&#149; Buckstay mounting plate and adjustable 
			<br/>&nbsp;&nbsp;cross arms
			<br/>&#149; Tilt, rotation, vertical, and horizontal 
			<br/>&nbsp;&nbsp;adjustments 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258154.pdf" target="_blank"><font color="#ACB0C3"><b>09A Bulletin Arm Style Mounting</b></font></a>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258155.pdf" target="_blank"><font color="#ACB0C3"><b>09AG Bulletin Arm Gimbal Mounting</b></font></a></p>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258156.pdf" target="_blank"><font color="#ACB0C3"><b>09P Bulletin Pedestal Style Mounting</b></font></a></p>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258157.pdf" target="_blank"><font color="#ACB0C3"><b>09PG Bulletin Pedestal Gimbal Mounting</b></font></a></p>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258159.pdf" target="_blank"><font color="#ACB0C3"><b>09Y Bulletin Overport Yoke Style</b></font></a></p>
			</div></div>
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Mixing-Tees.gif" alt="Eclipse Mixing Tees" title="Eclipse Mixing Tees"/></div>
			<div id="PartsContent"><h3>Mixing Tees</h3>
			<p>Eclipse Mixing Tees provide two valve control for the mixing of low pressure gas and air. The mixer consists of an Eclipse ALO-R valve, 
			with integral ratio adjuster for control of the gas, an air butterfly valve, and an air jet sized for air capacity required in the system. 
			Capacities will vary according to the gas and air pressures available and the allowable pressure drop. The jet size will be selected by 
			the factory, based on gas and air pressures and the Btu of the gas used.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162616.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>		
			</div></div>
<div id="35" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Minnox.gif" alt="Eclipse Minnox" title="Eclipse Minnox"/></div>
			<div id="PartsContent"><h3>Minnox</h3>
			<p>Now the world's best direct-fired heating system delivers lower emissions. The Eclipse Minnox gas burner exceeds all state, regional, 
			national and international emission standards for NOx and CO--less than 5 ppm NOx and less than 30 ppm CO at 3% 02. Even lower emissions 
			can be achieved under certain conditions! The Minnox allows Eclipse customers to use efficient direct-fired air heating to safely process 
			food products, malt, auto finishes, lumber, paper and other products with no performance penalty while holding the line on emissions regardless 
			of the emission standards in their country.
			<br/><br/>In the United States, installation of a Minnox system may allow your company to receive valuable emissions credits which you can sell 
			to other companies in your area. Minnox received the coveted DSM Environmental Technology Award, and is widely recognized as an engineering 
			breakthrough in the combustion industry.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161749.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="36" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Minimixers.gif" alt="Eclipse Minimixers" title="Eclipse Minimixers"/></div>
			<div id="PartsContent"><h3>Minimixers</h3>
			<p>The Eclipse Minimixer is a minature fan-type mixer fro producing an air/gas mixture and delivering it under pressure to a burner or series 
			of burners.
			<br/><br/>The Minimixers are normally used for small industrial heating applications or for piloting installations where a blast pilot is 
			desired and there is no other source of pressure air available.
			<br/><br/>Each Minimixer incorporates a small blower constructed of cast aluminum and employing a totally enclosed universal series wound 
			motor. Minimixers can be mounted in any desired position. The blower/mixer assembly can be rotated on the base to provide any desires mixture 
			outlet position. The gas inlet can be rotated in 90 degree increments, providing a choice of four different inlet positions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162618.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="37" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Mini_SS.gif" alt="Eclipse Mini Stainless" title="Eclipse Mini Stainless"/></div>
			<div id="PartsContent"><h3>Mini Stainless</h3>
			<p>The Exothermics Mini Stainless Steel Heat Exchanger is the answer to heat recovery needs in systems with low air volumes. Designed 
			specifically for systems with ACFM volumes of 300 to 5500 and temperatures up to 1200&deg;F (649&deg;C) , the Mini Heat Exchanger provides 
			optimum heat recovery.
			<br/><br/>The removable access cover on the exhaust airstream makes it possible to clean the unit and maintain peak effectiveness.
			<br/><br/>The Exothermics Mini Stainless Steel Heat Exchanger is a fully welded assembly that provides structural and airstream integrity. 
			This is accomplished with features such as a continuously welded primary seal that separates the two airstreams and fully welded end seals.
			<br/><br/>The Mini Heat Exchanger provides efficient heat recovery on multiple applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=287104.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="38" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/MarkIV.gif" alt="Eclipse Mark IV" title="Eclipse Mark IV"/></div>
			<div id="PartsContent"><h3>Mark IV</h3>
			<p>Eclipse Mark IV Burners are packaged, nozzle-mixing, combination gas/oil burners for use in process heating applications with specific 
			emphasis on ovens and dryers. Because it was created with the customer in mind, the Mark IV is one of our most convenient models to use and operate.
			<br/><br/>The Eclipse Mark IV burners can be used with any commercially available fuel gas and with fuel oils up to and including #2 oil. 
			After the complete burner package is bolted to an appliance, it is only necessary to connect the gas, oil, and atomizing air lines and wire 
			the terminal strip to a suitable control. (Control panels are also available from Eclipse to meet the particular needs of your application.)
			<br/><br/>The Mark IV burners have been used to dry paint (all colors), paper and paper products, cloth (all colors), food products, ink, 
			laminates, packaging film, carpets and a wide variety of other products. In addition, the Mark IV is capable of firing boilers, furnaces, 
			incinerators, metal smelters and other equipment requiring a combustion block. Four burner sizes are available with maximum capacities 
			from 1,000,000 to 5,000,000 Btu/hr.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161572.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="40" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Locktite_Manual_Reset_Gas_Shut-Off_Valves.gif" alt="Eclipse Locktite Manual Reset Gas Shut-Off Valves" title="Eclipse Locktite Manual Reset Gas Shut-Off Valves"/></div>
			<div id="PartsContent"><h3>Locktite Manual Reset Gas Shut-Off Valves</h3>
			<p>The Eclipse Inc. 200 LockTite valves are manually opening valves that automatically shut off the gas supply to a combustion system when any 
			interlocking limit switch opens. The valve handle cannot be reset until the condition causing the open switch is corrected and all limit switches 
			are closed. Typical limit switches include air pressure, gas pressure, temperature limit, and flame monitoring contacts.
			<br/><br/>In the event of a power failure, the valve immediately closes. Series 200 LT valves include two visual indicators of "open" or "closed" 
			position: a red "flag" visible in the plastic dome on top of the valve; and the reset handle position as indicated by cast markings on the operator 
			case. These valves also include two SPDT micro-switches that can be used for auxiliary circuits. Two contacts are made when the valve opens, and 
			two are made when the valve is closed.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162915.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="41" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/LineBurnerCross.gif" alt="Eclipse Line Burners" title="Eclipse Line Burners"/></div>
			<div id="PartsContent"><h3>Line Burners</h3>
			<p>Eclipse Line Burners are premix, retention type burners designed for applications where heat must be distributed over a large area. They are 
			especially suited for oven, kettle and either recirculating or nonrecirculating air heating applications. Several types of Line Burners sections 
			are available, making a variety of flame patterns possible. Capacities range from 19,000 to 336,000 Btu/hr. per lineal foot (based on 70% of required 
			air in premix) depending upon the mixture pressure used.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=268127.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="42" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/incinifume.gif" alt="Eclipse InciniFume" title="Eclipse InciniFume"/></div>
			<div id="PartsContent"><h3>InciniFume</h3>
			<p>The Eclipse InciniFume burner belongs to the Air Heat burner family and can be applied to a variety of high-temperature processes. It was 
			designed for trouble-free, high-temperature process air heating. Its design also design provides stable operation with a wide range of velocities, 
			inputs and fuels.
			<br/><br/>The InciniFume burner is modular in design and assembled from straight sections, tee sections and crosses to produce nearly any 
			configuration required. Each module consists of cast stainless steel bodies and divergent air wings made of Avesta 253MA. The burner bodies 
			supply fuel to the center of the air wings to control the air and fuel mixture inside the burner and optimize efficiency and emissions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161802.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="43" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Incinicone.gif" alt="Eclipse Incini-Cone" title="Eclipse Incini-Cone"/></div>
			<div id="PartsContent"><h3>Incini-Cone</h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Gas firing with interchangeable gas guns
			<br/>&#149; Insulated back-plate for cooler working 
			<br/>&nbsp;&nbsp;environment
			<br/>&#149; Fuel turndowns up to 20:1 depending on 
			<br/>&nbsp;&nbsp;the application
			<br/>&#149; Fume stream turndowns up to 2.45:1
			<br/>&#149; Concentric design insures even mixing and 
			<br/>&nbsp;&nbsp;combustion
			<br/>&#149; Compact design allows for easy burner 
			<br/>&nbsp;&nbsp;removal and access to incinerator
			<br/>&#149; Minimal maintenance 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162236.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="44" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ImmersoPak.gif" alt="Eclipse ImmersoPak" title="Eclipse ImmersoPak"/></div>
			<div id="PartsContent"><h3>ImmersoPak</h3>
			<p>Smooth, quiet performance, even during cold starts.
			<br/><br/>Completely redesigned for durability and ease of use.
			<br/><br/>Eclipse ImmersoPak (IP version 2) burners are easy to install, simple to operate, and offer long service life in industrial 
			environments. They are ideal for heating immersion tubes on cleaning tanks, spray washers, salt baths, quenching tanks, tempering tanks, 
			asphalt tanks and similar equipment. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162213.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="45" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ImmersoJet.gif" alt="Eclipse ImmersoJet" title="Eclipse ImmersoJet"/></div>
			<div id="PartsContent"><h3>ImmersoJet</h3>
			<p>Designed for high performance and convenience.
			<br/><br/>ImmersoJet burners fire at high capacities through a small diameter immersion tube. The combustion gases from the burner scrub 
			the inner tube surfaces to produce the highest heat transfer rate of any immersion burner available.
			<br/><br/>Other ImmersoJet benefits are:
			<br/><br/>&#149; Produces the industry's highest heat 
			<br/>&nbsp;&nbsp;capacities and efficiencies.
			<br/>&#149; Tube requirements save valuable space 
			<br/>&nbsp;&nbsp;inside the tank.
			<br/>&#149; Comes mounted with a reliable low or high 
			<br/>&nbsp;&nbsp;pressure blower for ease of installation.
			<br/>&#149; Quickly transfers heat to the immersion 
			<br/>&nbsp;&nbsp;tube, resulting in lower burner surface 
			<br/>&nbsp;&nbsp;temperature.
			<br/>&#149; Provides faster heat-up times than any 
			<br/>&nbsp;&nbsp;other immersion burner.
			<br/>&#149; Combustion chamber is outside the tank, 
			<br/>&nbsp;&nbsp;taking up less space and providing more 
			<br/>&nbsp;&nbsp;uniform heat.
			<br/>&#149; Unique nozzle design ensures quiet 
			<br/>&nbsp;&nbsp;operation. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162166.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="46" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Heat-up-Burner.gif" alt="Eclipse Heat-Up Burner" title="Eclipse Heat-Up Burner"/></div>
			<div id="PartsContent"><h3>Heat-Up Burner</h3>
			<p>The Eclipse portable Heat Up Burner package is designed to operate at 150% total air at maximum rating, and direct the fire to an area 
			selected by the operator by means of a stainless steel flame extension. It is normally used for controlled furnace heat-ups or cleaning of 
			regenerative furnace checkers.
			<br/><br/>The Heat-Up Burner package consists of:
			<br/><br/>&#149; The burner assembly with cone and flame 
			<br/>&nbsp;&nbsp;extension.
			<br/>&#149; The cart assembly with combustion air 
			<br/>&nbsp;&nbsp;blower train, natural gas train, and 
			<br/>&nbsp;&nbsp;electrical control panel complete with 
			<br/>&nbsp;&nbsp;flame safe-guard system.
			<br/>&#149; Interconnecting flexible air and gas hoses, 
			<br/>&nbsp;&nbsp;and electrical cables. 
			<br/><br/><br/>HEAT-UP BURNER FEATURES
			<br/><br/>&#149; Sizes to 24MM BTU/Hr. 
			<br/>&nbsp;&nbsp;(6.0 x 10(6) kcal/hr)
			<br/>&#149; Burner light-off by gas-pilot 
			<br/>&nbsp;&nbsp;ignition system
			<br/>&#149; Rated 150% total air at maximum firing 
			<br/>&nbsp;&nbsp;and over 3500% total air at minimum 
			<br/>&nbsp;&nbsp;firing Uses standard 15-25 psig 
			<br/>&nbsp;&nbsp;(1.0 -1.7 bars) gas inlet pressure
			<br/>&#149; Flame outlet extensions = 
			<br/>&nbsp;&nbsp;0", 30", 45", and 90"
			<br/>&#149; Manual/automatic temperature control 
			<br/>&nbsp;&nbsp;system
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258136.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="47" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/GT-Underport-Burner.gif" alt="Eclipse GT Underport Burner" title="Eclipse GT Underport Burner"/></div>
			<div id="PartsContent"><h3>GT Underport Burner</h3>
			<p>Underport firing allows a high degree of flame coverage of the melt using multiple burners mounted beneath the port sill, so providing easy access 
			for adjustment and maintenance.
			<br/><br/>Underport firing with the GT burner gives both high thermal efficiency and high melt rates with a variety of liquid fuels and gas. Using the 
			burner block sealing system, with integral low pressure air cooling, and with careful selection of burner system and furnace design, low levels of NOx 
			are achievable.
			<br/><br/>The GT oil and gas burner are fully interchangeable on the same mounting bracket. The burner support bracket ensures accurate and repeatable 
			burner location, simple adjustment, quick removal of burner, all of which are essential for an efficient underport burner system.
			</p>
			</div></div>
<div id="48" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/GT-CPA-Bracket.gif" alt="Eclipse GT Bracket" title="Eclipse GT Bracket"/></div>
			<div id="PartsContent"><h3>GT Bracket</h3>
			<p><b>Underport Burner Bracket</b>
			<br/><br/>Suitable for Eclipse GT Style Underport burners (GT/NG-DI, GT/NG, CT/CPA), this bracket allows for:
			<br/><br/>&#149; Angle adjustment without the need for 
			<br/>&nbsp;&nbsp;realignment of burner to burner block
			<br/>&#149; Quick and easy burner changeover
			<br/>&#149; Seven directions of adjustment and 
			<br/>&nbsp;&nbsp;alignment
			<br/>&#149; Robust construction for long operational 
			<br/>&nbsp;&nbsp;life, with minimum maintenance 
			<br/><br/>The bracket provides easy firing adjustment by a hand wheel, incorporating a self locking facility. As well as a clear and easy-to-read 
			firing angle indicator.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=273914.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="49" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/GT-CPA.gif" alt="Eclipse GT/CPA" title="Eclipse GT/CPA"/></div>
			<div id="PartsContent"><h3>GT/CPA</h3>
			<p><b>Underport Oil Firing System</b>
			<br/><br/>Underport firing allows a high degree of flame coverage of the melt using multiple burners mounted beneath the port sill so providing 
			easy access for adjustment and maintenance. Providing the furnace aerodynamics are correct underport firing gives both high thermal efficiency 
			and high melt raes with a variety of liquid fuels. Using the burner block sealing system, with internal low pressure air cooling, and with 
			careful selection of burner system and port design, low levels of NOx are achievable.
			<br/><br/>The Eclipse GT/CPA burner support bracket ensures accurate repeatable burner location, simple adjustment, quick removal of burner, 
			all of which are essential for an efficent underport burner system.
			<br/><br/>Eclipse provides advice on furnace aerodynamics, port design and auxiliary equipment as well as full supervision and commissioning services.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=273916.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="50" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/GT_NG-DI.gif" alt="Eclipse GT/NG-DI" title="Eclipse GT/NG-DI"/></div>
			<div id="PartsContent"><h3>GT/NG-DI</h3>
			<p><b>Dual impulse underport design allows adjustability of flame length and luminosity</b>
			<br/><br/>Underport firing allows a high degree of flame coverage of the melt by using multiple burners mounted beneath the port sill, 
			providing high thermal efficiency and easy access for adjustment and maintenance.
			<br/><br/>The Eclipse GT/NG Dual Impulse underport burner uses co-axial jets in which the center jet flow is adjustable by means of a 
			calibrated metering valve attached to the burner. By altering the gas to the center jet, the thrust of the burner can be controlled, 
			allowing the flame length to be varied by up to 30% without changing the nozzle and allowing easy optimization of the flame. This 
			arrangement also increases the flame luminosity, providing greater heat transfer to the glass and lower NOx.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Low NOx
			<br/>&#149; Adjustable flame length
			<br/>&#149; High luminosity
			<br/>&#149; Predictable and reproducable performance
			<br/>&#149; Burner sealing increases efficiency
			<br/>&#149; Simple, rapid and safe burner changing
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=276871.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="51" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Furnnox.gif" alt="Eclipse Furnnox" title="Eclipse Furnnox"/></div>
			<div id="PartsContent"><h3>Furnnox</h3>
			<p>Furnnox burners represent a technological leap forward in every area of design and performance. Furnnox burners set new standards for 
			emissions and flexibility, and tests show that the Furnnox delivers lower emissions than any of its competitor's comparable burners.
			<br/><br/>Integrated gas and air orifices simplify burner piping, set-up and adjustment. Air and gas inlets are independently adjustable 
			in 90 degree increments to suit a variety of piping alternatives. The Furnnox is made to last, rugged Eclipse dependability and reliability is built in.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=271279.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="52" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/PrimeFire300.gif" alt="Eclipse PrimeFire 300" title="Eclipse PrimeFire 300"/></div>
			<div id="PartsContent"><h3>PrimeFire 300</h3>
			<p>The Primefire 300 Series burner is a quantum leap in flame luminosity and glass bath coverage--providing:
			<br/><br/>&#149; <b>Fish-Tail or Fan-Shaped Flame:</b> Apart from providing increased load coverage, the Primefire 300 burner provides 
			uniform heat distribution that improves product quality, throughput, and extends furnace refractory life.
			<br/><br/>&#149; <b>Increased Flame Radiation:</b> In the visible light spectrum (500 to 2000 nanometers range), the Primefire 300 burner 
			will improve radiative heat transmission for the glass melting application and improve furnace efficiency.
			<br/><br/>&#149; <b>Adjustable Flame Shape:</b> An adjusting mechanism on the burner allows you to change the flame shape (overall length 
			and width) to suit the melter width and the required temperature profile.
			<br/><br/>&#149; <b>Extremely Low Mixing Rates of Oxygen and Fuel Streams:</b> The Primefire 300 burner creates a low momentum flame which 
			has a lower peak flame temperature. This should result in lower operating crown temperatures and lower volatile transport rates.
			<br/><br/>&#149; <b>Multi-Fuel Capability:</b> The same burner can fire natural gas or light/heavy fuel oil. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258093.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="53" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Forehearth-Burners.gif" alt="Eclipse Forehearth Burners" title="Eclipse Forehearth Burners"/></div>
			<div id="PartsContent"><h3>Forehearth Burners</h3>
			<p>Eclipse manufactures Series 0100 nozzles for use on most standard forehearth burners which use premixed combustion air and gas. The 
			nozzles are typically made of type 304 stainless steel. The stainless steel is rated good for approximately 1500 F (816 C) under continuous 
			operation without excessive scaling. Other materials are available as options.
			<br/><br/>Typical operating pressures at the nozzle are between .5 and 8 IWC (12.7 - 203.2 mmH20). At high temperatures, inter granular 
			attack is one of the main sources for corrosion, especially with the presence of sulfur. Type 446 stainless steel offers the best resistance 
			to inter granular attack. Type 316 stainless steel offers less resistance to inter granular attack, but it is rated better than the type 
			304 stainless steel.
			<br/><br/>If the presence of sulfur or other aggressive agents (i.e., chloride) is not predominating, then type 316 stainless steel would 
			be adequate for the application. A special quote is required for 446S/S for 5/8" (15.875 mm) diameter, due to material availability and the higher cost.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=282953.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="54" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/FlueFire.gif" alt="Eclipse FlueFire" title="Eclipse FlueFire"/></div>
			<div id="PartsContent"><h3>FlueFire</h3>
			<p>The Eclipse FlueFire is specially designed for supplemental firing of cogeneration and combined-cycle installations. In power generation 
			stations, the exhaust gases from gas turbines contain a significant quantity of energy in the form of heat. This energy can be used either 
			directly in heating processes or to generate steam, which can again drive steam turbines to generate more electricity.
			<br/><br/>When the heat requirement is greater than that available from the turbine exhaust gases, supplementary firing can be applied. 
			Eclipse has designed the FlueFire burner specifically for this purpose. It is an in-duct burner which can be located directly in the exhaust 
			gases between the turbine and waste heat boiler. The burner is also suitable for fresh air operation or incineration applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161789.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div> 
<div id="55" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Extern-a-Therm-Recuperator.gif" alt="Eclipse Extern-a-Therm Recuperator" title="Eclipse Extern-a-Therm Recuperator"/></div>
			<div id="PartsContent"><h3>Extern-a-Therm Recuperator</h3>
			<p>Eclipse Extern-a-Therm recuperators are tubular air-to-air heat exchangers designed to recover waste heat from industrial exhaust gases. 
			The heat recovered is used to preheat combustion air for the system's burners, which increases fuel efficiency.
			<br/><br/>Our customers have measured pre-heated air temperatures of 700&deg;F. This indicates a fuel savings of 20%. Which means an 
			Extern-a-Therm recuperator can pay for itself in less than two years, based on the amount of fuel savings at a cost of $7.00 per 1,000 
			cubic feet of natural gas.
			</p>
			</div></div>      
<div id="56" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ExtensoJet.gif" alt="Eclipse ExtensoJet" title="Eclipse High ExtensoJet"/></div>
			<div id="PartsContent"><h3>ExtensoJet</h3>
			<p>The ExtensoJet is a nozzle-mix burner that is designed to fire an intense stream of hot gases through a silicon tipped extended combustor 
			using ambient combustion air.
			<br/><br/>The ExtensoJet was designed to extend through the kiln wall to significantly increase hot gas penetration and circulation of the 
			products of combustion.
			<br/><br/>The high velocity of the gases improves temperature uniformity, product quality and system efficiency. The gas velocity can be as 
			high as 500/ft/s. The combustor comes in varying lengths from 20 inches to 56 inches.
			<br/><br/>The burner features:
			<br/><br/>&#149; On-Ratio control
			<br/>&#149; Direct Spark Ignition
			<br/>&#149; Flame Rod Supervision
			<br/>&#149; Multiple Fuel Capability 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=271277.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="57" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/ERHeater.gif" alt="Eclipse ER Indirect Air Heater" title="Eclipse ER Indirect Air Heater"/></div>
			<div id="PartsContent"><h3>ER Indirect Air Heater</h3>
			<p>The Eclipse ER Indirect Air Heater provides all of the benefits of an indirect air heater - the physical separation/isolation of the 
			products of combustion from the process - along with the energy efficiencies provided by a fully-integrated Exothermics heat recovery system.
			<br/><br/>Eclipse ER Indirect Air Heaters produce clean hot air, free of combustion by-products. They are ideal for heating and drying applications 
			that require contaminant free process air. ER Indirect Air Heaters deliver the lowest emissions of any indirect air heaters available.
			<br/><br/>Typical applications include:
			<br/><br/>&#149; Dairy Spray Dryers
			<br/>&#149; Pharmaceutical Spray Dryers
			<br/>&#149; Chemical Dryers
			<br/>&#149; Drying Ovens
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=297042.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="58" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/E-Jector.gif" alt="Eclipse E-Jector" title="Eclipse E-Jector"/></div>
			<div id="PartsContent"><h3>E-Jector</h3>
			<p>The Eclipse E-Jector emission reduction device is an easy to install flange-mounted device for use in radiant tube diameters of 4" up to 6" 
			with maximum capacities of up to 350,000 BTU/hr (102,5 kW). The design of this device provides significant reductions in NOX emissions. In field 
			trials, NOX reductions were achieved of more than 50% over existing emission levels. The E-Jector is designed to be used with Eclipse Bayonet 
			style Recuperators allowing for high fuel efficiencies while keeping NOX under control.
			<br/><br/>Every burner system that includes an E-Jector is custom engineered by Eclipse to maximize performance and assure compatibility with the 
			radiant tube.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162078.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="59" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/EHSeriesLanceBurner.gif" alt="Eclipse EH Series Lance Burner" title="Eclipse EH Series Lance Burner"/></div>
			<div id="PartsContent"><h3>EH Series Lance Burner</h3>
			<p>The EH Series Lance Burner offers the best solution available for continuous kiln heating furnaces for bricks, roof tiles and rough ceramic 
			products. It is ideal for continuous kilns with roof top installations and furnace zones with operating temperatures above 750&deg;C. The lance 
			design provides a flame that is remote from the burner for thick furnace wall applications. EH is a nozzle mixing burner that can fire natural 
			gas, LPG, butane and other types of fuel gas. EH does not require an ignition device or a flame safety system because it is installed where the 
			operating temperature is above 750&deg;C. The EH burner is available in one size with various lengths, depending of the furnace wall thickness.
			</p>
			</div></div>
<div id="60" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Eclipse-Float-Fire-System.gif" alt="Eclipse Float Fire" title="Eclipse Float Fire"/></div>
			<div id="PartsContent"><h3>Float Fire</h3>
			<p>Eclipse has successfully installed Tin Bath Auxiliary Heating Systems, called the "Float Fire", in several glass plants. Currently, several 
			prominent Eclipse glass customers are operating the Float Fire Auxiliary Heating System in their Tin Baths with excellent results.
			<br/><br/>The purpose of the Float Fire Auxiliary Heating System is threefold. First, as the Tin Bath ages, problems can frequently arise 
			with the failure of electric heating elements and/or electrode control systems. Second, additional heat is often needed after the coating process. 
			Third, there is a need for improved edge heating capabilities due to changes in job specifications or edge thickness. In all of these cases, 
			the application of the Float Fire Auxiliary Heating System has solved these operating issues and improved the control of the heating process.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=288556.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="62" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/DIR.gif" alt="Eclipse Dimple Insulated" title="Eclipse Dimple Insulated"/></div>
			<div id="PartsContent"><h3>Dimple Insulated</h3>
			<p>Exothermics' Air-to-Air Insulated Dimple Plate Heat Exchangers / Recuperators are designed specifically for high temperature (1200-1550&deg;F) 
			applications. They are available in a wide variety of sizes and flow configurations (e.g. counter-flow or cross-flow) to satisfy your needs whether 
			you are recovering heat from your ovens, preheating combustion air, or running a catalytic or thermal oxidizer.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 201_225 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 201_225 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="63" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Dimple.gif" alt="Eclipse Dimple Plate Heat Exchangers" title="Eclipse Dimple Plate Heat Exchangers"/></div>
			<div id="PartsContent"><h3>Dimple Plate Heat Exchangers</h3>
			<p>Exothermics' Dimple Plate Heat Exchangers, like all of our exchangers, are fully-welded and can be manufactured in a wide variety of sizes. 
			They can also be configured for counter-flow or cross-flow operation. In these exchangers the parallel plates are formed with either staggered 
			or inline dimple patterns to reduce the potential for particulate fouling.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=287110.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="64" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/DeepSpiralFlame.gif" alt="Eclipse Deep Spiral Flame (DSF/DSF-CGO)" title="Eclipse Deep Spiral Flame (DSF/DSF-CGO)"/></div>
			<div id="PartsContent"><h3>Deep Spiral Flame (DSF/DSF-CGO)</h3>
			<p>The Eclipse Spiral Flame and Dual-Fuel Flame Burners are a sealed nozzle mixing flat flame burner. Due to its unique construction, the 
			flame follows the contour of the combustion block and surrounding furnace wall with virtually no forward velocity. This permits the burner 
			to be placed close to the work load without producing flame impingement or hot spots in front of the burner and provides uniform heating over 
			a large area.
			<br/><br/>Eclipse Spiral Flame and Dual-Fuel Spiral Flame burners are used not only on new furnaces, but to increase work capacity on existing 
			installations. Since work load can be placed proportionately closer to the burners, a greater portion of the furnace chamber can be used.
			<br/><br/>&#149; Little forward flame travel
			<br/>&#149; Easy fuel changeover
			<br/>&#149; Provision for piloting and flame monitoring
			<br/>&#149; Can operate with excess fuel or excess air
			<br/>&#149; Short flame travel permits full use of 
			<br/>&nbsp;&nbsp;furnace space
			<br/>&#149; Evenly distrbuted radiation from 
			<br/>&nbsp;&nbsp;combustion block and wall promotes 
			<br/>&nbsp;&nbsp;temperature uniformity
			<br/>&#149; Turndowns up to 10:1 depending on 
			<br/>&nbsp;&nbsp;model
			<br/>&#149; Spiral flame pattern maintained at all 
			<br/>&nbsp;&nbsp;firing rates
			<br/>&#149; Exceptional flame stability 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161873.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="65" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Cross-Flow-Recuperators.gif" alt="Eclipse Cross Flow Recuperators" title="Eclipse Cross Flow Recuperators"/></div>
			<div id="PartsContent"><h3>Cross Flow Recuperators</h3>
			<p>Eclipse Cross-Flow recuperators are compact tubular air-to-air heat exchangers designed to recover waste heat from industrial exhaust gases. 
			The recovered heat is used to preheat combustion air for the system's burners, thereby increasing fuel efficiency.
			<br/><br/>Other advantages include:
			<br/><br/>&#149; Single-ended design allows free expansion 
			<br/>&nbsp;&nbsp;of recuperator tubes; no expansion joints 
			<br/>&nbsp;&nbsp;required.
			<br/>&#149; Compact, durable construction.
			<br/>&#149; Low pressure drops.
			<br/>&#149; Integral insulated enclosure.
			<br/>&#149; Easy installation.
			<br/>&#149; Wide range of inputs. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162275.pdf" target="_blank"><font color="#ACB0C3"><b>V1 CFR021 Datasheet 530-1 </b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162276.pdf" target="_blank"><font color="#ACB0C3"><b>V1 CFR048 Datasheet 530-2</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162277.pdf" target="_blank"><font color="#ACB0C3"><b>V1 CFR080 Datasheet 530-3</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162278.pdf" target="_blank"><font color="#ACB0C3"><b>V1 CFR121 Datasheet 530-4</b></font></a></p>
			</div></div>
<div id="66" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/BV-Pak_Packaged_Butterfly_Valves.gif" alt="Eclipse BV-Pak Packaged Butterfly Valves" title="Eclipse BV-Pak Packaged Butterfly Valves"/></div>
			<div id="PartsContent"><h3>BV-Pak Packaged Butterfly Valves</h3>
			<p>The Eclipse Packaged Butterfly Valve incorporates an Eclipse Trilogy T500 Series Actuator mounted to a standard Eclipse butterfly valve. 
			This package is the perfect solution for industrial burner and damper control. Eclipse simplifies selection and ordering in this single source 
			component purchase. Equipment sizing, mounting, and setup are all made easier.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=292905.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="67" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Butterfly_Valves.gif" alt="Eclipse Butterfly Valves" title="Eclipse Butterfly Valves"/></div>
			<div id="PartsContent"><h3>Butterfly Valves</h3>
			<p>Eclipse Butterfly Valves are designed to control air and gas flow to all types of combustion systems. They should not be used as 
			tight shut-off valves. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162780.pdf" target="_blank"><font color="#ACB0C3"><b>Linkage Adjustment Information Guide720</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162782.pdf" target="_blank"><font color="#ACB0C3"><b>Installation of Rotary Actuators Installation Guide 720</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=164521.pdf" target="_blank"><font color="#ACB0C3"><b>Hot Air Butterfly Valves Series HBV Bulletin 722</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258202.pdf" target="_blank"><font color="#ACB0C3"><b>Combustion Tec Hot Air Butterfly Valve's Bulletin 21</b></font></a>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=271206.pdf" target="_blank"><font color="#ACB0C3"><b>Butterfly Valves Information Guide 720-3</b></font></a></p>
			</div></div>
<div id="68" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Bulletin-VAC.gif" alt="Eclipse VAC Burner" title="Eclipse VAC Burner"/></div>
			<div id="PartsContent"><h3>VAC Burner</h3>
			<p>The VAC series burner is based on the popular Eclipse AH-MA series packaged burner design. It delivers extremely clean combustion, while 
			operating completely independent of the process air flow. This allows it to perform well in all types of furnaces and dryers, even in conditions 
			with high humidity, very low oxygen concentrations or variations in air volume. The burner can be mounted in a duct, on the wall or inside the 
			furnace or oven. VAC delivers a stable flame and provides complete combustion at all firing ranges, with excellent temperature uniformity. 
			Thanks to its modular design, VAC burners are available in a wide variety of sizes, based on customer requirements. 
			</p>
			</div></div>
<div id="69" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/BrightFire.gif" alt="Eclipse BrightFire" title="Eclipse BrightFire"/></div>
			<div id="PartsContent"><h3>BrightFire</h3>
			<p>The Eclipse Inc. Brightfire burner is an adjustable low NOx air-fuel burner. The Brightfire provides up to 35 percent lower NOx emissions 
			and superior flame control compared to other burners. In addition, the Brightfire burner provides a wide turndown ratio and its sleek design 
			will accommodate the tightest port constraints and provide years of trouble-free service.
			<br/><br/>The burner's low NOx emissions and superior flame control are accomplished through a combination of coaxial annular and central 
			core flow streams. The momentum flux (mass flow rate x velocity) of the center jet is always dominant over the surrounding annular jet, 
			except at the burner's minimum velocity setting, when the momentum flux are equal. The Brightfire's unique design varies the momentum flux 
			of the individual jets by diverting gas flow from one jet to the other while changing the nozzle's total flow area. That is, as the center 
			jet's momentum flux decreases, the annular jet's momentum flux increases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=257877.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="70" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/BlastTips.gif" alt="Eclipse Blast Tips" title="Eclipse Blast Tips"/></div>
			<div id="PartsContent"><h3>Blast Tips</h3>
			<p>Blast Tip is the name given any small burner to handle combustible mixtures of gas and air under sufficient pressure to cause a blast-like 
			flame. In general, Blast Tips are built to burn without the use of any refractory tunnel and are either self-piloting or built to prevent the 
			flame from blowing away uder normal conditions of operation.
			<br/><br/>Blast Tips are suitable for a variety of applications. They can be used in groups to heat a wide area such as the bottom of a kettle, 
			or mounted down a length of pipe as in a core oven.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=164477.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="71" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Blast.gif" alt="Eclipse Blast Gate Valve" title="Eclipse Blast Gate Valve"/></div>
			<div id="PartsContent"><h3>Blast Gate Valve</h3>
			<p>Eclipse Blast Gates are sturdy, inexpensive manual control valves for use with low pressure air up to 5 psig.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162800.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="72" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Bayonet-Ultra_Recuperator.gif" alt="Eclipse Bayonet-Ultra Recuperator" title="Eclipse Bayonet-Ultra Recuperator"/></div>
			<div id="PartsContent"><h3>Bayonet-Ultra Recuperator</h3>
			<p><b>Bayonet-Ultra Recuperator Worksheet</b>
			<br/><br/>Exclusive multi-tube design delivers fuel savings up to 30%!
			<br/><br/>&#149; Bayonet-Ultra recuperators are high efficiency heat exchangers designed to fit into the exhaust leg of single, U, O, W or 
			Trident-type radiant tubes. They're frequently paired with Tube Firing burners and suitable for use with exhaust streams up to 2100 Degrees 
			Fahrenheit provide fuel savings up to 30%.
			<br/><br/>&#149; Conventional recuperators have only one heat transfer tube. Each Bayonet-Ultra contains multiple tubes to dramatically increase 
			the heat transfer area while significantly decreasing the amount of fuel required to maintain required heat levels. Plus, an air cooled housing 
			cools the work area. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162062.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="73" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Bayonet-Recuperator.gif" alt="Eclipse Bayonet Recuperator" title="Eclipse Bayonet Recuperator"/></div>
			<div id="PartsContent"><h3>Bayonet Recuperator</h3>
			<p>Bayonet recuperators are low cost conventional heat exchangers designed to fit preheated combustion air to the burner. They're frequently 
			paired with tube firing burners to provide fuel savings up to 30%. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162034.pdf" target="_blank"><font color="#ACB0C3"><b>V1 Bayonet Recuperator BR4 Datasheet 317-1</b></font></a>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162035.pdf" target="_blank"><font color="#ACB0C3"><b>V1 Bayonet Recuperator BR5 Datasheet 317-2</b></font></a>
			<br/><a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162036.pdf" target="_blank"><font color="#ACB0C3"><b>V1 Bayonet Recuperator BR6 Datasheet 317-3</b></font></a></p>
			</div></div>
<div id="74" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/AutoTite_Gas_Shut-Off_Valves.gif" alt="Eclipse AutoTite Gas Shut-Off Valves" title="Eclipse AutoTite Gas Shut-Off Valves"/></div>
			<div id="PartsContent"><h3>AutoTite Gas Shut-Off Valves</h3>
			<p>AutoTite shut-off valves set the industry standard with broad NEMA approval.
			<br/><br/>Engineered and built with the precision you've come to expect from Eclipse, each AutoTite 2000AT valve is Performance Certified 
			- 100% factory tested before shipment.
			<br/><br/>For medium pressure range applications, you can't find automatic gas shut-off valves with more installation flexibility, 
			operational conveniences, and peace-of-mind safety features. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162919.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="75" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Automotive-Filter-for-Eclipse-Blowers.gif" alt="Eclipse Automotive Filters for Eclipse Blowers" title="Eclipse Automotive Filters for Eclipse Blowers"/></div>
			<div id="PartsContent"><h3>Automotive Filters for Eclipse Blowers</h3>
			<p>Eclipse automotive filters can be mounted in place of the standard grill on all Eclipse SMJ Blowers to clean excess dust and dirt from the combustion 
			air and keep the blower rotor clean.
			<br/><br/>Not only do these filters meet most auto plant specifications, but they have a wide variety of applications wherever SMJ blowers are used, 
			making them a great addition to any systems package. They are also replaceable and easy to clean.
			<br/><br/>Add an Eclipse Automotive Filter to your system and see how a filter can help your business thrive.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=269603.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="76" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Automatic-Operated-Valves.gif" alt="Eclipse Automatic Operated Valves" title="Eclipse Automatic Operated Valves"/></div>
			<div id="PartsContent"><h3>Automatic Operated Valves</h3>
			<p>Specifically designed for use with oil burners applied to glass furnaces with reversal systems. At the completion of the firing cycle, 
			this valve will, (a) shut-off the oil flow to the burner and, (b) extract or withdraw the oil in the burner back into the valve oil storage 
			chamber. This prevents the burner from after-burning, and dripping or cooking at the end of reversal, thus protecting the melt and the 
			furnace. Use one valve per burner for best results.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258205.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="77" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Atmospheric-Injectors.gif" alt="Eclipse Atmospheric Injectors" title="Eclipse Atmospheric Injectors"/></div>
			<div id="PartsContent"><h3>Atmospheric Injectors</h3>
			<p>Simple, low cost, adjustable mixers designed to provide an air/gas mixture to one or more burners. Atmospheric injectors use the kinetic 
			energy of a stream of gas to entrain part or all of the air required for combustion.
			<br/><br/>Two series of injectors are available: "Low Pressure," for use with gases up to 28" w.c. and "High Pressure," for gases from 1 psig 
			to 30 psig. Both are available as either "Single Stage" or "Compound" units depending on the type of gas used. Compound Injectors employ an 
			additional sleeve which assists in entrainment for higher Btu gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=298045.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="78" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Alum_air2.gif" alt="Eclipse Aluminum Air-to-Air" title="Eclipse Aluminum Air-to-Air"/></div>
			<div id="PartsContent"><h3>Aluminum Air-to-Air</h3>
			<p>The Exothermics Aluminum Air-to-Air Heat Exchanger is a counter-flow design allowing for two segregated air streams to flow through the 
			parallel plates within the exchanger. These exchangers utilize a state-of-the-art sinusoidal plate-type design for maximum heat transfer efficiency.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=287098.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="79" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Airjectors.gif" alt="Eclipse Airjectors" title="Eclipse Airjectors"/></div>
			<div id="PartsContent"><h3>Airjectors</h3>
			<p>The Eclipse Airjector uses compressed air to entrain larger volumes of ambient air for combustion systems. The Airjector is useful when 
			the operation of the combustion equipment is intermittent or air requirements are very small. A blower is recommended for constant operation.
			<br/><br/>Manual temperature control is achieved by a needle valve in the compressed air line. Automatic control can be achieved by 
			installing a solenoid valve to provide "on-off" control, while a solenoid valve with adjustable bypass will give "hi-lo" burner control.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162573.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="80" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Airheat.gif" alt="Eclipse AirHeat" title="Eclipse AirHeat"/></div>
			<div id="PartsContent"><h3>AirHeat</h3>
			<p>Simple, long-lasting operation plus much lower CO emissions than other competitive air heating burners.
			<br/><br/>Eclipse AirHeat Burners are line type burners ideal for generating volume of clean, hot air. Applications include oven, dryers, 
			fume incinerators, and similar industrial equipment.
			<br/><br/>All models feature an integral combustion air blower mounted on the burner's steel case. By supplying the correct air volume and 
			pressure to the burner, the blower allows stable operation over a wide range of duct velocities without installing a profile plate around 
			the burner. The burner can also be ordered without a blower for use in recirculating air applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://www.eclipsenet.com/products/listdocs.aspx?pid=159487.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="81" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/AHSideplate.gif" alt="Eclipse Air Heat Side Plates" title="Eclipse Air Heat Side Plates"/></div>
			<div id="PartsContent"><h3>Air Heat Side Plates</h3>
			<p>Eclipse Side Plate Assemblies consist of an industry proven Air Heat Burner pre-piped and mounted on a steel plate for easy installation 
			in a duct wall. All supply connections and pressure taps are conveniently located on the side plate, simplifying system piping and wiring.
			<br/><br/>Side Plate Assemblies include a burner, integral pilot, ignition plug, flame rod, low fire air bypass with check valve, adjustable 
			pilot cock, and combustion air blower.
			<br/><br/>Standard Sideplates are designed to fire horizontally with air flow from left to right when facing the gas piping. If necessary, 
			other arrangements can be supplied.
			<br/><br/>To further simplify installation, Eclipse can supply packaged valve trains for Sideplates. Depending on shipping considerations, 
			valve trains can be factory mounted and pre-wired to a terminal strip or panel. Call Eclipse for assistance in designing a package to suit your needs.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161603.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>  
<div id="82" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/AH-MA.gif" alt="Eclipse AH-MA" title="Eclipse AH-MA"/></div>
			<div id="PartsContent"><h3>AH-MA</h3>
			<p>Eclipse AH-MA v2.20 Air Heat burners produce a uniform, odorless, and smokeless flame ideal for heating fresh air in make-up and process air 
			heating applications. The AH-MA design provides stable operation over a wide range of velocities, inputs and fuels.
			<br/><br/>AH-MA v2.20 burners are line type burners constructed of cast iron or aluminum burner bodies and diverging stainless steel air wings. 
			The burner bodies supply fuel to the center of the air wings to control the air and fuel mixture inside the burner and to optimize emissions and 
			efficiency. Completely corrosion resistant design options are available using aluminum burner bodies or electroless nickel plated cast iron 
			burner bodies.
			<br/><br/>The AH-MA v2.20 Air Heat burner is assembled from straight sections, tees, and crosses to produce nearly any configuration required. 
			Large burners can be built as a combination of staged, individually controlled sections to increase turndown.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161778.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="83" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/AH-Duct.gif" alt="Eclipse Air Heat Duct Units" title="Eclipse Air Heat Duct Units"/></div>
			<div id="PartsContent"><h3>Air Heat Duct Units</h3>
			<p>Eclipse Duct Units are designed to produce large volumes of hot air for industrial processes such as drying, baking, or curing. Duct Units 
			consist of an Eclipse "AH" or "RAH" AirHeat burner mounted in a duct section complete with an IRI type gas valve train. Because they are completely 
			packaged, Duct Units are easy to install, connect and operate.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=161602.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="84" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Adjustable-Orfice-Gas-Cocks.gif" alt="Eclipse Adjustable Orifice Gas Cocks" title="Eclipse Adjustable Orifice Gas Cocks"/></div>
			<div id="PartsContent"><h3>Adjustable Orifice Gas Cocks</h3>
			<p>Lever Handled Brass gas cocks include adjustable orifice for gas, propane, & butane application 1/4" to 1/2" NPT.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162798.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="85" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/Adjustable-Limiting-Orfice.gif" alt="Eclipse Adjustable Limiting Orifice" title="Eclipse Adjustable Limiting Orifice"/></div>
			<div id="PartsContent"><h3>Adjustable Limiting Orifice</h3>
			<p>Eclipse ALO-R Valves are needle and seat type valves designed to provide accurate gas flow adjustment when installed in gas lines feeding nozzle 
			mix burners. Eclipse ALO Valves are recommended for installation in gas lines feeding nozzle mix burners as a means of gas flow adjustment for the 
			desired air/gas ratio.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=162796.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a>
			</p>
			</div></div>
<div id="86" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/0400(04V).gif" alt="Eclipse 0400(04V)" title="Eclipse 0400(04V)"/></div>
			<div id="PartsContent"><h3>0400(04V)</font></h3>
			<p>Recuperative or Direct-Fired Furnaces Nozzle Mix Burner.
			<br/><br/>The Eclipse Inc. Series 0400 hot air nozzle-mixing burners are suitable for many applications ranging from small glass day tanks, float 
			glass furnace working ends, refiners and distributors, to large multi-burner recuperative furnaces.
			<br/><br/>A recuperator or other means provides preheated combustion air to the burner. Using combustion air preheating will lead to increased fuel 
			savings. The Series 0400 burners can be either side or end fired on several types of continuous furnaces. Any clean industrial gas and/or grade of 
			fuel oil of suitable viscosity may be burned.
			<br/><br/><b>Features:</b> Hot/Cold Air - Combination Fuel Oil & Gas
			<br/><br/>&#149; Recuperative Furnace Operation
			<br/>&#149; Long life stainless steel construction
			<br/>&#149; Standard models to 1200 Degrees F 
			<br/>&nbsp;&nbsp;(649&deg;C), special models to 1400&deg;F 
			<br/>&nbsp;&nbsp;(760&deg;C)
			<br/>&#149; Improved adjustable gas orifice for better 
			<br/>&nbsp;&nbsp;flame shape control
			<br/>&#149; Oil uses atomizing air or steam to 40 psig 
			<br/>&nbsp;&nbsp;(2.76 bars)
			<br/>&#149; Variable oil flame shaping capability
			<br/>&#149; Pre-heated combustion air provides fuel 
			<br/>&nbsp;&nbsp;savings up to 45%
			<br/>&#149; Adjustable air for additional flame shape 
			<br/>&nbsp;&nbsp;control or capacity requirements. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258122.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="87" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/03V.gif" alt="Eclipse 03V" title="Eclipse 03V"/></div>
			<div id="PartsContent"><h3>03V</h3>
			<p>This adjustable gas orifice burner is called the "V" series since some of its features are adapted from a venturi syle nozzle. This design has 
			good flow characteristics and the reduced pressure losses in the nozzle mean greater kinetic energies are transferred to the jet stream of gas. 
			This, combined with higher aspect ratios on the gas annulus, enhance the stability and control of velocity resulting in excellent control of flame 
			length and flame shape.
			<br/><br/>Since gas velocity requirements vary from furnace to furnace, nozzle size selection is very important. The following sections on Gas 
			Firing, Flame Shape Control Factors Affecting Burner Capacity, and Burner Selection will be addressed for different capacity ranges.
			<br/><br/>Burner turndown normally encompasses either 2.5:1 or 3:1 gas flow operating ranges, and allows optimum burner operation. For operating 
			conditions not covered, consult your Eclipse Sales Engineer.
			<br/><br/>The burner design entails a simple construction for ease of burner part serviceability or replacement. Construction materials for the 
			gas burner are of carbon steel, ductile iron, and stainless steel. Items exposed to moderate heat are made of ductile iron, while items which 
			are directly exposed to furnace radiation are made of stainless steel.
			<br/><br/>Special features include:
			<br/><br/>&#149; Regernative furnace application
			<br/>&#149; "Sealed-in" firing for increased burner 
			<br/>&nbsp;&nbsp;efficiency
			<br/>&#149; Underport, overport, and sideport burner 
			<br/>&nbsp;&nbsp;firing
			<br/>&#149; Improved adjustable gas orifice for flame 
			<br/>&nbsp;&nbsp;shape control
			<br/>&#149; Gas to 5 psig (0.4 bar)
			<br/>&#149; Oil uses atomizing air or steam to 40 psig 
			<br/>&nbsp;&nbsp;(2.76 bars) 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258120.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="88" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/03FA.gif" alt="Eclipse 03FA" title="Eclipse 03FA"/></div>
			<div id="PartsContent"><h3>03FA</h3>
			<p>This medium pressure adjustable atomizing (2 to 40 PSIG air) orifice type, nozzle mixing oil burner is used extensively on glass regenerative 
			furnaces, hot combustion air being supplied from the regenerator.
			<br/><br/>Flame shapes are generally long and luminous but can be adjusted to shorter, sharper shape with the help of an adjustment mechanism 
			available on the burner.
			<br/><br/>The burner is made to burn any grade of fuel oil. When burning heavier oils, they should be preheated to obtain an oil viscosity of 120 SSU or less.
			<br/><br/>The burner can be fired in any standard position, i.e., overport, underport, etc. A side-of-port firing would require special consideration.
			<br/><br/>Special features include:
			<br/><br/>&#149; Regenerative furnace application
			<br/>&#149; Underport, overport, and sideport burner 
			<br/>&nbsp;&nbsp;firing
			<br/>&#149; Adjustable flame length and shape control
			<br/>&#149; Efficient mechanical and air-assist 
			<br/>&nbsp;&nbsp;atomization to 40 PSIG(2.76 bars)
			<br/>&#149; "Sealed-in" firing principle for greater fuel 
			<br/>&nbsp;&nbsp;efficiency 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258116.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div id="89" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/03f.gif" alt="Eclipse 03F" title="Eclipse 03F"/></div>
			<div id="PartsContent"><h3>03F</h3>
			<p>This medium pressure atomizing (5-40 PSIG air) nozzle mixing oil burner is used extensively on glass regenerative furnaces, hot combustion air 
			being supplied from the regenerator. It can also be used with gas burners for a dual fuel combination, or with hot or cold combustion air bodies 
			for recuperative or other type furnaces.
			<br/><br/>The Eclipse 03F burner is constructed to withstand a hot, dirty environment with minimal maintenance and a long life. The burner body 
			and oil boss are carbon steel and the oil tube assembly is stainless steel. The exposed venture atomizer is of premium heat resistant stainless 
			steel to withstand severe radiation. The oil boss and tube assembly has centering fins for easy insertion within the atomizing air tube assembly.
			<br/><br/>This particular model is a twin fluid nozzle mixing burner. It is designed so there is no interaction between the atomizing air and oil, 
			therefore the oil or atomizing air can be adjusted over the entire burner operating range, without affecting the other fluid.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=258117.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div> 
<div id="90" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/3D-Linnox.gif" alt="Eclipse Linnox" title="Eclipse Linnox"/></div>
			<div id="PartsContent"><h3>Linnox</h3>
			<p><b>Ultra-low emission technology delivers clean combustion and excellent heat distribution.</b>
			<br/><br/>The Eclipse Linnox burner is designed for direct fired air heating where ultra-low amounts of NOx and CO are required to meet legislative 
			and/or process requirements. Linnox burner operation is based on a high excess air pre-mix combustion to keep the flame temperatures low while the 
			burner geometry establishes an internal recirculation flame pattern. This allows for very low emissions at a high turndown rate (10:1) while maintaining 
			extremely stable combustion. The emissions in the undiluted flue gases are less than 15 ppm NOx at nominal input and less than 100 ppm CO, corrected 
			to 3% oxygen and firing on natural gas.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=290328.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div> 
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Eclipse/WTPUG.gif" alt="Eclipse WTPUG" title="Eclipse WTPUG"/></div>
			<div id="PartsContent"><h3>WTPUG</h3>
			<p><b>Water cooled throughport burners for regenerative glass furnaces.</b>
			<br/><br/>Eclipse WTPUG burners are compact, water-cooled burners designed to be inserted into the 
			port neck of a regenerative glass furnace. The burner is fitted to the Eclipse Retraction 
			Mechanism to withdraw the burner from the furnace when not firing. (See Bulletin 1140C for 
			further information.) Use of the Retraction Mechanism ensures consistent combustion quality, 
			which is essential to achieve high glass quality. The burner produces a conical flame, which 
			is ideal for low-flow ports in a glass furnace.
			<br/><br/><b>Features</b>
			<br/><br/>&#149; Low maintenance
			<br/>&#149; Fits existing retraction gear
			<br/>&#149; Ideal for low flow ports
			<br/>&#149; Compact design 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://riveon.eclipsenet.com/parkstreet/public/ViewFile.aspx?aid=276874.pdf" target="_blank"><font color="#ACB0C3"><b>Bulletin</b></font></a></p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/WTPUG_Thumbnail.gif" border="0" alt="Eclipse WTPUG" title="Eclipse WTPUG" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/WRASP_DI_Thumbnail.gif" border="0" alt="Eclipse WRASP-DI" title="Eclipse WRASP-DI" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Winnoxv2_2010_Thumbnail.gif" border="0" alt="Eclipse Winnox" title="Eclipse Winnox" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/WGD_Thumbnail.gif" border="0" alt="Eclipse WGD" title="Eclipse WGD" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Vortometric_Thumbnail.gif" border="0" alt="Eclipse Vortometric" title="Eclipse Vortometric" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Vari-Set-Mixers_Thumbnail.gif" border="0" alt="Eclipse Vari-Set Mixers" title="Eclipse Vari-Set Mixers" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/TubularPhoto2_Thumbnail.gif" border="0" alt="Eclipse Tubular" title="Eclipse Tubular" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ThermJet-Self-Recuperative-Burner_Thumbnail.gif" border="0" alt="Eclipse ThermJet Self-Recuperative Burner" title="Eclipse ThermJet Self-Recuperative Burner" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ThermJet-Liquid_Thumbnail.gif" border="0" alt="Eclipse ThermJet Liquid" title="Eclipse ThermJet Liquid" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ThermJet_Thumbnail.gif" border="0" alt="Eclipse ThermJet" title="Eclipse ThermJet" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ThermAir-web_Thumbnail.gif" border="0" alt="Eclipse ThermAir" title="Eclipse ThermAir" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/TFB_Thumbnail.gif" border="0" alt="Eclipse TFB" title="Eclipse TFB" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Sticktite_Thumbnail.gif" border="0" alt="Eclipse Open Burner Nozzles" title="Eclipse Open Burner Nozzles" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/StandardFilter-for-SMJ-Blower_Thumbnail.gif" border="0" alt="Eclipse Standard Filters for SMJ Blowers" title="Eclipse Standard Filters for SMJ Blowers" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/SPMT_Thumbnail.gif" border="0" alt="Eclipse Sinusoidal Plate Heat Exchangers" title="Eclipse Sinusoidal Plate Heat Exchangers" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Soleniod_Valves_Thumbnail.gif" border="0" alt="Eclipse Soleniod Valves" title="Eclipse Soleniod Valves" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/socket-plate_Thumbnail.gif" border="0" alt="Eclipse Socket Plates" title="Eclipse Socket Plates" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/SMJ-Blowers_Thumbnail.gif" border="0" alt="Eclipse "SMJ" Blowers" title="Eclipse "SMJ" Blowers" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Single-Ended-Radiant-Tube-Burner_Thumbnail.gif" border="0" alt="Eclipse Single-Ended Radiant Tube Burners" title="Eclipse Single-Ended Radiant Tube Burners" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RHT_Thumbnail.gif" border="0" alt="Eclipse RHT Indirect Air Heaters" title="Eclipse RHT Indirect Air Heaters" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RetractionMech-1140_Thumbnail.gif" border="0" alt="Eclipse Retraction Mechanism" title="Eclipse Retraction Mechanism" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RectangularFilters-For-SMJ-Blowers_Thumbnail.gif" border="0" alt="Eclipse Rectangular Filters for SMJ Blowers" title="Eclipse Rectangular Filters for SMJ Blowers" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RatioStar-duct_Thumbnail.gif" border="0" alt="Eclipse RatioStar" title="Eclipse RatioStar" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Ratiomatic_Thumbnail.gif" border="0" alt="Eclipse RatioMatic" title="Eclipse RatioMatic" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RatioAir_Thumbnail.gif" border="0" alt="Eclipse RatioAir" title="Eclipse RatioAir" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/RA-Euro-HP_Thumbnail.gif" border="0" alt="Eclipse European RatioAir HeatPak" title="Eclipse European RatioAir HeatPak" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/P-Tube_Thumbnail.gif" border="0" alt="Eclipse P-Tube" title="Eclipse P-Tube" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/PrimefireForehearth_Thumbnail.gif" border="0" alt="Eclipse PrimeFire Forehearth" title="Eclipse PrimeFire Forehearth" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Primefire400_Thumbnail.gif" border="0" alt="Eclipse PrimeFire 400" title="Eclipse PrimeFire 400" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Primefire100_Thumbnail.gif" border="0" alt="Eclipse PrimeFire 100" title="Eclipse PrimeFire 100" /></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Porportional-Mixers_Thumbnail.gif" border="0" alt="Eclipse Proportional Mixers" title="Eclipse Proportional Mixers" /></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Pilot-Mixers_Thumbnail.gif" border="0" alt="Eclipse Pilot Mixers" title="Eclipse Pilot Mixers" /></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/MountingBrackets_Thumbnail.gif" border="0" alt="Eclipse Mounting Brackets" title="Eclipse Mounting Brackets" /></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Mixing-Tees_Thumbnail.gif" border="0" alt="Eclipse Mixing Tees" title="Eclipse Mixing Tees" /></a></li>
		<li><a href="#?w=400" rel="35" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Minnox_Thumbnail.gif" border="0" alt="Eclipse Minnox" title="Eclipse Minnox" /></a></li>
		<li><a href="#?w=400" rel="36" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Minimixers_Thumbnail.gif" border="0" alt="Eclipse Minimixers" title="Eclipse Minimixers" /></a></li>
		<li><a href="#?w=400" rel="37" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Mini_SS_Thumbnail.gif" border="0" alt="Eclipse Mini Stainless" title="Eclipse Mini Stainless" /></a></li>
		<li><a href="#?w=400" rel="38" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/MarkIV_Thumbnail.gif" border="0" alt="Eclipse Mark IV" title="Eclipse Mark IV" /></a></li>
		<li><a href="#?w=400" rel="40" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Locktite_Manual_Reset_Gas_Shut-Off_Valves_Thumbnail.gif" border="0" alt="Eclipse Locktite Manual Reset Gas Shut-Off Valves" title="Eclipse Locktite Manual Reset Gas Shut-Off Valves" /></a></li>
		<li><a href="#?w=400" rel="41" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/LineBurnerCross_Thumbnail.gif" border="0" alt="Eclipse Line Burners" title="Eclipse Line Burners" /></a></li>
		<li><a href="#?w=400" rel="42" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/incinifume_Thumbnail.gif" border="0" alt="Eclipse InciniFume" title="Eclipse InciniFume" /></a></li>
		<li><a href="#?w=400" rel="43" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Incinicone_Thumbnail.gif" border="0" alt="Eclipse Incini-Cone" title="Eclipse Incini-Cone" /></a></li>
		<li><a href="#?w=400" rel="44" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ImmersoPak_Thumbnail.gif" border="0" alt="Eclipse ImmersoPak" title="Eclipse ImmersoPak" /></a></li>
		<li><a href="#?w=400" rel="45" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ImmersoJet_Thumbnail.gif" border="0" alt="Eclipse ImmersoJet" title="Eclipse ImmersoJet" /></a></li>
		<li><a href="#?w=400" rel="46" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Heat-up-Burner_Thumbnail.gif" border="0" alt="Eclipse Heat-Up Burner" title="Eclipse Heat-Up Burner" /></a></li>
		<li><a href="#?w=400" rel="47" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/GT-Underport-Burner_Thumbnail.gif" border="0" alt="Eclipse GT Underport Burner" title="Eclipse GT Underport Burner" /></a></li>
		<li><a href="#?w=400" rel="48" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/GT-CPA-Bracket_Thumbnail.gif" border="0" alt="Eclipse GT Bracket" title="Eclipse GT Bracket" /></a></li>
		<li><a href="#?w=400" rel="49" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/GT-CPA_Thumbnail.gif" border="0" alt="Eclipse GT/CPA" title="Eclipse GT/CPA" /></a></li>
		<li><a href="#?w=400" rel="50" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/GT_NG-DI_Thumbnail.gif" border="0" alt="Eclipse GT/NG-DI" title="Eclipse GT/NG-DI" /></a></li>
		<li><a href="#?w=400" rel="51" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Furnnox_Thumbnail.gif" border="0" alt="Eclipse Furnnox" title="Eclipse Furnnox" /></a></li>
		<li><a href="#?w=400" rel="52" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/PrimeFire300_Thumbnail.gif" border="0" alt="Eclipse PrimeFire 300" title="Eclipse PrimeFire 300" /></a></li>
		<li><a href="#?w=400" rel="53" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Forehearth-Burners_Thumbnail.gif" border="0" alt="Eclipse Forehearth Burners" title="Eclipse Forehearth Burners" /></a></li>
		<li><a href="#?w=400" rel="54" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/FlueFire_Thumbnail.gif" border="0" alt="Eclipse FlueFire" title="Eclipse FlueFire" /></a></li>
		<li><a href="#?w=400" rel="55" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Extern-a-Therm-Recuperator_Thumbnail.gif" border="0" alt="Eclipse Extern-a-Therm Recuperator" title="Eclipse Extern-a-Therm Recuperator" /></a></li>
		<li><a href="#?w=400" rel="56" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ExtensoJet_Thumbnail.gif" border="0" alt="Eclipse High ExtensoJet" title="Eclipse High ExtensoJet" /></a></li>
		<li><a href="#?w=400" rel="57" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/ERHeater_Thumbnail.gif" border="0" alt="Eclipse ER Indirect Air Heater" title="Eclipse ER Indirect Air Heater" /></a></li>
		<li><a href="#?w=400" rel="58" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/E-Jector_Thumbnail.gif" border="0" alt="Eclipse E-Jector" title="Eclipse E-Jector" /></a></li>
		<li><a href="#?w=400" rel="59" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/EHSeriesLanceBurner_Thumbnail.gif" border="0" alt="Eclipse EH Series Lance Burner" title="Eclipse EH Series Lance Burner" /></a></li>
		<li><a href="#?w=400" rel="60" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Eclipse-Float-Fire-System_Thumbnail.gif" border="0" alt="Eclipse Float Fire" title="Eclipse Float Fire" /></a></li>
		<li><a href="#?w=400" rel="62" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/DIR_Thumbnail.gif" border="0" alt="Eclipse Dimple Insulated" title="Eclipse Dimple Insulated" /></a></li>
		<li><a href="#?w=400" rel="63" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Dimple_Thumbnail.gif" border="0" alt="Eclipse Dimple Plate Heat Exchangers" title="Eclipse Dimple Plate Heat Exchangers" /></a></li>
		<li><a href="#?w=400" rel="64" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/DeepSpiralFlame_Thumbnail.gif" border="0" alt="Eclipse Deep Spiral Flame (DSF/DSF-CGO)" title="Eclipse Deep Spiral Flame (DSF/DSF-CGO)" /></a></li>
		<li><a href="#?w=400" rel="65" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Cross-Flow-Recuperators_Thumbnail.gif" border="0" alt="Eclipse Cross Flow Recuperators" title="Eclipse Cross Flow Recuperators" /></a></li>
		<li><a href="#?w=400" rel="66" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/BV-Pak_Packaged_Butterfly_Valves_Thumbnail.gif" border="0" alt="Eclipse BV-Pak Packaged Butterfly Valves" title="Eclipse BV-Pak Packaged Butterfly Valves" /></a></li>
		<li><a href="#?w=400" rel="67" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Butterfly_Valves_Thumbnail.gif" border="0" alt="Eclipse Butterfly Valves" title="Eclipse Butterfly Valves" /></a></li>
		<li><a href="#?w=400" rel="68" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Bulletin-VAC_Thumbnail.gif" border="0" alt="Eclipse VAC Burner" title="Eclipse VAC Burner" /></a></li>
		<li><a href="#?w=400" rel="69" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/BrightFire_Thumbnail.gif" border="0" alt="Eclipse BrightFire" title="Eclipse BrightFire" /></a></li>
		<li><a href="#?w=400" rel="70" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/BlastTips_Thumbnail.gif" border="0" alt="Eclipse Blast Tips" title="Eclipse Blast Tips" /></a></li>
		<li><a href="#?w=400" rel="71" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Blast_Thumbnail.gif" border="0" alt="Eclipse Blast Gate Valve" title="Eclipse Blast Gate Valve" /></a></li>
		<li><a href="#?w=400" rel="72" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Bayonet-Ultra_Recuperator_Thumbnail.gif" border="0" alt="Eclipse Bayonet-Ultra Recuperator" title="Eclipse Bayonet-Ultra Recuperator" /></a></li>
		<li><a href="#?w=400" rel="73" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Bayonet-Recuperator_Thumbnail.gif" border="0" alt="Eclipse Bayonet Recuperator" title="Eclipse Bayonet Recuperator" /></a></li>
		<li><a href="#?w=400" rel="74" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/AutoTite_Gas_Shut-Off_Valves_Thumbnail.gif" border="0" alt="Eclipse AutoTite Gas Shut-Off Valves" title="Eclipse AutoTite Gas Shut-Off Valves" /></a></li>
		<li><a href="#?w=400" rel="75" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Automotive-Filter-for-Eclipse-Blowers_Thumbnail.gif" border="0" alt="Eclipse Automotive Filters for Eclipse Blowers" title="Eclipse Automotive Filters for Eclipse Blowers" /></a></li>
		<li><a href="#?w=400" rel="76" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Automatic-Operated-Valves_Thumbnail.gif" border="0" alt="Eclipse Automatic Operated Valves" title="Eclipse Automatic Operated Valves" /></a></li>
		<li><a href="#?w=400" rel="77" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Atmospheric-Injectors_Thumbnail.gif" border="0" alt="Eclipse Atmospheric Injectors" title="Eclipse Atmospheric Injectors" /></a></li>
		<li><a href="#?w=400" rel="78" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Alum_air2_Thumbnail.gif" border="0" alt="Eclipse Aluminum Air-to-Air" title="Eclipse Aluminum Air-to-Air" /></a></li>
		<li><a href="#?w=400" rel="79" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/AirJectors_Thumbnail.gif" border="0" alt="Eclipse Airjectors" title="Eclipse Airjectors" /></a></li>
		<li><a href="#?w=400" rel="80" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Airheat_Thumbnail.gif" border="0" alt="Eclipse AirHeat" title="Eclipse AirHeat" /></a></li>
		<li><a href="#?w=400" rel="81" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/AHSideplate_Thumbnail.gif" border="0" alt="Eclipse Air Heat Side Plates" title="Eclipse Air Heat Side Plates" /></a></li>
		<li><a href="#?w=400" rel="82" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/AH-MA_Thumbnail.gif" border="0" alt="Eclipse Air Heat Side Plates" title="Eclipse Air Heat Side Plates" /></a></li>
		<li><a href="#?w=400" rel="83" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/AH-Duct_Thumbnail.gif" border="0" alt="Eclipse Air Heat Duct Units" title="Eclipse Air Heat Duct Units" /></a></li>
		<li><a href="#?w=400" rel="84" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Adjustable-Orfice-Gas-Cocks_Thumbnail.gif" border="0" alt="Eclipse Adjustable Orifice Gas Cocks" title="Eclipse Adjustable Orifice Gas Cocks" /></a></li>
		<li><a href="#?w=400" rel="85" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/Adjustable-Limiting-Orfice_Thumbnail.gif" border="0" alt="Eclipse Adjustable Limiting Orifice" title="Eclipse Adjustable Limiting Orifice" /></a></li>
		<li><a href="#?w=400" rel="86" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/0400(04V)_Thumbnail.gif" border="0" alt="Eclipse 0400(04V)" title="Eclipse 0400(04V)" /></a></li>
		<li><a href="#?w=400" rel="87" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/03V_Thumbnail.gif" border="0" alt="Eclipse 03V" title="Eclipse 03V" /></a></li>
		<li><a href="#?w=400" rel="88" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/03FA_Thumbnail.gif" border="0" alt="Eclipse 03FA" title="Eclipse 03FA" /></a></li>
		<li><a href="#?w=400" rel="89" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/03f_Thumbnail.gif" border="0" alt="Eclipse 03F" title="Eclipse 03F" /></a></li>
		<li><a href="#?w=400" rel="90" class="Product"><img src="Parts_by_Man_OK_By_Jon/Eclipse/thumbnails/3D-Linnox_Thumbnail.gif" border="0" alt="Eclipse Linnox" title="Eclipse Linnox" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

