<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="HistoryBackground"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php" id="Logo"></a>
<a href="index.php" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="index.php"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Packaged Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Case Studies</a></li>
			<li><a href="Newsletter-Archive.php">Newsletter Archive</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="HistoryLeftGrayBox"></div>
<div id="HistoryRightGrayBox"></div>
<div id="ThumbnailBackgroundWhite"></div>
<div id="ContentDropShadow"></div>
<ul id="slideshow">
	<li>
			<h3></h3>
			<span>1940big.gif</span>
			<p><br/><font size="8" face="Georgia">1940</font>
			<br/><br/><br/><br/>
			The ETTER Engineering Company was founded in Hamden, CT by Herbert Etter Sr., priding themselves on serving industrial New England.
			<br/><br/>ETTER Engineering originates as a manufacturer's sales representative firm, concentrating on natural gas equipment including, burners,
			regulators, meters, controls, flame safeguards, boilers, ovens, and furnaces. 
			<br/><br/>The Double E's become ETTER's first Signature logo.
			</p>
			<a href="#"><img src="1940.gif"/>1940</a>
		</li>	
		<li>
			<h3></h3>
			<span>Untitled-1.gif</span>
			<p><br/><font size="8" face="Georgia">1942</font>
			<br/><br/><br/><br/>
			Herbert Etter recognizes the booming industrial market. He makes the decision to expand ETTER opening a sales
			office in Boston, MA. Herbert Appoints Ford Waugh as Branch Manager.
			<br/><br/>Ford Waugh later goes on to become ETTER Engineering's Longest serving employee, remaining a major contributor 
			to the company for over 30 years.
			</p>
			<a href="#"><img src="1942.gif"/></a>
		</li>	
		<li>
			<h3></h3>
			<span>1943-photo.gif</span>
			<p><br/><font size="8" face="Georgia">1943</font>
			<br/><br/><br/><br/>
			ETTER Engineering moves to Waltham, Massachusetts, a city known for being at the forefront of modern, high-technology industry.
			<br/><br/>Waltham, Massachusetts is now known as a the prototype for 19th century industrial city planning.</p>
			<a href="#"><img src="1943.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Corp-Brochure-Pics-10011.gif</span>
			<p><br/><font size="8" face="Georgia">1960</font>
			<br/><br/><br/><br/>
			The ETTER Engineering logo is redesigned. The black and white diamond symbol is first introduced, representing strength and clarity of design,
			<br/><br/> The Walthman sales office relocates to Woburn, MA.</p>
			<a href="#"><img src="1960.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Corporate-Brochure-Scans0001.gif</span>
			<p><br/><font size="8" face="Georgia">1970</font>
			<br/><br/><br/><br/>
			Herbert Etter Sr.'s son Herbert Etter Jr. takes over as the president of the ETTER Engineering Company. Marking the second generation of 
			ETTER Engineering.</p>
			<a href="#"><img src="1970.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Corporate-Brochure-Scans0006.gif</span>
			<p><br/><font size="8" face="Georgia">1975</font>
			<br/><br/><br/><br/>
			Herbert Etter Jr. Broadens ETTER Engineering's realm of concentration within the process heating industry 
			by opening a manufacturing operation in Chelmsford, Massachusetts. ETTER Engineering adds assembling to their already vast
			industrial market resume.</p>
			<a href="#"><img src="1975.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>PrattPhoto.gif</span>
			<p><br/><font size="8" face="Georgia">1982</font>
			<br/><br/><br/><br/>
			ETTER Engineering is commissioned to design and manufacture the heated jet engines test cells for Pratt and Whitney jet engines.</p>
			<a href="#"><img src="1982.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Corp-Brochure-Pics-10010.gif</span>
			<p><br/><font size="8" face="Georgia">1986</font>
			<br/><br/><br/><br/>
			ETTER Engineering is taken back to its roots. The Chelmsford, Massachusetts office moves to Whitney Ave. in Hamden, Connecticut. </p>
			<a href="#"><img src="1986.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>ETTER1990Logo.gif</span>
			<p><br/><font size="8" face="Georgia">1990</font>
			<br/><br/><br/><br/>
			The ETTER logo is once again redesigned, transforming from yellow to blue, and finally opting for red. Red eventually evolves to become a signature color for the ETTER
			brand.</p>
			<a href="#"><img src="1990.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Corporate-Brochure-Scans0004.gif</span>
			<p><br/><font size="8" face="Georgia">1994</font>
			<br/><br/><br/><br/>
			ETTER Engineering's web scrubber nozzle design becomes an industry choice in the scratch lottery ticket market, 
			which is a challenging drying application. ETTER expanded with installations in numerous states along with Canada.</p>
			<a href="#"><img src="1994.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>ul-logo1.gif</span>
			<p><br/><font size="8" face="Georgia">1995</font>
			<br/><br/><br/><br/>
			ETTER Engineering earns UL Certification December 18, 1995. ETTER is now recognized for meeting standards and test procedures,
			materials, components, assemblies, tools, and equipment dealing with product safety.</p>
			<a href="#"><img src="1995.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>OldWebScreenShot.gif</span>
			<p><br/><font size="8" face="Georgia">1996</font>
			<br/><br/><br/><br/>
			ETTER Engineering launched their first website, www.etterengineering.com in order to keep up with rapidly growing technology as well as 
			broaden their geographic market.</p>
			<a href="#"><img src="1996.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Tom_Jon_80s_Photos0004greywall.gif</span>
			<p><br/><font size="8" face="Georgia">1999</font>
			<br/><br/><br/><br/>
			Tom Etter succeeds his father Herbert Etter Jr. as the president of ETTER Engineering.
			<br/><br/> Jon Moore is appointed as Vice President and later partner in 2008.</p>
			<a href="#"><img src="1999.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>IMG_2345.gif</span>
			<p><br/><font size="8" face="Georgia">2009</font>
			<br/><br/><br/><br/>
			ETTER Engineering launches their own line of UL Certified Hermetically sealed natural gas boosters, coining the acroynym ENGB (ETTER Next Generation Booster). 
			ETTER officially establishes themselves as a brand generating a great deal of buzz throughout the combustion industry.
			<br/><br/>Just a few months later the ENGB provides fueling capabilities for the Zamboni at Rockefeller Center.</p>
			<a href="#"><img src="2009.gif"/></a>
		</li>
		<li>
			<h3></h3>
			<span>Rendering-of-210-Century.gif</span>
			<p><br/><font size="8" face="Georgia">2010</font>
			<br/><br/><br/><br/>
			ETTER Engineering's workforce grows by over 30%. The company moves to a new location in Bristol, CT, featuring over 15,000 Square feet of modern 
			engineering and manufacturing facility space, in order to accommodate the growing business.</p>
			<a href="#"><img src="2010.gif"/></a>
		</li>
	</ul>
	<div id="wrapper2">
		<div id="Historyfullsize">
			<div id="image"></div>
			<div id="information">
				<h3></h3>
				<p></p>
			</div>
		</div>
		<div id="thumbnails">
			<div id="slideleft" title="Slide Left"></div>
			<div id="slidearea">
				<div id="Historyslider"></div>
			</div>
			<div id="slideright" title="Slide Right"></div>
		</div>
	</div>
<script type="text/javascript" src="compressed.js"></script>


<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="HistoryWhitebardrop"></div>
<div id="HistoryWhitebardroptopofbottom"></div>
<div id="HistoryWhitebardroptopoftop"></div>
<div id="HistoryPartsByManDropShadow"></div>
<div id="HistoryPartsByManDropShadow2"></div>
<div id="HistoryLrgProductDropShadow"></div>
<div id="HistoryWhtBar"></div>
<div id="HistoryWhtBarTop"></div>
<div id="BlackParts"></div>
<div id="BlackPartsTop"></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>
