<?php
$to      = "shayes@etterengineering.com";
$subject = "Email from website";
$message = $_REQUEST["body"];
$email = $_REQUEST["email"];

function is_valid_email($email) {
  return preg_match('#^[a-z0-9.!\#$%&\'*+-/=?^_`{|}~]+@([0-9.]+|([^\s]+\.+[a-z]{2,6}))$#si', $email);
}

if (!is_valid_email($email)) {
  echo 'Sorry, invalid email';
  exit;
}

$headers = "From: $email";
mail($to, $subject, $message, $headers);
echo "Thanks for submitting.";
?>
