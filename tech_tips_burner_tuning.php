<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Technical Tips</title>
<!--[if !IE]><!--><link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	<!--<![endif]-->
<!--[if IE]><link rel="stylesheet" type="text/css" href="ie7-only.css" /><![endif]-->
<!--[if lte IE 6]><link rel="stylesheet" type="text/css" href="all-ie6-only2.css" /><![endif]-->
<!--[if gte IE 8]><link rel="stylesheet" type="text/css" href="all-ie-only2.css" /><![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
</head>
<body link="#445679" vlink="#445679">
<script type="text/javascript">
google.load("jquery", "1");
</script>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="TechTipsLeftTxt">
<div id="TechTipsHeader"><font size="2" color="#445679"><b>Technical Tips</b></font></div>
<br/><a href="tech_tips.php" id="TechTipGoodVV"><font color="#ACB0C3"><b>&#149; The Good Old Vent Valve!</b></font></a>
<br/><a href="tech_tips_intro_burners.php"><font color="#ACB0C3"><b>&#149; Intro to Commercial and Industrial Burners</b></font></a>
<br/><a href="tech_tips_basic_burners.php"><font color="#ACB0C3"><b>&#149; Basic Burner Types</b></font></a>
<br/><a href="tech_tips_burner_tuning.php"><font color="#ACB0C3"><b>&#149; Basic Burner Tuning</b></font></a>
<br/><a href="tech_tips_burners_vs_clunkers.php"><font color="#ACB0C3"><b>&#149; Cash for Burners vs. Cash for Clunkers</b></font></a>
</div>
<div id="InsidetheJob">
<div id="TechRightHeader"><blockquote><font size="2" color="#D21D1F"><b><br/>Basic Burning Tuning</b></font></blockquote></div>
<br/><blockquote>By: Jon Moore
<br/><br/>With our most recent edition of Tech Tips, Herb had touched upon the numerous types of gas burners &amp; how they are similar 
yet different. The next logical step is to discuss the basics of combustion as it relates to the burner. Again, this column is to be 
taken as a broad overview, and is not intended to resolve or address specific needs or problems. Tuning of combustion equipment should 
be left to a trained professional (do not try this at home!).
<br/><br/>For combustion to take place, you not only need the fuel &amp; air, but also the "three T's of combustion", without which the 
process will extinguish itself, be dangerously poor in its performance, or fail to ignite. The three T's are: Time, Temperature &amp; 
Turbulence. You need to expose the fuel to the air for the proper amount of TIME to complete combustion. This is technically referred 
to as residence time, the time of exposure. The reaction needs TEMPERATURE in order to take place. Once the process is started, this 
heat is usually self-sustaining coming from the fire itself. Initially, the temperature or energy is usually provided using an 
external means, most typically a high voltage ignition transformer providing a spark just like your car's ignition. The TURBULENCE 
insures that the fuel &amp; the air are mixed properly. If these two streams stratify or segregate, combustion will be adversely 
affected. On an atomic level, the oxygen molecules must be able to rub shoulders with the carbon &amp; hydrogen molecules or they 
cannot burn. 
<br/><br/>Should you loose any of the 3 T's your process &amp; system efficiency will pay the price. Incomplete combustion results 
in abnormally high levels of CO (a noxious &amp; toxic gas), low levels of heat, cost you money &amp; present a danger when it comes to 
potential puffs (that's what we in the combustion business like to call uncontrolled combustion, also sometimes referred to as 
explosions). This is a serious financial &amp; safety matter that's why it is best left to the professional.
<br/><br/>With now knowing the 3 T's, what about stoichiometry? Huh, What did you say? Well, stoichiometry is the measurement 
of fuel to air ratio. Providing exactly the right molecular amount of Oxygen required to liberate the heat of the reaction 
from Carbon &amp; Hydrogen molecules and chains is referred to as a "Stoichiometric Burn," or a stoichiometric ratio (SR) equal 
to 1.0. Providing 10% excess air would be SR = 1.10, &amp; a 5% deficiency of air (or sub-stoichiometric burn) would be SR = 0.95. 
Yes, you can burn fuel in an oxygen deficient atmosphere; usually the burnout air (the air required to complete the reaction) 
is added downstream. An oxygen deficient atmosphere is called RICH or fuel rich, while an atmosphere that has more than enough 
excess air for the combustion is called LEAN. Most process &amp; industrial heat applications are LEAN applications with plenty of 
excess air. Great care should be used in "on ratio" or sub-stoichiometric applications. If the burnout air is added suddenly 
and the three T's are present, the probable result is a BANG (that's another one of those words we use in the industry)!
<br/><br/><b>Lean Fires:</b> Lean fires using natural gas get quite blue, sometimes even violet. When firing oil, lean burns 
are very bright yellow, almost white, &amp; this flame ought to be looked at through a blue lens so as not to harm your eyes. 
A fire on either fuel with excessive amounts of excess air can actually be quenched (losing the Temperature of the three T's) 
which will create high levels of CO and potentially white smoke.
<br/><br/><b>Rich Fires:</b> A rich fire on gas turns yellow, and if it even starts to look orange shut off the FUEL, it is 
getting to a dangerous point. A dark smoking gas fire is very dangerous. A rich fire on oil will get very orange and dirty, 
producing a dark smoke. 
<br/><br/>In either case, the most important thing to leave you with is this: If you ever have a RICH fire, do not add or 
increase the AIR to clean the fire up. You just gave the fire just what it needs to reach out and touch you, going BOOM! 
ALWAYS cut back on fuel in a rich condition, never add the air! When manually tuning &amp; increasing your firing rate, bring 
air up first, then fuel, keeping yourself LEAN. When decreasing your firing rate, bring fuel down first, and then decrease 
the air to the fuel, again staying LEAN. Remember, let a professional tune your combustion equipment. Those who play with 
fire may get burned! 
</blockquote></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>