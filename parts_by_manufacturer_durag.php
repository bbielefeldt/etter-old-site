<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Durag combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Durag,D-GF 150 Burner Controls,D-AM 150 Display Module,D-GF 55 Burner Control,D-LE 603 Flame Sensor,D-LE 703 Flame Sensor With Optic System,D-LE 701 Flame Sensor With Fibre Optic System,D-GT 800 Flame Sensor,D-LE 103 Flame Sensor,D-LX 200 Compact Flame Monitor,D-LX 720 Compact Flame Monitor With Fibre Optic System,D-LX 100 Compact Flame Monitor,D-LX 700 Compact Flame Monitor With Fibre Optic System,D-UG 120 Control Unit,D-UG 660 Control Unit,AAL 75 Flame Monitor,D-IR 55 Flame Monitor,D-UV 55 Flame Monitor,D-HG 400 High Energy Ignition Device,D-HG 55 Electronic Ignition Transformer,D-VE 500 Pneumatic Retraction Unit,E-LIGHT 2000 Ignition Units,BWO Ignition Units Ignition and Burner Control,SVECU Ignition Units Ignition and Burner Control,Portable Ignition Units,D-GF 150 Burner Controls" />
<title>ETTER Engineering - Durag Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="DuragLogoLarge"></div>
<div id="SensusText">The DURAG GROUP is a global provider of measuring and control technology 
in the fields of Combustion Technology, Environmental Monitoring, and Environmental and Process Data Management
throughout the world.  These include, for example, fossil fuel power stations, 
plants in the chemical industry, refineries, cement plants, waste incinerators, 
steam generators. thermal power plants and gas turbines.
<br/><br/>We can also provide solutions for applications in special environments, 
such as extreme climatic zones or potentially explosive atmospheres.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-GF150.gif" alt="Durag D-GF 150 Burner Controls" title="Durag D-GF 150 Burner Controls"/></div>			
			<div id="PartsContent"><h3>D-GF 150
			<br/><font color="#50658D">Burner Controls</font></h3>
			<p><br/>Self-monitoring and fail-safe burner control for the control of gas and oil burners as well as combined gas/oil burners of any capacity.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Controlling and monitoring of gas and oil 
			<br/>&nbsp;&nbsp;burners of any capacity
    			<br/>&#149; Suitable for intermittent operation, 
			<br/>&nbsp;&nbsp;continuous operation and 72-hour 
			<br/>&nbsp;&nbsp;operation according to TRD 604
    			<br/>&#149; Integrated gas valve monitoring system
    			<br/>&#149; Separate outputs for control of gas and oil 
			<br/>&nbsp;&nbsp;fuel valves
    			<br/>&#149; Quick fuel change "on the fly" 
			<br/>&nbsp;&nbsp;without burner shut down
    			<br/>&#149; Adjustable pre-purge timer
    			<br/>&#149; Integrated flame monitor
    			<br/>&#149; Input for external flame monitor
    			<br/>&#149; Data interface
    			<br/><br/><b>Applications:</b>
    			<br/><br/>&#149; Chemical industry
    			<br/>&#149; Refineries
    			<br/>&#149; Cement plants
    			<br/>&#149; Waste incinerators
    			<br/>&#149; Steam generators
    			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles//dgf150.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="2" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-AM150.gif" alt="Durag D-AM 150 Display Module" title="Durag D-AM 150 Display Module"/></div>		
			<div id="PartsContent"><h3>D-AM 150
			<br/><font color="#50658D">Display Module</font></h3>
			<p>These accurate and reliable switches feature a two-circuit control in which each set point controls two independent dry contacts to switch to two different voltages. The high and low pressure settings are adjustable and they are available in reset and recycle types.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Plain text for the burner control D-GF 150
			<br/>&#149; Initial value indicator with 24 inputs in 
			<br/>&nbsp;&nbsp;three groups
			<br/>&#149; Fault memory
			<br/>&#149; Text editor for plain text display
			<br/>&#149; Output relay for control via fieldbus
			<br/>&#149; Operational hours counter
 			<br/>&#149; Cycle counter
			<br/>&#149; Chip card for ease of programming
			<br/>&#149; Fieldbus communication (MODBUS-RTU) 
			<br/>&nbsp;&nbsp;for up to 32 devices
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dam150_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			 
</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-GF55.gif" alt="Durag D-GF 55 Burner Control" title="Durag D-GF 55 Burner Control"/></div>			
			<div id="PartsContent"><h3>D-GF 55
			<br/><font color="#50658D">Burner Control</font></h3>
			<p>Self-monitoring and fail-safe burner control for the control of gas and oil burners as well as combined gas/oil burners of any capacity.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Controlling and monitoring of gas and oil 
			<br/>&nbsp;&nbsp;burners of any capacity
			<br/>&#149; Suitable for intermittent operation 
			<br/>&nbsp;&nbsp;(D-GF 55-10) and continuous operation 
			<br/>&nbsp;&nbsp;(D-GF 55-20)
			<br/>&#149; Integrated ionisation flame monitor
			<br/>&#149; Input for external flame monitor
			<br/><br/><b>Applications</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LE603.gif" alt="Durag D-LE 603 Flame Sensor" title="Durag D-LE 603 Flame Sensor"/></div>		
			<div id="PartsContent"><h3>D-LE 603
			<br/><font color="#50658D">Flame Sensor</font></h3>
			<p>Flame sensor for the monitoring of gas, oil and coal flames, primarily in multi-burner furnaces.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Self-monitoring and fail-safe in conjunction 
			<br/>&nbsp;&nbsp;with a control unit/burner control
			<br/>&#149; Flame sensors for every spectral range 
			<br/>&nbsp;&nbsp;from UV to IR
			<br/>&#149; Connection to the D-UG 120 control unit, 
			<br/>&nbsp;&nbsp;D-UG 660 control unit as well as to the 
			<br/>&nbsp;&nbsp;D-GF 150 burner control
			<br/>&#149; Uniform output signal thus mutually 
			<br/>&nbsp;&nbsp;interchangeable
			<br/>&#149; Adjustable to different combustion 
			<br/>&nbsp;&nbsp;technologies such as exhaust gas 
			<br/>&nbsp;&nbsp;recirculation
			<br/>&#149; Compliance to general safety regulations
			<br/>&#149; ATEX approved (D-LE 603 /94 Ex for zone 
			<br/>&nbsp;&nbsp;1 and D-LE 603 /97 Ex for zone 2)
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dle603_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LE703.gif" alt="Durag D-LE 703 Flame Sensor With Optic System" title="Durag D-LE 703 Flame Sensor With Optic System"/></div>
			<div id="PartsContent"><h3>D-LE 703 
			<br/><font color="#50658D">Flame Sensor With Optic System</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Self-monitoring and fail-safe flame sensor 
			<br/>&nbsp;&nbsp;with a fibre-optic connection in conjunction 
			<br/>&nbsp;&nbsp;with a control unit/burner control
			<br/>&#149; Monitoring of gas, oil and coal flames
			<br/>&#149; Connection to the D-UG 120, D-UG 660 
			<br/>&nbsp;&nbsp;control unit and the D-GF 150 burner 
			<br/>&nbsp;&nbsp;control
			<br/>&#149; Spectral range from UV to IR
			<br/>&#149; Uniform output signal thus mutually 
			<br/>&nbsp;&nbsp;interchangeable
			<br/>&#149; Adjustable to different combustion 
			<br/>&nbsp;&nbsp;technologies such as exhaust 
			<br/>&nbsp;&nbsp;gas recirculation
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Burners with difficult installation conditions 
			<br/>&nbsp;&nbsp;for conventional flame sensors or on those 
			<br/>&nbsp;&nbsp;whose environmental temperature near the 
			<br/>&nbsp;&nbsp;sighting tube is too high
			<br/>&#149; Power stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dle701_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/DE-LE701.gif" alt="Durag D-LE 701 Flame Sensor With Fibre Optic System" title="Durag D-LE 701 Flame Sensor With Fibre Optic System"/></div>
			<div id="PartsContent"><h3>D-LE 701
			<br/><font color="#50658D">Flame Sensor With Fibre Optic System</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Self-monitoring and fail-safe flame sensor 
			<br/>&nbsp;&nbsp;with a fibre-optic connection (in conjunction 
			<br/>&nbsp;&nbsp;with a control unit/burner control)
			<br/>&#149; Monitoring of gas, oil and coal flames
			<br/>&#149; Connection to the D-UG 120, D-UG 660 
			<br/>&nbsp;&nbsp;control unit and the D-GF 150 burner 
			<br/>&nbsp;&nbsp;control
			<br/>&#149; Spectral range from UV to IR
			<br/>&#149; Uniform output signal thus mutually 
			<br/>&nbsp;&nbsp;interchangeable
			<br/>&#149; Adjustable to different combustion 
			<br/>&nbsp;&nbsp;technologies such as exhaust 
			<br/>&nbsp;&nbsp;gas recirculation
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Burners with difficult installation conditions 
			<br/>&nbsp;&nbsp;for conve
			<br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants			
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dle701_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="7" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-GT800.gif" alt="Durag D-GT 800 Flame Sensor" title="Durag D-GT 800 Flame Sensor"/></div>		
			<div id="PartsContent"><h3>D-GT 800
			<br/><font color="#50658D">Flame Sensor</font></h3>
			<p>Flame sensor for the monitoring of gas and oil flames, primarily in gas turbines or in particularly harsh environments.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Optionally available with air/water cooling
			<br/>&#149; Deployable with high combustion chamber 
			<br/>&nbsp;&nbsp;overpressure
			<br/>&#149; High vibrational stability
			<br/>&#149; Conforms to general safety regulations
			<br/>&#149; Self-monitoring and fail-safe in conjunction 
			<br/>&nbsp;&nbsp;with a control unit/burner control
			<br/>&#149; Connection to the D-UG 120, D-UG 660 
			<br/>&nbsp;&nbsp;control unit and the D-GF 150 burner 
			<br/>&nbsp;&nbsp;control
			<br/><br/><b>Applications</b>
			<br/><br/>&#149; Burners with difficult installation conditions 
			<br/>&nbsp;&nbsp;for conventional flame sensors or on those 
			<br/>&nbsp;&nbsp;whose environmental temperature near the 
			<br/>&nbsp;&nbsp;sighting tube is very high
			<br/>&#149; Power stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/>&#149; Gas turbines		
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dgt800_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="8" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LE103.gif" alt="Durag D-LE 103 Flame Sensor" title="Durag D-LE 103 Flame Sensor"/></div>	
			<div id="PartsContent"><h3>D-LE 103
			<br/><font color="#50658D">Flame Sensor</font></h3>
			<p>Flame sensors for the monitoring of gas, oil and coal flames, primarily in single burner furnaces.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Self-monitoring and fail-safe in conjunction 
			<br/>&nbsp;&nbsp;with a control unit/burner control
			<br/>&#149; Flame sensors for every spectral range of 
			<br/>&nbsp;&nbsp;flame monitoring from UV to IR
			<br/>&#149; Connection to D-UG 120 and D-UG 660 
			<br/>&nbsp;&nbsp;contol unit as well as D-GF 150 burner 
			<br/>&nbsp;&nbsp;control
			<br/>&#149; Uniform output signal thus mutually 
			<br/>&nbsp;&nbsp;interchangeable
			<br/>&#149; Compliance to general safety regulations
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Power stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dle103_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LX200.gif" alt="Durag D-LX 200 Compact Flame Monitor" title="Durag D-LX 200 Compact Flame Monitor"/></div>			
			<div id="PartsContent"><h3>D-LX 200
			<br/><font color="#50658D">Compact Flame Monitor</font></h3>
			<p>Self-monitoring and fail-safe compact flame monitor for the monitoring of gas, oil and coal flames with integrated UV or IR flame sensor.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Available with cable gland or cable plug 
			<br/>&nbsp;&nbsp;(version /MP)
			<br/>&#149; Wide sensitivity range
			<br/>&#149; For ambient temperatures from -40&deg;C up 
			<br/>&nbsp;&nbsp;to +85&deg;C
			<br/>&#149; Dual channel design throughout
			<br/>&#149; Measurement of flame flicker frequency
			<br/>&#149; Selective to individual burners and fuels
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dlx200_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LX720.gif" alt="Durag D-LX 720 Compact Flame Monitor With Fibre Optic System" title="Durag D-LX 720 Compact Flame Monitor With Fibre Optic System"/></div>			
			<div id="PartsContent"><h3>D-LX 720 
			<br/><font color="#50658D">Compact Flame Monitor With Fibre Optic System</font></h3>
			<p>Self-monitoring and fail-safe compact flame monitor for the monitoring of gas, oil and coal flames with integrated UV or IR flame sensor.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Wide sensitivity range
			<br/>&#149; For ambient temperatures from -40&deg;C up 
			<br/>&nbsp;&nbsp;to +85&deg;C
			<br/>&#149; Dual channel design throughout
			<br/>&#149; Measurement of flame flicker frequency
			<br/>&#149; Selective to individual burners and fuels
			<br/><br/><b>Applications</b>
			<br/><br/>&#149; Burners with difficult installation conditions 
			<br/>&nbsp;&nbsp;for conventional flame sensors or on those 
			<br/>&nbsp;&nbsp;whose ambient temperature near the 
			<br/>&nbsp;&nbsp;sighting tube is too high
			<br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dlx720_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a>
			</p>
</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LX100.gif" alt="Durag D-LX 100 Compact Flame Monitor" title="Durag D-LX 100 Compact Flame Monitor"/></div>
			<div id="PartsContent"><h3>D-LX 100
			<br/><font color="#50658D">Compact Flame Monitor</font></h3>
			<p>Self-monitoring and fail-safe compact flame monitor for the monitoring of gas, oil and coal flames with integrated UV, VIS or IR flame sensor, primarily in single burner furnaces.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Suitable for continuous operation and 
			<br/>&nbsp;&nbsp;72-hour operation according to TRD 604
			<br/>&#149; Compact design, flame sensor and control 
			<br/>&nbsp;&nbsp;unit in one enclosure, takes up no space in 
			<br/>&nbsp;&nbsp;control cabinet
			<br/>&#149; LED display for settings and operational 
			<br/>&nbsp;&nbsp;status
			<br/>&#149; ATEX approved (D-LX 100 .../94 Ex for 
			<br/>&nbsp;&nbsp;zone 1 and D-LX.../97 Ex for zone 2)
			<br/><br/><b>Applications</b>
			<br/><br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dlx100_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a>
			</p>
</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-LX700.gif" alt="Durag D-LX 700 Compact Flame Monitor With Fibre Optic System" title="Durag D-LX 700 Compact Flame Monitor With Fibre Optic System"/></div>			
			<div id="PartsContent"><h3>D-LX 700 
			<br/><font color="#50658D">Compact Flame Monitor With Fibre Optic System</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Suitable for continuous operation and 
			<br/>&nbsp;&nbsp;72-hour operation according to TRD 604
			<br/>&#149; Compact design, flame sensor and control 
			<br/>&nbsp;&nbsp;unit in one enclosure, takes up no space in 
			<br/>&nbsp;&nbsp;control cabinet
			<br/>&#149; LED display for settings and operational 
			<br/>&nbsp;&nbsp;status
			<br/>&#149; Self-monitoring and fail-safe compact 
			<br/>&nbsp;&nbsp;flame monitor for the monitoring of gas, 
			<br/>&nbsp;&nbsp;oil and coal flames with integrated UV, VIS 
			<br/>&nbsp;&nbsp;or IR flame sensor, primarily in single 
			<br/>&nbsp;&nbsp;burner furnaces
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Burners with difficult installation conditions 
			<br/>&nbsp;&nbsp;for conventional flame sensors or on those 
			<br/>&nbsp;&nbsp;whose ambient temperature near the 
			<br/>&nbsp;&nbsp;sighting tube is too high
			<br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dlx700_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a>
			<br/><a href="http://www.durag.com/userfiles/file/man_dlx700_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Preliminary Manual</b></font></a></p>
			
</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-UG120.gif" alt="Durag D-UG 120 Control Unit" title="Durag D-UG 120 Control Unit"/></div>
			<div id="PartsContent"><h3>D-UG 120 
			<br/><font color="#50658D">Control Unit</font></h3>
			<p>Self-monitoring and fail-safe control unit for the monitoring of gas, oil and coal flames with DURAG UV, UV+IR or IR flame sensors, primarily in single burner furnaces.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Suitable for intermittent operation, 
			<br/>&nbsp;&nbsp;continuous operation and 72-hour 
			<br/>&nbsp;&nbsp;operation according to TRD 604
			<br/>&#149; LED display
			<br/>&#149; Installation on DIN-rail
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dug120_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="14" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-UG660.gif" alt="Durag D-UG 660 Control Unit" title="Durag D-UG 660 Control Unit"/></div>	
			<div id="PartsContent"><h3>D-UG 660 
			<br/><font color="#50658D">Control Unit</font></h3>
			<p>Self-monitoring and fail-safe control unit for the monitoring of gas, oil and coal flames with DURAG UV, UV+IR or IR- flame sensors, primarily in multiple-burner furnaces.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Suitable for intermittent operation, 
			<br/>&nbsp;&nbsp;continuous operation and 72-hour 
			<br/>&nbsp;&nbsp;operation as per TRD 604
			<br/>&#149; Optional parallel operation of two 
			<br/>&nbsp;&nbsp;flame sensors in any combination: 
			<br/>&nbsp;&nbsp;UV/UV, UV/IR or IR/IR
			<br/>&#149; Three different settings supported for 
			<br/>&nbsp;&nbsp;various modes (e.g. dependent on fuel or 
			<br/>&nbsp;&nbsp;combustion technology), automatic 
			<br/>&nbsp;&nbsp;activation by burner management system
			<br/>&#149; Plain text display
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Power Stations
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dug660_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/AAL75.gif" alt="Durag AAL 75 Flame Monitor" title="Durag AAL 75 Flame Monitor"/></div>
			<div id="PartsContent"><h3>AAL 75 
			<br/><font color="#50658D">Flame Monitor</font></h3>
			<p>Particularly cost-effective, fail-safe flame monitors for the monitoring of gas and oil burners as well as combined gas/oil burners.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Monitoring of gas and oil burners of any 
			<br/>&nbsp;&nbsp;load
			<br/>&#149; Suitable for intermittent operation and 
			<br/>&nbsp;&nbsp;continuous operation (only AAL 75 POD)
			<br/>&#149; Simple installation on TS 35 DIN-rail
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_55_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a>
			<br/><a href="http://www.durag.com/userfiles/file/man_aal75_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Operating Manual</b></font></a></p>
			
</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-IR55.gif" alt="Durag D-IR 55 Flame Monitor" title="Durag D-IR 55 Flame Monitor"/></div>
			<div id="PartsContent"><h3>D-IR 55 
			<br/><font color="#50658D">Flame Monitor</font></h3>
			<p>Particularly cost-effective, fail-safe flame monitors for the monitoring of gas and oil burners as well as combined gas/oil burners.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Monitoring of gas and oil burners of any 
			<br/>&nbsp;&nbsp;load
			<br/>&#149; Simple installation on TS 35 DIN-rail
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_55_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-UV55.gif" alt="Durag D-UV 55 Flame Monitor" title="Durag D-UV 55 Flame Monitor"/></div>		
			<div id="PartsContent"><h3>D-UV 55 
			<br/><font color="#50658D">Flame Monitor</font></h3>
			<p>Particularly cost-effective, fail-safe flame monitors for the monitoring of gas and oil burners as well as combined gas/oil burners.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Monitoring of gas and oil burners of any 
			<br/>&nbsp;&nbsp;load
			<br/>&#149; Simple installation on TS 35 DIN-rail
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_55_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-HG400.gif" alt="Durag D-HG 400 High Energy Ignition Device" title="Durag D-HG 400 High Energy Ignition Device"/></div>		
			<div id="PartsContent"><h3>D-HG 400
			<br/><font color="#50658D">High Energy Ignition Device</font></h3>
			<p>The D-HG 400 high energy ignition device is suitable for the ignition of gas or liquid fuels in industrial burners of any capacity.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Ignition of liquid or gaseous fuels with 
			<br/>&nbsp;&nbsp;large power ratings - ignites even heavy 
			<br/>&nbsp;&nbsp;oil no. 6
			<br/>&#149; Compact design: control unit and ignition 
			<br/>&nbsp;&nbsp;lance form one unit (D-HG 400-50, -51)
			<br/>&#149; Special designs for potentially explosive 
			<br/>&nbsp;&nbsp;atmospheres are available 
			<br/>&nbsp;&nbsp;(D-HG 400-54 Ex, -72 Ex)
			<br/>&#149; Special design with battery operation 
			<br/>&nbsp;&nbsp;(D-HG 400-80 with D-HG 400-81)
			<br/>&#149; Special design for tilting burner with 
			<br/>&nbsp;&nbsp;flexible ignition lance (D-HG 400-90)
			<br/>&#149; No wear because thyristor controlled
			<br/>&#149; 20 ignition sparks with 4.5 J per second 
			<br/>&nbsp;&nbsp;(i.e. 90 J total per second)
			<br/>&#149; Ignition feedback signal via integrated 
			<br/>&nbsp;&nbsp;LED or potential-free relay output
			<br/>&#149; Suitable as "Ignitor Class 3 Special" in 
			<br/>&nbsp;&nbsp;accordance with NFPA 8501 and NFPA 8502
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/>&#149; Claus plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dhg400_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-HG55.gif" alt="Durag D-HG 55 Electronic Ignition Transformer" title="Durag D-HG 55 Electronic Ignition Transformer"/></div>		
			<div id="PartsContent"><h3>D-HG 55
			<br/><font color="#50658D">Electronic Ignition Transformer</font></h3>
			<p>The D-HG 400 high energy ignition device is suitable for the ignition of gas or liquid fuels in industrial burners of any capacity.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Ignition of oil and gas
			<br/>&#149; High-performance and reliable ignition
			<br/>&#149; Simple to use and installation
			<br/>&#149; Robust enclosure for industrial use
			<br/>&#149; Maintenance-free because no 
			<br/>&nbsp;&nbsp;wearing parts
			<br/>&#149; 100 ignition sparks/second with a main 
			<br/>&nbsp;&nbsp;frequency of 50Hz, 120 ignition 
			<br/>&nbsp;&nbsp;sparks/second with a mains frequency 
			<br/>&nbsp;&nbsp;of 60Hz
			<br/>&#149; Suitable as "Ignitor Class 3 Special" 
			<br/>&nbsp;&nbsp;in accordance with NFPA 8501 and 
			<br/>&nbsp;&nbsp;NFPA 8502
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dhg55_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-VE500.gif" alt="Durag D-VE 500 Pneumatic Retraction Unit" title="Durag D-VE 500 Pneumatic Retraction Unit"/></div>		
			<div id="PartsContent"><h3>D-VE 500 
			<br/><font color="#50658D">Pneumatic Retraction Unit</font></h3>
			<p>Pneumatic retraction unit for the insertion and retraction of ignition lances and ignition devices.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Automatic insertion and retraction of 
			<br/>&nbsp;&nbsp;ignition lances
			<br/>&#149; Compressed air drive
			<br/>&#149; Direction change with solenoid valve
			<br/>&#149; Speed control
			<br/>&#149; Non-contact limit switch
			<br/>&#149; For use with ignition deviceses D-HG 
			<br/>&nbsp;&nbsp;400-50 and rigid ignition lances D-ZL...
			<br/>&#149; Available stroke lengths: 300, 400, 500 
			<br/>&nbsp;&nbsp;and 600mm
			<br/>&#149; Pressure-tight and/or explosion protected 
			<br/>&nbsp;&nbsp;models also available
			<br/>&#149; Operational overpressure up to 10 bar
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Chemical industry
			<br/>&#149; Refineries
			<br/>&#149; Cement plants
			<br/>&#149; Waste incinerators
			<br/>&#149; Steam generators
			<br/>&#149; Heating plants
			<br/>&#149; Claus plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_dve500_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/E-Light2000.gif" alt="Durag E-LIGHT 2000 Ignition Units" title="Durag E-LIGHT 2000 Ignition Units"/></div>			
			<div id="PartsContent"><h3>E-LIGHT 2000
			<br/><font color="#50658D">Ignition Units</font></h3>
			<p>Ignition and burner control for ignition burners, pilot burners, lances and spark plugs.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; High energy ignition
			<br/>&#149; Capacitor discharge with thyristor/diode
			<br/>&#149; 100% Electronic, no wear
			<br/>&#149; Wide range of power supply voltages
			<br/>&#149; Low power consumption
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Furnaces
			<br/>&#149; Boilers
			<br/>&#149; Incinerators
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_smitsvonk_products_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/BWO-Ignition-Units.gif" alt="Durag BWO Ignition Units Ignition and Burner Control" title="Durag BWO Ignition Units Ignition and Burner Control"/></div>		
			<div id="PartsContent"><h3>BWO Ignition Units
			<br/><font color="#50658D">Ignition and Burner Control</font></h3>
			<p>Ignition and burner control for ignition burners, pilot burners, lances and spark plugs.
			<br/><br/><b>Applications:</b>
			<br/><br/>Ignition and burner control for:
			<br/>&#149; Ignition burners
			<br/>&#149; Pilots burners
			<br/>&#149; Lances
			<br/>&#149; Spark plugs
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_smitsvonk_products_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="23" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/SVECU-Ignition-Units.gif" alt="Durag SVECU Ignition Units Ignition and Burner Control" title="Durag SVECU Ignition Units Ignition and Burner Control"/></div>		
			<div id="PartsContent"><h3>SVECU Ignition Units
			<br/><font color="#50658D">Ignition and Burner Control</font></h3>
			<p>Ignition and control unit for pilot burners and lances for flares.
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Pilots burners
			<br/>&#149; Ignition Lances
			<br/>&#149; Explosion proof version available
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_smitsvonk_products_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/Portable-Ignition-Units.gif" alt="Durag ortable Ignition Units" title="Durag ortable Ignition Units"/></div>			
			<div id="PartsContent"><h3>Portable Ignition Units</h3>
			<p>Lightweight, battery operated ignition unit.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Compact and lightweight unit weighs 
			<br/>&nbsp;&nbsp;only 6 kg
			<br/>&#149; High energy ignition
			<br/>&#149; Battery indicator
			<br/>&#149; Ergonomically designed, easy to handle
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Direct spark ignition of industrial 
			<br/>&nbsp;&nbsp;burners in absence of fixed igniters
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_smitsvonk_products_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="25" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/Mobile-Ignition-Units.gif" alt="Durag Mobile Ignition Units" title="Durag Mobile Ignition Units"/></div>	
			<div id="PartsContent"><h3>Mobile Ignition Units</h3>
			<p>Allows ignition of main burners at places where a fixed pilot burner or igniter is not available and a spark is insufficient to ignite the main burner.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; High energy ignition unit
			<br/>&#149; Insensitive to moisture and dirt
			<br/>&#149; Self-aspirating SST igniter
			<br/>&#149; Available for different gases and pressures
			<br/><br/><b>Applications:</b>
			<br/><br/>&#149; Furnaces
			<br/>&#149; Boilers
			<br/>&#149; Ground flares
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles/file/bro_smitsvonk_products_en.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Durag/D-GF150.gif" alt="Durag D-GF 150 Burner Controls" title="Durag D-GF 150 Burner Controls"/></div>			
			<div id="PartsContent"><h3>D-GF 150
			<br/><font color="#50658D">Burner Controls</font></h3>
			<p><br/>Self-monitoring and fail-safe burner control for the control of gas and oil burners as well as combined gas/oil burners of any capacity.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Controling and monitoring of gas and oil 
			<br/>&nbsp;&nbsp;burners of any capacity
    			<br/>&#149; Suitable for intermittent operation, 
			<br/>&nbsp;&nbsp;continuous operation and 72-hour 
			<br/>&nbsp;&nbsp;operation according to TRD 604
    			<br/>&#149; Integrated gas valve monitoring system
    			<br/>&#149; Separate outputs for control of gas and oil 
			<br/>&nbsp;&nbsp;fuel valves
    			<br/>&#149; Quick fuel change "on the fly" 
			<br/>&nbsp;&nbsp;without burner shut down
    			<br/>&#149; Adjustable pre-purge timer
    			<br/>&#149; Integrated flame monitor
    			<br/>&#149; Input for external flame monitor
    			<br/>&#149; Data interface
    			<br/><br/><b>Applications:</b>
    			<br/><br/>&#149; Chemical industry
    			<br/>&#149; Refineries
    			<br/>&#149; Cement plants
    			<br/>&#149; Waste incinerators
    			<br/>&#149; Steam generators
    			<br/>&#149; Heating plants
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.durag.com/userfiles//dgf150.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Product Brochure</b></font></a></p>
			
</div></div>
<div class="sliderGallery">
		<ul>
		<li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-GF150_Thumbnail.gif" border="0" alt="Durag D-GF 150 Burner Controls" title="Durag D-GF 150 Burner Controls"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-AM150_Thumbnail.gif" border="0" alt="Durag D-AM 150 Display Module" title="Durag D-AM 150 Display Module"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-GF55_Thumbnail.gif" border="0" alt="Durag D-GF 55 Burner Control" title="Durag D-GF 55 Burner Control"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LE603_Thumbnail.gif" border="0" alt="Durag D-LE 603 Flame Sensor" title="Durag D-LE 603 Flame Sensor"/></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LE703_Thumbnail.gif" border="0" alt="Durag D-LE 703 Flame Sensor With Optic System" title="Durag D-LE 703 Flame Sensor With Optic System"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/DE-LE701_Thumbnail.gif" border="0" alt="Durag D-LE 701 Flame Sensor With Fibre Optic System" title="Durag D-LE 701 Flame Sensor With Fibre Optic System"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-GT800_Thumbnail.gif" border="0" alt="Durag D-GT 800 Flame Sensor" title="Durag D-GT 800 Flame Sensor"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LE103_Thumbnail.gif" border="0" alt="Durag D-LE 103 Flame Sensor" title="Durag D-LE 103 Flame Sensor"/></a></li>
 		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LX200_Thumbnail.gif" border="0" alt="Durag D-LX 200 Compact Flame Monitor" title="Durag D-LX 200 Compact Flame Monitor"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LX720_Thumbnail.gif" border="0" alt="Durag D-LX 720 Compact Flame Monitor With Fibre Optic System" title="Durag D-LX 720 Compact Flame Monitor With Fibre Optic System"/></font></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LX100_Thumbnail.gif" border="0" alt="Durag D-LX 100 Compact Flame Monitor" title="Durag D-LX 100 Compact Flame Monitor"/></font></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-LX700_Thumbnail.gif" border="0" alt="Durag D-LX 700 Compact Flame Monitor With Fibre Optic System" title="Durag D-LX 700 Compact Flame Monitor With Fibre Optic System"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-UG120_Thumbnail.gif" border="0" alt="Durag D-UG 120 Control Unit" title="Durag D-UG 120 Control Unit"/></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-UG660_Thumbnail.gif" border="0" alt="Durag D-UG 660 Control Unit" title="Durag D-UG 660 Control Unit"/></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/AAL75_Thumbnail.gif" border="0" alt="Durag AAL 75 Flame Monitor" title="Durag AAL 75 Flame Monitor"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-IR55_Thumbnail.gif" border="0" alt="Durag D-IR 55 Flame Monitor" title="Durag D-IR 55 Flame Monitor"/></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-UV55_Thumbnail.gif" border="0" alt="Durag D-UV 55 Flame Monitor" title="Durag D-UV 55 Flame Monitor"/></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-HG400_Thumbnail.gif" border="0" alt="Durag D-HG 400 High Energy Ignition Device" title="Durag D-HG 400 High Energy Ignition Device"/></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-HG55_Thumbnail.gif" border="0" alt="Durag D-HG 55 Electronic Ignition Transformer" title="Durag D-HG 55 Electronic Ignition Transformer"/></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/D-VE500_Thumbnail.gif" border="0" alt="Durag D-VE 500 Pneumatic Retraction Unit" title="Durag D-VE 500 Pneumatic Retraction Unit"/></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/E-Light2000_Thumbnail.gif" border="0" alt="Durag E-LIGHT 2000 Ignition Units" title="Durag E-LIGHT 2000 Ignition Units"/></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/BWO-Ignition-Units_Thumbnail.gif" border="0" alt="Durag BWO Ignition Units Ignition and Burner Control" title="Durag BWO Ignition Units Ignition and Burner Control"/></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/SVECU-Ignition-Units_Thumbnail.gif" border="0" alt="Durag SVECU Ignition Units Ignition and Burner Control" title="Durag SVECU Ignition Units Ignition and Burner Control"/></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/Portable-Ignition-Units_Thumbnail.gif" border="0" alt="Durag Portable Ignition Units" title="Durag Portable Ignition Units"/></font></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Durag/thumbnails/Mobile-Ignition-Units_Thumbnails.gif" border="0" alt="Durag D-GF 150 Burner Controls" title="Durag D-GF 150 Burner Controls"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>
