<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Maxitrol combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Maxitrol,Poppet Design,Poppet Design,Straight Thru Flow Design,Lever Acting Design,Balanced Value Design,Zero Governers,Pilot Loaded Design,Two-Stage Regulators,Gas Filters,Ball Valves,Exa Star Modulating Valves,Redundant Combination Controls Redundant CV 100,Redundant Combination Controls Redundant CV 200,Redundant Combination Controls Redundant CV 300" />
<title>ETTER Engineering - Maxitrol Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="MaxitrolLogoLarge"></div>
<div id="SensusText">
<br/><br/>Maxitrol Company's headquarters is currently located in Southfield, 
Michigan with production facilities in Blissfield and Colon, Michigan. 
Throughout the years Maxitrol Company has undergone many changes and faced 
many challenges, but through the work of talented and dedicated employees, 
it continues to develop innovative technology for the natural gas industry.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Poppet-Design.gif" alt="Maxitrol Poppet Design" title="Maxitrol Poppet Design"/></div>       
			<div id="PartsContent"><h3>Poppet Design</h3>
    			<p><b>Description:</b>
			<br/>The poppet model RV Series is designed primarily for main burner and 
			pilot load applications. Typical applications include residential and commercial 
			cooking appliances, hearth products, pilot lines, and residential water heaters.  
			They are rated for up to &#189; psi (35 mbar).
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>RV12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/8"
			<br/>RV20&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/4", 3/8"
			<br/>RV47&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>RV48&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2". 3/4"
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum 
			gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/RVPO_MS_EN_03.2008-5.pdf"><font color="#ACB0C3"><b>Poppet Regulators</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Straight-Thru-Flow-Design.gif" alt="Maxitrol Straight Thru Flow Design" title="Maxitrol Straight Thru Flow Design"/></div>
			<div id="PartsContent"><h3>Straight Thru Flow Design</h3>
    			<p><b>Description:</b>
			<br/>Maxitrol's original straight-thru-flow design regulators are main burner 
			only, non-lockup type.  Typical applications include residential, commercial, 
			and industrial gas-fired appliances and equipment used on low/medium pressure 
			gas supply.
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>RV52(F)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>RV53&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>RV61&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1", 1 1/4"
			<br/>RV81&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 1/4", 1 1/2"
			<br/>RV91&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2",2 1/2"
			<br/>RV111&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 1/2", 3"
			<br/>RV131&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4" Flanged
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/RVSTF_MS_EN_03.2008-2.pdf"><font color="#ACB0C3"><b>STF Regulators</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Lever-Acting-Design.gif" alt="Maxitrol Lever Acting Design" title="Maxitrol Lever Acting Design"/></div>
			<div id="PartsContent"><h3>Lever Acting Design</h3>
    			<p><b>Description:</b>
			<br/>325 Series regulators are for 2 psi and 5 psi piping systems. Models available 
			for appliance main burner or pilot applications and as line pressure regulators.
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/><b>Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Model Size&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PSI</b>
			<br/>Appliance&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-3 (3/8", 1/2")
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-5A (1/2", 3/4", 1")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;10
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-7 (1 1/4", 1 1/2")
			<br/><br/>Line&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-3L (3/8", 1/2")
			<br/>Regulator&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-5AL (1/2", 3/4", 1")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-7L (1 1/4", 1 1/2")
			<br/><br/>Line&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-3L47 (3/8", 1/2")
			<br/>Regulator&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-3L48 (1/2")
			<br/>with OPD&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-5AL48 (1/2", 3/4")&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-5AL600 (3/4", 1")
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;325-7L210D (1 1/4", 1 1/2")
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/RVPO_MS_EN_03.2008-5.pdf"><font color="#ACB0C3"><b>Poppet Regulators</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Balanced-Valve-Design.gif" alt="Maxitrol Balanced Value Design" title="Maxitrol Balanced Value Design"/></div>
			<div id="PartsContent"><h3>Balanced Value Design</h3>
    			<p><b>Description:</b>
			<br/>R & RS regulators are for main burner and pilot load applications. They are 
			ideal for use in industrial applications such as infrared heaters and on pilot 
			lines for large process heaters and bakers. They may also be used in residential 
			applications as appliance regulators.
			<br/><br/>210 Series balanced valve design is a lockup-type regulator.  Its intended 
			applications include gas-fired boilers, steam generators, industrial furnaces, and 
			ovens. Remote sensing option is available on 210D, E, and G models (consult Maxitrol).
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>R400&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>R400(F)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>R400S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>R500&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>R500S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2",3/4"
			<br/>R600&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>R600S&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>210D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1", 1 1/4", 1 1/2"
			<br/>210ES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 1/2", 2"
			<br/>210G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 1/2", 3"
			<br/>210JS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4" Flanged
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/GPR210_MS_EN_09.2008-2.pdf"><font color="#ACB0C3"><b>Balanced Valve Regulators 210</b></font></a>
			<br/><a href="Maxitrol-pdfs/RS_MS_EN_08.2006-1.pdf"><font color="#ACB0C3"><b>Balanced Valve Regulators R Series</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Zero-Governors.gif" alt="Maxitrol Zero Governers" title="Maxitrol Zero Governers"/></div>
			<div id="PartsContent"><h3>Zero Governers</h3>
    			<p><b>Description:</b>
			<br/>Zero Governor regulators are used for flow control of burners, nozzle mixers, 
			mixing tees, and proportional pre-mixers
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>R400Z(F)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>R500Z&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>R600Z&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>210DZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1", 1 1/4", 1 1/2"
			<br/>210EZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 1/2", 2"
			<br/>210GZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 1/2", 3"
			<br/>210JZ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4" Flanged
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/GPRZER_MS_EN_11.2007-1.pdf"><font color="#ACB0C3"><b>Zero Governor Regulators</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Pilot-Loaded-Design.gif" alt="Maxitrol Pilot Loaded Design" title="Maxitrol Pilot Loaded Design"/></div>
			<div id="PartsContent"><h3>Pilot Loaded Design</h3>
    			<p><b>Description:</b>
			<br/>The 220 Series, utilizing a servo-operated design rather than the more conventional 
			spring-loaded design, delivers higher outlet pressure than spring loaded models.
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>220D&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1", 1 1/4", 1 1/1"
			<br/>220E&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1 1/2", 2"
			<br/>220G&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 1/2", 3"
			<br/>220J&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4" Flanged
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum 
			gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/GPR210220_MS_EN_11.2007-2.pdf"><font color="#ACB0C3"><b>210/220 Regulators</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Two-Stage-Regulators.gif" alt="Maxitrol Two-Stage Regulators" title="Maxitrol Two-Stage Regulators"/></div>       
			<div id="PartsContent"><h3>Two-Stage Regulators</h3>
    			<p><b>Description:</b>
			<br/>An ideal replacement for dual manifold systems, 2-stage regulator valves combine 
			gas pressure regulating and flame staging in a single unit. Applications include 
			direct-fired heaters with two-speed fans, hi-lo controls for door heaters, natural 
			gas switchover, and industrial processing.
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>SR400&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>SR400-1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>SR400-2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/8", 1/2"
			<br/>SR500&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR500-1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR400-2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR500W&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR500W-1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR500W-2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2", 3/4"
			<br/>SR600W&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>SR600W-1&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/>SR600W-2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4", 1"
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum 
			gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/SR_MS_EN_08.2006-1.pdf"><font color="#ACB0C3"><b>2-Stage Regulators</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Gas-Filters.gif" alt="Maxitrol Gas Filters" title="Maxitrol Gas Filters"/></div>
			<div id="PartsContent"><h3>Gas Filters</h3>
    			<p><b>Description:</b>
			<br/>Gas and air filters protect downstream controls from particulate contamination. 
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>GF40-1-44&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/2"
			<br/>GF60-1-66&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3/4"
			<br/>GF60-1-88&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1"
			<br/>GF80-1-1010&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 1/4"
			<br/>GF80-1-1212&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 1/2"
			<br/>GF80-1-1616&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases, 
			LP, gas-air mixtures, sewer gas, and air.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/GF_MBH_EN_08.2006-2.pdf"><font color="#ACB0C3"><b>Gas Filters</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Ball-Valves.gif" alt="Maxitrol Ball Valves" title="Maxitrol Ball Valves"/></div>       
			<div id="PartsContent"><h3>Ball Valves</h3>
    			<p><b>Description:</b>
			<br/>Maxitrol ball valves are designed primarily for gas, water, oil, and steam 
			applications.  They have a forged brass body, hard chrome plated ball, and 
			anticorrosion Dacromet treated handle. Female NPT inlet and outlet.
			<br/><br/><br/><b>Model and Pipe Sizes</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pipe Size
			<br/><br/>BV250 Series &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250T-22 (1/4") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-33 (3/8") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-44 (1/2") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-66 (3/4") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-88 (1") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-1010 (1 1/4") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-1212 (1 1/2") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-1616 (2") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-2020 (2 1/2") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV250-2424 (3") 
			<br/>BV250 Series &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602-33 (3/8")  
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602-44 (1/2") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602S-44 (1/2")  
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602-66 (3/4") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602S-66 (3/4") 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;BV602S-88 (1") 
			<br/><br/><b>Fluid Service:</b>
			<br/>Natural and manufactured gas, fuel oil No.1-6 (260&deg;F), 
			air, acetylene, nitrogen, carbon dioxide, inert gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/BV_MS_EN-2.pdf"><font color="#ACB0C3"><b>Manual Ball Valves</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Exa-Star-Modulating-Valves.gif" alt="Maxitrol Exa Star Modulating Valves" title="Maxitrol Exa Star Modulating Valves"/></div>       
			<div id="PartsContent"><h3>Exa Star Modulating Valves</h3>
    			<p>The new EXA STAR Modulating Valves: a 3/8" to 1" modulating valve. High precision, 
			low hysteresis, and custom output characteristics allow for desired process control. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/EXA_MS_EN_03.2010.pdf"><font color="#ACB0C3"><b>Poppet Regulators</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Redundant-CV-100.gif" alt="Maxitrol Redundant Combination Controls CV100" title="Maxitrol Redundant Combination Controls CV100"/></div>
			<div id="PartsContent"><h3>Redundant Combination Controls
			<br/><font color="#50658D">CV100</font></h3>
    			<p><b>Description:</b>
			<br/>The CV100 is used for applications with high capacity pilot lines such as gas 
			boilers and power burners.
			<br/><br/><br/><b>Model and Regulations</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Range of Regulation
			<br/><br/>CV100&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2,500 to 45,000 Btu/hr 
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/CV_CC_EN_06.2006-1.pdf"><font color="#ACB0C3"><b>CV condensed Catalog</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Redundant-CV-200.gif" alt="Maxitrol Redundant Combination Controls CV200" title="Maxitrol Redundant Combination Controls CV200"/></div>       
			<div id="PartsContent"><h3>Redundant Combination Controls
			<br/><font color="#50658D">CV200</font></h3>
    			<p><b>Description:</b>
			<br/>The CV200 is a redundant combination control used in direct spark or hot 
			surface ignition applications, eliminating a standing or continuous burning pilot flame.  
			Applications include direct vent gas-fired baseboard heaters, space heaters, wall furnaces, 
			water heaters, gas fireplaces, and commercial cooking appliances.
			<br/><br/><br/><b>Model and Regulations</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Range of Regulation
			<br/><br/>CV200&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2,500 to 45,000 Btu/hr 
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/CV_CC_EN_06.2006-1.pdf"><font color="#ACB0C3"><b>CV condensed Catalog</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxitrol/Redundant-CV-300.gif" alt="Maxitrol Redundant Combination Controls CV300" title="Maxitrol Redundant Combination Controls CV300"/></div>       
			<div id="PartsContent"><h3>Redundant Combination Controls
			<br/><font color="#50658D">CV300</font></h3>
    			<p><b>Description:</b>
			<br/>The CV300 is a redundant combination control similar to the CV200, but an optional 
			regulated or unregulated pilot is also available.  Applications include gas fires, 
			commercial cooking appliances, and patio heaters. 
			<br/><br/><br/><b>Model and Regulations</b>
			<br/><br/>Model&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Range of Regulation
			<br/><br/>CV300&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2,10,000 to 80,000 Btu/hr  
			<br/><br/><b>Gases:</b>
			<br/>Suitable for natural, manufactured, mixed gases, liquefied petroleum gases and LP gas-air mixtures.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="Maxitrol-pdfs/CV_CC_EN_06.2006-1.pdf"><font color="#ACB0C3"><b>CV condensed Catalog</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Poppet-Design-Thumbnail.gif" border="0" alt="Maxitrol Poppet Design" title="Maxitrol Poppet Design"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Straight-Thru-Flow-Design-Thumbnail.gif" border="0" alt="Maxitrol Straight Thru Flow Design" title="Maxitrol Straight Thru Flow Design"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Lever-Acting-Design-Thumbnail.gif" border="0" alt="Maxitrol Lever Acting Design" title="Maxitrol Lever Acting Design"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Balanced-Valve-Design-Thumbnail.gif" border="0" alt="Maxitrol Balanced Value Design" title="Maxitrol Balanced Value Design"/></font></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Zero-Governors-Thumbnail.gif" border="0" alt="Maxitrol Zero Governers" title="Maxitrol Zero Governers"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Pilot-Loaded-Design-Thumbnail.gif" border="0" alt="Maxitrol Pilot Loaded Design" title="Maxitrol Pilot Loaded Design"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Two-Stage-Regulators-Thumbnail.gif" border="0" alt="Maxitrol Two-Stage Regulators" title="Maxitrol Two-Stage Regulators"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Gas-Filters-Thumbnail.gif" border="0" alt="Maxitrol Gas Filters" title="Maxitrol Gas Filters"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Ball-Valves-Thumbnail.gif" border="0" alt="Maxitrol Ball Valves" title="Maxitrol Ball Valves"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Exa-Star-Modulating-Valves-Thumbnail.gif" border="0" alt="Maxitrol Exa Star Modulating Valves" title="Maxitrol Exa Star Modulating Valves"/></font></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Redundant-CV-100-Thumbnail.gif" border="0" alt="Maxitrol Redundant Combination Controls Redundant CV 100" title="Maxitrol Redundant Combination Controls Redundant CV 100"/></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Redundant-CV-200-Thumbnail.gif" border="0" alt="Maxitrol Redundant Combination Controls Redundant CV 200" title="Maxitrol Redundant Combination Controls Redundant CV 200"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxitrol/thumbnails/Redundant-CV-300-Thumbnail.gif" border="0" alt="Maxitrol Redundant Combination Controls Redundant CV 300" title="Maxitrol Redundant Combination Controls Redundant CV 300"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

