<a href="../contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com/"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
        	<li><a href="../commercial_boilers_burners.php">Burners & Boilers</a></li>
			<li><a href="../gas_boosters.php">Gas Boosters</a></li>
			<li><a href="../valve_trains.php">Valve Trains</a></li>
			<li><a href="../ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="../web_drying.php">Web Drying</a></li>
			<li><a href="../packaged_heaters.php">Package Heaters</a></li>
			<li><a href="../control_panels.php">Control Panels</a></li>
			<li><a href="../packaged-burners.php">Industrial Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="../parts_line_card.php">Parts Line Card</a></li>
			<li><a href="../parts_by_manufacturer_sensus.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="../safety_audits.php">Safety Audits</a></li>
			<li><a href="../spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="../literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="../philosophy.php">Philosophy</a></li>
			<li><a href="../jobs.php">Jobs</a></li>
            <li><a href="http://www.etterengineering.com/blog/">Blog</a></li>
			<li><a href="../news.php">News</a></li>
			<li><a href="../accolades.php">Accolades</a></li>
			<li><a href="../tech_tips.php">Technical Tips</a></li>
			<li><a href="../inside_the_job.php">Case Studies</a></li>
			<li><a href="../Newsletter-Archive.php">Newsletter Archive</a></li>
			<li><a href="../company.php">Company</a></li>
            <li><a href="../video_library.php">Videos</a></li>
			</ul>
	</li>
</ul>