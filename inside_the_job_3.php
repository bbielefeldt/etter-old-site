<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="ETTER Engineering,ETTER Engineering reviews,Inside the Job,ETTER Engineering Case Studies,gas booster reviews, natural gas booster review" />
<title>ETTER Engineering - Inside the Job</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SpectrumTransBLK"></div>
<div id="RedBrowseBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<div id="InsideTheJobLeftTxt">ETTER Engineering's Inside the Job offers you an insight into our most recent projects. Inside the Job is our way of providing you with the intricate details and workings that go into some of the most impressive ETTER products. As well as providing you with the success stories of many satisfied ETTER customers.
<br/><br/><br/>
<font size="2" color="#D21D1F"><b>Podojil Builders, Inc</b></font>
<br/><a href="inside_the_job.php"><font size="1"><b>- IHOP</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>PJKennedy &amp; Sons</b></font>
<br/><a href="inside_the_job_2.php"><font size="1"><b>- Boston Fish Pier</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>The Nutmeg Companies, Inc</b></font>
<br/><a href="inside_the_job_3.php"><font size="1"><b>- Connecticut Forensic Investigators</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>Frank I. Rounds</b></font>
<br/><a href="inside_the_job_4.php"><font size="1"><b>- Verizon Wireless</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>A&M Bronx Baking</b></font>
<br/><a href="inside_the_job_5.php"><font size="1"><b>- Oil-to-Gas Conversion</b></font></a></div>
<div id="InsidetheJob">
<div id="InsideForensicPhoto"></div>
<div id="NutmegLogo"></div>
<div id="Meriden"><b>Meriden, Connecticut - </b></div>
<div id="CTBoosterPhoto"></div>
<div id="CTBoosterPhoto2"></div>
<blockquote>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
When a major crime is committed in the State of Connecticut, investigators turn to the professionals at the state's Forensic 
Science Laboratory in Meriden for help. When the professionals at the Forensic Science Laboratory needed help, they turned to 
The Nutmeg Companies and ETTER Engineering.
<br/><br/>The Nutmeg Companies, a local general contracting firm, whose main focus is on public work (specifically, federal 
government renovation, repair, remodeling and new construction) throughout New England, New York, New Jersey, Maryland, 
Virgina, and Georgia, was hired to complete a two story, 22,000 square foot addition to the facility. The new space 
includes additional offices, mechanical and electrical rooms, as well as laboratory space. When it was determined 
the available street pressure of 5" W.C. was insufficient to supply the two hot water boilers, on hot water heater 
and two roof top units, the contractors called ETTER Engineering for help.
<br/><div id="MeridenPhotoOne"></div>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
The application called for a 9"W.C. boost to be provided, for which ETTER specified a pair of ENGB3 
(ETTER Next Generation Gas Booster) units designed to run in a lead-lag setup to balance run-time on 
each booster; the lead-lag setup also provides backup in the unlikely case of an emergency. Whereas 
other manufacturers were only willing to provide loose components, ETTER immediately understood the 
importance, from an ease of install and cost standpoint, of providing a complete system to Nutmeg. 
According to Project Manager Andy Beatty, ETTER's willingness to "[take] the initiative to look 
over the plans and specs as well as provide a complete skid-mounted, fully piped and wired package 
that our crews would only need to connect to" was a key facter in his decision to go with ENGB 
gasPODTM packaged booster system. When asked what he thought about working with ETTER on the 
project, Beatty replied, "ETTER was good to work with. Their submittals were timely and 
complete, the unit arrived when they said it would; we had several questions during 
construction that they answered, and [they] actually came out to the job to speak with 
our foreman. They started the unit within the time frame we asked for and provided the 
reports we needed when we needed them."
<br/><div id="MeridenPhotoTwo"></div>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/></blockquote>
</div>
<a href="inside_the_job_4.php" id="InsideJobNextBTN">Next</a>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>