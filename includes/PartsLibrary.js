        window.onload = function () {
            var container = $('div.sliderGallery');
            var ul = $('ul', container);
            
            var itemsWidth = ul.innerWidth() - container.outerWidth();
            
            $('.slider', container).slider({
                min: 0,
                max: itemsWidth,
                handle: '.handle',
                stop: function (event, ui) {
                    ul.animate({'left' : ui.value * -1}, 50);
                },
                slide: function (event, ui) {
                    ul.css('left', ui.value * -1);
                }
            });
        };
$(document).ready(function() {
   $('a.Product').click(function() {
      var popID = $(this).attr('rel'); 
      var popURL = $(this).attr('href');

      $('.popup_block_Parts').each(function(){ $(this).hide(); });

      $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend();
   });
});