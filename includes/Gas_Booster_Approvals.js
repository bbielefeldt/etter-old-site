$(document).ready(function() {
$('a.approvals[href^=#]').click(function() {
    var popID = $(this).attr('rel'); //Get Popup 
    var popURL = $(this).attr('href'); //Get Popup href to define size
    var query= popURL.split('?');
    var dim= query[1].split('&amp;');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_approvals"><img border="0" src="closeXsmall.gif" class="btn_close_approvals" title="Close Window" alt="Close" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    return false;
});
$('a.close_approvals, #fade').live('click', function() { //When clicking on the close or fade layer...
    $('#fade , .popup_block_approvals').fadeOut(function() {
        $('#fade, a.close_approvals').remove();  //fade them both out
    });
    return false;
});
});