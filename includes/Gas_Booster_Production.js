$(function() {
$('a.Production').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; 
 $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGB"><img border="0" src="closeXsmall.gif" class="btn_close_ENGB" title="Close Window" alt="Close" /></a>');
var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;

   $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn();  
return false;
});

$('a.close_ENGB, #fade').live('click', function() { 
    $('#fade , .ENGB_block').fadeOut(function() {
        $('#fade, a.close_ENGB').remove();  
    });
    return false;
});
});