
jQuery(document).ready(function() {
jQuery('a.SolidVideo').click(function() {
    var popID = jQuery(this).attr('rel'); 
    var popURL = jQuery(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&amp;');
    var popWidth = dim[0].split('=')[1];
    jQuery('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGBSolid"><img border="0" src="closeXsmall.gif" class="btn_close_ENGBSolid" title="Close Window" alt="Close" /></a>');
    var popMargTop = (jQuery('#' + popID).height() + 80) / 2;
    var popMargLeft = (jQuery('#' + popID).width() + 80) / 2;
    jQuery('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    jQuery('body').append('<div id="fade"></div>'); 
    jQuery('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn(); 
return false;
});
jQuery('a.close_ENGBSolid, #fade').live('click', function() {
    jQuery('#fade , .ENGBSolid_block').fadeOut(function() {
        jQuery('#fade, a.close_ENGBSolid').remove(); 
    });
    return false;
});
});