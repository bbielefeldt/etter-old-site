// Literature Accordion
jQuery(document).ready(function() {
	jQuery('div.accordionButton').click(function() {
		jQuery('div.accordionContent').slideUp('normal');	
		jQuery(this).next().slideDown('normal');
	});
	jQuery("div.accordionBContent").hide();

});
// Packaged Heat Accordion
jQuery(document).ready(function() {
	jQuery('div.PackagedaccordionButton').click(function() {
		jQuery('div.PackagedaccordionContent').slideUp('normal');	
		jQuery(this).next().slideDown('normal');
	});
	jQuery("div.PackagedaccordionBContent").hide();

});
// Google Analytics
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7328693-3']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
// Site Wide Navigation
var timeout         = 500;
var closetimer	    = 0;
var ddmenuitem      = 0;
function dropdown_open()
{	dropdown_canceltimer();
	dropdown_close();
	ddmenuitem = jQuery(this).find('ul').eq(0).css('visibility', 'visible');}
function dropdown_close()
{	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}
function dropdown_timer()
{	closetimer = window.setTimeout(jsddm_close, timeout);}
function dropdown_canceltimer()
{	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}}
jQuery(document).ready(function()
{	jQuery('#dropdown > li').bind('mouseover', dropdown_open);
	jQuery('#dropdown > li').bind('mouseout',  dropdown_timer);});
document.onmouseout = dropdown_close;
// Index Slideshow
jQuery(document).ready(function() {
   jQuery('.IndexSlideshow').cycle({
		fx: 'fade'
	});
});
jQuery('#Wrapper').append(html).children('#sm')
var slideMenu=function(){
	var sp,st,t,m,sa,l,w,sw,ot;
	return{
		build:function(sm,sw,mt,s,sl,h){
			sp=s; st=sw; t=mt;
			m=document.getElementById(sm);
			sa=m.getElementsByTagName('li');
			l=sa.length; w=m.offsetWidth; sw=w/l;
			ot=Math.floor((w-st)/(l-1)); var i=0;
			for(i;i<l;i++){s=sa[i]; s.style.width=sw+'px'; this.timer(s)}
			if(sl!=null){m.timer=setInterval(function(){slideMenu.slide(sa[sl-1])},t)}
		},
		timer:function(s){s.onmouseover=function(){clearInterval(m.timer);m.timer=setInterval(function(){slideMenu.slide(s)},t)}},
		slide:function(s){
			var cw=parseInt(s.style.width,'10');
			if(cw<st){
				var owt=0; var i=0;
				for(i;i<l;i++){
					if(sa[i]!=s){
						var o,ow; var oi=0; o=sa[i]; ow=parseInt(o.style.width,'10');
						if(ow>ot){oi=Math.floor((ow-ot)/sp); oi=(oi>0)?oi:1; o.style.width=(ow-oi)+'px'}
						owt=owt+(ow-oi)}}
				s.style.width=(w-owt)+'px';
			}else{clearInterval(m.timer)}
		}
	};
}();

