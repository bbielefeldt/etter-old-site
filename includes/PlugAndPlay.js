$(function() {
$('a.plugandplay').hover(function(e) {
		
		var html = '<div id="infoPartsBlock">'
		html +=    '<div id="popup_block_plugandplay">' 
		html +=	   '<div id="PlugandPlayTxtpopup">Plug & Play is an advanced feature which comes standard with many ETTER products. Plug & Play units come completely pre-piped, wired, and tested for your application. Just another way ETTER&#39;s products are designed to suit your individual needs.</div>';
		html +=		'</div>';
		html +=		'</div>';			
		$('#Wrapper').append(html).children('#infoTwo').hide().fadeIn(400);
	}, function() {
		$('#infoPartsBlock').remove();
	});
});