<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Parts Line Card</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
<script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript" src="includes/RolloverButtons.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="DropRight2"></div>
<div id="DropRight22"></div>
<div id="DropLeft2"></div>
<div id="PartsTopDrop"></div>
<div id="PartsBottomDrop"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="LightBlueBox"></div>
<div id="ETTERPartsLogo"></div>
<div id="OneStop"></div>
<div id="PartsHeader"><b>For All Your Combustion and Process Heating Needs!</b></div>
<div id="LineCard">
<div id="PartsList"><font color="#D21D1F" face= "Verdana" size="2"><b>Burners</b></font> - Direct and Indirect Air Heating - Furnace Type
 - Tube Firing - Atmospheric - Low and High Intensity - Infrared - Single and Multi-Burner Systems
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Combustion Components</b></font> - Combustion Air Blowers - Ignition Pilots and Accessories
 - Pressure Gauges - Spark Plugs - Flame Rods - Air/Gas Mixers - Gas/Air Filters
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Controllers</b></font> - Temperature - Pressure - Process - Automation - DIN/Hybrid/PLC Style
 - Single and Multi-Loop - Software and PC-Based Systems - Thermocouples - Sensors - Transducers
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Flame Safety</b></font> - Single Burner - Multi Burner
 - State of the Art PLC Processors and Touch Screen Controls - Flame Safeguard Sensors - First Outage Annunciation - Linkage-less Control Systems
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Gas Boosters</b></font> - Hermetic Gas Boosters - Packaged Booster Systems - Emergency Rental Boosters
 - Back Flow Valves - Integrated Control Systems and Panels
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Meters</b></font> - Natural Gas/LP/Oil/Water/Electric/Power - Diaphragm - Rotary - Sonic - Turbine
 - Vortex Shedding - Test Meters - Electronic Instrumentation and Remote Readout Packages - Data Logging and Software
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Pressure Switches</b></font> - Air Gas - Pressure and Vacuum - Single and Dual Gas Pressure
 - Flow Switches - Ventless Gas Pressure Switches - Pressure Transducers - Integrated Packages with Control Panels.
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Recorders</b></font> - Single Pen Digital and Analog - Multi Pen - Circular Chart - Strip Chart
 - Paperless Recording - Controlling - Profiling - Replacement Charts and Pens
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Regulators</b></font> - Natural Gas, LP, and other gases - Pressure Reducing - Industrial and Combustion
 - Positive Lockup Type - Low, Intermediate and High Pressures/Flows - Specialty, including OPCO, UPCO, and Thermal Trip
<br/><br/><font color="#D21D1F" face= "Verdana" size="2"><b>Valves</b></font> - Automatic and Safety Shut-Off Valves - Check Valves - Butterfly Valves
 - Actuator Motors - Solenoid Valves - Fire-Link Valves - Seismic (Earthquake) and other Specialty Valves - Packaged Valve Pipe Trains</div>
<div id="PartsHeader2"><font color="#494A4A" face= "Verdana" ><b>Along with parts, ETTER Engineering offers a variety of services and custom manufacturing Solutions</b></font></div>
<div id="PartsList2"><font color="#445679" face= "Verdana" size="3"><b>Services</b></font>
<br/><br/>&#149; Burner Tuning<br/>
&#149; Preventative Maintenance<br/>
&#149; On-Site Equipment Startups<br/>
&#149; Emergency Service and Repairs<br/>
&#149; NFPA-86 Combustion Safety Audits<br/>
&#149; Controls Upgrades and Process Tuning<br/>
&#149; Calibration and Complete Recorder Services</div>
<br/><br/><div id="PartsHeader3"><font color="#445679" face= "Verdana"><b>We have over 100 years of combined on-staff combustion experience</b></font></div>
<br/><br/><div id="PartsList3"><font color="#D12D1F" face= "Verdana" size="3"><b>Custom Manufacturing Solutions</b></font>
<br/><br/>&#149; Packaged Gas Booster Systems<br/>
&#149; Complete Regulator Station Design and Build<br/>
&#149; Custom Process Heating Packages and Applications<br/>
&#149; Engineered Specialty Valve Packages and Valve Trains<br/>
&#149; Energy and Metering Services, including In-Plant Sub-Metering, Data Acquisition, and Complete Station Design and Build</div>
<br/><br/><div id="PartsHeader4"><font color="#494A4A" face= "Verdana" size="2" ><b>We are a UL-508A Certified Panel Shop Specializing in Combustion Controls and Flame Safeguard Systems.</b></font></div>
<br/><br/><div id="PartsList4"><font color="#445679" face= "Verdana" size="2"><b>ETTER Serves a Wide Variety of Industries including:</b></font>
<div id="PartsList42"><table>
<tr><td>&#149; Process Heating</td><td>&#149; Powder Coating</td><td>&#149; HVAC</td></tr>
<tr><td>&#149; General Contracting</td><td>&#149; Pharmaceuticals</td><td>&#149; Textiles</td></tr>
<tr><td>&#149; Power and Combustion</td><td>&#149; Food and Beverage</td><td>&#149; Printing</td></tr>
<tr><td>&#149; Mechanical Contracting</td><td>&#149; Foundry and Casting</td><td>&#149; Coatings</td></tr>
</table></div></div></div>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" target="_blank" id="PrintableLineCard"></a>

<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>
