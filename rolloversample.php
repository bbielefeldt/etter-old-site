<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="description" content="ETTER Engineering's skid-mounted packaged gas booster systems arrive at your job site pre-piped, wired and tested - No more TECHNICAL RISK!" /><meta name="keywords" content="ENGB, ETTER Engineering, hermetic design,custom design,gas booster,gas boosters,gas pressure booster,gas pressure boosters,hermetic gas booster,hermetic gas boosters, packaged gas booster systems, skid-mounted packaged booster systems, heat exchanger packages" /><title>Natural Gas Boosters - Packaged Gas Booster Systems - Hermetic Gas Boosters</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
<script type="text/javascript" src="includes/Gas_Booster_Approvals.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function(){
        $('#submit').hover(
            function(){ // Change the input image's source when we "roll on"
                $(this).attr({ src : 'ViewApprovalsbuttonhit.gif'});
            },
            function(){ // Change the input image's source back to the default on "roll off"
                $(this).attr({ src : 'viewapprovalsbuttonOriginal.gif'});             }
        );
    });
</script>
</head>
	
<input type="image" name="submit" id="submit" src="viewapprovalsbuttonOriginal.gif">

</div>
</body>
</html>