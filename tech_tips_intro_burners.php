<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Technical Tips</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
</head>
<body link="#445679" vlink="#445679">
<script type="text/javascript">
google.load("jquery", "1");
</script>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="TechTipsLeftTxt">
<div id="TechTipsHeader"><font size="2" color="#445679"><b>Technical Tips</b></font></div>
<br/><a href="tech_tips.php" id="TechTipGoodVV"><font color="#ACB0C3"><b>&#149; The Good Old Vent Valve!</b></font></a>
<br/><a href="tech_tips_intro_burners.php"><font color="#ACB0C3"><b>&#149; Intro to Commercial and Industrial Burners</b></font></a>
<br/><a href="tech_tips_basic_burners.php"><font color="#ACB0C3"><b>&#149; Basic Burner Types</b></font></a>
<br/><a href="tech_tips_burner_tuning.php"><font color="#ACB0C3"><b>&#149; Basic Burner Tuning</b></font></a>
<br/><a href="tech_tips_burners_vs_clunkers.php"><font color="#ACB0C3"><b>&#149; Cash for Burners vs. Cash for Clunkers</b></font></a>
</div>
<div id="InsidetheJob">
<div id="TechRightHeader"><blockquote><font size="2" color="#D21D1F"><br/><b>Intro to Commercial and Industrial Burners</b></font></blockquote></div>
<br/><blockquote>By: Herb H Etter
<br/><br/>The number and variety of gas burner installations in commercial and industrial establishments defy any simple generalization. However, in this series of continuing editorial we will attempt to provide some useful tips that will apply to the maintenance and operation of typical burners. Many applications, such as boiler burners, have their own very specific requirements and codes, and it is not our intent to delve into detailed requirements but rather present  information that will be applicable to a wide variety of burner installations.
<br/><br/> The function of any burner is to convert and transfer energy to a process or product. As with any energy source, whether 
it be a toaster, a campfire or a huge gas burner, the potential for disaster caused by neglect or ignorance is very real.
<br/><br/>Our first topic covers the area of Safety, with the intent that the reader will research further as regards his specific 
installation.
<br/><br/><font size="2" color="#445679"><b>Burner Safety:</b></font>
<br/><br/>Various codes and regulations have proliferated over the years, and these can be divided into two arbitrary categories - 
governmental and commercial. Local state and federal codes can encompass a variety of things ranging from building permits to 
emissions. It goes without saying that your burner installation should comply with these requirements, even if all of them are 
not safety related.  The commercial type requirements would include any requirements of your gas utility company, plus those 
required by your insurance carrier. Fortunately, most insurers subscribe to a common set of requirements as set forth in publications 
of the National Fire Protection Association. NFPA bulletin 86 covers ovens and furnaces and many miscellaneous applications, 
and other individual bulletins cover specialized equipment such as boilers, etc. It is reasonable to assume that if your installation 
met the above requirements, it was installed safely. However, these requirements require continuing participation on your part, 
and do not end with the initial installation. Continued and regular maintenance and safety checks are an essential part of any 
safety program.
<br/><br/>At the low end of the spectrum there are old burner systems, presumably installed before many of today's requirements 
and technologies. Owners of these systems may think that since they have operated for years without a problem there is no cause 
to upgrade. They are aware that there is better and safer equipment available, but nobody has come around and told them to make 
any changes. They have probably never seriously faced the fact that a comparatively inexpensive upgrade would greatly enhance 
the safety of their business and property, not to mention the human considerations. This might be a good time for anyone with 
a questionable installation to check his or her installation against modern safety standards.
<br/><br/>The more typical installation probably meets most if not all of current safety requirements. The system includes 
electronic flame safety system, approved type gas valve train, interlocked pressure and flow switches, proper exhaust and 
vents, etc. This is the type of installation with which we will concern ourselves in this series. In future articles we 
will review in some depth the various components which comprise a safe system, and the care and feeding of same. 
</blockquote></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>