<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Sensus combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Sensus,Service Regulators Domestic 61R2,Service Regulators Domestic 143-80,Service Regulators Domestic 143-80,Service Regulators Commercial 243,Combustion Boiler Regulators,Sensus Service Regulators High Pressure 046,Self Operated Regulators Lower Pressure,Self Operated Regulators Medium Pressure,Self Operated Regulators High Pressure,Pilot Operated Regulators Service 243 RPC,Pilot Operated Regulators Intermediate Capacity,Pilot Operated Regulators High Capacity 441 VPC,Sonix Ultrasonic Meters Industrial / Commercial Meters,Diaphragm Meters Cubix and accuWAVE,Turbine Meters Auto Adjust Meters,Turbine Meters Mark II Meters,Turbine Meters TPL 9 and T 10" />
<title>ETTER Engineering - Sensus Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="SensusLogoLarge"></div>
<div id="SensusText">With operations and service facilities on five continents, Sensus is a global leader in utility infrastructure 
systems and resource conservation. Our company is rooted in 100 years of experience - and focused squarely on the future.
<br/><br/>Helping utilities and consumers make the most of finite water and energy resources takes more than just innovative 
technology and know-how. Sensus delivers reliable, flexible, and proven products and solutions that provide advanced measurement, 
data collection, analysis and control capabilities that help our customers improve operational efficiency while reducing their 
environmental impact.</div>
<div id="ThumbnailBackground"></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Domestic_61R2.gif" alt="Sensus Service Regulators Domestic 61R2" title="Sensus Service Regulators Domestic 61R2"/></div>
			<div id="PartsContent"><h3>Service Regulators
			<br/><font size="2" color="#50658D">Domestic 61R2</font></h3>
			<p><br/>Sensus Model 61R2 domestic service regulators can be used on both residential and small 
			commercial and industrial applications. With a variety of body sizes, loading springs, and 
			orifice sizes, these regulators can provide the outlet pressure ranges and capacities to 
			fit most applications for a 6" service diaphragm regulator. An internal relief valve (IRV) 
			is a standard feature for the 61R2. 
			<br/><br/>The Sensus Model 61R2 was previously manufactured as the Sensus Model 143-B.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34332/61R2-FINAL-10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>Model 61R2 Technical Data (TD-1308)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/496-61R2_IM_FINAL10-12-11indd.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>Model 496 &amp; 61R2 Installation and Maintenance (RM-1307)</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Domestic_143_80.gif" alt="Sensus Service Regulators Domestic 143-80" title="Sensus Service Regulators Domestic 143-80"/></div>
			<div id="PartsContent"><h3>Service Regulators
			<br/><font size="2" color="#50658D">Domestic 143-80</font></h3>
			<p><br/>Sensus Model 143-80 domestic service regulators can be used on both residential and small
			commercial and industrial applications. With a variety of body sizes, loading springs, and 
			orifice sizes, these regulators can provide the outlet pressure ranges and capacities to fit 
			most applications for a 6" service diaphragm regulator. The model 143-80 design provides the 
			popular union nut connection, not requiring bolts or screws to connect the body to the 
			diaphragm assembly. It is available with an internal relief valve (IRV), and with a low pressure 
			cutoff (LPCO). Simple, rugged, precise and outstanding performance.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/>
			<br/><a href="http://sensus.com/documents/10157/34301/143-80-FINAL-10-12-11" target="_blank"><font color="#ACB0C3"><b>Model 143-80 Technical Data (TD-1301)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34301/143-80-6-FINAL-10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>Model 143-80-6 LPCO Data Sheet (TD-1301-L)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://sensus.com/documents/10157/34293/Orifice%20Change%20Procedure%2001-16-08.pdf" target="_blank"><font color="#ACB0C3"><b>Model 143-80 Orifice Change Procedure</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/34285/143IM-FINAL-10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>143-80 Installation &amp; Maintenance (RM-1301)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Commercial_243.gif" alt="Sensus Service Regulators Commercial 243" title="Sensus Service Regulators Commercial 243"/></div>
			<div id="PartsContent"><h3>Service Regulators
			<br/><font size="2" color="#50658D">Commercial 243</font></h3>
			<p><br/>The Model 243 service regulator is a large capacity industrial service regulator designed for commercial, industrial, and gas distribution 
			use. As a result of the union connection between the fully interchangeable bodies and the diaphragm case, they have remarkable field versatility.
		 	They are easy to install, adjust, inspect, and service in all piping arrangements. The model 243 can be used in a variety of applications such 
			as: factories, foundries, district regulator stations, commercial laundries, and all types of gas fueled equipment including boilers, burners, 
			furnaces, ovens, heaters, kilns, and engines.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34340/R-1306%20R%2010%20Model%20243%20Sales%20Brochure.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243 Regulator Bulletin (R-1306)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34340/RDS-1306-2.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243-DOT Regulator Bulletin (RDS-1306-2)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://sensus.com/documents/10157/34361/Orifice%20Change%20Procedure%2001-16-08.pdf" target="_blank"><font color="#ACB0C3"><b>243 Orifice Change Procedure</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34361/RDS%201306-1%20R6.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243 Low Pressure Cutoff (RDS-1306-1)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34361/RDS-1500.pdf" target="_blank"><font color="#ACB0C3"><b>Monitor Regulator Installation (RDS-1500)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34400/RM-1306-1%20243%20I-M%2011-10-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243 Installation &amp; Maintenance (RM-1306-1)</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Combustion_Boiler_Regulators.gif" alt="Sensus Combustion Boiler Regulators" title="Sensus Combustion Boiler Regulators"/></div>
			<div id="PartsContent"><h3>Combustion Boiler Regulators</h3>
			<p><br/>Sensus industrial combustion regulators combine new technology with modern design to provide 
			greater capacity, higher inlet pressure, more accurate performance, and faster speed of response. 
			In most cases, this will allow the use of a smaller regulator. Both the Model 121 and Model 122 
			regulators have high strength, corrosion resistant, die-cast diaphragm cases and cast iron bodies, 
			assuring a highly functional regulator at a better price. Both models incorporate soft seat valve 
			material plus a precision machined "knife edge" orifice to provide a positive, tight shutoff. 
			Sensus Metering Systems' industrial combustion regulators can be used in a variety of commercial 
			and industrial applications including: burners, boilers, furnaces, air heaters, kilns, or gas 
			engines where fast response will improve performance.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34461/SENSUS%20R-1328%20REV.%209-WEB.pdf" target="_blank"><font color="#ACB0C3"><b>Model 121 Regulator Bulletin (R-1328)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34461/R-1329%20Model%20122%20R10%202009-12-18.pdf" target="_blank"><font color="#ACB0C3"><b>Model 122 Regulator Bulletin (R-1329)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://www.sensus.com/Module/Catalog/File?id=382" target="_blank"><font color="#ACB0C3"><b>Model 121 Pilot Operated - Pressure Loaded and V-Port Valve (RDS-1328)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34493/RM-1328%20R11%20Model%20121%202010-11-15.pdf" target="_blank"><font color="#ACB0C3"><b>Model 121 Installation &amp; Maintenance (RM-1328)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34493/RM-1329%20R8%2012-18-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 122 Installation &amp; Maintenance (RM-1329)</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/High_Pressure_046.gif" alt="Sensus Service Regulators High Pressure 046" title="Sensus Service Regulators High Pressure 046"/></div>
			<div id="PartsContent"><h3>Service Regulators
			<br/><font size="2" color="#50658D">High Pressure 046</font></h3>
			<p><br/>Sensus field and high pressure service regulators combine simplicity of design with rugged 
			construction, exceptional performance, and operational safety to provide dependable, flexible, 
			and economical answers for pounds-to-pounds pressure regulation applications.
			<br/><br/>
			The Model 046 family of high pressure regulators is easy to install, adjust, inspect, and service 
			in all piping arrangements. Typical applications include farm taps, field regulator applications, 
			and high pressure industrial air or gases.
			<br/><br/>
			The 2" Model 141-A regulator is designed for high pressure applications such as intermediate and 
			small volume loads on gas transmission lines. It is excellent for single and double stage reduction 
			ahead of the service regulator. The 141-A can be used on pipeline taps serving remote farm, domestic, 
			commercial, and industrial customers. It can also be used on other kinds of high pressure loads including 
			producer field work, high pressure burners and compressed air systems.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34440/R-1312%20R2%20046.pdf" target="_blank"><font color="#ACB0C3"><b>Model 046 Field Regulator Bulletin (R-1312)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34440/R-1311%20R10%2012-02-2010.pdf" target="_blank"><font color="#ACB0C3"><b>Model 141-A Field Regulator Bulletin (R-1311)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://sensus.com/documents/10157/34429/Orifice%20Change%20Procedure%2001-16-08.pdf" target="_blank"><font color="#ACB0C3"><b>046 Orifice Change Procedure</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34408/RM-1312%20R3%2012-17-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 046 Installation &amp; Maintenance (RM-1312)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34408/RM-1311%20R5%2012-17-2009.pdf" target="_blank"><font color="#ACB0C3"><b>Model 141-A Installation &amp; Maintenance (RM-1311)</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Lower_Pressure.gif" alt="Sensus Self Operated Regulators Lower Pressure" title="Sensus Self Operated Regulators Lower Pressure"/></div>
			<div id="PartsContent"><h3>Self Operated Regulators
			<br/><font size="2" color="#50658D">Lower Pressure</font></h3>
			<p><br/>The Model 461-S and Model 441-S family of regulators are balanced valve, spring type regulators 
			designed for distribution and industrial applications. They are extremely dependable with simple 
			design, sturdy construction, and fast response. Service and adjustment are easy and overall 
			operation is stable and sensitive.
			<br/><br/>
			The 461-S, 461-8S, and 461-12S models are ideal for distribution and industrial applications 
			where a single seat regulator is too small and the usual 2" balanced valve regulators are too 
			large. Their large exit areas give them a broad capacity, making them applicable to a wide 
			variety of load handling requirements.
			<br/><br/>
			But, when large loads are the requirement, the model 441-S regulators have the larger bodies 
			and valves to deliver the gas. Large, flexible diaphragms combined with accurately calibrated 
			springs enable these large capacity regulators to produce precise pressure control while 
			maintaining a high level of sensitivity and stability. The combination of their fast response, 
			dependability, and accuracy make the 441-S models ideal for monitoring, as well as other 
			applications where speed and accuracy are significant.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34543/R-1350%20R7%20Model%20441-S%2012-17-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-S Regulator (R-1350)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34543/R-1330%20R8%2008-17-2010.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-S, -8S, -12S Regulators (R-1330)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34535/RP-1350%20R2%20Model%20441-S%2012-17-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-S Parts List (RP-1350)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34514/RM1350_rev6.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-S Installation &amp; Maintenance (RM-1350)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34514/RM-1330%20R6%2012-17-2009.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-S, -8S, -12S Installation &amp; Maintenance (RM-1330)</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Medium_Pressure.gif" alt="Sensus Self Operated Regulators Medium Pressure" title="Sensus Self Operated Regulators Medium Pressure"/></div>
			<div id="PartsContent"><h3>Self Operated Regulators
			<br/><font size="2" color="#50658D">Medium Pressure</font></h3>
			<p><br/>The Model 461-57S and Model 441-57S are spring-operated regulators that incorporate a 
			"roll-out" diaphragm which approximates the performance of a pilot-operated regulator. The 
			roll-out diaphragm makes this exceptional performance possible because its action reduces 
			"droop" to a minimum. By offering near pilot-operated performance without a pilot, the 57S 
			regulators offer the advantages of simplicity, dependability, freedom from freeze-up, and 
			exceptionally fast response.
			<br/><br/>
			Both models are perfect for most intermediate and large capacity applications including gas 
			distribution systems, district regulator sets, city gate stations, town border stations, 
			monitoring, and a wide variety of industrial applications. These workhorses of the industry 
			have provided dependable service for over 60 years.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34564/R-1360_Model_441-57SColor.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-57S Regulator Bulletin (R-1360)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34564/R-1331%20REV8%2011-12-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-57S Regulator Bulletin (R-1331)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34585/RP-1360.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-57S Parts List (RP-1360)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34593/RM-1360_Model_441-57SColor.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-57S Installation &amp; Maintenance (RM-1360)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34593/RM-1331%20R10%2012-17-09.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-57S Installation &amp; Maintenance (RM-1331)</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/High_Pressure.gif" alt="Sensus Self Operated Regulators High Pressure" title="Sensus Self Operated Regulators High Pressure"/></div>
			<div id="PartsContent"><h3>Self Operated Regulators
			<br/><font size="2" color="#50658D">High Pressure</font></h3>
			<p><br/>The Model 461-X57 and Model 441-X57 are unique high pressure, large capacity spring operated 
			regulators that incorporate the same "roll-out" diaphragm principal that achieved such success 
			in the widely used 461-57S and 441-57S regulators. Both regulators offer pilot type performance 
			with spring-operated regulator simplicity. The roll-out diaphragm makes this exceptional 
			performance possible because its action reduces "droop" to a minimum. Offering fast response 
			and ease of installation, adjustment, and servicing, the 461-X57 and 441-X57 models are perfect 
			for most high pressure, large capacity applications including high pressure regulator sets, gas 
			distribution systems, town border stations, transmission systems, monitoring, and most high 
			pressure large capacity industrial applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34608/R-1361%20R4%20Model%20441-X57%202010-02-25.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-X57 Regulator Bulletin (R-1361)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34608/R1332-428.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-X57 Regulator Bulletin (R-1332)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34629/RP-1332%20Model%20461-X57.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-X57 Parts List (RP-1332)</b></font></a>
			<br/><br/>Installation Recommendation
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34637/RM-1361%20Model%20441-X57.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-X57 Installation &amp; Maintenance (RM-1361)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34637/RM-1332%20R3%20Model%20461-X57%202010-02-25.pdf" target="_blank"><font color="#ACB0C3"><b>Model 461-X57 Installation &amp; Maintenance (RM-1332)</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Service_243_RPC.gif" alt="Sensus Pilot Operated Regulators Service 243 RPC" title="Sensus Pilot Operated Regulators Service 243 RPC"/></div>
			<div id="PartsContent"><h3>Pilot Operated Regulators
			<br/><font size="2" color="#50658D">Service 243 RPC</font></h3>
			<p><br/>Sensus Model 243-RPC regulators are rugged, reliable pilot-operated regulators that incorporate 
			the same relay operation principle as the well-known 441-VPC. They offer exceptionally precise 
			regulation and are accurate to within &#177; 0.5% (absolute outlet pressure) from minimum to wide-open
			flow. The 243-RPC pilot-operated regulator is designed for fixed factoring, pressure factor 
			measurement, pressure compensated metering, and other applications requiring exceptionally 
			precise pressure control.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34658/R-1343.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243 RPC Regulators (R-1343)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34666/RM-1343%20Model%20243-RPC%20R6.pdf" target="_blank"><font color="#ACB0C3"><b>Model 243 RPC Installation &amp; Maintenance (RM-1343)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34666/ACF5909.pdf" target="_blank"><font color="#ACB0C3"><b>Orifice Change Procedure</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Intermediate_Capacity.gif" alt="Sensus Pilot Operated Regulators Intermediate Capacity" title="Sensus Pilot Operated Regulators Intermediate Capacity"/></div>
			<div id="PartsContent"><h3>Pilot Operated Regulators
			<br/><font size="2" color="#50658D">Intermediate Capacity</font></h3>
			<p><br/>Sensus Model 1100 and 1200 pilot operated regulators are designed to fit your specialized 
			intermediate capacity regulator needs. These regulators provide sensitivity and a high degree 
			of accuracy for fixed factor billing on modulating loads. Note, solenoid load applications are 
			not recommended. Load limiting regulators on the inlet supply to the pilot will improve the 
			accuracy of these products.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://sensus.com/documents/10157/34687/R-1341.pdf" target="_blank"><font color="#ACB0C3"><b>Model 1100 Bulletin (R-1341)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34687/R-1342.pdf" target="_blank"><font color="#ACB0C3"><b>Model 1200 Bulletin (R-1342)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34708/RP-1341%20R4%20Model%201100%202010-02-18.pdf" target="_blank"><font color="#ACB0C3"><b>Model 1100 Parts List (RP-1341)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34708/RP-1342%20R6%20Model%201200%202010-02-18.pdf" target="_blank"><font color="#ACB0C3"><b>Model 1200 Parts List (RP-1342)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/High_Capacity_441_VPC.gif" alt="Sensus Pilot Operated Regulators High Capacity 441 VPC" title="Sensus Pilot Operated Regulators High Capacity 441 VPC"/></div>
			<div id="PartsContent"><h3>Pilot Operated Regulators
			<br/><font size="2" color="#50658D">High Capacity 441 VPC</font></h3>
			<p><br/>Sensus Model 441-VPC pilot operated regulators are designed to fit your specialized large 
			capacity regulator needs. These regulators provide sensitivity, high capacity, and a high degree 
			of accuracy for fixed factor billing on modulating loads. The 441-VPC incorporates Sensus 
			Metering Systems' extremely successful 441 series regulators with a pilot for sensing regulated 
			pressure, and for controlling overall operation. Use of a 441-VPC is especially recommended 
			where extremely close control is required. These regulators can be remotely controlled by using 
			air loading in the pilot upper case. Because of the exceptionally close control offered by the 
			441-VPC family, they are ideal for applications such as district and station regulator sets, 
			distribution systems, transmission systems, compressor stations, and large capacity industrial 
			combustion systems where a high degree of accuracy is required. Note, solenoid load applications 
			are not recommended. Load limiting regulators on the inlet supply to the pilot will improve the 
			accuracy of these products.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34751/R-1370.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-VPC Regulator (R-1370)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34730/RP-1370-1%20R6%202010-02-19.pdf" target="_blank"><font color="#ACB0C3"><b>Type 511 Pilot Parts List (RP-1370-1)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34730/RP-1370-2%20R3%202010-02-18.pdf" target="_blank"><font color="#ACB0C3"><b>Type 521 &amp; 53 Pilot Parts List (RP-1370-2)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34759/RM-1370.pdf" target="_blank"><font color="#ACB0C3"><b>Model 441-VPC Installation &amp; Maintenance (RM-1370)</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Sonix_Ultrasonic_Meters.gif" alt="Sensus Sonix Ultrasonic Meters Industrial / Commercial Meters" title="Sensus Sonix Ultrasonic Meters Industrial / Commercial Meters"/></div>
			<div id="PartsContent"><h3>Sonix Ultrasonic Meters
			<br/><font size="2" color="#50658D">Industrial / Commercial Meters</font></h3>
			<p><br/>Using digital ultrasonic technology, Sonix industrial and commercial meters feature no moving 
			parts, virtually eliminating repair costs. Sonix meters retain their calibration and notify the 
			user if they are tampered with or if they malfunction. Sonix's attractive and compact design 
			afford flexible installation options. The electronic platform allows the user to include automatic 
			meter reading more cost effectively and upgrade the meter with the latest options while it is 
			still in service
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33434/Sonix12-16-25.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix 12 / 16 / 25</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33434/Sonix%202000%20(SS-G-SNX-20-00)" target="_blank"><font color="#ACB0C3"><b>Sonix 2000 (M-1040E -R5)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33434/Sonix%20600-880%20(SS-G-SNX-88-00)" target="_blank"><font color="#ACB0C3"><b>Sonix 600 (M-1033E)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33434/Sonix%20600-880%20(SS-G-SNX-88-00)" target="_blank"><font color="#ACB0C3"><b>Sonix 880 (M-1034E)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33434/CommunicationSonix.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix Remote Communications (M-1030G)</b></font></a>
			<br/><br/>Specification Selection Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33490/Sonix%202000%20meter%20Spec%20sheet%20R10%20080411.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix 2000</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33490/Sonix%20420%20600%20880%20meters%20Spec%20sheet%20rev13%20090911.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix 420 (12), 600 (16), and 880 (25)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://sensus.com/documents/10157/33511/M-1031C.pdf" target="_blank"><font color="#ACB0C3"><b>Features &amp; Benefits for Utilities (M-1031C)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33511/Proving_Sonix.pdf" target="_blank"><font color="#ACB0C3"><b>Proving the Sonix Meter (M-1030K)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33511/M-1030C.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix FAQ (M-1030C)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33511/M-1031BR3%20Operational%20Principals%202010-01-15.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix Operating Principles (M-1031B)</b></font></a>
			<br/><br/>Additional Product Information
			<br/><br/><a href="http://sensus.com/documents/10157/33552/Sonix%20Capacity%20Chart%20100816.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix Capacity Chart</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33552/Sonix-vs-rotary-meters.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix vs. Rotary Meters</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33552/SonixProvingInstructions.pdf" target="_blank"><font color="#ACB0C3"><b>Sonix2000 Proving Instructions</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33552/Sonix%202000%20Manual%20Supplement%20(DV%20Edit%20August08)6.pdf" target="_blank"><font color="#ACB0C3"><b>SonixCom Manual - Sonix 2000 Supplement</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33552/Sonix%20Operating%20Manual%202010-01-18.pdf" target="_blank"><font color="#ACB0C3"><b>SonixCom Operating Manual</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33552/Sonix%20Palm%202010-01-18.pdf" target="_blank"><font color="#ACB0C3"><b>SonixPalm Users Guide</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Diaphragm_Meters.gif" alt="Sensus Diaphragm Meters Cubix and accuWAVE" title="Sensus Diaphragm Meters Cubix and accuWAVE"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">Cubix and accuWAVE</font></h3>
			<p><br/>Sensus domestic/residential meters incorporate the latest design concepts with modern materials 
			for lighter weight, more durable meters with greater life expectancy.
			<br/><br/>
			The Cubix250 is based on a Sensus international gas meter design that boasts more than 10 million 
			units in service throughout the Europe and Asia. The North American version meets utility 
			standards and offers the same connection and index options as our larger residential-class 
			meters - at a smaller size and cost.
			<br/><br/>
			The revolutionary accuWAVE diaphragm is reestablishing Sensus as the leader in residential-class 
			meter accuracy and long-term performance. The patented accuWAVE diaphragm, now standard on Sensus 
			R-275, 315 and 415 series domestic meters, registers consumption with smooth precision while 
			incorporating advanced materials that ensure proof stability over time. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33616/Cubix%20Data%20Sheet%20FINAL.pdf" target="_blank"><font color="#ACB0C3"><b>Cubix250/MR-7 Data Sheet (M-1005 R3)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33616/M-1002%20R10%20Res%20Dia%20Meters%202009-12-21.pdf" target="_blank"><font color="#ACB0C3"><b>Models R-275, 315 and 415 (M-1002)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33616/M-1015%20R2%20Residential%20Diaphragm%20Meter%20Modules%202010-01-07.pdf" target="_blank"><font color="#ACB0C3"><b>Replacement Modules (M-1015)</b></font></a>
			<br/><br/>Information Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33657/Cubix%20FAQ%20Sheet%20FINAL.pdf" target="_blank"><font color="#ACB0C3"><b>Cubix250/MR-7 Frequently Asked Questions (M-1005 FAQ)</b></font></a>
			<br/><br/>Technical Papers
			<br/><br/><a href="http://sensus.com/documents/10157/33665/LPG-1000%20Propane%202010-01-05.pdf" target="_blank"><font color="#ACB0C3"><b>Measurement for Propane (LPG-1000)</b></font></a>
			<br/><br/>Brochures
			<br/><br/><a href="http://sensus.com/documents/10157/38495/accuWAVE%20Brochure.pdf" target="_blank"><font color="#ACB0C3"><b>accuWAVE R-275/315/415 (M-1002AW)</b></font></a>
			<br/><br/>Parts Lists
			<br/><br/><a href="http://sensus.com/documents/10157/33674/MP-1001F%20R4%20415%20and%20MR-12%20Parts%202010-01-06.pdf" target="_blank"><font color="#ACB0C3"><b>Model 415 and MR-12 Parts List (MP-1001F)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33674/MP-1002A%20R-275%20Parts%202009-08-20.pdf" target="_blank"><font color="#ACB0C3"><b>Models MR-8, R-275 and 315 Parts (MP-1002A)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33674/MP-1002C_R3_RC-230_and_RCM_Parts.pdf" target="_blank"><font color="#ACB0C3"><b>Models RC-230, and RCM-230 Parts (MP-1002C)</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Auto_Adjust_Meters.gif" alt="Sensus Turbine Meters Auto Adjust Meters" title="Sensus Turbine Meters Auto Adjust Meters"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">Auto Adjust Meters</font></h3>
			<p><br/>Sensus Auto-Adjust II Turbo-Meters provide highly accurate large volume 
			measurement using patented dual rotor technology that self-checks and automatically 
			adjusts meter performance. The AAT is virtually insensitive to deviations in upstream 
			flow conditions. Used with Auto-Adjust electronics, AAT technology provides advanced 
			warning of a deteriorating condition well before meter failure occurs.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33717/M-1073-R9.pdf" target="_blank"><font color="#ACB0C3"><b>(M-1073) T-18/27, T-35/57, T-60/90, T-140/230 (M-1073)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33717/M-1073-3PD.pdf" target="_blank"><font color="#ACB0C3"><b>(M-1073-3PD) T-18/27, T-35/57, T-60/90, T-140/230 (M-1073-3PD)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33717/MDS-1073-30D.pdf" target="_blank"><font color="#ACB0C3"><b>30 Degree Blade Tip Rotors (MDS-1073-30D)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33717/MI-310%20R3%20Turbo%20Corrector%202009-10-13.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Corrector (MI-310)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33717/MI-300%20R3%20Turbo%20Monitor%202009-10-13.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Monitor (MI-300)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33717/MI-320%20R3%20Turbo%20Prover%202009-10-13.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Prover (MI-320)</b></font></a>
			<br/><br/>Articles
			<br/><br/><a href="http://sensus.com/documents/10157/33871/SEAGD%20Sensus%20Atuo%20Adjust%20Turbo%20Meter.pdf" target="_blank"><font color="#ACB0C3"><b>Southeast Alabama Gas District (Pipeline &amp; Gas Technology)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/33779/MP-1073CM.pdf" target="_blank"><font color="#ACB0C3"><b>200mm, A200U30 &amp; A200U45 (MP-1073CM)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33779/4%20inch%20AAT-18%20and%2027%20Part%20MP-1073E.pdf" target="_blank"><font color="#ACB0C3"><b>Model AAT-18/27 (MP-1073E)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33779/8inch_AAT-60-and-90-BTS-Part%20MP-1073CS.pdf" target="_blank"><font color="#ACB0C3"><b>Model AAT-60/90 Blade Tip Sensors (MP-1073CS)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33779/12inch_AAT-140_and_230_MP-1073D_R5.pdf" target="_blank"><font color="#ACB0C3"><b>Model AAT-140/230 (MP-1073D)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33779/6%20inch%20AAT-35%20and%2057%20Part%20MP-1073B%20R3.pdf" target="_blank"><font color="#ACB0C3"><b>Model AAT-35/57 (MP-1073B)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33779/8inch_AAT-60-and-90-Part%20MP-1073C.pdf" target="_blank"><font color="#ACB0C3"><b>Model AAT-60/90 (MP-1073C)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/33899/Mini-AT%20Operators%20Guide.pdf" target="_blank"><font color="#ACB0C3"><b>Mercury Instruments Mini-AT</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33899/Turbo%20Corrector%20Operators%20Guide2010-11-02.pdf" target="_blank"><font color="#ACB0C3"><b>Mercury Instruments Turbo Corrector</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33899/Turbo%20Monitor%20Manual.pdf" target="_blank"><font color="#ACB0C3"><b>Mercury Instruments Turbo Monitor/Prover</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/MM-1070M.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Installation &amp; Maintenance (International)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/MM-1070%20R9%20New%20Sensus.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Installation &amp; Maintenance (MM-1070)</b></font></a>
			<br/><br/>Additional Product Information
			<br/><br/><a href="http://sensus.com/documents/10157/33920/MDS-1073.pdf" target="_blank"><font color="#ACB0C3"><b>AAT Advantages (MDS-1073)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33920/AAT%20Construction%20and%20Design%20Features%20mim-1073.pdf" target="_blank"><font color="#ACB0C3"><b>AAT Construction &amp; Design Features (MIM-1073)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/31649/Refurbishment%20Program%20R1.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Refurbishment Program</b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Mark_II_Meters.gif" alt="Sensus Turbine Meters Mark II Meters" title="Sensus Turbine Meters Mark II Meters"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">Mark II Meters</font></h3>
			<p><br/>The Mark II Turbo-Meter established Sensus Metering Systems as the leader in turbine meter gas 
			measurement. The 4 - 12" Turbos feature a top entry design that allows an interchangeable 
			measurement module to be removed from the meter body while the body remains in-line. All moving 
			parts are housed in a sealed chamber protected from line contaminants. The unique design of 
			the 45&#176; or 30&#176; blade angle rotor allows the meter to extract the maximum amount of kinetic 
			energy from flowing gas. Mark II Turbo-Meters accept a multitude of meter-mounted readout 
			devices and provide calibrated pulse outputs for electronic measurement.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/33971/M-1070-10.pdf" target="_blank"><font color="#ACB0C3"><b>Slot Sensor Pulser (M-1070-10)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33971/Mark%20II%20Meters%20M-1070%20R10.pdf" target="_blank"><font color="#ACB0C3"><b>T-18/27, T-35/57, T-60/90, T-140/230 (M-1070)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/33971/M-1070-3PD.pdf" target="_blank"><font color="#ACB0C3"><b>T-18/27, T-35/57, T-60/90, T-140/230 (M-1070-3PD)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/MM-1070M.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Installation &amp; Maintenance (International)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/MM-1070%20R9%20New%20Sensus.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Installation &amp; Maintenance (MM-1070)</b></font></a>
			<br/><br/>Meter Specifications
			<br/><br/><a href="http://sensus.com/documents/10157/34002/OIML_1070_METRIC.pdf" target="_blank"><font color="#ACB0C3"><b>Mark II &amp; Mark IIE OIML (OIML-1070-METRIC)</b></font></a>
			<br/><br/>Parts List
			<br/><br/><a href="http://sensus.com/documents/10157/34010/MP-1070AM.pdf" target="_blank"><font color="#ACB0C3"><b>100mm T100U45/30 MK II (MP-1070AM)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/MP-1070Bm.pdf" target="_blank"><font color="#ACB0C3"><b>150mm T150U45/30 MK II (MP-1070BM)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/4_inch_T-18_and_27_MP-1070A.pdf" target="_blank"><font color="#ACB0C3"><b>Model T-18/27 (MP-1070A)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/12%20inch%20T-120%20and%20230%20MP-1070D%20R10.pdf" target="_blank"><font color="#ACB0C3"><b>T-140/230 (MP-1070D)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/MP-1070DS.pdf" target="_blank"><font color="#ACB0C3"><b>T-140/230 Blade Tip (MP-1070DS)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/MP-1070AS.pdf" target="_blank"><font color="#ACB0C3"><b>T-18/27 Blade Tip (MP-1070AS)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/6inchT-35and57MP-1070B_R10.pdf" target="_blank"><font color="#ACB0C3"><b>T-35/57 (MP-1070B)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/MP-1070BS.pdf" target="_blank"><font color="#ACB0C3"><b>T-35/57 Blade Tip (MP-1070BS)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/8%20inch%20T-60%20and%2090%20MP-1070C%20R10.pdf" target="_blank"><font color="#ACB0C3"><b>T-60/90 (MP-1070C)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34010/8%20inch%20AAT-60%20and%2090%20BTS%20Part%20MP-1073CS%20R1.pdf" target="_blank"><font color="#ACB0C3"><b>T-60/90 Blade Tip (MP-1070CS)</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/TPL_9_and_T_10.gif" alt="Sensus Turbine Meters TPL 9 and T 10" title="Sensus Turbine Meters TPL 9 and T 10"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">TPL 9 and T 10</font></h3>
			<p><br/>The Sensus TPL-9 Turbo-Meter is a 90&#176; angled body meter that permits compact installations 
			with the meter inlet in either a horizontal or vertical plane. The Sensus T-10 Turbo-Meter is 
			a straight-through (wafer-style) meter designed for high pressure applications. Both 2" and 3" 
			meters come with cast steel bodies and interchangeable modules.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34122/M-1083.pdf" target="_blank"><font color="#ACB0C3"><b>Model T-10 - 2" and 3" (M-1083)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34122/M-1080%20R7%20TPL-9.pdf" target="_blank"><font color="#ACB0C3"><b>TPL-9 - 2" and 3" (M-1080)</b></font></a>
			<br/><br/>Parts Lists
			<br/><a href="http://sensus.com/documents/10157/34143/MP-1083.pdf" target="_blank"><font color="#ACB0C3"><b>Model T-10 Parts List (MP-1083)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34143/MP-1080B.pdf" target="_blank"><font color="#ACB0C3"><b>Model TPL-9 Parts List (MP-1080B)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34143/MP-1081.pdf" target="_blank"><font color="#ACB0C3"><b>TPL-9 Safety Interlock Parts List (MP-1081)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/34174/MP-1083-10.pdf" target="_blank"><font color="#ACB0C3"><b>T-10 Slot Sensor On Follower Magnet Assembly (M-1083-10)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34174/Safety%20Interlock%20Device%20MM-1081.pdf" target="_blank"><font color="#ACB0C3"><b>TPL-9 - 2" and 3" Safety Interlock Device (MM-1081)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/34174/TPL-9%20Insallation%20and%20Maintenance%20MM-1080.pdf" target="_blank"><font color="#ACB0C3"><b>TPL-9 Installation &amp; Maintenance (MM-1080)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/MM-1070%20R9%20New%20Sensus.pdf" target="_blank"><font color="#ACB0C3"><b>Turbo Installation &amp; Maintenance (MM-1070)</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Sensus/Service_Regulators_Domestic_496.gif" alt="Sensus Service Regulators Domestic 496" title="Sensus Service Regulators Domestic 496"/></div>
			<div id="PartsContent"><h3>Service Regulators
			<br/><font size="2" color="#50658D">Domestic 496</font></h3>
			<p><br/>Sensus Model 496 domestic service regulators can be used on both residential and small 
			commercial and industrial applications. The regulator is available with a variety of body 
			sizes, loading springs, and orifice sizes. The 4" roll-out diaphragm provides exceptional 
			performance, and provides capacities that normally require 6" diaphragm regulators. An 
			internal relief valve is a standard feature in the model 496. 
			<br/><br/>The Sensus Model 496 was previously manufactured as the Sensus Model 043-B.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>Data Sheets
			<br/><br/><a href="http://sensus.com/documents/10157/34254/496.pdf" target="_blank"><font color="#ACB0C3"><b>Model 496 Technical Data (TD-1307)</b></font></a>
			<br/><br/>Installation Recommendations
			<br/><br/><a href="http://sensus.com/documents/10157/30922/GeneralSafety-Regulators10-12-11.pdf" target="_blank"><font color="#ACB0C3"><b>General Safety Instruction for Gas Regulators (RM-1399)</b></font></a>
			<br/><a href="http://sensus.com/documents/10157/30922/496-61R2_IM_FINAL10-12-11indd.pdf" target="_blank"><font color="#ACB0C3"><b>Model 496 &amp; 61R2 Installation and Maintenance (RM-1307)</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Service_Regulators_Domestic_496_Thumbnail.gif" border="0" alt="Sensus Service Regulators Domestic 61R2" title="Sensus Service Regulators Domestic 61R2" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Domestic_61R2_Thumbnail.gif" border="0" alt="Sensus Service Regulators Domestic 143-80" title="Sensus Service Regulators Domestic 143-80" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Domestic_143_80_Thumbnail.gif" border="0" alt="Sensus Service Regulators Domestic 143-80" title="Sensus Service Regulators Domestic 143-80" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Commercial_243_Thumbnail.gif" border="0" alt="Sensus Service Regulators Commercial 243" title="Sensus Service Regulators Commercial 243" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Combustion_Boiler_Regulators_Thumbnail.gif" border="0" alt="Sensus Combustion Boiler Regulators" title="Sensus Combustion Boiler Regulators"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Pressure_046_Thumbnail.gif" border="0" alt="Sensus Service Regulators High Pressure 046" title="Sensus Service Regulators High Pressure 046" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Lower_Pressure_Thumbnail.gif" border="0" alt="Sensus Self Operated Regulators Lower Pressure" title="Sensus Self Operated Regulators Lower Pressure" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Medium_Pressure_Thumbnail.gif" border="0" alt="Sensus Self Operated Regulators Medium Pressure" title="Sensus Self Operated Regulators Medium Pressure" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Pressure_Thumbnail.gif" border="0" alt="Sensus Self Operated Regulators High Pressure" title="Sensus Self Operated Regulators High Pressure" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Service_243_RPC_Thumbnail.gif" border="0" alt="Sensus Pilot Operated Regulators Service 243 RPC" title="Sensus Pilot Operated Regulators Service 243 RPC" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Intermediate_Capacity_Thumbnail.gif" border="0" alt="Sensus Pilot Operated Regulators Intermediate Capacity" title="Sensus Pilot Operated Regulators Intermediate Capacity" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/High_Capacity_441_VPC_Thumbnail.gif" border="0" alt="Sensus Pilot Operated Regulators High Capacity 441 VPC" title="Sensus Pilot Operated Regulators High Capacity 441 VPC" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Sonix_Ultrasonic_Meters_Thumbnail.gif" border="0" alt="Sensus Sonix Ultrasonic Meters Industrial / Commercial Meters" title="Sensus Sonix Ultrasonic Meters Industrial / Commercial Meters" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Diaphragm_Meters_Thumbnail.gif" border="0" alt="Sensus Diaphragm Meters Cubix and accuWAVE" title="Sensus Diaphragm Meters Cubix and accuWAVE" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Auto_Adjust_Meters_Thumbnail.gif" border="0" alt="Sensus Turbine Meters Auto Adjust Meters" title="Sensus Turbine Meters Auto Adjust Meters" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/Mark_II_Meters_Thumbnail.gif" border="0" alt="Sensus Turbine Meters Mark II Meters" title="Sensus Turbine Meters Mark II Meters" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Sensus/thumbnails/TPL_9_and_T_10_Thumbnail.gif" border="0" alt="Sensus Turbine Meters TPL 9 and T 10" title="Sensus Turbine Meters TPL 9 and T 10" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

