<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<title>Natural Gas Boosters - ETTER Engineering Hermetic Gas Boosters - ENGB</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="description" content="The UL-Listed ENGB Hermetic gas booster from ETTER Engineering uses Next Generation Gas Booster Technology to achieve improved efficiency, decreased install time, and lower costs when compared with traditional gas booster designs." /><meta name="keywords" content="ENGB, ETTER Engineering, hermetic design,custom design,gas booster,gas boosters,gas pressure booster,gas pressure boosters,high pressure gas booster,high pressure gas boosters,hermetic gas booster,hermetic gas boosters" />
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->	

<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7328693-3']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>

<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript">
var timeout         = 500;
var closetimer		= 0;
var ddmenuitem      = 0;

function dropdown_open()
{	dropdown_canceltimer();
	dropdown_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');}

function dropdown_close()
{	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function dropdown_timer()
{	closetimer = window.setTimeout(jsddm_close, timeout);}

function dropdown_canceltimer()
{	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}}

$(document).ready(function()
{	$('#dropdown > li').bind('mouseover', dropdown_open);
	$('#dropdown > li').bind('mouseout',  dropdown_timer);});

document.onmouseout = dropdown_close;
</script>

<script type="text/javascript" src="slidemenu.js"></script>

<script type="text/javascript">
$('#Wrapper').append(html).children('#sm')
</script>

<script type="text/javascript">
$(document).ready(function() {
    $('.sm').mouseover({
		fx: 'fade'
	});
});
</script>
</head>

<div id="Wrapper">
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php"div id="Logo"></a>
<a href="index.php"div id="Tagline">to ALL your process heating & combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php"div id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="index.php"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Packaged Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
		</ul>
        	</li>
        	<li class="PartSales"><a href="parts_line_card.php"><b>Part Sales</b></a>
        	</li>
        	<li class="Services"><a href="#"><b>Services</b></a>
        		<ul class="sub_menu">
        			 <li><a href="safety_audits.php">Safety Audits</a></li>
        		</ul>
        	</li>
        	<li><a href="#"><b>Literature</b></a>
			<ul class="sub_menu">
        			 <li><a href="#">Launching Soon</a></li>
        		</ul>
        	</li>
        	<li class="AboutUs"><a href="#"><b>About Us</b></a>
        		<ul class="sub_menu">
        			 <li><a href="philosophy.php">Philosophy</a></li>
        			 <li><a href="jobs.php">Jobs</a></li>
        			 <li><a href="company.php">Company</a></li>
        			 <li><a href="http://etterengineering.com/blog/">ETTER Blog</a></li>
        		</ul>
        	</li>
        </ul>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="GasBoosterTransBLK"></div>
<div id="GasBoosterSolidWhiteBkgrd"></div>
<div id="LightBlueTopBkgrd"></div>
<div id="ENGBBoosterDropRightBlkTrans"></div>
<div id="LightBlueTopBkgrdDrop"></div>
<div id="GasBoosterIE6Photo"></div>
<div id="ENGBLabelContainer">



<body onload="slideMenu.build('sm',286,10,10,1)">
<ul id="sm" class="sm">
	<li><a href="gasPOD_boosters.php"><img border="0" src="images/1.gif" alt="" /><img border="0"></a></li>
	<li><a href="engb_boosters.php"><img border="0" src="images/2.gif" alt="" /></a></li>
	<li><a href="E101P_boosters.php"><img border="0" src="images/3.gif" alt="" /></a></li>
	<li><a href="E101PHC_boosters.php"><img border="0" src="images/4.gif" alt="" /></a></li>
	<li><a href="E101PHCXtra_boosters.php"><img border="0" src="images/5.gif" alt="" /></a></li>
	<li><a href="booster_accessories_duplex.php"><img border="0" src="images/6.gif" alt="" /></a></li>
</ul>


<div id="AlternateBoosterNav">
<a href="gasPOD_boosters.php" div id="gasPODTxtBTN"><font color=#808080><b>gasPOD</b></font></a>
<a href="engb_boosters.php" div id="ENGBTxtBTN"><font color=#808080><b>ENGB</b></font></a>
<a href="E101P_boosters.php" div id="E101PTxtBTN"><font color=#808080><b>E101-P</b></font></a>
<a href="E101PHC_boosters.php" div id="E101PHCTxtBTN"><font color=#808080><b>E101-PHC</b></font></a>
<a href="E101PHCXtra_boosters.php" div id="E101PHCXtraTxtBTN"><font color=#808080><b>E101-PHC-Xtra</b></font></a>
<a href="booster_accessories_duplex.php" div id="AccessoriesTxtBTN"><font color=#808080><b>Accessories</b></font></a></div>
<div id="GasBoosterHeader"><font color=#D21D1F><b>The ETTER Gas Booster Series</b></font></div>
<div id="GasBoosterTxt">ETTER Engineering has been providing process 
heating and combustion solutions since 1940. Our vast experience in 
the 20th century led us to develop our own series of  UL-Listed, 
hermetically-sealed gas boosters designed for the 21st century, with 
the flexibility to fit whatever specifications your application calls for.
<br>
<br>
ETTER manufactures a complete line of packaged gas booster systems - true 
"Plug and Play"&#8482; designs that arrive at your job site ready to begin work.
<br>
<br>
ETTER also offers FM-approved check valves and a variety of accessories and
services to ensure your gas booster installation is as quick, easy, and
cost-effective as possible.
<br>
<br>
For more information about each individual ETTER series please browse the 
corresponding series pages on the right
</div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" div id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" div id="TermsofService">Terms of Service</a>
<a href="site_map.php" div id="SiteMap">Site Map</a></div>
</div>
</body>
</html>
