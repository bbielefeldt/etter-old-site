<? error_reporting (E_ALL ^ E_NOTICE); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="description" content="Request a detailed ENGB natural gas booster price quote from ETTER Engineering for your next gas booster project!" /><meta name="keywords" content="ENGB, ETTER Engineering, hermetic design,custom design,gas booster,gas boosters,gas pressure booster,gas pressure boosters,hermetic gas booster,hermetic gas boosters, request a quote, price quote, price quotes" />
		<title>ETTER Engineering ENGB Gas Booster Sizing Form</title>
		
		<style type="text/css">
			
			body {
	font-family: "Book Antiqua", "Times New Roman", Times, serif;
	background-color: #000000;
				}
				
			li	{
				margin-bottom: 15px;
				}
			
			.nudge {
				margin-left: 30px;
				}
				
			.even {
				background-color: #ffffff;
				}
		body,td,th {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #FFFFFF;
}
.style1 {color: #000000}
.style5 {color: #FFFFFF}
        .style7 {color: #000000; font-weight: bold; }
        a:link {
	color: #FFFFFF;
}
a:visited {
	color: #FF0000;
}
.style25 {	color: #ffffff;
	font-family: Verdana, sans-serif;
	font-size: 12px;
}
.style27 {	font-size: 12px;
	font-family: Verdana, sans-serif;
	color: #ed2124;
}
        </style>
        <link href="favicon.ico" rel="shortcut icon" />
</head>
	<body>
<h1 align="left"><a href="http://www.etterengineering.com/engb"><img src="http://www.etterengineering.com/engb/images/ENGB_logo_sizeselectform.gif" alt="" width="300" height="94" hspace="50" border="0" /></a></h1>
<h1 align="center"> <img src="http://www.etterengineering.com/engb/images/gas_booster_sizingselection_form_text.jpg" alt="" width="568" height="32" hspace="200" /></h1>
<p align="left"></p>
<p align="left">&nbsp;</p>
<p>
  <?
			if( $_POST )
			{		
				foreach( $_POST as $key => $value )
				{
					if( $value == 'on' ) $value = "Yes";
					
					if( $value != '' )
						$msg .= ucwords( str_replace( '_', ' ', $key ) ) . ': ' . $value . "\n";
				} // foreach
				
				$to = "cdelsole@gmail.com";
				$subject = "Gas Booster Sizing and Selection Form";
				
				$_POST['email'] == '' ? $_POST['email'] : 'cdelsole@etterengineering.com';
				
				$headers = "From: " . $_POST['email'] . "\r\n";
				$headers .= "Reply-To: " . $_POST['email'] . "\r\n";
				$headers .= "Return-Path: " . $_POST['email'] . "\r\n";

				mail( $to, $subject, $msg, $headers );
				
				
				echo "Thank you for submitting the form. An ETTER Engineering Representative will contact you shortly concerning your inquiry. Click <a href='http://www.etterengineering.com/engb'>here</a> to go back to ETTER Engineering's ENGB website.";
				
				
			} // if
			
			else
			{
		?>
	  To receive a price quote for your project, please fill out the form below. An ETTER Engineering representative will contact you shortly to discuss your project in more detail.	</p>
<p>&nbsp;</p>
<form action="" method="POST">
			<ol>
				<li>
					<b>What type of booster are you looking for?</b><br />
					<span class="style5">
					<input type="radio" name="booster_type" value="Packaged Gas Booster System, ENGBP" /> 
					<a href="http://www.etterengineering.com/engb/engb_packaged_systems.html">Packaged Gas Booster System (ENGBP)</a>
					<input type="radio" name="booster_type" value="Stand Alone Booster with Control Panel" class="nudge" /> 
					Stand Alone Booster with Control Panel			    </span></li>
                <li><b align="left">What type of equipment is it servicing?</b> <small>(Check all that apply and complete capacity information)</small>
                  <table border="2" cellpadding="1" cellspacing="3">
                    <tr class="odd">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">
                        <input type="checkbox" name="boilers" /> 
                        Boilers            </span></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Qty: 
                        <input type="text" size="4" name="boilers_qty" />
                        </span></td>
                      <td width="263" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Total Capacity: 
                      <input type="text" size="20" name="boilers_capacity" />
                        </span></td>
                      <td width="169" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="boilers_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="boilers_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="even">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">
                        <input type="checkbox" name="hot_water_heaters" /> 
                        Hot Water Heaters						    </span></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">Qty: </span>
                        <span class="style7">
                        <input type="text" size="4" name="hot_water_heaters_qty" />
                        </span> </td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">Total Capacity: 
                        <input type="text" size="20" name="hot_water_heaters_capacity" />
                        </span> </td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="hot_water_heaters_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="hot_water_heaters_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="odd">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        
                        <div align="left" class="style7">
                          <input type="checkbox" name="rtus" /> 
                          RTUs (Roof Top Units)				            </div></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                      Qty: 
                      <input type="text" size="4" name="rtus_qty" />						  
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Total Capacity: 
                        <input type="text" size="20" name="rtus_capacity" />							
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="rtus_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="rtus_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="even">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">
                        <input type="checkbox" name="maus" /> 
                        MAUs (Makeup Air Units)							</span></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Qty: 
                      <input type="text" size="4" name="maus_qty" />						  
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Total Capacity: 
                      <input type="text" size="20" name="maus_capacity" />						  
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="maus_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="maus_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="odd">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7">
                        <input type="checkbox" name="gas_filled_generator" /> 
                        Gas Filled Generator							</span></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Qty: 
                        <input type="text" size="4" name="gas_filled_generator_qty" />							
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Total Capacity: 
                        <input type="text" size="20" name="gas_filled_generator_capacity" />							
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="gas_filled_generator_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="gas_filled_generator_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="even">
                      <td width="300" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style1">
                        <strong>Other:					          </strong>
                      <input type="text" name="other" />							
                      </span></td>
                      <td width="100" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Qty: 
                      <input type="text" size="4" name="other_qty" />						  
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style7">
                        Total Capacity: 
                      <input type="text" size="20" name="other_capacity" />						  
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="other_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="other_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                    
                    <tr class="odd">
                      <td colspan="2" bordercolor="#FFFFFF" bgcolor="#D1CECB"><span class="style1"></span> </td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style1"><b>Total Required Capacity:</b> 
                        <input type="text" size="20" name="total_capacity" />							
                        </span></td>
                      <td bordercolor="#FFFFFF" bgcolor="#D1CECB">
                        <span class="style7"><small>
                        <input type="radio" name="total_mbtuh" /> 
                        MBTUH 
                        <input type="radio" name="total_cfh" /> 
                        CFH</small> </span></td>
                    </tr>
                  </table>
                </li>
              <li>
		        <b>Equipment Location:</b><br />
		        <input type="radio" name="equipment_location" value="indoors" /> Indoors
		        <input type="radio" name="equipment_location" value="outdoors" class="nudge"/> Outdoors
		        <p>If outdoors, do you want a steel transclosure to protect the unit?
		          <input type="radio" name="steel_transclosure" value="yes" /> Yes
		          <input type="radio" name="steel_transclosure" value="no" /> No</p>
					  
				  <div style="margin-bottom: 5px;">Where is the booster relative to the equipment it is servicing?
					  <div style="padding-left: 15px; margin-top: 5px;">
					    <input type="radio" name="booster_relative" value="same_room" /> Same Room<br />
					    <input type="radio" name="booster_relative" value="nearby" /> Nearby, less than 50 feet<br />
					    <input type="radio" name="booster_relative" value="remote" /> Remote, more than 50 feet
					    <span class="nudge">How far away? <input type="text" name="remote_distance" /></span><br />
					    <input type="radio" name="booster_relative" value="explosion_proof" /> Explosion Proof Classified Area						</div>
				  </div>
			    </li>
              <li>
		        <b>Pressure Requirements:</b><br />
		        What is the supply pressure to the new gas booster in Inches of Water (" WC)? <input type="text" name="supply_pressure" /><br />
		        What is the HIGHEST minimum pressure supply required (" WC) to the equipment in Section 2 above? 
		        <input type="text" name="highest_minimum_pressure" /><br />
		        Do you need a steady regulated pressure from the gas booster? 
		        <input type="radio" name="steady_pressure" value="yes" /> Yes
		        <input type="radio" name="steady_pressure" value="no" /> No				</li>
              <li>
		        <b>Options: Check all that apply. Would you like:</b><br />
		        <input type="checkbox" name="option_gas_leak_detector" /> Gas Leak Detector<br />
		        <input type="checkbox" name="option_auto_bypass" /> Auto Bypass System, for installations where during parts of the year there is sufficient pressure without a booster to run<br />
		        <input type="checkbox" name="option_duplex_system" /> Duplex system, for critical redundancy? Typical for Hospitals, Healthcare, Schools, Hotels, etc.<br />
		        <input type="checkbox" name="option_stacked_duplex" /> Stacked Duplex system, for when space is at a premium<br />
		        <input type="checkbox" name="option_inlet_three_valve" /> Inlet three valve by-pass to the booster				</li>
              <li>
		        <b>Contact Information</b>
		        <table>
		          <tr>
		            <td width="110px" align="right">
		              Your Name:							</td>
					  <td>
					      <input type="text" name="name" />							</td>
					  <td align="right">
					      Company:							</td>
					  <td>
					      <input type="text" name="company" />							</td>
				    </tr>
		          
		          <tr>
		            <td align="right">
		              Phone:							</td>
					  <td>
					      <input type="text" name="phone" />							</td>							
					  <td align="right">
					      Fax:							</td>
					  <td>
					      <input type="text" name="fax" />							</td>
				    </tr>
		          
		          <tr>
		            <td align="right">
		              Email:							</td>
					  <td colspan="3">
					      <input type="text" name="email" size="65" />							</td>
				    </tr>
		          
		          <tr>
		            <td align="right">
		              Project Ref:							</td>
					  <td>
					      <input type="text" name="project_ref" />							</td>
							  
					  <td colspan="2">
						  City: <input type="text" name="city" />
						  State: <input type="text" name="state" size="4" />							</td>
				    </tr>
		          </table>
			    </li>
		  </ol>
			
	      <input type="submit" value="Submit" />
		</form>
		
	    <p>
	      <?
			}// else
		?>
	      </p>
	    <p align="center">&nbsp;</p>
	    <p align="left"><a href="http://www.etterengineering.com"><img src="http://www.etterengineering.com/engb/images/etter_diamond_for_web.jpg" alt="" width="155" height="81" hspace="50" vspace="0" border="0" /></a> <span class="style25">© 2008 </span><span class="style27"><a href="http://www.etterengineering.com">ETTER Engineering Company, Inc.</a></span><span class="style25"> 1486 Highland Avenue Cheshire, CT. 06410 PH: 800.444.1962 F: 203.272.9860</span></p>
</body>
</html>