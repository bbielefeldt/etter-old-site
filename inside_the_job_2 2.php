<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="ETTER Engineering,ETTER Engineering reviews,Inside the Job,ETTER Engineering Case Studies,gas booster reviews, natural gas booster review" />
<title>ETTER Engineering - Inside the Job</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<div id="InsideTheJobLeftTxt">ETTER Engineering's Inside the Job offers you an insight into our most recent projects. Inside the Job is our way of providing you with the intricate details and workings that go into some of the most impressive ETTER products. As well as providing you with the success stories of many satisfied ETTER customers.
<br/><br/><br/>
<font size="2" color="#D21D1F"><b>Podojil Builders, Inc</b></font>
<br/><a href="inside_the_job.php"><font size="1"><b>- IHOP</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>PJKennedy &amp; Sons</b></font>
<br/><a href="inside_the_job_2.php"><font size="1"><b>- Boston Fish Pier</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>The Nutmeg Companies, Inc</b></font>
<br/><a href="inside_the_job_3.php"><font size="1"><b>- Connecticut Forensic Investigators</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>Frank I. Rounds</b></font>
<br/><a href="inside_the_job_4.php"><font size="1"><b>- Verizon Wireless</b></font></a></div>
<div id="InsidetheJobBoston"><div id="InsideFishPierPhoto"></div>
<div id="PJKennedyLogo"></div>
<div id="Boston"><b>Boston, Massachuetts - </b></div>
<div id="BostonTransPhoto"></div>
<div id="BostonTransPhoto2"></div>
<blockquote>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<br/><div id="TextIndent">
For ETTER Engineering, providing a natural gas booster to the oldest continually operating active port in the Western Hemisphere 
is just another day on the job.
<br/><br/>
When Boston area mechanical contractors PJ Kennedy &amp; Sons was awarded the contract to </div><br/><br/><br/>upgrade the Boston Fish Pier's 
IT/UPS facilities - a job which required a natural gas booster to provide fuel for the facility's emergency backup generator - 
ETTER Engineering seemed to be a natural fit for the task. According to PJ Kennedy's Project Manager Bob Collins, "Working with 
ETTER Engineering was a positive experience throughout the project!"
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<div id="PhotoCaptionBostonOne"><font size="1"><b>ENGB gasPOD&#0153; in grey outdoor transclosure feeding emergency backup generator 
(green transclosure).</b></font></div>
<br/>Utilizing the company's UL-Listed ENGBP gasPOD&#0153; (Gas Pressure On Demand) packaged gas booster system, ETTER delivered a 
complete "Plug &amp; Play" package to the job site, making PJ Kennedy's job that much easier. Arriving on-site fully piped, wired and 
contained within an outdoor steel transclosure, the Boston Fish Pier gas booster required minimal installation time before it was 
up and running.
<br/><br/>
The ENGBP gasPOD&#0153; packaged gas booster system ETTER delivered will boost the incoming line pressure up 6.9" W.C. over the 
incoming supply for flows up to 3,400 SCFH, which is enough to ensure all the power needs for the new Massport IT/UPS room are 
met during a power failure.
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
<div id="PhotoCaptionBostonTwo"><font size="1"><b>ENGB2 with piping, wiring, and control panel inside outdoor steel transclosure.</b></font></div>
<br/>
Given the importance of the Port of Boston to the United States economy, the gravity of the job was noted early on by ETTER's 
engineers and field personnel, and the focus on getting the job done right did not escape the notice of JP Kennedy's project 
manager. "Your field staff (Norm Myers) should be commended for his professionalism and commitment to excellence, and ultimately 
for the quality of the end product being delieverd," Collins said.
<br/><br/>
The gas booster installation was part of a larger project, part which was supported by a $100,000 grant provided by the 
Massachusetts Department of Environmental Protection to reduce air pollution and increase electric power for boats docked 
at the Boston Fish Pier. Prior to this project, the facility was too small to dock the growing number of fishing boats. 
Since the boats had their fresh catch on board they could not be directly docked and patched in to landline power, it was 
necessary to keep their diesel engines running to support their refrigeration systems on board. When completed later this 
year, the upgrades to the facility will produce a 95 percent reduction in engine idling and air pollution. Now this is a 
"green project" ETTER is proud to be a part of!
<br/><br/><h5>To learn more about how ETTER Engineering can assist you on your next project, call 1-800-444-1962.
</h5></blockquote></div>
<a href="inside_the_job_3.php" id="InsideJobNextBTN">Next</a>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>