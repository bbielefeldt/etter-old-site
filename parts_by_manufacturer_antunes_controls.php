<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Antunes Controls combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Antunes Controls,Versa Plus Gas Pressure Switches,Model A Gas Pressure Switches,Model D Gas Pressure Switches,Model H Gas Pressure Switches,JD-2 Air Pressure Switches,SMD Air Pressure Switches" />
<title>ETTER Engineering - Antunes Controls Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="AntunesLogoLarge"></div>
<div id="SensusText">Antunes Controls is widely valued by engineers who require precision mechanism and their quality products can be found in virtually every building in the world. Plus, with only minor design improvements, their earliest switches are still recognized as industry standards. Antunes Controls currently designs and manufactures a broad range of:
<br/><br/>&#149; Air differential switches.
<br/>&#149; Gas pressure switches.
<br/>&#149; Temperature controllers.
<br/>&#149; Electronic timers. </div>
<div id="ThumbnailBackground"></div>

<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/Versa_Plus_Gas_Pressure_Switches.gif" alt="Antunes Controls Versa Plus Gas Pressure Switches" title="Antunes Controls Versa Plus Gas Pressure Switches"/></div>
			<div id="PartsContent"><h3>Versa Plus 
			<br/><font color="#50658D">Gas Pressure Switches</font></h3>
			<p><br/>These sturdy gas switches are made with a durable plastic enclosure and a die-cast aluminum inlet base. They accurately monitor gas pressure and break the electrical control when pressure rises above or drops below the desired set point. Available in manual or automatic reset operation. 
			<br/><br/>&#149; Adjustable gas pressure settings
			<br/>&#149; Ventless
			<br/>&#149; Mounts to any modular valve body
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Available in low to high ranges
			<br/>&#149; Cost-effective pricing			
			<br/><br/>The Versa Plus Gas Pressure Switch has replaced the older Model G Gas Pressure Switch please reference the link below for model conversions. 
			<br/><br/><a href="http://www.ajantunes.com/AntunesControls/Category.aspx?CategoryId=5#anchor53" target="_blank"><font color="#50658D"><b>Model G to Versa Plus Conversion Chart</b></font></a>
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020306 Rev A.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="2" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/Model_A_Gas_Pressure_Switches.gif" alt="Antunes Controls Model A Gas Pressure Switches" title="Antunes Controls Model A Gas Pressure Switches"/></div>	
			<div id="PartsContent"><h3>Model A 
			<br/><font color="#50658D">Gas Pressure Switches</font></h3>
			<p><br/>Model A high-low single gas switches monitor gas pressure and cut off the electrical control circuit when pressure rises above or drops below the desired set point. The high-low gas switches are adjustable. All models are available in reset or recycle type.
			<br/><br/>&#149; Mount horizontally without leveling
			<br/>&#149; Double switch available in combination of 
				&nbsp;&nbsp;high or low pressure ranges
			<br/>&#149; Rugged die-cast aluminum construction
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Cost-effective pricing
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020372.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="3" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/Model_D_Gas_Pressure_Switches.gif" alt="Antunes Controls Model D Gas Pressure Switches" title="Antunes Controls Model D Gas Pressure Switches"/></div>		
			<div id="PartsContent"><h3>Model D 
			<br/><font color="#50658D">Gas Pressure Switches</font></h3>
			<p><br/>These accurate and reliable switches feature a two-circuit control in which each set point controls two independent dry contacts to switch to two different voltages. The high and low pressure settings are adjustable and they are available in reset and recycle types.
			<br/><br/>&#149; Mounts horizontally without leveling
			<br/>&#149; Double switch may be ordered with any 
				&nbsp;&nbsp;combination of high or low pressure ranges
			<br/>&#149; Rugged die-cast aluminum construction
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Cost-effective pricing
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020375.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/Model_H_Gas_Pressure_Switches.gif" alt="Antunes Controls Model H Gas Pressure Switches" title="Antunes Controls Model H Gas Pressure Switches"/></div>		
			<div id="PartsContent"><h3>Model H 
			<br/><font color="#50658D">Gas Pressure Switches</font></h3>
			<p><br/>The Antunes Controls Model H switch offers reliability, repeatability and accuracy. Made of rugged die-cast aluminum, this high-pressure gas switch monitors gas pressure and breaks the electrical control circuit when the gas pressure rises above or drops below the desired set point.
			<br/><br/>&#149; Mounts horizontally without leveling
			<br/>&#149; Easily visible on/off indicator
			<br/>&#149; Available in automatic recycle or manual 
				&nbsp;&nbsp;reset
			<br/>&#149; Adjustable range: 1-15 PSI
			<br/>&#149; Max. surge pressure: 35 PSI
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Cost-effective pricing
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020373.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/JD-2_Air_Pressure_Switches.gif" alt="Antunes Controls JD-2 Air Pressure Switches" title="Antunes Controls JD-2 Air Pressure Switches"/></div>			
			<div id="PartsContent"><h3>JD-2
			<br/><font color="#50658D">Air Pressure Switches</font></h3>
			<p><br/>These compact, sensitive JD-2 switches are made of rugged die-cast aluminum and feature adjustable set point, set-point indication and visible on/off indicator. They're available in five range scales and deliver the accuracy and reliability you can depend on.
			<br/><br/>&#149; Rugged die-cast aluminum construction
			<br/>&#149; Custom design per OEM specifications 
				<br/>&nbsp;&nbsp;available
			<br/>&#149; Cost-effective pricing
			<br/>&#149; Dual scales calibrated in millimeters and 
				<br/>&nbsp;&nbsp;inches of water column
			<br/>&#149; Visible ON-OFF indicator
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020379.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/SMD_Air_Pressure_Switches.gif" alt="Antunes Controls SMD Air Pressure Switches" title="Antunes Controls SMD Air Pressure Switches"/></div>			
			<div id="PartsContent"><h3>SMD 
			<br/><font color="#50658D">Air Pressure Switches</font></h3>
			<p><br/>These sensitive SMD sheet metal air differentiators are compact, easy to install, accurate and reliable. The SMDs switches monitor positive, vacuum or differential air pressure.
			<br/><br/>&#149; Sensitive diaphragm
			<br/>&#149; Snap-action electrical switch, S.P.D.T. rated 
				&nbsp;&nbsp;at 10 amps
			<br/>&#149; Ranges available from .17" up to 12" W.C.
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Cost-effective pricing
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020376.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Antunes/Versa_Plus_Gas_Pressure_Switches.gif" alt="Antunes Controls Versa Plus Gas Pressure Switches" title="Antunes Controls Versa Plus Gas Pressure Switches"/></div>
			<div id="PartsContent"><h3>Versa Plus 
			<br/><font color="#50658D">Gas Pressure Switches</font></h3>
			<p><br/>These sturdy gas switches are made with a durable plastic enclosure and a die-cast aluminum inlet base. They accurately monitor gas pressure and break the electrical control when pressure rises above or drops below the desired set point. Available in manual or automatic reset operation. 
			<br/><br/>&#149; Adjustable gas pressure settings
			<br/>&#149; Ventless
			<br/>&#149; Mounts to any modular valve body
			<br/>&#149; Custom design per OEM specifications 
				&nbsp;&nbsp;available
			<br/>&#149; Available in low to high ranges
			<br/>&#149; Cost-effective pricing			
			<br/><br/>The Versa Plus Gas Pressure Switch has replaced the older Model G Gas Pressure Switch please reference the link below for model conversions. 
			<br/><br/><a href="http://www.ajantunes.com/AntunesControls/Category.aspx?CategoryId=5#anchor53" target="_blank"><font color="#50658D"><b>Model G to Versa Plus Conversion Chart</b></font></a>
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ajantunes.com/Content/pdf/1020306 Rev A.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a></p>
			
</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/Versa_Plus_Gas_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls Versa Plus Gas Pressure Switches" title="Antunes Controls Versa Plus Gas Pressure Switches"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/Model_A_Gas_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls Model A Gas Pressure Switches" title="Antunes Controls Model A Gas Pressure Switches"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/Model_D_Gas_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls Model D Gas Pressure Switches" title="Antunes Controls Model D Gas Pressure Switches"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/Model_H_Gas_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls Model H Gas Pressure Switches" title="Antunes Controls Model H Gas Pressure Switches"/></font></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/JD-2_Air_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls JD-2 Air Pressure Switches" title="Antunes Controls JD-2 Air Pressure Switches"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Antunes/thumbnails/SMD_Air_Pressure_Switches_Thumbnail.gif" border="0" alt="Antunes Controls SMD Air Pressure Switches" title="Antunes Controls SMD Air Pressure Switches"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>
