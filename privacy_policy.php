<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="robots" content="noindex"/>
<title>ETTER Engineering - Privacy Policy</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com/"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Package Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
			<li><a href="packaged-burners.php">Package Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="accolades.php">Accolades</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Case Studies</a></li>
			<li><a href="Newsletter-Archive.php">Newsletter Archive</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="PolicyHeadline">
ETTER Engineering Company, Inc. - Privacy Policy</div>
<div id="PrivacyPolicyText">
<blockquote>ETTER Engineering Company, Inc. has created this privacy statement to demonstrate our commitment to 
user privacy. Privacy on the ETTER Engineering web site (www.etterengineering.com) is of great importance 
to us. We gather important information from our visitors and customers, so we publish this statement 
as a way to communicate our information gathering and dissemination practices. We reserve the right 
to change this statement and will provide notification of the change at least thirty (30) business days 
prior to the change taking effect. The notice would include directions on how users may respond to the change.
<br/><br/><h5>Collected Information</h5>
<br/>We require people who register to use the services offered on our site to give us contact information. 
Depending on the service chosen, we ask for information such as their name, company name, address, phone number, 
e-mail address, and financial and billing information, such as billing name and address, and credit card number.
<br/><br/>ETTER Engineering uses the information that we collect to set up services for individuals. We may also use 
the information to contact users and prospects to further discuss their interest in ETTER Engineering Company, Inc., 
the services we provide and ways we can improve them. We may also email information regarding updates to the services 
we provide.
<br/><br/>We do not distribute or share customer email addresses. Customers can opt out of being contacted by us, or receiving 
information from us, at any time. Customers can opt out of receiving any communication by visiting our Contact Us page 
and requesting they be unsubscribed from any further communications.
<br/><br/>Except as we explicitly state at the time we request information, or as provided for in the 
ETTER Engineering Terms of Service, we do not disclose to any third party the information provided. 
All financial and billing information that we collect through the site is used solely to bill for services. 
This billing information is not used for marketing or promotional purposes.
<br/><br/>ETTER Engineering Company, Inc. uses a third party intermediary to manage online credit card processing. 
This intermediary is solely a link in the distribution chain, and is not permitted to store, retain, or 
use the information provided, except for the sole purpose of credit card processing. Other third parties, 
such as content providers, may provide content on the web site but they are not permitted to collect any 
information, nor does ETTER Engineering share any personally identifiable user information with these parties.
<br/><br/>Users of the service use the site to host data and information.
<br/>ETTER Engineering Company, Inc. will not review, share, distribute, print, or reference any such data except 
as provided in our Terms of Service, or as may be required by law. We will only view or access individual records
with your permission (for example, to resolve a problem or support issue). The only exception is when we are 
required by law.
<br/><br/>ETTER Engineering Company, Inc. may also collect certain information from visitors to and customers of the
site, such as Internet addresses. This information is logged to help diagnose technical problems, and to administer
our site in order to constantly improve the quality of the service. We may also track and analyze non-identifying 
and aggregate usage and volume statistical information from our visitors and customers and provide such information 
to third parties.
<br/><br/><br/><h5>Cookies</h5>
<br/>Cookies are files web browsers place on a computer's hard drive. They are used to help us authenticate users and 
provide time saving shortcuts and preferences. ETTER Engineering uses a persistent encrypted cookie if the user requests 
to save and retrieve their authentication information as well as other per-client device preferences. Upon initial visit 
to the site, ETTER Engineering stores in a session cookie information about the affiliate or promotion from which a site 
visitor originated. ETTER Engineering issues a mandatory session cookie to each user only to record encrypted authentication 
information for the duration of a specific session. If this cookie is rejected, access to and usage of the service will be 
denied.
<br/><br/><br/><h5>Third Party Sites</h5>
<br/>The site contains links to other web sites. ETTER Engineering is not responsible for the privacy practices or the 
content of these other web sites. Customers and visitors will need to check the policy statement of these others 
web sites to understand their policies. Customers and visitors who access a linked site may be disclosing their
private information. It is the responsibility of the user to keep such information private and confidential.
<br/><br/><br/><h5>Security</h5>
<br/>Our site has security measures in place to help protect against the loss, misuse, and alteration of the data
under our control. When our site is accessed using Netscape Navigator, or Microsoft Internet Explorer versions 5.0 
or higher, Secure Socket Layer (SSL) technology protects information using both server authentication and data
encryption to help ensure that data is safe and secure. ETTER Engineering also implements an advanced security 
method based on dynamic data and encoded session identifications, and hosts the site in a secure server environment
that uses a firewall and other advanced technology to prevent interference or access from outside intruders.
<br/><br/><br/><h5>Opt-Out Policy</h5>
<br/>ETTER Engineering offers its visitors and customers a means to choose how we may use information provided. 
If, at any time after registering for information or ordering Services, you change your mind about receiving 
information from us or about sharing information with third parties, please visit our unsubscribe.
<br/><br/><br/><h5>Correcting &amp; Updating Your Information</h5>
<br/>If users need to update or change registration information they may do so by editing the user or organization 
record. To discontinue the Service visit our unsubscribe pages as indicated in the Opt-Out Policy above.
<br/><br/><br/><h5>Additional Information</h5>
<br/>Questions regarding this Statement or the practices of this Site should be directed to ETTER Engineering.  
You may e-mail such questions to info@ETTER Engineering.com.</blockquote></div>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>