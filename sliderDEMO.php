<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title></title>
<link type="text/css" rel="stylesheet" href="ParaStyle2.css" />	
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>

    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"> </script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>


</head>
<body>
<div id="Wrapper">
<div id="PLC-Operator-Interface" class="popup_block_Parts">
			<h3>PLC Operator Interface</h3>
    			<p>Flameless Catalytic heating which has brought energy cost savings to numerous thermo formers and paint finishers, now has a well proven method of control - Gas Pulse System (GPS).
			<br/>The GPS unit is simple, cost effective and offers the option of multi zone configuration, permitting accurate and repeatable heating profiles for the catalytic heating system. Vulcan has pioneered Gas Pulse Technology, having installed many hundreds in both the finishing and thermo forming industries to control catalytic heaters. Typical gas industrial control valves, are totally unsuitable for controlling catalytic heaters. GPS has been proven to save gas consumption over other industry control valves.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/plc-operator-interface-2.gif" alt="Gas Booster Approvals"/></div>
			</div>       
<div id="Vert-Oven" class="popup_block_Parts">
			<h3>Catalytic Oven Applications</h3>
    			<p>Control of the GPS is via one of two operator control types. For systems with less than five control zones, a simple percentage timer may be used. For systems with a greater number of zones, a PLC based control with an intuitive operator interface, offers the customer the best solution for multi zone systems, with recipe storage, system diagnostics functions and global increase / decrease of any heating profile.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/vert-oven-lg.gif" alt="Gas Booster Approvals"/></div>
			</div>
<div id="Gas-Pulse" class="popup_block_Parts">
			<h3>Gas Pulse</h3>
    			<p>Gas pulse technology uses electrically actuated solenoid valves, to cycle the gas supply between flow rates of 100% and 20%. Longevity and reliability have been the hallmarks for this technology, and synchronizes very well with the mechanics of the flameless oxidation process within Vulcan's patented catalytic heaters. By alternating between the high and low flow rates, the entire depth of catalyst is used, ensuring an even heat distribution across entire heater face at all percentage settings.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/GasPulse_sm.gif" alt="Gas Booster Approvals"/></div>
			</div>
<div id="Dial" class="popup_block_Parts">
			<h3>Percentage Timer Dial</h3>
    			<p>With the exception of the UL-listing, the above approval agencies are region-specific, should your local agencies require any further documentation other than our UL-listing, please contact ETTER Engineering Toll Free 1-800-444-1962 for further assistance.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/dial.gif" alt="Gas Booster Approvals"/></div>
			</div>
<div id="Control-Panel" class="popup_block_Parts">
			<h3>Control Panel</h3>
    			<p>A Vulcan Catalytic heating system is controlled with state-of-the-art UL approved controls, which incorporate our unique gas pulse technology (GPS). Proved and tested for many years GPS is the most economical method of providing multi zone capability for catalytic ovens via PLC based controls. GPS carefully meters precise volume pulses of gas, ensuring ideal temperature settings while allowing for multi zoned heating arrays. The Vulcan Gas Pulse System incorporates a recipe-based menu which interfaces with digital output cards. Operators call up the recipe on the touch-sensitive PLC screen and the system automatically sets the control using the Gas Pulse System.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/controlP.gif" alt="Gas Booster Approvals"/></div>
			</div>
<div id="Catalytic-Heater" class="popup_block_Parts">
			<h3>Catalytic Heater</h3>
    			<p>When the catalytic heater is turned on the catalyst is preheated with a tubular electric heating element for approximately 15 minutes. Once the catalyst has reached 300F, safety devices are activated allowing gas to enter the back of the heater. The gas intermingles with the hot platinum catalyst and oxygen present in the surrounding air, starting an oxidation reduction reaction. This raises the catalyst temperature to between 350F and 900F, at the same time releasing water vapor and carbon dioxide. Efficiency tests have established that up to 80% of the gas is converted into infrared heat. Since the reaction temperature is well below the auto-ignition temperature for natural gas (1300 F), the reaction is flameless. The catalytic reaction is fully established five minutes after the gas enters the heater, and the pre heater is turned off.</p>
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/catalyticheater-01.gif" alt="Gas Booster Approvals"/></div>
			</div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="PLC-Operator-Interface" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/plc-operator-interface-2_Thumbnail.gif" border="0" /></a></li>
		<li><a href="#?w=400" rel="Vert-Oven" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/vert-oven-lg_Thumbnail.gif" border="0"/></a></li>
		<li><a href="#?w=400" rel="Gas-Pulse" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/GasPulse_sm_Thumbnail.gif" border="0"/></a></li>
		<li><a href="#?w=400" rel="Dial" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/dial_Thumbnail.gif" border="0"/></font></a></li>
		<li><a href="#?w=400" rel="Control-Panel" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/controlP_Thumbnail.gif" border="0"/></a></li>
		<li><a href="#?w=400" rel="Catalytic-Heater" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/catalyticheater-01-sm_Thumbnail.gif" border="0"/></a></li>
	  
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>


</div>
</body>
</html>





