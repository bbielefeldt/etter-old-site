<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Partlow combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Partlow,Series 1000 1/4 DIN Analog Controller/Limit Device,Series 1161+ 1/16 DIN Limit Controller,Series 1401+ 1/4 DIN Limit Controller,Series 1801+ 1/8 DIN Limit Controller,Series 1160+ 1/16 DIN Temperature Controller,Series 1400+ 1/4 Temperature Controller,Series 1800+ 1/8 Temperature Controller,MRC 5000 Basic Digital Circular Chart Recorder,Partlow MRC 7000 Recorder/Controller/Profiler,MRC 7700 RH Recorder/Controller/Profiler,MRC 7800 Flow Recorder,MRC 8000 Recorder/Controller,MRC 9400 VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms,MRC 9000 VersaChart (Full Version) High-End Recording Controller/Profiler,N79-79 Non Indicating Temperature Controller,ZF79 Non Indicating Temperature Controller,OL63X Non Indicating High Temp,O63X Non Indicating High Temp,OHL63X Non Indicating High Temp,N5-10X Non Indicating High Temp,ZFHL Non Indicating High Temp,RFT Recording Thermometer,RF15-79 Two Switch Recording Temperature Control,Pneumatic RFA Temperature Controller/Recorder,RFP Modulating Temperature Recorder,RFHTT Dual Recording Thermometer,RFH15-79/15-15 Dual Recording Temperature Control,RFC15-52 Recording Temperature Programmer,RFHAA Dual Recording Pnematic Temperature Control,RFC52 Recording Temperature Programmer" />
<title>ETTER Engineering - Partlow Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>\
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="PartlowLogoLarge"></div>
<div id="SensusText">Partlow offers a range of reputable brand name mechanical, 
analog and digital micro-based electronic temperature recorder, process controller and circle chart 
recorder products to satisfy a full range of temperature and process control validation industry 
applications involving temperature, pressure, level and flow. Over the last 80 years, Partlow has 
been supplying customers with temperature controller and circle chart recorder system solutions. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1000_1_4_DIN_Analog_Controller_Limit_Device.gif" alt="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" title="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" /></div>
			<div id="PartsContent"><h3>Series 1000 1/4
			<br/><font color="#50658D">DIN Analog Controller/Limit Device</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Series 1000 are versatile analog controllers designed for rugged, reliable, low cost control of process
			variables in harsh industrial environments, even those plagued by environmental electronic noise.
			<br/><br/>Available as a non-indicating analog on/off controller or high or low limit device, it offers a quality 
			level not commonly found in comparably priced devices. It is a 1/4 DIN instrument with a depth of only 5.8 
			inches. All versions are UL recognized and CSA certified as controllers and limit devices are FM approved.
			<br/><br/>The Series 1000 can accommodate J, K, T, R, B, and S thermocouples. Outputs are either SPST or SPDT 
			electro-mechanical relay or solid state relay drivers.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Non-display, easy dial setting
			<br/>&#149; 1/4 DIN panel mount
			<br/>&#149; Thermocouple input
			<br/>&#149; Sensor fault detection
			<br/>&#149; Relay or SSR driver output
			<br/>&#149; High or Low Limit or On/Off control
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/Series1000_DS705.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1161+_1_16_DIN_Limit_Controller.gif" alt="Partlow Series 1161+ 1/16 DIN Limit Controller" title="Partlow Series 1161+ 1/16 DIN Limit Controller" /></div>
			<div id="PartsContent"><h3>Series 1161+ 1/16  
			<br/><font color="#50658D">DIN Limit Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1161+ is part of a range of new generation '+' Series limit devices that share the same 
			distinctive styling as the '+' Series temperature controllers.
			<br/><br/>The expanded '+' Series limit controller platform includes 1/4, 1/8 and 1/16 DIN models that incorporate 
			numerous product specification, communication, display interface and software improvements that surpass 
			competitive limit device offerings in ease of use, delivery and valueper dollar characteristic of the 
			growing '+' Series product family.
			<br/><br/>By adding more versatile features and user-friendly functionality like digital inputs, an easy-to-use HMI, 
			jumperless and auto-hardware configuration, 24VDC transmitter power supply and MODBUS communication across 
			the range - the new generation Partlow + Series limit controllers transform the complicated into the simple 
			while saving you time (as much as 50% on product set-up), reducing inventory stock and virtually eliminating 
			the likelihood of operator errors.
			<br/><br/>The + Series limit controllers are affordable, well-featured, easy to use and adaptable with performance 
			features that work for you to make limit control simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; <b>NEW</b> Improved easy-to-use HMI
			<br/>&#149; <b>NEW</b> Jumperless input configuration
			<br/>&#149; <b>NEW</b> Auto-hardware recognition
			<br/>&#149; <b>NEW</b> Improved Windows PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Process alarms
			<br/>&#149; Optional digital input and remote reset
			<br/>&#149; Optional 10V SSR driver output
			<br/>&#149; Faster communication speeds in selectable 
			<br/>&nbsp;&nbsp;MODBUS or ASCII format
			<br/>&#149; FM Approved
			<br/>&#149; Backward compatible panel cutout, housing 
			<br/>&nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1161DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1401+_1_4_DIN_Limit_Controller.gif" alt="Partlow Series 1401+ 1/4 DIN Limit Controller" title="Partlow Series 1401+ 1/4 DIN Limit Controller" /></div>
			<div id="PartsContent"><h3>Series 1401+ 1/4  
			<br/><font color="#50658D">DIN Limit Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1401+ is part of a range of new generation '+' Series limit devices that share the same distinctive styling 
			as the '+' Series temperature controllers.
			<br/><br/>The expanded '+' Series limit controller platform includes 1/4, 1/8 and 1/16 DIN models that incorporate numerous product 
			specification, communication, display interface and software improvements that surpass competitive limit device offerings in 
			ease of use, delivery and valueper dollar characteristic of the growing '+' Series product family.
			<br/><br/>By adding more versatile features and user-friendly functionality like digital inputs, an easy-to-use HMI, jumperless 
			and auto-hardware configuration, 24VDC transmitter power supply and MODBUS communication across the range - the new generation 
			Partlow + Series limit controllers transform the complicated into the simple while saving you time (as much as 50% on product 
			set-up), reducing inventory stock and virtually eliminating the likelihood of operator errors.
			<br/><br/>The + Series limit controllers are affordable, well-featured, easy to use and adaptable with performance features that 
			work for you to make limit control simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; <b>NEW</b> Improved easy-to-use HMI
			<br/>&#149; <b>NEW</b> Jumperless input configuration
			<br/>&#149; <b>NEW</b> Auto-hardware recognition
			<br/>&#149; <b>NEW</b> Improved Windows PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Process alarms
			<br/>&#149; Optional digital input and remote reset
			<br/>&#149; Optional 10V SSR driver output
			<br/>&#149; Faster communication speeds in selectable 
			<br/>&nbsp;&nbsp;MODBUS or ASCII format
			<br/>&#149; FM Approved
			<br/>&#149; Backward compatible panel cutout, housing 
			<br/>&nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1401DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1801+_1_8_DIN_Limit_Controller.gif" alt="Partlow Series 1801+ 1/8 DIN Limit Controller" title="Partlow Series 1801+ 1/8 DIN Limit Controller" /></div>
			<div id="PartsContent"><h3>Series 1801+ 1/8  
			<br/><font color="#50658D">DIN Limit Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1801+ is part of a range of new generation '+' Series limit devices that share the same distinctive 
			styling as the '+' Series temperature controllers.
			<br/><br/>The expanded '+' Series limit controller platform includes 1/4, 1/8 and 1/16 DIN models that incorporate numerous 
			product specification, communication, display interface and software improvements that surpass competitive limit device 
			offerings in ease of use, delivery and value per dollar characteristic of the growing '+' Series product family.
			<br/><br/>By adding more versatile features and user-friendly functionality like digital inputs, an easy-to-use HMI, jumperless 
			and auto-hardware configuration, 24VDC transmitter power supply and MODBUS communication across the range - the new generation 
			Partlow + Series limit controllers transform the complicated into the simple while saving you time (as much as 50% on product 
			set-up), reducing inventory stock and virtually eliminating the likelihood of operator errors.
			<br/><br/>The + Series limit controllers are affordable, well-featured, easy to use and adaptable with performance features that 
			work for you to make limit control simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; <b>NEW</b> Improved easy-to-use HMI
			<br/>&#149; <b>NEW</b> Jumperless input configuration
			<br/>&#149; <b>NEW</b> Auto-hardware recognition
			<br/>&#149; <b>NEW</b> Improved Windows PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Process alarms
			<br/>&#149; Optional digital input and remote reset
			<br/>&#149; Optional 10V SSR driver output
			<br/>&#149; Faster communication speeds in selectable 
			<br/>&nbsp;&nbsp;MODBUS or ASCII format
			<br/>&#149; FM Approved
			<br/>&#149; Backward compatible panel cutout, housing 
			<br/>&nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1801DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1160+_1_16_DIN_Temperature_Controller.gif" alt="Partlow Series 1160+ 1/16 DIN Temperature Controller" title="Partlow Series 1160+ 1/16 DIN Temperature Controller" /></div>
			<div id="PartsContent"><h3>Series 1160+ 1/16  
			<br/><font color="#50658D">DIN Temperature Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1160+ is a new generation of '+' Series controller that takes flexibility and ease of use to new levels. The 
			expanded '+' Series platform of 1/4, 1/8 and 1/16 DIN model controllers incorporates numerous product specification, 
			communication, display interface and configuration software improvements that surpass competitive offerings in ease of use, 
			delivery and value per-dollar.
			<br/><br/>By adding more versatile features and user-friendly functionalities than ever before - like remote setpoint inputs, 
			digital inputs, plug-in output modules, a customizable operator/HMI menu, jumperless and auto-hardware configuration, 
			and 24VDC transmitter power supply - the new generation 1160+ Series controller transforms the complicated into the 
			simple while saving you time (as much as 50% on product set-up), reducing inventory stock and virtually eliminating 
			the likelihood of operator errors.
			<br/><br/>Making things complicated is easy - the clever trick with the new generation 1160+ Series controller from 
			Partlow is to make it simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Dual setpoints with optional remote 
			<br/>&nbsp;&nbsp;selection
			<br/>&#149; Plug-in output modules allow installing just 
			<br/>&nbsp;&nbsp;the functions needed
			<br/>&#149; Expanded user-selectable operator modes
			<br/>&#149; User-selectable plug-and-play output cards
			<br/>&#149; Process and loop alarms
			<br/>&#149; Adjustable hysteresis
			<br/>&#149; Optional 10V SSR driver
			<br/>&#149; Improved Windows&reg; PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Improved easy-to-use HMI
			<br/>&#149; Jumperless input configuration
			<br/>&#149; Auto-hardware recognition
			<br/>&#149; Faster communication speeds
			<br/>&#149; More security options
			<br/>&#149; Backward compatible panel cutout, housing &nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1160DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1400+_1_4_DIN_Temperature_Controller.gif" alt="Partlow Series 1400+ 1/4 Temperature Controller" title="Partlow Series 1400+ 1/4 Temperature Controller" /></div>
			<div id="PartsContent"><h3>Series 1400+ 1/4  
			<br/><font color="#50658D">Temperature Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1400+ is a new generation of '+' Series controller that takes flexibility and ease 
			of use to new levels. The expanded '+' Series platform of 1/4, 1/8 and 1/16 DIN model controllers 
			incorporates numerous product specification, communication, display interface and configuration 
			software improvements that surpass competitive offerings in ease of use, delivery and value per-dollar.
			<br/><br/>By adding more versatile features and userfriendly functionalities than ever before - like remote 
			setpoint inputs, digital inputs, plug-in output modules, a customizable operator/HMI menu, jumperless 
			and auto-hardware configuration, and 24VDC transmitter power supply - the new generation 1400+ Series 
			controller transforms the complicated into the simple while saving you time (as much as 50% on product 
			set-up), reducing inventory stock and virtually eliminating the likelihood of operator errors.
			<br/><br/>Making things complicated is easy - the clever trick with the new generation 1400+ Series 
			controller from Partlow is to make it simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Dual setpoints with optional remote 
			<br/>&nbsp;&nbsp;selection
			<br/>&#149; Plug-in output modules allow installing just 
			<br/>&nbsp;&nbsp;the functions needed
			<br/>&#149; Expanded user-selectable operator modes
			<br/>&#149; User-selectable plug-and-play output cards
			<br/>&#149; Process and loop alarms
			<br/>&#149; Adjustable hysteresis
			<br/>&#149; Optional 10V SSR driver
			<br/>&#149; Improved Windows&reg; PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Improved easy-to-use HMI
			<br/>&#149; Jumperless input configuration
			<br/>&#149; Auto-hardware recognition
			<br/>&#149; Faster communication speeds
			<br/>&#149; More security options
			<br/>&#149; Backward compatible panel cutout, housing &nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1400DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1800+_1_8_DIN_Temperature_Controller.gif" alt="Partlow Series 1800+ 1/8 Temperature Controller" title="Partlow Series 1800+ 1/8 Temperature Controller" /></div>
			<div id="PartsContent"><h3>Series 1800+ 1/8  
			<br/><font color="#50658D">Temperature Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Partlow 1801+ is part of a range of new generation '+' Series limit devices that share the same distinctive styling as 
			the '+' Series temperature controllers.
			<br/><br/>The expanded '+' Series limit controller platform includes 1/4, 1/8 and 1/16 DIN models that incorporate numerous product 
			specification, communication, display interface and software improvements that surpass competitive limit device offerings in 
			ease of use, delivery and value per dollar characteristic of the growing '+' Series product family.
			<br/><br/>By adding more versatile features and user-friendly functionality like digital inputs, an easy-to-use HMI, jumperless 
			and auto-hardware configuration, 24VDC transmitter power supply and MODBUS communication across the range - the new generation 
			Partlow + Series limit controllers transform the complicated into the simple while saving you time (as much as 50% on product 
			set-up), reducing inventory stock and virtually eliminating the likelihood of operator errors.
			<br/><br/>The + Series limit controllers are affordable, well-featured, easy to use and adaptable with performance features that 
			work for you to make limit control simple.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; <b>NEW</b> Improved easy-to-use HMI
			<br/>&#149; <b>NEW</b> Jumperless input configuration
			<br/>&#149; <b>NEW</b> Auto-hardware recognition
			<br/>&#149; <b>NEW</b> Improved Windows PC configuration 
			<br/>&nbsp;&nbsp;software
			<br/>&#149; Process alarms
			<br/>&#149; Optional digital input and remote reset
			<br/>&#149; Optional 10V SSR driver output
			<br/>&#149; Faster communication speeds in selectable 
			<br/>&nbsp;&nbsp;MODBUS or ASCII format
			<br/>&#149; FM Approved
			<br/>&#149; Backward compatible panel cutout, housing 
			<br/>&nbsp;&nbsp;and terminal wiring capability
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/P1800DS605.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_500_Basic_Digital_Circular_Chart_Recorder.gif" alt="Partlow MRC 5000 Basic Digital Circular Chart Recorder" title="Partlow MRC 5000 Basic Digital Circular Chart Recorder" /></div>
			<div id="PartsContent"><h3>MRC 5000  
			<br/><font color="#50658D">Basic Digital Circular Chart Recorder</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>Designed with the latest innovations in recording technology, enclosures and functionality, 
			the MRC 5000 is slim, trim and simple. The MRC 5000 is a digital circle chart recorder capable of 
			measuring, recording and controlling up to two process variables from a variety of inputs. Finding 
			a place to install this recorder is easy, with its compact panel depth and short protrusion from the 
			front panel. The mode switch, located next to the display, provides two functions. When in the 
			PROG/CAL/TEST position, it provides access to program parameters, calibration facilities and test 
			functions. Hardware options are matrix selectable and are shipped with default settings. The MRC 5000 
			directly connects to either thermocouple, RTD, mVDC, VDC, or mADC inputs. Changes in input type are 
			easily accomplished in the field through programming. This instrument has a universal power supply, 
			90-264VAC line voltage at 50-60Hz.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Easy and simple in every aspect of 
			<br/>&nbsp;&nbsp;operation: programming, testing 
			<br/>&nbsp;&nbsp;and calibration
			<br/>&#149; Standard easy read display saves setup 
			<br/>&nbsp;&nbsp;time, ensures proper configuration 
			<br/>&nbsp;&nbsp;and allows for field calibration
			<br/>&#149; Completely assembled slim/trim design no 
			<br/>&nbsp;&nbsp;assembly required reduces your panel 
			<br/>&nbsp;&nbsp;depth and saves you money
			<br/>&#149; Wide range of power operation
			<br/>&#149; More standard input selections, time 
			<br/>&nbsp;&nbsp;rotations and optional features
			<br/>&#149; Value priced basic recording
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 5000DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_7000_Recorder_Controller_Profiler.gif" alt="Partlow MRC 7000 Recorder/Controller/Profiler" title="Partlow MRC 7000 Recorder/Controller/Profiler" /></div>
			<div id="PartsContent"><h3>MRC 7000  
			<br/><font color="#50658D">Recorder/Controller/Profiler</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The MRC7000 Recorder/Controller is a microprocessor based circular chart instrument capable of measuring, displaying, 
			recording and controlling up to two process variables from a variety of inputs. Record and control functions, alarm settings 
			and other parameters are easily configured via the keys on the front cover and self prompting displays. All user entered data 
			can be protected against unauthorized changes via the MRC7000 Enable Mode security system.
			<br/><br/><b>Features and Benefits:</b>
			<br/>Micro based recording with controller or profiler options with programming setpoint profile capability in one instrument
			<br/>Two displays allow you to see critical process values at the same time (on 2 pen units)
			<br/>Easy, straightforward programming allows you to configure your recorder with a logical step-by-step process using a simple keypad
			<br/>Optional true time based profiling capability puts you in control of your process
			<br/>Reliability maintenance free recording for years to come
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 7000DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_7700_RH_Recorder_Controller_Profiler.gif" alt="Partlow MRC 7700 RH Recorder/Controller/Profiler" title="Partlow MRC 7700 RH Recorder/Controller/Profiler" /></div>
			<div id="PartsContent"><h3>MRC 7700  
			<br/><font color="#50658D">RH Recorder/Controller/Profiler</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The MRC7700 Relative Humidity Recorder is a microprocessor based circular chart profile recorder capable of measuring, 
			displaying, recording and controlling relative humidity and/or temperature using Dry Bulb and Wet Bulb temperatures from a 
			variety of inputs. Record and control functions, alarm settings and other parameters are easily configured via the keys 
			on the front cover and self prompting displays. NEMA 3 protection and sealed door lock optional.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Micro based recording controller 
			<br/>&nbsp;&nbsp;with relative humidity profile capability
			<br/>&#149; Two displays allowing you to see critical 
			<br/>&nbsp;&nbsp;process values at the same time
			<br/>&#149; Easy, straightforward programming allows 
			<br/>&nbsp;&nbsp;you to configure your recorder with a logical 
			<br/>&nbsp;&nbsp;step-by-step process using a simple 
			<br/>&nbsp;&nbsp;keypad
			<br/>&#149; True time based profiling capability puts 
			<br/>&nbsp;&nbsp;you in control of your process
			<br/>&#149; Reliability maintenance free recording for 
			<br/>&nbsp;&nbsp;years to come
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 7700DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC-7800-Flow-Recorder.gif" alt="Partlow MRC 7800 Flow Recorder" title="Partlow MRC 7800 Flow Recorder" /></div>
			<div id="PartsContent"><h3>MRC 7800  
			<br/><font color="#50658D">Flow Recorder</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The MRC 7800 Flow Controller is a microprocessor based circular chart recorder capable of measuring, displaying, 
			recording and totaling flow process variables. Record functions, alarm settings and other parameters are easily configured 
			from the front mounted keypad and self prompting displays. All user data can be protected from unauthorized changes by the 
			Enable Mode security system. The MRC7800 is protected against loss of data from AC power failures by a standard battery 
			backup system. Applications for the MRC 7800 include process validation, trend analysis, regulatory compliance and 
			product safety. Flow tracking is available with the optional totalizing capability.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Micro based recording controller
			<br/>&#149; Two displays, allowing you to see critical 
			<br/>&nbsp;&nbsp;process values at the same time
			<br/>&#149; Easy, straightforward programming allows 
			<br/>&nbsp;&nbsp;you to configure your recorder with a logical 
			<br/>&nbsp;&nbsp;step-by-step process using a simple 
			<br/>&nbsp;&nbsp;keypad
			<br/>&#149; High reliability eliminates cost of process 
			<br/>&nbsp;&nbsp;downtime
			<br/>&#149; Up to 8 relay or solid state outputs for use 
			<br/>&nbsp;&nbsp;as totalizer presets
			<br/>&#149; Up to 4 analog outputs for process 
			<br/>&nbsp;&nbsp;retransmission
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 7800DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_8000_Recorder_Controller.gif" alt="Partlow MRC 8000 Recorder/Controller" title="Partlow MRC 8000 Recorder/Controller" /></div>
			<div id="PartsContent"><h3>MRC 8000  
			<br/><font color="#50658D">Recorder/Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The MRC 8000 incorporates numerous refinements over its 10&#189; MRC 7000&#8482; sibling, including a 
			larger 12 inch recording area; an advanced platen assembly which eliminates slide wire feedback for improved reliability; as 
			well as the incorporation of optional isolated 4-20mA outputs to provide the highest resolution and accuracy available in a 
			recorder of this type. The MRC 8000 is offered in two versions: a basic recorder and a recording controller. Both may be 
			specified as either single pen or dual pen units. Each version is capable of measuring, recording and displaying temperature, 
			pressure, level, flow and other process variables. The MRC 8000 includes isolated universal inputs and a list of optional 
			capabilities including: transmitter power supply, 5A relay outputs, auxiliary inputs for remote setpoints, RS-485 communications, 
			door lock and NEMA 4X enclosure.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; 12&#189; Micro-based recording controller with 
			<br/>&nbsp;&nbsp;remote setpoint capability
			<br/>&#149; Two large displays allow you to see critical 
			<br/>&nbsp;&nbsp;process values at the same time on 2 
			<br/>&nbsp;&nbsp;pen models
			<br/>&#149; Easy, straightforward programming allows 
			<br/>&nbsp;&nbsp;you to configure your recorder with a 
			<br/>&nbsp;&nbsp;logical step-by-step process using a simple 
			<br/>&nbsp;&nbsp;keypad
			<br/>&#149; High resolution of recorder process 
			<br/>&nbsp;&nbsp;variables for better record accuracy
			<br/>&#149; Reliability maintenance free recording for 
			<br/>&nbsp;&nbsp;years to come
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 8000DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_9400_VersaEZ_Simplified_4_Pen_Recorder_with_Alarms.gif" alt="Partlow MRC 9400 VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms" title="Partlow MRC 9400 VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms" /></div>
			<div id="PartsContent"><h3>MRC 9400  
			<br/><font color="#50658D">VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The MRC 9000 VersaEZ will record up to four process variables on a circular chart. The circular chart 
			sizes are 10&#189;, 11&#189; or 12&#189; with the unit shipped to you configured to record on 12&#189; paper. The VersaEZ is 
			designed to provide you with a product that is user-friendly, and delivers a high quality unalterable 
			printout of your process data. Out of the box the VersaEZ is set up for most common applications. Given 
			that there are many variables in the world of process control, no one set-up will cover all needs. Yet 
			preconfigured as the VersaEZ is, it will minimize the amount of time you need to commit to programming. 
			You need only change the parameters necessary to get your process running and documented.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Pre-configured for you for E-Z installation 
			<br/>&nbsp;&nbsp;and out-of-the box, plug and play 
			<br/>&nbsp;&nbsp;capability
			<br/>&#149; Four pens and four colors
			<br/>&#149; Flexible chart sizes-choose from 10-12&#189; 
			<br/>&nbsp;&nbsp;diameter
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 9400DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/MRC_9000_VersaChart_Full_Version_Recording_Controller_Profiler.gif" alt="Partlow MRC 9000 VersaChart (Full Version) High-End Recording Controller/Profiler" title="Partlow MRC 9000 VersaChart (Full Version) High-End Recording Controller/Profiler"/></div>
			<div id="PartsContent"><h3>MRC 9000  
			<br/><font color="#50658D">VersaChart (Full Version) High-End Recording Controller/Profiler</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The VersaChart is perfect for demanding applications requiring a high performance recorder/controller 
			with custom ramp/soak capability. The Versachart provides powerful true time based profile capability and 
			exceptional value per data. Using advanced printing technology, the Versachart produces a circle chart in 
			four colors with alpha/ numeric chart characters for clear, accurate and readable charts that can be archived. 
			It is the only circle chart recorder to deliver up to 16 Profiles with up to 8 segments per profile. The profiling 
			feature provides closed loop PID process control with the added capability of configurable setpoints as a function 
			of time. Additionally, the profile feature includes actuators, parameters and a function block feature permitting 
			logic equations. The entire recorder (recorder, controller and profile feature) is easy to setup with its innovative 
			PC configuration port and available PC configuration software with security and single point data access features. 
			A recovery feature is also part of the profile option which enables users to choose a course of action in the advent 
			of power loss, preventing unnecessary process cycling. Finally, a guaranteed soak feature assures that the actual 
			process variable during soak is within a preselected tolerance for the soak time to increase product quality and 
			prevent waste/loss.
			<br/><br/><b>Features / Benefits:</b>
			<br/>&#149; Micro-based recorder/controller with profile 
			<br/>&nbsp;&nbsp;capability
			<br/>&#149; Four pens and four colors
			<br/>&#149; Flexible chart sizes choose from 10-12 
			<br/>&nbsp;&nbsp;diameter
			<br/>&#149; Two displays allowing you to see critical 
			<br/>&nbsp;&nbsp;process values at the same time
			<br/>&#149; Easy, straightforward PC programming 
			<br/>&nbsp;&nbsp;allows you to configure your recorder with 
			<br/>&nbsp;&nbsp;a logical step-by-step process using a 
			<br/>&nbsp;&nbsp;simple keypad
			<br/>&#149; True time based profiling capability 
			<br/>&nbsp;&nbsp;(including Four Process PID Loops, 
			<br/>&nbsp;&nbsp;Recovery Action, Guaranteed Soak)-and 
			<br/>&nbsp;&nbsp;up to 16 profiles puts you in control of your 
			<br/>&nbsp;&nbsp;process
			<br/>&#149; Reliability maintenance free recording for 
			<br/>&nbsp;&nbsp;years to come
			<br/>&#149; Excellent features/reasonable price
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/MRC 9000DS.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/N79_79_Non_Indicating_Temperature_Controller.gif" alt="Partlow N79-79 Non Indicating Temperature Controller" title="Partlow N79-79 Non Indicating Temperature Controller" /></div>
			<div id="PartsContent"><h3>N79-79 
			<br/><font color="#50658D">Non Indicating Temperature Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is a two-switch, non-indicating temperature control with unlimited differential 
			setting capabilities between switches. It is used where two fixed temperatures are specified 
			as control points and where a change or adjustment of those points is seldom required. UL and 
			CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>Temperature setting on the N79-79 may be set by the factory or the end user by adjusting 
			screws inside the control case in conjunction with an accurate temperature-sensing device 
			placed adjacent to the thermal sensing element.
			<br/><br/>Due to the construction of the instruments, the switches are held in a depressed position 
			and revert to their normal, or free state, in response to a temperature increase.
			<br/><br/>Therefore, as a mechanism function in this control, a normally open two-wire switch, or 
			the normally open side of a three-wire switch, operates as a normally closed circuit when the 
			bulb temperature is below set point.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/N79-79_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/ZF79_Non_Indicating_Temperature_Controller.gif" alt="Partlow ZF79 Non Indicating Temperature Controller" title="Partlow ZF79 Non Indicating Temperature Controller" /></div>
			<div id="PartsContent"><h3>ZF79  
			<br/><font color="#50658D">Non Indicating Temperature Controller</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is a compact single-switch industrial temperature controller designed to operate 
			fuel valves or relays which start and stop heating or cooling systems. It can also be used as a 
			limit device for sounding an alarm, actuating lights or to shut down equipment at a pre-determined 
			termperature. Compact, flush or wall mounted (brackets included), externally adjustable set point, 
			available in twelve optional ranges within -30&deg;F to 1100&deg;F. UL and CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>Temperature is set by turning the large, easy to handle setting knob on the front of the 
			instrument to the desired point on 200 angular degree semi-circular scale.
			<br/><br/>Expansion and contraction in the sensing element moves the switch-actuating lever through 
			a simple mechanical linkage. Optional weather resistance accessory (Acc 243) is available. 
			Adaptionof Acc 243 provides for inverted mounting and special gasketing for resistance to normal 
			outdoor climate conditions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/ZF79_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/OL63X_Non_Indicating_High_Temp.gif" alt="Partlow OL63X Non Indicating High Temp" title="Partlow OL63X Non Indicating High Temp" /></div>
			<div id="PartsContent"><h3>OL63X  
			<br/><font color="#50658D">Non Indicating High Temp</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is used to cut off a heat supply at any pre-determined temperature within 
			the overall range of -30&deg;F to 1100&deg;F. It operates in conjunction with a separate temperature 
			control to provide positive safety shutdown of industrial heating appliances. Reset button 
			and safety light on front cover, trip free switch, fully tamperproof. UL and FM listed. 
			Agency recognition is void if instrument is modified from factory standard.
			<br/><br/><b>Operation:</b>
			<br/>The high limit switch is manually reset to reactuate the equipment after shutdown. 
			It can be reset only after temperature has returned below the high-limit setting. The 
			switch is of a trip-free design, i.e. it will not function in the event the reset 
			button is tied or welded down, providing protection from tampering.
			<br/><br/>It is designed primarily for user applications where high temperature limitations 
			can be specified in advance of ordering , the OL63X is always factory preset to customer 
			requirements and the setting adjustment is factory sealed to prevent unauthorized changing. 
			<b>Note:</b> The element must be purchased with instrument to have switch set correctly. 
			Preset limit temperature is stamped on the rating plate affixed to the side of the control case.
			<br/><br/>A cover-mounted red signal light remains "on" during normal operation. Light is de-energized 
			the instant the high limit switch is activated. The instrument also is equipped with capstan cover 
			screws through which a wire may be threaded and secured with a lead seal.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/OL63X_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/O63X_Non_Indicating_High_Temp.gif" alt="Partlow O63X Non Indicating High Temp" title="Partlow O63X Non Indicating High Temp" /></div>
			<div id="PartsContent"><h3>O63X  
			<br/><font color="#50658D">Non Indicating High Temp</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is for OEM's. Identical to the OL63X in all respects except for the preset high temperature limit and 
			sealing of the setting adjustment, the O63X is listed under UL component recognition program. However, it does not 
			carry the UL label when it leaves the factory.
			<br/><br/>This modification of the OL63X is made to accommodate the original equipment manufacturer who, at the time of 
			ordering, has not determined the high temperature limit to be imposed on a given applicaiton.
			<br/><br/>The OEM is required to make his own temperature setting and to seal the setting adjustment screw to qualify 
			the limit device for incorporation in UL listed appliance.
			<br/><br/><b>Operation:</b>
			<br/>Identical to the OL63X in all respects except for the preset high temperature limit and sealing of the setting 
			adjustment - refer to OL63X operation specifications for more details.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/O63X_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/OHL63X_Non_Indicating_High_Temp.gif" alt="Partlow OHL63X Non Indicating High Temp" title="Partlow OHL63X Non Indicating High Temp" /></div>
			<div id="PartsContent"><h3>OHL63X  
			<br/><font color="#50658D">Non Indicating High Temp</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit incorporates element failure protection. It operates in conjunction with a primary temperature controller to provide positive safety shutdown of industrial heating appliances. If features fully tamperproof trip-free switch, reset button and signal light on front cover. It has both UL and FM listing. Agency recognition is void if instrument is modified from factory standard.
			<br/><br/><b>Operation:</b>
			<br/>The high limit switch is manually reset to reactuate the equipment after shutdown. It can be reset only 
			after temperature has returned below the high-limit setting. The switch is of a trip-free design, i.e. it 
			will not function in the event the reset button is tied or welded down, providing protection from tampering.
			<br/><br/>Designed primarily for user applications where high temperature limitations can be specified in advance 
			of ordering , the OHL63X is always factory preset to customer requirements and the setting adjustment is factory 
			sealed to prevent unauthorized changing. <b>Note:</b> The element must be purchased with instrument to have 
			switch set correctly. Preset limit temperature is stamped on the rating plate affixed to the side of the control case.
			<br/><br/>The element failure switch, connected in series with the high limit switch, is factory set are a point below normal 
			ambient, at 50&deg;F, unless otherwise specified. This switch will prevetn appliance start-up if ambient temperature is lower 
			than switch setting or if the instrument's thermal element has lost some of its fill.
			<br/><br/>A cover-mounted amber signal light remains energized during normal operation, while the light is de-energized the 
			instant the high limit switch is activated. The instrument also is equipped with capstan cover screws through which a 
			wire may be threaded and secured with a lead seal.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/OHL63X_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/N5_10X_Non_Indicating_High_Temp.gif" alt="Partlow N5-10X Non Indicating High Temp" title="Partlow N5-10X Non Indicating High Temp" /></div>
			<div id="PartsContent"><h3>N5-10X  
			<br/><font color="#50658D">Non Indicating High Temp</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is designed to operate in conjunction with a primary temperature controller to provide positive 
			safety shutdown of industrial heating systems.
			<br/><br/>The instrument also contains a fail-safe feature in the event temperature drops below normal ambient because 
			of accidental severance of sensing bulb or capillary. This instrument has manual reset, internal-switch actuation 
			adjustment and is available with any temperature ranged element from -30&deg;F to 1100&deg;F. CSA and FM listed. Agency 
			recognition is void if instrument is modified from factory standard.
			<br/><br/><b>Operation:</b>
			<br/>Switches are set at designated temperatures by removing the front cover and adjusting setting of switch 
			actuating screws on the lever arms. The element failure switch is usually set at ambient temperature.
			<br/><br/>Adjustment is made by reference to a test thermometer of known accuracy, its probe placed adjacent to 
			the thermal sensing bulb in the controlled medium.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/N5-10X_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/ZFHL_Non_Indicating_High_Temp.gif" alt="Partlow ZFHL Non Indicating High Temp" title="Partlow ZFHL Non Indicating High Temp" /></div>
			<div id="PartsContent"><h3>ZFHL  
			<br/><font color="#50658D">Non Indicating High Temp</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is designed to operate in conjunction with a primary temperature controller to 
			cut off heat supply if preset temperature is exceeded.
			<br/><br/>The externally-set Model ZFHL is tamperproof by application of clear acrylic dial and knob 
			shield which is held in place by capstan cover screws. This feature permits tie-wiring for positive 
			sealing of the protective shield. Reset button on front of cover, FM listed. Agency recognition is 
			void if instrument is modified from factory standard.
			<br/><br/><b>Operation:</b>
			<br/>The high limit switch is positioned to desired setting by turning dial knob on the instrument 
			cover, this is accomplished only after the acrylic dial shield is removed by withdrawing the capstan 
			cover screws.
			<br/><br/>In response to minuet temperature changes, expansion and contraction in the thermal element 
			moves a switch actuating lever through a simple mechanical linkage. When the bulb reflects the 
			temperature set on the dial, the limit switch is actuated, shutting down equipment, energizing 
			the lights or sounding alarms.
			<br/><br/>Only when the process temperature drops beow the dial setting can the switch be reset by 
			pushing the reset button on the cover of the instrument.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/ZFHL_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFT_Recording_Thermometer.gif" alt="Partlow RFT Recording Thermometer" title="Partlow RFT Recording Thermometer" /></div>
			<div id="PartsContent"><h3>RFT  
			<br/><font color="#50658D">Recording Thermometer</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>Records temperature on a 10" chart. Twelve ambient compensated ranges within -30&deg;F to 1100&deg;F, permit 
			application diversity from refrigeration to high temperature ovens. Standard clock rotations 12, 24 and 
			48 hour, 7 day, others availalble. Choice of electric or spring wound chart drives available. Wall mounted 
			(brackets furnished) or flush mounted. UL listed.
			<br/><br/><b>Operation:</b>
			<br/>Pen recording is powered by the Piston Pak thermal sensing element, which is field replaceable. 
			Temperature ranges may be substituted at any time by obtaining the correct Piston Pak assembly and proper 
			chart graph, and exchange these with those already on the recorder, providing field range changeability.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFT_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RF15_79_Two_Switch_Recording_Temperature_Control.gif" alt="Partlow RF15-79 Two Switch Recording Temperature Control" title="Partlow RF15-79 Two Switch Recording Temperature Control" /></div>
			<div id="PartsContent"><h3>RF15-79  
			<br/><font color="#50658D">Two Switch Recording Temperature Control</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This recording temperature controller incorporates two switches which operate in fixed 	
			relationship to the temperature setting. Differential between switches can be adjustable up to 
			5% of scale range. Like other RF series recorders, this unit has a 10" chart, may be flush or 
			surface mounted. Unit is shipped with brackets for surface mounting operation. UL and CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>This unit incorporates two snap-setting switches mounted on a common setting arm (red pointer) 
			and actuated by the same temperature response mechanism which moves the recording pen.
			<br/><br/>First to be actuated on a temperature rise is the leaf-type switch. The second, a pin type switch, 
			is actuated only when the recorded temperature exceeds the operation of the first switch.
			<br/><br/>Switches are mounted one behind the other, each having its own circuit. Temperature actuation points 
			between the two switches (differential) is adjustable from 0 to 5 percent of scale range with tolerance 
			on switch settings &#177;1/2%. Adjustment is made by set screws inside the instrument case.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RF15_17_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFA_Pneumatic_Recording_Temperature_Control.gif" alt="Partlow Pneumatic RFA Temperature Controller/Recorder" title="Partlow Pneumatic RFA Temperature Controller/Recorder" /></div>
			<div id="PartsContent"><h3>RFA  
			<br/><font color="#50658D">Pneumatic Temperature Controller/Recorder</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This recorder is an accurate, sensitive pneumatic controller used in conjunction with an 
			air-operated valve for control of steam, gas or fuel for combustion equipment or to operate 
			other pneumatic devices. Requires approximately 16 psi input, air output is 3 to 15 psi, 
			throttling span is 5% to 25% of scale range. Flush or wall mounted (brackets supplied) 
			with 12 optional ranges from -30&deg;F to 1100&deg;F.
			<br/><br/><b>Operation:</b>
			<br/>Operates in an adjustable throttling range of 5-25% of scale range. As the indicating 
			pointer, moving up or down scale in response to the expansion or contraction in the thermal 
			sensing element, enters the throttling range and approches set point, it changes the effective 
			orifice in the instrument's bleed valve.
			<br/><br/>Depending upon its control action (reverse or direct acting) this increases or decreases 
			the pressure delivered to the remotely-placed air operated control device.
			<br/><br/>Pressure transmitted by the control instrument is reflected by teh valve postition of the 
			air-operated device which modulates the flow of heating or cooling medium.
			<br/><br/>Load error, inherent in throttling controls, is compensated by manual reset adjustment. 
			<b>Note:</b> For long air line feeds, ie. 10 feet or greater, or large volume air valves or 
			control devices, an air pressure booster relay is strongly recommended.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFA_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFP_Modulating_Temperature_Recorder.gif" alt="Partlow RFP Modulating Temperature Recorder" title="Partlow RFP Modulating Temperature Recorder" /></div>
			<div id="PartsContent"><h3>RFP  
			<br/><font color="#50658D">Modulating Temperature Recorder</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This recorder is a potentiometer-type controller designed for use with porportional positioning motors 
			to operate modulating valves or damper systems where extremely close sensitivity or straight-line control is required.
			<br/><br/>The instrument automatically positions any of a variety of standard motor operators to provide precise 
			temperature control without sawtooth line characteristics of conventional on-off control. Flush or wall mounted, 
			brackets supplied. Available in 12 optional ranges from -30&deg;F to 1100&deg;F. CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>The potentiometer coil, which moves up or down scale in response to the expansion or contraction in the 
			thermal element, also slides the contact finger along the potentiometer coil within the modulating range.
			<br/><br/>In essence, the coil forms half of a Wheatstone Bridge circuit, while the other half of the bridge is 
			formed by a potentiometer of similar electrical characteristics built into the proportioning motor and 
			driven by the motor shaft.
			<br/><br/>When the contact finger is located at the low end of the potentiometer coil as in process start-up, 
			the motro drives the device to the fully open position.
			<br/><br/>With the contact finger at the high end of the coil, the motor moves the drive to the fully closed position.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFCP_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFHTT_Dual_Recording_Thermometer.gif" alt="Partlow RFHTT Dual Recording Thermometer" title="Partlow RFHTT Dual Recording Thermometer" /></div>
			<div id="PartsContent"><h3>RFHTT  
			<br/><font color="#50658D">Dual Recording Thermometer</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is a dual recording thermometer designed to sense and record temperature from two separate 
			locations simultaneously on a single chart. The case contains two pen mechanisms with individual thermal 
			sensing elements. The pens operate on 1/12 revolution of the chart, for example 2 hours per 24 hour rotation 
			to prevent possible interference when both sensed termperatures happen to coincide. Flush or wall mount 
			available (brackets supplied). Available in 12 ranges from -30&deg;F to 1100&deg;F.
			<br/><br/><b>Operation:</b>
			<br/>This unit consists of a chart drive either electrically driven or spring wound. This chart drive rotates 
			a 10" chart on which two independent pens mark temperature. The mechanism pens function as a result of the 
			Piston Pak expanding or contracting due to termperature change.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFHTT_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFH15_79_15_15_Dual_Recording_Temperature_Control.gif" alt="Partlow RFH15-79/15-15 Dual Recording Temperature Control" title="Partlow RFH15-79/15-15 Dual Recording Temperature Control" /></div>
			<div id="PartsContent"><h3>RFH15-79/15-15  
			<br/><font color="#50658D">Dual Recording Temperature Control</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This dual instrument controls and records two temperature variables on a single chart. It is ideal for 
			temperature and humidity controlled applications such as growth chambers, smokehouses and kilns. The unit 
			is made up of two switch mechanisms. UL and CSA listed. Flush or wall mount available (brackets supplied).
			<br/><br/><b>Operation:</b>
			<br/>The unit is a mechanical, recording controller that incorporates two independent control switching 
			systems in a single case. To avoid interference, the two pens register a difference on one-twelth revolution 
			of the chart, or a two hour differential on a 24 hour chart. Control points are indicated by a red setting 
			pointer on the right hand mechanism, a green pointer on the left.
			<br/><br/>There are two control switches mounted on each mechanism, they are mounted one behind the other. 
			First switch to be actuated on a termperature rise is the leaf-type switch. The second switch (a pin 
			type for left mechanism and a leaf type for the right mechanism) is actuated when the recorded 
			temperature exceeds the operation of the first switch. Temperature actuation points between the 
			two switches (differential) is adjustable from 0 to 5% of scale range, with tolerance on switch 
			setting &#177;1/2%, adjustment is made by set screws inside the instrument case.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFH15-79_15-15_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>

<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFC15_52_Recording_Temperature_Programmer.gif" alt="Partlow RFC15-52 Recording Temperature Programmer" title="Partlow RFC15-52 Recording Temperature Programmer"/></div>
			<div id="PartsContent"><h3>RFC15-52  
			<br/><font color="#50658D">Recording Temperature Programmer</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This instrument is a recorder with a two switch control mechanism which provides automatic temperature programming 
			by use of a pre-cut plastic cam. It is used when two temperatures separated by not more than 5% of scale range are 
			specified as control points. The instrument contains two internally-mounted three-wire thermostatic relays. Wall 
			mounted (brackets furnished) or flush mounts. CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>Control point of the RFC15-52 is determined by a cam-follower which rides the edge of a revolving pre-cut cam 
			and postiions the snap-acting switches in accordance with the program for which the cam is shaped. Cam is configured 
			and fabricated by end user.
			<br/><br/>Switches are actuated by the same termperature-responsive mechanism which moves the recording pen, operating at 
			preselected points in relation to the center line of the cam follower roller.
			<br/><br/>First switch to be actuated on a temperature rise is the leaf-type #15 if normal sensitivity. Second is a 
			super-sensitive #52 pin type, actuated only when the recorded temperature exceeds the operating point of the first switch.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFC15_52_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFHAA_Dual_Recording_Pnematic_Temperature_Control.gif" alt="Partlow RFHAA Dual Recording Pnematic Temperature Control" title="Partlow RFHAA Dual Recording Pnematic Temperature Control" /></div>
			<div id="PartsContent"><h3>RFHAA  
			<br/><font color="#50658D">Dual Recording Pnematic Temperature Control</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This unit is designed to control pneumatically can record two separate temperature variables on a common 
			chart. Relative humidity can be governed through the control of wet and dry bulb temperatures. Two independent 
			control systems in a single case. Humidity and temperature control, output 3-15 psi. Manual reset adjustment 
			with adjustable throttling range 5-25%.
			<br/><br/><b>Operation:</b>
			<br/>The dual RFHAA operates air powered throttling valves regulating the flow of steam, water or gas or actuates 
			other pneumatic devices such as pressure switches or relays. Wet and dry bulbs are recorded when it is used to 
			control temperature and relative humidity.
			<br/><br/>Two instruments with independent pen arms comprise the control. Pen arms are set at two-hour time differential 
			on a 24 hour chart to prevent interference when recording at or near the same control temperature.
			<br/><br/>Pen arms move up or down scale in response to the expansion or contraction in the thermal sensing element. 
			As a pen arm enters the throttling range an approaches set point, it changes the effective orifice size in its 
			control mechanism's bleed valve.
			<br/><br/>The control produces an outpu pressure of 3 to 15 psi, approximately 16 psi input pressure is required to 
			obtain this range. Throttling span is adjustable within the extreme of 5 to 15% of the scale range.
			<br/><br/>Load arm, inherent in throttling controls, is compensated by a manual reset adjustment.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFHAA_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFCP_Recording_Temperature_Programmer.gif" alt="Partlow RFCP Recording Temperature Control" title="Partlow RFCP Recording Temperature Control" /></div>
			<div id="PartsContent"><h3>RFCP  
			<br/><font color="#50658D">Recording Temperature Control</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This instrument is a recorder which utilizes a potentiometer-type contolling mechanism which 
			provides automatic temperature programming by use of a pre-cut plastic cam. It governs proportional 
			positioning motors which operate modulating valves, damper systems, or electric multi-state systems 
			where extremely close sensitive or straigh-line control is required. The instrument automatically 
			positions any of a wide variety of standard motor operators to provide precise temperature control 
			without the sawtooth line characteristics of conventional on-off control. CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>Control point of the RFCP is determined by a cam-follower which rides the edge of a revolving 
			pre-cut cam and postiions the potentiometer in accordance with the program for which the cam is shaped. 
			Cam is configured and fabricated by end user.
			<br/><br/>The pen arm, which moves up or down scale in response to the expansion and contraction in the 
			thermal sensing element, also slides the contact finger along the potentiometer coil within the 
			modulating range. In essence, the coil forms half of a Wheatstone bridge circtui, while the 
			other half of the bridge is formed by a potentiometer of similarly electric characteristics 
			built into the proportioning motor and driven by the motor shaft. A detector relay, either 
			incorporated into the proportioning motor or operating as a separate unti, detects any 
			inbalance in the Wheatstone bridge caused by a change in control temperature and drives 
			the proportioning motor in the direction necessary to regain a balanced bridged circuit.
			<br/><br/>The shaft fo teh proportioning motro is connected through linkage to the device which 
			controls the process fuel flow or temperature variable. When the contact finger is located 
			at the low end of the potentiometer coil, the motor drives the device to the fully open 
			position. When the contact finger at the high end of the coil, the moteor moves the 
			divide to the fully-closed position.
			<br/><br/>Resistance of the potentiometer coil to the Model RFCP should match that of the coil in the positioning motor.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFCP_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/RFC52_Recording_Temperature_Programmer.gif" alt="Partlow RFC52 Recording Temperature Programmer" title="Partlow RFC52 Recording Temperature Programmer" /></div>
			<div id="PartsContent"><h3>RFC52  
			<br/><font color="#50658D">Recording Temperature Programmer</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>This instrument is a recorder with a mechanism that has single switch control capability 
			which provides automatic temperature programming by use of a pre-cut plastic cam. An 
			internally-mounted three-wire thermostatic relay insures extremely long switch life and 
			often eliminates the need for an external conductor. Wall mounted (brackets furnished) 
			or flush mounted. CSA listed.
			<br/><br/><b>Operation:</b>
			<br/>Control point of the RFC52 is determined by a cam-follower which rides the edge of 
			a revolving pre-cut cam and postiions the snap-acting switch in accordance with the program 
			for which the cam is shaped. Cam is configured and fabricated by end user.
			<br/><br/>The switch is actuated by the same temperature responsive mechanism which moves the 
			recording pen, actuated when the pen point is at the center line of the cam follower, 
			the switch operates the three-wire thermostatic relay which handles the load.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/RFC52_DS896.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Partlow/Series_1000_1_4_DIN_Analog_Controller_Limit_Device.gif" alt="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" title="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" /></div>
			<div id="PartsContent"><h3>Series 1000 1/4 
			<br/><font color="#50658D">DIN Analog Controller/Limit Device</font></h3>
			<p><b>Overview / Design Principle:</b>
			<br/>The Series 1000 are versatile analog controllers designed for rugged, reliable, low cost control of process
			variables in harsh industrial environments, even those plagued by environmental electronic noise.
			<br/><br/>Available as a non-indicating analog on/off controller or high or low limit device, it offers a quality 
			level not commonly found in comparably priced devices. It is a 1/4 DIN instrument with a depth of only 5.8 
			inches. All versions are UL recognized and CSA certified as controllers and limit devices are FM approved.
			<br/><br/>The Series 1000 can accommodate J, K, T, R, B, and S thermocouples. Outputs are either SPST or SPDT 
			electro-mechanical relay or solid state relay drivers.
			<br/><br/><b>Features and Benefits:</b>
			<br/>&#149; Non-display, easy dial setting
			<br/>&#149; 1/4 DIN panel mount
			<br/>&#149; Thermocouple input
			<br/>&#149; Sensor fault detection
			<br/>&#149; Relay or SSR driver output
			<br/>&#149; High or Low Limit or On/Off control
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.partlow.com/uploadedFiles/Downloads/Series1000_DS705.pdf" target="_blank"><font color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
		<li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1000_1_4_DIN_Analog_Controller_Limit_Device_Thumbnail.gif" img border="0" alt="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" title="Partlow Series 1000 1/4 DIN Analog Controller/Limit Device" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1161+_1_16_DIN_Limit_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1161+ 1/16 DIN Limit Controller" title="Partlow Series 1161+ 1/16 DIN Limit Controller" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1401+_1_4_DIN_Limit_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1401+ 1/4 DIN Limit Controller" title="Partlow Series 1401+ 1/4 DIN Limit Controller" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1801+_1_8_DIN_Limit_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1801+ 1/8 DIN Limit Controller" title="Partlow Series 1801+ 1/8 DIN Limit Controller" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1160+_1_16_DIN_Temperature_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1160+ 1/16 DIN Temperature Controller" title="Partlow Series 1160+ 1/16 DIN Temperature Controller" /></a></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1400+_1_4_DIN_Temperature_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1400+ 1/4 Temperature Controller" title="Partlow Series 1400+ 1/4 Temperature Controller" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/Series_1800+_1_8_DIN_Temperature_Controller_Thumbnail.gif" img border="0" alt="Partlow Series 1800+ 1/8 Temperature Controller" title="Partlow Series 1800+ 1/8 Temperature Controller"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_500_Basic_Digital_Circular_Chart_Recorder_Thumbnail.gif" img border="0" alt="Partlow MRC 5000 Basic Digital Circular Chart Recorder" title="Partlow MRC 5000 Basic Digital Circular Chart Recorder"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_7000_Recorder_Controller_Profiler_Thumbnail.gif" img border="0" alt="Partlow MRC 7000 Recorder/Controller/Profiler" title="Partlow MRC 7000 Recorder/Controller/Profiler"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_7700_RH_Recorder_Controller_Profiler_Thumbnail.gif" img border="0" alt="Partlow MRC 7700 RH Recorder/Controller/Profiler" title="Partlow MRC 7700 RH Recorder/Controller/Profiler"/></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_7800_Flow_Recorder_Thumbnail.gif" img border="0" alt="Partlow MRC 7800 Flow Recorder" title="Partlow MRC 7800 Flow Recorder"/></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_8000_Recorder_Controller_Thumbnail.gif" img border="0" alt="Partlow MRC 8000 Recorder/Controller" title="Partlow MRC 8000 Recorder/Controller" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_9400_VersaEZ_Simplified_4_Pen_Recorder_with_Alarms_Thumbnail.gif" img border="0"  alt="Partlow MRC 9400 VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms" title="Partlow MRC 9400 VersaEZ (Simplified) High-End 4-Pen Color Temperature Recorder with Alarms" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/MRC_9000_VersaChart_Full_Version_Recording_Controller_Profiler_Thumbnail.gif" img border="0" alt="Partlow MRC 9000 VersaChart (Full Version) High-End Recording Controller/Profiler" title="Partlow MRC 9000 VersaChart (Full Version) High-End Recording Controller/Profiler" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/N79_79_Non_Indicating_Temperature_Controller_Thumbnail.gif" img border="0" alt="Partlow N79-79 Non Indicating Temperature Controller" title="Partlow N79-79 Non Indicating Temperature Controller" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/ZF79_Non_Indicating_Temperature_Controller_Thumbnail.gif" img border="0" alt="Partlow ZF79 Non Indicating Temperature Controller" title="Partlow ZF79 Non Indicating Temperature Controller" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/OL63X_Non_Indicating_High_Temp_Thumbnail.gif" img border="0" alt="Partlow OL63X Non Indicating High Temp" title="Partlow OL63X Non Indicating High Temp" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/O63X_Non_Indicating_High_Temp_Thumbnail.gif" img border="0" alt="Partlow O63X Non Indicating High Temp" title="Partlow O63X Non Indicating High Temp" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/OHL63X_Non_Indicating_High_Temp_Thumbnail.gif" img border="0" alt="Partlow OHL63X Non Indicating High Temp" title="Partlow OHL63X Non Indicating High Temp" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/N5_10X_Non_Indicating_High_Temp_Thumbnail.gif" img border="0" alt="Partlow N5-10X Non Indicating High Temp" title="Partlow N5-10X Non Indicating High Temp" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/ZFHL_Non_Indicating_High_Temp_Thumbnail.gif" img border="0" alt="Partlow ZFHL Non Indicating High Temp" title="Partlow ZFHL Non Indicating High Temp" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFT_Recording_Thermometer_Thumbnail.gif" img border="0" alt="Partlow RFT Recording Thermometer" title="Partlow RFT Recording Thermometer" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RF15_79_Two_Switch_Recording_Temperature_Control_Thumbnail.gif" img border="0" alt="Partlow RF15-79 Two Switch Recording Temperature Control" title="Partlow RF15-79 Two Switch Recording Temperature Control" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFA_Pneumatic_Recording_Temperature_Control_Thumbnail.gif" img border="0" alt="Partlow Pneumatic RFA Temperature Controller/Recorder" title="Partlow Pneumatic RFA Temperature Controller/Recorder" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFP_Modulating_Temperature_Recorder_Thumbnail.gif" img border="0" alt="Partlow RFP Modulating Temperature Recorder" title="Partlow RFP Modulating Temperature Recorder" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFHTT_Dual_Recording_Thermometer_Thumbnail.gif" img border="0" alt="Partlow RFHTT Dual Recording Thermometer" title="Partlow RFHTT Dual Recording Thermometer" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFH15_79_15_15_Dual_Recording_Temperature_Control_Thumbnail.gif" img border="0" alt="Partlow RFH15-79/15-15 Dual Recording Temperature Control" title="Partlow RFH15-79/15-15 Dual Recording Temperature Control" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFC15_52_Recording_Temperature_Programmer_Thumbnail.gif" img border="0" alt="Partlow RFC15-52 Recording Temperature Programmer" title="Partlow RFC15-52 Recording Temperature Programmer" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFHAA_Dual_Recording_Pnematic_Temperature_Control_Thumbnail.gif" img border="0" alt="Partlow RFHAA Dual Recording Pnematic Temperature Control" title="Partlow RFHAA Dual Recording Pnematic Temperature Control" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFCP_Recording_Temperature_Programmer_Thumbnail.gif" img border="0" alt="Partlow RFCP Recording Temperature Control" title="Partlow RFCP Recording Temperature Control" /></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Partlow/thumbnails/RFC52_Recording_Temperature_Programmer_Thumbnail.gif" img border="0" alt="Partlow RFC52 Recording Temperature Programmer" title="Partlow RFC52 Recording Temperature Programmer" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>
