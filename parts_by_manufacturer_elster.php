<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Elster Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="ElsterLogoLarge"></div>
<div id="SensusText">Elster Group is a world leader in Advanced Metering Infrastructure (AMI) and integrated 
metering and utilization solutions to the gas, electricity and water industries. Elster's high quality AMI and 
AMR products, systems, and solutions reflect the wealth of knowledge and experience gained from over 170 years 
of dedication to measuring precious resources and energy. Elster provides world class solutions and advanced 
technologies to help utilities more easily, efficiently and reliably obtain and use advanced metering intelligence 
to improve customer service, enhance operational efficiency, and increase revenues. Elster's AMI solutions enable 
utilities to cost-effectively generate, deliver, manage and conserve the life-essential resources of gas, 
electricity and water. The group has over 7,000 staff and operations in 38 countries, focused in North and 
South America, Europe, and Asia. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AC-250DiaphragmMeter.jpg" alt="Elster AC-250 Diaphragm Meter" title="Elster AC-250 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AC-250 Diaphragm Meter</font></h3>
			<p><br/>The aluminum class AC-250 is the industry's most cost-effective gas meter for 
			residential applications. It is unequaled for accuracy retention and for life cycle maintenance economies.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3535.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-RPL3835.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a></p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AM-250DiaphragmMeter.jpg" alt="Elster AM-250 Diaphragm Meter" title="Elster AM-250 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AM-250 Diaphragm Meter</font></h3>
			<p><br/>The ideal meter for measuring gas to mobile home parks, for LP applications, 
			or wherever US standard pipe thread connections are desirable. Properly installed, this 
			meter will accurately measure gas loads ranging from one pilot light to its full recommended capacity.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3537.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM-250.dwg"><font size="1" color="#ACB0C3"><b>AutoCad drawings</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-RPL3835.pdf"><font size="1" color="#ACB0C3"><b> Parts List</b></font></a>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AT-210DiaphragmMeter.jpg" alt="Elster AT-210 Diaphragm Meter" title="Elster AT-210 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AT-210 Diaphragm Meter</font></h3>
			<p><br/>The AT-210 and AT-250 were designed as replacements for the tinned steelcase 
			meters or other meters that have wide center-to-center top connections. The AT-210 
			has 5LT connections while the AT-250 has 10LT connections.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3504.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-RPL3835.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AL-425DiaphragmMeter.jpg" alt="Elster AL-425 Diaphragm Meter" title="Elster AL-425 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AL-425 Diaphragm Meter</font></h3>
			<p><br/>The AL-425 diaphragm meter is ideally suited for larger residential or 
			small commercial/industrial installations. Compatible with automatic meter reading technology.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3520.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AC-630DiaphragmMeter.jpg" alt="Elster AC-630 Diaphragm Meter" title="Elster AC-630 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AC-630 Diaphragm Meter</font></h3>
			<p><br/>The AC-630, another of the diaphragm meters in the Elster American Meter 
			product line, provides the customer with rugged durability and accurate long-term 
			performance. Compatible with automatic meter reading technology.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3550.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AC630partslist.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/CurbMeter.jpg" alt="Elster Curb Meter" title="Elster Curb Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">Curb Meter</font></h3>
			<p><br/>The Curb Meter uses our existing aluminum body meters for service in the 
			wet and corrosive environment of the curb vault. To combat corrosion the meter is 
			fully coated in twocomponent polyurethane. The index and badge are modified to 
			allow for reading from above the meter. Curb meters are available in several 
			connection styles; standard top or side connections or the new extended top 
			version with front and back connections.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMDS3861.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/BK-G4Meter.jpg" alt="Elster BK-G4 Meter" title="Elster BK-G4 Meter"/></div>
			<div id="PartsContent"><h3>BK-G4 Meter</h3>
			<p><br/>The BK-G4 is a synthetic diaphragm meter with the latest technology and injection-molded 
			aluminum body. BK-G4 meter is designed for a maximum flow of 200 cfh at 0.25 psig of flow and an 
			operating pressure of 5 psig. It is available with various threads and distances between connection centers.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/BKG4metertechnicalbulletin.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AL-800_AL-1000.jpg" alt="Elster Diaphragm Meters AL-800/AL-1000" title="Elster Diaphragm Meters AL-800/AL-1000"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AL-800/AL-1000</font></h3>
			<p><br/>The AL-800/AL-1000 diaphragm meters are for usage in commercial/industrial 
			applications. These meters are compact and lightweight, but are designed for positive 
			displacement accuracy for industrial/commercial loads.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AL1000.pdf"><font size="1" color="#ACB0C3"><b>AL-1000 Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMDS3817.pdf"><font size="1" color="#ACB0C3"><b>AL-800 Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII3711.pdf"><font size="1" color="#ACB0C3"><b>Adjustment Instructions</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/installtioninstructions.pdf"><font size="1" color="#ACB0C3"><b>Installation Instructions</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AL-1400_AL-2300_AL-5000.jpg" alt="Elster Diaphragm Meters AL-1400/AL-2300/AL-5000" title="Elster Diaphragm Meters AL-1400/AL-2300/AL-5000"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AL-1400/AL-2300/AL-5000</font></h3>
			<p><br/>The Elster Meter Services Refurbish program applies to AL-1400/AL-2300/AL-5000 
			series Elster American or Elster Canadian meters, suggested for fifteen (15) years old and newer.			
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3818.pdf"><font size="1" color="#ACB0C3"><b>AL-1400 Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMDS3819.pdf"><font size="1" color="#ACB0C3"><b>AL-2300 Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3811.pdf"><font size="1" color="#ACB0C3"><b>AL-5000 Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AL_5000_Ref.pdf"><font size="1" color="#ACB0C3"><b>Technical Info</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RPM-CMTC.jpg" alt="Elster Rotary Meters RPM-CMTC" title="Elster Rotary Meters RPM-CMTC"/></div>
			<div id="PartsContent"><h3>Rotary Meters
			<br/><font size="2" color="#50658D">RPM-CMTC</font></h3>
			<p><br/>All RPM Series meters mount in either a horizontal or vertical position, 
			depending on available space and convenience. Once installed, all standard and 
			optional accessories can be easily positioned for convenient reading and quick service. 
			All models have extremely good rangeability and are available in various pipe sizes to 
			meet a variety of applications.			
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMBR5500_x.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RPM-STD.jpg" alt="Elster Rotary Meters RPM-STD" title="Elster Rotary Meters RPM-STD"/></div>
			<div id="PartsContent"><h3>Rotary Meters
			<br/><font size="2" color="#50658D">RPM-STD</font></h3>
			<p><br/>All RPM Series meters mount in either a horizontal or vertical position, 
			depending on available space and convenience. Once installed, all standard and optional 
			accessories can be easily positioned for convenient reading and quick service. All models 
			have extremely good rangeability and are available in various pipe sizes to meet a variety 
			of applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMBR5500_x.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-II5750.pdf"><font size="1" color="#ACB0C3"><b>RPM Rotary Gas Meters</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-II6780.pdf"><font size="1" color="#ACB0C3"><b>Rotary Meter High and Low Frequency Pulser</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RPM-ID.jpg" alt="Elster Rotary Meters RPM-ID" title="Elster Rotary Meters RPM-ID"/></div>
			<div id="PartsContent"><h3>Rotary Meters
			<br/><font size="2" color="#50658D">RPM-ID</font></h3>
			<p><br/>All RPM Series meters mount in either a horizontal or vertical position, depending 
			on available space and convenience. Once installed, all standard and optional accessories 
			can be easily positioned for convenient reading and quick service. All models have extremely 
			good rangeability and are available in various pipe sizes to meet a variety of applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMBR5500_x.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RPM-CMTC-ID.jpg" alt="Elster Rotary Meters RPM-CMTC-ID" title="Elster Rotary Meters RPM-CMTC-ID"/></div>
			<div id="PartsContent"><h3>Rotary Meters
			<br/><font size="2" color="#50658D">RPM-CMTC-ID</font></h3>
			<p><br/>All RPM Series meters mount in either a horizontal or vertical position, depending 
			on available space and convenience. Once installed, all standard and optional accessories 
			can be easily positioned for convenient reading and quick service. All models have extremely 
			good rangeability and are available in various pipe sizes to meet a variety of applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMBR5500_x.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RPM-CMTCwithDirect-MountERT.jpg" alt="Elster Rotary Meters RPM-CMTC with Direct-Mount ERT" title="Elster Rotary Meters RPM-CMTC with Direct-Mount ERT"/></div>
			<div id="PartsContent"><h3>Rotary Meters
			<br/><font size="2" color="#50658D">RPM-CMTC with Direct-Mount ERT</font></h3>
			<p><br/>All RPM Series meters mount in either a horizontal or vertical position, depending 
			on available space and convenience. Once installed, all standard and optional accessories 
			can be easily positioned for convenient reading and quick service. All models have extremely 
			good rangeability and are available in various pipe sizes to meet a variety of applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMBR5500_x.pdf.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/en/336.html"><font size="1" color="#ACB0C3"><b>Approvals</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/SSonic.jpg" alt="Elster Ultrasonic Meters S Sonic" title="Elster Ultrasonic Meters S Sonic"/></div>
			<div id="PartsContent"><h3>Ultrasonic Meters
			<br/><font size="2" color="#50658D">S Sonic</font></h3>
			<p><br/>The Instromet S.Sonic Single Path Ultrasonic Flow Meter is a device ideally 
			suited for a variety of gas measurement applications. Sophisticated transducers utilizing 
			a unique bounce path configuration along with digital electronics gives the S.Sonic low 
			uncertainty (&#177;1.0%) with excellent repeatability and linearity characteristics. 
			The S.Sonic is recommended for non-custody applications. It is available in line sizes 
			from 4" and all ANSI ratings. It can be configured for retractable and non-retractable 
			transducers. The absence of moving parts makes it a very low maintenance meter. For optimal 
			performance, the installation of a flow conditioning plate or system is recommended.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EIBR0005.pdf"><font size="1" color="#ACB0C3"><b>Brochure</b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/QSonic5Path.jpg" alt="Elster Ultrasonic Meters Q Sonic 5 Path" title="Elster Ultrasonic Meters Q Sonic 5 Path"/></div>
			<div id="PartsContent"><h3>Ultrasonic Meters
			<br/><font size="2" color="#50658D">Q Sonic 5 Path</font></h3>
			<p><br/>The Elster Instromet Q.Sonic 5 Path Ultrasonic Flow Meter is one-of-a-kind. 
			The combination of sophisticated transducers, digital electronics and the unique patented 
			path configuration results in a meter unsurpassed in gas measurement accuracy. Its proven 
			accuracy (better than &#177;0.5%) and ability to analyze swirl and asymmetrical flow profiles 
			has resulted in the approval of this meter for custody transfer by numerous natural gas 
			pipeline operators in the United States.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/QSonic5Path.pdf"><font size="1" color="#ACB0C3"><b>Brochure</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/fmrv1.pdf"><font size="1" color="#ACB0C3"><b>Approvals</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/QSonic-3C.jpg" alt="Elster Ultrasonic Meters Q Sonic - 3C" title="Elster Ultrasonic Meters Q Sonic - 3C"/></div>
			<div id="PartsContent"><h3>Ultrasonic Meters
			<br/><font size="2" color="#50658D">Q Sonic - 3C</font></h3>
			<p><br/>The Q Sonic - 3C has a new transducer design with a smaller footprint with 
			face-to-face flange dimensions equal to comparable diameter turbine meters
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/fmrv1.pdf"><font size="1" color="#ACB0C3"><b>Approvals</b></font></a>
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/QSonic3Path.jpg" alt="Elster Ultrasonic Meters Q Sonic 3 Path" title="Elster Ultrasonic Meters Q Sonic 3 Path"/></div>
			<div id="PartsContent"><h3>Ultrasonic Meters
			<br/><font size="2" color="#50658D">Q Sonic 3 Path</font></h3>
			<p><br/>The Elster Instromet Q.Sonic 3 Path Ultrasonic Flow Meter is one-of-a-kind. 
			The combination of sophisticated transducers, digital electronics and the unique patented 
			path configuration results in a meter unsurpassed in gas measurement accuracy. The proven 
			accuracy (better than &#177;0.5%) and its swirl analysis capability makes the Q.Sonic 3 
			Path the ideal meter for natural gas custody transfer measurement.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/QSonic3Path.pdf"><font size="1" color="#ACB0C3"><b>Brochure</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/fmrv1.pdf"><font size="1" color="#ACB0C3"><b>Approvals</b></font></a>
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/TurbineGasMeterQ&QIC.jpg" alt="Elster Turbine Meters Turbine Gas Meter Q & QIC" title="Elster Turbine Meters Turbine Gas Meter Q & QIC"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">Turbine Gas Meter Q & QIC</font></h3>
			<p><br/>&#149; Suitable for all non-corrosive gases
			<br/>&#149; Standard with LF & HF outputs
			<br/>&#149; Series Q & QIC turbine meters are 
             		<br/>&nbsp;&nbsp;furnished with an integral oil pump by
             		<br/>&nbsp;&nbsp;means of which instrument oil can be 
             		<br/>&nbsp;&nbsp;forced through the bearing while the meter 
             		<br/>&nbsp;&nbsp;is operating
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/TurbineInstallation.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/TurbineGasMeterX&XIC.jpg" alt="Elster Turbine Meters Turbine Gas Meter X & XIC" title="Elster Turbine Meters Turbine Gas Meter X & XIC"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">Turbine Gas Meter X & XIC</font></h3>
			<p><br/>&#149; Suitable for all non-corrosive gases
			<br/>&#149; Standard with LF & HF outputs
			<br/>&#149; The X turbine meter incorporates the 
             		<br/>&nbsp;&nbsp;patented X4X&reg; flow conditioner
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/TurbineGasMeterX_XICSeries.pdf"><font size="1" color="#ACB0C3"><b>Brochure</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/TurbineInstallation.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AccuTestMeter.gif" alt="Elster Turbine Meters AccuTest Meter" title="Elster Turbine Meters AccuTest Meter"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">AccuTest Meter</font></h3>
			<p><br/>The AccuTest Turbine meter is designed for on-site, in-line testing under 
			actual operating conditions. This meter, with a laptop connected to it, becomes a 
			transfer proving system that gives immediate results.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMII4715.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/390_eam_GTS_GTX_Meter_large.gif" alt="Elster Turbine Meters GTS/GTX Meter" title="Elster Turbine Meters GTS/GTX Meter"/></div>
			<div id="PartsContent"><h3>Turbine Meters
			<br/><font size="2" color="#50658D">GTS/GTX Meter</font></h3>
			<p><br/>The GTS/GTX Turbine meters are used for heavy-duty commercial/industrial 
			applications. These meters measure gas volume via rotor blades. The rotor rotation 
			rate is proportional to the gas flow. These meters are available in the GTS or GTX models.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB4510_x.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII4725.pdf"><font size="1" color="#ACB0C3"><b>GT to GTS Meter Conversion Installation Instructions</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII4720.pdf"><font size="1" color="#ACB0C3"><b>GTX/GTS Gas Turbine Meter Installation Instructions</b></font></a>
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/QA_QAe.jpg" alt="Elster Submetering Equipment QA / QAe" title="Elster Submetering Equipment QA / QAe"/></div>
			<div id="PartsContent"><h3>Submetering Equipment
			<br/><font size="2" color="#50658D">QA / QAe</font></h3>
			<p><br/>The wafer type housing of QA and QAe (with electronic totalizer) is made of 
			aluminium. Depending on the version, the volume is displayed on a mechanical (QA) or 
			an electronic totalizer (QAe). QA and QAe Quantometers combine the high accuracy of the 
			Q series of Quantometers with low weight.
			<br/><br/>The Quantometers are available in a number of flow ranges, diameters and pressure 
			ratings. Depending on the version, a low or medium-frequency pulser and an optical readout 
			or an optical readout with additional M-bus output is avaliable. All of the Quantometers 
			are DVGW approved.
			<br/><br/>For special industrial applications, stainless steel versions are also available. 
			<br/><br/>&#149; Rangeability: up to 1:20
			<br/>&#149; Flow ranges: 1.6 -1,600 m&#179;/h 
             		<br/>&nbsp;&nbsp;(56 - 56,000 ft&#179;/h)
			<br/>&#149; Diameters: DN 25 - 150
			<br/>&#149; Pressure rates: PN 4, PN 16, ANSI 150
			<br/>&#149; Gas temperature QA: -10&deg;C to +60&deg;C 
             		<br/>&nbsp;&nbsp;(14&deg;F to 140&deg;F)
			<br/>&#149; Ambient temperature QA: -20&deg;C to +70&deg;C 
             		<br/>&nbsp;&nbsp;(-4&deg;F to 158&deg;F)
			<br/>&#149; Ambient- / gas temperature QAe: 
             		<br/>&nbsp;&nbsp;0&deg;C to +50&deg;C (32&deg;F to 122&deg;F)
			</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/WetTestMeters.jpg" alt="Elster Laboratory Meters Wet Test Meters" title="Elster Laboratory Meters Wet Test Meters"/></div>
			<div id="PartsContent"><h3>Laboratory Meters
			<br/><font size="2" color="#50658D">Wet Test Meters</font></h3>
			<p><br/>Elster American Wet Test Meters are individually calibrated under controlled conditions. 
			The Hinman-type drums (low-differential) provide a high degree of accuracy. These meters will 
			maintain the original calibration for long periods due to the extensive use of corrosion resistant 
			material. Additionally, the grommet-type seal is designed for minimum friction to give maximum meter 
			sensitivity.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/Wettest.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/DryTestMeters.jpg" alt="Elster Laboratory Meters Dry Test Meters" title="Elster Laboratory Meters Dry Test Meters"/></div>
			<div id="PartsContent"><h3>Laboratory Meters
			<br/><font size="2" color="#50658D">Dry Test Meters</font></h3>
			<p><br/>Elster American Meter Dry Test Meters are used for field or laboratory testing of gas 
			unsaturated with water vapor.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS7510.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/KleanlineFilters.jpg" alt="Elster Kleanline Filters" title="Elster Kleanline Filters"/></div>
			<div id="PartsContent"><h3>Kleanline Filters</h3>
			<p><br/>All natural gas pipelines have some degree of contamination present. Contaminants 
			in the form of dust, dirt, rust, pipe scale, welding slag and valve grease are a potential 
			source of problems for regulators, meters and orifices. The Elster American Meter KleanLine 
			filter is designed to remove these particles without substantial pressure loss and are available 
			in screwed and flanged connections.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/KleanlineTechRel.pdf"><font size="1" color="#ACB0C3"><b>Technical Information</b></font></a>
			</p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/StraighteningVanes.jpg" alt="Elster Straightening Vanes" title="Elster Straightening Vanes"/></div>
			<div id="PartsContent"><h3>Straightening Vanes</h3>
			<p><br/>Swirl is a condition where the gas velocity is not totally parallel to the axis 
			of the pipe but has a spiral component . It may be caused by complex piping, control and/or 
			shutoff valves, elbows or other fittings in the meter/regulator station. Swirl in the direction 
			of the rotor rotation will cause aturbine meter to over-register and vice-versa for swirl in the 
			opposite direction of the rotor. Elster American Meter Straightening Vanes straighten the gas flow 
			to a normal pattern, enabling accurate measurement by the downstream turbine meter. Elster American 
			Meter Straightening Vanes are fabricated from carbon steel in a variety of sizes for installation in either 
			schedule 40 and schedule 80 pipe. They are produced in accordance with AGA and ASME recommendations.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/StraighteningVanes.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/Splashguard.jpg" alt="Elster Splashguard" title="Elster Splashguard"/></div>
			<div id="PartsContent"><h3>Splashguard</h3>
			<p><br/>When the weather turns cold, regulator vents can freeze shut - preventing the regulator's 
			diaphragm chamber from breathing. This is a common problem among utilities doing business in areas 
			that experience melting snow or freezing rain. Nothing is quite so simple or effective as Splashguard.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EMS-DS0001_1_EN_P.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/RemoteVolumePulser(RVP).jpg" alt="Elster Submetering Equipment Remote Volume Pulser (RVP)" title="Elster Submetering Equipment Remote Volume Pulser (RVP)"/></div>
			<div id="PartsContent"><h3>Submetering Equipment
			<br/><font size="2" color="#50658D">Remote Volume Pulser (RVP)</font></h3>
			<p><br/>The pulser provides a dry-contactswitch closure from two separated magnetic reed 
			switchesduring each revolution of the meter index drive. The timingof the two switch closures 
			is separated by approximately a 90&deg; turn of the index drive.The RVP-FI Pulser is installed 
			over the meter index driveusing the holes in the meter body used for the index box. 
			The existing index and index box become part of thePulser during installation. Contact closures 
			are supplied by normally open Form Aswitches. One side of each switch is connected in common. 
			The output of each switch is available through an integral, 12-foot cable along with a common 
			connection for each switch. The cable also includes access to a loop for floating tamper-detection 
			circuits as well as a connectionto the common when needed for.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EMSTB6771.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/LubricationPumps.jpg" alt="Elster Lubrication Pumps" title="Elster Lubrication Pumps"/></div>
			<div id="PartsContent"><h3>Lubrication Pumps</h3>
			<p><br/>For meters operating up to 500 psig, a plunger-type pump is available (PN 93723K002) 
			that contains the following:
			<br/>Lubrication Gun
			<br/>Carrying Case
			<br/>8 oz. Bottle of Anderol 401-D Oil
			<br/>Instruction Manual
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/LubricationPumps.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/NeedleValves.jpg" alt="Elster Needle valves" title="Elster Needle valves"/></div>
			<div id="PartsContent"><h3>Needle valves</h3>
			<p><br/>Elster Meter Services Needle valves are used for many gas and liquid measurement and 
			control applications - orifice meter gauge line connections, shut-off valves for pressure gauges, 
			odorizers, sampling devices, and other high pressure applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/NeedleValves.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/T210ElectronicOutreader.jpg" alt="Elster T210 Electronic Outreader" title="Elster T210 Electronic Outreader"/></div>
			<div id="PartsContent"><h3>T210 
			<br/><font size="2" color="#50658D">Electronic Outreader</font></h3>
			<p><br/>Inexpensive and reliable, the ScanCounter T210 is the cost effective solution to meter 
			reading access problems. The ScanCounter T210 electronic outreader is designed specifically 
			for accurate counting of water meter pulse outputs. It incorporates an easy to read display.
			<br/><br/>
			<br/>&#149; High integrity pulse counting
  			<br/>&#149; Fully configurable
    			<br/>&#149; Long battery life
    			<br/>&#149; Operates at -30&deg;C to 70&deg;C
    			<br/>&#149; Leak detection
    			<br/>&#149; 'Touch Free' reading
    			<br/>&#149; Display meter reading
    			<br/>&#149; Holds last read value
   			<br/>&#149; 'Bypass' & 'Main' display icon
  			<br/>&#149; 8 digits displayed
    			<br/>&#149; Supports bi-directional pulsers
    			<br/>&#149; Incorporates a watchdog
    			<br/>&#149; Installed away from pit meters improving 
             		<br/>&nbsp;&nbsp;access safety
    			<br/>&#149; Installed in easy to access locations to 
             		<br/>&nbsp;&nbsp;deal with difficult to access meters
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/IND_T210_100.pdf"><font size="1" color="#ACB0C3"><b>Specification Sheet</b></font></a>
			</p>
			</div></div>
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/SR100SEriesServiceRegulator.jpg" alt="Elster Pipe Size 1 1/4" or less SR100 Series Service Regulator" title="Elster Pipe Size 1 1/4" or less SR100 Series Service Regulator"/></div>
			<div id="PartsContent"><h3>Pipe Size 1 1/4" or less
			<br/><font size="2" color="#50658D">SR100 Series Service Regulator</font></h3>
			<p><br/>The SR113 Compact design combined with high performance provides wide capacity 
			ranges with the ability to standardize on varying applications. The full lockup capability 
			of the SR113 provides assurance that downstream pressure will not build up during no-flow 
			situations while the full capacity relief provides safety during abnormal overpressure occurrences.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB8100.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMIM8101.pdf"><font size="1" color="#ACB0C3"><b>Instruction Manual</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMRPL8102.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/J48.jpg" alt="Elster Pipe Size 1 1/4" or less J48 (up to 2 inch size)" title="Elster Pipe Size 1 1/4" or less J48 (up to 2 inch size)"/></div>
			<div id="PartsContent"><h3>Pipe Size 1 1/4" or less
			<br/><font size="2" color="#50658D">J48 (up to 2 inch size)</font></h3>
			<p><br/>The J48 is an industrial low pressure regulator suitable for a wide range of pressure 
			reduction applications, including such OEM equipment as boiler and burner trains for a variety of gases.
			<br/><br/>&#149; Sizes: 3/4", 1", 1 1/4", 1 1/2" & 2"
			<br/>&#149; Inlet pressure up to 350 mbar
			<br/>&#149; Outlet pressure 5 - 160 mbar (2" up to 
             		<br/>&nbsp;&nbsp;100 mbar)
			<br/>&#149; Temperature range -20 to +70&deg;C
			<br/>&#149; Quick release spring change facility
			<br/>&#149; Sealed outlet pressure adjustment
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EMS-TB9100.1-EN-P.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="35" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/J78.jpg" alt="Elster Pipe Size 1 1/2' and 2 J78"" title="Elster Pipe Size 1 1/2' and 2 J78"/></div>
			<div id="PartsContent"><h3>Pipe Size 1 1/2' and 2"
			<br/><font size="2" color="#50658D">J78</font></h3>
			<p><br/>The J78 range is a compact, accurate and economical regulator. It is suitable for 
			a wide range of commercial and industrial applications such as boiler and burner trains. 
			The J78 offers positive lock up, and is available in a number of sizes, with a comprehensive 
			range of regulator springs for different requirements.
			<br/><br/>The J78 range is suitable for inlet pressures up to 350mbar (5psig). It meets 
			the essential requirements of the European Gas Directive (90/396/EEC). All units are 
			designed for natural, liquefied petroleum and manufactured gases and can be installed in 
			horizontal or vertical pipelines.
			<br/><br/>The regulators may be utilised for domestic meter service applications with 
			inlet pressures up to 75mbar (30"wg) complying with British Gas specifications.
			<br/><br/>&#149; Sizes: 1/2", 3/4" and 1"
    			<br/>&#149; Inlet pressure up to 350 mbar
    			<br/>&#149; Outlet pressure 2 - 48 mbar
    			<br/>&#149; Temperature range -20&deg;C to +70&deg;C
    			<br/>&#149; R version with compact design
    			<br/>&#149; RS version with threaded vent
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-instromet.com/downloads/EMS-TB9200.pdf"><font size="1" color="#ACB0C3"><b>J78RS Gas Pressure Regulator Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-instromet.com/downloads/JEA_J78_DS_UK_2005_02.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="36" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1200B2SeriesREgulators.jpg" alt="Elster 1200B2 Series Regulators" title="Elster 1200B2 Series Regulators"/></div>
			<div id="PartsContent"><h3>1200B2 
			<br/><font size="2" color="#50658D">Series Regulators</font></h3>
			<p><br/>1200B2 series pressure regulators are designed for natural gas residential 
			applications. The 1200B2 has added designs to improve relief performance.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/1200B2TechBulletin.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII8705.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_1213B2_Series_RP_UK_RPL_208801_2.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMIM8506.pdf"><font size="1" color="#ACB0C3"><b>Instruction Manual</b></a></font></p>
			</div></div>
<div id="37" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1200BSeriesRegulators.jpg" alt="Elster 1200B Series Regulators" title="Elster 1200B Series Regulators"/></div>
			<div id="PartsContent"><h3>1200B 
			<br/><font size="2" color="#50658D">Series Regulators</font></h3>
			<p><br/>1200B series pressure regulators are designed for natural gas residential applications.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_1200B_Series_DS_UK_SB_208505_2.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII8704.pdf"><font size="1" color="#ACB0C3"><b>Installation Instructions</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_1200_Series_MA_UK_IM_208704_4.pdf"><font size="1" color="#ACB0C3"><b>Installation Manual</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/http://www.elster-americanmeter.com/downloads/AM_PR_1200_Series_RP_UK_RPL_208810_3.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="38" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1800B2SeriesPressureRegulators.gif" alt="Elster 1800B2 Series Pressure Regulators" title="Elster 1800B2 Series Pressure Regulators"/></div>
			<div id="PartsContent"><h3>1800B2  
			<br/><font size="2" color="#50658D">Series Pressure Regulators</font></h3>
			<p><br/>The Elster American Meter Series 1800B2 pressure regulators are designed for 
			natural gas applications and feature a compact, lightweight design for fast, easy installation. 
			Interchangeable orifices and springs provide a wide range of outlet pressures and flow rates. 
			Outlet pressures between 3.5" W.C. and 2 PSIG are available. Operating temperature range is 
			-20&deg;F to 150&deg;F (-30&deg;C to 65&deg;C). Maximum flow rate is 2500 SCFH (70.8 m3/h). 
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/1800B2RegulatorTB.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="39" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1800CPressureRegulators.jpg" alt="Elster 1800C Pressure Regulators" title="Elster 1800C Pressure Regulators"/></div>
			<div id="PartsContent"><h3>1800C 
			<br/><font size="2" color="#50658D">Pressure Regulators</font></h3>
			<p><br/>The Elster American Meter Series 1800C pressure regulators are designed for natural 
			gas applications and feature a compact, lightweight design for fast, easy installation. 
			Interchangeable orifices and springs provide a wide range of outlet pressures and flow rates. 
			Outlet pressures between 3.5" W.C. and 2 PSIG are available. Operating temperature range is 
			-20&deg;F to 150&deg;F (-30&deg;C to 65&deg;C). Maximum flow rate is 2500 SCFH (70.8 m3/h).
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB8515.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMII8720.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="40" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/3000SeriesRegulators.jpg" alt="Elster 3000 Series Regulators Pipe Size 3" and above" title="Elster 3000 Series Regulators Pipe Size 3" and above"/></div>
			<div id="PartsContent"><h3>3000 Series Regulators
			<br/><font size="2" color="#50658D">Pipe Size 3" and above</font></h3>
			<p><br/>The unique design of the 3000 industrial regulator is used for distribution, 
			commercial and industrial applications. It is best suited for furnaces, batch ovens and boilers.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_DS_UK_SB_SB_208535.pdf"><font size="1" color="#ABC0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_MA_UK_IMP_208735_1.pdf"><font size="1" color="#ABC0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="41" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/CR4000ServiceRegulator.jpg" alt="Elster CR4000 Service Regulator" title="Elster CR4000 Service Regulator"/></div>
			<div id="PartsContent"><h3>CR4000 
			<br/><font size="2" color="#50658D">Service Regulator</font></h3>
			<p><br/>The Elster American Meter Series CR4000 pressure regulator is designed for natural 
			gas applications and features a compact, lightweight design for fast, easy installation. 
			Interchangeable springs provide a wide range of outlet pressures and flow rates. Outlet 
			pressures between 3.5" W.C. and 5 PSIG are available. Operating temperature range is 
			-20&deg;F to 150&deg;F (-30&deg;C to 65&deg;C).
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB8580.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII8780.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="42" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1800CPB2SeriesRegulators.gif" alt="Elster 1800CPB2 Series Regulators" title="Elster 1800CPB2 Series Regulators"/></div>
			<div id="PartsContent"><h3>1800CPB2 
			<br/><font size="2" color="#50658D">Series Regulators</font></h3>
			<p><br/>The 1800CPB2 Series pressure regulators are designed for natural gas applications. 
			This series is ideal for light commercial and industrial use.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_1800CPB2_Series_DS_UK_SB_208520_2.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB8520.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMII8725.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_1800CPB2_Series_RP_UK_RPL_208815_2.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="43" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AxialFlowValves.jpg" alt="Elster Pipe Size 1 1/2' and 2 Axial Flow Valves" title="Elster Pipe Size 1 1/2' and 2 Axial Flow Valves"/></div>
			<div id="PartsContent"><h3>Pipe Size 1 1/2' and 2"
			<br/><font size="2" color="#50658D">Axial Flow Valves</font></h3>
			<p><br/>The Axial flow valve can be used for pressure regulation, over pressure relief, 
			flow control or act as an on/off valve. This unique design provides a valve that is 
			extremely compact and lightweight.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AxialFlowValvesTB.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="44" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/1800PFMSeriesIndustrialREgulator.jpg" alt="Elster 1800 PFM Series Industrial Regulator" title="Elster 1800 PFM Series Industrial Regulator"/></div>
			<div id="PartsContent"><h3>1800 PFM Series
			<br/><font size="2" color="#50658D">Industrial Regulator</font></h3>
			<p><br/>The 1800 PFM 1-1/2" and 2" industrial regulator is designed for applications 
			requiring medium-to-high capacity, precise outlet pressure control and fast response 
			to changing loads. It is suited for use with meters having base capacities up to 11,000 CFH.
			<br/><br/>An 1800 PFM handles varying inlet pressures up to 125 PSIG while controlling 
			a set constant outlet pressure within &#177;1% of the absolute set pressure over a wide 
			range of flow rates. It is ideal for fixed factor measurement at metering pressures from 0.5 to 30 PSIG.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/1800PFMTechBulletin.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			</p>
			</div></div>
<div id="45" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/3000SeriesRegulators2.jpg" alt="Elster 3000 Series Regulators Pipe Size 1 1/4" or less" title="Elster 3000 Series Regulators Pipe Size 1 1/4" or less"/></div>
			<div id="PartsContent"><h3>3000 Series Regulators
			<br/><font size="2" color="#50658D">Pipe Size 1 1/4" or less</font></h3>
			<p><br/>The unique design of the 3000 industrial regulator is used for distribution, commercial 
			and industrial applications. It is best suited for furnaces, batch ovens and boilers.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_DS_UK_SB_SB_208535.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_MA_UK_IMP_208735_1.pdf"><font size="1" color="ABC0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="46" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/3000SeriesRegulators3.jpg" alt="Elster 3000 Series RegulatorsPipe Size 1 1/2" and 2" title="Elster 3000 Series Regulators Pipe Size 1 1/2" and 2"/></div>
			<div id="PartsContent"><h3>3000 Series Regulators
			<br/><font size="2" color="#50658D">Pipe Size 1 1/2" and 2"</font></h3>
			<p><br/>The unique design of the 3000 industrial regulator is used for distribution, 
			commercial and industrial applications. It is best suited for furnaces, batch ovens and boilers.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_DS_UK_SB_SB_208535.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/AM_PR_3000_Series_MA_UK_IMP_208735_1.pdf"><font size="1" color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="47" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/60SeriesPilots.jpg" alt="Elster 60 Series Pilots" title="Elster 60 Series Pilots"/></div>
			<div id="PartsContent"><h3>60 Series Pilots</h3>
			<p>The 60 Series Pilot Regulator provides fast, accurate, and stable Pressure Control, 
			improving the performance of all types of Flexible Element Valves. All 60Series Pilots are 
			rated for an Maximum Allowable Operating Pressure (MAOP) of 1480 PSIG. (Class 600 Natural Gas Service)
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAMTB9800.pdf"><font size="1" color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/60SERIESPILOT.dwg"><font size="1" color="#ACB0C3"><b>CAD Drawing</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-PL9810.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a>
			</p>
			</div></div>
<div id="48" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/USSA.jpg" alt="Elster USSA" title="Elster USSA"/></div>
			<div id="PartsContent"><h3>USSA</h3>
			<p><br/>The Universal Safety Shut-off Assembly (USSA) is a new design of integral shutoff. 
			It was created to meet the requirements in North America and associated markets and is offered 
			in three shut-off versions
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/USSAtechbul.pdf"><font size="1" color="#ABC0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAMIIPL8870.pdf"><font size="1" color="#ABC0C3"><b>Installation"</b></font></a>
			</p>
			</div></div>
<div id="49" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/CFRFilters.jpg" alt="Elster CFR Filters" title="Elster CFR Filters"/></div>
			<div id="PartsContent"><h3>CFR Filters</h3>
			<p><br/>Elster American Meter CFR gas filters, including flanged, screwed and pilot filters 
			effectively remove dirt, pipe scale and other matter from gas line in commercial and industrial 
			applications. Filter elements are made of rein impregnated cellulose fiber so that moisture will 
			not impair their effectiveness.
			</p>
			</div></div>
<div id="50" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/GasketStrainers.jpg" alt="Elster Gasket Strainers" title="Elster Gasket Strainers"/></div>
			<div id="PartsContent"><h3>Gasket Strainers</h3>
			<p><br/>Gasket Strainers are an effective way to protect downstream metering and regulation 
			equipment from weld slag and other debris in the gas stream.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EMS-DS0004.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/elster/AC-250DiaphragmMeter.jpg" alt="Elster Diaphragm Meters AC-250 Diaphragm Meter" title="Elster Diaphragm Meters AC-250 Diaphragm Meter"/></div>
			<div id="PartsContent"><h3>Diaphragm Meters
			<br/><font size="2" color="#50658D">AC-250 Diaphragm Meter</font></h3>
			<p><br/>The aluminum class AC-250 is the industry's most cost-effective gas meter for 
			residential applications. It is unequaled for accuracy retention and for life cycle maintenance economies.
			<br/><br/><br/>
			<font color=#494A4A><b>Printable Literature</b></font>
			<br/><br/><a href="http://www.elster-americanmeter.com/downloads/EAM-DS3535.pdf"><font size="1" color="#ACB0C3"><b>Data Sheet</b></font></a>
			<br/><a href="http://www.elster-americanmeter.com/downloads/EAM-RPL3835.pdf"><font size="1" color="#ACB0C3"><b>Parts List</b></font></a></p>
			</div></div>
<div class="sliderGallery">
		<ul>
		<li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AC-250DiaphragmMeter_Thumbnail.gif" alt="Elster AC-250 Diaphragm Meter" title="Elster AC-250 Diaphragm Meter" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AM-250DiaphragmMeter_Thumbnail.gif" alt="Elster AM-250 Diaphragm Meter" title="Elster AM-250 Diaphragm Meter" /></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AT-210DiaphragmMeter_Thumbnail.gif" alt="Elster AT-210 Diaphragm Meter" title="Elster AT-210 Diaphragm Meter" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AL-425DiaphragmMeter_Thumbnail.gif" alt="Elster AL-425 Diaphragm Meter" title="Elster AL-425 Diaphragm Meter" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AC-630DiaphragmMeter_Thumbnail.gif" alt="Elster AC-630 Diaphragm Meter" title="Elster AC-630 Diaphragm Meter" /></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/CurbMeter_Thumbnail.gif" alt="Elster Curb Meter" title="Elster Curb Meter" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/BK-G4Meter_Thumbnail.gif" alt="Elster BK-G4 Meter" title="Elster BK-G4 Meter" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AL-800_AL-1000_Thumbnail.gif" alt="Elster Diaphragm Meters AL-800/AL-1000" title="Elster Diaphragm Meters AL-800/AL-1000" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AL-1400_AL-2300_AL-5000_Thumbnail.gif" alt="Elster Diaphragm Meters AL-1400/AL-2300/AL-5000" title="Elster Diaphragm Meters AL-1400/AL-2300/AL-5000" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RPM-CMTC_Thumbnail.gif" alt="Elster Rotary Meters RPM-CMTC" title="Elster Rotary Meters RPM-CMTC" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RPM-STD_Thumbnail.gif" alt="Elster Rotary Meters RPM STD" title="Elster Rotary Meters RPM STD" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RPM-ID_Thumbnail.gif" alt="Elster Rotary Meters RPM ID" title="Elster Rotary Meters RPM ID" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RPM-CMTC-ID_Thumbnail.gif" alt="Elster Rotary Meters RPM CMTC ID" title="Elster Rotary Meters RPM CMTC ID" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RPM-CMTCwithDirect-MountERT_Thumbnail.gif" alt="Elster Rotary Meters RPM/CMTC with Direct-Mount ERT" title="Elster Rotary Meters RPM/CMTC with Direct-Mount ERT" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/SSonic_Thumbnail.gif" alt="Elster Ultrasonic Meters S Sonic" title="Elster Ultrasonic Meters S Sonic" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/QSonic5Path_Thumbnail.gif" alt="Elster Ultrasonic Meters Q Sonic 5 Path" title="Elster Ultrasonic Meters Q Sonic 5 Path" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/QSonic-3C_Thumbnail.gif" alt="Elster Ultrasonic Meters Q Sonic - 3C" title="Elster Ultrasonic Meters Q Sonic - 3C" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/QSonic3Path_Thumbnail.gif" alt="Elster Ultrasonic Meters Q Sonic 3 Path" title="Elster Ultrasonic Meters Q Sonic 3 Path" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/TurbineGasMeterQ&QIC_Thumbnail.gif" alt="Elster Turbine Meters Turbine Gas Meter Q & QIC" title="Elster Turbine Meters Turbine Gas Meter Q & QIC" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/TurbineGasMeterX&XIC_Thumbnail.gif" alt="Elster Turbine Meters Turbine Gas Meter X & XIC" title="Elster Turbine Meters Turbine Gas Meter X & XIC" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AccuTestMeter_Thumbnail.gif" alt="Elster Turbine Meters AccuTest Meter" title="Elster Turbine Meters AccuTest Meter" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/GTS_GTXMeter_Thumbnail.gif" alt="Elster Turbine Meters GTS/GTX Meter" title="Elster Turbine Meters GTS/GTX Meter" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/QA_QAe_Thumbnail.gif" alt="Elster Submetering Equipment QA / QAe" title="Elster Submetering Equipment QA / QAe" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/WetTestMeters_Thumbnail.gif" alt="Elster Laboratory Meters Wet Test Meters" title="Elster Laboratory Meters Wet Test Meters" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/DryTestMeters_Thumbnail.gif" alt="Elster Laboratory Meters Dry Test Meters" title="Elster Laboratory Meters Dry Test Meters" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/KleanlineFilters_Thumbnail.gif" alt="Elster Kleanline Filters" title="Elster Kleanline Filters" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/StraighteningVanes_Thumbnail.gif" alt="Elster Straightening Vanes" title="Elster Straightening Vanes" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/Splashguard_Thumbnail.gif" alt="Elster Splashguard" title="Elster Splashguard" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/RemoteVolumePulser(RVP)_Thumbnail.gif" alt="Elster Submetering Equipment Remote Volume Pulser (RVP)" title="Elster Submetering Equipment Remote Volume Pulser (RVP)" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/LubricationPumps_Thumbnail.gif" alt="Elster Lubrication Pumps" title="Elster Lubrication Pumps" /></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/NeedleValves_Thumbnail.gif" alt="Elster Needle valves" title="Elster Needle valves" /></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/T210ElectronicOutreader_Thumbnail.gif" alt="Elster T210 Electronic Outreader" title="Elster T210 Electronic Outreader" /></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/SR100SEriesServiceRegulator_Thumbnail.gif" alt="Elster Pipe Size 1 1/4" or less SR100 Series Service Regulator" title="Elster Pipe Size 1 1/4" or less SR100 Series Service Regulator" /></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/J48(up-to-2-inch-size)_Thumbnail.gif" alt="Elster Pipe Size 1 1/4" or less J48 (up to 2 inch size)" title="Elster Pipe Size 1 1/4" or less J48 (up to 2 inch size)" /></a></li>
		<li><a href="#?w=400" rel="35" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/J78_Thumbnail.gif" alt="Elster Pipe Size 1 1/2' and 2 J78" title="Elster Pipe Size 1 1/2' and 2 J78" /></a></li>
		<li><a href="#?w=400" rel="36" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1200B2SeriesREgulators_Thumbnail.gif" alt="Elster 1200B2 Series Regulators" title="Elster 1200B2 Series Regulators" /></li>
		<li><a href="#?w=400" rel="37" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1200BSeriesRegulators_Thumbnail.gif" alt="Elster 1200B Series Regulators" title="Elster 1200B Series Regulators" /></a></li>
		<li><a href="#?w=400" rel="38" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1800B2SeriesPressureRegulators_Thumbnail.gif" alt="Elster 1800B2 Series Pressure Regulators" title="Elster 1800B2 Series Pressure Regulators" /></a></li>
		<li><a href="#?w=400" rel="39" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1800CPressureRegulators_Thumbnail.gif" alt="Elster 1800C Pressure Regulators" title="Elster 1800C Pressure Regulators" /></a></li>
		<li><a href="#?w=400" rel="40" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/3000SeriesRegulators_Thumbnail.gif" alt="Elster 3000 Series Regulators Pipe Size 3" title="Elster 3000 Series Regulators Pipe Size 3" /></a></li>
		<li><a href="#?w=400" rel="41" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/CR4000ServiceRegulator_Thumbnail.gif" alt="Elster CR4000 Service Regulator" title="Elster CR4000 Service Regulator" /></a></li>
		<li><a href="#?w=400" rel="42" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1800CPB2SeriesRegulators_Thumbnail.gif" alt="Elster 1800CPB2 Series Regulators" title="Elster 1800CPB2 Series Regulators" /></a></li>
		<li><a href="#?w=400" rel="43" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/AxialFlowValves_Thumbnail.gif" alt="Elster Pipe Size 1 1/2' and 2 Axial Flow Valves" title="Elster Pipe Size 1 1/2' and 2 Axial Flow Valves" /></a></li>
		<li><a href="#?w=400" rel="44" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/1800PFMSeriesIndustrialREgulator_Thumbnail.gif" alt="Elster Combustion Parts" title="Elster 1800 PFM Series Industrial Regulator" /></a></li>
		<li><a href="#?w=400" rel="45" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/3000SeriesRegulators2_Thumbnail.gif" alt="Elster 3000 Series Regulators Pipe Size 1 1/4" or less" title="Elster 3000 Series Regulators Pipe Size 1 1/4" or less" /></a></li>
		<li><a href="#?w=400" rel="46" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/3000SeriesRegulators3_Thumbnail.gif" alt="Elster 3000 Series RegulatorsPipe Size 1 1/2" and 2" title="Elster 3000 Series RegulatorsPipe Size 1 1/2" and 2" /></li>
		<li><a href="#?w=400" rel="47" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/60SeriesPilots_Thumbnail.gif" alt="Elster 60 Series Pilots" title="Elster 60 Series Pilots" /></a></li>
		<li><a href="#?w=400" rel="48" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/USSA_Thumbnail.gif" alt="Elster USSA" title="Elster USSA" /></a></li>
		<li><a href="#?w=400" rel="49" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/CFRFilters_Thumbnail.gif" alt="Elster CFR Filters" title="Elster CFR Filters" /></a></li>
		<li><a href="#?w=400" rel="50" class="Product"><img src="Parts_by_Man_OK_By_Jon/elster/thumbnails/GasketStrainers_Thumbnail.gif" alt="Elster Gasket Strainers" title="Elster Gasket Strainers"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>


