<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a full line of E-mon D-mon combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,E-mon D-mon,250 Series Gas Meter with Pulse Output,E-Mon Series 400 Gas Meter,Series 200 CFG Gas Meter,FM Series Gas Meter with Pulse Output,Class 1000 Single-Phase kWh Submeter,E-Mon Class 2000 Three-Phase kWh/kW (Demand) Submeter,E-Mon Class 3000 Advanced Submeter with Communications,E-Mon Class 4000 Multi-Family Single-Phase kWh Submeter,Class 5000 Advanced Submeter with Communications,E-Mon Green Class Meter with CO2 Carbon Footprint Data,Green Class Meter with Net Metering Capabilities,Split-Core Current Sensors,P3 Pulser Pulse Output for Interface to EMS/BMS,D/A (Digital to Analog) Converter Module,MMU Multiple Meter Unit Compact Enclosure Cabinets,Outdoor Enclosure Option,WDC Wireless Data Collector,Wireless Socket Meter Package,EWM Single-Point External Wireless Module,GW1 Wireless Module for Gas and Water meters,GW2 Dual-Point Wireless Module for Gas and Water meters,Energy with utility meters,Temperature Sensor,E-Mon PCA Interface Energy to Electromechanical Meters" />
<title>ETTER Engineering - E-mon D-mon Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="EmonLogoLarge"></div>
<div id="SensusText">E-Mon, established in 1981, is the leading manufacturer of solid-state electronic kWh submeters, energy monitors, interval data recorders and automatic meter reading software.
<br>E-Mon D-Mon submeters are installed worldwide for tenant billing, cost allocation, green building programs and load profiling in skyscrapers, shopping centers, airports, factories, office buildings, apartment complexes, industrial, governmental and educational facilities. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/250SeriesGasMeter.gif" alt="E-Mon 250 Series Gas Meter with Pulse Output" title="E-Mon 250 Series Gas Meter with Pulse Output"/></div>
			<div id="PartsContent"><h3>250 Series 
			<br/><font color="#50658D">Gas Meter with Pulse Output</font></h3>
			<p>&#149; 4-chamber diaphragm type gas meter.
			<br/>&#149; Ideal for light commercial or larger 
            		<br/>&nbsp;&nbsp;residential use.
			<br/>&#149; Equipped with pulse output for interfacing 
            		<br/>&nbsp;&nbsp;with interval data recorders.
			<br/>&#149; For use with loads ranging from 
            		<br/>&nbsp;&nbsp;200-250CuFt/Hr.
			<br/>&#149; Operating pressure 5 psi.
			<br/>&#149; Available for pipe sizes of 3/4" and 1".
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/gas250_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>250 Series Gas Meter Specification Sheet</b></font></a></p>
			
</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Series400GasMeter.gif" alt="E-Mon Series 400 Gas Meter" title="E-Mon Series 400 Gas Meter"/></div>
			<div id="PartsContent"><h3>Series 400 
			<br/><font color="#50658D">Gas Meter</font></h3>
			<p>E-Mon offers a variety of gas meters equipped with pulse output to meet your specific application. The meters listed below are the most commonly required units, however if your specifications require something different, please feel free to contact E-Mon directly at (800) 334-3666 to specify the appropriate meter.
			<br/><br/>&#149; Positive displacement type gas meter with 
             		<br/>&nbsp;&nbsp;three chambered design.
			<br/><br/>&#149; Ideal for small to medium commercial and 
             		<br/>&nbsp;&nbsp;industrial applications.
			<br/><br/>&#149; Equipped with pulse output for interfacing 
             		<br/>&nbsp;&nbsp;with interval data recorders.
			<br/><br/>&#149; For use with loads up to 400 CuFt/hr.
			<br/><br/>&#149; Operating pressure 10 psi.
			<br/><br/>&#149; Available for pipe sizes of 1" and 1.25".
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/gas400_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>400A Series Gas Meter with Specification Sheet</b></font></a></p>
			
</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Series200CFGMgasMeter.gif" alt="E-Mon Series 200 CFG Gas Meter" title="E-Mon Series 200 CFG Gas Meter"/></div>
			<div id="PartsContent"><h3>Series 200 CFG
			<br/><font color="#50658D">Gas Meter</font></h3>
			<p><br/>E-Mon offers a variety of gas meters equipped with pulse output to meet your specific application. The meters listed below are the most commonly required units, however if your specifications require something different, please feel free to contact E-Mon directly at (800) 334-3666 to specify the appropriate meter.
			<br/><br/>&#149; Four measuring chambers separate by 
             		<br/>&nbsp;&nbsp;synthetic diaphragms.
			<br/>&#149; Ideal for residential submetering 
             		<br/>&nbsp;&nbsp;applications for natural or LP gas.
			<br/><br/>&#149; Equipped with pulse output for interfacing 
             		<br/>&nbsp;&nbsp;with interval data recorders.
			<br/><br/>&#149; For use with loads up to 200 CuFt/hr.
			<br/><br/>&#149; Operating pressure 5 psi.
			<br/><br/>&#149; Requires installation connectors not 
             		<br/>&nbsp;&nbsp;supplied by E-Mon.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/gas200cfgm_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>200CFGM Gas Meter Specification Sheet</b></font></a></p>
			
</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/gasfmmeter.gif" alt="E-Mon FM Series Gas Meter with Pulse Output" title="E-Mon FM Series Gas Meter with Pulse Output"/></div>		
			<div id="PartsContent"><h3>FM Series 
			<br/><font color="#50658D">Gas Meter with Pulse Output</font></h3>
			<p><br/>E-Mon offers a variety of gas meters equipped with pulse output to meet your specific application. The meters listed below are the most commonly required units, however if your specifications require something different, please feel free to contact E-Mon directly at (800) 334-3666 to specify the appropriate meter.
			<br/><br/>&#149; Meter uses Fluidic Oscillating Principle and 
             		<br/>&nbsp;&nbsp;has no moving parts.
			<br/><br/>&#149; Ideal for commercial & industrial 
             		<br/>&nbsp;&nbsp;applications for flow rates from 40 to 
             		<br/>&nbsp;&nbsp;56,000 actual CuFt/Hr.
			<br/><br/>&#149; Equipped with pulse output for interfacing 
             		<br/>&nbsp;&nbsp;with interval data recorders.
			<br/><br/>&#149; For use with loads up to 56,000 CuFt/hr.
			<br/><br/>&#149; Operating pressure 150 psi.
			<br/><br/>&#149; Optionally available with Modbus 
             		<br/>&nbsp;&nbsp;communication for use with SCADA and 
             		<br/>&nbsp;&nbsp;other equipment that communicates via 
             		<br/>&nbsp;&nbsp;Modbus.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/gasfm_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>FM Series Gas Meter Specification Sheet</b></font></a></p>
			
</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Class1000Single-PhasekWhSubmeter.gif" alt="E-Mon Class 1000 Single-Phase kWh Submeter" title="E-Mon Class 1000 Single-Phase kWh Submeter"/></div>			
			<div id="PartsContent"><h3>Class 1000
			<br/><font color="#50658D">Single-Phase kWh Submeter</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Direct-read 8-digit LCD display without 
            		<br/>&nbsp;&nbsp;multiplier displays cumulative kWh and 
            		<br/>&nbsp;&nbsp;"real-time" kW load.
			<br/><br/>&#149; Revenue Grade Metering Accuracy.
			<br/><br/>&#149; Patented 0-2 volt output split-core 
            		<br/>&nbsp;&nbsp;current sensors promote enhanced safety 
            		<br/>&nbsp;&nbsp;and accurate remote mounting of current 
            		<br/>&nbsp;&nbsp;sensors up to 2,000 feet from the 
            		<br/>&nbsp;&nbsp;meter without power interruption. 
            		<br/>&nbsp;&nbsp;(Optional solid-core current sensors 
            		<br/>&nbsp;&nbsp;available in 100 & 200 Amp.)
			<br/>&#149; Parallel up to three (3) sets of current 
            		<br/>&nbsp;&nbsp;&nbsp;sensors for cumulative reading.
			<br/><br/>&#149; Current sensor installation diagnostics.
			<br/><br/>&#149; Meter can be used in the following 
            		<br/>&nbsp;&nbsp;configurations:
            		<br/>&nbsp;&nbsp;- 1-Phase, 2 Wire
            		<br/>&nbsp;&nbsp;- 2-Phase, 3 Wire
            		<br/>&nbsp;&nbsp;For other configurations contact factory.
			<br/><br/>&#149; Industrial grade JIC steel enclosure 
            		<br/>&nbsp;&nbsp;(Dim. 6 3/4" H x 5 3/16" W x 3 1/4" D) 
            		<br/>&nbsp;&nbsp;for indoor installations with 1 1/16" 
            		<br/>&nbsp;&nbsp;Knockout (3/4" conduit.) on bottom 
            		<br/>&nbsp;&nbsp;of enclosure.
			<br/><br/>&#149; Optional Enclosures:
            		<br/>&nbsp;&nbsp;- MMU Multiple Meter Unit Cabinets 
             		<br/>&nbsp;&nbsp;- NEMA 4X outdoor enclosure 
			<br/><br/>&#149; Optional removable terminal block for 
            		<br/>&nbsp;&nbsp;pulse output available for 2W 120V & 
            		<br/>&nbsp;&nbsp;3W 208V meters.
			<br/><br/>&#149; Padlocking hasp & mounting flanges.
			<br/><br/>&#149; Maintains reading in the event of power 
            		<br/>&nbsp;&nbsp;failure.
			<br/><br/>&#149; Compatible with E-Mon D-Mon accessories.
			<br/><br/>&#149; Non-volatile memory.
			<br/><br/>&#149; UL/CUL Listed.
			<br/><br/>&#149; Certified to ANSI C12.1 and C12.16 
            		<br/>&nbsp;&nbsp;electronic meter national accuracy 
            		<br/>&nbsp;&nbsp;standards. (+/-1% from 1% to 100% 
            		<br/>&nbsp;&nbsp;of rated load.)
			<br/><br/>&#149; California CTEP approved for use with 
            		<br/>&nbsp;&nbsp;solid-core current sensors. Listed by 
            		<br/>&nbsp;&nbsp;the California Energy Commission.
			<br/><br/>&#149; New York City approved, Con Edison 
            		<br/>&nbsp;&nbsp;&nbsp;approved for RSP program.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_1000spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 1000 Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/manuals/1000_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 1000 Installation Manual</b></font></a>
			</p>
</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Class2000Three-PhasekWh_kW(Demand)Submeter.gif" alt="E-Mon Class 2000 Three-Phase kWh/kW (Demand) Submeter" title="E-Mon Class 2000 Three-Phase kWh/kW (Demand) Submeter"/></div>			
			<div id="PartsContent"><h3>Class 2000
			<br/><font color="#50658D">Three-Phase kWh/kW (Demand) Submeter</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Direct-read 8-digit LCD display without 
            		<br/>&nbsp;&nbsp;multiplier displays cumulative kWh and 
            		<br/>&nbsp;&nbsp;"real-time" kW load.
			<br/><br/>&#149; Demand option displays kW/Demand and 
            		<br/>&nbsp;&nbsp;kW peak date and time (15 minute 
            		<br/>&nbsp;&nbsp;interval standard, 30 minute interval 
            		<br/>&nbsp;&nbsp;available.)
			<br/><br/>&#149; Revenue Grade Metering Accuracy.
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
            		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
            		<br/>&nbsp;&nbsp;accurate remote mounting of current 
            		<br/>&nbsp;&nbsp;sensors up to 2,000 feet from the meter 
            		<br/>&nbsp;&nbsp;without power interruption. (Optional 
            		<br/>&nbsp;&nbsp;solid-core current sensors available in 100 
            		<br/>&nbsp;&nbsp;& 200 Amp.)
			<br/><br/>&#149; Current sensor installation diagnostics 
            		<br/>&nbsp;&nbsp;indicator.
			<br/><br/>&#149; Parallel up to three (3) sets of current 
            		<br/>&nbsp;&nbsp;sensors for cumulative reading.
			<br/><br/>&#149; Meter can be used in the following 
            		<br/>&nbsp;&nbsp;configurations:
             		<br/>&nbsp;&nbsp;&nbsp;- 3-Phase, 4 Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 3-Phase, 3 Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 2-Phase, 3 Wire
             		<br/>&nbsp;&nbsp;&nbsp;For other configurations contact factory.
			<br/><br/>&#149; Optional removal terminal block for 
            		<br/>&nbsp;&nbsp;fixed-value pulse output. 
			<br/><br/>&#149; Industrial grade JIC steel enclosure with 
            		<br/>&nbsp;&nbsp;padlocking hasp and mounting flanges 
            		<br/>&nbsp;&nbsp;(Dim. 6 3/4" H x 5 3/16" W x 3 1/4" D)
            		<br/>&nbsp;&nbsp;for indoor installations with 1 1/16" 
            		<br/>&nbsp;&nbsp;Knockout (3/4" conduit) on bottom of 
            		<br/>&nbsp;&nbsp;enclosure.
			<br/><br/>&#149; Optional Enclosures:
             		<br/>&nbsp;&nbsp;&nbsp;- MMU Multiple Meter Unit Cabinets 
             		<br/>&nbsp;&nbsp;&nbsp;- NEMA 4X outdoor enclosure 
			<br/><br/>&#149; Compatible with E-Mon D-Mon accessories.
			<br/><br/>&#149; Non-volatile memory.
			<br/><br/>&#149; UL/CUL Listed. CSA Approved
			<br/><br/>&#149; Revenue Grade Accuracy. Meter certified 
            		<br/>&nbsp;&nbsp;toIEEE/ANSI C12.20 accuracy standard.
			<br/><br/>&#149; California CTEP approved for use with 
            		<br/>&nbsp;&nbsp;solid-core current sensors. Listed by 
            		<br/>&nbsp;&nbsp;the California Energy Commission.
			<br/><br/>&#149; New York City approved, Con Edison 
            		<br/>&nbsp;&nbsp;approved for RSP program.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_2000spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 2000 Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 2000 Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/2000_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 2000 Installation Manual</b></font></a>
			</p>
</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Class3000AdvancedSubmeterwithCommunications.gif" alt="E-Mon Class 3000 Advanced Submeter with Communications" title="E-Mon Class 3000 Advanced Submeter with Communications"/></div>
			<div id="PartsContent"><h3>Class 3000 
			<br/><font color="#50658D">Advanced Submeter with Communications</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Easy to read cycling 4-line by 20 character 
            		<br/>&nbsp;&nbsp;backlit LCD display:
             		<br/>&nbsp;&nbsp;&nbsp;- kWh
             		<br/>&nbsp;&nbsp;&nbsp;- kWh/Demand (with peak date and time)
             		<br/>&nbsp;&nbsp;&nbsp;- Power factor per Phase
             		<br/>&nbsp;&nbsp;&nbsp;- Real-time load in kW
             		<br/>&nbsp;&nbsp;&nbsp;- Amps per Phase
             		<br/>&nbsp;&nbsp;&nbsp;- Volts per Phase
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
            		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
            		<br/>&nbsp;&nbsp;accurate remote mounting of current 
            		<br/>&nbsp;&nbsp;sensors up to 500 feet from the meter 
            		<br/>&nbsp;&nbsp;without power interruption. 
            		<br/>&nbsp;&nbsp;(Optional solid-core current sensors 
            		<br/>&nbsp;&nbsp;available in 100 & 200 Amp.)
			<br/><br/>&#149; Installation diagnostics and verification 
            		<br/>&nbsp;&nbsp;system.
			<br/><br/>&#149; Revenue Grade Accuracy
			<br/><br/>&#149; RS-485 communication capability supports 
            		<br/>&nbsp;&nbsp;the following connection configurations 
            		<br/>&nbsp;&nbsp;(or combinations not to exceed 52 devices 
            		<br/>&nbsp;&nbsp;per channel):
			<br/>&nbsp;&nbsp;&nbsp;- Up to 52 Class 3000 meters and/or 
            		<br/>&nbsp;&nbsp;IDR-8 interval data recorders
             		<br/>&nbsp;&nbsp;&nbsp;- Up to 26 IDR-16 interval data recorders 
            		<br/>&nbsp;&nbsp;(IDR-16 counts as two devices)
			<br/><br/>Cabling can either be daisy-chain or star configuration, 4-conductor, 24-26 AWG, up to 4,000 cable feet total per channel.
			<br/><br/>&#149; Communication Options:
			<br/>Requires E-Mon Energy software for reading:
             		<br/>&nbsp;&nbsp;&nbsp;- RS-232/RS-485 (Standard)
             		<br/>&nbsp;&nbsp;&nbsp;- Telephone Modem
             		<br/>&nbsp;&nbsp;&nbsp;- Ethernet
			<br/><br/>Requires third-party EMS/BMS system supplied by others. E-Mon Energy software not used:
             		<br/>&nbsp;&nbsp;&nbsp;- Modbus RTU or TCP/IP
             		<br/>&nbsp;&nbsp;&nbsp;- BACnet IP or MS/TP
             		<br/>&nbsp;&nbsp;&nbsp;- LONworks TP (Twisted Pair)
			<br/><br/>&#149; Records kWh and kVARh data for two 
            		<br/>&nbsp;&nbsp;channels. Data stored in 15-minute 
            		<br/>&nbsp;&nbsp;intervals for up to 36 days or 
            		<br/>&nbsp;&nbsp;5-minute intervals for up to 12 days. 
            		<br/>&nbsp;&nbsp;Maintains the last 36 days of data 
            		<br/>&nbsp;&nbsp;in a first-in, first-out format.
			<br/><br/>&#149; External meter pulse input (water, gas, 
            		<br/>&nbsp;&nbsp;BTU, etc.) on 3rd channel.
			<br/><br/>&#149; Meter is designed for use on both 
            		<br/>&nbsp;&nbsp;3-phase, 3-wire (delta) and 3-phase, 
            		<br/>&nbsp;&nbsp;4-wire (wye) circuits. (Specify when 
            		<br/>&nbsp;&nbsp;ordering.)
			<br/><br/>&#149; Industrial grade JIC steel enclosure 
            		<br/>&nbsp;&nbsp;(Dim: 9 1/2" H x 6 3/4" W x 4 1/4" D) with 
            		<br/>&nbsp;&nbsp;padlocking hasp and mounting flanges 
            		<br/>&nbsp;&nbsp;(Dim. 6 3/4" H x 5 3/16" W x 3 1/4" D) 
            		<br/>&nbsp;&nbsp;for indoor installations with three 
            		<br/>&nbsp;&nbsp;1 1/16" Knockouts (3/4" conduit) 
            		<br/>&nbsp;&nbsp;on bottom of enclosure.
			<br/><br/>&#149; UL/CUL Listed. Meets or exceeds ANSI 
            		<br/>&nbsp;&nbsp;C12 national accuracy standards.
			<br/><br/>&#149; Optional load control/alarm relay 
            		<br/>&nbsp;&nbsp;(3A, 240V max.) with high and low 
            		<br/>&nbsp;&nbsp;threshold adjustment.
			<br/><br/>&#149; MV-90 compatible.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_3000spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 3000 Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 3000 Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/3000_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 3000 Installation Manual</b></font></a>
			</p>
</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Class4000Multi-FamilySingle-PhasekWhSubmeter.gif" alt="E-Mon Class 4000 Multi-Family Single-Phase kWh Submeter" title="E-Mon Class 4000 Multi-Family Single-Phase kWh Submeter"/></div>			
			<div id="PartsContent"><h3>Class 4000
			<br/><font color="#50658D">Multi-Family Single-Phase kWh Submeter</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Easy-to-read 6-digit electromechanical 
             		<br/>&nbsp;&nbsp;display for diagnostics.
			<br/><br/>&#149; Meter has built-in wireless transceiver for 
             		<br/>&nbsp;&nbsp;in-building remote meter data collection 
             		<br/>&nbsp;&nbsp;and can interface with E-Mon wireless data 
             		<br/>&nbsp;&nbsp;collectors (WDC) to automatically compile 
             		<br/>&nbsp;&nbsp;data in PC-based E-Mon Energy metering 
             		<br/>&nbsp;&nbsp;software.
			<br/><br/>&#149; Meters with built-in wireless transceivers 
             		<br/>&nbsp;&nbsp;can be mounted inside buildings within 
             		<br/>&nbsp;&nbsp;approximately 500 feet line-of-sight from 
             		<br/>&nbsp;&nbsp;each other and up to approximately 200 
             		<br/>&nbsp;&nbsp;feet through walls, depending on wall 
             		<br/>&nbsp;&nbsp;material.
			<br/><br/>&#149; Revenue Grade Metering Accuracy.
			<br/><br/>&#149; On-board memory.
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
             		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
             		<br/>&nbsp;&nbsp;accurate remote mounting of current 
             		<br/>&nbsp;&nbsp;sensors up to 2,000 feet from the meter 
             		<br/>&nbsp;&nbsp;without power interruption. (Optional 
             		<br/>&nbsp;&nbsp;solid-core current sensors available in 100 
             		<br/>&nbsp;&nbsp;& 200 Amp.)
			<br/><br/>&#149; Meter can be used in the following 
             		<br/>&nbsp;&nbsp;configurations:
             		<br/>&nbsp;&nbsp;&nbsp;- 1-Phase, 2 Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 2-Phase, 3 Wire
			<br/><br/>&#149; Non-metallic enclosure is Ideal for 
             		<br/>&nbsp;&nbsp;installation inside tenant and 
             		<br/>&nbsp;&nbsp;residential spaces.
			<br/><br/>&#149; Wireless mesh network operates in the 
             		<br/>&nbsp;&nbsp;915 MHz license-free band. No cellular 
             		<br/>&nbsp;&nbsp;wireless service contracts are required.
			<br/><br/>&#149; Fully self-configuring wireless mesh 
             		<br/>&nbsp;&nbsp;network allows for easy installation and 
             		<br/>&nbsp;&nbsp;configuration with no network management 
             		<br/>&nbsp;&nbsp;required.
			<br/><br/>&#149; Sealing option for California applications 
             		<br/>&nbsp;&nbsp;available.
			<br/><br/>&#149; FCC certified not to interfere with existing 
             		<br/>&nbsp;&nbsp;infrastructure.
			<br/><br/>&#149; Meets national accuracy standards of 
             		<br/>&nbsp;&nbsp;ANSI C12.1 and C12.16.
			<br/><br/>&#149; UL Listed. New York City approved, 
             		<br/>&nbsp;&nbsp;Con Edison approved for RSP program. 
             		<br/>&nbsp;&nbsp;Measurement Canada approved for 
             		<br/>&nbsp;&nbsp;revenue metering. California Bureau 
             		<br/>&nbsp;&nbsp;of Weights & Measures certified. 
             		<br/>&nbsp;&nbsp;Listed by the California Energy 
             		<br/>&nbsp;&nbsp;Commission.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_4000spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 4000 Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 4000 Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/4000_4100manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 4000 Installation Manual</b></font></a>
			</p>
</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/Class5000AdvancedSubmeterwithCommunications.gif" alt="E-Mon Class 5000 Advanced Submeter with Communications" title="E-Mon Class 5000 Advanced Submeter with Communications"/></div>
			<div id="PartsContent"><h3>Class 5000
			<br/><font color="#50658D">Advanced Submeter with Communications</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Direct-read 8-digit LCD display of 
             		<br/>&nbsp;&nbsp;cumulative kWh.
			<br/><br/>&#149; E-Mon Energy software supports load 
             		<br/>&nbsp;&nbsp;profiling (5 or 15 minute) of kWh & kVARh
			<br/><br/>&#149; E-Mon Energy software supports real-time 
             		<br/>&nbsp;&nbsp;reading of:
             		<br/>&nbsp;&nbsp;&nbsp;- Amps per Phase
             		<br/>&nbsp;&nbsp;&nbsp;- Volts per Phase-to-Neutral
             		<br/>&nbsp;&nbsp;&nbsp;- Volts per Phase-to-Phase
             		<br/>&nbsp;&nbsp;&nbsp;- kW, kVA and kVAR per Phase
             		<br/>&nbsp;&nbsp;&nbsp;- Total kW, kVA and kVAR
             		<br/>&nbsp;&nbsp;&nbsp;- Frequency
             		<br/>&nbsp;&nbsp;&nbsp;- Power Factor Percent & Angle per Phase
			<br/><br/>&#149; Open protocol for easy integration to 
             		<br/>&nbsp;&nbsp;various building automation and energy 
             		<br/>&nbsp;&nbsp;management systems, including optional 
             		<br/>&nbsp;&nbsp;Modbus RTU & TCP/IP, BACnet IP & MS/TP 
             		<br/>&nbsp;&nbsp;and LonWorks Twisted Pair.
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
             		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
             		<br/>&nbsp;&nbsp;accurate remote mounting of current 
             		<br/>&nbsp;&nbsp;sensors up to 500 feet from the meter 
             		<br/>&nbsp;&nbsp;without power interruption. (Optional 
             		<br/>&nbsp;&nbsp;solid-core current sensors available in 
             		<br/>&nbsp;&nbsp;100 & 200 Amp.)
			<br/><br/>&#149; Current sensor installation diagnostics 
             		<br/>&nbsp;&nbsp;indicator on display.
			<br/><br/>&#149; Revenue Grade Accuracy.
			<br/><br/>&#149; RS-485 Communications capability 
             		<br/>&nbsp;&nbsp;supports the following connection 
             		<br/>&nbsp;&nbsp;configurations (or combinations not 
             		<br/>&nbsp;&nbsp;to exceed 52 devices per channel):
             		<br/>&nbsp;&nbsp;&nbsp;- Up to 52 Class 3000 meters and/or 
             		<br/>&nbsp;&nbsp;IDR-8 interval data recorders
             		<br/>&nbsp;&nbsp;&nbsp;- Up to 26 IDR-16 interval data recorders 
             		<br/>&nbsp;&nbsp;(IDR-16 counts as two devices)
			<br/><br/>Cabling can be either daisy-chain or star configuration, 4-conductor, 24-26 AWG, up to 4,000 cable feet total per channel.
			<br/><br/>&#149; Communication Options:
    			<br/>Requires E-Mon Energy software for reading:
             		<br/>&nbsp;&nbsp;&nbsp;- RS-232/RS-485 (Standard)
             		<br/>&nbsp;&nbsp;&nbsp;- Telephone Modem
             		<br/>&nbsp;&nbsp;&nbsp;- Ethernet
        		<br/><br/>Requires third-party EMS/BMS system supplied by others. E-Mon Energy software not used:
             		<br/>&nbsp;&nbsp;&nbsp;- Modbus RTU or TCP/IP
             		<br/>&nbsp;&nbsp;&nbsp;- BACnet IP or MS/TP
             		<br/>&nbsp;&nbsp;&nbsp;- LONworks TP (Twisted Pair)
			<br/><br/>&#149; Modbus and BACnet options support two 
             		<br/>&nbsp;&nbsp;external inputs from gas, water, etc. 
             		<br/>&nbsp;&nbsp;meters. (Dry contact, 10 Hz max. input.) 
             		<br/>&nbsp;&nbsp;Modbus RTU, BACnet IP & MS/TP available 
             		<br/>&nbsp;&nbsp;with RS-485. Modbus TCP/IP or BACnet IP 
             		<br/>&nbsp;&nbsp;requires Ethernet. Standard EZ-7 protocol 
             		<br/>&nbsp;&nbsp;supports one external input.
			<br/><br/>&#149; Meter is designed for use on both 
             		<br/>&nbsp;&nbsp;3-phase, 3-wire (delta) and 3-phase, 
             		<br/>&nbsp;&nbsp;4-wire (wye) circuits. (Specify when 
             		<br/>&nbsp;&nbsp;ordering.)
			<br/><br/>&#149; Available in standard JIC industrial-grade 
             		<br/>&nbsp;&nbsp;steel enclosure (gray-standard, 
             		<br/>&nbsp;&nbsp;green-optional) 
             		<br/>&nbsp;&nbsp;(Dim: 7 1/4"H x 7"W x 3 1/4"D) 
             		<br/>&nbsp;&nbsp;or optional NEMA 4X outdoor enclosure 
             		<br/>&nbsp;&nbsp;(Dim: 7 1/2"H x 7 1/2"W x 4"D.)
			<br/><br/>&#149; UL/CUL Listed. CSA Approved.
			<br/><br/>&#149; Revenue Grade Accuracy. Meter certified 
             		<br/>&nbsp;&nbsp;to IEEE/ANSI C12.20 accuracy standard.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_5000spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 5000 Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 5000 Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/5000_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Class 5000 Installation Manual</b></font></a>
			</p>
</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/GreenClassMeterwithCO2andCarbonFootprintData.gif" alt="E-Mon Green Class Meter with CO2 & Carbon Footprint Data" title="E-Mon Green Class Meter with CO2 & Carbon Footprint Data"/></div>
			<div id="PartsContent"><h3>Green Class Meter
			<br/><font color="#50658D">with CO2 & Carbon Footprint Data</font></h3>
			<p>Features
			<br/><br/>&#149; Direct-read 8-digit LCD display without 
             		<br/>&nbsp;&nbsp;multiplier displays cumulative kWh and 
             		<br/>&nbsp;&nbsp;"real-time kW load.
			<br/><br/>&#149; User entered cost per kWh provides 
             		<br/>&nbsp;&nbsp;to-date energy cost and projected hourly 
             		<br/>&nbsp;&nbsp;cost based on metered load.
			<br/><br/>&#149; Displays total carbon (CO2) emissions in 
             		<br/>&nbsp;&nbsp;pounds (lbs.) and indicates hourly 
             		<br/>&nbsp;&nbsp;emissions based on metered load.
			<br/><br/>&#149; Revenue Grade Metering Accuracy.
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
             		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
             		<br/>&nbsp;&nbsp;accurate remote mounting of current 
             		<br/>&nbsp;&nbsp;sensors up to 2,000 feet from the meter 
             		<br/>&nbsp;&nbsp;without power interruption. (Optional 
             		<br/>&nbsp;&nbsp;solid-core current sensors available in 100 
             		<br/>&nbsp;&nbsp;& 200 Amp only.)
			<br/><br/>&#149; Current sensor installation diagnostics 
             		<br/>&nbsp;&nbsp;indicator.
			<br/><br/>&#149; Parallel up to three (3) sets of current 
             		<br/>&nbsp;&nbsp;sensors for cumulative reading.
			<br/><br/>&#149; Meter can be used in the following 
             		<br/>&nbsp;&nbsp;configurations:
             		<br/>&nbsp;&nbsp;&nbsp;- 3-Phase, 4 Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 3-Phase, 3 Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 2-Phase, 3 Wire
             		<br/>&nbsp;&nbsp;&nbsp; For other configurations contact factory.
			<br/><br/>&#149; Optional plug-in terminal block for pulse 
             		<br/>&nbsp;&nbsp;output.
			<br/><br/>&#149; Industrial grade JIC steel enclosure with 
             		<br/>&nbsp;&nbsp;padlocking hasp and mounting flanges 
             		<br/>&nbsp;&nbsp;(Dim. 6 3/4" H x 5 3/16" W x 3 1/4" D) 
             		<br/>&nbsp;&nbsp;for indoor installations with 1 1/16" 
             		<br/>&nbsp;&nbsp;Knockout (3/4" conduit) on bottom of 
             		<br/>&nbsp;&nbsp;enclosure.
			<br/><br/>&#149; Optional Enclosures:
             		<br/>&nbsp;&nbsp;&nbsp;- MMU Multiple Meter Unit Cabinets 
             		<br/>&nbsp;&nbsp;&nbsp;- NEMA 4X outdoor enclosure 
			<br/><br/>&#149; Non-volatile memory.
			<br/><br/>&#149; UL/CUL Listed. CSA Approved
			<br/><br/>&#149; Certified to ANSI C12.1 and C12.16 
             		<br/>&nbsp;&nbsp;electronic meter national accuracy 
             		<br/>&nbsp;&nbsp;standards. (+/-1% from 1% to 100% of 
             		<br/>&nbsp;&nbsp;rated load.)
			<br/><br/>&#149; New York City approved, Con Edison 
             		<br/>&nbsp;&nbsp;approved for RSP program.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_greenspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Green Class Meter Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/green_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Installation Manual</b></font></a>
			</p>
</div></div>
<div id="11" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/GreenClassMeterwithNetMeteringCapabilities.gif" alt="E-Mon Green Class Meter with Net Metering Capabilities" title="E-Mon Green Class Meter with Net Metering Capabilities"/></div>	
			<div id="PartsContent"><h3>Green Class Meter 
			<br/><font color="#50658D">with Net Metering Capabilities</font></h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Direct-read 8-digit LCD display of delivered 
             		<br/>&nbsp;&nbsp;kWh, received kWh, net kWh and 
             		<br/>&nbsp;&nbsp;real-time load in kW.
			<br/><br/>&#149; Meter provides load profile data of 
             		<br/>&nbsp;&nbsp;delivered kWh, delivered kVARh and 
             		<br/>&nbsp;&nbsp;received kWh along with real-time readings 
             		<br/>&nbsp;&nbsp;of Power Factor, kW, kVa, kVAR, Amps per 
             		<br/>&nbsp;&nbsp;phase, Volts per phase and Frequency; 
             		<br/>&nbsp;&nbsp;available via E-Mon Energy software.
			<br/><br/>&#149; Displays total carbon (CO2) emissions in 
             		<br/>&nbsp;&nbsp;pounds (lbs.) based on DOE national 
             		<br/>&nbsp;&nbsp;average and indicates hourly emissions 
             		<br/>&nbsp;&nbsp;based on metered load.
			<br/><br/>&#149; Communication options include telephone 
             		<br/>&nbsp;&nbsp;modem, RS-232/RS-485 and Ethernet.
			<br/><br/>&#149; MV-90 compatible.
			<br/><br/>&#149; Patented 0-2 volt output split-core current 
             		<br/>&nbsp;&nbsp;sensors promote enhanced safety and 
             		<br/>&nbsp;&nbsp;accurate remote mounting of current 
             		<br/>&nbsp;&nbsp;sensors up to 500 feet from the meter 
             		<br/>&nbsp;&nbsp;without power interruption. (Optional 
             		<br/>&nbsp;&nbsp;solid-core current sensors available in 100 
             		<br/>&nbsp;&nbsp;& 200 Amp only.)
			<br/><br/>&#149; Current sensor installation diagnostics 
             		<br/>&nbsp;&nbsp;indicator.
			<br/><br/>&#149; Optional fixed-value pulse output of 
             		<br/>&nbsp;&nbsp;delivered kWh.
			<br/><br/>&#149; Non-volatile memory.
			<br/><br/>&#149; Industrial grade green JIC steel enclosure 
             		<br/>&nbsp;&nbsp;with padlocking hasp and mounting 
             		<br/>&nbsp;&nbsp;flanges (Dim. 7 1/4" H x 7" W x 3 1/4" D) 
             		<br/>&nbsp;&nbsp;for indoor installations with 1 1/16" 
             		<br/>&nbsp;&nbsp;Knockout (3/4" conduit) on bottom of 
             		<br/>&nbsp;&nbsp;enclosure.
			<br/><br/>&#149; Optional Enclosures:
             		<br/>&nbsp;&nbsp;&nbsp;- NEMA 4X (gray) outdoor enclosure
			<br/><br/>&#149; Non-volatile memory.
			<br/><br/>&#149; UL/CUL Listed.
			<br/><br/>&#149; Certified to ANSI C12.1 and C12.16 
             		<br/>&nbsp;&nbsp;electronic meter national accuracy 
             		<br/>&nbsp;&nbsp;standards. (+/-1% from 1% to 100% of 
             		<br/>&nbsp;&nbsp;rated load.)
			<br/><br/>&#149; Listed as eligible system performance 
             		<br/>&nbsp;&nbsp;meter by California Solar Initiative 
             		<br/>&nbsp;&nbsp;Emerging Renewables Program.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/class_greennetspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Green Class Net Meter Specification Sheet</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/meter_install_overview.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Installation Overview</b></font></a>
			<br/><a href="http://www.emon.com/manuals/greennet_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Installation Manual</b></font></a>
			</p>
</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monD-monSplit-CoreCurrentSensors.gif" alt="E-Mon Split-Core Current Sensors" title="E-Mon Split-Core Current Sensors"/></div>		
			<div id="PartsContent"><h3>Split-Core Current Sensors</h3>
			<p><b>Features:</b>
			<br/><br/>&#149; E-Mon's patented 0-2V output split-core 
             		<br/>&nbsp;&nbsp;current sensors allow users to easily 
             		<br/>&nbsp;&nbsp;monitor energy usage in new or retrofit 
             		<br/>&nbsp;&nbsp;applications. Sensors are sized according 
             		<br/>&nbsp;&nbsp;to the meter they are installed with and 
             		<br/>&nbsp;&nbsp;are sized to fit the typical wire size 
             		<br/>&nbsp;&nbsp;for that amperage.
			<br/><br/>&#149; E-Mon current sensors are installed inside 
             		<br/>&nbsp;&nbsp;the electrical panel and "snap" around 
             		<br/>&nbsp;&nbsp;the load wires to be monitored. The 
             		<br/>&nbsp;&nbsp;sensors can monitor anything from a 
             		<br/>&nbsp;&nbsp;single branch circuit, to multiple circuits, 
             		<br/>&nbsp;&nbsp;to the entire building.
			<br/><br/>&#149; E-Mon current sensors can be installed up 
             		<br/>&nbsp;&nbsp;to 2,000 feet away from the meter (500 
             		<br/>&nbsp;&nbsp;feet on Class 3000 & Class 5000 meters) 
             		<br/>&nbsp;&nbsp;by extending the current sensor leads 
             		<br/>&nbsp;&nbsp;using low voltage #14-22 AWG wire 
             		<br/>&nbsp;&nbsp;(stranded/twisted not required). 3' leads 
             		<br/>&nbsp;&nbsp;are supplied from the factory, consult 
             		<br/>&nbsp;&nbsp;local electrical costs for proper wiring 
             		<br/>&nbsp;&nbsp;size requirements.
			<br/><br/>&#149; Current sensors can be installed around 
             		<br/>&nbsp;&nbsp;multiple load wires from the same source 
             		<br/>&nbsp;&nbsp;to provide a cumulative reading of usage 
             		<br/>&nbsp;&nbsp;for all of the loads. Note: Phase 
             		<br/>&nbsp;&nbsp;correspondence is required, and all loads 
             		<br/>&nbsp;&nbsp;must be from the same voltage 
             		<br/>&nbsp;&nbsp;source/transformer.
			<br/><br/>&#149; Multiple sets of current sensors can be 
             		<br/>&nbsp;&nbsp;installed on one meter to provide a 
             		<br/>&nbsp;&nbsp;cumulative reading of multiple loads from 
             		<br/>&nbsp;&nbsp;different panels. Up to 3 sets of sensors 
             		<br/>&nbsp;&nbsp;can be installed on one meter. Note: Phase 
             		<br/>&nbsp;&nbsp;correspondence is required, all loads must 
             		<br/>&nbsp;&nbsp;be from the same voltage 
             		<br/>&nbsp;&nbsp;source/transformer, and all current sensors 
             		<br/>&nbsp;&nbsp;must be of the same amperage rating.
			<br/><br/>&#149; Current sensors can be used for 
             		<br/>&nbsp;&nbsp;subtractive metering. An example of 
             		<br/>&nbsp;&nbsp;subtractive metering is one set of current 
             		<br/>&nbsp;&nbsp;sensors installed around the main load 
             		<br/>&nbsp;&nbsp;wires in a panel. A second set of current 
             		<br/>&nbsp;&nbsp;sensors is installed around the load wires 
             		<br/>&nbsp;&nbsp;going to a subpanel, but the current 
             		<br/>&nbsp;&nbsp;sensors are reversed during installation. 
             		<br/>&nbsp;&nbsp;This will provide a net reading of the total 
             		<br/>&nbsp;&nbsp;panel less the sub panel. Note: Phase 
             		<br/>&nbsp;&nbsp;correspondence is required, all loads must 
             		<br/>&nbsp;&nbsp;be from the same voltage 
             		<br/>&nbsp;&nbsp;source/transformer, and all current sensors 
             		<br/>&nbsp;&nbsp;must be of the same amperage rating.
			<br/><br/>&#149; Optional solid-core current sensors are 
             		<br/>&nbsp;&nbsp;available in 100 & 200 amp configuration, 
             		<br/>&nbsp;&nbsp;specify when ordering.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/sensorspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Split-Core Current Sensor Specifications</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/sensorassembly.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Split-Core Current Sensor Assembly</b></font></a>
			</p>
</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/P3Pulser-PluseOutputforInterfacetoEMS_BMS.gif" alt="E-Mon P3 Pulser Pulse Output for Interface to EMS/BMS" title="E-Mon P3 Pulser Pulse Output for Interface to EMS/BMS"/></div>			
			<div id="PartsContent"><h3>P3 Pulser
			<br/><font color="#50658D">Pulse Output for Interface to EMS/BMS</font></h3>
			<p><b>Features</b>
			<br/>The E-Mon D-Mon P3 Pulser is an optically coupled interface device that allows E-Mon D-Mon Class 1000 & Class 2000 kWh & kWh/Demand meters to be connected to an energy management (EMS) or building automation system (BAS) for data-gathering and/or load control. The pulse width and value are selected by 2 DIP switches and can be tailored to fit your specific requirements in the field. A modular plug connects the pulser to the E-Mon D-Mon meter; a two-screw terminal provides easy connection to the EMS/BAS. A LED on the pulser shows the rate and duration of the pulse. The pulser has an operating range of 1.5 to 36 volts AC or DC (supplied by the EMS/BAS.)
			<br/><br/><b>Applications include:</b>
			<br/><br/>&#149; Tenant billing, based on both kilowatt hour 
             		<br/>&nbsp;&nbsp;(kWh) and kilowatt (kW) demand 
             		<br/>&nbsp;&nbsp;information from the E-Mon D-Mon meters 
             		<br/>&nbsp;&nbsp;through the pulsers.
			<br/><br/>&#149; "Real-Time demand reading, allowing the 
             		<br/>&nbsp;&nbsp;user to see the effects of loads as they 
             		<br/>&nbsp;&nbsp;come on or off-line.
			<br/><br/>&#149; Automatic load/shedding/limiting by the 
             		<br/>&nbsp;&nbsp;EMS to lower usage and costs.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/p3pulser_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>P3 Pulser Specifications</b></font></a>
			<br/><a href="http://www.emon.com/manuals/P3_pulser%20manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>P3 Pulser Installation Instruction Manual</b></font></a>
			</p>
</div></div>
<div id="14" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/D_A(DigitaltoAnalog)ConverterModule.gif" alt="E-Mon D/A (Digital to Analog) Converter Module" title="E-Mon D/A (Digital to Analog) Converter Module"/></div>		
			<div id="PartsContent"><h3>D/A (Digital to Analog)
			<br/><font color="#50658D">Converter Module</font></h3>
			<p><b>Features:</b>
			<br/><br/>The E-Mon D-Mon D/A Converter (Digital to Analog) module is used with E-Mon D-Mon Class 2000 meters to interface with analog meters, chart recorders and programmable controllers.
			<br/><br/>D/A Converter Specifications:
			<br/><br/>&#149; Input Voltage: 120 VAC, 60 Hz
			<br/><br/>&#149; Available Outputs: 
			<br/> - 0-10 volts DC (at 10 mA max.)
			<br/> - 0-1 mA (into load of 0-10K ohms)
			<br/> - 4-20 mA (into load of 0-250K ohms)
			<br/><br/>&#149; Operating Temperature: 0&deg;C to 60&deg;C	
			<br/><br/>&#149; Dimensions: 6 3/4"H x 3 3/4"W x 3 1/4"D		
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/daconverter_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>D/A Converter Specifications</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/daconverter_installation.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>D/A Converter Installation Overview</b></font></a>
			</p>
</div></div>
<div id="15" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/MMUMultipeMeterUnitCompactEnclosureCabinets.gif" alt="E-Mon MMU Multiple Meter Unit Compact Enclosure Cabinets" title="E-Mon MMU Multiple Meter Unit Compact Enclosure Cabinets"/></div>	
			<div id="PartsContent"><h3>MMU Multiple Meter Unit 
			<br/><font color="#50658D">Compact Enclosure Cabinets</font></h3>
			<p><b>Features</b>
			<br/><br/>&#149; Space-saving multiple meter units available in configurations containing up to 8, 16 or 24 meters.
			<br/><br/>&#149; MMU cabinets may contain any combination of E-Mon D-Mon class 1000, Class 2000 (kWh * kWh/Demand) and standard Green Class Meters.
			<br/><br/>&#149; Compact installation of multiple meters allows for easy and centralized reading.
			<br/><br/>&#149; IDRs (Interval Data Recorders) can be factory installed inside the MMU enclosures along with the meters allowing for easy interface to the E-Mon Energy software system. (IDRs are mounted on the back wall of the enclosure).
			<br/><br/>&#149; Three-phase MMU cabinets come with factory prewired voltage feeds. If IDR(s) are installed inside the MMU cabinet, the connections from the meters to the IDR are also prewired at the factory.
			<br/><br/>&#149; MMU cabinets may contain meters of different voltage configurations. (i.e. 208V & 480V meters inside a single MMU enclosure. ) Note that voltage from all sources being monitored will need to be fed to the MMU enclosure.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/mmu_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>MMU Specification Sheet</b></font></a></p>
			
</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monD-monOutdoorEnclosureOption.gif" alt="E-Mon Outdoor Enclosure Option" title="E-Mon Outdoor Enclosure Option"/></div>			
			<div id="PartsContent"><h3>Outdoor Enclosure Option</h3>
			<p><b>Features:</b>
			<br/><br/>&#149; Outdoor meter enclosures are compatible 
             		<br/>&nbsp;&nbsp;with Class 1000, 2000, 2100, 5000, Green 
             		<br/>&nbsp;&nbsp;and Green Class Net meters.
			<br/><br/>&#149; NEMA 4X rated outdoor enclosure made of 
             		<br/>&nbsp;&nbsp;hot compression molded fiberglass 
             		<br/>&nbsp;&nbsp;reinforced polyester.
			<br/><br/>&#149; 1 1/16" KO (3/4" conduit) on bottom of 
             		<br/>&nbsp;&nbsp;enclosure
			<br/><br/>&#149; Padlocking hasp.
			<br/><br/>&#149; Temperature Range: -40 degrees F to 
             		<br/>&nbsp;&nbsp;+250 degrees F (-40&deg;C to +120&deg;C)
			<br/><br/>&#149; Flamability Rating: UL94-5V
			<br/><br/>&#149; Self Extinguishing: Non-flame propogating
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/outdoor_encspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Outdoor Enclosure Specifications</b></font></a></p>
			
</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monWDCWirelessDataCollector.gif" alt="E-Mon WDC Wireless Data Collector" title="E-Mon WDC Wireless Data Collector"/></div>
			<div id="PartsContent"><h3>WDC 
			<br/><font color="#50658D">Wireless Data Collector</font></h3>
			<p>The wireless data collector (WDC) is a reliable, fully bi-directional gateway between the Internet/backbone network and the E-Mon wireless mesh network. It can support multiple meter types on the same network. The WDC provides simple remote configuration of the wireless devices and accurately transmits the energy data via Ethernet from the wireless network to E-Mon Energy automatic meter reading software for billing and analysis.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Complete wireless socket meter package 
             		<br/>&nbsp;&nbsp;compatible with E-Mon Energy wireless 
             		<br/>&nbsp;&nbsp;metering systems.
			<br/><br/><b>Meter Features</b>
			<br/><br/>&#149; Compiles data from wireless transceivers 
             		<br/>&nbsp;&nbsp;and transmits data to E-Mon Energy 
             		<br/>&nbsp;&nbsp;automatic meter reading software for 
             		<br/>&nbsp;&nbsp;billing and analysis.
			<br/><br/>&#149; Monitor up to 1,000 individual metering 
             		<br/>&nbsp;&nbsp;points.
			<br/><br/>&#149; Interfaces with E-Mon D-Mon Class 2100 & 
             		<br/>&nbsp;&nbsp;4100 meters with built-in wireless 
             		<br/>&nbsp;&nbsp;transceivers, E-Mon's wireless socket meter 
             		<br/>&nbsp;&nbsp;packages as well as EWM, GW1 & GW2
             		<br/>&nbsp;&nbsp;modules which are stand-alone 
             		<br/>&nbsp;&nbsp;transceivers connected to existing E-Mon 
             		<br/>&nbsp;&nbsp;D-Mon meters or other metering devices 
             		<br/>&nbsp;&nbsp;such as gas, water or other metering 
             		<br/>&nbsp;&nbsp;devices such as gas, water and utility 
             		<br/>&nbsp;&nbsp;electric meters.
			<br/><br/>&#149; Wireless mesh network operates in the 
             		<br/>&nbsp;&nbsp;915 MHz license-free band. No cellular 
             		<br/>&nbsp;&nbsp;wireless service contracts are required.
			<br/><br/>&#149; Wireless data collectors & meters with 
             		<br/>&nbsp;&nbsp;wireless transceivers can be mounted 
             		<br/>&nbsp;&nbsp;inside buildings within 500 feet 
             		<br/>&nbsp;&nbsp;line-of-sight from each other and up 
             		<br/>&nbsp;&nbsp;to 200 feet through walls, depending 
             		<br/>&nbsp;&nbsp;on wall material.
			<br/><br/>&#149; Fully self-configuring wireless mesh 
             		<br/>&nbsp;&nbsp;network allows for easy installation and 
             		<br/>&nbsp;&nbsp;configuration with no network management 
             		<br/>&nbsp;&nbsp;required.
			<br/><br/>&#149; Requires 120V power supply to power unit.
			<br/><br/>&#149; Remote access requires IP address.
			<br/><br/>&#149; NEMA 4X outdoor enclosure standard.
			<br/><br/>&#149; FCC certified not to interfere with existing 
             		<br/>&nbsp;&nbsp;infrastructure.
			<br/><br/>&#149; 128 bit encryption on wireless network.
			<br/><br/>&#149; Expandable local storage.
			<br/><br/>&#149; Communication logs and automatic data 
             		<br/>&nbsp;&nbsp;recovery features.
			<br/><br/>&#149; User and administration access control.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/wireless_wdcspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Data Collector (WDC) Specifications</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/wirelessmeshconfiguration.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Mesh Network System Configuration</b></font></a></p>
			
</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/socket-meter.gif" alt="E-Mon Wireless Socket Meter Package" title="E-Mon Wireless Socket Meter Package"/></div>
			<div id="PartsContent"><h3>Wireless Socket Meter Package</h3>
			<p>The E-Mon D-Mon wireless socket meter package allows users to easily integrate their elecric utility meter data to the E-Mon Energy wireless metering system. The meter includes a wireless module equipped with plug-and-play installation which self-configures the meter to interface with the E-Mon wireless mesh network system for automatic meter reading & energy analysis with E-Mon Energy software.
			<br/><br/><b>Features:</b>
			<br/>&#149; Complete wireless socket meter package compatible with E-Mon Energy wireless metering systems.
			<br/><br/><b>Meter Features</b>
			<br/><br/>&#149; Electronic LCD Display	
			<br/><br/>&#149; Demand Reset	
			<br/><br/>&#149; Test Mode Puch Button	
			<br/><br/>&#149; Meets ANSI,IEC, and FCC Standards	
			<br/><br/>&#149; Humidity 0% to 95% non-condensing	
			<br/><br/>&#149; Polycarbonate Cover	
			<br/><br/>&#149; Test LED	
			<br/><br/>&#149; Temperature: -40 to +85&deg;C	
			<br/><br/>&#149; ANSI C12.20 0.5 Accuracy Class	
			<br/><br/>&#149; Avaiable Configurations:
             		<br/>&nbsp;&nbsp;&nbsp;- Two or Three Wire
             		<br/>&nbsp;&nbsp;&nbsp;- 120 or 240 Volt
             		<br/>&nbsp;&nbsp;&nbsp;- Form 2S or 12S
             		<br/>&nbsp;&nbsp;&nbsp;- Class 20, 100, 200 or 300
			<br/><br/><b>Wireless Features</b>
			<br/><br/>&#149; Operates in 915 MHz license-free bands
			<br/><br/>&#149; Fully self-configuring wireless mesh 
             		<br/>&nbsp;&nbsp;network
			<br/><br/>&#149; Local non-volatile data storage
			<br/><br/>&#149; Provides real-time access to interval data
			<br/><br/>&#149; Full peer-to-peer communication
			<br/><br/>&#149; No network address management required
			<br/><br/>&#149; Automatic time synchronization of all 
             		<br/>&nbsp;&nbsp;meters int he network.
			<br/><br/>&#149; Installed "under the glass"
			<br/><br/>&#149; Automatic time sychronization
			<br/><br/>&#149; Internal and external antenna options
			<br/><br/>&#149; Provides full wireless routing capability
			<br/><br/>&#149; Multiple redundant paths
			<br/><br/>&#149; Automatic network acquisition at Power ON.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/wirelesssocketmeterspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Socket Meter Package</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/wirelessmeshconfiguration.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Mesh Network System Configuration</b></font></a>
			</p>
</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monEWMSingle-PointExternalWirelessModule.gif" alt="E-Mon EWM Single-Point External Wireless Module" title="E-Mon EWM Single-Point External Wireless Module"/></div>
			<div id="PartsContent"><h3>EWM  
			<br/><font color="#50658D">Single-Point External Wireless Module</font></h3>
			<p>The E-Mon D-Mon EWM external wireless module allows for easy upgrade of new or existing E-Mon D-Mon Class 1000 & 2000 meters to wireless mesh capability. The module's plug-and-play installation self-configures the meter to interface with the E-Mon wireless mesh network system for automatic meter reading & energy analysis with E-Mon Energy software.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Stand-alone wireless mesh network 
             		<br/>&nbsp;&nbsp;module for retrofit of existing E-Mon 
             		<br/>&nbsp;&nbsp;D-Mon Class 1000 (277V 2W meter 
             		<br/>&nbsp;&nbsp;excluded) & 2000 meters. Ideal for 
             		<br/>&nbsp;&nbsp;upgrading existing systems to an 
             		<br/>&nbsp;&nbsp;automatic meter reading system.
			<br/><br/>&#149; Module powered by E-Mon D-Mon meter.
			<br/><br/>&#149; Integrate with E-Mon wireless metering 
             		<br/>&nbsp;&nbsp;systems.
			<br/><br/>&#149; Plug-and-play installation. Self-configures 
             		<br/>&nbsp;&nbsp;to the wireless mesh network.
			<br/><br/>&#149; Allows creation of usage graphs, charts 
             		<br/>&nbsp;&nbsp;and usage statements via E-Mon Energy 
             		<br/>&nbsp;&nbsp;software.
			<br/><br/>&#149; Contains local storage for 2+ months of 
             		<br/>&nbsp;&nbsp;15-minute interval data.
			<br/><br/>&#149; Wireless transceiver is FCC certified not to 
             		<br/>&nbsp;&nbsp;interfere with existing infrastructure.
			<br/><br/>&#149; Wireless mesh network operates in the 
             		<br/>&nbsp;&nbsp;915 MHz frequency hopping 
             		<br/>&nbsp;&nbsp;spread-spectrum license-free band. No 
             		<br/>&nbsp;&nbsp;cellular wireless service contracts are 
             		<br/>&nbsp;&nbsp;required.
			<br/><br/>&#149; Wireless modules can be mounted inside 
             		<br/>&nbsp;&nbsp;buildings within approximately 500 feet 
             		<br/>&nbsp;&nbsp;line-of-sight from each other and up to 
             		<br/>&nbsp;&nbsp;200 feet through walls, depending on wall 
             		<br/>&nbsp;&nbsp;material.
			<br/><br/>&#149; Non-metallic enclosure for indoor 
             		<br/>&nbsp;&nbsp;installation.
			<br/><br/>&#149; Automatic time synchronization.
			<br/><br/>&#149; Automatic network acquisition at power ON.
			<br/><br/>&#149; Utilizes EKA Systems technology.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/wirelessewm_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>EWM External Wireless Module Specifications</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/wirelessmeshconfiguration.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Mesh Network System Configuration</b></font></a>
			</p>
</div></div>
<div id="20" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monGW1WirelessModuleforGasandWatermeters.gif" alt="E-Mon GW1 Wireless Module for Gas and Water meters" title="E-Mon GW1 Wireless Module for Gas and Water meters"/></div>		
			<div id="PartsContent"><h3>GW1
			<br/><font color="#50658D">Wireless Module for Gas and Water meters</font></h3>
			<p>The E-Mon D-Mon GW1 wireless module allows for easy upgrade of a pulse equipped gas or water meter to wireless. The module's plug-and-play installation self-configures the meter to interface with the E-Mon wireless mesh network system for automatic meter reading & energy analysis of multiple utilities with E-Mon Energy software.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Interface with water & gas meters 
             		<br/>&nbsp;&nbsp;equipped with pulse output 
             		<br/>&nbsp;&nbsp;(solid-state or read switch.)
			<br/><br/>&#149; Includes 120 VAC power supply module.
			<br/><br/>&#149; Compatible with E-Mon wireless metering 
             		<br/>&nbsp;&nbsp;system.
			<br/><br/>&#149; Plug-and-play installation. Self-configures 
             		<br/>&nbsp;&nbsp;to the wireless mesh network.
			<br/><br/>&#149; Allows creation of usage graphs, charts 
             		<br/>&nbsp;&nbsp;and usage statements via E-Mon Energy 
             		<br/>&nbsp;&nbsp;software.
			<br/><br/>&#149; Contains local storage for 2+ months of 
             		<br/>&nbsp;&nbsp;15-minute interval data.
			<br/><br/>&#149; Wireless transceiver is FCC certified not to 
             		<br/>&nbsp;&nbsp;interfere with existing infrastructure.
			<br/><br/>&#149; Wireless mesh network operates in the 
             		<br/>&nbsp;&nbsp;915 MHz frequency hopping 
             		<br/>&nbsp;&nbsp;spread-spectrum license-free band. No 
             		<br/>&nbsp;&nbsp;cellular wireless service contracts are 
             		<br/>&nbsp;&nbsp;required.
			<br/><br/>&#149; Wireless modules can be mounted inside 
             		<br/>&nbsp;&nbsp;buildings within approximately 500 feet 
             		<br/>&nbsp;&nbsp;line-of-sight from each other and up to 
             		<br/>&nbsp;&nbsp;200 feet through walls, depending on wall 
             		<br/>&nbsp;&nbsp;material.
			<br/><br/>&#149; Non-metallic enclosure for indoor 
             		<br/>&nbsp;&nbsp;installation.
			<br/><br/>&#149; Automatic time synchronization.
			<br/><br/>&#149; Automatic network acquisition at power ON.
			<br/><br/>&#149; Utilizes EKA Systems technology.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/wirelessgw1_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>GW1 Wireless Module for Gas & Water Meters</b></font></a>
			<br/><a href="http://www.emon.com/techspecs/wirelessmeshconfiguration.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Wireless Mesh Network System Configuration</b></font></a>
			</p>
</div></div>
<div id="21" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/GW2-Dual-PointWirelessModuleforGasandWatermeters.gif" alt="E-Mon GW2 Dual-Point Wireless Module for Gas & Water meters" title="E-Mon GW2 Dual-Point Wireless Module for Gas & Water meters"/></div>		
			<div id="PartsContent"><h3>GW2 
			<br/><font color="#50658D">Dual-Point Wireless Module for Gas & Water meters</font></h3>
			<p>The E-Mon D-Mon GW2 wireless module allows for easy upgrade of two pulse equipped gas or water meters to wireless. The module's plug-and-play installation self-configures the meters to interface with the E-Mon wireless mesh network system for automatic meter reading & energy analysis of multiple utilities with E-Mon Energy software.
			<br/><br/><b>Features:</b>
			<br/><br/>&#149; Interface with pulse output (solid-state or 
             		<br/>&nbsp;&nbsp;read switch) devices including water, gas, 
             		<br/>&nbsp;&nbsp;steam, BTU and electric meters and 
             		<br/>&nbsp;&nbsp;humidity and temperature sensors.
			<br/><br/>&#149; Interface with two external devices.
			<br/><br/>&#149; Includes 120 VAC power supply module.
			<br/><br/>&#149; Integrate with E-Mon wireless metering 
             		<br/>&nbsp;&nbsp;systems.
			<br/><br/>&#149; Plug-and-play installation. Self-configures 
             		<br/>&nbsp;&nbsp;to the wireless mesh network.
			<br/><br/>&#149; Allows creation of usage graphs, charts 
             		<br/>&nbsp;&nbsp;and usage statements via E-Mon Energy 
             		<br/>&nbsp;&nbsp;software.
			<br/><br/>&#149; Contains local storage for 2+ months of 
             		<br/>&nbsp;&nbsp;15-minute interval data.
			<br/><br/>&#149; Wireless transceiver is FCC certified not to 
             		<br/>&nbsp;&nbsp;interfere with existing infrastructure.
			<br/><br/>&#149; Wireless mesh network operates in the 
             		<br/>&nbsp;&nbsp;915 MHz frequency hopping 
             		<br/>&nbsp;&nbsp;spread-spectrum license-free band. No 
             		<br/>&nbsp;&nbsp;cellular wireless service contracts are 
             		<br/>&nbsp;&nbsp;required.
			<br/><br/>&#149; Wireless modules can be mounted inside 
             		<br/>&nbsp;&nbsp;buildings within approximately 500 feet 
             		<br/>&nbsp;&nbsp;line-of-sight from each other and up to 
             		<br/>&nbsp;&nbsp;200 feet through walls, depending on 
             		<br/>&nbsp;&nbsp;wall material.
			<br/><br/>&#149; Devices can be mounted up to 200 feet 
             		<br/>&nbsp;&nbsp;away from wireless module.
			<br/><br/>&#149; Non-metallic enclosure for indoor 
             		<br/>&nbsp;&nbsp;installation.
			<br/><br/>&#149; Automatic time synchronization.
			<br/><br/>&#149; Automatic network acquisition at power ON.
			<br/><br/>&#149; Utilizes EKA Systems technology.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/wirelessgw2_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>GW2-Dual-Point Wireless Module for Gas & Water Meters</b></font></a></p>
			
</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/ECAtointerfaceE-monEnergywithutilitymeters.gif" alt="E-Mon ECA to interface E-mon Energy with utility meters" title="E-Mon ECA to interface E-mon Energy with utility meters"/></div>		
			<div id="PartsContent"><h3>ECA to interface
			<br/><font color="#50658D">E-mon Energy with utility meters</font></h3>
			<p><b>Features:</b>
			<br/><br/>The E-Mon D-Mon ECA-2 External Contact Adapter allows the user to monitor the utility electric meter with E-Mon Energy AMR software. A pulse contact provided by the utility meter sends energy information via the ECA-2 to the IDR interval data recorder. E-Mon Energy software reads the kilowatt hour and demand data from the IDR for the purpose of energy conservation, demand side management programs, utility rebate incentives, cost analysis or monthly comparisons of usage. The ECA-2 can also be used to integrate water, gas and BTU meters equipped with contact (pulse) output. Note: ECA-2 is not required when using IDR-ST units.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/eca_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>ECA Specifications</b></font></a>
			</p>
</div></div>
<div id="23" class="popup_block_Parts">		
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/E-monTemperatureSensor.gif" alt="E-Mon Temperature Sensor" title="E-Mon Temperature Sensor"/></div>	
			<div id="PartsContent"><h3>Temperature Sensor</font></h3>
			<p><b>Features:</b>
			<br/><br/>The E-Mon D-Mon Temperature Sensor works with the IDR interval data recorder to provide temperature data as a frequency and pulses are stored in 5- or 15-minute intervals. This data can be utilized to determine the average temperage during these intervals.
			<br/><br/>The temperature Sensor can be used to determine the average outdoor temperature when it is necessary to accurately compare present energy usage against historical data without having to rely on regional data supplied by the weather service, which may not correlate with your specific location. It is also useful when it is important to monitor the inside temperature of an electrical room for maintenance or observation.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/manuals/tempmodule_manual.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Temperature Sensor Specifications</b></font></a>
			</p>
</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/PCAinterfaceE-monEnergytoelectromechanicalmeters.gif" alt="E-Mon PCA Interface Energy to Electromechanical Meters" title="E-Mon PCA Interface Energy to Electromechanical Meters"/></div>			
			<div id="PartsContent"><h3>PCA Interface
			<br/><font color="#50658D">Energy to Electromechanical Meters</font></h3>
			<p><b>Features:</b>
			<br/><br/>The E-Mon D-Mon PCA (Pulse Contact Adapter) allows standard electro-mechanical style kilowatt-hour meters to interface to E-Mon Energy software.
			<br/><br/>The PCA installs on the X1 (times one) dial of standard electro-mechanical style kilowatt hour meters to provide 10 pulses per revolution of the dial.
			<br/><br/>Models of the PCA are also available for some types of gas meters to allow integration with E-Mon Energy.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/pcaspec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>PCA Specifications</b></font></a></p>
</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Emon/250SeriesGasMeter.gif" alt="E-Mon 250 Series Gas Meter with Pulse Output" title="E-Mon 250 Series Gas Meter with Pulse Output"/></div>
			<div id="PartsContent"><h3>250 Series 
			<br/><font color="#50658D">Gas Meter with Pulse Output</font></h3>
			<p>&#149; 4-chamber diaphragm type gas meter.
			<br/>&#149; Ideal for light commercial or larger 
            		<br/>&nbsp;&nbsp;residential use.
			<br/>&#149; Equipped with pulse output for interfacing 
            		<br/>&nbsp;&nbsp;with interval data recorders.
			<br/>&#149; For use with loads ranging from 
            		<br/>&nbsp;&nbsp;200-250CuFt/Hr.
			<br/>&#149; Operating pressure 5 psi.
			<br/>&#149; Available for pipe sizes of 3/4" and 1".
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.emon.com/techspecs/gas250_spec.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>250 Series Gas Meter Specification Sheet</b></font></a></p>
			
</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/250SeriesGasMeter_Thumbnail.gif" border="0"  alt="E-Mon 250 Series Gas Meter with Pulse Output" title="E-Mon 250 Series Gas Meter with Pulse Output"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Series400GasMeter_Thumbnail.gif" border="0" alt="E-Mon Series 400 Gas Meter" title="E-Mon Series 400 Gas Meter"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Series200CFGMgasMeter_Thumbnail.gif" border="0"  alt="E-Mon Series 200 CFG Gas Meter" title="E-Mon Series 200 CFG Gas Meter"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/gasfmmeter_Thumbnail.gif" border="0" alt="E-Mon FM Series Gas Meter with Pulse Output" title="E-Mon FM Series Gas Meter with Pulse Output"/></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Class1000Single-PhasekWhSubmeter_Thumbnail.gif" border="0" alt="E-Mon Class 1000 Single-Phase kWh Submeter" title="E-Mon Class 1000 Single-Phase kWh Submeter"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Class2000Three-PhasekWh_kW(Demand)Submeter_Thumbnail.gif" border="0" alt="E-Mon Class 2000 Three-Phase kWh/kW (Demand) Submeter" title="E-Mon Class 2000 Three-Phase kWh/kW (Demand) Submeter"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Class3000AdvancedSubmeterwithCommunications_Thumbnail.gif" border="0" alt="E-Mon Class 3000 Advanced Submeter with Communications" title="E-Mon Class 3000 Advanced Submeter with Communications"/></font></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Class4000Multi-FamilySingle-PhasekWhSubmeter_Thumbnail.gif" border="0" alt="E-Mon Class 4000 Multi-Family Single-Phase kWh Submeter" title="E-Mon Class 4000 Multi-Family Single-Phase kWh Submeter"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/Class5000AdvancedSubmeterwithCommunications_Thumbnail.gif" border="0" alt="E-Mon Class 5000 Advanced Submeter with Communications" title="E-Mon Class 5000 Advanced Submeter with Communications"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/GreenClassMeterwithCO2andCarbonFootprintData_Thumbnail.gif" border="0" alt="E-Mon Green Class Meter with CO2 & Carbon Footprint Data" title="E-Mon Green Class Meter with CO2 & Carbon Footprint Data"/></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/GreenClassMeterwithNetMeteringCapabilities_Thumbnail.gif" border="0" alt="E-Mon Green Class Meter with Net Metering Capabilities" title="E-Mon Green Class Meter with Net Metering Capabilities"/></font></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monD-monSplit-CoreCurrentSensors_Thumbnail.gif" border="0" alt="E-Mon Split-Core Current Sensors" title="E-Mon Split-Core Current Sensors"/></font></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/P3Pulser-PluseOutputforInterfacetoEMS_BMS_Thumbnails.gif" border="0" alt="E-Mon P3 Pulser Pulse Output for Interface to EMS/BMS" title="E-Mon P3 Pulser Pulse Output for Interface to EMS/BMS" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/D_A(DigitaltoAnalog)ConverterModule_Thumbnail.gif" border="0"  alt="E-Mon D/A (Digital to Analog) Converter Module" title="E-Mon D/A (Digital to Analog) Converter Module"/></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/MMUMultipeMeterUnitCompactEnclosureCabinets_Thumbnail.gif" border="0" alt="E-Mon MMU Multiple Meter Unit Compact Enclosure Cabinets" title="E-Mon MMU Multiple Meter Unit Compact Enclosure Cabinets"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monD-monOutdoorEnclosureOption_Thumbnail.gif" border="0" alt="E-Mon Outdoor Enclosure Option" title="E-Mon Outdoor Enclosure Option"/></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monWDCWirelessDataCollector_Thumbnail.gif" border="0" alt="E-Mon WDC Wireless Data Collector" title="E-Mon WDC Wireless Data Collector"/></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/socket-meter_Thumbnail.gif" border="0" alt="E-Mon Wireless Socket Meter Package" title="E-Mon Wireless Socket Meter Package"/></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monEWMSingle-PointExternalWirelessModule_Thumbnail.gif" border="0" alt="E-Mon EWM Single-Point External Wireless Module" title="E-Mon EWM Single-Point External Wireless Module"/></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monGW1WirelessModuleforGasandWatermeters_Thumbnail.gif" border="0" alt="E-Mon GW1 Wireless Module for Gas and Water meters" title="E-Mon GW1 Wireless Module for Gas and Water meters"/></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/GW2-Dual-PointWirelessModuleforGasandWatermeters_Thumbnail.gif" border="0" alt="E-Mon GW2 Dual-Point Wireless Module for Gas and Water meters" title="E-Mon GW2 Dual-Point Wireless Module for Gas and Water meters"/></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/ECAtointerfaceE-monEnergywithutilitymeters_Thumbnail.gif" border="0" alt="E-Mon ECA to interface E-mon Energy with utility meters" title="E-Mon ECA to interface E-mon Energy with utility meters"/></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/E-monTemperatureSensor_Thumbnail.gif" border="0" alt="E-Mon Temperature Sensor" title="E-mon Temperature Sensor"/></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Emon/thumbnails/PCAinterfaceE-monEnergytoelectromechanicalmeters_Thumbnail.gif" border="0" alt="E-Mon PCA Interface Energy to Electromechanical Meters" title="E-Mon PCA Interface Energy to Electromechanical Meters"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>