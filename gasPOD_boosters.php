<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta name="description" content="ETTER Engineering's skid-mounted packaged gas booster systems arrive at your job site pre-piped, wired and tested - No more TECHNICAL RISK!" /><meta name="keywords" content="ENGB, ETTER Engineering, hermetic design,custom design,gas booster,gas boosters,gas pressure booster,gas pressure boosters,hermetic gas booster,hermetic gas boosters, packaged gas booster systems, skid-mounted packaged booster systems, heat exchanger packages" /><title>Natural Gas Boosters - Packaged Gas Booster Systems - Hermetic Gas Boosters</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
<script type="text/javascript" src="includes/Gas_Booster_Approvals.js"> </script>
<script type="text/javascript" src="includes/Gas_Booster_Features.js"> </script>
<script type="text/javascript" src="includes/PlugAndPlay.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript" src="includes/RolloverButtons.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color=#494A4A><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script>
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Wrapper">
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="gasPODPhoto"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="RedBrowseBar"></div>
<div id="ENGBBoosterTransBLK"></div>
<div id="ENGBSlideshow">
<div class="IndexSlideshow" >
    <img src="IMG_3807.gif" width="399" height="433" alt="packaged gas booster"/> 
    <img src="Packaged_Gas_Booster.gif" width="399" height="433" alt="packaged gas boosters"/> 
    <img src="Trade_Show_Gas_Booster_FINAL.jpg" width="399" height="433" alt="ETTER Engineering Gas Booster"/> 
    <img src="Gas_booster_duplex.gif" width="399" height="433" alt="gas booster"/> 	
    <img src="Simplex_Gas_Booster_Photo.gif" width="399" height="433" alt="Simplex gas booster"/>
</div> 
</div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="ENGBBoosterSolidWhiteBkgrd"></div>
<div id="ENGBBoosterDropRightInside"></div>
<div id="ENGBBoosterDropRightBlkTrans"></div>
<div id="PlugAndPlayLogo"><a href="#?w=170" rel="popup_name" class="plugandplay"><img src="Plug_and_Play_Logo.gif" border="0" width="110" height="34" alt="Plug and Play"/></a></div>

<div id="gasPODLogo"></div>
<div id="gasPODText"><font color="#FFFFFF">The gasPOD&#0153; is our skid-mounted, 
packaged hermetic gas booster system. It arrives at your job site pre-piped, wired and
tested - no more technical risk!
<br/><br/>On-site installation is quick and easy - simply mount the skid in place, connect electrical
power to the control panel, connect the system's supply and discharge piping to the field 
piping, then turn the switch.
<br/><br/>As options, ETTER has various integrated control philosophies to suit your application 
needs, along with features such as Gas Leak Detection, outdoor steel transclosures, and heat 
exchanger packages. Your system can be customized to meet your needs.</font></div>
<div id="ENGBBoosterTextRightSide"><font color="#445679" size= "4"><b>ENGB Features</b></font>
<br/><br/><br/><br/><font color="#4E4848" size= "1">Being limited to four or six outlet positions is a thing<br/> of the past.</font>
<br/><br/><br/><font color="#4E4848" size= "1">Wrong voltage? No big deal - you can change it in the field.</font>
<br/><br/><br/><font color="#4E4848" size= "1">CFT means higher turndown capabilities and a longer, more reliable motor life.</font>
<br/><br/><br/><font color="#4E4848" size= "1">Any required maintenance is now a quick, easy one-man job.</font></div>
<a href="pdfs/ETTER-Engineering-gasPOD-gas-booster.pdf" target="_blank" id="gasPODPDFButton"></a>
<a href="pdfs/Newsday-Article.pdf" target="_blank" id="gasPODNewsArticleButton"></a>
<div id="ENGBButton"><a href="#?w=425" rel="popup_name" class="poplightOne"><font color="#000000" size="2">Custom Outlet Positioning</font></a></div>
<div id="ENGBButton2"><a href="#?w=425" rel="popup_name" class="poplightTwo"><font color="#000000" size="2">External Motor Wiring</font></a></div>
<div id="ENGBButton3"><a href="#?w=425" rel="popup_name" class="poplightThree"><font color="#000000" size="2">Cool Flow Technology&#8482</font></a></div>
<div id="ENGBButton4"><a href="#?w=425" rel="popup_name" class="poplightFour"><font color="#000000" size="2">Sliding Service Sled</font></a></div>
<div id="container"><a href="#?w=380" rel="popup_name" class="approvals"><input type="image" name="submit2" id="submit2" src="viewapprovalsbuttonOriginal.gif" border="0" width="206" height="15" alt="Gas Booster Approvals"></a></div>
	<div id="popup_name" class="popup_block_approvals">
    
			<a href="http://www.coned.com/es/specs/gas/Section%20VII.pdf" target="_blank" id="ConED"><img border="0" src="con_edison_logo.png" alt="Gas Booster Approvals"/></a>
			<a href="pdfs/2009-natl-grid-blue-book-web.pdf" target="_blank" id="NationalGrid"><img border="0" src="national_grid_logo.png"  alt="Gas Booster Approvals"/></a>
			<a href="http://database.ul.com/cgi-bin/XYV/template/LISEXT/1FRAME/showpage.html?name=JIFQ.MH46473&ccnshorttitle=Gas+Boosters&objid=1079858133&cfgid=1073741824&version=versionless&parent_id=1073988683&sequence=1" target="_blank" id="ULLogo"><img border="0" src="ul_logo_a.png" alt="Gas Booster Approvals"/></a>
			<a href="http://license.reg.state.ma.us/pubLic/pl_products/pb_search.asp?type=C&manufacturer=Etter+Engineering+Company+Inc.&model=&product=&description=&psize=50" target="_blank" id="MassGov"><img border="0" src="massgovlogo.png" alt="Gas Booster Approvals"/></a>
			<div id="ApprovalsTxtpopup">With the exception of the UL-listing, the above approval agencies are region-specific, should your local agencies require any further documentation other than our UL-listing, please contact ETTER Engineering Toll Free 1-800-444-1962 for further assistance.</div>
		</div></div>

<div id="AlternateBoosterNav">
<a href="gasPOD_boosters.php" id="gasPODTxtBTN"><font color="#808080"><b>gasPOD</b></font></a>
<a href="engb_boosters.php" id="ENGBTxtBTN"><font color="#808080"><b>ENGB</b></font></a>
<a href="E101P_boosters.php" id="E101PTxtBTN"><font color="#808080"><b>E101-P</b></font></a>
<a href="E101PHC_boosters.php" id="E101PHCTxtBTN"><font color="#808080"><b>E101-PHC</b></font></a>
<a href="E101PHCXtra_boosters.php" id="E101PHCXtraTxtBTN"><font color="#808080"><b>E101-PHC-Xtra</b></font></a>
<a href="booster_accessories_duplex.php" id="AccessoriesTxtBTN"><font color="#808080"><b>Accessories</b></font></a></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</a></div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;">
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>