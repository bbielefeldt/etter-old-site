<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Shinko American combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Shinko American,Shinko,Shinko 1/32 DIN Digital Temperature Controller FCL Series,Shinko Digital Temperature Controller with Ramp Soak JCL Series,Digital Indicating Controller ACS-13A Series,Limit Controller 1/16 DIN JCS Series,Digital Indicating Controller 1/8 DIN ACR Series,Shinko Digital Indicating Temperature Controllers 1/8 DIN JCR Series,Digital Indicating Controller 1/4 DIN ACD Series,Limit Controller 1/4 DIN JCD Series,DIN Rail Indicating Controller DCL 33A Series,Temperature Control Unit NCL 13A,1/8 DIN Digital Indicator JIR-301-M,Dual Input Controller 1/16 DIN WCS Series,DIN Rail Limit Controller DCL-33A Series,Digital Indicating Controller 1/16 DIN JCS Series,Digital Indicating Controller 1/4 DIN JCD Series,HandHeld Thermometer/Hygrometer DFT 700,Infrared Thermometer with White LED Light R/IRLL02 Series,Infrared Thermometer Thermocouple (Thermo-Twin) RCT103F Series,Temperature Calibrator T5014 Series,Single Phase Power Controller FPT Series,Shinko Digital Program Controller 1/8 DIN OMR 100,Shinko High Performance Programmable Controller Multifunctional PC 900,PCD-300 Programmable Controller,Hybrid Recorder RM18L,Strip Chart Recorder 100 mm HR 700,1/8 and 1/4 DIN Digital Indicating Controller ACR and ACD Series,Programmable Signal Conditioner With Dual Display SA Series,Signal Conditioner SB Series,Soild State Relay SA-300-Z Series" />

<title>ETTER Engineering - Shinko American Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="ShinkoLogoLarge"></div>
<div id="SensusText">Since 1945, Shinko Technos has been manufacturing quality products used to support various industries from manufacturers of plastics & rubber, food & packaging, textiles, ceramics, as well as industries which provide essential services such as medical, environmental and energy industries.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/1_32-DIN-Digital-Temperature-Controller-Shinko-FCL-Series.gif"  alt="Shinko 1/32 DIN Digital Temperature Controller FCL Series" title="Shinko 1/32 DIN Digital Temperature Controller FCL Series"/></div>
			<div id="PartsContent"><h3>1/32 DIN Digital Temperature Controller 
			<br/><font color="#445678">Shinko FCL Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>NEMA 4X protective construction. 
			<br/>Black enclosure color.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output.
			<br/><br/><b>Multi-Input</b>
			<br/>Units feature multi-input capabilities:
			<br/>5 thermocouple types and 1 RTD type.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your
			process at anytime.
			<br/><br/><b>Superior Security Options</b>
			<br/>This feature eliminates potential operator error.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL and CE Safety Approvals.
			<br/><br/><b>Warranty</b>
			<br/> All units manufactured to strict ISO standards and offer full 3 year manufacturers warranty.
			<br/><br/><b>Low Cost</b>
			<br/>Most advanced price/performance package available.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature bright display of either PV or SV, red 4 digits.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.usashinko.com/sites/default/files/FCL.pdf" target="_blank"><font color="#ACB0C3"><b>FCL Series Brochure</b></font></a>
			<br/><a href="http://usashinko.com/sites/default/files/FCL Series Instruction Manual.pdf" target="_blank"><font color="#ACB0C3"><b>Instruction Manual</b></font></a>
			<br/><a href="http://usashinko.com/sites/default/files/FCL Series Communication Instruction Manual.pdf" target="_blank"><font color="#ACB0C3"><b>Communication Instruction Manual</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Temperature-Controller-with-Ramp_Soak---Shinko-JCL-Series.gif" alt="Shinko Digital Temperature Controller with Ramp Soak JCL Series" title="Shinko Digital Temperature Controller with Ramp Soak JCL Series"/></div>
			<div id="PartsContent"><h3>Digital Temperature Controller with Ramp Soak 
			<br/><font color="#445678">Shinko JCL Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Ramp/Soak Function</b>
			<br/>Up to 9 ramp/soak segments.
			<br/><br/><b>Dual Use</b>
      			<br/>This instrument is easily switched between controller or transmitter by simple key operation.
			<br/><br/><b>Structure</b>
      			<br/>IP-66 protective construction.
      			<br/>Black enclosure color.
			<br/><br/><b>Programmable Alarms</b>
     			<br/>Units feature standard single alarm output.
			<br/><br/><b>Multi-Input</b>
      			<br/>Units feature multi-input capabilities:
      			<br/>10 thermocouple types, 1 RTD type, 2 current inputs,
      			<br/>and 4 voltage inputs.
			<br/><br/><b>Digital Input</b>
      			<br/>Change between setpoints (SV1, SV2).
			<br/><br/><b>Safety Approvals</b>
      			<br/>UL, CUL and CE Safety Approvals.
			<br/><br/><b>Warranty</b>
      			<br/>All units manufactured to strict ISO standards and offer full 3 year manufacturers warranty.
			<br/><br/><b>Low Cost</b>
      			<br/>Most advanced price/performance package available.
			<br/><br/><b>PID Autotune</b>
      			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>Large LED Display</b>
      			<br/>All units feature bright display of either PV or SV, red 4 digits.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://usashinko.com/sites/default/files/JCL RampSoak Series Manual.pdf" target="_blank"><font color="#ACB0C3"><b>JCL Series Instruction Manual</b></font></a>
			<br/><a href="http://usashinko.com/sites/default/files/JCL Series Communication Instruction Manual.pdf" target="_blank"><font color="#ACB0C3"><b>JCL Series Communication Instruction Manual </b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Controller---Shinko-ACS--13A-Series.gif" alt="Shinko Digital Indicating Controller ACS-13A Series" title="Shinko Digital Indicating Controller ACS-13A Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Controller 
			<br/><font color="#445678">Shinko ACS-13A Series</font></h3>
			<p>&#149; Compact, Space saving
			<br/>&#149; 11-segment LCD display
			<br/>&#149; An easier viewing display PV color 
			<br/>&nbsp;&nbsp;switchable
			<br/>&#149; Standard console connector equipped
			<br/>&#149; Standard Output rate of change limit
			<br/>&#149; Multi-input &deg;C
			<br/>&#149; High-speed sampling
			<br/>&#149; Multiple options
			<br/>&nbsp;&nbsp;Set value memory external selection
			<br/>&nbsp;&nbsp;Serial communication
			<br/>&nbsp;&nbsp;Heater burnout alarm output
			<br/>&nbsp;&nbsp;Heating/Cooling control output
			<br/><br/><b>Standard Features:</b>
			<br/>&#149; Compact, Space saving
			<br/>&#149; 1/16 DIN 48 x 48 x 62mm (WxHxD)
			<br/><br/>The shortest depth of panel interior 56mm will contribute to space saving in the control panel area.
			<br/>When gasket (Dust-proof, Drip-proof, IP66) is used, the depth will be 54.5mm
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://usashinko.com/sites/default/files/ACS-13A Series Instruction Manual.pdf" target="_blank"><font color="#ACB0C3"><b>ACS Series Instruction Manual</b></font></a>
			<br/><a href="http://usashinko.com/sites/default/files/ACS-13A Series Communication Manual.pdf" target="_blank"><font color="#ACB0C3"><b>ACS Series Communication Manual</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Limit-Controller-1_16-DIN---Shinko-JCS-Series.gif" alt="Shinko Limit Controller 1/16 DIN JCS Series" title="Shinko Limit Controller 1/16 DIN JCS Series"/></div>
			<div id="PartsContent"><h3>Limit Controller 1/16 DIN 
			<br/><font color="#445678">Shinko JCS Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/16 & 1/4 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output with optional second alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities: 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Limit Control Function Specifications</b>
			<br/>1 setpoint, high- or low-limit control type, and latching limit action.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display, PV red 4 digits, SV green 4 digits.
			<br/><br/><b>Modbus Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL. CUL and CE Approvals</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Controller-1_8-DIN-Shinko-ACR-Series.gif" alt="Shinko Digital Indicating Controller 1/8 DIN ACR Series" title="Shinko Digital Indicating Controller 1/8 DIN ACR Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Controller 1/8 DIN 
			<br/><font color="#445678">Shinko ACR Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/8 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard TWO alarm output.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities: 10 thermocouple 
			<br/><br/><b>Ramp & Soak function is provided as standard</b>
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>MODBUS Protocol With RS485 or RS232C(Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL and CE Safety Approvals.
			<br/><br/><b>Warranty</b>
			<br/>All units manufactured to strict ISO standards and offer full 3 year manufacturers warranty.
			<br/><br/><b>Options</b>
			<br/>&#149; Transmission output 
			<br/>&nbsp;&nbsp;(4 to 20mA or 0 to 1V)
			<br/>&#149; Event output
			<br/>&#149; Event input
			<br/>&#149; Remote setpoint 
			<br/>&nbsp;&nbsp;(0 to 20mA, 4 to 20mA, 0 to 1V or 0 to 5V)
			<br/>&#149; Communications (RS485 or RS 232C)
			<br/>&#149; Insulated power output (24V. DC)
			<br/>&#149; Cooling & Heating Control
			<br/>&#149; Heater burnout alarm
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Temperature-Controllers-1_8-DIN---Shinko-JCR-Series.gif" alt="Shinko Digital Indicating Temperature Controllers 1/8 DIN JCR Series" title="Shinko Digital Indicating Temperature Controllers 1/8 DIN JCR Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Temperature Controllers 1/8 DIN
			<br/><font color="#445678">Shinko JCR Series</font></h3>
			<p>Digital Indicating Temperature Controllers 1/8 DIN Shinko JCR Series
			<br/><br/><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/8 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output with optional second alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature true multi-input capabilities:</b> 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>MODBUS Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL, CE and CSA Safety Approvals.
			<br/><br/><b>Warranty</b>
			<br/>All units manufactured to strict ISO standards and offer full 3 year manufacturers warranty.</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Controller-1_4-DIN---Shinko-ACD-Series.gif" alt="Shinko Digital Indicating Controller 1/4 DIN ACD Series" title="Shinko Digital Indicating Controller 1/4 DIN ACD Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Controller 1/4 DIN 
			<br/><font color="#445678">Shinko ACD Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/4 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard TWO alarm output.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities:</b> 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Ramp & Soak function is provided as standard</b>
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>MODBUS Protocol With RS485 or RS232C(Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL and CE Safety Approvals. </p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Limit-Controller-1_4-DIN---Shinko-JCD-Series.gif" alt="Shinko Limit Controller 1/4 DIN JCD Series" title="Shinko Limit Controller 1/4 DIN JCD Series"/></div>
			<div id="PartsContent"><h3>Limit Controller 1/4 DIN 
			<br/><font color="#445678">Shinko JCD Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/16 & 1/4 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output with optional second alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities: 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Limit Control Function Specifications</b>
			<br/>1 setpoint, high- or low-limit control type, and latching limit action.
			<br/><br/><b>Retransmission Output (optional) JCD Only</b>
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display, PV red 4 digits, SV green 4 digits.
			<br/><br/><b>Modbus Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL. CUL and CE Approvals</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/DIN-Rail-Indicating-Controller---Shinko-DCL---33A-Series.gif" alt="Shinko DIN Rail Indicating Controller DCL 33A Series" title="Shinko DIN Rail Indicating Controller DCL 33A Series"/></div>
			<div id="PartsContent"><h3>DIN Rail Indicating Controller 
			<br/><font color="#445678">Shinko DCL 33A Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Space Saving Size</b>
			<br/>Easy to mount even in a small machine because the width is just 22.5mm, height 7.5mm and depth 100 mm.
			<br/><br/><b>Dual Use</b>
			<br/>This instrument is easily switched to controller or transmitter by key operation. When using as a transmitter, the signal or thermocouple input, RTD 
			<br/>input or DC input is converted and output to 4 to 20mA DC. The scale is changed easily, too.
			<br/>(This function is limited only A/&#149;p type)
			<br/><br/><b>Expandable</b>
			<br/>It is possible to measure and control ranging from 1 point to a maximum of 31 points using serial communication (RS-485). It is very easy to connect between plural DCl-33A's, between communication converter and DCL-33A using communication cable CPP and CPM (sold separately).
			<br/><br/><b>Easy Mounting And Detaching</b>
			<br/>Hook this instrument on the DIN rail and mount it. Maintenance is very easy. As it is fixed to the DIN rail, it is vibration proof. When detaching it pull down the hook at the lower part of the main body using flat bladed screwdriver then pull up this instrument.
			<br/><br/><b>Large LED Display</b>
			<br/>PV Red 4 digits 7.5 x 4.1mm (H x W)
			<br/>SV Green 4 digits 7.5 x 4.1 mm (H x W)
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL and CE Safety Approvals.
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature full multi-input capability:</b>
			<br/>1 RTD type, 10 thermocouple types, mA, and V inputs
			<br/><br/><b>PID Autotune</b>
			<br/>Units feature full function third generation PID Autotune.</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Temperature-Control-Unit-Shinko-NCL---13A.gif" alt="Shinko Temperature Control Unit NCL 13A" title="Shinko Temperature Control Unit NCL 13A"/></div>
			<div id="PartsContent"><h3>Temperature Control Unit 
			<br/><font color="#445678">Shinko NCL 13A</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Small and Space Saving</b>
			<br/>Width: 17.5mm, Height: 75mm, Depth: 85mm (excluding the plug)
			<br/>Its compactness ensures easy mounting when only limited space permits.
			<br/><br/><b>Standard 4 Alarm Points</b>
			<br/>Four points of alarms are standard features. However, for Alarm 2, Alarm 3 and Alarm 4, the output and Energized/Deenergized functions are not available.
			<br/><br/><b>Multi-Input</b>
			<br/>18 type inputs such as thermocouple (10 types), RTD (2 types), DC current (2 types), DC voltage (4 types) are available. Fast 0.25 second input sampling period allows applications for various processes.
			<br/><br/><b>Easy Mounting, Removal and Maintenance</b>
			<br/>Hook the NCL - 13A into the DIN rail, then fit the unit to the DIN rail. Since the unit is fixed tightly to the DIN rail, it is resistant to vibration, whereby ensuring ease of maintenance.
			<br/><br/><b>Multi-Drop Communication</b>
			<br/>Various operations and settings can be carried out from PC's and programmable interfaces the communication function (up to 31 units of the NCL - 33A can be connected.). For communication protocol, Shinko protocol and Modbus protocol are available. (For Modbus protocol, RTU mode and ASCII mode can be selected by the DIP switch.). Connection to the open network Modbus is possible.
			<br/><br/><b>Wiring Is Not Necessary</b>
			<br/>Using a screw type plug, the power and communication lines can be connected. Bus plugs enable multiple connection of the NCL - 13A units. The spring type plug for input and output can also be connected or removed.
			<br/><br/><b>Safety Stnadard</b>
			<br/>UL/CSA, CE marking: Approval pending.
			<br/>The NCL - 13A Temperature Control Unit features a full 3 year warranty and lifetime technical support! </p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/1_8-DIN-Digital-Indicator---Shinko-JIR---301-M.gif" alt="Shinko 1/8 DIN Digital Indicator JIR-301-M" title="Shinko 1/8 DIN Digital Indicator JIR-301-M"/></div>
			<div id="PartsContent"><h3>1/8 DIN Digital Indicator
			<br/><font color="#445678">Shinko JIR-301-M</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/8 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard three alarm output.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities: 10 thermocouple types,
			<br/>2 RTD types, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Retransmission</b>
			<br/>Unit features standard 4-20mA process variable retransmission.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, Alarm SV green 4 digits.
			<br/><br/><b>MODBUS Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities featuring MODBUS RTU or ASCII.
			<br/><br/><b>Approvals</b>
			<br/>UL, ULc, and CE Approvals. 
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Dual-Input-Controller-1_16-DIN---Shinko-WCS-Series.gif" alt="Shinko Dual Input Controller 1/16 DIN WCS Series" title="Shinko Dual Input Controller 1/16 DIN WCS Series"/></div>
			<div id="PartsContent"><h3>Dual Input Controller 1/16 DIN 
			<br/><font color="#445678">Shinko WCS Series</font></h3>
			<p>Dual Input Controller 1/16 DIN - Shinko WCS Series
			<br/><br/><b>Standard Features:</b>
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard TWO alarm output.
			<br/><br/><b>Input</b>
			<br/>CH1: Multi-range or DC Voltage
			<br/>CH2: Multi-range or DC Voltage or Delay timer or PV difference
			<br/><br/><b>Output</b>
			<br/>CH1: Relay or SSR or Current
			<br/>CH2: Relay or SSR or Current or No control output
			<br/><br/><b>Save mounting space</b>
			<br/><br/><b>Safety Approvals</b>
			<br/>UL and CE Safety Approvals.</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/DIN-Rail-Limit-Controller---Shinko-DCL---33A-Series,-Shinko-DCL---33A-Series.gif" alt="Shinko DIN Rail Limit Controller DCL-33A Series" title="Shinko DIN Rail Limit Controller DCL-33A Series"/></div>
			<div id="PartsContent"><h3>DIN Rail Limit Controller
			<br/><font color="#445678">Shinko DCL-33A Series</font></h3>
			<p>DIN Rail Limit Controller - Shinko DCL-33A Series, Shinko DCL-33A Series
			<br/><br/><b>Standard Features:</b>
			<br/><br/><b>Space Saving Size</b>
			<br/>Easy to mount even in a small machine because the width is just 22.5mm, height 7.5mm and depth 100 mm.
			<br/><br/><b>DCL-33A</b>
			<br/><br/><b>Limit Controller Function Specifications</b>
			<br/>1 setpoint, high- or low-limit control type, and latching limit action
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature full muti-input capability:</b> 1 RTD Type, 10 thermocouple types, mA and V inputs
			<br/><br/><b>Programmable Alarm</b>
			<br/>Units feature standard single alarm output
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL and CE Approvals
			<br/><br/><b>Large LED Display</b>
			<br/>PV Red 4 digits 7.5 x 4.1 mm (H x W)
			<br/>SV Green 4 digits 7.5 x 4.1 mm (H x W)
			<br/><br/><b>Modbus Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilites
			<br/><br/><b>DCL-38A</b>
			<br/><br/><b>Limit Controller Function Specifications</b>
			<br/>1 setpoint, high- or low-limit control type, and latching limit action
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature full muti-input capability:</b> 1 RTD Type, 10 thermocouple types, V inputs (*Ma Input NOT available*) 
			<br/><br/><b>Safety Approvals</b>
			<br/>CE Marking
			<br/><br/><b>Large LED Display</b>
			<br/>PV Red 4 digits 7.5 x 4.1 mm (H x W)
			<br/>SV Green 4 digits 7.5 x 4.1 mm (H x W)</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Controller-1_16-DIN---Shinko-JCS-Series.gif" alt="Shinko Digital Indicating Controller 1/16 DIN JCS Series" title="Shinko Digital Indicating Controller 1/16 DIN JCS Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Controller 1/16 DIN 
			<br/><font color="#445678">Shinko JCS Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/16 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output with optional second alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature true multi-input capabilities:</b> 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>MODBUS Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL, CE and CSA Safety Approvals.</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Indicating-Controller-1_4-DIN---Shinko-JCD-Series.gif" alt="Shinko Digital Indicating Controller 1/4 DIN JCD Series" title="Shinko Digital Indicating Controller 1/4 DIN JCD Series"/></div>
			<div id="PartsContent"><h3>Digital Indicating Controller 1/4 DIN 
			<br/><font color="#445678">Shinko JCD Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Units available in standard DIN sizes (1/4 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output with optional second alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Units feature true multi-input capabilities:</b> 10 thermocouple types, 1 RTD type, 2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>MODBUS Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL, CE and CSA Safety Approvals. </p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/HandHeld-Thermometer_Hygrometer---Shinko-DFT---700.gif" alt="Shinko HandHeld Thermometer/Hygrometer DFT 700" title="Shinko HandHeld Thermometer/Hygrometer DFT 700"/></div>
			<div id="PartsContent"><h3>HandHeld Thermometer/Hygrometer 
			<br/><font color="#445678">Shinko DFT 700</font></h3>
			<p>HandHeld Thermometer/Hygrometer -Shinko DFT-700
			<br/><br/><b>Specifications:</b>
			<br/><br/><b>MultiInput</b>
			<br/>Units offer 4 thermocouple inputs and 1 RTD input.As Hygrometer units use RTD input and humidity sensorwith 0-1vDC
			<br/>(corresponds to 0-100% RH)
			<br/><br/><b>IndicatingAccuracy</b>
			<br/>Input:
			<br/>Thermocouple: &#177;0.2% of full scale &#177;1digit
			<br/>RTD: &#177;0.1% of full scale &#177;1 digit
			<br/>Input:
			<br/>Temperature/Humidity Sensor
			<br/>Temperature: &#177;0.3&deg;C
			<br/>Humidity: &#177;0.3% RH
			<br/><br/><b>InputSampling Rate</b>
			<br/>0.4 seconds
			<br/><br/><b>FunctionKey Settings</b>
			<br/>POWER key: turns DFT-700-M ON or OFF.
			<br/>MODE key: holding this key for approximately 1 second switches the measuring mode between regularand peak hold.
			<br/>HOLD/RST key: holding this key retains the temperature or humidity data at that time, pressing key again resets the data.
			<br/>MEMO key: pressing this key starts data memorymode.
			<br/>SAVE key: pressing this key saves the measuredvalue indicated on the PV display.
			<br/>LIGHT key: pressing this key turns the backlighton (backlight turns of in approximately 10 seconds).
			<br/><br/><b>PowerSupply</b>
			<br/><b>3v DC - 2 AA alkaline batteries. Battery life:</b> Thermometer- 200 hours. Hygrometer - 100 hours.
			<br/><br/><b>Structure</b>
			<br/>Dimensions: W56 x H144 xD30mm.
			<br/>Weight: Approximately 90 grams.
			<br/>Dust-Proof/Drip-Proof: IP65 rated.
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Infrared-Thermometer-with-White-LED-Light-Shinko-R_IRLL02-Series.gif" alt="Shinko Infrared Thermometer with White LED Light R/IRLL02 Series" title="Shinko Infrared Thermometer with White LED Light R/IRLL02 Series"/></div>
			<div id="PartsContent"><h3>Infrared Thermometer with White LED Light 
			<br/><font color="#445678">Shinko R/IRLL02 Series</font></h3>
			<p>The R/IRLL02 a non-contact infrared thermometer with white light LED torch. The applications are indoor/outdoor, laboratory, food, beverage, automobile, electric, animal, and much more.
			<br/><br/><b>Features:</b>
			<br/>&#149; Infrared thermometer with white light LED 
			<br/>&nbsp;&nbsp;torch, 2-in-1 design
			<br/>&#149; Instant, non-contact and accurate 
			<br/>&nbsp;&nbsp;temperature measurement
			<br/>&#149; Easy to use and carry
			<br/>&#149; DS ratio 1:1
			<br/>&#149; Measuring top with anti-dust protection
			<br/>&#149; Measuring top with thermal protection
			<br/>&#149; Low power consumption
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Infrared-Thermometer_Thermocouple-(Thermo-Twin)-Shinko-RCT103F-Series.gif" alt="Shinko Infrared Thermometer Thermocouple (Thermo-Twin) RCT103F Series" title="Shinko Infrared Thermometer Thermocouple (Thermo-Twin) RCT103F Series"/></div>
			<div id="PartsContent"><h3>Infrared Thermometer Thermocouple (Thermo-Twin) 
			<br/><font color="#445678">Shinko RCT103F Series</font></h3>
			<p>The RCT103F a 2-in-1 infrared temperature sensor combined with a thermocouple. This dual function tool fulfills most temperature measurement requirements. The non-contact IR sensor can be used for instantaneous, hard-to-reach, and surface-oriented measurements. The thermocouple functions as the dual of the former: accurately measuring subsurface temperatures.
			<br/><br/><b>Features:</b>
			<br/>&#149; Infrared temperature sensor and 
			<br/>&nbsp;&nbsp;thermocouple 2-in-1 design
			<br/>&#149; NIST traceable
			<br/>&#149; Emissivity adjustable
			<br/>&#149; Maximum temperature, minimum 
			<br/>&nbsp;&nbsp;temperature and lock modes
			<br/>&#149; Retractable thermocouple prove
			<br/>&#149; Dual display
			<br/>&#149; Auto power off (15 seconds)
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Temperature-Calibrator-Shinko-T5014-Series.gif" alt="Shinko Temperature Calibrator T5014 Series" title="Shinko Temperature Calibrator T5014 Series"/></div>
			<div id="PartsContent"><h3>Temperature Calibrator 
			<br/><font color="#445678">Shinko T5014 Series</font></h3>
			<p>The Shinko T5014 Temperature Calibrator lets you calibrate with ease and provides performance in price and use.
			<br/><br/><b>Features:</b>
			<br/>&#149; 8 functions within a single unit - 1/O of 
			<br/>&nbsp;&nbsp;voltage, resistance TC and RTD 
			<br/>&nbsp;&nbsp;selectable
			<br/>&#149; Usable anwhere - 98mm Wide x 204mm 
			<br/>&nbsp;&nbsp;Height x 42mm Depth, weighing only 
			<br/>&nbsp;&nbsp;550g (including holster)
			<br/>&#149; Power saver with its auto-power off 
			<br/>&nbsp;&nbsp;function.
			<br/>&#149; Portable - A carrying case with lead wires is 
			<br/>&nbsp;&nbsp;provided.
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Single-Phase-Power-Controller---Shinko-FPT-Series.gif" alt="Shinko Single Phase Power Controller FPT Series" title="Shinko Single Phase Power Controller FPT Series"/></div>
			<div id="PartsContent"><h3>Single Phase Power Controller
			<br/><font color="#445678">Shinko FPT Series</font></h3>
			<p>Single phase power controller FPT series from Shinko is low cost. It answers to highly precise control needs by phase control like a SSR.
			<br/><br/><b>Standard Features:</b>
			<br/>Slimming called a width of 22mm was realized in 20A article, 45mm was realized in 40A article
			<br/>Easily attach to DIN rail
			<br/>Terminal protective cover
			<br/>UL Standard
			<br/><br/><b>Specifications:</b>
			<br/><br/><b>Maximum Load Current:</b>
			<br/>AC5A, 10A, 20A, 40A
			<br/>Control System: Phase control
			<br/>Applicable Load: Linear (R) load
			<br/>Input Signals: 4-20mA DC
			<br/>Input Impedance: 540 ohm (with 20-mA input) 
			<br/>Built-in constant voltage circuit:</b> 5V
			<br/>Load Voltage: AC100 - 220V
			<br/><br/><b>Permissible Voltage Fluctuation:</b>
			<br/>Within +/- 10% of the rated load voltage
			<br/>Leak Current: 5 mA or less (at 200 VAC)
			<br/>Insulation System: Photo-triac or photo-coupler
			<br/>Insulation Resistance: Over 100 Mohm (500-VDC megger)
			<br/>Insulation Withstand Voltage: 1500 VAC for one minute
			<br/>Permissible Ambient Temperature Range:
			<br/>0&deg;C to 40&deg;C
			<br/>(If the ambient temperature exceeds 40&deg;C, see the load current charateristic diagram.)
			<br/>Permissible Ambient Humidity Range:
			<br/>45% to 85% RH
			<br/>Environmental Conditions:
			<br/>No exposure to corrosive/inflammable gases, dust or vibration
			<br/>Cooling System:
			<br/>Self cooling by radiator fins
			<br/>Installation:
			<br/>DIN rail mounted or wall mounted
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Digital-Program-Controller-1_8-DIN---Shinko-OMR---100.gif" alt="Shinko Digital Program Controller 1/8 DIN OMR 100" title="Shinko Digital Program Controller 1/8 DIN OMR 100"/></div>
			<div id="PartsContent"><h3>Digital Program Controller 1/8 DIN
			<br/><font color="#445678">Shinko OMR 100</font></h3>
			<p>Digital Program Controller 1/8 DIN - Shinko OMR-100
			<br/><br/><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Unit available in standard 1/8 DIN size.
			<br/>NEMA 4X protective construction.vGrey enclosure.
			<br/><br/><b>Auto/Manual Control</b>
			<br/> Manual override allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>MODBUS Protocol With RS485 (Option)</b>
			<br/>Units offer communications capabilities. The OMR-100 Series Controller is designed to work with up to 10 DCL Controllers to provide a versatile multi-zone temperature control system.
			<br/><br/><b>Approvals</b>
			<br/>UL, ULc and CE Approvals.</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/High-Performance-Programmable-Controller-Multifunctional-Shinko-PC-900.gif" alt="Shinko High Performance Programmable Controller Multifunctional PC 900" title="Shinko High Performance Programmable Controller Multifunctional PC 900"/></div>
			<div id="PartsContent"><h3>High Performance Programmable Controller Multifunctional 
			<br/><font color="#445678">Shinko PC 900</font></h3>
			<p>Shinko 1/4 DIN High Performance Programmable Controller PC-900 Series features a maximum of 10 patterns with 10 segments per pattern which can be stored in memory. Each pattern can be linked together so that it is possible to have a program with a maximum of 100 segments.
			<br/>The 1/4 DIN High Performance Programmable Controller PC-900 Series offers full multi-input capabilities, 10 thermocouple types, 2 RTD types, 2 DC current inputs and 1 DC voltage inputs. Output types can be either DC current, relay or SSR driver. Standard supply voltatge is 110 VAC, with 24 volt AC/DC also available. MODBUS Protocol is available if the communication option is psecified. All units off UL, ULc, and CE certification and feature a full 3 year warranty.
			<br/><br/><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Unit available in standard DIN size (1/4 DIN) IP-54 protective construction.
			<br/><br/><b>Programming Features</b>
			<br/>Unit features large program storage capacity of 100 segments in 10 patterns.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units features as standard 3 Alarm outputs with optional 1 Alarm.
			<br/><br/><b>True Multi-Input</b>
			<br/><b>Unit features true multi-input capabilities:</b>10 thermocouple types,
			<br/>2 RTD type, 2 current inputs, and 1 voltage input.
			<br/><br/><b>Control Action</b>
			<br/>Unit features as standard full function third generation PID Autotune or ON/OFF servo output PID.
			<br/><br/><b>Heat/Cool Control Action (option)</b>
			<br/><b>There are 3 types of control output:</b> Relay control output (DR), Non-Contact Voltage output (DS), DC Current output (DA).
			<br/><br/><b>Approvals</b>
			<br/>UL, CUL and CE Safety Approvals.</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/PCD-300-Programmable-Controller.gif" alt="Shinko PCD-300 Programmable Controller" title="Shinko PCD-300 Programmable Controller"/></div>
			<div id="PartsContent"><h3>PCD-300 Programmable Controller</font></h3>
			<p>Shinko North America Limited is pleased to introduce it's 1/4 DIN full function Economical Programable Controller-Model PCD-300. This Controller features nine patterns with up to nine steps per pattern. The unit offers full multi-input capabilities, 10 thermocouple types, 2 RTD types, 2 DC current inputs and 4 DC voltage inputs. Output types can be either DC current, relay, or SSR driver.
			<br/>Model PCD
			<br/>1/4 DIN (96mm X96mm)
			<br/><br/><b>Features:</b>
			<br/><br/><b>Structure</b>
			<br/>Unit available in standard DIN sizes (1/4 DIN).
			<br/>NEMA 4X protective construction.
			<br/>Black enclosure.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Unit features nine patterns with up to nine steps per pattern. Stepts can be programmed from 99 hours and 59 minutes each.
			<br/><br/><b>Ture Multi-Input</b>
			<br/>Unit features true multi-input capabilities.
			<br/>10 thermocouple types, 2 RTD type.
			<br/>2 current inputs, and 4 voltage inputs.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual overide allows you to take control of your process at anytime.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature dual display. PV red 4 digits, SV green 4 digits.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>Modbus Protocol with RS485 (option)</b>
			<br/>Units offer communications capabilities. The PCD-300 can be used in conjunction with the JC Series or the DCL DIN Rail Controller to set up master/slave control systems.
			<br/><br/><b>Approvals</b>
			<br/>UL, CUL and CE Safety Approvals.</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Hybrid-Recorder-Shinko-RM18L.gif" alt="Shinko Hybrid Recorder RM18L" title="Shinko Hybrid Recorder RM18L"/></div>
			<div id="PartsContent"><h3>Hybrid Recorder 
			<br/><font color="#445678">Shinko RM18L</font></h3>
			<p>The RM18L Hybrid Recorder which allows digital printout on an analog trend or pen traces on 180 mm wide chart paper. The RM18L has simple, flexible and reliable functions in a 288 x 288 mm DIN size case.
			<br/><br/><b>Features:</b>
			<br/>&#149; Contact free feedback, 
			<br/>&nbsp;&nbsp;potentiometer and input selector
			<br/>&#149; Both trend and data loggin printouts
			<br/>&#149; Universal inputs and ranges
			<br/>&#149; Wide range of power input voltage
			<br/>&#149; Individual scale plate
			<br/>&#149; Optional communication interface RS-232c 
			<br/>&nbsp;&nbsp;and RS-422A
			<br/>&#149; Recording of up 6, 12 & 24 points
			<br/>&#149; Recording Accuracy +/-0.5%
			<br/>&#149; Dot print interval: 5 Sec. per channel
			<br/>&#149; Input sampling: 5 Sec. per channel
			<br/>&#149; Print head: 6 color ribbon cartridge
			<br/>&#149; Alarm: 4 set points per channel
			<br/>&#149; Alarm ouput (option): Nos. of output: 
			<br/>&nbsp;&nbsp;8 relays
			<br/>&#149; Power supply voltage: 86~264VAC, 
			<br/>&nbsp;&nbsp;45~65Hz
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Strip-Chart-Recorder-100-mm---Shinko-HR---700.gif" alt="Shinko Strip Chart Recorder 100 mm HR 700" title="Shinko Strip Chart Recorder 100 mm HR 700"/></div>
			<div id="PartsContent"><h3>Strip Chart Recorder 100 mm 
			<br/><font color="#445678">Shinko HR 700</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>True Hybrid Design</b>
			<br/>Units offer fully programmable features including: input types, chart speed, data recording session and intervals, scaling, alarm points, etc.
			<br/><br/><b>True Multi-Input</b>
			<br/>Units feature true multi-input capabilities: 13 thermocouple types, 1 RTD type, 1 current inputs, and 7 voltage inputs.
			<br/><br/><b>Computer Communications</b>
			<br/>All units feature standard RS-232C communication interface.
			<br/><br/><b>Large/Full Function LED Display</b>
			<br/>All units feature full function digital display of channel number, PV value, and recording and alarm status, etc. Bright red 18mm display.
			<br/><br/><b>1, 2, And 6 Point Versions Available</b>
			<br/>Units are available in either 1 or 2 channel, or 6 point recording versions.
			<br/><br/><b>Designed For Harsh Environments</b>
			<br/>Units rated to IP65, and constructed of flame resistant resin.
			<br/><br/><b>Compact Design</b>
			<br/>Unit measures only 150 mm in depth and weighs less than 1.5kg.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CSA and CE Safety Approvals.</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/1_8-and-1_4-DIN-Digital-Indicating-Controller-Shinko-ACR-and-ACD-Series.gif" alt="Shinko 1/8 and 1/4 DIN Digital Indicating Controller ACR and ACD Series" title="Shinko 1/8 and 1/4 DIN Digital Indicating Controller ACR and ACD Series"/></div>
			<div id="PartsContent"><h3>1/8 and 1/4 DIN Digital Indicating Controller 
			<br/><font color="#445678">Shinko ACR and ACD Series</font></h3>
			<br/><p>Shinko North America Limited introduces the ACR, ACD Series digital indicating 
			controller. A variety of functions extend the possibility of the SHINKO ACR & ACD controller 
			including a guaranteed Dual output, Programmable alarm, Analog transmission, Heater burnout alarm, 
			RS-485, RS-232C communications, Remote setting, Transmitter power supply, Event input, and Event output.
			<br/>Industry Leading Large Display
			<br/>Multi-Functions, Simple to Operate
			<br/><br/><b>Features:</b>
			<br/><br/><b>Large LCD display</b>
			<br/>A specially treated large LCD display makes it easier to view even in bright light and open-air.
			<br/><br/><b>PV display (ACD series):</b> 24.0mm X 11.0mm (H XW) 	
			<br/><br/><b>An easily viewable bar graph</b>
			<br/>22-segment bar graph allows simultaneous PV, SV, MV viewing.
			<br/>Ease of viewing for manual output operation.
			<br/>For the ACD-15A and ACR-15A, the motor valve opening can be checked with the bar graph.
			<br/>(When feedback potentiometer "Yes" is set.) 	
			<br/><br/><b>Enhanced visibility</b>
			<br/>PV display color selectable from red, green and orange. Colors can be set depending on the deviation between PV and SV, so status can be checked from a distance. 	
			<br/><br/><b>PID zone function:</b> PID resetting due to SV change Unnecessary
			<br/>Up to 5 groups of PID parameters can be set. When SV is changed, PID parameters are automatically changed for optimal contol. (It is not necessary to reset PID after SV is changed.)
			<br/><br/><b>Simple Operation in Simplified setting mode</b>
			<br/>Without setting engineering items, simplified settting mode can prevent operational mistakes, and simple operations run smoothly. Basic settings and key operations now are doable via 3-key usage.
			<br/><br/><b>Power unnecessary if USB comm. cable used</b>
			<br/>If CMB-001 USB communication cable (sold seperately) is used, a power supply for the controller is not necessary. Wiring for the initial setting is reduced. Data logging and monitoring can be carried out via the monitoring sofeware (sold seperately).</p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Programmable-Signal-Conditioner-With-Dual-Display-Shinko-SA-Series.gif" alt="Shinko Programmable Signal Conditioner With Dual Display SA Series" title="Shinko Programmable Signal Conditioner With Dual Display SA Series"/></div>
			<div id="PartsContent"><h3>Programmable Signal Conditioner With Dual Display 
			<br/><font color="#445678">Shinko SA Series</font></h3>
			<p>Shinko North America Limited is pleased to introduce the SA Series Programmable Signal Conditioner with Dual Display..
			<br/>Make your work "convy"nient with CONVY.
			<br/>Configurable I/O minimizes your stock.
			<br/>Oversee your I/O with dual front displays.
			<br/>No need to purchase extra configuration software or units, front key configurable
			<br/>Various functions incorporated as standard besides signal conversion
			<br/>Yes, the answer is CONVY.
			<br/><br/><b>Features:</b>
			<br/>I/O Configurable
			<br/>I/O configuration with 3 front keys (UP, DOWN, and MODE key). Besides I/O configuration, other items such as ramp time, reverse output are configurable with these keys.
			<br/>Dual Display
			<br/>4 digits Dual display for input (real value) and output (percentage) is incorporated. The indication time is adjustable within 0:00 - 60:00 (Min:Sec) for saving energy.
			<br/>Dual Output Models
			<br/>Signal splitter is available with models: SAWU (Universal I/O) and SAWD (Current loop supply).
			<br/>Various Functions
			<br/>Filter time constant (Ramp buffer) and Reverse output function incorporated as standard. The adjustable range for the Filter timeconstant is within 0.0 to 10sec.
			<br/>The SA Series Programmable Signal Conditioner featuresa full 3 year warranty and lifetime technical support! </p>
			</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/Signal-Conditioner-Shinko-SB-Series.gif" alt="Shinko Signal Conditioner SB Series" title="Shinko Signal Conditioner SB Series"/></div>
			<div id="PartsContent"><h3>Signal Conditioner 
			<br/><font color="#445678">Shinko SB Series</font></h3>
			<p>The Shinko SB Series Signal Conditioner is slim, economical and compact.
			<br/><br/><b>Features:</b>
			<br/><br/><b>Removable Terminal Block</b>
			<br/>Front access makes wiring and maintenance easy.
			<br/><br/><b>Space-Saving</b>
			<br/>17.5 x 75 x 85mm (W x H x D)
			<br/><br/><b>3-Port Isolation</b>
			<br/>Input - Output - Power at 500V DC
			<br/><br/><b>Universal Power Supply</b>
			<br/>24V DC (20 to 28 V DC) or 100 to 240V AC (85 to 264V AC)
			<br/>COST or cost
			<br/>Choose your cost.
			<br/>"Cost reduction" is a key phrase for successful business.
			<br/>The SB series is designed to meet the needs of the standard users and the cost focused.
			<br/>When you need a reliable, standard conditioner, the SB series can help.
			<br/>Models
			<br/>Model
			<br/>Input Type
			<br/>SBE
			<br/>Thermocouple
			<br/>SBR
			<br/>RTD
			<br/>SBA
			<br/>DC Current
			<br/>SBV
			<br/>DC Voltage
			<br/>SBP
			<br/>Potentiometer
			<br/>SBD
			<br/>Current Loop Supply
			<br/>The SB Series Signal Conditioner features a full 3 year warranty and lifetime technical support! </p>
			</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/SOild-State-Relay-Shinko-SA-300-Z-Series.gif" alt="Shinko Soild State Relay SA-300-Z Series" title="Shinko Soild State Relay SA-300-Z Series"/></div>
			<div id="PartsContent"><h3>Soild State Relay Shinko 
			<br/><font color="#445678">SA-300-Z Series</font></h3>
			<p>Shinko SA-300-Z Series Solid State Relay enable your own system to be constructed with outstanding functionality at little expense.
			<br/><br/><b>Features:</b>
			<br/>&#149; Economically priced
			<br/>&#149; Built-in varistor for absorbing external 
			<br/>&nbsp;&nbsp;power surges
			<br/>&#149; Wide range of input power suplly: 
			<br/>&nbsp;&nbsp;4 to 32V DC
			<br/>&#149; Easy viewing of action by LED display
			<br/>&#149; High insulation, dielectric stength 
			<br/>&nbsp;&nbsp;structure: Ranging between input and 
			<br/>&nbsp;&nbsp;output: 4,000V
			<br/><br/><b>Applications:</b>
			<br/>&#149; Molding machinery (heater control)
			<br/>&#149; Constant temperature ovens 
			<br/>&nbsp;&nbsp;(heater control)
			<br/>&#149; Printing machinery (heater control)
			<br/>&#149; Machine tools (motor control)
			<br/><br/><b>Specifications:</b>
			<br/>&#149; Rated load current: 20A (SA-320-Z), 
			<br/>&nbsp;&nbsp;40A (SA-340-Z)
			<br/>&#149; Load supply voltage: 75 top 250V AC, 
			<br/>&nbsp;&nbsp;45 to 65Hz
			<br/>&#149; Operation input voltage: 4 to 32V DC
			<br/>&#149; Max. input current: 20mA or less
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Shinko/1_32-DIN-Digital-Temperature-Controller-Shinko-FCL-Series.gif" alt="Shinko 1/32 DIN Digital Temperature Controller FCL Series" title="Shinko 1/32 DIN Digital Temperature Controller FCL Series"/></div>
			<div id="PartsContent"><h3>1/32 DIN Digital Temperature Controller 
			<br/><font color="#445678">Shinko FCL Series</font></h3>
			<p><b>Standard Features:</b>
			<br/><br/><b>Structure</b>
			<br/>NEMA 4X protective construction. 
			<br/>Black enclosure color.
			<br/><br/><b>Programmable Alarms</b>
			<br/>Units feature standard single alarm output.
			<br/><br/><b>Multi-Input</b>
			<br/>Units feature multi-input capabilities:
			<br/>5 thermocouple types and 1 RTD type.
			<br/><br/><b>Auto/Manual Control</b>
			<br/>Manual override allows you to take control of your
			process at anytime.
			<br/><br/><b>Superior Security Options</b>
			<br/>This feature eliminates potential operator error.
			<br/><br/><b>Safety Approvals</b>
			<br/>UL, CUL and CE Safety Approvals.
			<br/><br/><b>Warranty</b>
			<br/> All units manufactured to strict ISO standards and offer full 3 year manufacturers warranty.
			<br/><br/><b>Low Cost</b>
			<br/>Most advanced price/performance package available.
			<br/><br/><b>PID Autotune</b>
			<br/>All units feature as standard full function third generation PID Autotune. This feature minimizes process overshoot under the most demanding applications.
			<br/><br/><b>Large LED Display</b>
			<br/>All units feature bright display of either PV or SV, red 4 digits.</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/1_32-DIN-Digital-Temperature-Controller-Shinko-FCL-Series_Thumbnail.gif" img border="0" alt="Shinko 1/32 DIN Digital Temperature Controller FCL Series" title="Shinko 1/32 DIN Digital Temperature Controller FCL Series" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Temperature-Controller-with-Ramp_Soak---Shinko-JCL-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Temperature Controller with Ramp Soak JCL Series" title="Shinko Digital Temperature Controller with Ramp Soak JCL Series" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Controller---Shinko-ACS--13A-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Controller ACS-13A Series" title="Shinko Digital Indicating Controller ACS-13A Series"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Limit-Controller-1_16-DIN---Shinko-JCS-Series_Thumbnail.gif" img border="0" alt="Shinko Limit Controller 1/16 DIN JCS Series" title="Shinko Limit Controller 1/16 DIN JCS Series" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Controller-1_8-DIN-Shinko-ACR-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Controller 1/8 DIN ACR Series" title="Shinko Digital Indicating Controller 1/8 DIN ACR Series" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Temperature-Controllers-1_8-DIN---Shinko-JCR-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Temperature Controllers 1/8 DIN JCR Series" title="Shinko Digital Indicating Temperature Controllers 1/8 DIN JCR Series" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Controller-1_4-DIN---Shinko-ACD-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Controller 1/4 DIN ACD Series" title="Shinko Digital Indicating Controller 1/4 DIN ACD Series" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Limit-Controller-1_4-DIN---Shinko-JCD-Series_Thumbnail.gif" img border="0" alt="Shinko Limit Controller 1/4 DIN JCD Series" title="Shinko Limit Controller 1/4 DIN JCD Series"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/DIN-Rail-Indicating-Controller---Shinko-DCL---33A-Series_Thumbnail.gif" img border="0" alt="Shinko DIN Rail Indicating Controller DCL 33A Series" title="Shinko DIN Rail Indicating Controller DCL 33A Series" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Temperature-Control-Unit-Shinko-NCL---13A_Thumbnail.gif" img border="0" alt="Shinko Temperature Control Unit NCL 13A" title="Shinko Temperature Control Unit NCL 13A"/></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/1_8-DIN-Digital-Indicator---Shinko-JIR---301-M_Thumbnail.gif" img border="0" alt="Shinko 1/8 DIN Digital Indicator JIR-301-M" title="Shinko 1/8 DIN Digital Indicator JIR-301-M" /></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Dual-Input-Controller-1_16-DIN---Shinko-WCS-Series_Thumbnail.gif" img border="0" alt="Shinko Dual Input Controller 1/16 DIN WCS Series" title="Shinko Dual Input Controller 1/16 DIN WCS Series" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/DIN-Rail-Limit-Controller---Shinko-DCL---33A-Series,-Shinko-DCL---33A-Series_Thumbnail.gif" img border="0" alt="Shinko DIN Rail Limit Controller DCL-33A Series" title="Shinko DIN Rail Limit Controller DCL-33A Series" /></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Controller-1_16-DIN---Shinko-JCS-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Controller 1/16 DIN JCS Series" title="Shinko Digital Indicating Controller 1/16 DIN JCS Series" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Indicating-Controller-1_4-DIN---Shinko-JCD-Series_Thumbnail.gif" img border="0" alt="Shinko Digital Indicating Controller 1/4 DIN JCD Series" title="Shinko Digital Indicating Controller 1/4 DIN JCD Series" /></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/HandHeld-Thermometer_Hygrometer---Shinko-DFT---700_Thumbnail.gif" img border="0" alt="Shinko HandHeld Thermometer/Hygrometer DFT 700" title="Shinko HandHeld Thermometer/Hygrometer DFT 700" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Infrared-Thermometer-with-White-LED-Light-Shinko-R_IRLL02-Series_Thumbnail.gif" img border="0" alt="Shinko Infrared Thermometer with White LED Light R/IRLL02 Series" title="Shinko Infrared Thermometer with White LED Light R/IRLL02 Series" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Infrared-Thermometer_Thermocouple-(Thermo-Twin)-Shinko-RCT103F-Series_Thumbnail.gif" img border="0" alt="Shinko Infrared Thermometer Thermocouple (Thermo-Twin) RCT103F Series" title="Shinko Infrared Thermometer Thermocouple (Thermo-Twin) RCT103F Series" /></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Temperature-Calibrator-Shinko-T5014-Series_Thumbnail.gif" img border="0" alt="Shinko Temperature Calibrator T5014 Series" title="Shinko Temperature Calibrator T5014 Series" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Single-Phase-Power-Controller---Shinko-FPT-Series_Thumbnail.gif" img border="0" alt="Shinko Single Phase Power Controller FPT Series" title="Shinko Single Phase Power Controller FPT Series" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Digital-Program-Controller-1_8-DIN---Shinko-OMR---100_Thumbnail.gif" img border="0" alt="Shinko Digital Program Controller 1/8 DIN OMR 100" title="Shinko Digital Program Controller 1/8 DIN OMR 100" /></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/High-Performance-Programmable-Controller-Multifunctional-Shinko-PC-900_Thumbnail.gif" img border="0" alt="Shinko High Performance Programmable Controller Multifunctional PC 900" title="Shinko High Performance Programmable Controller Multifunctional PC 900" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/PCD-300-Programmable-Controller_Thumbnail.gif" img border ="0" alt="Shinko PCD-300 Programmable Controller" title="Shinko PCD-300 Programmable Controller" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Hybrid-Recorder-Shinko-RM18L_Thumbnail.gif" img border="0" alt="Shinko Hybrid Recorder RM18L" title="Shinko Hybrid Recorder RM18L" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Strip-Chart-Recorder-100-mm---Shinko-HR---700_Thumbnail.gif" img border="0" alt="Shinko Strip Chart Recorder 100 mm HR 700" title="Shinko Strip Chart Recorder 100 mm HR 700" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/1_8-and-1_4-DIN-Digital-Indicating-Controller-Shinko-ACR-and-ACD-Series_Thumbnail.gif" img border="0" alt="Shinko 1/8 and 1/4 DIN Digital Indicating Controller ACR and ACD Series" title="Shinko 1/8 and 1/4 DIN Digital Indicating Controller ACR and ACD Series" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Programmable-Signal-Conditioner-With-Dual-Display-Shinko-SA-Series_Thumbnail.gif" img border="0" alt="Shinko Programmable Signal Conditioner With Dual Display SA Series" title="Shinko Programmable Signal Conditioner With Dual Display SA Series" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/Signal-Conditioner-Shinko-SB-Series_Thumbnail.gif" img border="0" alt="Shinko Signal Conditioner SB Series" title="Shinko Signal Conditioner SB Series"/></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Shinko/thumbnails/SOild-State-Relay-Shinko-SA-300-Z-Series_Thumbnail.gif" img border="0" alt="Shinko Soild State Relay SA-300-Z Series" title="Shinko Soild State Relay SA-300-Z Series" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>