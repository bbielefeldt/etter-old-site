<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Technical Tips</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
</head>
<body link="#445679" vlink="#445679">
<script type="text/javascript">
google.load("jquery", "1");
</script>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="TechTipsLeftTxt">
<div id="TechTipsHeader"><font size="2" color="#445679"><b>Technical Tips</b></font></div>
<br/><a href="tech_tips.php" id="TechTipGoodVV"><font color="#ACB0C3"><b>&#149; The Good Old Vent Valve!</b></font></a>
<br/><a href="tech_tips_intro_burners.php"><font color="#ACB0C3"><b>&#149; Intro to Commercial and Industrial Burners</b></font></a>
<br/><a href="tech_tips_basic_burners.php"><font color="#ACB0C3"><b>&#149; Basic Burner Types</b></font></a>
<br/><a href="tech_tips_burner_tuning.php"><font color="#ACB0C3"><b>&#149; Basic Burner Tuning</b></font></a>
<br/><a href="tech_tips_burners_vs_clunkers.php"><font color="#ACB0C3"><b>&#149; Cash for Burners vs. Cash for Clunkers</b></font></a>
</div>
<div id="InsidetheJob">
<div id="TechRightHeader"><blockquote><font size="2" color="#D21D1F"><b><br/>Basic Burner Types</b></font></blockquote></div>
<br/><blockquote>By: Herb H Etter
<br/><br/>In an earlier article we concentrated on the safety aspects of commercial and industrial gas burner operation.  
As with any equipment, the more we understand of the operation, the better equipped we are to safely get the most productivity 
from the unit. We will start our equipment review by beginning with the gas line that comes into the building.
<br/><br/>The most common gas fuel is natural gas, as distributed by your local gas company. Gas is odorless ( they add the 
distinctive smell) and lighter than air      ( 0.6 specific gravity, air is 1.0 ). One cubic foot of natural gas contains 
approximately 1000 Btus.  One British Thermal Unit has a heat content about equal to that obtained in burning one wooden 
match. Natural gas is a hydrocarbon, mostly methane, CH4. When burned ( united with oxygen) the products of combustion are 
mostly CO2 and H2O vapor. The part we are mostly interested in is the heat generated by the burning or chemical reaction. 
A proper combustion mix involves 1 part of natural gas and 10 parts of air. More than 1 part gas, the mix is called rich, 
less than 1 part gas the mix is lean. Flame from a rich mix tends toward being yellow, a lean flame more on the blue side.
<br/><br/>Having mastered the "short-short" gas course above, we move on to burners. With the great variety of burner styles 
available, it is probably easier to first list some of the ways air and gas get mixed and delivered to the various burners.
<br/><br/>- <b>Atmospheric Mixers:</b>  In this type mixer, gas is discharged through a small orifice into the open end of 
a venturi tube. The gas velocity entrains ambient air ( about 80% of that needed for complete combustion) and delivers the 
mix to the burner. The remaining 20% of air comes from the air surrounding the flame. 
<br/><br/>- <b>Low Pressure or Proportional Mixers:</b>  These mixers utilize a zero governor, which is a diaphragm-actuated 
valve similar to a gas pressure regulator. This downward opening valve is held up and shut by a spring, and opens only when 
a negative pressure is sensed at the valve outlet.   The zero governor is piped to the negative chamber of a venturi tube 
or mixer. A combustion air blower directs air into the venturi, creating a negative pressure at the zero governor outlet, 
causing gas to flow and mix with the combustion air. Varying the air flow to the venturi varies the negative pressure and 
consequently the gas flow. This provides single air valve control for both the air and gas flow.
<br/><br/>- <b>Premix:</b>  This system provides 100% air-gas mix through a mechanical mixer, usually a centrifugal blower 
that pulls air in one side inlet and gas in the other side. The resultant mix is delivered to the burner or burners through 
appropriate flame arresters, etc.
<br/><br/>Burner types are endless, but a generalization would include these groups:
<br/><br/>- Drilled pipe burners, ring burners, nozzle burners, can be supplied  with air-gas mix via atmospheric mixers 
described above.
<br/><br/>- Nozzle burners equipped with proportional mixers will have a higher heat release than when equipped with 
atmospheric mixers. This is due to the higher mixture pressure available. Other burners using these mixers would include 
line burners, and some infrared burners.
<br/><br/>- Nozzle or Burner mix burners keep the air and gas separate until they mix within the burner housing. This is 
typical of burners used on high temperature furnaces.
<br/><br/>- Packaged burners, sometimes called power burners, have a combustion air blower as an integral part of the 
burner assembly. This design is typical of boiler burners and air heating burners.      
<br/><br/>While the above listing is by no means complete, hopefully it will enable you to identify your burner type, 
and provide an indication as to how it functions. Our future discussions will cover how safety and temperature control 
accessories are applied to the burner.  
</blockquote></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>