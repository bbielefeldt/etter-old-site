<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Bryan Donkin combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Bryan Donkin,High Pressure Regulators Series 200,High Pressure Regulators Series 273 Pilot Loaded,High Pressure Regulators Series 280/281,High Pressure Regulators Series 284/285,Medium Pressure Regulators Series 240 and 240PL,Medium Pressure Regulators Series 260,Medium Pressure Regulators Series 274,Low Pressure Regulators Series 225,Low Pressure Regulators Series 226,Rotary Meters,Turbine Meters,Vortex Shedding Meter,Ultra Sonics Meter,Meter Correctors,FM-Approved Check Valves Model 590 Rollchek,Pressure Slam Shuts Model 290,Pressure Slam Shuts Model 309LP,Pressure Slam Shuts Model 309LP2,Pressure Slam Shuts Model 309LP4,Pressure Slam Shuts Model 303,Pressure Slam Shuts Model 304,Pressure Slam Shuts Model 305,Pressure Relief Valves Model 201,Pressure Relief Valves Model 225LP,Pressure Relief Valves Model 225LP2,Pressure Relief Valves Model 225LP4" />
<title>ETTER Engineering - Bryan Donkin Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="BryanDonkinLogoLarge"></div>
<div id="SensusText">Bryan Donkin RMG (A Honeywell Company) has a strong reputation as a world leader in the manufacture and marketing of Gas Control Equipment. This has been achieved by combining nearly two centuries of experience and expertise with an impressive yet calculated strategy of continuous product development, investment and total quality commitment to all aspects of the business. Bryan Donkin are new to the United States but have been a staple in the utility products used in Canada and abroad. The Bryan Donkin product line brings with it many exciting offerings including indoor ventless gas regulators, and OPCO over pressure cut-offs or slam-shuts. Give us a call and we would be pleased to assist you. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_highseries_200.gif" alt="Bryan Donkin High Pressure Regulators Series 200" title="Bryan Donkin High Pressure Regulators Series 200"/></div>
			<div id="PartsContent"><h3>High Pressure Regulators
			<br/><font color="#445679" size="2">Series 200</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1000 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 to 175 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#190;" and 1"
			<br/><font color="#424242" size="1"><b>Cross Ref :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sensus 046, 141-A
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher 627, 630
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actaris VR 75
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 200 are high pressure, first-cut regulators, also known as "Farm Taps" ideally designed for multi-stage pressure reduction applications, active/monitor configurations and as a by-pass regulator for pressure reducing stations.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 200 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 200 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>      
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_highseries_273pilotloaded.gif" alt="Bryan Donkin High Pressure Regulators Series 273 Pilot Loaded" title="Bryan Donkin High Pressure Regulators Series 273 Pilot Loaded"/></div>
			<div id="PartsContent"><h3>High Pressure Regulators
			<br/><font color="#445679" size="2">Series 273 Pilot Loaded</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;265 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2 PSIG to 60 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><font color="#424242" size="1"><b>Cross Ref : </b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sensus 243-RPC
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher 299
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American 2002, 1800-CP
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 273 PL are pilot-loaded regulators resulting in superior accuracy and control of the outlet pressure. Ideally designed for multi-installations, commercial and industrial applications. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 273PL Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 273PL Configurator_REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_highseries_280.gif" alt="Bryan Donkin High Pressure Regulators Series 280/281" title="Bryan Donkin High Pressure Regulators Series 280/281"/></div>
			<div id="PartsContent"><h3>High Pressure Regulators
			<br/><font color="#445679" size="2">Series 280/281</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 150 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 6" wc to 6 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Sensus 441-S
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher 399
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American AFV300, RFV
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 280-281 are a high pressure, high capacity regulator ideally designed for commercial, industrial applications along with municipal installations. This series has many options, accessories and related specialty products that offers a total solution to most applications.</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_highseries_284.gif" alt="Bryan Donkin High Pressure Regulators Series 284/285" title="Bryan Donkin High Pressure Regulators Series 284/285"/></div>
			<div id="PartsContent"><h3>High Pressure Regulators
			<br/><font color="#445679" size="2">Series 284/285</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 65 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 2 PSI to 15 PSI
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Sensus 441-S
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher 399
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American AFV300, RFV
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 284-285 are a high pressure, high capacity regulator ideally designed for commercial, industrial applications along with municipal installations. This series has many options, accessories and related specialty products that offers a total solution to most applications. </p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_mediumpressure_240.gif" alt="Bryan Donkin Medium Pressure Regulators Series 240 and 240PL" title="Bryan Donkin Pressure Regulators Series 240 and 240PL"/></div>
			<div id="PartsContent"><h3>Medium Pressure Regulators
			<br/><font color="#445679" size="2">Series 240 and 240PL</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 150 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 4" wc to 3 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> &#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#189;"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Sensus 134-80, 143-B
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher S-100, S-400
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American 1813-C
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 240 is a high pressure regulator ideally designed for domestic installations. Commonly it is utilized in commercial and light industrial applications. With this series comes many options, accessories and related specialty products. As a result this regulator is very flexible and offers a solution to most applications. A wide range of body connections are available with this series.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 240 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>240 Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 240 Regulator 2010 Configurator REV_2010-02.pdf" target="_blank"><font color="#ACB0C3"><b>240 Configurator</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Literature/Model 240PL Regulator 2010 Technical Manual.pdf" target="_blank"><font color="#ACB0C3"><b>240PL Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 240PL Configurator_REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>240PL Configurator</b></font></a></p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_mediumpressure_260.gif" alt="Bryan Donkin Medium Pressure Regulators Series 260" title="Bryan Donkin Medium Pressure Regulators Series 260"/></div>
			<div id="PartsContent"><h3>Medium Pressure Regulators
			<br/><font color="#445679" size="2">Series 260</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></font>150 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 4" wc to 5 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> &#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Sensus 043-B
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher S-250
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American 1200
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actaris 133-31
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 260 is a high pressure regulator ideally designed for domestic installations. It is often utilized in commercial and light industrial applications. This series has unique features compared to competitive products. With this series comes many options, accessories and related specialty products. As a result this regulator is very flexible and offers a solution to most applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 260 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 260 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon//Bryan_Donkin/product_regulator_mediumpressure_274.gif" alt="Bryan Donkin Medium Pressure Regulators Series 274" title="Bryan Donkin Medium Pressure Regulators Series 274"/></div>
			<div id="PartsContent"><h3>Medium Pressure Regulators
			<br/><font color="#445679" size="2">Series 274</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></font>150 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></font>6" wc to 6 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></font> 1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b></font>Sensus 243-12
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher S-200
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;American 1800, 2000
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actaris 233-31
			<br/><br/><font color="#424242" size="1"><b>Description : </b></font>Series 274 is a high pressure, high capacity regulator ideally designed for commercial, industrial applications along with municipal installations. This series has many options, accessories and related specialty products that offers a total solution to most applications. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 274 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 274 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>		
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_lowpressure_225.gif" alt="Bryan Donkin Low Pressure Regulators Series 225" title="Bryan Donkin Low Pressure Regulators Series 225"/></div>
			<div id="PartsContent"><h3>Low Pressure Regulators
			<br/><font color="#445679" size="2">Series 225</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 3 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 2" wc to 13" wc
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> &#190;" and 1"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Maxitrol 325 Series
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 225 is a low pressure regulator often referred to as "Inches to Inches" regulators. Commonly it is utilized in commercial and light industrial applications directly installed on the appliance. For an example, this regulator could be for an industrial boiler taking a 2 PSIG plant header and reducing it to 10" wc at the boiler.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 201_225 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 201_225 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_lowpressure_226.gif" alt="Bryan Donkin Low Pressure Regulators Series 226" title="Bryan Donkin Low Pressure Regulators Series 226"/></div>
			<div id="PartsContent"><h3>Low Pressure Regulators
			<br/><font color="#445679" size="2">Series 226</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> up to 5 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> 1.5" wc to 60" wc
			<br/><font color="#424242" size="1"><b>Body Sizes :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> &#190;"
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1"
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#188;"
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;"
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2-&#189;" 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3"
			<br/><font color="#424242" size="1"><b>Cross Ref :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font> Maxitrol 210 Series
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 226 is a low pressure regulator often referred to as "Inches to Inches" regulators. It is considered the big brother to the Series 225 and has sizes all the way up to 3". Commonly it is utilized in commercial and light industrial applications directly installed on the appliance. For an example, this regulator could be for an industrial boiler taking a 4 PSIG plant header and reducing it to 28" wc at the heat treating furnace.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 226 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Techincal Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Literature/Model 226 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_rotarymeter.gif" alt="Bryan Donkin Rotary Meters" title="Bryan Donkin Rotary Meters"/></div>
			<div id="PartsContent"><h3>Rotary Meters</h3>
			<p><br/><font color="#424242" size="1"><b>Typical Application :</b></font> Commercial to low end Industrial
			<br/><br/><font color="#424242" size="1"><b>Suitable for : </b></font>Suitable for natural gas and all non-corrosive gases. Field orientable for flow direction. No upstream or downstream piping restrictions. Can be combined with EC-21 and EC-24 meter corrector electronics.
			<br/><br/><font color="#424242" size="1"><b>Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2" and 3" 
			<br/><font color="#424242" size="1"><b>Flows : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 17,600 SCFH at 15 PSIG 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/documents/BD132ARotary.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_turbine.gif" alt="Bryan Donkin Turbine Meters" title="Bryan Donkin Turbine Meters"/></div>
			<div id="PartsContent"><h3>Turbine Meters</h3>
			<p><br/><font color="#424242" size="1"><b>Typical Application : </b></font>Large Commercial to Industrial
			<br/><br/><font color="#424242" size="1"><b>Suitable for : </b></font>Suitable for natural gas and all non-corrosive gases, special versions for corrosive gases. Field orientable for flow direction, horizontal or vertical piping. Comes standard with low and high frequency outputs, built in battery powered electronic totalizer. Can be combined with EC-21 and EC-24 meter corrector electronics.
			<br/><br/><font color="#424242" size="1"><b>Sizes :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1" up to 24" 
			<br/><font color="#424242" size="1"><b>Flows :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From 900 to 1,800,000 SCFH 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;at 15 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/documents/TERZ-94OMManualRev0107.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_vortexshedding.gif" alt="Bryan Donkin Vortex Shedding Meter" title="Bryan Donkin Vortex Shedding Meter"/></div>
			<div id="PartsContent"><h3>Vortex Shedding Meter</h3>
			<p><br/><font color="#424242" size="1"><b>Typical Application :</b></font> Industrial and Specialty Applications
			<br/><br/><font color="#424242" size="1"><b>Suitable for :</b></font> Suitable for all gaseous and liquid medias typical to the gas, engineering and chemical industry. Large measuring range and highly accurate, excellent repeatability, installed in any position, maintenance possible without interruption of flow, dual output signal for additional devices such as data acquisition, processing devices, and flow computers.
			<br/><br/><font color="#424242" size="1"><b>Sizes :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1" up to 24" 
			<br/><font color="#424242" size="1"><b>Flows:</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From 2,300 to 2,800,000 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCFH at 15 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/meters/Vortex Shedding Meter OM wbz08.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_ultrasonic.gif" alt="Bryan Donkin Ultra Sonics Meter" title="Bryan Donkin Ultra Sonics Meter"/></div>
			<div id="PartsContent"><h3>Ultra Sonics Meter</h3>
			<p><br/><font color="#424242" size="1"><b>Typical Application : </b></font>Large Industrial to Utility &amp; Pipeline
			<br/><br/><font color="#424242" size="1"><b>Suitable for : </b></font>High stability in case of flow disturbances through the use of up to 6 acoustical paths, replacement of sensors without the need for recalibration, high flow and essentially no pressure drop, robust sensor design using titanium is essentially dirt repellent. It measures flow in both directions, extremely accurate and repeatable.
			<br/><br/><font color="#424242" size="1"><b>Sizes : </b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4" up to 40" 
			<br/><font color="#424242" size="1"><b>Flows: </b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From 500 to 7,700,000 SCFH 
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;at 15 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/meters/UltraSonic Meter O&M usz08.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_metercorrector.gif" alt="Bryan Donkin Meter Correctors" title="Bryan Donkin Meter Correctors"/></div>
			<div id="PartsContent"><h3><font color="#D21D1F" size="2"><b>Meter Correctors</b></font></h3>
			<p><br/><font color="#424242" size="1"><b>Typical Application :</b></font> Rotary and Turbine meters for instances when real time compressibility compensation is required
			<br/><br/><font color="#424242" size="1"><b>Suitable for :</b></font> Both Temperature, and Pressure and Temperature correctors to account for variations in actual measured flows due to compressibility variants. The EC-21 Temperature Corrector and EC-24 Pressure and Temperature Corrector can be battery or 24 Vdc powered, options for explosion proof environments, and 4-20mA analog output of corrected flow rates available.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/documents/ec24ec21manual.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/590_rollchek.gif" alt="Bryan Donkin FM-Approved Check Valves Model 590 Rollchek" title="Bryan Donkin FM-Approved Check Valves Model 590 Rollchek"/></div>
			<div id="PartsContent"><h3><font color="#D21D1F" size="2"><b>FM-Approved Check Valves</b></font>
			<br/><font color="#445679" size="2">Model 590 Rollchek</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description :</b></font> The FM Approved Series 590 is designed to prevent reverse flow of gas or air in a pipeline and provdes a positive seal with very low differential pressure.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/documents/590_ Rollchek_FM_Approval.pdf" target="_blank"><font color="#ACB0C3"><b>View FM Approval</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Literature/Model 590 Rollchek 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a></p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_290.gif" alt="Bryan Donkin Pressure Slam Shuts Model 290" title="Bryan Donkin Pressure Slam Shuts Model 290"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 290</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Model 290 is only available with over pressure cut-off (OPCO) protection.
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1/2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;" 
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><font color="#424242" size="1"><b>Pressure :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 375 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;1" and 2"
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Safety Slam-Shut Valves 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/290 Safety Slam-Shut Valve Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_390lp.gif" alt="Bryan Donkin Pressure Slam Shuts Model 309LP" title="Bryan Donkin Pressure Slam Shuts Model 309LP"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 309LP</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Series 309 are available with over pressure cut-off (OPCO) or both under and over pressure cut-off protection (UPCO/OPCO).
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1/2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#188;",
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 150 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1" 
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2"
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Safety Slam-Shut Valves 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/309 Safety Slam-Shut Valves Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_309lp2.gif" alt="Bryan Donkin Pressure Slam Shuts Model 309LP2" title="Bryan Donkin Pressure Slam Shuts Model 309LP2"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 309LP2</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Series 309 are available with over pressure cut-off (OPCO) or both under and over pressure cut-off protection (UPCO/OPCO).
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1/2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1",
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#189;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2"
			<br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 275 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;1" and 2"
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Safety Slam-Shut Valves 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/309 Safety Slam-Shut Valves Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_309lp4.gif" alt="Bryan Donkin Pressure Slam Shuts Model 309LP4" title="Bryan Donkin Pressure Slam Shuts Model 309LP4"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 309LP4</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Series 309 are available with over pressure cut-off (OPCO) or both under and over pressure cut-off protection (UPCO/OPCO).
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1/2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#190;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-&#189;" 
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2"
			<br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 275 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1" and 2"
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Safety Slam-Shut Valves 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/309 Safety Slam-Shut Valves Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_303.gif" alt="Bryan Donkin Pressure Slam Shuts Model 303" title="Bryan Donkin Pressure Slam Shuts Model 303"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 303</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>Designed to safeguard gas distribution systems, normally installed upstream of the regulator with the external control line piped to the downstream of the regulator, closing automatically in the event of an overpressure condition. Integral push-button type pressure equalizing valve, manual reset, UPCO low pressure loss, completely self-acting, no external power supply required.
			<br/><br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 275 PSIG	
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;6" to 12"
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_304.gif" alt="Bryan Donkin Pressure Slam Shuts Model 304" title="Bryan Donkin Pressure Slam Shuts Model 304"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 304</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>Designed to safeguard gas distribution systems, normally installed upstream of the regulator closing automatically in the event of temporary interruption of the power supply to the solenoid. Accurate and dependable action, integral push-button type pressure, equalizing valve manual reset, UPCO low pressure loss, and flameproof.
			<br/><br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 230 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;2" to 8"
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_slamshuts_305.gif" alt="Bryan Donkin Pressure Slam Shuts Model 305" title="Bryan Donkin Pressure Slam Shuts Model 305"/></div>
			<div id="PartsContent"><h3>Pressure Slam Shuts<br/><font color="#445679" size="2">Model 305</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>Designed to safeguard gas distribution systems, normally installed upstream of the regulator with the external control line piped to the downstream of the regulator, closing automatically in the event of an overpressure condition. Fully enclosed color coded valve position indicator, integral push-button type pressure equalizing valve, manual reset, low pressure loss.
			<br/><br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 230 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;2" to 8"
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_reliefvalve_201.gif" alt="Bryan Donkin Pressure Relief Valves Model 201" title="Bryan Donkin Pressure Relief Valves Model 201"/></div>
			<div id="PartsContent"><h3>Pressure Relief Valves
			<br/><font color="#445679" size="2">Model 201</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Model 201 is a high pressure relief valve,direct-acting, diaphragm operated.
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&#190;" and 1"
			<br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 375 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/></p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_reliefvalve_225LP.gif" alt="Bryan Donkin Pressure Relief Valves Model 225LP" title="Bryan Donkin Pressure Relief Valves Model 225LP"/></div>
			<div id="PartsContent"><h3>Pressure Relief Valves
			<br/><font color="#445679" size="2">Model 225LP</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description :</b></font> The Model 201 is a high pressure relief valve,direct-acting, diaphragm operated.
			<br/><br/><font color="#424242" size="1"><b>Threaded Size :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&#190;" and 1"
			<br/><font color="#424242" size="1"><b>Pressure :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 30 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 201_225 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 201_225 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_reliefvalve_225LP2.gif" alt="Bryan Donkin Pressure Relief Valves Model 225LP2" title="Bryan Donkin Pressure Relief Valves Model 225LP2"/></div>
			<div id="PartsContent"><h3>Pressure Relief Valves
			<br/><font color="#445679" size="2">Model 225LP2</font></h3>
			<p><br/><font color="#424242" size="1"><b>Description : </b></font>The Series 225 has three versions of low pressure relief valves, which are direct-acting, diaphragm operated. The 225LP2 has the middle inlet pressure rating of the three models.
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&#190;" and 1"
			<br/><font color="#424242" size="1"><b>Pressure :</b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 60 PSIG
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 201_225 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 201_225 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_reliefvalve_225LP4.gif" alt="Bryan Donkin Pressure Relief Valves Model 225LP4" title="Bryan Donkin Pressure Relief Valves Model 225LP4"/></div>
			<div id="PartsContent"><h3>Pressure Relief Valves<br/><font color="#445679" size="2">Model 225LP4</font></h3>
			<p><br/><font color="#424242" size="1" ><b>Description : </b>
			</font>The Series 225 has three versions of low pressure relief valves, which are direct-acting, diaphragm operated. 
			The 225LP4 has the highest inlet pressure rating of the three models.
			<br/><br/><font color="#424242" size="1"><b>Threaded Size : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1-&#188;"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-1/2"
			<br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2"
			<br/><font color="#424242" size="1"><b>Pressure : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Up to 150 PSIG
			<br/><font color="#424242" size="1"><b>Flanged Sizes : </b></font> &nbsp;&nbsp;&nbsp;&nbsp;1" and 2" 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 201_225 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 201_225 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/product_regulator_highseries_200.gif" alt="Bryan Donkin High Pressure Regulators Series 200" title="Bryan Donkin High Pressure Regulators Series 200"/></div>
			<div id="PartsContent"><h3>High Pressure Regulators
			<br/><font color="#445679" size="2">Series 200</font></h3>
			<p><br/><font color="#424242" size="1"><b>Max Inlet :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1000 PSIG
			<br/><font color="#424242" size="1"><b>Outlet Range :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3 to 175 PSIG
			<br/><font color="#424242" size="1"><b>Body Sizes :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#190;" and 1"
			<br/><font color="#424242" size="1"><b>Cross Ref :</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sensus 046, 141-A
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fisher 627, 630
			<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Actaris VR 75
			<br/><br/><font color="#424242" size="1"><b>Description :</b></font> Series 200 are high pressure, first-cut regulators, also known as "Farm Taps" ideally designed for multi-stage pressure reduction applications, active/monitor configurations and as a by-pass regulator for pressure reducing stations.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<a href="http://bryandonkinusa.com/download/MMX/Literature/Model 200 Regulator 2010 Technical Bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Technical Bulletin</b></font></a>
			<br/><a href="http://bryandonkinusa.com/download/MMX/Configurators/Model 200 Regulator Configurator REV_2010-01.pdf" target="_blank"><font color="#ACB0C3"><b>Product Configurator</b></font></a></p>
			</div></div> 
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_highseries_200_Thumbnail.gif" border="0" alt="Bryan Donkin High Pressure Regulators Series 200" title="Bryan Donkin High Pressure Regulators Series 200" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_highseries_273pilotloaded_Thumbnail.gif" border="0" alt="Bryan Donkin High Pressure Regulators Series 273 Pilot Loaded" title="Bryan Donkin High Pressure Regulators Series 273 Pilot Loaded" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_highseries_280_Thumbnail.gif" border="0" alt="Bryan Donkin High Pressure Regulators Series 280/281" title="Bryan Donkin High Pressure Regulators Series 280/281" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_highseries_284_Thumbnail.gif" border="0" alt="Bryan Donkin High Pressure Regulators Series 284/285" title="Bryan Donkin High Pressure Regulators Series 284/285" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_mediumpressure_240_Thumbnail.gif" border="0" alt="Bryan Donkin Medium Pressure Regulators Series 240 and 240PL" title="Bryan Donkin Medium Pressure Regulators Series 240 and 240PL" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_mediumpressure_260_Thumbnail.gif" border="0" alt="Bryan Donkin Medium Pressure Regulators Series 260" title="Bryan Donkin Medium Pressure Regulators Series 260" /></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_mediumpressure_274_Thumbnail.gif" border="0" alt="Bryan Donkin Medium Pressure Regulators Series 274" title="Bryan Donkin Medium Pressure Regulators Series 274" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_lowpressure_225_Thumbnail.gif" border="0" alt="Bryan Donkin Low Pressure Regulators Series 225" title="Bryan Donkin Low Pressure Regulators Series 225" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_regulator_lowpressure_226_Thumbnail.gif" border="0" alt="Bryan Donkin Low Pressure Regulators Series 226" title="Bryan Donkin Low Pressure Regulators Series 226" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_rotarymeter_Thumbnail.gif" border="0" alt="Bryan Donkin Rotary Meters" title="Bryan Donkin Rotary Meters" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_turbine_Thumbnail.gif" border="0" alt="Bryan Donkin Turbine Meters" title="Bryan Donkin Turbine Meters" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_vortexshedding_Thumbnail.gif" border="0" alt="Bryan Donkin Vortex Shedding Meter" title="Bryan Donkin Vortex Shedding Meter" /></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_ultrasonic_Thumbnail.gif" border="0" alt="Bryan Donkin Ultra Sonics Meter" title="Bryan Donkin Ultra Sonics Meter" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_metercorrector_Thumbnail.gif" border="0" alt="Bryan Donkin Meter Correctors" title="Bryan Donkin Meter Correctors" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/590_rollchek_Thumbnail.gif" border="0" alt="Bryan Donkin FM-Approved Check Valves Model 590 Rollchek" title="Bryan Donkin FM-Approved Check Valves Model 590 Rollchek" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_290_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 290" title="Bryan Donkin Pressure Slam Shuts Model 290" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_390lp_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 309LP" title="Bryan Donkin Pressure Slam Shuts Model 309LP" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_309lp2_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 309LP2" title="Bryan Donkin Pressure Slam Shuts Model 309LP2" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_309lp4_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 309LP4" title="Bryan Donkin Pressure Slam Shuts Model 309LP4" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_303_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 303" title="Bryan Donkin Pressure Slam Shuts Model 303" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_304_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 304" title="Bryan Donkin Pressure Slam Shuts Model 304" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_slamshuts_305_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Slam Shuts Model 305" title="Bryan Donkin Pressure Slam Shuts Model 305" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_reliefvalve_201_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Relief Valves Model 201" title="Bryan Donkin Pressure Relief Valves Model 201" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_reliefvalve_225LP_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Relief Valves Model 225LP" title="Bryan Donkin Pressure Relief Valves Model 225LP" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_reliefvalve_225LP2_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Relief Valves Model 225LP2" title="Bryan Donkin Pressure Relief Valves Model 225LP2" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Bryan_Donkin/thumbnails/product_reliefvalve_225LP4_Thumbnail.gif" border="0" alt="Bryan Donkin Pressure Relief Valves Model 225LP4" title="Bryan Donkin Pressure Relief Valves Model 225LP4" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>


