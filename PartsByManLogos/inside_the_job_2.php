<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link=#445679 vlink=#445679>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color=#494A4A><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>


<script>
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php"div id="Logo"></a>
<a href="idnex.php"div id="Tagline">to ALL your process heating & combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php"div id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="index.php"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Packaged Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
		</ul>
	</li>
	<li><a href="parts_line_card.php"><b>Part Sales</b></a></li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Inside the Job</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<div id="InsideTheJobLeftTxt">ETTER Engineering's Inside the Job offers you an insight into our most recent projects. Inside the Job is our way of providing you with the intricate details and workings that go into some of the most impressive ETTER products. As well as providing you with the success stories of many satisfied ETTER customers.
<br><br><br>
<font size= 2 color=#D21D1F><b>Podojil Builders, Inc</font><br><font size= 1><a href="inside_the_job.php">- IHOP</font></a>
<br><br><font size= 2 color=#D21D1F>JPKennedy & Sons</font><br><font size= 1><a href="inside_the_job_2.php">- Boston Fish Pier</font></a>
<br><br><font size= 2 color=#D21D1F>The Nutmeg Companies, Inc</font><br><font size= 1><a href="inside_the_job_3.php">- Connecticut Forensic Investigators.</a></b></font></div>
<div id="InsidetheJobBoston"><div id="InsideFishPierPhoto"></div>
<div id="PJKennedyLogo"></div>
<div id="Boston"><b>Boston, Massachuetts - </b></div>
<div id="BostonTransPhoto"></div>
<div id="BostonTransPhoto2"></div>
<BLOCKQUOTE>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<p></p><br><div id="TextIndent">
For ETTER Engineering, providing a natural gas booster to the oldest continually operating active port in the Western Hemisphere is just another day on the job.
<br><br>
When Boston area mechanical contractors PJ Kennedy & Sons was awarded the contract to </div><br><br>upgrade the Boston Fish Pier's IT/UPS facilities - a job which required a natural gas booster to provide fuel for the facility's emergency backup generator - ETTER Engineering seemed to be a natural fit for the task. According to PJ Kennedy's Project Manager Bob Collins, "Working with ETTER Engineering was a positive experience throughout the project!"
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div id="PhotoCaptionBostonOne"><font size=1><b>ENGB gasPOD&#0153; in grey outdoor transclosure feeding emergency backup generator (green transclosure).</b></font></div>
<br>Utilizing the company's UL-Listed ENGBP gasPOD&#0153; (Gas Pressure On Demand) packaged gas booster system, ETTER delivered a complete "Plug & Play" package to the job site, making PJ Kennedy's job that much easier. Arriving on-site fully piped, wired and contained within an outdoor steel transclosure, the Boston Fish Pier gas booster required minimal installation time before it was up and running.
<br><br>
The ENGBP gasPOD&#0153; packaged gas booster system ETTER delivered will boost the incoming line pressure up 6.9" W.C. over the incoming supply for flows up to 3,400 SCFH, which is enough to ensure all the power needs for the new Massport IT/UPS room are met during a power failure.
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<div id="PhotoCaptionBostonTwo"><font size=1><b>ENGB2 with piping, wiring, and control panel inside outdoor steel transclosure.</b></font></div>
<br>
Given the importance of the Port of Boston to the United States economy, the gravity of the job was noted early on by ETTER's engineers and field personnel, and the focus on getting the job done right did not escape the notice of JP Kennedy's project manager. "Your field staff (Norm Myers) should be commended for his professionalism and commitment to excellence, and ultimately for the quality of the end product being delieverd," Collins said.
<br><br>
The gas booster installation was part of a larger project, part which was supported by a $100,000 grant provided by the Massachusetts Department of Environmental Protection to reduce air pollution and increase electric power for boats docked at the Boston Fish Pier. Prior to this project, the facility was too small to dock the growing number of fishing boats. Since the boats had their fresh catch on board they could not be directly docked and patched in to landline power, it was necessary to keep their diesel engines running to support their refrigeration systems on board. When completed later this year, the upgrades to the facility will produce a 95 percent reduction in engine idling and air pollution. Now this is a "green project" ETTER is proud to be a part of!
<br><br><center><h5><BLOCKQUOTE>
To learn more about how ETTER Engineering can assist you on your next project, call 1-800-444-1962.</h5></center>
</BLOCKQUOTE></BLOCKQUOTE></div>
<a href="inside_the_job_3.php" div id="InsideJobNextBTN">Next</a>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.html" div id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" div id="TermsofService">Terms of Service</a>
<a href="site_map.php" div id="SiteMap">Site Map</a></div>
</div>
</body>
</html>