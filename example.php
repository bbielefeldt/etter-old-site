<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE 7]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"> </script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7328693-3']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>

<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript">
var timeout         = 500;
var closetimer		= 0;
var ddmenuitem      = 0;

function dropdown_open()
{	dropdown_canceltimer();
	dropdown_close();
	ddmenuitem = $(this).find('ul').eq(0).css('visibility', 'visible');}

function dropdown_close()
{	if(ddmenuitem) ddmenuitem.css('visibility', 'hidden');}

function dropdown_timer()
{	closetimer = window.setTimeout(jsddm_close, timeout);}

function dropdown_canceltimer()
{	if(closetimer)
	{	window.clearTimeout(closetimer);
		closetimer = null;}}

$(document).ready(function()
{	$('#dropdown > li').bind('mouseover', dropdown_open);
	$('#dropdown > li').bind('mouseout',  dropdown_timer);});

document.onmouseout = dropdown_close;
</script>
<script type="text/javascript">
$(document).ready(function() {
   // put all your jQuery goodness in here.
   // Do not instantiate elements until the document has finished loading
 
	//jQuery Easy Accordion 
	$('.accordion-list:not(:first)').hide();
 
	$('.accordion-list:first').show();
 
	$('.accordion-header:first').addClass('active');	
	$('.accordion-list:first').addClass('content-active');
 
	$('.accordion-header').click(function () {
		$('.accordion-list:visible').slideUp().prev().removeClass('active');
		$('.accordion-list:visible').removeClass('content-active');
		$(this).addClass('active').next().slideDown();
		$('.accordion-list').addClass('content-active');
 
	});
});
</script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>

<script type="text/javascript">

$(document).ready(function() {
   // put all your jQuery goodness in here.
   // Do not instantiate elements until the document has finished loading

	//Accordion functions
	$('.accordion-list:not(:first)').hide();
	
	$('.accordion-list:first').show();
	
	$('.accordion-header:first').addClass('active');	
	$('.accordion-list:first').addClass('content-active');
	
	$('.accordion-header').click(function () {
		$('.accordion-list:visible').slideUp().prev().removeClass('active');
		$('.accordion-list:visible').removeClass('content-active');
		$(this).addClass('active').next().slideDown();
		$('.accordion-list').addClass('content-active');
		
	});
});
</script>
</head>
<body>

<div id="Wrapper">
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>

<a href="index.html"div id="Logo"></a>



<a href="index.html"div id="Tagline">to ALL your process heating & combustion needs!</a>      
<!--Logo / Logo Link End--> 

<a href="contact_us.html"div id="ContactButton"><b>Contact Us</b></a>
   
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="OVENS.php">Ovens and Furnaces</a></li>
			<li><a href="DRYING.php">Web Drying</a></li>
			<li><a href="HEATERS.php">Packaged Heaters</a></li>
			<li><a href="Panels.php">Control Panels</a></li>
		</ul>
	</li>
	<li><a href="parts_line_card.php"><b>Part Sales</b></a></li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Literature</b></a>
	</li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>







   
<div id="LiteratureMenu">
		<div class="accordionButton"><b>Product Literature</b></div>
		<div class="accordionContent"><BLOCKQUOTE>Gas Booster Literature
					<div class="accordionBContent"><BLOCKQUOTE>gasPOD - Packaged Gas Boosters
								       <br>ENGB - Standalone Gas Booster
								       <br>E101P - 
								       <br>E101PHC - High Capacity 
								       <br>E101PHC-Xtra - Ultimate </BLOCKQUOTE></div>
					<br>Packaged Heat Literature
					<br>Ovens and Furnaces Literature
					<br>Web Drying Literature
					<br>Control Panel Literature</BLOCKQUOTE></div>
		<div class="accordionButton"><b>Safety Literature</b></div>
		<div class="accordionContent">Content 2<br /><br /><br /><br /><br />Medium Example</div>
		<div class="accordionButton"><b>Company Literature</b></div>
		<div class="accordionContent">Content 1<br />Short Example</div>
		<div class="accordionButton"><b>All Literature</b></div>
		<div class="accordionContent">Content 4<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />Extra Long Example</div>
	</div>


<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>

<a href="privacy_policy.html" div id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.html" div id="TermsofService">Terms of Service</a>
<a href="site_map.html" div id="SiteMap">Site Map</a></div>
</div>
</body>
</html>
