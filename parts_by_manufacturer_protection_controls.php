<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Protection Controls combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Protection Controls,Ignitifier I Form 5909N,Ignitifier II 6954,Ignitifier III 7759,P-C II Ultra-Violet Scanner,Protectal Unified Control Panels,Protectofier Form 6642 Combustion Safeguards,Protectofier 7256 Combustion Safeguards,Tele-Fault II 8966 First-Outage Fault Finder"/>
<title>ETTER Engineering - Protection Controls Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="ProtectionLogoLarge"></div>
<div id="SensusText">
<br/><br/>Protection Controls, Inc. is a highly specialized company with over 40 years of 
experience in flame safety control. Our product line includes many plug-in components and 
controls that are interchangeable with existing systems. The systems are designed with high 
signal strength to provide safe and secure operation while reducing the potential for system 
shutdown due to noise.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Ignitifier-I-5909N.gif" alt="Protection Controls Ignitifier I Form 5909N" title="Protection Controls Ignitifier I Form 5909N"/></div>       
			<div id="PartsContent"><h3>Ignitifier I Form 5909N</h3>
    			<p>Motor-driven Multi-Burner igniter control. Automatically fires up to 40 burners in seconds!
			<br/><br/><b>Features:</b>
			<br/><br/><b>Improved Safety and Performance:</b>
			<br/>One unit automatically fires up a complete network of gas burners in ovens, 
			furnaces, boilers and heat treating systems.
			<br/><br/><b>High-Speed Spark Ignition:</b>
			<br/>40 burners within 3 seconds
			<br/><br/><b>No Spark Gap Loss:</b>
			<br/>Positive contact-wiper switching design eliminates arcing gap and wasted 
			energy. - puts full voltage spark at burner!
			<br/><br/><b>Interchangeable Components:</b>
			<br/>When systems are enlarged, Rotor unit and spark electrode end plates can 
			be replaced with higher capacity units to accommodate additional burners.
			<br/><br/><b>How it works</b>
    			<br/>When the Ignitifier is energized by a START button, a motorized double-end 
			shaft rotates its wiping contact against a series of stationary contact tips 
			(one for each Ignitor in the system). This causes a high voltage impulse from an 
			ignition transformer to be fed to each burner spark electrode, and immediately 
			ignites the gas-air mixture of each burner position (gas-air mixture has been 
			simultaneously released with actuation of the START button). The unit repeats 
			until all burners are ignited.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/IGNI5909N.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Ignitifier I 5909N Brochure</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Ignitifier-II-6954.gif" alt="Protection Controls Ignitifier II 6954" title="Protection Controls Ignitifier II 6954"/></div>
			<div id="PartsContent"><h3>Ignitifier II 6954</h3>
    			<p>Solid-State Multiple Ignition System. 
			<br/><br/><b>Features One-Wire Distribution:</b> 
			<br/>One single compact unit ignites up to 30 (or more) spark ignited gas burners 
			all at the same time and instaneously. 
			<br/><br/><b>Features:</b>
			<br/>&#149; All Electric
			<br/>&#149; Instantaneous
			<br/>&#149; Dependable
			<br/>&#149; Low Primary Current for many burners
			<br/>&#149; No need for special high current switches, 
             		<br/>&nbsp;&nbsp;contactors or power supply
			<br/><br/><b>No mechanical action</b>
			<br/>No need for multiplicity of complex high voltage wiring; requires only single wire service to electrical box or similar connections. For ovens, furnaces, boilers, etc. 
			<br/><br/><b>Compare to rotary and other mechanical systems:</b>
			<br/>&#149; Solid-state dependability, compactness
			<br/>&#149; Can be installed for continuous operation
			<br/>&#149; No moving parts, no mechanical wear
			<br/>&#149; Instant, simultaneous sparking at all 
             		<br/>&nbsp;&nbsp;points
			<br/>&#149; Ignites unbalanced mixtures more 
             		<br/>&nbsp;&nbsp;effectively
			<br/>&#149; Saves on equipment installation time
			<br/>&#149; Less space, less wiring, less conduit
			<br/>&#149; Maintenance practically eliminated
			<br/>&#149; Simple, quality components
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/IGNII6954.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Ignitifier II 6954 Brochure</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Ignitifier-III-7759.gif" alt="Protection Controls Ignitifier III 7759" title="Protection Controls Ignitifier III 7759"/></div>
			<div id="PartsContent"><h3>Ignitifier III 7759</h3>
    			<p>Features Spark Blind Ignitor (single burner)
			<br/><br/>Form 7759 Ignitifier III Spark Blind Ignitor may be used with all 
			models of Protectofier combustion safeguards, operating in conjuction with P-CII 
			ultraviolet scanners to avoid false sensing indication resulting from ignition
			spark. The Ignitifier III is to be used as a source of electric spark only and is not
			intended for use as a safety device.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/IGNIII7759.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Ignitifier III 7759 Brochure</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/P-CII.gif" alt="Protection Controls P-C II Ultra-Violet Scanner" title="Protection Controls P-C II Ultra-Violet Scanner"/></div>
			<div id="PartsContent"><h3>P-C II
			<br/><font size="2" color="#50658D">Ultra-Violet Scanner</font></h3>
    			<p>Detects any flame which contains ultra-violet
			<br/><br/><b>Features:</b>
			<br/>&#149; High flame signal
			<br/>&#149; Can be mounted in any position
			<br/>&#149; Encapsulated sensor tube permanently 
             		<br/>&nbsp;&nbsp;sealed in gas tight housing
			<br/>&#149; Operates with gas and/or oil flames
			<br/><br/><b>P-C II</b>
			<br/>&#149; Basic model.
			<br/>&#149; Provided with an electrical connector for 
             		<br/>&nbsp;&nbsp;flexible metal conduit (3/8" max.)
			<br/><br/><b>P-C II W</b>
			<br/>&#149; Provided with 1/2" straight liquid-tite 
             		<br/>&nbsp;&nbsp;electrical connector.
			<br/><br/><b>P-C II WRA</b>
			<br/>&#149; Provided with a 1/2" right angle liquid-tite 
             		<br/>&nbsp;&nbsp;electrical connector.
   			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/PCII.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>P-C II Brochure</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Protectal-Unified-Control-Panels.gif" alt="Protection Controls Protectal Unified Control Panels" title="Protection Controls Protectal Unified Control Panels"/></div>
			<div id="PartsContent"><h3>Protectal Unified Control Panels
			<br/><font size="2" color="#50658D">Single and Multi-burner Packaged Combustion Protection</font></h3>
    			<p><b>Protectal Unified Features</b>
			<br/>&#149; Ready to install
			<br/>&#149; No additional components necessary
			<br/>&#149; Prewired for immediate operation
			<br/><br/><b>Protectal Unified 62019</b>
			<br/>&#149; Form 6642VA Protectofier*
			<br/>&#149; Plug-in solid state electronic FLAME-PAK
			<br/>&#149; Plug-in ACF relays
			<br/>&#149; START pushbutton
			<br/>&#149; Signal light to indicate flame
			<br/>&#149; Ignition transformer (optional)
			<br/>&#149; Dust-tight steel enclosure
			<br/><br/> *Available options include Form "B" Protectofier for automatic operation and safety requirements such as non-relight, low fire start, and interrupted pilot.
			<br/><br/><b>Protectal Unified 62021</b>
			<br/>&#149; Form 6642VT Protectofier with plug-in 
             		<br/>&nbsp;&nbsp;TIMOFIER for purge and ignition trial, 
             		<br/>&nbsp;&nbsp;plug-in solid state electronic FLAME-PAK 
             		<br/>&nbsp;&nbsp;and plug-in ACF relays
			<br/>&#149; Burner Off/Run/Ignition switch
			<br/>&#149; Signal light to indicate flame
			<br/>&#149; Signal light to indicate timer operation
			<br/>&#149; Ignition transformer (optional)
			<br/>&#149; Dust-tight steel enclosure
			<br/><br/>*Available options include Form "B" Protectofier for automatic operation and safety requirements such as non-relight, low fire start, and interrrupted pilot.
			<br/><br/><b>Standard Complete Unified 82064 and 82065</b>
			<br/>Compact Control Panel with same features as Unified Panel - plus Motor Control
			<br/><br/><b>Complete Unified 63031 and 63032</b>
			<br/>Compact Control Panel with same features as Standard Complete Unified Panel - except customized to perform a wide range of additional control functions.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/Unified.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Protectal Unified Control Panels Brochure</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Protectofier-6642.gif" alt="Protection Controls Protectofier Form 6642 Combustion Safeguards" title="Protection Controls Protectofier Form 6642 Combustion Safeguards"/></div>
			<div id="PartsContent"><h3>Protectofier Form 6642
			<br/><font size="2" color="#50658D">Combustion Safeguards</font></h3>
    			<p>Operate with Flame Rod and/or P-C II Scanner
			<br/><br/><b>6642 Features</b>
			<br/>&#149; Modularized Multi-Burner Control
			<br/>&#149; Time Proven Plug-in Solid State FLAME-PAK 
             		<br/>&nbsp;&nbsp;Amplifier
			<br/>&#149; Plug-in, Interchangeable Relays
			<br/>&#149; Plug-in Power Transformer
			<br/>&#149; Flame Rod and/or Ultra-Violet Sensing. 
             		<br/>&nbsp;&nbsp;Independent or simultaneously using 
             		<br/>&nbsp;&nbsp;same FLAME-PAK Amplifier.
			<br/><br/><b>Form 6642VA</b>
			<br/>&#149; Basic combustion safeguard model
			<br/><br/><b>Form 6642VB</b>
			<br/>&#149; Breaker - lockout switch model
			<br/>&#149; Required for automatic lighted 
             		<br/>&nbsp;&nbsp;installations. System "latches out" after 
             		<br/>&nbsp;&nbsp;failure to light within ignition trial time 
             		<br/>&nbsp;&nbsp;and cannot attempt restart until switch is 
             		<br/>&nbsp;&nbsp;manually reset.
			<br/>&#149; Standard ignition trial time is 15 seconds. 
             		<br/>&nbsp;&nbsp;Also available in 10-second and 5-second 
             		<br/>&nbsp;&nbsp;trial time.
			<br/>&#149; Form 6642VBNR - Automatic only and 
             		<br/>&nbsp;&nbsp;Non-relight.
			<br/><br/><b>Form 6642VL</b>
			<br/>&#149; Load relay - included for all multi-burner 
             		<br/>&nbsp;&nbsp;models
			<br/><br/><b>Form 6642VT</b>
			<br/>&#149; Timofier model plugin, fixed time 
             		<br/>&nbsp;&nbsp;assembly.
			<br/>&#149; The timofier provides purge time and 
             		<br/>&nbsp;&nbsp;ignition trial time. The ignition trial time is 
             		<br/>&nbsp;&nbsp;deducted from the motor time cycle 
             		<br/>&nbsp;&nbsp;(1/2, 1, 1-1/4, 2, 3, 4, and 6 minutes 
             		<br/>&nbsp;&nbsp;available for manual and automatic 
             		<br/>&nbsp;&nbsp;models; 10 minutes for manual model 
             		<br/>&nbsp;&nbsp;only) to purge the time.
			<br/>&#149; Standard ignition trial time is 15 seconds. 
             		<br/>&nbsp;&nbsp;Also available in 10-second and 5-second 
             		<br/>&nbsp;&nbsp;trial time.
			<br/>&#149; Specify both purge and ignition times.
			<br/>&#149; Purge time shall allow system to have a 
             		<br/>&nbsp;&nbsp;minimum of four fresh air changes.
			<br/><br/><b>Form 6642VBL</b>
			<br/>&#149; Combines features of Breaker and Load 
             		<br/>&nbsp;&nbsp;relay models described above.
			<br/>&#149; Form 6642VBLNR - Automatic only and 
             		<br/>&nbsp;&nbsp;Non-relight.
			<br/><br/><b>Form 6642VBT</b>
			<br/>&#149; Combines features of Breaker and 
             		<br/>&nbsp;&nbsp;Timofier models described above.
			<br/>&#149; Form 6642VBTNR - Automatic only and 
             		<br/>&nbsp;&nbsp;Non-relight.
			<br/><br/><b>Form 6642VLT</b>
			<br/>&#149; Combines features of Load relay 
             		<br/>&nbsp;&nbsp;and Timofier models described above.
			<br/><br/><b>Form 6642VBLT</b>
			<br/>&#149; Combines features of Breaker, Load relay, 
             		<br/>&nbsp;&nbsp;and Timofier models described above.
			<br/>&#149; Form 6642VBLTNR - Automatic only and 
             		<br/>&nbsp;&nbsp;Non-relight.
			<br/><br/><b>Form 6642FF</b>
			<br/>&#149; Full Feature Combustion Safeguard 
             		<br/>&nbsp;&nbsp;including:
			<br/>&#149; Proof of Closure
			<br/>&#149; Proof of High Fire Purge
			<br/>&#149; Purge Timing (In 15 sec. increments, 
             		<br/>&nbsp;&nbsp;7 min. 45 sec. max)
			<br/>&#149; Proof of Low Fire Start
			<br/>&#149; Ignition Trial Timing (Pilot) 10 sec.
			<br/>&#149; Pilot Interrupt Timing 
             		<br/>&nbsp;&nbsp;(Main burner ignition trial) 10 or 15 sec.
			<br/>&#149; Alarm output if the safety interlocks 
             		<br/>&nbsp;&nbsp;(limit switches) or flame failure 
             		<br/>&nbsp;&nbsp;causes shutdown.
			<br/>&#149; Relay contacts available to drive actuator 
             		<br/>&nbsp;&nbsp;to high fire for purge, low fire for 
             		<br/>&nbsp;&nbsp;ignition trial, and control when main 
             		<br/>&nbsp;&nbsp;burner is proven.
			<br/>&#149; Eight status lights
 			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/PRO6642V.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Protectofier 6642 Brochure</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VA.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VA</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VB.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VB</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBNR.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBNR</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VL.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VL</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VT.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VT</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBL.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBL</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBLNR.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBLNR </b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBT.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBT</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBTNR.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBTNR </b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VLT.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VLT</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBLT.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBLT</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X6642VBLTNR.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642VBLTNR </b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/bulletins/PRO6642FF.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 6642FF</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Protectofier-7256.gif" alt="Protection Controls Protectofier 7256 Combustion Safeguards" title="Protection Controls Protectofier 7256 Combustion Safeguards"/></div>
			<div id="PartsContent"><h3>Protectofier 7256
			<br/><font size="2" color="#50658D">Combustion Safeguards</font></h3>
    			<p>Operate with Flame Rod and/or P-C II Scanner
			<br/><br/><b>7256 Features</b>
			<br/>&#149; Compact Design - Single Burner
			<br/>&#149; Time Proven Plug-in Solid State FLAME-PAK 
             		<br/>&nbsp;&nbsp;Amplifier
			<br/>&#149; Plug-in, Interchangeable Relays
			<br/>&#149; Plug-in Power Transformer
			<br/>&#149; Flame Rod and/or Ultra-Violet Sensing. 
             		<br/>&nbsp;&nbsp;Independent or simultaneously using 
             		<br/>&nbsp;&nbsp;same FLAME-PAK Amplifier.
			<br/><br/><b>Form 7256-AH/AHE</b>
			<br/>&#149; Basic manual model
			<br/>&#149; "E" indicates enclosed model
			<br/><br/><b>Form 7256-B/BE</b>
			<br/>&#149; Manual or automatic operation
			<br/>&#149; Recycle on flame failure automatic.
			<br/>&#149; Select ignition trial timing cycle: 
             		<br/>&nbsp;&nbsp;15 sec.(standard), 5 sec. or 10 sec.
			<br/>&#149; "E" indicates enclosed model
			<br/><br/><b>Form 7256-BH/BHE</b>
			<br/>&#149; "H" indicates flame failure alarm
			<br/>&#149; Recycle on flame failure automatic.
			<br/>&#149; Select ignition trial timing cycle: 
             		<br/>&nbsp;&nbsp;15 sec.(standard), 5 sec. or 10 sec.
			<br/>&#149; "E" indicates enclosed model
			<br/><br/><b>Form 7256-BNRE/BNRHE</b>
			<br/>&#149; "NR" indicates automatic models with 
             		<br/>&nbsp;&nbsp;additional relay to prevent relight 
             		<br/>&nbsp;&nbsp;attempt on flame failure.
			<br/>&#149; Select ignition trial timing cycle: 
             		<br/>&nbsp;&nbsp;15 sec.(standard), 5 sec. or 10 sec.
			<br/>&#149; "E" indicates enclosed model
			<br/><br/><b>Form 7256-BTNRE/BTNRHE</b>
			<br/>&#149; "T" indicates plug-in "SST" purge timer 
             		<br/>&nbsp;&nbsp;provided for fixed pre-purged cycle. 
             		<br/>&nbsp;&nbsp;Select purge cycle: 15, 30, 60 or 90 sec.
			<br/>&#149; Select ignition trial timing cycle: 
             		<br/>&nbsp;&nbsp;15 sec.(standard), 5 sec. or 10 sec.
			<br/>&#149; "E" indicates enclosed model
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/PRO7256.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Protectofier 7256 Brochure</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X7256B-BH.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 7256-B/BE</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X7256B-BH.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 7256-BH/BHE</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X7256BNR.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 7256-BNRE/BNRHE</b></font></a>
			<br/><a href="http://www.protectioncontrolsinc.com/drawing/X7256BTN.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Form 7256-BTNRE/BTNRHE</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Tele-Fault-II-8966.gif" alt="Protection Controls Tele-Fault II 8966 First-Outage Fault Finder" title="Protection Controls Tele-Fault II 8966 First-Outage Fault Finder"/></div>       
			<div id="PartsContent"><h3>Tele-Fault II 8966
			<br/><font size="2" color="#50658D">First-Outage Fault Finder</font></h3>
    			<p>Unequalled reliability and operating savings for monitoring equipment in heating and processing systems. Monitors up to 8 read-out positions.
			<br/><br/><b>Features</b>
			<br/>&#149; Single Wire Connections
			<br/>&#149; No Additional Relays
			<br/>&#149; Compact Design 1/4 Din Size
			<br/>&#149; Start-Up Status Display
			<br/>&#149; First-Outage Readout
			<br/>&#149; Power Interruption Indicator
			<br/><br/><b>Lights and Pushbuttons</b>
			<br/>&#149; Strart-up Lights indicate all monitored 
             		<br/>&nbsp;&nbsp;open limits when power is applied or when 
             		<br/>&nbsp;&nbsp;reset button is pressed.
			<br/>&#149; First-out Lights indicate first monitored 
             		<br/>&nbsp;&nbsp;limit to open.
			<br/>&#149; Silence alarm button de-energizes 
             		<br/>&nbsp;&nbsp;first-out alarm circuit.
			<br/>&#149; Reset Required light indicates Tele-Fault II 
             		<br/>&nbsp;&nbsp;requires reset.
			<br/>&#149; Power Interruption light indicates 115V 
             		<br/>&nbsp;&nbsp;power supply has been interrupted.
			<br/>&#149; Reset light indicates Tele-Fault II is 
             		<br/>&nbsp;&nbsp;operational.
			<br/>&#149; Reset button silences alarm and resets 
             		<br/>&nbsp;&nbsp;Tele-Fault II.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/TeleII8966.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Tele-Fault II 8966 Brochure</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Protection-Controls/Ignitifier-I-5909N.gif" alt="Protection Controls Ignitifier I Form 5909N" title="Protection Controls Ignitifier I Form 5909N"/></div>       
			<div id="PartsContent"><h3>Ignitifier I Form 5909N</h3>
    			<p>Motor-driven Multi-Burner igniter control. Automatically fires up to 40 burners in seconds!
			<br/><br/><b>Features:</b>
			<br/><br/><b>Improved Safety and Performance:</b>
			<br/>One unit automatically fires up a complete network of gas burners in ovens, 
			furnaces, boilers and heat treating systems.
			<br/><br/><b>High-Speed Spark Ignition:</b>
			<br/>40 burners within 3 seconds
			<br/><br/><b>No Spark Gap Loss:</b>
			<br/>Positive contact-wiper switching design eliminates arcing gap and wasted 
			energy. - puts full voltage spark at burner!
			<br/><br/><b>Interchangeable Components:</b>
			<br/>When systems are enlarged, Rotor unit and spark electrode end plates can 
			be replaced with higher capacity units to accommodate additional burners.
			<br/><br/><b>How it works</b>
    			<br/>When the Ignitifier is energized by a START button, a motorized double-end 
			shaft rotates its wiping contact against a series of stationary contact tips 
			(one for each Ignitor in the system). This causes a high voltage impulse from an 
			ignition transformer to be fed to each burner spark electrode, and immediately 
			ignites the gas-air mixture of each burner position (gas-air mixture has been 
			simultaneously released with actuation of the START button). The unit repeats 
			until all burners are ignited.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.protectioncontrolsinc.com/bulletins/IGNI5909N.pdf" target="_blank"><font size="1" color="#ACB0C3"><b>Ignitifier I 5909N Brochure</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Ignitifier-I-5909N-Thumbnail.gif" border="0" alt="Protection Controls Ignitifier I Form 5909N" title="Protection Controls Ignitifier I Form 5909N"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Ignitifier-II-6954-Thumbnail.gif" border="0" alt="Protection Controls Ignitifier II 6954" title="Protection Controls Ignitifier II 6954"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Ignitifier-III-7759-Thumbnail.gif" border="0" alt="Protection Controls Ignitifier III 7759" title="Protection Controls Ignitifier III 7759"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/P-CII-Thumbnail.gif" border="0" alt="Protection Controls P-C II Ultra-Violet Scanner" title="Protection Controls P-C II Ultra-Violet Scanner"/></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Protectal-Unified-Control-Panels-Thumbnail.gif" border="0" alt="Protection Controls Protectal Unified Control Panels" title="Protection Controls Protectal Unified Control Panels"/></font></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Protectofier-6642-Thumbnail.gif" border="0" alt="Protection Controls Protectofier Form 6642 Combustion Safeguards" title="Protection Controls Protectofier Form 6642 Combustion Safeguards"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Protectofier-7256-Thumbnail.gif" border="0" alt="Protection Controls Protectofier 7256 Combustion Safeguards" title="Protection Controls Protectofier 7256 Combustion Safeguards"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Protection-Controls/thumbnails/Tele-Fault-II-8966-Thumbnail.gif" border="0" alt="Protection Controls Tele-Fault II 8966 First-Outage Fault Finder" title="Protection Controls Tele-Fault II 8966 First-Outage Fault Finder"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>


