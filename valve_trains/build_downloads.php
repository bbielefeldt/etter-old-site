<style type="text/css">
.excel-icon{
	background: url('../images/pdf-icon.png') no-repeat;
	padding: 0 0 0 27px;
	overflow: visible;
}
.build-list{
	font-family: Arial, Helvetica, sans serif;
	color:#cf0d29;
	font-size:14px;
	font-weight:bold;
}
.build-list a{
	font-weight: normal;
	font-size:12px;
}
ul li {
	list-style-type: none;
	margin: 0 0 20px 0;
	min-height: 20px;
}
ul li a{
	color: #475578;
	text-decoration: none;
}
</style>

<div class="build-list">
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 1000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT1000_configurator_REV02.pdf" target="_blank">Download EVT 1000 Configurator</a></li>
	</ul>
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 2000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT2000_configurator_REV02.pdf" target="_blank">Download EVT 2000 Configurator</a></li>
	</ul>
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 3000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT3000_configurator_REV02.pdf" target="_blank">Download EVT 3000 Configurator</a></li>
	</ul>

	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 4000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT4000_configurator_REV02.pdf" target="_blank">Download EVT 4000 Configurator</a></li>
	</ul>
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 5000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT5000_configurator_REV02.pdf" target="_blank">Download EVT 5000 Configurator</a></li>
	</ul>
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">EVT 6000</h2>
	<ul>
	<li class="excel-icon"><a href="../valve_trains/docs/build_files/EVT6000_configurator_REV02.pdf" target="_blank">Download EVT 6000 Configurator</a></li>
	</ul>

</div>