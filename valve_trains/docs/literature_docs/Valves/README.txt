NOTE: In the configurator, we list "Honeywell Modular" as a valve, which corresponds to the 164 pg. piece of literature in this folder.

This should be more specific if at all possible - it's just not practical to be downloading a 164 pg. "general catalog" type of literature for one component.