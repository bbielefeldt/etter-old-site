<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-8000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Valve Train for liquid fuel oil (light oil), that is intended to satisfy the requirements of the Main Burner pilot, and includes the fuel safety interlocks (Oil pressure switches) and Safety Shut-off valves, but not the fuel feed equipment, for applications that are covered by NFPA 85. These are designed for retrofit or new applications where an existing fuel delivery system is either new or being replaced. Sizes are offered to cover most applications for the Single and Multiple Boiler Burners covered by NFPA 86 where a Pilot Fuel Train is required based on the burner design criteria, utilizing a Class 2 or Class 3 Igniter design. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>