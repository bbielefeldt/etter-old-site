<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">ERM- Regulator Module</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a pressure regulating device for either gas or liquid fuel oil, and is intended as part of the complete Valve Train assemblies for the EVT-3000, 6000, or 9000 series assemblies, to satisfy either the NFPA 85 or 86 requirements. The specific inlet and outlet pressure, fuel flow rate, and turn-down requirements will be used to determine the component selection. Additionally, the location and installation will be dependent on the fuel type and application. </p>


</div>