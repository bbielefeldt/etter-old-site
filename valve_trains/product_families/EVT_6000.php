<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-6000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Complete Valve Train assembly for natural or Propane gas, that includes the gas safety interlocks (Gas pressure Switches), Safety Shut-off valves, and the gas feed equipment including the strainer and drip leg, for applications that are covered by NFPA 85. Provisions are made for the regulating, metering, and flow control devices. These are designed for retrofit or new applications where an existing fuel delivery system is either new or being replaced. Sizes are offered to cover most applications for the Single and Multiple Boiler Burners covered by NFPA 85. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>