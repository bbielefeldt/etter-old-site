<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-3000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Complete Valve Train assembly for natural or Propane gas, that includes the gas safety interlocks (Gas pressure Switches), Safety Shut-off valves, and the gas feed equipment including the strainer and drip leg, for applications that are covered by the NFPA 86 Guideline. Provisions are made for the regulating, metering, and flow control devices. These are designed for retrofit or new applications where an existing fuel delivery and control systems are either new or being replaced. Sizes are offered to cover most applications in the equipment families covered by NFPA 86. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>