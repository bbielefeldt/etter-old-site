<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-4000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Valve Train module for natural or Propane gas that includes the gas interlocks (Gas Pressure Switches) and safety Shut-off valves, but not the gas delivery, regulating, or flow control equipment, for applications that are covered by NFPA 85. These are designed for typical retrofit applications where existing fuel delivery systems are already in place, but not the machine specific valves and switches. Sizes are offered to cover most applications for the Single and Multiple Boiler Burners covered by NFPA 85. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>