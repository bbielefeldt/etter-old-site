<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-2000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Valve Train assembly for natural or Propane gas that is intended to satisfy the requirements of the Main Burner pilot, and includes the Safety Shut-off valves and pressure regulating equipment, but not the gas delivery or flow control equipment, for applications that are covered by the NFPA 86 Guideline. These are designed for retrofit or new applications where existing fuel delivery systems is either new or being replaced. Sizes are offered to cover most applications in the equipment families covered by NFPA 86 where a Pilot Fuel Train is required based on the burner design criteria. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>