<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">EVT-5000 Series</span></h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
This is a Valve Train assembly for natural or Propane gas that is intended to satisfy the requirements of the Main Burner pilot, and includes the Safety Shut-off valves and pressure regulating equipment, but not the gas delivery or flow control equipment, for applications that are covered by NFPA 85. These are designed for retrofit or new applications where an existing fuel delivery system is either new or being replaced. Sizes are offered to cover most applications for the Single and Multiple Boiler Burners covered by NFPA 85 where a Pilot Fuel Train is required based on the burner design criteria, utilizing a Class 2 or Class 3 Igniter design. All components meet the listing requirements, design criteria’s, and are fully assembled and tested prior to shipping.</p>


</div>