<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Valve Train Product Families</title>
<meta name="keywords" content="valve train product families, nfpa, fuel train, pilot trains, evt, ETTER Engineering, boilers, natural gas, propane gas, oil, " />
<meta name="description" content="Learn about ETTER Engineering’s families of valve trains including the guidelines they satisfy, included equipment, applications, and more."/>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="../ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="../ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="../all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="../all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="../includes/javascript.js"> </script>
<script type="text/javascript" src="../includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="../includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type= "text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="NavBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("../mainnav.php"); ?>

<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="ENGBBoosterTransBLK"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SpectrumSolidWhiteBkgrd"></div>

<!-- 
<div id="ENGBSlideshow">
<div class="IndexSlideshow" > 
    <img src="../Picture_957.gif" width="399" height="433" alt="Valve Trains"/> 
    <img src="../slideshow_Valve_Train_Photo.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-3.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-4.gif" width="399" height="433" alt="Valve Trains"/> 
</div> 
</div> -->

<div id="ControlPanel" style="margin-top:-75px;overflow:scroll;width:245px;height:433px;">
	
<div style="width:215px;">

<h2 style="color:#ffffff;font-weight:bold;font-size:13px;">Valve Train Product Families</h2>

<p style="font-size:11px;width:215px;color:#ffffff;">ETTER Engineering has developed a line of Fuel Train that specifically meets the two different guidelines, depending on the equipment you have. These include both Main and Pilot trains, for both gaseous and liquid fuels. While these families are standard products, there are many applications that fall outside of these, and ETTER is ready to help design, and built these systems as well. Our standard offerings include:</p>

<a href="../valve_trains/product_families/EVT_1000.php" target="product_families" style="color:#ffffff;">EVT-1000 Series is a Valve Train Module for natural or propane gas that satisfies NFPA-86 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_2000.php" target="product_families" style="color:#ffffff;">EVT-2000 Series is a Pilot Valve Train for natural or propane gas that satisfies NFPA-86 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_3000.php" target="product_families" style="color:#ffffff;">EVT-3000 Series is a Complete Valve Train assembly for natural or propane gas that satisfies NFPA-86 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_4000.php" target="product_families" style="color:#ffffff;">EVT-4000 Series is a Valve Train Module for boilers utilizing natural or propane gas, and satisfies NFPA-85 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_5000.php" target="product_families" style="color:#ffffff;">EVT-5000 Series is a Pilot Valve Train for boilers utilizing natural or propane gas, and satisfies NFPA-85 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_6000.php" target="product_families" style="color:#ffffff;">EVT-6000 Series is a Complete Valve Train for boilers utilizing natural or propane gas, and satisfies NFPA-85 Guidelines.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_7000.php" target="product_families" style="color:#ffffff;">EVT-7000 Series is a Valve Train module for boilers utilizing light oil, and satisfies NFPA-85.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_8000.php" target="product_families" style="color:#ffffff;">EVT-8000 Series is a Pilot Valve Train module for boilers utilizing light oil, and satisfies NFPA-85.</a><br/><br/>

<a href="../valve_trains/product_families/EVT_9000.php" target="product_families" style="color:#ffffff;">EVT-9000 Series is a Complete Valve Train for boilers utilizing light oil, and satisfies NFPA-85.</a><br/><br/>

<a href="../valve_trains/product_families/ERM_regulator.php" target="product_families" style="color:#ffffff;">ERM- Regulator Module</a><br/><br/>

<a href="../valve_trains/product_families/EMM_fuel_metering.php" target="product_families" style="color:#ffffff;">EMM- Fuel Metering Module</a><br/><br/>

<a href="../valve_trains/product_families/EFM_flow_control.php" target="product_families" style="color:#ffffff;">EFM- Flow Control Device Module</a><br/><br/>

</div>

</div>
<div id="ValveTrainFAQ">
<iframe src="product_families/EVT_1000.php" name="product_families" height="400" width="500" frameborder="0">You need a Frames Capable browser to view this content.</iframe> 


<!-- <font color="#4E4848" size="1">&#149; Hand Valves<br/>
&#149; Gauges<br/>
&#149; Check Valves<br/>
&#149; Strainers/Filters<br/>
&#149; Regulators<br/>
&#149; Safety Shut Off Valves<br/>
&#149; Flow Control Valves<br/>
&#149; Pressure Switches<br/>
&#149; Junction Boxes<br/>
&#149; Control Panels</font> -->
</div>
<!-- <a href="pdfs/ETTER-Engineering-Valve-Trains-EVT.pdf" target="_blank" id="ValveTrainPDFButton"></a> -->
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="../privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="../terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="../site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="../viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="../ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>