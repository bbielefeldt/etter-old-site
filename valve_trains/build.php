<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Build Your Valve Train</title>
<meta name="keywords" content="build your valve train, valve train models, valve train configurator, evt 1000, evt 2000, evt 3000, valve train brochure," />
<meta name="description" content="Brochures and configurators to help you build an ETTER Engineering valve train"/>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="../ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="../ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="../all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="../all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="../includes/javascript.js"> </script>
<script type="text/javascript" src="../includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="../includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type= "text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="NavBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("../mainnav.php"); ?>

<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="ENGBBoosterTransBLK"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SpectrumSolidWhiteBkgrd"></div>

<!-- 
<div id="ENGBSlideshow">
<div class="IndexSlideshow" > 
    <img src="../Picture_957.gif" width="399" height="433" alt="Valve Trains"/> 
    <img src="../slideshow_Valve_Train_Photo.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-3.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-4.gif" width="399" height="433" alt="Valve Trains"/> 
</div> 
</div> -->

<div id="ControlPanel" style="margin-top:-75px;width:245px;height:433px;">
	
<div style="width:215px;">
	
<h2 style="color:#ffffff;font-weight:bold;font-size:13px;">Build Your Valve Trains</h2>

To begin building your valve train, click on the ETTER Engineering Valve Train model you are interested in below. The individual brochures describe our various valve train models, and each contains a corresponding part number configurator. For help using the configurator, please call ETTER Engineering at 1-800-444-1962.

</div>

</div>
<div id="ValveTrainFAQ">
<iframe src="build_downloads.php" name="valve_train_builds" height="400" width="500" frameborder="0">You need a Frames Capable browser to view this content.</iframe> 


<!-- <font color="#4E4848" size="1">&#149; Hand Valves<br/>
&#149; Gauges<br/>
&#149; Check Valves<br/>
&#149; Strainers/Filters<br/>
&#149; Regulators<br/>
&#149; Safety Shut Off Valves<br/>
&#149; Flow Control Valves<br/>
&#149; Pressure Switches<br/>
&#149; Junction Boxes<br/>
&#149; Control Panels</font> -->
</div>
<!-- <a href="pdfs/ETTER-Engineering-Valve-Trains-EVT.pdf" target="_blank" id="ValveTrainPDFButton"></a> -->
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="../privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="../terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="../site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b><!--Learn More Text Goes Here --></b></font></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="../ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>