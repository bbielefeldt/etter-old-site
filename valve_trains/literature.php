<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>Valve Train Literature</title>
<meta name="keywords" content="gas pressure switches, pressure gauges, regulators, safety shutoff valves, filters, valve train literature," />
<meta name="description" content="Further information about the parts that ETTER Engineering uses in its valve trains.
Page copy: The below files provide more information about the filters, gas pressure switches, pressure gauges, regulators, and safety shutoff valves that ETTER Engineering uses in its valve trains."/>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="../ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="../ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="../all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="../all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="../includes/javascript.js"> </script>
<script type="text/javascript" src="../includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="../includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type= "text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="NavBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("../mainnav.php"); ?>

<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SpectrumSolidWhiteBkgrd"></div>

<!-- 
<div id="ENGBSlideshow">
<div class="IndexSlideshow" > 
    <img src="../Picture_957.gif" width="399" height="433" alt="Valve Trains"/> 
    <img src="../slideshow_Valve_Train_Photo.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-3.gif" width="399" height="433" alt="Valve Trains"/>
    <img src="../valve_train_photo-4.gif" width="399" height="433" alt="Valve Trains"/> 
</div> 
</div> -->

<style type="text/css">
table .PDF-icon{
	background: url('../images/pdf-icon.png') no-repeat;
	padding: 0 0 0 25px;
	overflow: visible;
}
table td{
	width: 183px;
	padding: 0 25px 0 0;

}
table ul {
	margin:0;
	padding:0;
}
table li {
	list-style-type: none;
	margin: 0 0 10px 0;
	min-height: 20px;
}
table li a{
	color: #475578;
	text-decoration: none;
}
</style>


<div id="Literature">
<p style="margin: 0;padding: 0; width:480px;font-size:10px;">The below files provide more information about the filters, gas pressure switches, pressure gauges, regulators, and safety shutoff valves that ETTER Engineering uses in its valve trains.</p>
<table>

<tr>
	
<td valign="top">
<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">Filters</h2>
<ul>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Filters/Maxitrol_Gas_Filters.pdf" target="_blank">Maxitrol Gas Filters</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Filters/Model_11-M_Strainer.pdf" target="_blank">Model 11-M Strainer</a></li>
</ul>

<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;padding-top:12px;">Gas Pressure Switches</h2>
<ul>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/Antunes_Model_G_Gas_Pressure_Switch.pdf" target="_blank">Antunes Model G Gas Pressure Switch</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Antunes_Model_G_Gas_Pressure_Switch.pdf" target="_blank">Antunes Versa Plus Gas Pressure Switches Specifications</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Ashcroft_B400_Series_Pressure_Switches_Ranges.pdf" target="_blank">Ashcroft B400 Series Pressure Switches Ranges</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Ashcroft_B400_Series_Pressure_Switches_Specifications.pdf" target="_blank">Ashcroft B400 Series Pressure Switches Specifications</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Honeywell_C6097_Pressure_Switch.pdf" target="_blank">Honeywell C6097 Presssure Switch</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Karl_Dungs_Gas_Pressure_Switch_for_Gas_Air.pdf" target="_blank">Karl Dungs Gas Pressure Switch for Gas &amp; Air</a></li>
</ul>
</td>

<td valign="top">

<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">Gas Pressure Switches Cont'd.</h2>
<ul>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Kromschroder_DG_Pressure_Switches_Specifications.pdf" target="_blank">Kromschroder DG Pressure Switches Specifications</a></li>
</ul>

<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;padding-top:12px;">Pressure Gauges</h2>
<ul>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Pressure_Gauges/Miljoco_LP2507_Gauge.pdf" target="_blank">Miljoco LP2507 Gauge</a></li>
</ul>

<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;padding-top:12px;">Regulators</h2>
<ul>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Bryan_Donkin_USA_Model_240_Regulator.pdf" target="_blank">Bryan Donkin USA Model 240</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Bryan_Donkin_USA_Model_241_Regulator.pdf" target="_blank">Bryan Donkin USA Model 241</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Bryan_Donkin_USA_Model_260_Regulator.pdf" target="_blank">Bryan Donkin USA Model 260</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Bryan_Donkin_USA_Model_274_Regulator.pdf" target="_blank">Bryan Donkin USA Model 274</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Maxitrol_325_Regulator.pdf" target="_blank">Maxitrol 325 Regulator</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Maxitrol_325L_Regulator.pdf" target="_blank">Maxitrol 325L Regulator</a></li>
<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Maxitrol_RV_Sizing_Chart.pdf" target="_blank">Maxitrol RV Sizing Chart</a></li>
</ul>

</td>

<td valign="top">
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">Regulators Cont’d.</h2>
	<ul>
  <li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Maxitrol_RV52_RV131.pdf" target="_blank">Maxitrol RV52 &amp; RV131</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_043-B_Gas_Regulator.pdf" target="_blank">Sensus Model 043-B</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_043-C_Gas_Regulator.pdf" target="_blank">Sensus Model 043-C</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_121_Gas_Regulator.pdf" target="_blank">Sensus Model 121</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_122_Gas_Regulator.pdf" target="_blank">Sensus Model 122</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_143_Gas_Regulator.pdf" target="_blank">Sensus Model 143</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_243_Gas_Regulator.pdf" target="_blank">Sensus Model 243</a></li>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Regulators/Sensus_Model_496_Gas_Regulator.pdf" target="_blank">Sensus Model 496</a></li>
	</ul>
	
	<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;padding-top:12px;">Safety Shutoff Valves</h2>
	<ul>
	<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/ASCO-Valve-8040-8215-spec-R2.pdf" target="_blank">ASCO Valve 8040 &amp; 8215
	</a></li>
    <li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Gas_Pressure_Switches/Karl_Dungs_Gas_Pressure_Switch_for_DMV_Safety_Shutoff_Valves.pdf" target="_blank">Karl Dungs Gas Pressure Switch for DMV Safety Shutoff Valves</a></li>
	</ul>
	
</td>

<td valign="top">
		
		<h2 style="color:#cf0d29;font-size:14px;font-weight:bold;">Safety Shutoff Valves Cont'd.</h2>
		<ul>
        <li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/Maxon_5000_Specifications.pdf" target="_blank">Maxon 5000</a></li>
		<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/SIEMENS_SKP15.pdf" target="_blank">Siemens SKP15</a></li>
		<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/SIEMENS_SKP25.pdf" target="_blank">Siemens SKP25</a></li>
		<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/SIEMENS_VG_Valve_Bodies.pdf" target="_blank">Siemens VG Valve Bodies</a></li>
		<li class="PDF-icon"><a href="../valve_trains/docs/literature_docs/Valves/VAS_Val-Vario.pdf" target="_blank">Kromschroder VAS Val-Vario</a></li>
		</ul>
		
	</td>
	
</tr>

</table>

<!-- <font color="#4E4848" size="1">&#149; Hand Valves<br/>
&#149; Gauges<br/>
&#149; Check Valves<br/>
&#149; Strainers/Filters<br/>
&#149; Regulators<br/>
&#149; Safety Shut Off Valves<br/>
&#149; Flow Control Valves<br/>
&#149; Pressure Switches<br/>
&#149; Junction Boxes<br/>
&#149; Control Panels</font> -->
</div>


<!-- <a href="pdfs/ETTER-Engineering-Valve-Trains-EVT.pdf" target="_blank" id="ValveTrainPDFButton"></a> -->
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="../privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="../terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="../site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b><!--Learn More Text Goes Here! --></b></font></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="../ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>