<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> What does a basic ETTER Engineering valve train include?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> 

The equipment involved is similar whether the overall design criteria are to satisfy NFPA 85 or NFPA 86. In either case, the basic equipment includes:<br/><br/>

Fuel Inlet and Delivery Equipment:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>This typically includes the components for fuel supply, manually shut off of the fuel line, and a gas or fuel oil filter strainer</li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">Fuel Pressure Regulating Equipment:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>This typically includes the Gas or Fuel Oil Pressure Regulator. These components are chosen specifically by application specific data.</li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">Safety Shut-Off Valves:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>These are Normally Closed valves that are electrically actuated to open and either allow or inhibit the flow of fuel. They are not flow control valves, as they are to be dedicated to the shut-off function. The valves will have visual indication and proved close switches for applications and sizes that require these features.</li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">Fuel Pressure Switches:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>These are pressure switches, for either gas or oil that are normally closed in a safe operating state, which open when the fuel pressure exceeds the switch setting.</li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">Flow Control Device:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>These are valves that are designed to modulate the flow of the gas or oil, into the burner. They are intended to function with the burner management system or device to maintain a desired and safe operating condition.</li>
</ul>


</div>