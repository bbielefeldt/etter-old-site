<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> What is a valve train?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> 

Commercial, Industrial, and Power Generation equipment that utilizes natural gas, propane, or various grades of heating oil as the fuel source, all need and have a series of valves and control devices to control the input of these fuels in a safe, controlled manner. The design, installation, operations, maintenance, component selection and arrangement, and approval of these devices are dictated by national and International Codes and Guidelines, to insure the utmost safety, reliability, and consistency.<br/><br/>

These fuel delivery systems are commonly referred to as Gas Trains, Fuel Trains, or Pipe Trains, and typically include the Main burner and Pilot system fuel controls.</p>

</div>
