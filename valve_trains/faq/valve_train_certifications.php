<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> Do ETTER Engineering valve trains adhere to other certifications?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> 

Regardless of which type of equipment or which guideline is being followed, most Fuel Trains use similar equipment and features. The most common requirement for any of the components is that they are “listed and approved”. This means that the device being used to satisfy a specific function has been tested and approved by a third party other than the manufacture, and carries a certification that it will perform as advertised. This insures safety is the priority. These components are typically tested and approved by Underwriters Laboratory (UL), or Factory Mutual (FM), or both.

</p>

</div>