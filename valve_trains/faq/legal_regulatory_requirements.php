<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> Are there other legal or regulatory requirements for valve trains?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> 

Additionally, in the interest of safety, both NFPA-85 and NFPA-86 require that the equipment is designed, fabricated, installed, and operated by qualified personnel.<br/><br/>

Frequent testing, at intervals prescribed at the manufactures discretion, is required for all components, and the design needs to include means of testing without disassembly of any components.

</p>

</div>