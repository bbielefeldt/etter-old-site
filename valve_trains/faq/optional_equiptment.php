<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> Is there any optional equiptment that can be added by ETTER Engineering?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> <br/><br/>

ERM- Regulator Module:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>-	This is a pressure regulating device for either gas or liquid fuel oil, which is intended as part of the complete Valve Train assemblies for the EVT-3000, 6000, or 9000 series assemblies, to satisfy either the NFPA 85 or 86 requirements. The specific inlet and outlet pressure, fuel flow rate, and turn-down requirements will be used to determine the component selection. Additionally, the location and installation will be dependent on the fuel type and application. </li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">EMM- Fuel Metering Module:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>This is a fuel metering device for either gas or liquid fuel oil, and is intended as part of the complete Valve Train assemblies for the EVT-3000, 6000, or 9000 series assemblies, to satisfy either the NFPA 85 or 86 requirements. The specific fuel flow rate, and turn-down requirements, will be used to determine the component selection. Additionally, the location and installation will be dependent on the fuel type and application. Data requirements such as temperature and pressure compensation, and pulse type are part of the component selection process. </li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">EFM- Flow Control Device Module:</p>
<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>This is a fuel flow control device for either gas or liquid fuel oil, and is intended as part of the complete Valve Train assemblies for the EVT-3000, 6000, or 9000 series assemblies, to satisfy either the NFPA 85 or 86 requirements. The specific fuel pressure, flow rate, and turn-down requirements, will be used to determine the component selection. Additionally, the location and installation will be dependent on the fuel type and application. Integration of the Control signal type from the Burner Control System is part of the component selection process.</li>
</ul>


</div>