<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> What codes do ETTER Engineering valve trains adhere to?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> 

There are several families of equipment that are categorized into two different sets of codes and guidelines. These are the National Fire Protection Agency Standard (NFPA) 86, and the NFPA Code 85. The NFPA 86 Guideline covers Industrial equipment such as:</p>

<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>Industrial Ovens</li>
<li>Furnaces</li>
<li>Dryers</li>
<li>Thermal Oxidizers</li>
<li>Process Heat Enclosures</li>
</ul>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">The NFPA 85 Code covers:</p>

<ul style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>Single Burner Boilers</li>
<li>Multiple Burner Boilers</li>
<li>Stokers</li>
<li>Fluidized Bed Boilers</li>
<li>Heat Recovery Steam Generators</li>
<li>Turbine Exhaust Re-heaters</li>
</ul>

</div>