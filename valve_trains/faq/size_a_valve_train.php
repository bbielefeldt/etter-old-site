<style type="text/css">
img{
	max-width: 300px;
}
.caption{
	font-style:italic;
	color:#888888;
}
</style>

<div style="padding-right:20px;">
	
<h2 style="font-family:Georgia,Times,Serif;font-size:16px;font-weight:normal;">
<span style="color:#cf0d29;font-weight:bold;">Question:</span> What do I need to know to size a valve train?</h2>

<p style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<span style="color:#cf0d29;font-weight:bold;">Answer:</span> <br/><br/></p>

<ol style="font-family:Georgia,Times,Serif;font-size:12px;line-height:18px;">
<li>What is your application?<br/><br/>

This is a pressure regulating device for either gas or liquid fuel oil, which is intended as part of the complete Valve Train assemblies for the EVT-3000, 6000, or 9000 series assemblies, to satisfy either the NFPA 85 or 86 requirements. The specific inlet and outlet pressure, fuel flow rate, and turn-down requirements will be used to determine the component selection. Additionally, the location and installation will be dependent on the fuel type and application.<br/><br/>

Fuel Trains vary depending on if your application requires compliance with NFPA 86 for Industrial Ovens and Furnaces, or NFPA 85 for Commercial boilers, dual-fuel boilers, and hot water heaters. Below is a more thorough list of what equipment falls under each design guide:<br/><br/>

<style type="text/css">
table { 
	font-family:Georgia,Times,Serif;
	font-size:10px;
	line-height:14px;
	border: solid 1px #aaaaaa;
}
td {
	border: solid 1px #aaaaaa;
	padding: 5px 5px;
}

</style>

<table cellpadding="0" cellspacing="0">
	<tr>
		<td>NFPA-86, Ovens and Furnaces
		</td>
		<td>NFPA-85, Boilers and Combustion Systems
		</td>
	</tr>
	<tr>
		<td>Industrial Ovens (Class A,B,C,D)
		</td>
		<td>Single Burner Boiler
		</td>
	</tr>
	<tr>
		<td>Industrial Dryers
		</td>
		<td>Multiple Boiler Burner
		</td>
	</tr>
	<tr>
		<td>Furnaces
		</td>
		<td>Fluidized Bed Boilers
		</td>
	</tr>
	<tr>
		<td>Thermal Oxidizers
		</td>
		<td>Other systems covered under NFPA-85 are NOT covered in these standard Fuel Trains
		</td>
	</tr>
	<tr>
		<td>Any heated enclosure for the purpose of processing material and related equipment
		</td>
		<td>&nbsp;
		</td>
	</tr>
</table>
</li><br/><br/>


<li>What is your fuel type?<br/><br/>

Burners typically operate on natural Gas, Propane, or Heating Oil. These standard trains will cover any of these fuels, provided the following:<br/><br/>

<ul>
<li>The gaseous fuels fall within the pressure ratings of the standard components. These are provided in the descriptions of valves and pressure switches in the configurators. Typically the gases will be under 15 PSI, downstream of the pressure reducing regulator.</li>
<li>The Heating oil is #2 thru #6, heated or un-heated. Fuel temperature and viscosity will determine which valves and components can be used. See the configurators for limitations.</li>
<li>On a Dual-Fuel system, there can be two Valve Trains provided, one for the gas, and one for the oil. The configurators will allow you to configure different Fuel trains, depending on your applications.</li>
</ul>

</li><br/><br/>



<li>Is this a retrofit or new installation?<br/><br/>

<ul>
<li>While the gas trains for a new or retrofit application need to follow the same design criteria, ETTER has provide two main Valve Train configurations, with add-on modules, to provide the user the ability to use existing equipment such as filters, and determine if they need pressure regulators, control motors, or flow meter modules. The basic offering of a Valve Train, for a main burner application, is either a Module, or a Complete Train. The Module covers the basic safety equipment that includes the isolation valves, electronic safety shut-off valves, and pressure switches. The complete units include the inlet drip leg and filter/strainer, and provides for the optional pressure regulator module, the flow meter module, and control valve module. Please note that both NFPA-85 and NFPA-86 require when upgrading older and existing equipment that everything be brought up to current code and guideline requirements.</li>
</ul>

</li><br/><br/>


<li>Does your heating equipment have a pilot?<br/><br/>

<ul>
<li>Some burner systems are direct spark systems, with no pilot or igniter fuel requirements. Other system will have a pilot fuel system, and would need a stand-alone pilot fuel train. It is common that on a main fuel system firing on oil, the pilot can be either natural gas or propane.</li>
</ul>
	
</li><br/><br/>



<li>What is the minimum and maximum flow rates?<br/><br/>

<ul>
<li>Different types, makes, and models of equipment have different ranges that they can operate over. This rangeability is typically called “turn-down”. For an example, if a boiler burner has a maximum firing rate of 1 MM BTU/Hr., and a minimum firing rate of 100,000 BTU/Hr., then the system has a 10:1 turndown.</li>
<li>While the piping and valve body sizes will be based on the maximum flow rate, other components such as the regulator and flow control valve will be based on the maximum flow and turn down ratio. </li>
</ul>
	
</li><br/><br/>


<li>What is the fuel pressure delivered to the valve train?<br/><br/>

<ul>
<li>The supply pressure is a critical piece of data in sizing a valve train, as it will determine how much pressure loss is available, or required, to satisfy the burner fuel pressure requirements. Most burners have a fuel control mechanism, being a flow control valve, or other device, that regulates the burner from its maximum to minimum firing rate. The objective of the valve train design is to deliver fuel to this flow control device at a constant pressure, providing the proper pressure drop for accurate and repeatable control. The delivered pressure is also required to determine what, if any, is required for a fuel pressure regulator.</li>
</ul>
	
</li><br/><br/>



<li>What pressure do you need delivered to the burner front?<br/><br/>

<ul>
<li>The required fuel pressure to the burner is determined from the manufacturer’s literature. This is critical to ensure proper and safe operations of your system. Inadequate fuel pressure will result in lean burning and reduced turn-down ability. Excessive pressure will either result in over-firing or poor control. Knowing the required fuel pressure, along with the system pressure delivered to your valve train, will allow for proper sizing.</li>
</ul>
	
</li><br/><br/>


<li>Do you have any preferred or required component manufacturers?<br/><br/>

<ul>
<li>Many facilities have standardized on certain brands and components, for ease of maintenance and spare parts. The ETTER Fuel trains, whether standard or customized, can help you fulfill these requirements.</li>
<li>Additionally, operating ranges of flow and pressure may dictate what components you can and cannot use. As an example, a steel mill may require cast iron valve bodies, which would eliminate the use of the aluminum body brands. Check with your plant Engineers for any facility specific requirements.</li>
</ul>
	
</li><br/><br/>


<li>How do I size my valve train?<br/><br/>
	
Valve Trains should not be sized on burner connection size, but on the fuel flow rates, pressure, and available pressured drop. Most Valve Train’s will be sized for minimal pressure drops at maximum flow rate. The brand and model of the valve bodies will be determined by three factors:<br/><br/>

<ul>
<li>The pressure rating of the bodies</li>
<li>Any desired or local requirements for Brand</li>
<li>The pressure drop of a given brand and body size</li>
<li>The budget – steel bodies cost more than aluminum bodies, etc.</li>
</ul><br/>

The charts below are for the three main brands that we have standardized our Valve Trains on. As you find your flow, you will see that when you move from the Maxon, to the SIEMENS, to the Kromschroder, the pressure drop for a given flow goes up, and the valve CV goes down.<br/><br/>

The fuel pressure switches have similar differences from manufacturer to manufacturer.<br/><br/>

Other issues to address include location (indoor or outdoor), operating temperature, available voltage, etc.
ETTER Engineering would be pleased to help you select your components with you, as this can be cumbersome if you do not do this frequently!<br/><br/>

<a href="../docs/pressure_switch_charts/Maxon_5000_gas.pdf"><img src="images/maxon-gas.jpg" /></a><br/>
<p class="caption">Maxon 5000 Series Valve Chart for Natural Gas</p><br/><br/>

<a href="../docs/pressure_switch_charts/SIEMENS_VGD_gas.pdf"><img src="images/siemens-gas.jpg" /></a><br/>
<p class="caption">SIEMENS VGD Gas Valve Chart</p><br/><br/>

<a href="../docs/pressure_switch_charts/SIEMENS_VGG_gas.pdf"><img src="images/siemens-gas.jpg" /></a><br/>
<p class="caption">SIEMENS VGG Gas Valve Chart</p><br/><br/>

<a href="../docs/pressure_switch_charts/Kromschroeder.pdf"><img src="images/kromshroeder.jpg" /></a><br/>
<p class="caption">Kromschroder Chart for Size 1, 2, 3 Valvario Valves for Natural Gas</p><br/><br/>

<a href="../docs/pressure_switch_charts/Maxon_5000_oil.pdf"><img src="images/maxon-oil.jpg" /></a><br/>
<p class="caption">Maxon 5000 Series Chart for Oil</p><br/><br/>

<a href="../docs/pressure_switch_charts/SIEMENS_oil.pdf"><img src="images/siemens-oil.jpg" /></a><br/>
<p class="caption">SIEMENS Oil Valve Chart</p><br/><br/>


	
</li><br/><br/>




</ol>


</div>