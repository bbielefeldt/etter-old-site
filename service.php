<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering's annual Combustion Safety Audit Program is designed to verify the existence of proper safety equipment per the National Fire Prevention Associationís NFPA-86 Standard for Ovens and Furnaces and ensure the proper operation of your combustion systems." /><meta name="keywords" content="Safety Audits, safety audits, Combustion Safety Audits, combustion safety audits, NFPA 86, NFPA-86, National Fire Protection Association, national fire protection association, ETTER Engineering, Etter Engineering, etter engineering, ETTER engineering, ovens, burners, furnaces, service, tuneup, safety, OSHA" />
<title>Service - Combustion Services - ETTER Engineering</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->	
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com/"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Package Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
			<li><a href="packaged-burners.php">Package Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="accolades.php">Accolades</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Case Studies</a></li>
			<li><a href="Newsletter-Archive.php">Newsletter Archive</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="SafetyAuditWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SafetyAuditPhoto"></div>
<div id="SafetyAuditTransBLK"></div>
<div id="SafetyAuditDropRightBlkTrans"></div>
<div id="RedBrowseBar"></div>
<div id="SafetyAudits">
ETTER Engineering has over 100 years of combined combustion expertise. Our service team will always offer you to most competitive service rates,
parts, and system pricing in conjunction with a quick response time.
<br/><br/>
<br/>
<font size="2" color="#445679"><b>Our Services Include:</b></font>
<br/><br/>&#149; System Troubleshooting
<br/>&#149; Onsite Equipment Start-ups
<br/>&#149; NFPA-86 Combustion Safety Audits
<br/>&#149; Emergency Service and Repairs
<br/>&#149; Preventative Maintenance
<br/>&#149; Burner Updates
<br/>&#149; Process Tuning
<br/>&#149; Burner Tuning
</div>
<div id="CompanyName"><b>ETTER Engineering Company, Inc.</b></div>
<div id="Address"><b>210 Century Drive, Bristol, CT 06010</b></div>
<div id="Phone"><b>1-800-444-1962</b></div>
<a href="privacy_policy.php" id="PrivacyPolicy"><b>Privacy Policy</b></a>
<a href="terms_of_service.php" id="TermsofService"><b>Terms of Service</b></a>
<a href="site_map.php" id="SiteMap"><b>Site Map</b></a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>