<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Vulcan Catalytic combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Vulcan Catalytic,custom control panels,control panels,plc control panels,control panel,plc control panel,PLC Operator Interface,PLC Operator Interfaces,Gas Pulse,Percentage Timer Dial,Percentage Timer Dials" />
<title>ETTER Engineering - Vulcan Catalytic Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="VulcanLogoLarge"></div>
<div id="SensusText">
<br/><br/>Since 1990, Vulcan Catalytic Systems, Ltd. has designed, manufactured, and marketed high 
quality industrial heating equipment.
<br/><br/>Located on Aquidneck Island in the Portsmouth Business Park in Portsmouth, RI, Vulcan, 
a leader in the development of infrared technology continues to engineer and manufacture gas 
catalytic infrared heaters, oven systems and controls for the process heating industry.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/plc-operator-interface-2.gif" alt="Vulcan Catalytic PLC Operator Interface" title="Vulcan Catalytic PLC Operator Interface"/></div>       
			<div id="PartsContent"><h3>PLC Operator Interface</h3>
    			<p>Flameless Catalytic heating which has brought energy cost savings to 
			numerous thermo formers and paint finishers, now has a well proven method 
			of control - Gas Pulse System (GPS).
			<br/><br/>The GPS unit is simple, cost effective and offers the option of 
			multi zone configuration, permitting accurate and repeatable heating profiles 
			for the catalytic heating system. Vulcan has pioneered Gas Pulse Technology, 
			having installed many hundreds in both the finishing and thermo forming industries 
			to control catalytic heaters. Typical gas industrial control valves, are totally 
			unsuitable for controlling catalytic heaters. GPS has been proven to save gas 
			consumption over other industry control valves.</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/vert-oven-lg.gif" alt="Vulcan Catalytic Oven Applications" title="Vulcan Catalytic Oven Applications"/></div>
			<div id="PartsContent"><h3>Catalytic Oven Applications</h3>
    			<p>Control of the GPS is via one of two operator control types. For systems with 
			less than five control zones, a simple percentage timer may be used. For systems 
			with a greater number of zones, a PLC based control with an intuitive operator 
			interface, offers the customer the best solution for multi zone systems, with 
			recipe storage, system diagnostics functions and global increase / decrease of 
			any heating profile.</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/GasPulse_sm.gif" alt="Vulcan Catalytic Gas Pulse" title="Vulcan Catalytic Gas Pulse"/></div>
			<div id="PartsContent"><h3>Gas Pulse</h3>
    			<p>Gas pulse technology uses electrically actuated solenoid valves, to cycle 
			the gas supply between flow rates of 100% and 20%. Longevity and reliability 
			have been the hallmarks for this technology, and synchronizes very well with 
			the mechanics of the flameless oxidation process within Vulcan's patented 
			catalytic heaters. By alternating between the high and low flow rates, the 
			entire depth of catalyst is used, ensuring an even heat distribution across 
			entire heater face at all percentage settings.</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/dial.gif" alt="Vulcan Catalytic Percentage Timer Dial" title="Vulcan Catalytic Percentage Timer Dial"/></div>
			<div id="PartsContent"><h3>Percentage Timer Dial</h3>
    			<p>Control of the GPS is via one of two operator control types. For systems with less than five control zones, a simple percentage timer may be used.</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/controlP.gif" alt="Vulcan Catalytic Control Panel" title="Vulcan Catalytic Control Panel"/></div>
			<div id="PartsContent"><h3>Control Panel</h3>
    			<p>A Vulcan Catalytic heating system is controlled with state-of-the-art UL 
			approved controls, which incorporate our unique gas pulse technology (GPS). 
			Proved and tested for many years GPS is the most economical method of providing 
			multi zone capability for catalytic ovens via PLC based controls. GPS carefully 
			meters precise volume pulses of gas, ensuring ideal temperature settings while allowing 
			for multi zoned heating arrays. The Vulcan Gas Pulse System incorporates a recipe-based 
			menu which interfaces with digital output cards. Operators call up the recipe on the 
			touch-sensitive PLC screen and the system automatically sets the control using the Gas 
			Pulse System.</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/catalyticheater-01.gif" alt="Vulcan Catalytic Heater" title="Vulcan Catalytic Heater"/></div>
			<div id="PartsContent"><h3>Catalytic Heater</h3>
    			<p>When the catalytic heater is turned on the catalyst is preheated with a tubular 
			electric heating element for approximately 15 minutes. Once the catalyst has reached 300&deg;F, 
			safety devices are activated allowing gas to enter the back of the heater. The gas 
			intermingles with the hot platinum catalyst and oxygen present in the surrounding air, 
			starting an oxidation reduction reaction. This raises the catalyst temperature to between 
			350&deg;F and 900&deg;F, at the same time releasing water vapor and carbon dioxide. 
			Efficiency tests have established that up to 80% of the gas is converted into infrared heat. 
			Since the reaction temperature is well below the auto-ignition temperature for natural gas 
			(1300&deg;F), the reaction is flameless. The catalytic reaction is fully established five minutes 
			after the gas enters the heater, and the pre heater is turned off.</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Vulcan/plc-operator-interface-2.gif" alt="Vulcan Catalytic PLC Operator Interface" title="Vulcan Catalytic PLC Operator Interface"/></div>       
			<div id="PartsContent"><h3>PLC Operator Interface</h3>
    			<p>Flameless Catalytic heating which has brought energy cost savings to numerous thermo 
			formers and paint finishers, now has a well proven method of control - Gas Pulse System (GPS).
			<br/><br/>The GPS unit is simple, cost effective and offers the option of multi zone 
			configuration, permitting accurate and repeatable heating profiles for the catalytic 
			heating system. Vulcan has pioneered Gas Pulse Technology, having installed many hundreds 
			in both the finishing and thermo forming industries to control catalytic heaters. Typical 
			gas industrial control valves, are totally unsuitable for controlling catalytic heaters. 
			GPS has been proven to save gas consumption over other industry control valves.</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/plc-operator-interface-2_Thumbnail.gif" border="0" alt="Vulcan Catalytic PLC Operator Interface" title="Vulcan Catalytic PLC Operator Interface"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/vert-oven-lg_Thumbnail.gif" border="0" alt="Vulcan Catalytic Oven Applications" title="Vulcan Catalytic Oven Applications"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/GasPulse_sm_Thumbnail.gif" border="0" alt="Vulcan Catalytic Gas Pulse" title="Vulcan Catalytic Gas Pulse"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/dial_Thumbnail.gif" border="0" alt="Vulcan Catalytic Percentage Timer Dial" title="Vulcan Catalytic Percentage Timer Dial"/></font></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/controlP_Thumbnail.gif" border="0" alt="Vulcan Catalytic Control Panel" title="Vulcan Catalytic Control Panel"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Vulcan/thumbnails/catalyticheater-01-sm_Thumbnail.gif" border="0" alt="Vulcan Catalytic Heater" title="Vulcan Catalytic Heater"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

