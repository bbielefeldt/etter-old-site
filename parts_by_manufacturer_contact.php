<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->	
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
<script type="text/javascript">
$(function() {
$('a.SolidVideo').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGBSolid"><img border="0" src="closeXsmall.gif" class="btn_close_ENGBSolid" title="Close Window" alt="Close" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn(); 
return false;
});
$('a.close_ENGBSolid, #fade').live('click', function() {
    $('#fade , .ENGBSolid_block').fadeOut(function() {
        $('#fade, a.close_ENGBSolid').remove(); 
    });
    return false;
});
});
</script>
</head>
<body link=#445679 vlink=#445679>
<div <div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color=#494A4A><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script>
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php"div id="Logo"></a>
<a href="index.php"div id="Tagline">to ALL your process heating & combustion needs!</a>     
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="AddressETC">
<font color=#000000><b><font face="Tahoma"><font size="2">210 Century Drive</b></font></font></font>
<br><font color=#D21D1F><b><font face="Tahoma"><font size="2">Bristol, Ct 06010</b></font></font></font></div>
<div id="PhoneNumber">
Tel: <font face="Tahoma"><font size="2">860-584-8842</font></font><br>
Fax: <font face="Tahoma"><font size="2">860-584-8612</font></font><br>
Toll Free: <font face="Tahoma"><font size="2">1-800-444-1962</font></font></div>
<a href="contact_us_employee_directory.php"div id="DirectButtonTxt"><b>View Employee Directory</b></a>

<div id="PartsContactForm">Our Parts by Manufacturer library is constantly growing. The manufacturer's library you have selected is currently not available. Please fill out the form on the right to receive information about your parts or manufacturer of interest. </div>

<div id="ContactFormParts">
<form id="emf-form" enctype="multipart/form-data" method="post" action="http://www.emailmeform.com/builder/form/LHMjmv41q3ey4" name="emf-form">
  <table style="text-align:left;" cellpadding="2" cellspacing="0" border="0" bgcolor="transparent">
    <tr>
      <td style="" colspan="2">
        <font face="Verdana" size="1" color="#000000"><br />
        <br /></font>
      </td>
    </tr>
    <tr valign="top">
      <td style="" align="">
        <font face="Verdana" size="1" color="#000000"><b>Your Name</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td style="">
        <input id="element_0" name="element_0" value="" size="30" class="validate[required]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td style="" align="">
        <font face="Verdana" size="1" color="#000000"><b>Your Company</b></font>
      </td>
    </tr>
    <tr>
      <td style="">
        <input id="element_1" name="element_1" value="" size="30" class="validate[optional]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td style="" align="">
        <font face="Verdana" size="1" color="#000000"><b>Your Email</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td style="">
        <input id="element_2" name="element_2" class="validate[required,custom[email]]" value="" size="30" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td style="" align="">
        <font face="Verdana" size="1" color="#000000"><b>Subject</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td style="">
        <input id="element_3" name="element_3" value="" size="30" class="validate[required]" type="text" />
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr valign="top">
      <td style="" align="">
        <font face="Verdana" size="1" color="#000000"><b>Message</b></font> <span style="color:red;"><small>*</small></span>
      </td>
    </tr>
    <tr>
      <td style="">
        <textarea id="element_4" name="element_4" cols="30" rows="5" class="validate[required]">
</textarea>
        <div style="padding-bottom:0px;color:#000000;"></div>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="left">
        <input name="element_counts" value="5" type="hidden" /> <input name="embed" value="forms" type="hidden" /><input value="Send" type="submit" /><input value="Clear" type="reset" />
      </td>
    </tr>
  </table>
</form>
<div>
  <font face="Verdana" size="1" color="#000000"></font><span style="position: relative; padding-left: 3px; bottom: -5px;"></span><font face="Verdana" size="1" color="#000000"></font> <a style="text-decoration:none;" href="http://www.emailmeform.com"
  target="_blank"><font face="Verdana" size="1" color="#000000"></font></a>
</div><a style="line-height:20px;font-size:70%;text-decoration:none;" href="http://www.emailmeform.com/report-abuse.html?http://www.emailmeform.com/builder/form/LHMjmv41q3ey4" target=
"_blank"><font face="Verdana" size="1" color="#000000"></font></a>
</div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" div id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" div id="TermsofService">Terms of Service</a>
<a href="site_map.php" div id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"></div>
<div id="LearnMoreFooterText"></div>
<div id="ENGBFooterSolidVideoBTN"></div>
<div id="ENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><img src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf">
        <param name='quality' value="high">
        <param name='bgcolor' value='#FFFFFF'>
        <param name='loop' value="true">
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;">
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" "value="" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;">
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;">
<input type="hidden" name="llr" value="qksvr8cab"> <input type="hidden" name="m" value="1102583613776"> 
<input type="hidden" name="p" value="oi"> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>