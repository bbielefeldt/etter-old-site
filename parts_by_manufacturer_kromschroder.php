<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Kromschroder Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="KromSchroderLogoLarge"></div>
<div id="SensusText"><br/><br/>Kromschroder's high quality components, intelligent system 
solutions and various services make it possible for you to use natural gas economically, 
safely and in an environmentally-friendly way, therefore saving resources in every respect, 
when generating and using heat for process and heating purposes.
</div>
<div id="ThumbnailBackground"></div>

<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Ball-Valves-AKT.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Ball Valves AKT</h3>
			<p><br/>Manual valves for manual shut-off, for gas and air, UL listed and CSA approved.
			<br/><br/><b>DN:</b> 10 to 50
			<br/><b>connection:</b> thread
			<br/><b>pe:</b> max. 125 psig (8.8 bar) brass housing
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/tpb_uvs.pdf" target="_blank"><font color="#ACB0C3"><b>AKT Product Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_akt_gb-1.pdf" target="_blank"><font color="#ACB0C3"><b>AKT Technical Information</b></font></a>
			</p>
		</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Filters-GFK-A.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas Filters GFK..A</h3>
			<p><br/>Gas filter with polypropylene fleece filter pad for cleaning gas and air.
			<br/><br/><b>DN:</b> 15 to 100
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 15 or 60 psig (1 or 4 bar)
			</p>
</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Filters-GFK-N.gif"alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas Filters GFK..N</h3>
			<p><br/>Gas filter with polypropylene fleece filter pad for cleaning gas and air.
			<br/><br/><b>DN:</b> 15 to 100
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 15 or 60 psig (1 or 4 bar)
			</p>
</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Pressure-Regulators-GDJ-15.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas Pressure Regulators GDJ 15</h3>
			<p><br/>Spring-loaded pressure regulator with inlet pressure compensation diaphragm and zero shut-off, for maintaining a constant set outlet pressure pa when gas throughput fluctuates.
			<br/><br/><b>DN:</b> 15 to 100
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 15 or 60 psig (1 or 4 bar)
			<br/><b>pa:</b> 2 "WC to 5 psig (5 to 350 mbar)
			</p>
</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Pressure-Regulators-GDJ-20-50.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas Pressure Regualtors GDJ 20-50</h3>
			<p><br/>Spring-loaded pressure regulator with inlet pressure compensation diaphragm and zero shut-off, for maintaining a constant set outlet pressure pa when gas throughput fluctuates. GDJ for gas, GDJ..L for air.
			<br/><br/><b>DN:</b> 15 to 50
			<br/><b>connection:</b> NPT thread
			<br/><b>pe:</b> max. 5 psig (350 mbar)
			<br/><b>pa:</b> 1 to 64 "WC (2 to 160 mbar)
			</p>
</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Pressure-Regulators-VGBF-A.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"<h3>Gas Pressure Regulators VGBF..N</h3>
			<p><br/>Spring-loaded pressure regulator with inlet pressure compensation diaphragm and zero shut-off, for maintaining a constant set outlet pressure pa when gas throughput fluctuates.
			<br/><br/><b>DN:</b> 15 to 10
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 15 or 60 psig (1 or 4 bar)
			<br/><b>pa:</b> 2 "WC to 5 psig (5 to 350 mbar) 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/tti_vgbf.pdf" target="_blank"><font color="#ACB0C3"><b>VGBF Technical Information</b></font></a>
			</p>
</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas-Pressure-Regulators-VGBF-N.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas Pressure Regulators VGBF..A</h3>
			<p><br/>Spring-loaded pressure regulator with inlet pressure compensation diaphragm and zero shut-off, for maintaining a constant set outlet pressure pa when gas throughput fluctuates.
			<br/><br/><b>DN:</b> 15 to 100
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 15 or 60 psig (1 or 4 bar)
			<br/><b>pa:</b> 2 "WC to 5 psig (5 to 350 mbar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/tti_vgbf.pdf" target="_blank"><font color="#ACB0C3"><b>VGBF Technical Information</b></font></a>
			</p>
</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Gas_Air-Ratio-Regulators-GIK.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Gas/Air Ratio Regulators GIK</h3>
			<p><br/>Gas/air ratio regulators for maintaining a constant gas/air pressure ratio of 1:1, GIK, GI for continuous control, GIK..B for high/low/off control.
			<br/><br/><b>DN:</b> 15 to 100
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 3 psig (200 mbar)
			<br/><b>pL:</b> 0.2 to 48 "WC (0.5 to 120 mbar)
			<br/><b>pG:</b> 0.08 to 47.6 "WC (0.2 to 119 mbar)
			<br/><b>&#916;p:</b> max. 1.5 psig (100 mbar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_gik_gikh.pdf" target="_blank"><font color="#ACB0C3"><b>GIK Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV-25.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Safety Overpressure Shut Valves JSAV 25</h3>
			<p><br/><b>JSAV:</b> Safety overpressure shut valve to secure all downstream fittings against too high gas pressure, for gas.
			<br/><br/><b>DN:</b> 25 to 100
			<br/><b>connection:</b> thread or flange
			<br/><b>pe:</b> max. 60 psig (4 bar)
			<br/><b>pso:</b> 7.5 "WC to 8 psig (18 to 550 mbar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr2_jsav.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba_jsav_gb.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV-40.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Safety Overpressure Shut Valves JSAV 40</h3>
			<p><br/><b>JSAV:</b> Safety overpressure shut valve to secure all downstream fittings against too high gas pressure, for gas.
			<br/><br/><b>DN:</b> 25 to 100
			<br/><b>connection:</b> thread or flange
			<br/><b>pe:</b> max. 60 psig (4 bar)
			<br/><b>pso:</b> 7.5 "WC to 8 psig (18 to 550 mbar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr2_jsav.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba_jsav_gb.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV50-100.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Safety Overpressure Shut Valves JSAV 50-100</h3>
			<p><br/><b>JSAV:</b> Safety overpressure shut valve to secure all downstream fittings against too high gas pressure, for gas.
			<br/><br/><b>DN:</b> 25 to 100
			<br/><b>connection:</b> thread or flange
			<br/><b>pe:</b> max. 60 psig (4 bar)
			<br/><b>pso:</b> 7.5 "WC to 8 psig (18 to 550 mbar) 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr2_jsav.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba_jsav_gb.pdf" target="_blank"><font color="#ACB0C3"><b>JSAV Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Variable-Gas_Air-Ratio-Regulators-GIKH.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Variable Gas/Air Ratio Regulators GIKH</h3>
			<p><br/>Variable gas/air ratio regulators for maintaining a constant gas/air pressure ratio of 4:1 on systems with air pre-heating, GIKH for continuous control, GIKH..B for high/low/off control.
			<br/><br/><b>DN:</b> 25 and 40
			<br/><b>connection:</b> NPT thread
			<br/><b>pe:</b> max. 3 psig (200 mbar)
			<br/><b>p:</b> max. 1.5 psig (100 mbar)
			<br/><b>pL:</b> 0.06 to 12 "WC (0.15 to 30 mbar)
			<br/><b>pG:</b> 0.08 to 48 "WC (0.2 to 120 mbar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_gik_gikh.pdf" target="_blank"><font color="#ACB0C3"><b>GIK Operating instructions</b></font></a>
			</p>
</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Butterfly-Valves-DKL-V.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Buttefly Valves DKL..V</h3>
			<p><br/>Butterfly valves to set the flow rate of air on gas burners, prepared for attachment of a gear motor GT, for regulation ratios up to 1:10, with angle-of-rotation indicator.
			<br/><br/><b>DN:</b> 25 to 150,
			<br/>for fitting between two flanges
			<br/><b>pe:</b> max. 120 "WC (300 mbar),
			<br/>with manual adjustment, with square shaft for mounting a gear motor GT or with free shaft end,
			<br/><b>max. medium temperature:</b> 212&#176;F (100&#176;C) disc with clearance
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_gik_gikh.pdf" target="_blank"><font color="#ACB0C3"><b>DKL Brochure</b></font></a>
			</p>
</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Butterfly-Valves-DKL-H.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Buttefly Valves DKL..H</h3>
			<p><br/>Butterfly valves to set the flow rate of air on gas burners, prepared for attachment of a gear motor GT, for regulation ratios up to 1:10, with angle-of-rotation indicator.
			<br/><br/><b>DN:</b> 25 to 150, for fitting between two flanges
			<br/><b>pe:</b> max. 120 "WC (300 mbar),
			<br/>with manual adjustment, with square shaft for mounting a gear motor GT or with free shaft end
			<br/><b>max. medium temperature:</b> 212&#176;F (100&#176;C) disc with clearance. 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_gik_gikh.pdf" target="_blank"><font color="#ACB0C3"><b>DKL Brochure</b></font></a>
			</p>
</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Solenoid-Valve-VGP.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves VGP</h3>
			<p><br/>Gas valves for automatic shut-off, closed when de-energized, for gas and air, FM approved.
			<br/><br/><b>DN:</b> 15 to 80
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 2 or 5 psig (130 or 360 mbar)
			<br/>quick opening, quick closing or slow opening, quick closing
			<br/><b>mains voltage:</b> 120 V AC
			<br/><b>electrical connection:</b> terminals
			<br/><br/><b>The following versions are available:</b>
			<br/>valves with flow adjustment
			<br/>valves for closed position indicator assembly
			<br/>valves for visual indicator assembly 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_vgp-1.pdf" target="_blank"><font color="#ACB0C3"><b>VGP Brochure</b></font></a>
			</p>
</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Soleniod-Valve-VG-15-40_32.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves VG 15-40/32</h3>
			<p><br/>Gas valves for automatic shut-off, closed when de-energized, for gas and air, FM approved.
			<br/><br/><b>DN:</b> 15 to 80
			<br/><b>connection:</b> NPT thread or ANSI flange
			<br/><b>pe:</b> max. 2 or 5 psig (130 or 360 mbar)
			<br/>quick opening, quick closing or slow opening, quick closing
			<br/>mains voltage:</b> 120 V AC
			<br/><b>electrical connection:</b> terminals
			<br/><br/><b>The following versions are available:</b>
			<br/>valves with flow adjustment
			<br/>valves for closed position indicator assembly
			<br/>valves for visual indicator assembly
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/vg.pdf" target="_blank"><font color="#ACB0C3"><b>VG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_vg.pdf" target="_blank"><font color="#ACB0C3"><b>VG Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Solenoid-Valve-VG-40-100.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves VG 40-100</h3>
			<p><br/>Gas valves for automatic shut-off, closed when de-energized, for gas and air, FM approved.
			<br/><br/><b>DN:</b> 15 to 80
			<br/><b>connection:</b> NPT thread
			<br/>or ANSI flange
			<br/><b>pe:</b> max. 2 or 5 psig (130 or 360 mbar)
			<br/>quick opening, quick closing or slow opening, quick closing
			<br/><b>mains voltage:</b> 120 V AC
			<br/><b>electrical connection:</b> terminals
			<br/><br/><b>The following versions are available:</b>
			<br/>valves with flow adjustment
			<br/>valves for closed position indicator assembly
			<br/>valves for visual indicator assembly
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/vg.pdf" target="_blank"><font color="#ACB0C3"><b>VG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_vg.pdf" target="_blank"><font color="#ACB0C3"><b>VG Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Solenoid-Valve-for-Air-VR.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves for Air VR</h3>
			<p><br/>Solenoid valves for air, for high/low control, for cold-air operation on industrial burners, UL and CSA approved.
			<br/><br/><b>DN:</b> 40 to 65
			<br/><b>connection:</b> NPT thread
			<br/><b>pe:</b> max. 2 psig (130 mbar)
			<br/>quick opening, quick closing or slow opening, slow closing
			<br/><b>mains voltage:</b> 120 V AC
			<br/><b>electrical connection:</b> terminals with flow adjustment
			<br/><br/><b>The following versions are available:</b>
			<br/>valves with position indicator
			<br/>valves with bypass orifice
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/vr.pdf" target="_blank"><font color="#ACB0C3"><b>VR Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_vr.pdf" target="_blank"><font color="#ACB0C3"><b>VR Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Switch-for-Gas-DG-T.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Switches for Gas DG..T</h3>
			<p><br/>Diaphragm pressure switch with micro-switch for monitoring gas pressures in industry, for gas and air, also for biologically produced methane, with hand wheel for switching point adjustment, FM and UL approved.
			<br/><br/><b>Switching ranges:</b> 0.2 to 200 "WC (0.5 to 500 mbar)
			<br/>DG..T for positive pressure, negative pressure and differential pressure
			<br/><b>electrical connection:</b> terminals or standard plug with socket
			<br/><br/><b>The following versions are available:</b>
			<br/>pressure switch with gold-plated contacts for voltages < 30 V
			<br/>pressure switch with control lamp
			<br/>pressure switch with external adjustment
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Switch-for-Gas-DG-H_NT.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Switches for Gas DG..H/NT</h3>
			<p><br/>Diaphragm pressure switch with micro-switch for monitoring gas pressures in industry, for gas and air, also for biologically produced methane, with hand wheel for switching point adjustment, FM and UL approved.
			<br/><br/><b>Switching ranges:</b> 0.2 to 200 "WC (0.5 to 500 mbar)
			<br/>DG..HT locks off with rising pressure
			<br/>DG..NT locks off with falling pressure
			<br/><b>electrical connection:</b> terminals or standard plug with socket
			<br/><br/><b>The following versions are available:</b>
			<br/>pressure switch with gold-plated contacts for voltages < 30 V
			<br/>pressure switch with control lamp
			<br/>pressure switch with external adjustment 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Switch-for-Gas-DG-VT.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Switches for Gas DG..VT</h3>
			<p><br/>Diaphragm pressure switch with micro-switch (silicone-free) for monitoring gas pressures on industrial installations and OEM heating systems, for positive pressure, for gas, also for biologically produced methane, variable hand wheel switching point adjustment, UL listed.
			<br/><br/><b>Switching ranges:</b> 8 to 120 "WC (2 to 300 mbar)
			<br/><b>with external thread:</b> 1/4" NPT or 1/8" NPT, thread with sealant
			<br/><b>electrical connection:</b> standard socket, with change-over contact
			<br/><br/><b>The following versions are available:</b>
			<br/>pressure switch with gold-plated contacts for voltages < 24 V
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Switch-for-Air-DL-1-50ET.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Switches for Air DL 1-50ET</h3>
			<p><br/>Diaphragm pressure switch with micro-switch for monitoring air pressures in industry and trade, for positive pressure, negative pressure and differential pressure, for air and flue gas, FM and UL approved.
			<br/><br/><b>Switching ranges:</b> 0.08 to 20 "WC (0.2 to 50 mbar)
			<br/>DL..ET with tube connection and switching point set at factory
			<br/><b>electrical connection:</b> AMP plug, terminals or standard sockets
			<br/><br/><b>The following versions are available:</b>
			<br/>pressure switch with gold-plated contacts for voltages < 30 V
			<br/>pressure switch with control lamp
			<br/>pressure switch with external adjustment
			<br/>pressure switch with angle bracket
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/dl.pdf" target="_blank"><font color="#ACB0C3"><b>DL Brochure</b></font></a>
			</p>
</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Switches-for-Air-DL-3AT.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Switches for Air DL 3AT</h3>
			<p><br/>Diaphragm pressure switch with micro-switch for monitoring air pressures in industry and trade, for positive pressure, negative pressure and differential pressure, for air and flue gas, FM and UL approved.
			<br/><br/><b>Switching ranges:</b> 0.08 to 20 "WC (0.2 to 50 mbar)
			<br/>DL..AT with threaded and tube connection and hand wheel for switching point adjustment
			<br/><b>electrical connection:</b> AMP plug, terminals or standard sockets
			<br/><br/><b>The following versions are available:</b>
			<br/>pressure switch with gold-plated contacts for voltages < 30 V
			<br/>pressure switch with control lamp
			<br/>pressure switch with external adjustment
			<br/>pressure switch with angle bracket
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/dl.pdf" target="_blank"><font color="#ACB0C3"><b>DL Brochure</b></font></a>
			</p>
</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Solenoid-Valves-for-Gas-VS-N.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves for Gas VS..N</h3>
			<p><br/>Gas safety valves for automatic shut-off, closed when de-energized, for gas and air, FM and ETL approved.
			<br/><br/><b>Size:</b> 1 to 3
			<br/><b>DN:</b> 15 to 50
			<br/><b>connection:</b> MODULINE system
			<br/><b>pe:</b> max. 2 or 5 psig (140 or 360 mbar)
			<br/>quick opening, quick closing or slow opening, quick closing
			<br/><b>mains voltage:</b> 120 V AC
			<br/>electrical connection:</b> terminals
			<br/><b>Accessories:</b>
			<br/>Visual indicator
			<br/>Closed position indicator
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/vs.pdf" target="_blank"><font color="#ACB0C3"><b>VS Brochure</b></font></a>
			</p>
</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Solenoid-Valves-for-Gas-VS-L.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Solenoid Valves for Gas VS..L</h3>
			<p><br/>Gas safety valves for automatic shut-off, closed when de-energized, for gas and air, FM and ETL approved.
			<br/><br/><b>Size:</b> 1 to 3
			<br/><b>DN:</b> 15 to 50
			<br/><b>connection:</b> MODULINE system
			<br/><b>pe:</b> max. 2 or 5 psig (140 or 360 mbar)
			<br/>quick opening, quick closing or slow opening, quick closing
			<br/><b>mains voltage:</b> 120 V AC
			<br/>electrical connection:</b> terminals
			<br/><b>Accessories:</b>
			<br/>Visual indicator
			<br/>Closed position indicator
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/vs.pdf" target="_blank"><font color="#ACB0C3"><b>VS Brochure</b></font></a>
			</p>
</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Governers-with-Solenoid-Valve-GVS-GVI.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Governors with Solenoid Valve GVS, GVI</h3>
			<p><br/>For safeguarding and control of gas pressures in gas inlet and burner lines, for gas and air, FM and ETL approved.
			<br/><br/><b>GVS:</b> universal governor
			<br/><b>GVI:</b> air/gas ratio control for maintaining a constant gas/air ratio
			<br/><b>size:</b> 1 to 3
			<br/><b>DN:</b> 15 to 50
			<br/><b>connection:</b> MODULINE system
			<br/><b>pe:</b> max. 2 psig (140 mbar)
			<br/><b>mains voltage:</b> 120 V AC
			<br/><b>electrical connection:</b> terminals
			</p>
</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Accessories-for-MODULINE-FL-ES.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>FL..ES
			</br>Accessories for MODULINE</h3>
			<p><br/><b>Inlet flanges with or without strainer for pipes:</b> 3/8 to 2 NPT
			<br/><br/><b>Outlet flanges for pipes:</b> 3/8 to 2 NPT
			<br/><br/>Filter and strainer modules for intermediate fitting, for protecting downstream devices.
			<br/>Orifice modules for adjusting the maximum flow rate to the respective combustion process.
			<br/><br/>Connecting sets consisting of screws, nuts and sealing ring, for connecting two MODULINE controls.
			<br/><br/>Intermediate element with threaded connections 1/2 to 1/4 NPT for ignition gas branch and pressure gauge connection.
			<br/><br/>Attachment bracket 1/4 NPT for locked installation of pressure gauges and other accessories.
			</p>
</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Accessories-for-MODULINE-BV.gif"/></div>
			<div id="PartsContent"><h3>FL..BV
			</br>Accessories for MODULINE</h3>
			<p><br/><b>Inlet flanges with or without strainer for pipes:</b> 3/8 to 2 NPT
			<br/><br/><b>Outlet flanges for pipes:</b> 3/8 to 2 NPT
			<br/><br/>Filter and strainer modules for intermediate fitting, for protecting downstream devices.
			<br/><br/>Orifice modules for adjusting the maximum flow rate to the respective combustion process.
			<br/><br/>Connecting sets consisting of screws, nuts and sealing ring, for connecting two MODULINE controls.
			<br/><br/>Intermediate element with threaded connections 1/2 to 1/4 NPT for ignition gas branch and pressure gauge connection.
			<br/><br/>Attachment bracket 1/4 NPT for locked installation of pressure gauges and other accessories.
			</p>
</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Accessories-for-MODULINE-FL-A.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>FL..A
			</br>Accessories for MODULINE</h3>
			<p><br/><b>Inlet flanges with or without strainer for pipes:</b> 3/8 to 2 NPT
			<br/><br/><b>Outlet flanges for pipes:</b> 3/8 to 2 NPT
			<br/><br/>Filter and strainer modules for intermediate fitting, for protecting downstream devices.
			<br/><br/>Orifice modules for adjusting the maximum flow rate to the respective combustion process.
			<br/><br/>Connecting sets consisting of screws, nuts and sealing ring, for connecting two MODULINE controls.
			<br/><br/>Intermediate element with threaded connections 1/2 to 1/4 NPT for ignition gas branch and pressure gauge connection.
			<br/><br/>Attachment bracket 1/4 NPT for locked installation of pressure gauges and other accessories.
			</p>
</div></div>
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burner-Control-Units-BCU-400.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burner Control Units BCU 400</h3>
			<p><br/><b>Safety time for start-up:</b> 3, 5 or 10 s
			<br/><b>safety time for operation:</b> 1 or 2 s
			<br/><b>mains voltage:</b> 230 V AC or 115 V AC, for earthed or unearthed mains
			<br/><br/>Automatic burner control unit, ignition transformer, Manual/Automatic mode and display of operating and fault statuses in a metal housing, for controlling and monitoring gas burners, for continuous operation with ionisation control, for intermittent operation with UV control (with UV sensors of type UVS) or as a variant for continuous operation with UV control (with UV sensor UVD 1), single-electrode operation possible, setting of various operating modes via optical interface using separate software, FM approved.
			<br/><br/>BCU 460 for directly ignited burners of unlimited capacity, with air valve control (optional).
			<br/><br/>BCU 465 with extended air control.
			<br/><br/>BCU 480 for pilot and main burners of unlimited capacity, with air valve control.
			</p>
</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Flame-Relay-IFW-15T-N.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Flame Relay IFW 15T-N</h3>
			<p><br/>Flame detector for multi-flame control in conjunction with IFS 110IM or for flame signalling, for intermittent operation, ionization or UV control, FM approved.
			<br/>For grounded or non grounded mains
			<br/><b>mains voltage:</b> 110/120 V AC
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ifw15.pdf" target="_blank"><font color="#ACB0C3"><b>IFW 15 Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_ifw15.pdf" target="_blank"><font color="#ACB0C3"><b>IFW 15 Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Flame-Relay-PFF-704.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Flame Relay
			<br/>PFF 704</h3>
			<p><br/>Flame detector in 19" standard rack for multi-flame control in conjunction with PFS or for flame signalling.
			<br/><br/><b>PFF 704:</b> for intermittent operation
			<br/>ionization or UV control, FM approved. With 4 independent flame amplifiers
			<br/><b>mains voltage:</b> switchable from 220/240 V AC to 110/120 V AC, for grounded or non grounded mains
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_pff_704-1.pdf" target="_blank"><font color="#ACB0C3"><b>PFF 704 Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Automatic-Burner-Control-Units-IFS-110IM.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Automatic Burner Control Units IFS 110IM</h3>
			<p><br/>Automatic burner control units for controlling and monitoring gas burners,
			for intermittent operation, ionization or UV control. Single-electrode operation possible, multi-flame control possible, for earthed mains, FM and CSA approved.
			For grounded mains:
			<br/>main gas after safety time, fault lock-out or restart following flame failure
			<br/><br/><b>trial for ignition period:</b> 3, 5 or 10 s, flame failure response time for V2:</b> 1 or 2 s
			<br/><b>flame failure response time for V1:</b> 1 or 2 s
			<br/><b>mains voltage:</b> 110/120 V AC
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ifs110.pdf" target="_blank"><font color="#ACB0C3"><b>IFS 110IM Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_ifs_110im.pdf" target="_blank"><font color="#ACB0C3"><b>IFS 110IM Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Automatic-Burner-Control-Units-IFS-258.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Automatic Burner Control Units IFS 258</h3>
			<p><br/>Automatic burner control units for controlling and monitoring gas burners, for intermittent operation, ionization or UV control, cut-off threshold for flame current variable, measuring sockets for uninterrupted flame current measurement, single-electrode operation possible, signal contacts for operation and faults, FM and CSA approved.
			<br/>Immediate fault lock-out or restart following flame failure
			<br/><br/><b>trial for ignition period:</b> 3, 5 or 10 s
			<br/><b>flame failure response time:</b> 2 s
			<br/><b>mains voltage:</b> 110 V AC, 115 V AC, 200 V AC or 230 V AC, for grounded or non grounded mains
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_ifs258.pdf" target="_blank"><font color="#ACB0C3"><b>IFS 258 Brochure</b></font></a>
			</p>
</div></div>
<div id="35" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Automatic-Burner-Control-Units-PFS-778,-748.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Automatic Burner Control Units PFS 778, 748</h3>
			<p><br/>Automatic burner control units in 19" standard rack for controlling and monitoring gas burners, for intermittent operation, ionization or UV control, FM approved.
			<br/><br/><b>PFS 778:</b> main gas after safety time or main gas after flame proving period, switchable for continuously controlled burners, single-electrode operation possible,
			<br/><br/><b>PFS 748:</b> main gas following flame signal for burners in intermittent operation high/low
			<br/>restart or immediate fault lock-out
			<br/>following flame failure
			<br/>switchable air valve control possible
			<br/>multi-flame control possible
			<br/><br/><b>trial for ignition period:</b> 3, 5 or 10 s
			<br/><b>flame failure response time for V2:</b> 2 s
			<br/><b>flame failure response time for V1:</b> 2 s
			<br/><b>mains voltage:</b> switchable from 220/240 V AC to 110/120 V AC, for grounded or non grounded mains,
			<br/>controlled by standard 24 V DC signals.
			</p>
</div></div>
<div id="36" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Relay-Module-PfR-704.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Relay Module PFR 704</h3>
			<p><br/>Relay card for 19" standard rack, for signal coupling, FM approved.
			<br/>Controlled by 24 V DC/AC, 110/120 V AC or 220/240 V AC.
			<br/>With 4 independent relays. 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_pfp_pfr-1.pdf" target="_blank"><font color="#ACB0C3"><b>PFP 700 Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="37" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Power-Supply-PFP-700.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Power Supply PFP 700</h3>
			<p><br/>Power supply in 19" standard rack for supplying the control inputs on automatic burner control units PFS, FM approved.
			<br/><br/><b>Output rating:</b> 24 V DC, 600 mA,
			<br/><b>mains voltage:</b> switchable from 220/240 V AC to 110/120 V AC. 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_pfp_pfr-1.pdf" target="_blank"><font color="#ACB0C3"><b>PFP 700 Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="38" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burners-for-Gas-BIC-65.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burners for Gas BIC 65</h3>
			<p><br/>Burners with cast steel housing, connection for ceramic tube provided.
			<br/><br/><b>Burner size:</b> 50 to 140
			<br/><b>capacity:</b> 34,000 to 1,228,000 BTU/h
			<br/>(10 to 360 kW)
			<br/><b>flame shape:</b> normal flame or long, soft flame
			<br/><b>gas type:</b> natural gas, town gas or LPG
			<br/>length of burner tube:</b> depending on ceramic tube
			<br/><b>connection:</b>
			<br/><b>gas:</b> thread
			<br/><b>air:</b> thread or flange
			<br/>The following versions are available:</b> burner with integrated ignition lance. 
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="39" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burners-for-Gas-BIC-140.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burners for Gas BIC 140</h3>
			<p><br/>Burners with cast steel housing, connection for ceramic tube provided.
			<br/><br/><b>Burner size:</b> 50 to 140
			<br/><b>capacity:</b> 34,000 to 1,228,000 BTU/h
			<br/>(10 to 360 kW)
			<br/><b>flame shape:</b> normal flame or long, soft flame
			<br/><b>gas type:</b> natural gas, town gas or LPG
			<br/><b>length of burner tube:</b> depending on ceramic tube
			<br/><b>connection:</b>
			<br/><b>gas:</b> thread
			<br/><b>air:</b> thread or flange
			<br/>The following versions are available:</b> burner with integrated ignition lance
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="40" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burners-for-Gas-BIO.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burners for Gas BIO</h3>
			<p><br/>Burners with cast steel housing, steel burner tube.
			<br/><br/><b>Burner size:</b> 50 to 140
			<br/><b>capacity:</b> 137,000 to 1,536,000 BTU/h
			<br/>(40 to 450 kW)
			<br/><b>flame shape:</b> normal flame, long, soft flame or short, flat flame
			<br/><b>gas type:</b> natural gas, town gas or LPG
			<br/><b>length of burner tube:</b> 2 to 40" (50 to 1000 mm)
			<br/><b>connection:</b>
			<br/><b>gas:</b> thread
			<br/><b>air:</b> thread or flange
			<br/>The following versions are available:</b> supply, burner with integrated ignition lance
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="41" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burners-for-Gas-ZIO-165.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burners for Gas ZIO 165</h3>
			<p><br/>Burners with steel-plate housing, steel burner tube.
			<br/><b>Burner size:</b> 165 to 200
			<br/><b>capacity:</b> 2,150,000 to 3,413,000 BTU/h (630 to 1000 kW)
			<br/><b>flame shape:</b> normal flame, long, soft flame or short, flat flame
			<br/><b>gas type:</b> natural gas, town gas or LPG
			<br/><b>length of burner tube:</b> 2 to 40" (50 to 1000 mm)
			<br/><b>connection:</b>
			<br/><b>gas:</b> thread
			<br/><b>air:</b> flange
			<br/><b>The following versions are available:</b>
			<br/>burner with integrated ignition lance
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/pr3_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dg.pdf" target="_blank"><font color="#ACB0C3"><b>DG Operating Instructions</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_dg_gb.pdf" target="_blank"><font color="#ACB0C3"><b>DG Technical Information</b></font></a>
			</p>
</div></div>
<div id="42" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Burner-with-Intergrated-Recuperator-BICR.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Burner with Integrated Recuperator BICR</h3>
			<p><br/>Recuperator burner for indirectly or directly heated furnace systems.
			<br/><br/><b>Burner size:</b> 65/50, 80/65, 100/80
			<br/><b>Capacity:</b> 70,000 to 280,000 BTU/h (21 to 82 kW)
			<br/><b>flame shape:</b> long, soft flame
			<br/><b>gas type:</b> natural gas or LPG
			<br/><b>length of burner tube:</b> depending on ceramic tube
			<br/><b>connection:</b>
			<br/><b>gas:</b> thread
			<br/><b>air:</b> thread
			<br/><b>max. furnace temperature:</b> 950&#176;C
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_bicr.pdf" target="_blank"><font color="#ACB0C3"><b>BICR Technical Information</b></font></a>
			</p>
</div></div>
<div id="43" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Ionization-pilot-ZMI.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Ionization Pilot ZMI</h3>
			<p><br/>Pilot burner with one ignition/monitoring electrode for safe ignition of gas burners in conjunction with automatic burner control units IFS or PFS, with forced air supply.
			<br/><br/><b>Capacity:</b> 2,800 to 11,500 BTU/h (0.8 to 3.4 kW)
			<br/><b>gas type:</b> natural gas, town gas
			<br/>or LPG
			<br/><b>length of burner tube:</b> 6 to 38" (150 to 1000 mm)
			<br/><b>connection:</b> NPT thread
			</p>
</div></div>
<div id="44" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Excess-Air-Burner-BIC-L.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Excess Air Burner BIC..L</h3>
			<p><br/>For all applications requiring precise temperature control and consistent product quality. The BIC..L burner is perfectly designed for use in the tunnel kilns and intermittent systems as used in the ceramics industry. A single burner type enables you to follow all the temperature profile requirements of the kiln. Thanks to the high excess air capability of up to approx. 1500% high flue gas temperatures of up to approx. 100&#176;C can be reached in near-stoichiometric burner operation with maximum excess air. With reliable ignition over the entire burner output range this accommodates the furnace operator's wish for a simple structured gas/air control.
			<br/><br/><b>Burner size:</b> 80, 100 and 140
			<br/><b>capacity:</b> 256,000, 512,000 and 1,024,000 BTU/h (75, 150 and 300 kW)
			<br/><b>gas type:</b> natural gas or LPG
			<br/><b>length of burner tube:</b> depending on ceramic tube
			<br/><b>connection:</b> thread or flange
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba1_bicl.pdf" target="_blank"><font color="#ACB0C3"><b>BIC Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="45" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pilot-Burner-ZIO-40.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pilot Burner ZIO 40</h3>
			<p><br/>Pilot burner with forced air supply for safe ignition of gas burners in conjunction with automatic burner control units IFS or PFS.
			<br/><br/><b>Capacity:</b> 68,000 BTU/h (20 kW), gas type:</b> natural gas, town gas or LPG
			<br/><b>length of burner tube:</b> 4 to 38"
			<br/>(100 to 1000 mm)
			<br/><b>connection:</b> thread
			</p>
</div></div>
<div id="46" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pilot-Burners-ZKIH.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pilot Burners ZKIH</h3>
			<p><br/>Pilot burner with forced air supply for safe ignition of gas burners in conjunction with automatic burner control units IFS or PFS.
			<br/><br/><b>Capacity:</b> 6830 to 23900 BTU/h (2 to 7 kW)
			<br/><b>gas type:</b> natural gas, town gas
			<br/>or LPG
			<br/><b>length of burner tube:</b> 6/4 to 40/4 inch (150/100 to 1000/100 mm)
			<br/><b>connection:</b> thread
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/ba_zai_zmi_zkih.pdf" target="_blank"><font color="#ACB0C3"><b>ZKIH Operating Instructions</b></font></a>
			</p>
</div></div>
<div id="47" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/UV-detectors-UVS-6.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>UV Detectors UVS 6</h3>
			<p><br/>UV detectors for monitoring gas burners, FM approved
			<br/><br/><b>UVS 6:</b> with connection terminals, with integrated heat protection, IP 55
			</p>
</div></div>
<div id="48" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/UV-detecors-UVS-8.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>UV Detectors UVS 8</h3>
			<p><br/>UV detectors for monitoring gas burners, FM approved
			<br/><br/><b>UVS 8:</b> with non-detachable silicone cable, with integrated heat protection, IP 40
			</p>
</div></div>
<div id="49" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Flow-Meters-DM-N.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Flow Meters DM..N</h3>
			<p><br/>Turbine wheel meters for measuring gas flow on gas consumption equipment, for gas burner adjustment, for gas and air.
			<br/><br/><b>DM:</b> mechanical counter head
			<br/><b>measuring range:</b> 50 to 35000 ft3/h
			<br/><b>connection:</b> thread or installation between two flanges
			<br/><b>pe:</b> max. 175 psig (12 bar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Operating Instructions</b></font></a>
			</p>
			</div></div>
<div id="50" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Flow-Meters-DM-W.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Flow Meters DM..W</h3>
			<p><br/>Turbine wheel meters for measuring gas flow on gas consumption equipment, for gas burner adjustment, for gas and air.
			<br/><br/><b>DM:</b> mechanical counter head
			<br/><b>measuring range:</b> 50 to 35000 ft3/h
			<br/><b>connection:</b> thread or installation between two flanges
			<br/><b>pe:</b> max. 175 psig (12 bar)
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Operating Instructions</b></font></a>
			</p>
			</div></div>
<div id="51" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Flow-Meters-DE-W.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Flow Meters DE..W</h3>
			<p><br/>Turbine wheel meters for measuring gas flow on gas consumption equipment, for gas burner adjustment, for gas and air.
			<br/><br/><b>DE:</b> electronic counter head
			<br/><b>measuring range:</b> 50 to 35000 ft3/h
			<br/><b>connection:</b> thread or installation between two flanges
			<br/><b>pe:</b> max. 175 psig (12 bar)	
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/ba1_dm_de.pdf" target="_blank"><font color="#ACB0C3"><b>DM Operating Instructions</b></font></a>
			</p>
			</div></div>
<div id="52" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Gauges-KFM.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Gauges KFM</h3>
			<p><br/>Pressure gauges with capsule elements and Bourdon tubes for pressure display, resistant to positive pressure, with zero point correction, for gas and air.
			<br/><br/><b>Measuring ranges:</b>
			<br/><b>KFM:</b> 0 to 5 psig (0 to 400 mbar)
			<br/><b>diameter:</b> 63 or 100 mm
			</p>
			</div></div>
<div id="53" class="popup_block_Parts">	
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Pressure-Gauges-RFM.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Pressure Gauges RFM</h3>
			<p><br/>Pressure gauges with capsule elements and Bourdon tubes for pressure display, resistant to positive pressure, with zero point correction, for gas and air.
			<br/><br/><b>Measuring ranges:</b>
			<br/><b>RFM:</b> 0 to 230 psig (0 to 16 bar)
			<br/><b>diameter:</b> 63 or 100 mm
			</p>
			</div></div>
<div id="54" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Orifice-Assemablies-FLS.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Orifice Assemablies FLS</h3>
			<p><br/>The appropriate differential pressure can be checked using two pressure test points before and after the orifice. 
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Krom_Schroder/Ball-Valves-AKT.gif" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></div>
			<div id="PartsContent"><h3>Ball Valves AKT</h3>
			<p><br/>Manual valves for manual shut-off, for gas and air, UL listed and CSA approved.
			<br/><br/><b>DN:</b> 10 to 50
			<br/><b>connection:</b> thread
			<br/><b>pe:</b> max. 125 psig (8.8 bar) brass housing
			<br/><br/><br/>
			<b><font color="#494A4A">Product Literature</font></b>
			<br/><br/><a href="Krom-Schroder-Parts-pdfs/tpb_uvs.pdf" target="_blank"><font color="#ACB0C3"><b>AKT Product Brochure</b></font></a>
			<br/><a href="Krom-Schroder-Parts-pdfs/iti_akt_gb-1.pdf" target="_blank"><font color="#ACB0C3"><b>AKT Technical Information</b></font></a>
			</p>
		</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Ball-Valves-AKT_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Filters-GFK-A_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Filters-GFK-N_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Pressure-Regulators-GDJ-15_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Pressure-Regulators-GDJ-20-50_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Pressure-Regulators-VGBF-A_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas-Pressure-Regulators-VGBF-N_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Gas_Air-Ratio-Regulators-GIK_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV-25_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV-40_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Safety-Overpressure-Shut-Valves-JSAV-Relief-Valves-VSBV-JSAV50-100_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Variable-Gas_Air-Ratio-Regulators-GIKH_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Butterfly-Valves-DKL-V_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Butterfly-Valves-DKL-H_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Solenoid-Valve-VGP_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Soleniod-Valve-VG-15-40_32_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Solenoid-Valve-VG-40-100_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Solenoid-Valve-for-Air-VR_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Switch-for-Gas-DG-T_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Switch-for-Gas-DG-H_NT_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Switch-for-Gas-DG-VT_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Switch-for-Air-DL-1-50ET_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Switches-for-Air-DL-3AT_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Solenoid-Valves-for-Gas-VS-N_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Solenoid-Valves-for-Gas-VS-L_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Governers-with-Solenoid-Valve-GVS-GVI_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Accessories-for-MODULINE-FL-ES_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Accessories-for-MODULINE-BV_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Accessories-for-MODULINE-FL-A_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burner-Control-Units-BCU-400_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Flame-Relay-IFW-15T-N_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Flame-Relay-PFF-704_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Automatic-Burner-Control-Units-IFS-110IM_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Automatic-Burner-Control-Units-IFS-258_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="35" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Automatic-Burner-Control-Units-PFS-778,-748_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="36" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Relay-Module-PfR-704_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="37" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Power-Supply-PFP-700_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="38" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burners-for-Gas-BIC-65_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="39" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burners-for-Gas-BIC-140_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="40" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burners-for-Gas-BIO_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="41" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burners-for-Gas-ZIO-165_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="42" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Burner-with-Intergrated-Recuperator-BICR_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="43" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Ionization-pilot-ZMI_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="44" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Excess-Air-Burner-BIC-L_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="45" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pilot-Burner-ZIO-40_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="46" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pilot-Burners-ZKIH_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="47" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/UV-detectors-UVS-6_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="48" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/UV-detecors-UVS-8_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="49" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Flow-Meters-DM-N_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="50" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Flow-Meters-DM-W_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="51" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Flow-Meters-DE-W_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="52" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Gauges-KFM_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="53" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Pressure-Gauges-RFM_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		<li><a href="#?w=400" rel="54" class="Product"><img src="Parts_by_Man_OK_By_Jon/Krom_Schroder/thumbnails/Orifice-Assemablies-FLS_Thumbnail.gif" border="0" alt="KromSchroder Combustion Parts" title="KromSchroder Combustion Parts"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>


