<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Maxon combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Maxon,8000 Series Gas Valves,ACTIONAIR Gas and Oil Valves,Maxon Gas Electro-Mechanical Valves,Circular INCINO-PAK Natural Gas Low Temp Burners,CYCLOMAX LOW NOX Natural Gas Low Temp Burners,INDIPAK Natural Gas Low Temp Burners,INDITHERM Natural Gas Low Temp Burners,LINOFLAME Industrial Burner,M-PAKT Ultra Low NOx Burners,MEGAFIRE Industrial Burner,OPTIMA SLS Burner Natural Gas Burners,OVENPAK 400 Series,OVENPAK 500 Series,OVENPAK II 400 Series,OVENPAK LE Burner,RADMAX ULTRA Low NOx Radiant Line Burners,Maxon TUBE-O-FLAME Natural Gas Low Temp Burners,TUBE-O-THERM Natural Gas Low Temp Burners,VALUPAK Natural Gas Low Temp Burners,XPO Indirect Burner Low NOx Burners" />
<title>ETTER Engineering - Maxon Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="MaxonLogoLarge"></div>
<div id="SensusText">Maxon's full line of industrial burners and combustion 
equipment includes gas burners, oil burners, gas valves, shut off valves, 
flow control valves, and low NOx burners. All of their combustion systems 
and equipment are designed and engineered with the customer's needs in 
mind. As the first industrial burner manufacturer to receive ISO 9001 
certification, Maxon produces nothing but high quality products, which 
are held to the highest standards in the industry.</div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/8000_Series_Gas_Valves.jpg" alt="Maxon 8000 Series Gas Valves" title="Maxon 8000 Series Gas Valves"/></div>
			<div id="PartsContent"><h3>8000 Series Gas Valves
			<br/><font color="#445679" size="2">Gas Shut Off Valves Hazardous Area</font></h3>
			<p>MAXON Series 8000 Air Actuated Shut Off Valves combine a unique space-saving design with a 
			maintenance-free bonnet seal and a replaceable actuator for easy installation and smooth, 
			trouble-free operation. This gas valve's quick exhaust and powerful closing spring provide 
			valve closure in less than one second and reliable, long-life operation. Valve seats are 
			micro-lapped metals. MAXON gas valves ensure positive sealing with our trademark durability. 
			Insist on MAXON shut off valves for your safety-critical combustion systems.
			<br/><br/>Unlike other pneumatic valves, MAXON valves require no maintenance. More importantly, 
			MAXON seats require no regular tightening of valve packings like ball valve designs The Series 
			8000 Valve has been FM, CSA and CE rated for hazardous locations. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-8000valve/E-8000_valve-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-8000valve/E-8000_valve-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_maintenance.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Maintenance</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-leak_testing.pdf" target="_blank"><font color="#ACB0C3"><b>Leak Testing</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_sizing_charts.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Sizing Charts</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/ACTIONAIR_Gas_and_Oil_Valves.jpg" alt="Maxon ACTIONAIR Gas and Oil Valves" title="Maxon ACTIONAIR Gas and Oil Valves"/></div>
			<div id="PartsContent"><h3>ACTIONAIR&reg; Gas and Oil Valves
			<br/><font color="#445679" size="2">Electro-Mechanical Gas Shut OffValves</font></h3>
			<p>These air actuated valves shut off gas or oil lines in less than one second and the Series 
			1000 and Series 2000 are approved for General Purpose and Hazardous Duty locations by FM 
			(Factory Mutual) and CGA (Canadian Gas Association). Powerful spring-loaded air operators 
			permit much higher differential pressure ratings.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/bulletin_6300_10-10.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-ep-actionair.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-ep-actionair.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_maintenance.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Maintenance</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-ep-actionair.pdf" target="_blank"><font color="#ACB0C3"><b>Leak Testing</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_sizing_charts.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Sizing Charts</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/Gas_Electro_Mechanical_Valves.jpg" alt="Maxon Gas Electro-Mechanical Valves" title="Maxon Gas Electro-Mechanical Valves"/></div>
			<div id="PartsContent"><h3>Gas Electro-Mechanical Valves
			<br/><font color="#445679" size="2">Electro-Mechanical Gas Shut Off Valves</font></h3>
			<p>These electrically actuated valves shut off gas or oil lines in less than one second. 
			All shut off valves come equipped with MAXON's long-lasting metal-to-metal seating. 
			A variety of optional body materials and body connections provide reliable operation 
			even for highly corrosive fuels and for oxygen. Application flexibility is provided 
			with 3/4" through 6" diameter line sizes, Cv flow factors up to 1230, and line pressures 
			up to 125 PSIG (8.6 bar). 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Gas_Electro_Mechanical/E-gas_em_valves-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Gas_Electro_Mechanical/E-gas_em_valves-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_maintenance.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Maintenance </b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-leak_testing.pdf" target="_blank"><font color="#ACB0C3"><b>Leak Testing </b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_sizing_charts.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Sizing Charts</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/Circular_INCINO_PAK.jpg" alt="Maxon Circular INCINO-PAK Natural Gas Low Temp Burners" title="Maxon Circular INCINO-PAK Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>Circular INCINO-PAK&reg;
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>Circular INCINO-PAK&reg; Industrial Burners have been specifically designed for the 
			incineration of combustible gaseous effluents inside cylindrical combustion chambers. 
			With INCINO-PAK&reg; direct combustion, higher efficiencies can be achieved than 
			with powered burner systems. The special cone-type COMBUSTIFUME&reg; Natural Gas 
			Burners provide easy outside access to the raw gas pilot, ignitor, and flame monitoring 
			components. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-circular.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-circular.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-circular.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/CYCLOMAX_LOW_NOX.jpg" alt="Maxon CYCLOMAX LOW NOX Natural Gas Low Temp Burners" title="Maxon CYCLOMAX LOW NOX Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>CYCLOMAX&reg; LOW NOX
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>The CYCLOMAX&reg; Low NOx Burner provides clean combustion with NOx emission levels 
			below 25 ppm and CO levels less than 75 ppm. Lower emissions may be possible based on 
			application. The integral fan version of this industrial burner is available in 5 sizes, 
			up to 3,700,000 Btu/hr (1080 kW), and the EBMRV version is available in 4 sizes, up to 
			7,400,000 Btu/hr (2165 kW). 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/603a_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/603A_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/603A_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/INDIPAK.jpg" alt="Maxon INDIPAK Natural Gas Low Temp Burners" title="Maxon INDIPAK Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>INDIPAK
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>MAXON INDIPAK natural gas burners are value engineered, nozzle-mix burners suited for 
			indirect fired applications in heat exchangers and fire tubes. The small, compact design 
			of this industrial burner produces outstanding turndown ratios of up to 25:1. Low NOx and 
			low CO production makes Indipak suitable for installation in most every area of the world. 
			Easy installation is provided with a complete packaged design including pressure regulator, 
			control actuator, and integral fan. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-indipak.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-indipak.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/INDITHERM.jpg" alt="Maxon INDITHERM Natural Gas Low Temp Burners" title="Maxon INDITHERM Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>INDITHERM&reg;
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>MAXON INDITHERM&reg; industrial burners are nozzle-mix, natural gas burners suited for 
			indirect fired applications in heat exchangers and fire tubes. The rugged design with 
			integral air and gas valves produces outstanding turndown ratios of up to 25:1. Low NOx 
			and low CO production make INDITHERM&reg; ideal for installation in most areas of the world. 
			INDITHERM&reg; is available with an integral fan or with flanged inlet to be used with a 
			separate blower. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Inditherm/E-inditherm-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Inditherm/E-inditherm-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/LINOFLAME_Industrial_Burner.jpg" alt="Maxon LINOFLAME Industrial Burner" title="Maxon LINOFLAME Industrial Burner"/></div>
			<div id="PartsContent"><h3>LINOFLAME&reg; Industrial Burner
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>MAXON's style A, B & C LINOFLAME&reg; Industrial Burners produce short, ribbon-type 
			flames by modular-designed section with customized drillings. Our type "VF" LINOFLAME&reg; 
			Gas Burners are cast iron, V-faced, modular sections incorporating standardized drillings 
			for interchangeability. Maxon LINOFLAME&reg; Burners are useful for foam laminating, 
			dip tanks, hot oil cooking, glass surface treatments, and general air heating. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-linoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-linoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-linoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/M-PAKT_Ultra_low_NOx.jpg" alt="Maxon M-PAKT Ultra Low NOx Burners" title="Maxon M-PAKT Ultra Low NOx Burners"/></div>
			<div id="PartsContent"><h3>M-PAKT&reg; Ultra
			<br/><font color="#445679" size="2">Low NOx Burners</font></h3>
			<p>Typical MAXON quality and reliability is found in the M-PAKT&reg; Ultra Low NOx Burners, 
			which provide the world's lowest levels of NOx and CO. NOx is typically < 9 ppm while CO 
			is maintained < 20 ppm in most applications. The M-PAKT&reg; low NOx burner is suitable 
			for industrial air heating for ovens and dryers for paint finishing, paper making, food 
			baking, textile production, grain drying, and make-up air heating. With optional sleeve 
			materials, M-PAKT&reg; burners substantially reduce emissions in oxidizers, incinerators, 
			heat exchangers and process heaters.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-M-PAKT/E-MPakt-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview </b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-M-PAKT/E-MPakt-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/MEGAFIRE_Industrial_Burner.jpg" alt="Maxon MEGAFIRE Industrial Burner" title="Maxon MEGAFIRE Industrial Burner"/></div>
			<div id="PartsContent"><h3>MEGAFIRE&reg; Industrial Burner
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>The MEGAFIRE&reg; Industrial burner cleanly burns #2 oil, propane, or natural gas with 
			quiet operation and exceedingly low horsepower requirements. Standard heat releases are 
			available to 45,000,000 BTU/hr (13,000 kW), providing excellent application flexibility. 
			Dual fuel capability of this low temp burner provides reliable heat to kilns, dryers, 
			process heaters as well as numerous other industrial applications. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-megafire.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-megafire.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-megafire.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/OPTIME_SLS_Burner.jpg" alt="Maxon OPTIMA SLS Burner Natural Gas Burners" title="Maxon OPTIMA SLS Burner Natural Gas Burners"/></div>
			<div id="PartsContent"><h3>OPTIMA&#8482; SLS Burner
			<br/><font color="#445679" size="2">Natural Gas Burners</font></h3>
			<p>&#149; Extremely Clean, reliable heat with ultra 
			<br/>&nbsp;&nbsp;Low NOx and CO production for ease of 
			<br/>&nbsp;&nbsp;air permitting and environmental 
			<br/>&nbsp;&nbsp;compliance
			<br/>&#149; Large capacity heat releases with a 
			<br/>&nbsp;&nbsp;compact, robust flame geometry
			<br/>&#149; Intelligent ratio control for reliable 
			<br/>&nbsp;&nbsp;operation and optimal fuel efficiency
			<br/>&#149; Direct factory operational support with 
			<br/>&nbsp;&nbsp;optional communication gateway
			<br/>&#149; All steel and high temperature alloy 
			<br/>&nbsp;&nbsp;construction for reduced weight and 
			<br/>&nbsp;&nbsp;increased durability
			<br/>&#149; Fuel flexible with natural gas, propane, 
			<br/>&nbsp;&nbsp;and butane capability. Contact MAXON for 
			<br/>&nbsp;&nbsp;other fuels
			<br/>&#149; Configurable for dryers, air heaters, ovens, 
			<br/>&nbsp;&nbsp;kilns, process heaters, paper machines, 
			<br/>&nbsp;&nbsp;and a variety of other industrial 
			<br/>&nbsp;&nbsp;heating equipment.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Optima/E-optima-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-Optima/E-optima-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/OVENPAK_400_Series.jpg" alt="Maxon OVENPAK 400 Series" title="Maxon OVENPAK 400 Series"/></div>
			<div id="PartsContent"><h3>OVENPAK&reg; 400 Series
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>MAXON's OVENPAK&reg; 400 Series is the world's most flexible and reliable industrial burner. 
			The OVENPAK&reg; burns most any fuel gas and requires only low pressure fuel. This natural gas 
			burner provides clean combustion with low NOx levels while providing unmatched turndown.
			<br/><br/>OVENPAK&reg; Gas Burners provide outstanding performance in ovens and dryers, paint 
			finishing lines, paper and textile machines, food baking ovens, coffee roasters, grain dryers, 
			and fume incinerators. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-ovenpack400.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-ovenpack400.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-ovenpack400.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/OVENPAK_500_Series.jpg" alt="Maxon OVENPAK 500 Series" title="Maxon OVENPAK 500 Series"/></div>
			<div id="PartsContent"><h3>OVENPAK&reg; 500 Series
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>The OVENPAK&reg; 500 natural gas burner burns clean fuel gases, light oils, or both at 
			the same time. This industrial burner provides clean combustion with low NOx levels while 
			maintaining exceptional turndown for precise temperature control in a wide variety of processes 
			which include ovens and dryers, paint finishing lines, grain dryers, paper and textile machines, 
			coffee roasters, food baking ovens, and fume incinerators.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-ovenpack500.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-OVENPAK_500/2300-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/OVENPAK_II_400_Series.jpg" alt="Maxon OVENPAK II 400 Series" title="Maxon OVENPAK II 400 Series"/></div>
			<div id="PartsContent"><h3>OVENPAK II&reg; 400 Series
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>			
			<p>MAXON's OVENPAK II&reg; 400 Series improves on the world's most flexible and 
			reliable industrial burner by providing easier installation and extended accessories. 
			The OVENPAK II&reg; burns most any fuel gas and requires only low pressure fuel. 
			This natural gas burner provides clean combustion with low NOx levels while 
			providing unmatched turndown.
			<br/><br/>OVENPAK II&reg; Industrial Burners provide outstanding performance 
			in ovens and dryers, paint finishing lines, paper and textile machines, 
			food baking ovens, coffee roasters, grain dryers, and fume incinerators.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-ovenpack2400.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-ovenpack2400.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-ovenpack2400.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/OVENPAK_LE_Burner.jpg" alt="Maxon OVENPAK LE Burner" title="Maxon OVENPAK LE Burner"/></div>
			<div id="PartsContent"><h3>OVENPAK&reg; LE Burner
			<br/><font color="#445679" size="2">Natural Gas Burners Low Temperature</font></h3>			
			<p>&#149; Operates on low gas supply pressures
			<br/>&#149; Provides clean combustion with low NOx 
			<br/>&nbsp;&nbsp;and CO levels
			<br/>&#149; Compact burner design provides quick and 
			<br/>&nbsp;&nbsp;easy installation
			<br/>&#149; Balanced pressured design for easy 
			<br/>&nbsp;&nbsp;commissioning and adjustment
			<br/>&#149; Visible ignition action speeds 
			<br/>&nbsp;&nbsp;commissioning and maintenance
			<br/>&#149; High turndown for exceptional process 
			<br/>&nbsp;&nbsp;control
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-OVENPAK-LE/E-ovenpak_le-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview </b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-OVENPAK-LE/E-ovenpak_le-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation </b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/RADMAX_ULTRA_Low_NOx_Radiant_Line_Burners.jpg" alt="Maxon RADMAX ULTRA Low NOx Radiant Line Burners" title="Maxon RADMAX ULTRA Low NOx Radiant Line Burners"/></div>
			<div id="PartsContent"><h3>RADMAX&reg; ULTRA Low NOx Radiant Line Burners
			<br/><font color="#445679" size="2">Natural Gas Line Burners</font></h3>			
			<p>RADMAX&reg; Radient Low NOx Burners are designed to deliver uniform, high 
			intensity radiant energy for moisture removal in textile and paper ovens, paint 
			drying, and powder coating, as well as many pre-heat, plastic forming, heat 
			treating and annealing operations. With innovative ceramic or metal foam tiles, 
			the RADMAX&reg; natural gas burner produces low to Ultra-Low NOx and CO levels 
			for a wide variety of industrial gas burner applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lb-radmax.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lb-radmax.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lb-radmax.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/TUBE_O_Flame.jpg" alt="Maxon TUBE-O-FLAME Natural Gas Low Temp Burners" title="Maxon TUBE-O-FLAME Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>TUBE-O-FLAME&reg;
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>			
			<p>MAXON's TUBE-O-FLAME&reg; industrial burners provide clean, low NOx heating 
			for tube firing. The TUBE-O-FLAME&reg; burns most fuel gases inside an array of 
			tubes to provide reduced fuel TUBE-O-FLAME&reg;consumption and better process 
			response. The low horsepower requirements reduce operating costs, and produce 
			clean combustion with outstanding reliability.
			<br/><br/>TUBE-O-FLAME&reg; natural gas burners ensure improved performance for 
			industrial washers, dip tanks, water-bath heaters, solution tanks, and asphalt tanks.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/B-lt-tubeoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/S-lt-tubeoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/I-lt-tubeoflame.pdf" target="_blank"><font color="#ACB0C3"><b>Installation</b></font></a>
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/TUBE_O_THERM.jpg" alt="Maxon TUBE-O-THERM Natural Gas Low Temp Burners" title="Maxon TUBE-O-THERM Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>TUBE-O-THERM&reg;
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>			
			<p>The MAXON TUBE-O THERM&reg; burner-to-tube direct firing system provides uniform 
			heat transfer, eliminates "hot spots", and produces faster bring-up times than all 
			other tube burners. This industrial burner fires directly into smaller-bore, less 
			expensive tubes and burns butane, , propane or natural gas with reduced levels of NOx and CO.
			<br/><br/>TUBE-O THERM&reg; gas burners ensure improved performance for industrial 
			washers, dip tanks, water-bath heaters, and solution tanks.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-tube-o-therm/E-TubeOTherm-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-tube-o-therm/E-TubeOTherm-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation </b></font></a>
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/VALUPAK.jpg" alt="Maxon VALUPAK Natural Gas Low Temp Burners" title="Maxon VALUPAK Natural Gas Low Temp Burners"/></div>
			<div id="PartsContent"><h3>VALUPAK&reg;
			<br/><font color="#445679" size="2">Natural Gas Low Temp Burners</font></h3>
			<p>The MAXON VALUPAK&reg; gas burner provides a convenient, packaged solution 
			for direct fired heating applications in ovens and dryers. The burner fires 
			cleanly into applications with balanced pressure where high turndown of over 
			40:1 is required for precise temperature control. Durable steel construction 
			provides years of reliable service. VALUPAK&reg; is available in wide variety 
			of packaged forms to suit your application for a bolt-on solution. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-valupak/E-valupak-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-valupak/E-valupak-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/XPO_Burner.jpg" alt="Maxon XPO Indirect Burner Low NOx Burners" title="Maxon XPO Indirect Burner Low NOx Burners"/></div>
			<div id="PartsContent"><h3>XPO&#8482; Indirect Burner
			<br/><font color="#445679" size="2">Low NOx Burners</font></h3>
			<p>&#149; Low temperature burner for use with clean 
			<br/>&nbsp;&nbsp;fuel gases
			<br/>&#149; Single digit NOx emissions at 30% excess 
			<br/>&nbsp;&nbsp;air
			<br/>&#149; High efficiency with low excess air 
			<br/>&nbsp;&nbsp;requirements
			<br/>&#149; Capacities up to 3 MBtu/h with 4:1 
			<br/>&nbsp;&nbsp;turndown ratio
			<br/>&#149; For use in indirect fired solution backed 
			<br/>&nbsp;&nbsp;heaters
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-XPO/E-XPO_burner-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-XPO/E-XPO_burner-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			</p>
			</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Maxon/8000_Series_Gas_Valves.jpg" alt="Maxon 8000 Series Gas Valves Gas Shut Off Valves" title="Maxon 8000 Series Gas Valves Gas Shut Off Valves"/></div>
			<div id="PartsContent"><h3>8000 Series Gas Valves
			<br/><font color="#445679" size="2">Gas Shut Off Valves Hazardous Area</font></h3>
			<p>MAXON Series 8000 Air Actuated Shut Off Valves combine a unique space-saving design with a 
			maintenance-free bonnet seal and a replaceable actuator for easy installation and smooth, 
			trouble-free operation. This gas valve's quick exhaust and powerful closing spring provide 
			valve closure in less than one second and reliable, long-life operation. Valve seats are 
			micro-lapped metals. MAXON gas valves ensure positive sealing with our trademark durability. 
			Insist on MAXON shut off valves for your safety-critical combustion systems.
			<br/><br/>Unlike other pneumatic valves, MAXON valves require no maintenance. More importantly, 
			MAXON seats require no regular tightening of valve packings like ball valve designs The Series 
			8000 Valve has been FM, CSA and CE rated for hazardous locations. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-8000valve/E-8000_valve-i-bulletin.pdf" target="_blank"><font color="#ACB0C3"><b>Product Overview</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/E-8000valve/E-8000_valve-i-specs_instructions.pdf" target="_blank"><font color="#ACB0C3"><b>Specifications/Installation</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_maintenance.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Maintenance</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-leak_testing.pdf" target="_blank"><font color="#ACB0C3"><b>Leak Testing</b></font></a>
			<br/><a href="https://www.maxoncorp.com/Files/pdf/English/Valve_Technical_Data/E-valve_sizing_charts.pdf" target="_blank"><font color="#ACB0C3"><b>Valve Sizing Charts</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/8000_Series_Gas_Valves_Thumbnail.gif" border="0" alt="Maxon 8000 Series Gas Valves" title="Maxon 8000 Series Gas Valves" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/ACTIONAIR_Gas_and_Oil_Valves_Thumbnail.gif" border="0" alt="Maxon ACTIONAIR Gas and Oil Valves" title="Maxon ACTIONAIR Gas and Oil Valves" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/Gas_Electro_Mechanical_Valves_Thumbnail.gif" border="0" alt="Maxon Gas Electro-Mechanical Valves" title="Maxon Gas Electro-Mechanical Valves" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/Circular_INCINO_PAK_Thumbnail.gif" border="0" alt="Maxon Circular INCINO-PAK Natural Gas Low Temp Burners" title="Maxon Circular INCINO-PAK Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/CYCLOMAX_LOW_NOX_Thumbnail.gif" border="0" alt="Maxon CYCLOMAX LOW NOX Natural Gas Low Temp Burners" title="Maxon CYCLOMAX LOW NOX Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/INDIPAK_Thumbnail.gif" border="0" alt="Maxon INDIPAK Natural Gas Low Temp Burners" title="Maxon INDIPAK Natural Gas Low Temp Burners" /></a></li>
	        <li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/INDITHERM_Thumbnail.gif" border="0" alt="Maxon INDITHERM Natural Gas Low Temp Burners" title="Maxon INDITHERM Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/LINOFLAME_Industrial_Burner_Thumbnail.gif" border="0" alt="Maxon LINOFLAME Industrial Burner" title="Maxon LINOFLAME Industrial Burner" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/M_PAKT_Ultra_low_NOx_Thumbnail.gif" border="0" alt="Maxon M-PAKT Ultra Low NOx Burners" title="Maxon M-PAKT Ultra Low NOx Burners" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/MEGAFIRE_Industrial_Burner_Thumbnail.gif" border="0" alt="Maxon MEGAFIRE Industrial Burner" title="Maxon MEGAFIRE Industrial Burner" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/OPTIME_SLS_Burner_Thumbnail.gif" border="0" alt="Maxon OPTIMA SLS Burner Natural Gas Burners" title="Maxon OPTIMA SLS Burner Natural Gas Burners" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/OVENPAK_400_Series_Thumbnail.gif" border="0" alt="Maxon OVENPAK 400 Series" title="Maxon OVENPAK 400 Series" /></a></li>
	        <li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/OVENPAK_500_Series_Thumbnail.gif" border="0" alt="Maxon OVENPAK 500 Series" title="Maxon OVENPAK 500 Series" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/OVENPAK_II_400_Series_Thumbnail.gif" border="0" alt="Maxon OVENPAK II 400 Series" title="Maxon OVENPAK II 400 Series" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/OVENPAK_LE_Burner_Thumbnail.gif" border="0"alt="Maxon OVENPAK LE Burner" title="Maxon OVENPAK LE Burner" /></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/RADMAX_ULTRA_Low_NOx_Radiant_Line_Burners_Thumbnail.gif" border="0" alt="Maxon RADMAX ULTRA Low NOx Radiant Line Burners" title="Maxon RADMAX ULTRA Low NOx Radiant Line Burners" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/TUBE_O_Flame_Thumbnail.gif" border="0" alt="Maxon TUBE-O-FLAME Natural Gas Low Temp Burners" title="Maxon TUBE-O-FLAME Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/TUBE_O_THERM_Thumbnail.gif" border="0" alt="Maxon TUBE-O-THERM Natural Gas Low Temp Burners" title="Maxon TUBE-O-THERM Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/VALUPAK_Thumbnail.gif" border="0" alt="Maxon VALUPAK Natural Gas Low Temp Burners" title="Maxon VALUPAK Natural Gas Low Temp Burners" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Maxon/thumbnails/XPO_Burner_Thumbnail.gif" border="0" alt="Maxon XPO Indirect Burner Low NOx Burners" title="Maxon XPO Indirect Burner Low NOx Burners" /></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>
<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>

