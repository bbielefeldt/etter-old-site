<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Spectrum Program</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropLeft"></div>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="SpectrumSolidWhiteBkgrd"></div>
<div id="SpectrumTransBLK"></div>
<div id= "RedBrowseBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<div id="SpectrumProviding"><b>Providing the Full</b></div>
<div id="Spectrum"></div>
<div id="SpectrumExpertise"><b>of Expertise and Services</b></div>
<div id="SpectrumLogo"></div>
<div id="SpectrumText">ETTER Engineering developed the 
SPECTRUM Program in response to the growing needs of our 
customers. With today's focus on effient, safe, and 
productive operations, SPECTRUM provides you with the 
tools necessary to compete and thrive. ETTER's vast 
knowledge of combustion and <br/>combustion systems will 
ensure your <br/>operation is running as efficiently as <br/>
possible, allowing you to spend <br/>more time doing 
business and <br/>less time worrying about <br/>potential<br/> 
problem <br/>areas.</div>
<div id="EnergyEfficiency">
<center><div id="Header"><font size="2" color="#D42321">Energy Efficiency</font></div></center><br/>
<br/><br/>ETTER can help you analyze your process and determine ways to reduce energy usage without 
compromising quality or productivity. Most processes waste more energy than what is used to
actually make the product. Updating older equipment and controls can greatly reduce energy 
consumption and improve your energy efficieny.
<br/><br/><br/><font size="2" color="#D42321">Low Temperature Applications:</font>
<br/><br/> - processes used in paint curing, food processing, printing and laminating as well as many other fields
<br/> - typically include high volumes of air being heated from 150F to 750F
<br/><br/><br/><font size="2" color="#D42321">Upgrades Include:</font>
<br/><br/><blockquote>Exhaust Recirculation: By recirculating some or most of the exhaust 
air from a process, the required energy to heat the process air is reduced, as some of the air 
is already heated. Damper and fan controls allow for processes to vary how much exhaust is used 
for maimum fuel savings.
<br/><br/>Heat Recovery: Exhaust heat (or latent heat) is heat that literally 
goes up the exhaust stack. This has energy in it that can be recovered by an air-to-air or 
air-to-fluid heat echanger, which can then be used as space heating or in other process applications.
<br/><br/>Active LEL Control: Many processes that use solvents are operating with excessive exhaust flow rates
basedon maximum solvent loading. By actively monitoring LEL levels, the exhaust flow can be reduced 
to ensure that excess air is not being exhausted, which causes unnecessary latent heat loss.
<br/><br/>Modulating Control: Many older gas, electric, and steam systems have On/Off control, or very limited 
control ranges. By upgrading the control turn-down the process can be run in a more efficient and more 
accurate manner. Electric heating systems can be upgraded with SCR or SSR controls to achieve tighter 
tolerance and reduced energy usage. These controls can also reduce inrush and lower "Demand Charges" 
by tempering the heat output during startup and operations.</blockquote>
<br/><br/><font size="2" color ="#D42321">High Temperature Applications:</font>
<br/><br/> - processes used in heat treating, melting, casting, ceramic and glass processing
<br/> - operating at temperatures in the 1200F to 2200F range
<br/><br/><br/><font size="2" color ="#D42321">Upgrades Include:</font>
<br/><br/><blockquote>Combustion Air Preheating: By capturing the exhaust air to preheat the combustion air, the required 
load on the heat source is reduced by up to 25%. A heat exchanger is used to take latent heat from the 
exhaust and heat fresh air into the burners. Recuperative burners can also utilize exhaust heat at theburner front.
<br/><br/>Heat Recovery: With such high operating and exhaust temperatures, there is a large amount of energy 
available for heat recovery. As with the lower temperature applications, the heat can used for space heating, or other process heat.
<br/><br/>Reduced Low Fire Excess Air: All of the air entering a high temperature application must be heated 
to a very high temperature. As burners modulate to lower fuel flows, the amount of excess air can be 
reduced with upgraded burners and burner control systems. This can reduce the amount of air entering 
the furnace that is not required, and save energy.
<br/><br/>Pulse Fired Combustion Control: One way to reduce excess air while maintaining furnace temperature 
uniformity is to upgrade the combustion controls to Pulse Fired Control. This allows each burner to be
tuned at high fire (the most efficient point), and not operate over a wide range of less efficient 
outputs. Excess air is also dramatically reduced with this control logic as well.</blockquote>
<br/><br/><br/><font size="2" color ="#D42321">Process Boilers:</font>
<br/><br/> - boilers have similar opportunities for efficiency gains
<br/><br/><br/><font size="2" color="#D4232">Upgrades Include:</font>
<br/><blockquote>Linkageless Burner Controls: By using these controls, the boiler can be tuned to operate with the 
proper air and gas ratios ideal over the entire operating range. Lack of linkage hysteresis improves effiency and repeatability.
<br/><br/>O2 Trim Control: Exhaust oxygen levels are typically used to tune a boiler. By using active oxygen 
tuning, the boiler will change its air-to-fuel ratio continuously based on firing rate to achieve the 
best performance and efficieny.</blockquote></div>
<a href="spectrum_program_7.php" id="Previous"><font color="#8D518F">Previous</font></a>
<a href="spectrum_program_2.php" id="Next"><font color="#D69221">Next</font></a>
<div id="IndexContent"><img src="http://www.etterengineering.com/sen_ally_box.jpg" /><br /><b>Proud Partner: </b><a href="http://www.etterengineering.com/spectrum/">U.S. Department of Energy's Industrial Technologies Program</a></div>
<br/>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video" border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>