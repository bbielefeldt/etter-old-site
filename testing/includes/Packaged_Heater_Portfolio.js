$(document).ready(function() {
$('a.poplight[href^=#]').click(function() {
    var popID = $(this).attr('rel');
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&amp;');
    var popWidth = dim[0].split('=')[1]; //Gets the first query string value
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close"><img src="closeXsmall.gif" border="0" class="btn_close" title="Close Window" alt="Close Packaged Heater" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    return false;
});
$('a.close, #fade').live('click', function() { 
    $('#fade , .popup_block').fadeOut(function() {
        $('#fade, a.close').remove(); 
    });
    return false;
});
});