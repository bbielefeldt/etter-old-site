$(function() {
$('a.poplightOne').hover(function(e) {
		var html =    '<div id="infoOne">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" alt="Gas Boosters"/></div>';
		html +=	   '<div id="NumberOne"><img src="Number1_ENGB.gif" alt="Gas Boosters"/></div>';
		html +=    '<div id="OneContentThree"><font color=#D21D1F><b>New Advances</b></font><br/>&#149; Volumes in excess of 120,000 ICFH<br/>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=	   '<div id="BlackBox"></div>';
		html +=	   '<div id="infoOneInside">'
		html +=	   '<div id="OneTitle"><b>Custom Outlet Positioning</b></div>';
		html +=	   '<div id="OneContent">Discharge can be rotated on-site to achieve a fully custom outlet position using standard hand tools.<br/><br/><font color=#D21D1F><b>Benefit:</b></font>No longer are you forced to select one of the standard four or six discharge positions when ordering. Installation is faster, more flexible, and does not require expensive fittings and long lead-times due to custom builds.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoTwo').hide().fadeIn(400);
	}, function() {
		$('#infoOne').remove();
	});
});
$(function() {
$('a.poplightTwo').hover(function(e) {
		var html =    '<div id="infoTwo">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" alt="Gas Boosters"/></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=	   '<div id="NumberTwo"><img src="Number2_ENGB.gif" alt="Gas Boosters"/></div>';
		html +=    '<div id="OneContentThree"><font color="#D21D1F"><b>New Advances</b></font><br/>&#149; Volumes in excess of 120,000 ICFH<br/>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="BlackBox"></div>';
		html +=	   '<div id="infoTwoInside">'
		html +=	   '<div id="TwoTitle"><b>External Motor Wiring</b></div>';
		html +=	   '<div id="TwoContent">Externally accessible wiring for 120V single phase or 208, 230/460V, three-phase Class 1 Div 1 Group D explosion-proof motors with thermal overload protection.<br/><br/><font color="#D21D1F"><b>Benefit:</b></font> Voltage changes can now be made in the field without jeopardizing the hermetic seal of the tank. This in turn, eliminates the need to send the booster back to the factory.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoTwo').hide().fadeIn(400);
			
	}, function() {
		$('#infoTwo').remove();
	});
});
$(function() {
$('a.poplightThree').hover(function(e) {
		var html =    '<div id="infoThree">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" alt="Gas Boosters"/></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=    '<div id="OneContentThree"><font color="#D21D1F"><b>New Advances</b></font><br/>&#149; Volumes in excess of 120,000 ICFH<br/>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="NumberThree"><img src="Number3_ENGB.gif" alt="Gas Boosters"/></div>';
		html +=	   '<div id="BlackBox"></div>';	
		html +=	   '<div id="infoThreeInside">'
		html +=	   '<div id="ThreeTitle"><b>Cool Flow Technology</b></div>';
		html +=	   '<div id="ThreeContent">ETTER&#39s new Cool Flow Technology (CFT) maximizes the cooling effects of the gas stream by providing a 360&#176; flow of natural gas focused directly on the motor.<br/><br/> <font color="#D21D1F"><b>Benefit:</b></font> Higher turndown capabilities for all booster sizes means more reliable, efficient motor operation, longer motor life, and in many cases does away with the need for more complicated piping and the additional costs of a heat exchanger.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoThree').hide().fadeIn(400);
			
	}, function() {
		$('#infoThree').remove();
	});
});
$(function() {
$('a.poplightFour').hover(function(e) {
		var html =    '<div id="infoFour">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" alt="Gas Boosters"/></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=	   '<div id="NumberFour"><img src="Number4_ENGB.gif" alt="Gas Booster"/></div>';		
		html +=    '<div id="OneContentThree"><font color="#D21D1F"><b>New Advances</b></font><br/>&#149; Volumes in excess of 120,000 ICFH<br/>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="BlackBox"></div>';
		html +=	   '<div id="infoFourInside">'
		html +=	   '<div id="FourTitle"><b>Sliding Service Sled</b></div>';
		html +=	   '<div id="FourContent">The ENGB tank housing features a sliding service stand, with both the motor and impeller being accessed by removing the end plate bolts and gliding the tank housing in the opposite direction while it remains supported by the integral stand.<br/><br/><font color="#D21D1F"><b>Benefit:</b></font> Any required motor or wheel inspection, cleaning or maintenance is now a quick, easy one-man job, enhancing serviceability and significantly decreasing downtime.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoFour').hide().fadeIn(400);
			
	}, function() {
		$('#infoFour').remove();
	});
});