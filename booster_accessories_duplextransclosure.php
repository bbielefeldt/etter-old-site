<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Natural Gas Boosters - Gas Booster Accessories</title>
<meta name="keyword" content="Hermetic Gas Boosters, Hermetic Gas Booster, Hermetically Sealed Gas Booster, Hermetically Sealed Gas Boosters, natural gas booster, natural gas boosters, gas pumps, gas booster pumps, packaged gas booster systems, gas booster packages, booster package, E-101P Series, natural gas booster, booster rental, emergency rental systems for gas boosters, gas booster panels, gas booster accessories, outdoor transclosures, outdoor enclosures, duplex transclosure, duplex enclosure" />
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="http://cloud.github.com/downloads/malsup/cycle/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="BoosterAccessoriesPhoto"></div>
<div id= "RedBrowseBar"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id="ENGBBoosterTransBLK"></div>
<div id="ENGBBoosterSolidWhiteBkgrd"></div>
<div id="ENGBBoosterDropRightBlkTrans"></div>
<div id="AccessoriesBkgrdPhoto"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="ENGBBoosterDropRightInside"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="BoosterAccessoriesTxt"><font color="#DBDBDB">ETTER Engineering offers an 
infinite<br/> variety of options to customize your natural gas booster system. Although standard designs 
are always available,<br/> we are always willing to customize your<br/> project to meet 
your specific needs.</font></div>
<div id="DuplexTransclosurePhoto"></div>
<div id="AccessoriesList">
<font color="#445679" size="2"><b>Gas Booster Accessories</b></font>
<br/><a href="booster_accessories_duplex.php" id="DuplexBTN"><b><font size="2">&#149;</font> Duplex Systems</b></a>
<br/><a href="booster_accessories_touchscreen.php" id="TouchScreenBTN"><b><font size="2">&#149;</font> PLC Touch Screen Controls</b></a>
<br/><a href="booster_accessories_stacked.php" id="StackedBTN"><b><font size="2">&#149;</font> Stacked Systems</b></a>
<br/><a href="booster_accessories_ventless.php" id="VentlessBTN"><b><font size="2">&#149;</font> Ventless Pressure Systems</b></a>
<br/><a href="booster_accessories_transclosure.php" id="TransBTN"><b><font size="2">&#149;</font> Outdoor Transclosures</b></a>
<br/><a href="booster_accessories_duplextransclosure.php" id="DuplexTransBTN"><b><font size="2">&#149;</font> Duplex Outdoor Transclosures</b></a>
</div>
<div id="DuplexTransclosureTxt"><font color="#445679" size="2"><b>Duplex Outdoor Transclosures</b></font>
<br/>
<br/>
When tamper-proof security and better protection from the elements is 
required for duplex systems, the preferred option is a steel transclosure. With this option, ETTER's 
packaged gas booster systems leave our factory skid-mounted inside an outdoor paneled enclosure to 
provide superior weather protection, with vented panels and heavy duty, tamper-proof hinged doors 
with lockable handles to prevent unauthorized access.</div>
s<div id="AlternateBoosterNav">
<a href="gasPOD_boosters.php" id="gasPODTxtBTN"><font color="#808080"><b>gasPOD</b></font></a>
<a href="engb_boosters.php" id="ENGBTxtBTN"><font color="#808080"><b>ENGB</b></font></a>
<a href="E101P_boosters.php" id="E101PTxtBTN"><font color="#808080"><b>E101-P</b></font></a>
<a href="E101PHC_boosters.php" id="E101PHCTxtBTN"><font color="#808080"><b>E101-PHC</b></font></a>
<a href="E101PHCXtra_boosters.php" id="E101PHCXtraTxtBTN"><font color="#808080"><b>E101-PHC-Xtra</b></font></a>
<a href="booster_accessories_duplex.php" id="AccessoriesTxtBTN"><font color="#808080"><b>Accessories</b></font></a>
</div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="gas booster video "border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
        </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>