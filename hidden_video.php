<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<style type="text/css">
a { text-decoration:none }
</style>
<head>
<title>ETTER Engineering - ENGB - Natural Gas Boosters</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css">	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript">
$(function() {
$('a.poplightOne').hover(function(e) {
		var html =    '<div id="infoOne">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" /></div>';
		html +=	   '<div id="NumberOne"><img src="Number1_ENGB.gif" /></div>';
		html +=    '<div id="OneContentThree"><font color=#D21D1F><b>New Advances</b></font><br>&#149; Volumes in excess of 120,000 ICFH<br>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=	   '<div id="BlackBox"></div>';
		html +=	   '<div id="infoOneInside">'
		html +=	   '<div id="OneTitle"><b>Custom Outlet Positioning</b></div>';
		html +=	   '<div id="OneContent">Discharge can be rotated on-site to achieve a fully custom outlet position using standard hand tools.<br><br><font color=#D21D1F><b>Benefit:</b></font>No longer are you forced to select one of the standard four or six discharge positions when ordering. Installation is faster, more flexible, and does not require expensive fittings and long lead-times due to custom builds.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoOne').hide().fadeIn(400);
	}, function() {
		$('#infoOne').remove();
	});
});
$(function() {
$('a.poplightTwo').hover(function(e) {
		var html =    '<div id="infoTwo">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" /></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=	   '<div id="NumberTwo"><img src="Number2_ENGB.gif" /></div>';
		html +=    '<div id="OneContentThree"><font color=#D21D1F><b>New Advances</b></font><br>&#149; Volumes in excess of 120,000 ICFH<br>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="BlackBox"></div>';
		html +=	   '<div id="infoTwoInside">'
		html +=	   '<div id="TwoTitle"><b>External Motor Wiring</b></div>';
		html +=	   '<div id="TwoContent">Externally accessible wiring for 120V single phase or 208, 230/460V, three-phase Class 1 Div 1 Group D explosion-proof motors with thermal overload protection.<br><br><font color=#D21D1F><b>Benefit:</b></font> Voltage changes can now be made in the field without jeopardizing the hermetic seal of the tank. This in turn, eliminates the need to send the booster back to the factory.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoTwo').hide().fadeIn(400);
			
	}, function() {
		$('#infoTwo').remove();
	});
});
$(function() {
$('a.poplightThree').hover(function(e) {
		var html =    '<div id="infoThree">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" /></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=    '<div id="OneContentThree"><font color=#D21D1F><b>New Advances</b></font><br>&#149; Volumes in excess of 120,000 ICFH<br>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="NumberThree"><img src="Number3_ENGB.gif" /></div>';
		html +=	   '<div id="BlackBox"></div>';	
		html +=	   '<div id="infoThreeInside">'
		html +=	   '<div id="ThreeTitle"><b>Cool Flow Technology</b></div>';
		html +=	   '<div id="ThreeContent">ETTER&#39s new Cool Flow Technology (CFT) maximizes the cooling effects of the gas stream by providing a 360&#176; flow of natural gas focused directly on the motor.<br><br> <font color=#D21D1F><b>Benefit:</b></font> Higher turndown capabilities for all booster sizes means more reliable, efficient motor operation, longer motor life, and in many cases does away with the need for more complicated piping and the additional costs of a heat exchanger.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoThree').hide().fadeIn(400);
			
	}, function() {
		$('#infoThree').remove();
	});
});
$(function() {
$('a.poplightFour').hover(function(e) {
		var html =    '<div id="infoFour">'
		html +=	   '<div id="BoosterDiagram"><img src="solidworks_graphic.gif" /></div>';
		html +=	   '<div id="OneContentTwo">The ENGB was engineered with variable-frequency drives (VFD) in mind. By integrating a VFD with your booster control philosophy, you will decrease horsepower consumption - saving money and energy while also enjoying integrated pressure control.</div>';
		html +=    '<div id="OneContentThree"><font color=#D21D1F><b>New Advances</b></font><br>&#149; Volumes in excess of 120,000 ICFH<br>&#149; Static gains in excess of 41" W.C.</div>';
		html +=	   '<div id="NumberFour"><img src="Number4_ENGB.gif" /></div>';
		html +=	   '<div id="BlackBox"></div>';	
		html +=	   '<div id="infoFourInside">'
		html +=	   '<div id="FourTitle"><b>Sliding Service Sled</b></div>';
		html +=	   '<div id="FourContent">The ENGB tank housing features a sliding service stand, with both the motor and impeller being accessed by removing the end plate bolts and gliding the tank housing in the opposite direction while it remains supported by the integral stand.<br><br><font color=#D21D1F><b>Benefit:</b></font> Any required motor or wheel inspection, cleaning or maintenance is now a quick, easy one-man job, enhancing serviceability and significantly decreasing downtime.</div>';
		html +=		'</div>';
		html +=		'</div>';				
		$('#Wrapper').append(html).children('#infoFour').hide().fadeIn(400);
			
	}, function() {
		$('#infoFour').remove();
	});
});
</script>
<script type="text/javascript">
$(function() {
$('a.Production').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1]; 
 $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGB"><img border="0" src="closeXsmall.gif" class="btn_close_ENGB" title="Close Window" alt="Close" /></a>');
var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;

   $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });

    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn();  
return false;
});

$('a.close_ENGB, #fade').live('click', function() { 
    $('#fade , .ENGB_block').fadeOut(function() {
        $('#fade, a.close_ENGB').remove();  
    });
    return false;
});
});
</script>
<script type="text/javascript">
$(function() {
$('a.SolidVideo').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGBSolid"><img border="0" src="closeXsmall.gif" class="btn_close_ENGBSolid" title="Close Window" alt="Close" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn(); 
return false;
});
$('a.close_ENGBSolid, #fade').live('click', function() {
    $('#fade , .ENGBSolid_block').fadeOut(function() {
        $('#fade, a.close_ENGBSolid').remove(); 
    });
    return false;
});
});
</script>
<script>
$(function() {
$('a.approvals').mouseover(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    //Pull Query & Variables from href URL
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_approvals"><img border="0" src="closeXsmall.gif" class="btn_close_approvals" title="Close Window" alt="Close" /></a>');
    //Define margin for center alignment (vertical   horizontal) - we add 80px to the height/width to accomodate for the padding  and border width defined in the css
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    //Apply Margin to Popup
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    return false;
});
$('a.close_approvals, #fade').live('click', function() { //When clicking on the close or fade layer...
    $('#fade , .popup_block_approvals').fadeOut(function() {
        $('#fade, a.close_approvals').remove();  //fade them both out
    });
    return false;
});
});
</script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
<script type="text/javascript">
$(function() {
$('a.SolidVideo').click(function() {
    var popID = $(this).attr('rel'); 
    var popURL = $(this).attr('href'); 
    var query= popURL.split('?');
    var dim= query[1].split('&');
    var popWidth = dim[0].split('=')[1];
    $('#' + popID).fadeIn('slow').css({ 'width': Number( popWidth ) }).prepend('<a href="#" class="close_ENGBSolid"><img border="0" src="closeXsmall.gif" class="btn_close_ENGBSolid" title="Close Window" alt="Close" /></a>');
    var popMargTop = ($('#' + popID).height() + 80) / 2;
    var popMargLeft = ($('#' + popID).width() + 80) / 2;
    $('#' + popID).css({
        'margin-top' : -popMargTop,
        'margin-left' : -popMargLeft
    });
    $('body').append('<div id="fade"></div>'); 
    $('#fade').css({'filter' : 'alpha(opacity=50)'}).fadeIn(); 
return false;
});
$('a.close_ENGBSolid, #fade').live('click', function() {
    $('#fade , .ENGBSolid_block').fadeOut(function() {
        $('#fade, a.close_ENGBSolid').remove(); 
    });
    return false;
});
});
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script>
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<script>
    $("#btn_close_ENGBSolid").click(function () {
      $("#popup_ENGBSolid").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#ENGBSolidVideoBTN").click(function () {
      $("#popup_ENGBSolid").show(2000);
    });
</script>
<script>
    $("#btn_close_ENGB").click(function () {
      $("#popup_ENGB").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#ENGBProductionBTN").click(function () {
      $("#popup_ENGB").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="BottomDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="index.php" id="Logo"></a>
<a href="index.php" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="index.php"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Packaged Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
			<li><a href="packaged-burners.php">Packaged Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="accolades.php">Accolades</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Inside the Job</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="ENGBBoosterPhoto"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<div id="ENGBBoosterTransBLK"></div>
<div id="ENGBBoosterSolidWhiteBkgrd"></div>
<div id="ENGBBoosterDropRightBlkTrans"></div>
<div id="ENGBLogo"><borderstyle="none" borderwidth=0></div>
<div id="ENGBBoosterDropRightPhoto"></div>
<div id="ENGBBoosterDropLeftPhoto"></div>
<div id="ENGBBoosterText"><font color="#FFFFFF">ETTER Engineering has been providing process heating and 
combustion solutions since 1940. Our vast experience in the 20th century led 
us to develop the ETTER Natural Gas Booster (ENGB) - a UL-Listed, hermetically-sealed 
gas booster designed for the 21st century, with the flexibility to fit whatever 
specifications your application calls for.
<br/><br/>ETTER manufactures a complete line of packaged gas booster systems - true 
"Plug and Play"&#8482; designs that arrive at your job site ready to begin work.
<br/><br/>ETTER also offers FM-approved check valves and a variety of accessories and 
services to ensure your gas booster installation is as quick, easy, and 
cost-effective as possible.
<br/><br/>For more information about the ETTER ENGB Gas Booster or ETTER Engineering, 
call us today at 1-800-444-1962.</font></div>
<div id="ENGBBoosterTextRightSide"><font color="#445679" size= "4"><b>ENGB Features</b></font>
<br/><br/><br/><br/><font color="#4E4848" size= "1">Being limited to four or six outlet positions is a thing<br/> of the past.</font>
<br/><br/><br/><font color="#4E4848" size= "1">Wrong voltage? No big deal - you can change it in the field.</font>
<br/><br/><br/><font color="#4E4848" size= "1">CFT means higher turndown capabilities and a longer, more reliable motor life.</font>
<br/><br/><br/><font color="#4E4848" size= "1">Any required maintenance is now a quick, easy one-man job.</font></div>



<a href="pdfs/ETTER-Engineering-ENGB-gas-booster.pdf"div id="ENGBPDFButton"></a>
<div id="ENGBButton"><a href="#?w=425" rel="popup_name" class="poplightOne"><font color="#000000" face="Verdana" size="2">Custom Outlet Positioning</font></a></div>
<div id="ENGBButton2"><a href="#?w=425" rel="popup_name" class="poplightTwo"><font color="#000000" face="Verdana" size="2">External Motor Wiring</font></a></div>
<div id="ENGBButton3"><a href="#?w=425" rel="popup_name" class="poplightThree"><font color="#000000" face="Verdana" size="2">Cool Flow Technology&#8482</font></a></div>
<div id="ENGBButton4"><a href="#?w=425" rel="popup_name" class="poplightFour"><font color="#000000" face="Verdana" size="2">Sliding Service Sled</font></a></div>
<div id="container"><a href="#?w=380" rel="popup_name" class="approvals"><img src="ViewApprovalsbutton.gif" alt="booster aprrovals" border="0" width="206" height="15"></a></div>
	<div id="popup_name" class="popup_block_approvals">
    
			<a href="http://www.coned.com/es/specs/gas/Section%20VII.pdf" div id="ConED"><img border="0" src="con_edison_logo.png" alt="booster approvals" /></a>
			<a href="pdfs/2009-natl-grid-blue-book-web.pdf" div id="NationalGrid"><img border="0" src="national_grid_logo.png" alt="booster approvals" /></a>
			<a href="http://database.ul.com/cgi-bin/XYV/template/LISEXT/1FRAME/showpage.html?name=JIFQ.MH46473&ccnshorttitle=Gas+Boosters&objid=1079858133&cfgid=1073741824&version=versionless&parent_id=1073988683&sequence=1" div id="ULLogo"><img border="0" src="ul_logo_a.png" alt="booster approvals" /></a>
			<a href="http://license.reg.state.ma.us/pubLic/pl_products/pb_search.asp?type=G&manufacturer=Etter+Engineering+Company+Inc.&model=&product=&description=&psize=50" div id="MassGov"><img border="0" src="massgovlogo.png" alt="booster approvals" /></a>
			<div id="ApprovalsTxtpopup">With the exception of the UL-listing, the above approval agencies are region-specific, should your local agencies require any further documentation other than our UL-listing, please contact ETTER Engineering Toll Free 1-800-444-1962 for further assistance.
		</div>
		</div>
<div id="ENGBProductionBTN"><a href="#?w=400" rel="popup_ENGB" class="Production"><img src="viewproductionbutton.gif" border="0" width="206" height="15" alt="booster production"></a></div>
	<div id="popup_ENGB" class="ENGB_block">
	<div class="IndexSlideshow"> 
    		<img src="engb_production_slideshow/photo_1.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_2.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_3.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_4.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_5.gif" width="400" height="371" alt="booster production" /> 
   	 	<img src="engb_production_slideshow/photo_6.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_7.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_8.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_9.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_10.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_11.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_12.gif" width="400" height="371" alt="booster production" /> 
   	 	<img src="engb_production_slideshow/photo_13.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_14.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_15.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_16.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_17.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_18.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_19.gif" width="400" height="371" alt="booster production" /> 
   	 	<img src="engb_production_slideshow/photo_20.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_21.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_22.gif" width="400" height="371" alt="booster production" /> 
    		<img src="engb_production_slideshow/photo_23.gif" width="400" height="371" alt="booster production" /> 
	</div>
	</div>

<div id="ENGBSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><img src="viewVideobutton.gif" alt="booster video"border="0" width="206px" height="15px"></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf">
        <param name='quality' value="high">
        <param name='bgcolor' value='#FFFFFF'>
        <param name='loop' value="true">
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="AlternateBoosterNav">
<a href="gasPOD_boosters.php" id="gasPODTxtBTN"><font color="#808080"><b>gasPOD</b></font></a>
<a href="engb_boosters.php" id="ENGBTxtBTN"><font color="#808080"><b>ENGB</b></font></a>
<a href="E101P_boosters.php" id="E101PTxtBTN"><font color="#808080"><b>E101-P</b></font></a>
<a href="E101PHC_boosters.php" id="E101PHCTxtBTN"><font color="#808080"><b>E101-PHC</b></font></a>
<a href="E101PHCXtra_boosters.php" id="E101PHCXtraTxtBTN"><font color="#808080"><b>E101-PHC-Xtra</b></font></a>
<a href="booster_accessories_duplex.php" id="AccessoriesTxtBTN"><font color="#808080"><b>Accessories</b></font></a></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"></div>
<div id="LearnMoreFooterText"></div>
<div id="ENGBFooterSolidVideoBTN"></div>
<div id="ENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><img src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf">
        <param name='quality' value="high">
        <param name='bgcolor' value='#FFFFFF'>
        <param name='loop' value="true">
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;">
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" "value="" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;">
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;">
<input type="hidden" name="llr" value="qksvr8cab"> <input type="hidden" name="m" value="1102583613776"> 
<input type="hidden" name="p" value="oi"> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>