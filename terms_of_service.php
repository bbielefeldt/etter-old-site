<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="robots" content="noindex"/>
<title>ETTER Engineering - Terms of Service</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="IndexWhiteBkgrd"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<a href="contact_us.php" id="ContactButton"><b>Contact Us</b></a>
<ul id="dropdown">
	<li><a href="http://www.etterengineering.com/"><b>Home</b></a></li>
	<li><a href="#"><b>Products</b></a>
		<ul>
			<li><a href="gas_boosters.php">Gas Boosters</a></li>
			<li><a href="valve_trains.php">Valve Trains</a></li>
			<li><a href="ovens_and_furnaces.php">Ovens and Furnaces</a></li>
			<li><a href="web_drying.php">Web Drying</a></li>
			<li><a href="packaged_heaters.php">Package Heaters</a></li>
			<li><a href="control_panels.php">Control Panels</a></li>
			<li><a href="packaged-burners.php">Package Burners</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Part Sales</b></a>
		<ul>
			<li><a href="parts_line_card.php">Parts Line Card</a></li>
			<li><a href="parts_by_manufacturer_bryan_donkin.php">Parts By Manufacturers</a></li>
		</ul>
	</li>
	<li><a href="#"><b>Services</b></a>
		<ul>
			<li><a href="safety_audits.php">Safety Audits</a></li>
			<li><a href="spectrum_program.php">SPECTRUM Program</a></li>
		</ul>
	</li>
	<li><a href="literature.php"><b>Literature</b></a>
        </li>
	<li><a href="#"><b>About Us</b></a><ul>
			<li><a href="philosophy.php">Philosophy</a></li>
			<li><a href="jobs.php">Jobs</a></li>
			<li><a href="news.php">News</a></li>
			<li><a href="accolades.php">Accolades</a></li>
			<li><a href="tech_tips.php">Technical Tips</a></li>
			<li><a href="inside_the_job.php">Case Studies</a></li>
			<li><a href="Newsletter-Archive.php">Newsletter Archive</a></li>
			<li><a href="company.php">Company</a></li>
			<li><a href="blog">ETTER Blog</a></li>
		</ul>
	</li>
</ul>
<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="TOSHeadline">
ETTER Engineering Company, Inc. - Terms of Service</div>
<div id="TOSParagraph">
<blockquote>By accessing any web page contained within the ETTER Engineering Company, Inc. Web site, 
http://www.etterengineering.com (the "Site"), you agree to be legally bound and to abide by the terms set forth below:
<br/><br/><h5>PROPRIETARY RIGHTS</h5>
<br/>All information, data, software, photographs, graphs, videos, typefaces, graphics, music, sounds, 
and other material (collectively "Content") included on this Site (including, without limitation, text, 
graphics, logos, button icons, images, audio clips and software) is the property of ETTER Engineering 
Limited Liability Company ("SITE OWNER") or its licensors and is protected by U.S. and international 
copyright laws. The compilation (meaning the collection, arrangement and assembly) of all Content on 
this Site is the exclusive property of SITE OWNER and protected by U.S. and international copyright laws.
<br/><br/>SUBJECT TO THESE TERMS AND CONDITIONS, SITE OWNER GRANTS YOU A LIMITED, NON-EXCLUSIVE, 
ROYALTY-FREE LICENSE TO VIEW THE CONTENT ON THIS SITE. ANY OTHER USE, INCLUDING THE REPRODUCTION, 
COPYING, MODIFICATION, DISTRIBUTION, TRANSMISSION, REPUBLICATION, DISPLAY OR PERFORMANCE, OF THE 
CONTENT ON THIS SITE IS STRICTLY PROHIBITED.
<br/><br/>The mark "ETTER Engineering Company, Inc." and other graphics, logos and service names 
are trademarks or service marks of SITE OWNER. You are not granted any right to use SITE OWNER'S 
trademarks, service marks, logos, trade dress (including without limitation the layout of this site) 
or other marks and all such items shall remain the property of SITE OWNER. All other trademarks, 
service marks, product names, and company names and logos appearing on the Site are the property 
of their respective owners.
<br/><br/><h5>DISCLAIMERS AND LIMITATION OF LIABILITY</h5>
<br/>SITE OWNER makes every effort to insure the presentation of accurate and current information 
on this Site, but because of the dynamic nature of the Internet, SITE OWNER makes no warranties or 
guarantees of the accuracy of the information presented on this Site. In addition, no oral advice 
or written information given by SITE OWNER or its affiliates, or any of its officers, directors, 
employees, agents, providers, merchants, sponsors, licensors, or the like, shall create a warranty; 
nor shall you rely on any such information or advice. You agree that use of this Site is at your sole 
risk. Neither SITE OWNER, nor its affiliates, nor any of its officers, directors, or employees, 
agents, third-party content providers, merchants, sponsors, licensors or the like, warrant that 
the Content or this Site will be uninterrupted or error-free; nor do they make any warranty as to 
the results that may be obtained from the use of this Site, or as to the accuracy, reliability, or 
currency of any information content, service, merchandise or software provided through this Site.
<br/><br/>THIS SITE IS PROVIDED BY SITE OWNER ON AN "AS IS" BASIS. SITE OWNER MAKES NO REPRESENTATIONS 
OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO THE OPERATION OF THE SITE, THE INFORMATION, CONTENT, 
MATERIALS OR PRODUCTS, INCLUDED ON THIS SITE. TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, SITE 
OWNER DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO, IMPLIED WARRANTIES 
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TRADE USAGE AND NONINFRINGEMENT.
<br/><br/>SITE OWNER AND ITS LICENSORS WILL NOT BE LIABLE FOR ANY LOSS OF USE, LOSS OF DATA, INTERRUPTION 
OF BUSINESS, LOST PROFITS OR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL, PUNITIVE AND CONSEQUENTIAL DAMAGES 
OF ANY KIND, REGARDLESS OF THE FORM OF ACTION WHETHER IN CONTRACT, TORT, NEGLIGENCE, STRICT LIABILITY OR 
OTHERWISE, ARISING FROM THE USE OF OR THE INABILITY TO USE THIS SITE OR THAT RESULT FROM MISTAKES, OMISSIONS, 
INTERRUPTIONS, DELETION OF FILES OR EMAIL, ERRORS, DEFECTS, VIRUSES, DELAYS IN OPERATION OR TRANSMISSION, 
OR ANY FAILURE OF PERFORMANCE, WHETHER OR NOT RESULTING FROM ACTS OF GOD, COMMUNICATIONS FAILURE, THEFT, 
DESTRUCTION, OR UNAUTHORIZED ACCESS TO SITE OWNER'S RECORDS, PROGRAMS, OR SERVICES, EVEN IF SITE OWNER 
HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSSES OR DAMAGES. YOU AGREE THAT THIS PARAGRAPH SHALL 
APPLY TO ALL CONTENT, SOFTWARE AND SERVICES AVAILABLE THROUGH THE SITE.
<br/><br/>This Site has links to other web sites which are not under the control of SITE OWNER and 
SITE OWNER neither endorses nor is it responsible for the accuracy or reliability of the Contents 
of any linked site or any link contained in a linked site, or any changes or updates to such sites. 
Furthermore, the mere presence of a link is not an implication of sponsorship, endorsement or affiliation. 
Under no circumstances shall SITE OWNER, or its affiliates, or any of its licensors, officers, directors, 
employees, or agents be liable for any loss or damage caused by a person's reliance on information 
obtained through SITE OWNER or the Site. It is up to you to take precautions to ensure that whatever 
you select for your use is free of such items as viruses, worms, trojan horses and other items of a 
destructive nature.
<br/><br/><h5>EXPORT CONTROLS</h5>
<br/>The U.S. export control laws regulate the export and re-export of technology originating in the 
United States. This includes the electronic transmission of information and software to foreign 
countries and to certain foreign nationals. You agree to abide by these laws and their regulations, 
including, without limitation, the Export Administration Act and the Arms Export Control Act and not 
to transfer, by electronic transmission or otherwise, any Content derived from the Site to either a 
foreign national or a foreign destination in violation of such laws.
<br/><br/><h5>SITE OWNER'S RIGHTS</h5>
<br/>SITE OWNER may elect to monitor areas of the Site electronically and may disclose any Content, 
records, or electronic communication of any kind (1) to satisfy any law, regulation, or government 
request, (2) if such disclosure is necessary or appropriate to operate SITE OWNER or (3) to protect 
the rights or property of SITE OWNER, its customers, sponsors, providers, or licensors.
<br/><br/>SITE OWNER reserves the right to prohibit conduct, communication, or Content that it deems 
in its sole discretion to be harmful, whether to an individual, business entity, SITE OWNER, or any 
rights of SITE OWNER or any third party, or to violate any applicable law or these Terms and Conditions. 
Any conduct by you that, in SITE OWNER'S sole discretion, restricts or inhibits any other person from 
using or enjoying the Site will not be permitted. You agree to use the Site only for lawful purposes.
<br/><br/>WE RESERVE THE RIGHT TO MAKE CHANGES TO THIS SITE AND THESE TERMS AND CONDITIONS AT ANY TIME 
AND WITHOUT NOTICE TO YOU.
<br/><br/>If SITE OWNER removes any Content from this Site all licenses granted with respect to the 
removed Content are immediately terminated.
<br/><br/>SITE OWNER reserves the right to cooperate fully with government officials in any investigation 
relating to any Content (including personal or private electronic communication transmitted on
<br/><br/>SITE OWNER) or purported unlawful activities of any user.
<br/><br/><h5>MISCELLANEOUS</h5>
<br/>If any provision of this Agreement is held by a court of competent jurisdiction to be unenforceable 
for any reason, the remaining provisions hereof shall be unaffected and remain in full force and effect. 
This Agreement shall for all purposes be governed by and interpreted in accordance with the laws of the 
State of Connecticut without regard to its conflict of laws principles. Any suit or proceeding arising 
out of or relating to this Agreement shall be commenced exclusively in state or federal court in 
Connecticut, and you irrevocably submit to the exclusive jurisdiction and venue of such courts.
<br/><br/><h5>ACKNOWLEDGMENT</h5>
<br/>This Agreement represents the entire understanding between you and SITE OWNER regarding your 
relationship with SITE OWNER and supersedes any prior statements or representations.
<br/><br/>YOU AGREE TO BE BOUND BY THESE TERMS AND CONDITIONS BY USING THE SITE.
<br/><br/><h5>COPYRIGHT NOTICE</h5>
<br/>Copyright &#169; 2010 ETTER Engineering Company, Inc. and/or its suppliers, 210 Century Drive, 
Bristol, CT 06010. All rights reserved.</blockquote></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>