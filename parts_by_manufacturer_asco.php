<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<title>ETTER Engineering - Asco Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="AscoLogoLarge"></div>
<div id="SensusText">ASCO is the worldwide leader in the design and manufacture of quality solenoid valves. ASCO products are designed to control the flow of air, gas, water, oil and steam. Our heritage of innovation has resulted in an extensive line of ASCO products that range from two position on/off valves to entire flow control solutions designed to meet requirements of thousands of customers. Whether you need a minor modification of a core product or a complete flow control solution, we can help. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8030_Series.jpg" alt="Asco 8030 Series" title="Asco 8030 Series"/></div>
			<div id="PartsContent"><h3>8030 Series</h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Brass body. 3/8" thru 3/4" pipe size
			<br/><br/>
			Used in double block and vent systems and mounted in the vent piping between the shutoff valves. 
			Provides on-off control of fuel gas in vent line. Must be vented to the outside atmosphere. Brass 
			body. 3/8" thru 3/4" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030_ncr2.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030_nor1.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030r2.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			</p>
</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8040_Series.jpg" alt="Asco 8040 Series" title="Asco 8040 Series"/></div>
			<div id="PartsContent"><h3>8040 Series
			<br/><font size="2" color="#50658D">High Flow/Low Pressure - 1/8"- 3/8"</font></h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Aluminum body. 1/8" thru 3/8" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8040_nc_pilot.pdf" target="_blank"><font color="#ACB0C3"><b>8040 Catalog</b></font></a>
			</p>
</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8040_Series_2.jpg" alt="Asco 8040 Series 2" title="Asco 8040 Series 2"/></div>
			<div id="PartsContent"><h3>8040 Series 2</h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Aluminum body. 3/8" thru 1-1/4" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8040_ncr1.pdf" target="_blank"><font color="#ACB0C3"><b>8040 Catalog</b></font></a>
			</p>
</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8042_Series.jpg" alt="Asco 8042 Series" title="Asco 8042 Series"/></div>
			<div id="PartsContent"><h3>8042 Series</h3>
			<p><br/>Internal pilot operated valve with 5 pound closing spring providing on-off control of fuel gas. Aluminum body. 3/4" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8042r2.pdf" target="_blank"><font color="#ACB0C3"><b>8042 Catalog</b></font></a>
			</p>
</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8043_Series.jpg" alt="Asco 8043 Series" title="Asco 8043 Series"/></div>
			<div id="PartsContent"><h3>8043 Series</h3>
			<p><br/>Internal pilot operated valve providing on-off control of fuel gas. Aluminum body. 3/4" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8043r2.pdf" target="_blank"><font color="#ACB0C3"><b>8043 Catalog</b></font></a>
			</p>
</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8044_Series.jpg" alt="Asco 8044 Series" title="Asco 8044 Series"/></div>
			<div id="PartsContent"><h3>8044 Series</h3>
			<p><br/>Provides on-off control of fuel gas. Provides visual indication of the valves open or closed 
			position. The free handle will not open the valve until the solenoid is energized allowing the 
			lever to engage, then the lever can be manually raised to the latched popsition to open the 
			valve. Aluminum body. 3/4" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8044r2.pdf" target="_blank"><font color="#ACB0C3"><b>8044 Catalog</b></font></a>
			</p>
</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8210_Series.jpg" alt="Asco 8210 Series" title="Asco 8210 Series"/></div>
			<div id="PartsContent"><h3>8210 Series</h3>
			<p><br/>Internal pilot operated valve providing on-off control of fuel gas. Brass body. 3/8" thru 3/4" pipe size.
			<br/><br/>
			Used in double block and vent systems and mounted in the vent piping between the shutoff valves. 
			Provides on-off control of fuel gas in vent line. Must be vented to the outside atmosphere. Brass 
			body. 3/8" thru 3/4" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8210_ncr1.pdf" target="_blank"><font color="#ACB0C3"><b>8210 Catalog</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_200_Series_suffix_C.jpg" alt="Asco 8214(200) Series (suffix C)" title="Asco 8214(200) Series (suffix C)"/></div>
			<div id="PartsContent"><h3>8214(200) Series
			<br/><font size="2" color="#50658D">(suffix C)</font></h3>
			<p><br/>Internal pilot operated shutoff valves providing on-off control of fuel gas with visual and 
			electrical indication of valve's open and close position. General purpose or watertight enclosure.
			Aluminum body. 3/4" thru 3" pipe size.  Optional silicon free construction available (Suffix SF).
			<br/><br/>
			For double valve construction, view the Modular Gas Valve web page. 
			<br/><br/>
			To learn more about ASCO's Modular Gas Valves, view features and benefits information sheet.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_200a.pdf" target="_blank"><font color="#ACB0C3"><b>8214(200) (suffix C) Catalog</b></font></a>
			</p>
</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_200_Series_suffix_VI.jpg" alt="Asco 8214(200) Series (suffix VI)" title="Asco 8214(200) Series (suffix VI)"/></div>
			<div id="PartsContent"><h3>8214(200) Series
			<br/><font size="2" color="#50658D">(suffix VI)</font></h3>
			<p><br/>Internal pilot operated shutoff valves providing on-off control of fuel gas with visual 
			indication of the valve's open and close position.  General purpose or watertight enclosure.  
			Aluminum body. 3/4" thru 3" pipe size.  Optional silicon free construction available (Suffix SF).
			<br/><br/>
			For double valve construction, view the Modular Gas Valve web page. 
			<br/><br/>
			To learn more about ASCO's Modular Gas Valves, view features and benefits information sheet.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_200a.pdf" target="_blank"><font color="#ACB0C3"><b>8214(200) (suffix VI) Catalog</b></font></a>
			</p>
</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_200_Series.jpg" alt="Asco 8214(200) Series" title="Asco 8214(200) Series"/></div>
			<div id="PartsContent"><h3>8214(200) Series</h3>
			<p><br/>Internal pilot operated shutoff valves providing on-off control of fuel gas. General 
			purpose or watertight enclosure.  Aluminum body. 3/4" thru 3"  pipe size.  Optional silicon 
			free construction available (Suffix SF).
			<br/><br/>
			For double valve construction, view the Modular Gas Valve web page. 
			<br/><br/>
			To learn more about ASCO's Modular Gas Valves, view our features and benefits information sheet.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_200a.pdf" target="_blank"><font color="#ACB0C3"><b>8214(200) Catalog</b></font></a>
			</p>
</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_Series_general_purpose.jpg" alt="Asco 8214 Series General Purpose" title="Asco 8214 Series General Purpose"/></div>
			<div id="PartsContent"><h3>8214 Series
			<br/><font size="2" color="#50658D">General Purpose</font></h3>
			<p><br/>Used in double block and vent systems and mounted in the vent piping between the shutoff 
			valves. Provides on-off control of fuel gas in vent line. Must be vented to the outside 
			atmosphere. Aluminum body. 3/4" thru 2 1/2 " pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_gp_nc.pdf" target="_blank"><font color="#ACB0C3"><b>8214 General Purpose Catalog</b></font></a>
			</p>
</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_Series_suffix_C.jpg" alt="Asco 8214 Series (suffix C)" title="Asco 8214 Series (suffix C)"/></div>
			<div id="PartsContent"><h3>8214 Series
			<br/><font size="2" color="#50658D">(suffix C)</font></h3>
			<p><br/>Internal pilot operated valve providing on-off control of fuel gas. Aluminum body. 3/4" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_gp_nc_cr1.pdf" target="_blank"><font color="#ACB0C3"><b>8214 (suffix C) Catalog</b></font></a>
			</p>
</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_Series_suffix_VI.jpg" alt="Asco 8214 Series (suffix VI)" title="Asco 8214 Series (suffix VI)"/></div>
			<div id="PartsContent"><h3>8214 Series
			<br/><font size="2" color="#50658D">(suffix VI)</font></h3>
			<p><br/>Internal pilot operated valve providing on-off control of fuel gas. Aluminum body. 3/4" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_gp_nc_vir1.pdf" target="_blank"><font color="#ACB0C3"><b>8214 (suffix VI) Catalog</b></font></a>
			</p>
</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8214_Series_water_tight.jpg" alt="Asco 8214 Series Water Tight" title="Asco 8214 Series Water Tight"/></div>
			<div id="PartsContent"><h3>8214 Series
			<br/><font size="2" color="#50658D">Water Tight</font></h3>
			<p><br/>Used in double block and vent systems and mounted in the vent piping between the shutoff 
			valves. Provides on-off control of fuel gas in vent line. Must be vented to the outside atmosphere. 
			Aluminum body. 3/8" thru 2 " pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_wt_nor1.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Water Tight Catalog</b></font></a>
			</p>
</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8262_Series.jpg" alt="Asco Series 8262" title="Asco Series 8262"/></div>
			<div id="PartsContent"><h3>Series 8262</h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Brass body. 1/8" thru 1/4" pipe size.
			</p>
</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/AH2_E_On_Off_Actuator.jpg" alt="Asco AH2(E) On-Off Actuator" title="Asco AH2(E) On-Off Actuator"/></div>
			<div id="PartsContent"><h3>AH2(E) On-Off Actuator</h3>
			<p><br/>AH(E) push-type self-contained electrohydraulic linear actuators. 
			Used with all V710 gas valve bodies through 4" pipe size to provide on-off 
			control of main gas lines to industrial and commercial burners. Power interruption 
			fully closes the valve in one second or less.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/ah2e.pdf" target="_blank"><font color="#ACB0C3"><b>AH2(E) On-Off Actuator Catalog</b></font></a>
			</p>
</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/Double_Hydramotor_Configuration.jpg" alt="Asco Double Hydramotor Configuration" title="Asco Double Hydramotor Configuration"/></div>
			<div id="PartsContent"><h3>Double Hydramotor Configuration</h3>
			<p><br/>Connect two AH(E)/V710(B) electro-hydraulic safety shutoff valves together 
			for slow opening applications up to 15 psi of pressure.  Joining these two valves 
			provide the highest flow and pressure to meet the industry's most demanding 
			requirements.  General purpose or watertight enclosure.  Aluminum body.  3/4" 
			thru 3" pipe size. Optional flange adapters available for ease of installation 
			and service.
			<br/><br/>
			Optional flange adapters available for ease of installation and service.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/ah2e.pdf" target="_blank"><font color="#ACB0C3"><b>AH2(E) On-Off Actuator Catalog</b></font></a>
			</p>
</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/HV216_Cable_valve.jpg" alt="Asco HV216 JV216 Cable Valve" title="Asco HV216 JV216 Cable Valves"/></div>
			<div id="PartsContent"><h3>HV216 JV216 Cable Valve</h3>
			<p><br/>Cable controled valves type valves used for gas shut off in commercial, industrial and institutional kitche ns. Aluminum body. 1/2" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/hv_jv216r1.pdf" target="_blank"><font color="#ACB0C3"><b>HV216 JV216 Cable Valve Catalog</b></font></a>
			</p>
</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/HV266_Series_Stainless_Steel.jpg" alt="Asco HV266 Series (Stainless Steel)" title="Asco HV266 Series (Stainless Steel)"/></div>
			<div id="PartsContent"><h3>HV266 Series
			<br/><font size="2" color="#50658D">(Stainless Steel)</font></h3>
			<p><br/>Internal pilot operated valve with providing on-off control of fuel gas. Stainless steel body. 1/24" thru 1 " pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/hv266.pdf" target="_blank"><font color="#ACB0C3"><b>HV266 Catalog</b></font></a>
			</p>
</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/K3A_Series.jpg" alt="Asco K3A Series" title="Asco K3A Series"/></div>
			<div id="PartsContent"><h3>K3A Series</h3>
			<p><br/>Direct acting (K3A, 4, 5, 7) and internal pilot operated (K3A6) valves providing on-off control of fuel gas. Aluminum body. 3/8" thru 1 1/2 " pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/k3a_all.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			</p>
</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/LP_Valves.jpg" alt="Asco LP Valves" title="Asco LP Valves"/></div>
			<div id="PartsContent"><h3>LP Valves</h3>
			<p><br/>2-way normally closed valves for liquid petroleum gases (propine) in both liquified and 
			gaseous states. Brass and aluminum body. 1/4" thru 3/4" pipe size.
			</p>
</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/Optional_Hardwar_Kits_For_Modular_Gas_Valves.jpg" alt="Optional Hardware Kits for Modular Gas Valves" title="Optional Hardware Kits for Modular Gas Valves"/></div>
			<div id="PartsContent"><h3>Optional Hardware Kits for Modular Gas Valves</h3>
			<p><br/>Optional connecting kits and flange adapters are available for ease of 
			installation and service
			</p>
</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/S261_Series.jpg" alt="Asco S261 Series" title="Asco S261 Series"/></div>
			<div id="PartsContent"><h3>S261 Series</h3>
			<p><br/>Internal pilot operated valve providing on-off control of fuel gas. Aluminum body. 3/8" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/s261r2.pdf" target="_blank"><font color="#ACB0C3"><b>S261 Catalog</b></font></a>
			</p>
</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/S262_Series.jpg" alt="Asco S262 Series" title="Asco S262 Series"/></div>
			<div id="PartsContent"><h3>S262 Series</h3>
			<p><br/>Used in double block and vent systems and mounted in the vent piping between the shutoff 
			valves. Provides on-off control of fuel gas in vent line. Must be vented to the outside 
			atmosphere. Aluminum body. 3/8" thru 3" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/imsheets/s262-1.pdf" target="_blank"><font color="#ACB0C3"><b>S262 Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/s262r1.pdf" target="_blank"><font color="#ACB0C3"><b>S262 Catalog</b></font></a>
			</p>
</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/Single_8214_200_Series.jpg" alt="Asco Single 8214(200) Series" title="Asco Single 8214(200) Series"/></div>
			<div id="PartsContent"><h3>Single 8214(200) Series</h3>
			<p><br/>Internal pilot operated shutoff valves provides on-off control of 
			fuel gas which are available in aluminum body, 3/4" thru 3" pipe size. 
			Optional visual indication (VI), proof of closure (C) and silicon-free 
			(SF) constructions available.
			<br/><br/>
			Optional flange adapters available for ease of installation and service.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8214_200a.pdf" target="_blank"><font color="#ACB0C3"><b>Single 8214(200) Catalog</b></font></a>
			</p>
</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/Single_V710_B_Series.jpg" alt="Asco Single V710(B) Series" title="Asco Single V710(B) Series"/></div>
			<div id="PartsContent"><h3>Single V710(B) Series</h3>
			<p><br/>The V710(B) valve is a push-to-open type body for on/off control of 
			fuel gas when mounted to an AH2(E) Hydramotor actuator. The V710(B) valve 
			body is available in quick opening and quick opening with valve seal 
			overtravel trim styles are available in aluminum body, 3/4" thru 3" pipe size.
			<br/><br/>
			Optional flange adapters available for ease of installation and service.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/v710b_r1.pdf" target="_blank"><font color="#ACB0C3"><b>Single V710(B) Catalog</b></font></a>
			</p>
</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/Solenoid_and_Hydramotor_Configuration.jpg" alt="Asco Solenoid and Hydramotor Configuration" title="Asco Solenoid and Hydramotor Configuration"/></div>
			<div id="PartsContent"><h3>Solenoid and Hydramotor Configuration</h3>
			<p><br/>Connect an 8214(200) solenoid valve and an AH(E)/V710(B) electro-hydraulic 
			safety shutoff valve together for the most economical slow opening applications, 
			up to 5 psi of pressure.  General purpose or watertight enclosure, a luminum body, 
			3/4" thru 3" pipe size.  Optional flange adapters available for ease of 
			installation and service.
			<br/><br/>
			Optional flange adapters available for ease of installation and service.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/doublevalver1.pdf" target="_blank"><font color="#ACB0C3"><b>Solenoid and Hydramotor Configuration Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/doublevalve_benefits.pdf" target="_blank"><font color="#ACB0C3"><b>Solenoid and Hydramotor Configuration Benefits</b></font></a>
			</p>
</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/SV311.jpg" alt="Asco SV311 Series" title="Asco SV311 Series"/></div>	
			<div id="PartsContent"><h3>SV311 Series</h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Aluminum body. 1/8" thru 3/4" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/sv311_nc.pdf" target="_blank"><font color="#ACB0C3"><b>SV311 Catalog</b></font></a>
			</p>
</div></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Asco/8030_Series.jpg" alt="Asco 8030 Series" title="Asco 8030 Series"/></div>
			<div id="PartsContent"><h3>8030 Series</h3>
			<p><br/>Direct acting valves providing on-off control of fuel gas. Brass body. 3/8" thru 3/4" pipe size
			<br/><br/>
			Used in double block and vent systems and mounted in the vent piping between the shutoff valves. 
			Provides on-off control of fuel gas in vent line. Must be vented to the outside atmosphere. Brass 
			body. 3/8" thru 3/4" pipe size.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030_ncr2.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030_nor1.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			<br/><a href="http://www.ascovalve.com/common/pdffiles/product/8030r2.pdf" target="_blank"><font color="#ACB0C3"><b>8030 Catalog</b></font></a>
			</p>
</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8030_Series_Thumbnail.gif" title="Asco 8030 Series" alt="Asco 8030 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8040_Series_Thumbnail.gif" title="Asco 8040 Series" alt="Asco 8040 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8040_Series_2_Thumbnail.gif" title="Asco 8040 Series 2" alt="Asco 8040 Series 2" border="0"/></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8042_Series_Thumbnail.gif" title="Asco 8042 Series" alt="Asco 8042 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8043_Series_Thumbnail.gif" title="Asco 8043 Series" alt="Asco 8043 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8044_Series_Thumbnail.gif" title="Asco 8044 Series" alt="Asco 8044 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8210_Series_Thumbnail.gif" title="Asco 8210 Series" alt="Asco 8210 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_200_Series_suffix_C_Thumbnail.gif" title="Asco 8214(200) Series (suffix C)" alt="Asco 8214(200) Series (suffix C)" border="0"/></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_200_Series_suffix_VI_Thumbnail.gif" title="" title="Asco 8214(200) Series (suffix VI)" alt="" title="Asco 8214(200) Series (suffix VI)" border="0"/></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_200_Series_Thumbnail.gif" title="Asco 8214(200) Series" alt="Asco 8214(200) Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_Series_general_purpose_Thumbnail.gif" title="Asco 8214 Series General Purpose" alt="Asco 8214 Series General Purpose" border="0"/></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_Series_suffix_C_Thumbnail.gif" title="Asco 8214 Series (suffix C)" alt="Asco 8214 Series (suffix C)" border="0"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_Series_suffix_VI_Thumbnail.gif" title="Asco 8214 Series (suffix VI)" alt="Asco 8214 Series (suffix VI)" border="0"/></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8214_Series_water_tight_Thumbnail.gif" title="Asco 8214 Series Water Tight" alt="Asco 8214 Series Water Tight" border="0"/></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/8262_Series_Thumbnail.gif" title="Asco Series 8262" alt="Asco Series 8262" border="0"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/AH2_E_On_Off_Actuator_Thumbnail.gif" title="Asco AH2(E) On-Off Actuator" alt="Asco AH2(E) On-Off Actuator" border="0"/></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/Double_Hydramotor_Configuration_Thumbnail.gif" title="Asco Double Hydramotor Configuration" alt="Asco Double Hydramotor Configuration" border="0"/></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/HV216_Cable_valve_Thumbnail.gif" title="Asco HV216 JV216 Cable Valve" alt="Asco HV216 JV216 Cable Valve" border="0"/></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/HV266_Series_Stainless_Steel_Thumbnail.gif" title="Asco HV266 Series (Stainless Steel)" alt="Asco HV266 Series (Stainless Steel)" border="0"/></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/K3A_Series_Thumbnail.gif" title="Asco K3A Series" alt="Asco K3A Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/LP_Valves_Thumbnail.gif" title="Asco LP Valves" alt="Asco LP Valves" border="0"/></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/Optional_Hardware_Kits_for_Modular_Gas_Valves_Thumbnail.gif" title="Optional Hardware Kits for Modular Gas Valves" alt="Optional Hardware Kits for Modular Gas Valves" border="0"/></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/S261_Series_Thumbnail.gif" title="Asco S261 Series" alt="Asco S261 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/S262_Series_Thumbnail.gif" title="Asco S262 Series" alt="Asco S262 Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/Single_8214_200_Series_Thumbnail.gif" title="Asco Single 8214(200) Series" alt="Asco Single 8214(200) Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/Single_V710_B_Series_Thumbnail.gif" title="Asco Single V710(B) Series" alt="Asco Single V710(B) Series" border="0"/></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/Solenoid_and_Hydramotor_Configuration_Thumbnail.gif" title="Asco Solenoid and Hydramotor Configuration" alt="Asco Solenoid and Hydramotor Configuration" border="0"/></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Asco/thumbnails/SV311_Thumbnail.gif" title="Asco SV311 Series" alt="Asco SV311 Series" border="0"/></a></li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size=2 color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</b></div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <OBJECT classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <EMBED src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </EMBED>
        </OBJECT>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>