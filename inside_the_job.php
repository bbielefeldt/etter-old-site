<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="keywords" content="ETTER Engineering,ETTER Engineering reviews,Inside the Job,ETTER Engineering Case Studies,gas booster reviews, natural gas booster review" />
<title>ETTER Engineering - Inside the Job</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css"/>	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
<script type="text/javascript">
google.load("jquery", "1");
</script>
</head>
<body link="#445679" vlink="#445679">
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="http://www.google.com/jsapi" type="text/javascript"></script>
<script type="text/javascript">
  google.load('search', '1', {language : 'en', style : google.loader.themes.SHINY});
  google.setOnLoadCallback(function() {
    var customSearchControl = new google.search.CustomSearchControl('012677673255316824096:sean13fvlei');
    customSearchControl.setResultSetSize(google.search.Search.FILTERED_CSE_RESULTSET);
    var options = new google.search.DrawOptions();
    options.setSearchFormRoot('cse-search-form');
    customSearchControl.draw('cse', options);
  }, true);
</script>
<script type="text/javascript">
    $("#hidr").click(function () {
      $("#cse").hide("slow", function () {
        // use callee so don't have to name the function
        $(this).hide("fast"); 
      });
    });
    $("#cse-search-form").click(function () {
      $("#cse").show(2000);
    });
</script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<div id="LightBlueBkgrd"></div>
<div id="SolidWhiteBkgrd"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="DropBottomSolidWhiteBkgrd"></div>
<div id="SolidWhiteBkgrdBottom"></div>
<div id= "RedBrowseBar"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="Linkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<div id="InsidetheJobWhite"></div>
<div id="InsidetheJobWhiteRight"></div>
<div id="InsideTheJobLeftTxt">ETTER Engineering's Inside the Job offers you an insight into our most recent projects. 
Inside the Job is our way of providing you with the intricate details and workings that go into some of the most 
impressive ETTER products. As well as providing you with the success stories of many satisfied ETTER customers.
<br/><br/><br/>
<font size="2" color="#D21D1F"><b>Podojil Builders, Inc</b></font>
<br/><a href="inside_the_job.php"><font size="1"><b>- IHOP</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>PJKennedy &amp; Sons</b></font>
<br/><a href="inside_the_job_2.php"><font size="1"><b>- Boston Fish Pier</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>The Nutmeg Companies, Inc</b></font>
<br/><a href="inside_the_job_3.php"><font size="1"><b>- Connecticut Forensic Investigators</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>Frank I. Rounds</b></font>
<br/><a href="inside_the_job_4.php"><font size="1"><b>- Verizon Wireless</b></font></a>
<br/><br/><font size="2" color="#D21D1F"><b>A&M Bronx Baking</b></font>
<br/><a href="inside_the_job_5.php"><font size="1"><b>- Oil-to-Gas Conversion</b></font></a>
</div>
<div id="InsidetheJob"><div id="InsideIHOPPhoto"></div>
<div id="PodojilLogo"></div>
<div id="Philadelphia"><b>Philadelphia, Pennsylvania - </b></div>
<div id="PABoosterPhoto"></div>
<blockquote>
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
Everyone around here loves pancakes, so when the team at ETTER Engineering was asked to help out on a project to build a new 
IHOP restaurant in Philadelphia, they jumped at the chance! The city of brotherly love is known for many things - a diverse culture,
rich national history, and passionate sports fans amongst them - but it certainly cannot claim to be the natural gas pressure capital 
of the Northeast. This is where Ohio-based Podojil Builders ran into a problem while constructing the building for the city's lastest 
IHOP restaurant.
<br/><br/>
According to Vice President of Podojil Builers Paul Podojil, "downtown Philadelphia is supplied by a low pressure natural gas system, 
so [Utility Company] Philadelphia Gas Works could only garantee a street pressure of up to four and a half inches W.C." With IHOPS's 
kicthen equipment requiring a pressure of seven inches W.C., it was determined a gas booster system was necessary.
<br/><br/>
After doing some research, Paul Podojil contacted ETTER Engineering. Working closely with our engineers, it was determined that an 
ENGB - gasPOD (Gas Pressure On Demand) was the solution to Podojil's gas pressure needs, capable of supplying a steady, regulated 
pressure of 7"W.C. to the restaurant's cooking equipment.
<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
Designed with energy efficiency in mind, ETTER equipped the restaurant's booster with a pressure transducer that monitors the 
existence of flow on the system. By utilizing a VFD to increase and decrease power to the booster motor dependent on the gas 
usage of the building, energy use can be tailored to demand in real-time, cutting down on both fuel use and expenses.
<br/><br/>
In keeping with the company's devotion to top-notch service, ETTER was able to deliver, completely assembled and ready for 
installation, an ENGB-gasPOD in an outdoor steel transclosure, all in nearly half the time quoted by it's competitors, and 
at a better price, thereby ensuring that the grand opening of the IHOP was not delayed.
<br/><br/>
ETTER's customer service, prompt response, and competitive pricing helped ensure Podojil Builders were able to provide a 
quality product to the owner," Podojil said. He continued, saying that "any future needs I encounter for booster systems 
will be handled by the team at ETTER Engineering."</blockquote>
<br/><br/><blockquote><h5>To learn more about how ETTER Engineering can assist you on your next project, 
call 1-800-444-1962.</h5></blockquote></div>
<a href="inside_the_job_2.php" id="InsideJobNextBTN">Next</a>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
<div id="WhiteNEWareaDrop"></div>
<div id="WhiteBlockFooterTwo"></div>
<div id="WhiteBlockFooterThree"></div>
<div id="FooterBarTopDrop"></div>
<div id="FooterBarBottomDrop"></div>
<div id="ENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="LearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="ENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="Gas Booster Video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="ENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
</div>
</body>
</html>