<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
a { text-decoration:none }
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="description" content="ETTER Engineering offers a complete line of Dwyer combustion parts and services" />
<meta name="keywords" content="ETTER Engineering,Dwyer,custom design,Series 2-5000 Minihelic II Differential Pressure Gage,Series 2000 Magnehelic Differential Pressure Gage,Series 2000-SP Magnehelic Differential Pressure Gage,Series 4000 Capsuhelic Differential Pressure Gage,Series DM-1000 DigiMag Digital Differential Pressure and Flow Gages,Series PTGB Differential Pressure Piston-Type Gage,Series 3000MR/3000MRS,Series 3000SGT Photohelic Pressure Switch Gage with Integral Transmitter,Series 43000 Capsu-Photohelic Pressure Switch/Gage,Series A3000 Photohelic Pressure Switch/Gage,Series MP Mini-Photohelic Differential Pressure Switch/Gage,Series DH Digihelic Differential Pressure Controller,Series DHII Digihelic II Differential Pressure Controller,Series DH3 Digihelic Differential Pressure Controller,Series 1620 Single and Dual Pressure Switch,Series 1630 Large Diaphragm Differential Pressure Switch,Series 1640 Floating contact Null Switch for High and Low Actuation,Series 1700 Low Differential Pressure Switch Designed for OEM Products,Series 1800 Low Differential Pressure Switch for General Industrial Service,Series 1900 Compact Low Differential Pressure Switch,Series 1950G Explosion-proof Differential Pressure Switch,1996 Gas Pressure Switch,Series ADPS HAVC Differential Pressure Switch,Series BYDS Bypass Damper Switch,Series H3 Explosion-Proof Differential Pressure Switch,Series 3100D Explosion-proof Differential Pressure Transmitter,Series 603A Differential Pressure Indicating Transmitter,Series 604D Minihelic Differential Pressure Indicating Transmitter,Series 605 Magnehilc Differential Pressure Indicating,Series 631B Wet/Wet Differential Pressure Transmitter,Series 636D Fixed Range Differential Pressure Transmitter,Series 655A 316 Wet/Wet Differential Pressure Switch,Series DM-2000 Differential Pressure Transmitter,Series 3200G Explosion proof Pressure Transmitter,Series MS Magnesense Differential Pressure Transmitter" />
<title>ETTER Engineering - Dwyer Parts Library</title>
<!--[if !IE]><!-->
<link type="text/css" rel="stylesheet" href="ParaStyle.css" />	
<!--<![endif]-->
<!--[if IE]>
  <link rel="stylesheet" type="text/css" href="ie7-only.css" />
<![endif]-->
<!--[if lte IE 6]>
	<link rel="stylesheet" type="text/css" href="all-ie6-only2.css" />	
<![endif]-->
<!--[if gte IE 8]>
  <link rel="stylesheet" type="text/css" href="all-ie-only2.css" />
<![endif]-->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<!-- include Cycle plugin -->
<script type="text/javascript" src="includes/jquery.cycle.all.2.74.js"></script>
<script type="text/javascript" src="includes/javascript.js"> </script>
<script type="text/javascript" src="includes/ENGB_Gas_Booster_Video.js"> </script>
    <script src="jquery-1.2.6.js" type="text/javascript" charset="utf-8"></script>
    <script src="jquery-ui-full-1.5.2.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="includes/SliderGallery.js"></script>
    <script type="text/javascript" src="includes/PartsLibrary.js"></script>
    <script type="text/javascript" src="includes/VideoRollover.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        var $ = jQuery;
    </script>
</head>
<body>
<div id="Wrapper">
<div id="Hidebutton"><button id="hidr"><font color="#494A4A"><b>X</b></font></button></div>
<div id="cse-search-form" style= "z-index:999997; top:0px;"></div>
<div id="cse-search-form" style="width: 100%;"></div>
<div id="cse" style="width:43%; z-index:999996; top:40px;"></div>
<script src="googlejs.js" type="text/javascript"></script>
<div id="Head"></div>
<div id="BluePrint"></div>
<div id="NavBar"></div>
<div id="NavBarDrop"></div>
<div id="NavBar2"></div>
<div id="NavBarDrop2"></div>
<div id="Red"></div>
<div id="DropRight"></div>
<div id="DropLeft"></div>
<!--Logo / Logo Link Begin--> 
<a href="http://www.etterengineering.com/" id="Logo"></a>
<a href="http://www.etterengineering.com/" id="Tagline">to ALL your process heating &amp; combustion needs!</a>      
<!--Logo / Logo Link End--> 

<?php include("mainnav.php"); ?>

<a href="http://www.linkedin.com/company/etter-engineering" id="PartsLinkedin" alt="Process Heating Linkedin" title="Process Heating Linkedin"></a>
<a href="parts_by_manufacturer_antunes_controls.php" id="AntunesLogo"></a>
<a href="parts_by_manufacturer_asco.php" id="AscoLogo"></a>
<a href="parts_by_manufacturer_bryan_donkin.php" id="BryanDonkinLogo"></a>
<a href="parts_by_manufacturer_dwyer.php" id="DwyerLogo"></a>
<a href="parts_by_manufacturer_maxon.php" id="MaxonLogo"></a>
<a href="parts_by_manufacturer_kromschroder.php" id="KromSchroderLogo"></a>
<a href="parts_by_manufacturer_shinko.php" id="ShinkoLogo"></a>
<a href="parts_by_manufacturer_maxitrol.php" id="MaxitrolLogo"></a>
<a href="parts_by_manufacturer_vulcan_catalytic.php" id="VulcanLogo"></a>
<a href="parts_by_manufacturer_sensus.php" id="SensusLogo"></a>
<a href="parts_by_manufacturer_partlow.php" id="PartlowLogo"></a>
<a href="parts_by_manufacturer_protection_controls.php" id="ProtectionLogo"></a>
<a href="parts_by_manufacturer_eclipse.php" id="EclipseLogo"></a>
<a href="parts_by_manufacturer_hauck.php" id="HauckLogo"></a>
<a href="parts_by_manufacturer_emon.php" id="EmonLogo"></a>
<a href="parts_by_manufacturer_durag.php" id="DuragLogo"></a>
<div id="HoneywellLogo" title="Manufacturer Available Online Soon"></div>
<a href="parts_by_manufacturer_siemens.php" id="SiemensLogo"></a>
<a href="contact_us_employee_directory.php" id="PartsRequstInfo"></a>
<a href="pdfs/ETTER-Engineering-Parts-Line-Card.pdf" id="PartsLineCardBTN"></a>
<div id="LeftGrayBox"></div>
<div id="RightGrayBox"></div>
<div id="RedParts"></div>
<div id="DwyerLogoLarge"></div>
<div id="SensusText">Dwyer is a leading manufacturer in the controls and instrumentation industry, continuing to grow 
and serve major markets including, but not limited to HVAC, chemical, food, oil and gas, and 
pollution control. New applications are discovered daily through a cooperative effort between 
Dwyer and its customers. Dwyer continues to develop market demand products and further their success from leading established 
brand names such as the Magnehelic&reg; pressure gages, Photohelic&reg; switch/gages, Rate Master&reg; 
flowmeters, and Hi-Flow&reg; valves. </div>
<div id="ThumbnailBackground"></div>
<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_2_5000_Minihelic_II_Differential_Pressure_Gage.jpg" alt="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage" title="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage"/></div>
			<div id="PartsContent"><h3>Series 2-5000 
			<br/><font color="#445678">Minihelic&reg; II Differential Pressure Gage</font></h3>
			<p>Combining clean design, small size and low cost with enough accuracy for all but the most demanding applications our 
			Minihelic&reg; II gage offers the latest in design features for a dial type differential pressure gage. It is our most compact 
			gage but is easy to read and can safely operate at total pressures up to 30 psig. The Minihelic&reg; II gage is designed for panel 
			mounting in a single 2-5/8" diameter hole. Standard pressure connections are barbed fittings for 3/16" I.D. tubing; optional 1/8" 
			male NPT connections are also available. Over-pressure protection is built into the Minihelic&reg; II gage by means of a blow-out 
			membrane molded in conjunction with the diaphragm. Accidental over-ranging up to the rated total pressure will not damage the gage. 
			With removable lens and rear housing, the gage may be easily serviced at minimum cost.
			<br/><br/>With the housing molded from mineral and glass filled nylon and the lens molded from acrylic, the gage will withstand 
			rough use and exposure as well as high total pressure. The 5% accuracy and low cost of the Minihelic&reg; II gage make it 
			well-suited for a wide variety of OEM and user applications. OEM applications include cabinet air purging, medical respiratory 
			therapy equipment, air samplers, laminar flow hoods, and electronic air cooling systems. As an air filter gage, the 
			Minihelic&reg; II gage finds many end use applications on large stationary engines, compressors, ventilators, and air handling 
			units. The Minihelic&reg; II gage is suitable for many of the same applications as the Magnehelic&reg; gage where the greater 
			accuracy, sensitivity, and higher and lower differential pressure ranges of the Magnehelic&reg; gage are not required.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/2-5000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/2-5000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_8i_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="2" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_2000_SP_Magnehelic_Differential_Pressure_Gages.jpg" alt="Dwyer Series 2000 Magnehelic Differential Pressure Gage" title="Dwyer Series 2000 Magnehelic Differential Pressure Gage"/></div>
			<div id="PartsContent"><h3>Series 2000 
			<br/><font color="#445678">Magnehelic&reg; Differential Pressure Gage</font></h3>
			<p>Select the Magnehelic&reg; gage for high accuracy--guaranteed within 2% of full scale--and for the wide choice of 81 models 
			available to suit your needs precisely. Using Dwyer's simple, frictionless Magnehelic&reg; gage movement, it quickly indicates 
			low air or non-corrosive gas pressures--either positive, negative (vacuum) or differential. The design resists shock, vibration 
			and over-pressures. No manometer fluid to evaporate, freeze or cause toxic or leveling problems. It's inexpensive, too.
			<br/><br/>The Magnehelic&reg; gage is the industry standard to measure fan and blower pressures, filter resistance, air velocity, 
			furnace draft, pressure drop across orifice plates, liquid levels with bubbler systems and pressures in fluid amplifier or fluidic 
			systems. It also checks gas-air ratio controls and automatic valves, and monitors blood and respiratory pressures in medical 
			care equipment.
			<br/><br/>Note: May be used with Hydrogen. Order a Buna-N diaphragm. Pressures must be less than 35 psi. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/2000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - Series 2000</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/2000-AF_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - Series 2000 AF</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/2000-HP-MP_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - Series 2000 with HP or MP Option</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/2000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/2000_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="3" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_2000_SP_Magnehelic_Differential_Pressure_Gages.jpg" alt="Dwyer Series 2000-SP Magnehelic Differential Pressure Gage" title="Dwyer Series 2000-SP Magnehelic Differential Pressure Gage"/></div>
			<div id="PartsContent"><h3>Series 2000-SP 
			<br/><font color="#445678">Magnehelic Differential Pressure Gage</font></h3>
			<p>Bright red LED on right of scale shows when setpoint is reached. Field adjustable from gage face, unit operates on 12-24 VDC. 
			Requires MP or HP style cover and bezel. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/2000-SP_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/2000-SP_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/2000-SP_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="4" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_4000_Capsuhelic_Differential_Pressure_Gage.jpg" alt="Dwyer Series 4000 Capsuhelic Differential Pressure Gage" title="Dwyer Series 4000 Capsuhelic&reg; Differential Pressure Gage"/></div>
			<div id="PartsContent"><h3>Series 4000 
			<br/><font color="#445678">Capsuhelic&reg; Differential Pressure Gage</font></h3>
			<p>The Capsuhelic&reg; gage is designed to give fast, accurate indication of differential pressures. The gage may 
			be used as a readout device when measuring flowing fluids, pressure drop across filters, liquid levels in storage 
			tanks and many other applications involving pressure, vacuum or differential pressure.
			<br/><br/>Using the basic design 
			of Dwyer's time-proven Magnehelic&reg; gage, the Capsuhelic&reg; gage contains a simple, frictionless movement that 
			permits full scale readings as low as 0.5 inch water column. The pressure being measured is held within a capsule 
			which is an integral part of the gage. This containment of the pressure permits the use of the gage on system 
			pressures of up to 500 psig, even when differentials to be read are less than 0.1 inch w.c.
			<br/><br/>The diaphragm-actuated Capsuhelic&reg; gage requires no filling liquid which might limit its outdoor 
			applications. Zero and range adjustments are made from outside the gage, and there is no need to disassemble 
			the gage in normal service.
			<br/><br/>Note: May be used with Hydrogen. Order a Buna-N diaphragm. Pressures must be less than 35 psi.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/4000_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/4000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/4000_intl.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="5" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_DM_1000_DigiMag_Digital_Differential_Pressure_and_Flow_Gages.jpg" alt="Dwyer Series DM-1000 DigiMag Digital Differential Pressure and Flow Gages" title="Dwyer Series DM-1000 DigiMag Digital Differential Pressure and Flow Gages"/></div>
			<div id="PartsContent"><h3>Series DM-1000 
			<br/><font color="#445678">DigiMag&reg; Digital Differential Pressure and Flow Gages</font></h3>
			<p>The DigiMag&reg; Series DM-1000 Digital Differential Pressure and Flow Gages monitor 
			the pressure of air and compatible gases just as its famous analog predecessor the 
			Magnehelic&reg; Differential Pressure Gage. All models are factory calibrated to 
			specific ranges. The 4-digit LCD can display readings in common English and metric 
			units so conversions are not necessary. The simplified four button operation reduces set 
			up time and simplifies calibration with its digital push button zero and span.
			<br/><br/>The DigiMag&reg; Digital Gages are more versatile than analog gages with 
			their ability to be field-programmed to select pressure, air velocity or flow operation 
			depending on model. The DigiMag&reg; Digital Gages have an added feature for filter 
			applications where a set point can be input where the display will blink when the 
			filter is dirty, alerting the user that a maintenance action needs to occur.
			<br/><br/>Programming the Series DM-1000 is easy using the menu key to access 4 simplified 
			menus which provide access to, depending on model: security level; engineering units; K-factor 
			for use with various Pitot tubes and flow sensors circular or rectangular duct size for volumetric 
			flow operation; filter set point; view peak and valley process readings; digital damping for
			smoothing erratic process applications; display update to conserve battery life; zero and span field calibration.
			<br/><br/>The Series DM-1000 DigiMag&reg; Digital Differential Pressure and Air Flow Gages possess a 
			full-scale accuracy of 1% on ranges down to 2" w.c. and 2% accuracy down to the very low range 
			of 1" to 0.25" w.c. DigiMag&reg; Digital Differential Pressure Gages offer power versatility by 
			working with 9-24 VDC line power or simply 9V battery power. If using line power and connecting 
			the 9V battery, the battery will act as a back-up if line power is lost or interrupted.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/P_DM_1100_low.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - DM-1100</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DM-1200_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - DM-1200</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/DM-1000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DM-1000_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="6" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_PTGB_Differential_Pressure_Piston_Type_Gage.jpg" alt="Dwyer Series PTGB Differential Pressure Piston-Type Gage" title="Dwyer Series PTGB Differential Pressure Piston-Type Gage"/></div>
			<div id="PartsContent"><h3>Series PTGB 
			<br/><font color="#445678">Differential Pressure Piston-Type Gage</font></h3>
			<p>The Series PTGB Differential Pressure Piston-Type Gage is a low cost differential pressure gage 
			used to measure the pressure drop across filters, strainers, pump performance testing, and heat 
			exchanger pressure drop monitoring. Its simple, rugged and compact design possesses a weather 
			and corrosion resistant gage front with a shatter resistant lens. The Series PTGB contains a 
			piston-sensing element, which provides differential pressure ranges from 0-10 psid to 0-110 
			psid with accuracies of &#177;3-2-3%. Constructed from aluminum or 316 stainless steel and 
			available with two 1/4" female NPT back or end connections, the Series PTGB is 
			cost-effective and accurate in monitoring differential pressure.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/PTGA-PTGB-PTGC_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/PTGA-PTGB-PTGC_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/PTGB_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="7" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_3000MR_3000MRS.jpg" alt="Dwyer Series 3000MR/3000MRS" title="Dwyer Series 3000MR/3000MRS"/></div>
			<div id="PartsContent"><h3>Series 3000MR/3000MRS</h3>
			<p>Using solid state technology, the Series 3000MR and 3000MRS 
			Photohelic&reg; switch/gages combine the functions of a 
			precise, highly repeatable differential pressure switch with a large easy-to-read analog pressure gage employing the durable, 
			time-proven Magnehelic&reg; gage design. Switch setting is easy to adjust with large external knobs on the gage face. Gage reading 
			is unaffected by switch operation - will indicate accurately even if power is interrupted. Solid state design now results in 
			greatly reduced size and weight. Units can be flush mounted in 4-13/16" (122 mm) hole or surface mounted with hardware supplied. 
			3000MR models employ versatile electromechanical relays with gold over silver contacts - ideal for dry circuits. For applications 
			requiring high cycle rates, choose 3000MRS models with SPST (N.O.) solid state relays. All models provide both low and high limit 
			control and include 18-inch (45 cm) cable assemblies for electrical connections.
			<br/><br/>Gage accuracy is &#177;2% of full scale and switch 
			repeatability is &#177;1%. Switch deadband is one pointer width - less than 1% of full scale. Compatible with air and other 
			non-combustible, non-corrosive gases, they can be used in systems with pressures to 25 psig (1.725 bar). Optional construction 
			is available for use to either 35 psig (2.42 bar) or 80 psig (5.51 bar).
			<br/><br/>Included Accessories: mounting ring, snap ring; 18" 
			(45 cm) cable assembly; (2) 3/16" tubing to 1/8" NPT adapters; (2) 1/8" NPT pipe plugs; (4) 6-32 x 1-1/4" RH machine screws 
			(panel mounting); and (3) 6-32 x 5/16" RH machine screws (surface mounting). 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/3000MR_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - 3000MR Photohelic&reg;</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/3000MRS_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - 3000MRS Photohelic&reg;</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/3000MR-3000MRS_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_22i_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="8" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_3000SGT_Photohelic_Pressure_Switch_Gage_with_Integral_Transmitter.jpg" alt="Dwyer Series 3000SGT Photohelic Pressure Switch Gage with Integral Transmitter" title="Dwyer Series 3000SGT Photohelic Pressure Switch Gage with Integral Transmitter"/></div>
			<div id="PartsContent"><h3>Series 3000SGT 
			<br/><font color="#445678">Photohelic&reg; Pressure Switch Gage with Integral Transmitter</font></h3>
			<p>The Series 3000SGT Photohelic&reg; Switch/Gage/Transmitter combines several critical control functions into a single, 
			easy-to-install package. This versatile instrument starts with the universally accepted standard for reliable low air 
			pressure measurement, the Dwyer&reg; Magnehelic&reg; Gage. It measures positive, negative or differential pressures 
			within &#177;2% of full scale accuracy. This time-proven component provides highly reliable analog indication of air 
			or compatible gas pressure on a 4", 80&deg; scale. Gage operation is completely independent - functions normally even 
			if power is interrupted to electrical elements of the control.
			<br/><br/>Next, two DPDT relays are added which serve as Low/High limit controls (or pressure switches) capable of 
			handling up to 10 amps @ 28 VDC or 120/240 VAC directly. Individual set point deadband is one pointer width - less 
			than 1% of full scale; just enough to assure positive, chatter-free operation. Integral holding coils enable user 
			to connect the two so they work like a single control with variable deadband - ideal for applications such as 
			clean room and building pressurization, HVAC systems, automatic air filter or level control and much more. 
			Actuation points are fully adjustable over the entire pressure range with convenient front mounted knobs 
			linked to bright red set point indicators.
			<br/><br/>Finally, the Series 3000SGT Photohelic&reg; Switch/Gage includes 
			a separate 4-20 mA, 2-wire transmitter operating from an external 10-35 VDC power supply. Separate adjustments 
			are included for zero and span inside the rear electronics enclosure. Optional A-700 Power Supply is a perfect 
			companion rated for AC inputs from 100-240V; DC outputs from 24-28V. The transmitter component is an ideal 
			driver for variable speed blowers and fans, damper positioners and for continuous data logging on computerized 
			VAV systems or strip chart recorders.
			<br/><br/>Besides the obvious cost and space saving advantages of combining all 
			these control functions in a single unit, think of the additional savings in time and material with just one 
			set of pneumatic lines to connect instead of three or four.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/3000sgt_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/3000SGT_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/3000sgt_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="9" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_43000_Capsu_Photohelic_Pressure_Switch_Gage.jpg" alt="Dwyer Series 43000 Capsu-Photohelic Pressure Switch/Gage" title="Dwyer Series 43000 Capsu-Photohelic Pressure Switch/Gage"/></div>
			<div id="PartsContent"><h3>Series 43000 
			<br/><font color="#445678">Capsu-Photohelic&reg; Pressure Switch/Gage</font></h3>
			<p>Capsu-Photohelic&reg; Switch/Gages function as versatile, highly repeatable pressure switches combined with 
			a precise pressure gage employing the time-proven Magnehelic&reg; gage design. The Capsu-Photohelic&reg; switch/gage 
			employs an encapsulated sensing element for use with both liquids and gases at pressures to 500 psig (34 bar). 
			Optional cast brass case is available for water or water based liquids. Two phototransistor actuated, 
			DPDT relays are included for low/high limit control. Easy to adjust set point indicators are controlled by 
			knobs located on the gage face. Individual set point deadband is one pointer width - less than 1% of full 
			scale. Set points can be interlocked to provide variable deadband - ideal for control of pumps, etc.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/B_34_low.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/43000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/43000_intl_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="10" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_A3000_Phptphelic_Pressure_Switch_Gage.jpg" alt="Dwyer Series A3000 Photohelic Pressure Switch/Gage" title="Dwyer Series A3000 Photohelic Pressure Switch/Gage"/></div>
			<div id="PartsContent"><h3>Series A3000 
			<br/><font color="#445678">Photohelic&reg; Pressure Switch/Gage</font></h3>
			<p>Photohelic&reg; Switch/Gages function as versatile, highly repeatable pressure switches combined with a 
			precise pressure gage employing the time-proven Magnehelic&reg; gage design. The Photohelic&reg; switch/gage 
			measures and controls positive, negative or differential pressures of air and compatible gases. Standard models 
			are rated to 25 psig (1.7 bar) with options to 35 (2.4) or 80 (5.5 bar) psig. Single pressure 36000S models 
			measure to 6000 psig (413 bar) with a 9000 psig (620 bar) rating. Two phototransistor actuated, DPDT relays 
			are included for low/high limit control. Easy to adjust set point indicators are controlled by knobs located 
			on the gage face. Individual set point deadband is one pointer width - less than 1% of full scale. Set points 
			can be interlocked to provide variable deadband - ideal for control of fans, dampers, etc. Gage reading is 
			continuous and unaffected by switch operation, even during loss of electrical power. Choose from full scale 
			pressure ranges from a low 0-.25" (0-6 mm) w.c. up to 30 psi (21 bar); single positive pressure to 6000 psig 
			(413 bar).
			<br/><br/>Photohelic Sensing - How It WorksIn typical applications, these Dwyer switch/gages control between 
			high and low pressure set points. When pressure changes, reaching either set point pressure, the infrared 
			light to the limiting phototransistor is cut off by the helix-driven light shutter. The resulting phototransistor 
			signal is electronically amplified to actuate its DPDT slave relay and switching occurs. Dead band between make 
			and break is 1% of full scale or less - just enough to assure positive, chatter-free operation.
			<br/><br/>Relay-Transformer Features - A plastic housing protects all electronic components. Solid-state 
			and integrated circuit electronics are on glass-epoxy printed circuit boards and self-extinguishing terminal boards.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/A3000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - A3000 Photohelic&reg;</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/3000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - 3000 Photohelic&reg;</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/A3000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/A3000_intl_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="11" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_MP_Mini_Photohelic_Differential_Pressure_Switch_Gage.jpg" alt="Dwyer Series MP Mini-Photohelic Differential Pressure Switch/Gage" title="Dwyer Series MP Mini-Photohelic Differential Pressure Switch/Gage"/></div>
			<div id="PartsContent"><h3>Series MP 
			<br/><font color="#445678">Mini-Photohelic Differential Pressure Switch/Gage</font></h3>
			<p>The Series MP Mini-Photohelic&reg; differential pressure switch/gage combines the time proven 
			Minihelic&reg; II differential pressure gage with two SPDT switching set points. The Mini-Photohelic&reg; 
			switch/gage is designed to measure and control positive, negative, or differential pressures consisting 
			of non-combustible and non-corrosive gases. Gage reading is independent of switch operation. Switching 
			status is visible by LED indicators located on the front and rear of the gage. Set points are adjusted 
			with push buttons on back of unit. This extremely compact switch/gage is ideal for fume hoods, dust 
			collection, pneumatic conveying and clean room applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/MP_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/MP_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/MP_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="12" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_DH_Digihelic_Differential_Pressure_Controller.jpg" alt="Dwyer Series DH Digihelic Differential Pressure Controller" title="Dwyer Series DH Digihelic Differential Pressure Controller"/></div>
			<div id="PartsContent"><h3>Series DH 
			<br/><font color="#445678">Digihelic&reg; Differential Pressure Controller</font></h3>
			<p>The Series DH Digihelic&reg; Differential Pressure Controller is a 3 in 1 instrument possessing a 
			digital display gage, control relay switches, and a transmitter with current output. Combining 
			these 3 features allows the reduction of several instruments with one product, saving inventory, 
			installation time and money. The Digihelic&reg; controller is the ideal instrument for pressure, 
			velocity and flow applications, achieving a 0.5% full scale accuracy on ranges from 0.25 to 100 in. w.c.
			<br/><br/>The Digihelic&reg; controller allows the selection of pressure, velocity or volumetric flow 
			operation in several commonly used engineering units. Two SPDT control relays with adjustable dead 
			bands are provided along with a scalable 4-20 mA process output. In velocity of flow modes, a square 
			root output is provided on the 4-20 mA signal to coincide with the actual flow curve. The Series 
			DH provides extreme flexibility in power usage by allowing 120/220 VAC and also 24 VDC power which 
			is often used in control panels.
			<br/><br/>Programming is easy using the menu key to access 5 simplified menus which provide access 
			to: security level; selection of pressure, velocity or flow operation; selection of engineering 
			units; K-factor for use with flow sensors; rectangular or circular duct for inputting area in flow 
			applications; set point control or set point and alarm operation; alarm operation as a high, low or 
			high/low alarm; automatic or manual alarm reset; alarm delay; view peak and valley process readings; 
			digital damping for smoothing erratic process applications; scaling the 4-20 mA process output to 
			fit your application's range; Modbus&reg; communications; and field calibration.
			<br/><br/>With all this packed into one product it is easy to see why the Digihelic&reg; controller 
			is the only instrument you will need for all your pressure applications. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/DH_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - DH Digihelic&reg;</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DH-QS_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - DH Quick Set-up</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/DH_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DH_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="13" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_DHII_Digihelic_II_Differential_Pressure_Controller.jpg" alt="Dwyer Series DHII Digihelic II Differential Pressure Controller" title="Dwyer Series DHII Digihelic II Differential Pressure Controller"/></div>
			<div id="PartsContent"><h3>Series DHII 
			<br/><font color="#445678">Digihelic&reg; II Differential Pressure Controller</font></h3>
			<p>The Digihelic&reg; Controller just got better with the New Series DHII Differential Pressure 
			Controller. The DHII takes all the fabulous features of the standard Digihelic&reg; Pressure 
			Controller and packages them in a robust NEMA 4 (IP66) housing.
			<br/><br/>The Digihelic&reg; II Pressure Controller combines the 2 SPDT control relays, 4-20 
			mA process output and Modbus&reg; communications with a large, brightly backlit 4 digit LCD 
			display that can easily be seen from long distances. The electrical wiring has also been 
			enhanced in the DHII with its detachable terminal blocks. The removable terminals allow 
			the install to easily wire the terminal block outside the housing and then attach to the 
			circuit board, reducing wiring difficulties and installation time on the process.
			<br/><br/>The Digihelic&reg; II Differential Pressure Control in the new NEMA 4 (IP66) 
			enclosure enables this product to be the perfect choice when mounting pressure controls 
			outdoors in such applications as rooftop air handlers. This housing also makes it the 
			ideal solution for surface mounting in clean rooms or facilities where water or a 
			cleaning solution is utilized in maintaining plant cleanliness.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/DHII_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/DHII_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DHII_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="14" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_DH3_Digihelic_Differential_Pressure_Controller.jpg" alt="Dwyer Series DH3 Digihelic Differential Pressure Controller" title="Dwyer Series DH3 Digihelic Differential Pressure Controller"/></div>
			<div id="PartsContent"><h3>Series DH3 
			<br/><font color="#445678">Digihelic Differential&reg; Pressure Controller</font></h3>
			<p>The Series DH3 Digihelic&reg; Differential Pressure Controller is a 3 in 1 instrument possessing a digital 
			display gage, control relay switches, and a transmitter with current output all packed in the popular 
			Photohelic&reg; gage style housing. Combining these 3 features allows the reduction of several instruments 
			with one product, saving inventory, installation time and money. The Digihelic&reg; controller is the ideal 
			instrument for pressure, velocity and flow applications, achieving a 1% full scale accuracy on 
			ranges down to the extremely low 0.25" w.c. to 2.5" w.c. full scale. Ranges of 5" w.c. and greater 
			maintain 0.5% F.S. accuracy. Bi-directional ranges are also available.
			<br/><br/>The Series DH3 Digihelic&reg; 
			controller allows the selection of pressure, velocity or volumetric flow operation in several commonly 
			used engineering units. 2 SPDT control relays with adjustable deadbands are provided along with a 
			scalable 4-20 mA process output.
			<br/><br/>Programming is easy using the menu key to access 5 simplified 
			menus which provide access to: security level; selection of pressure, velocity or flow operation; 
			selection of engineering units; K-factor for use with flow sensors; rectangular or circular duct 
			for inputting area in flow applications; set point control or set point and alarm operation; 
			alarm operation as a high, low or high/low alarm; automatic or manual alarm reset; alarm delay; 
			view peak and valley process reading; digital damping for smoothing erratic process applications; 
			scaling the 4-20 mA process output to fit your applications range and field calibration.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/DH3_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/DH3_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DH3_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="15" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1620_Single_And_Dual_Pressure_Switch.jpg" alt="Dwyer Series 1620 Single and Dual Pressure Switch" title="Dwyer Series 1620 Single and Dual Pressure Switchs"/></div>
			<div id="PartsContent"><h3>Series 1620 
			<br/><font color="#445678">Single and Dual Pressure Switch</font></h3>
			<p>	
			Our old faithful switch design is still best where highest precision combined with diaphragm sealed leak proof construction and mounting 
			simplicity are required. Model 1626 and 1627 differential pressure switches are identical in design and construction except that Model 1626 has a 
			single electric switch and Model 1627 has dual electric switches. Model 1627 can therefore provide dual control when required. It 
			can be set to open or close two independent electrical circuits, each preset for its own actuation pressure. Both units have diaphragm 
			sealed motion take outs providing maximum protection against leakage.
			<br/><br/><b>CAUTION:</b> For use only with air or compatible gases.   
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1620_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1620_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1620_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="16" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1630_Large_Diaphragm_Differential_Pressure_Switch.jpg" alt="Dwyer Series 1630 Large Diaphragm Differential Pressure Switch" title="Dwyer Series 1630 Large Diaphragm Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series 1630 
			<br/><font color="#445678">Large Diaphragm Differential Pressure Switch</font></h3>
			<p>Our highest precision conventional large diaphragm pressure switch provides maximum dependability. In addition, it incorporates a 
			visible set point indicator for maximum convenience. UL and CSA listed, FM approved for general service, these switches are suitable 
			for most applications in air conditioning and industrial service. Electrical capability of 15 amps handles most small electrical loads.
			<br/><br/><b>CAUTION:</b> For use only with air or compatible gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1630_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1630&1640_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1630_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="17" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1640_Floating_contact_Null_Switch_for_High_and_Low_Actuation.jpg" alt="Dwyer Series 1640 Floating contact Null Switch for High and Low Actuation" title="Dwyer Series 1640 Floating contact Null Switch for High and Low Actuation"/></div>
			<div id="PartsContent"><h3>Series 1640 
			<br/><font color="#445678">Floating contact Null Switch for High and Low Actuation</font></h3>
			<p>The unique electric switch design in the 1640 is another Dwyer Instrument, Inc. innovation. The Dwyer&reg; Model 1640 Differential Pressure 
			Switch resembles the Series 1630 switches. The Model 1640, however, is equipped with a single pole, double throw floating contact switch 
			(not snap acting) so it functions as a null switch.
			<br/><br/>Drawing shows the switching action schematically. As the diaphragm moves in response 
			to pressure changes, it moves the floating contact to cause switching action at two preset points with no switching action between these points. 
			The "high" circuit will be closed when rising pressure differential reaches the preset level. The "low" circuit will be closed when falling 
			pressure differential reaches the preset level.
			<br/><br/>A typical example of usage is to position motorized dampers when static pressure in a duct 
			system reaches a desired maximum and reposition the dampers when the static pressure falls to a pre-established minimum. By using a Pitot tube 
			sensing element the Model 1640 switch can serve in the same way to control air velocity and maintain a constant volume of air in a supply duct.
			<br/><br/><b>CAUTION:</b> For use only with air or compatible gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1640_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1630&1640_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1640_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="18" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1700_Low_Differential_Pressure_Switch_Designed_For_OEM_Products.jpg" alt="Dwyer Series 1700 Low Differential Pressure Switch Designed for OEM Products" title="Dwyer Series 1700 Low Differential Pressure Switch Designed for OEM Products"/></div>
			<div id="PartsContent"><h3>Series 1700 
			<br/><font color="#445678">Low Differential Pressure Switch Designed for OEM Products</font></h3>
			<p>Series 1700 Differential Pressure Switches were specifically designed to satisfy the demands of high volume original equipment manufacturers 
			for a tough, dependable limit switch at a very low price. The Model 1710 uses Dwyer's simple, time-proven method of mechanically coupling a 
			sensitive 3" diameter silicone rubber diaphragm to a versatile SPDT snap switch. The switch housing is precision molded of a high strength glass 
			filled PPO which has excellent dimensional and thermal stability. This unique combination of existing and new technologies enables Dwyer to offer 
			a limit switch priced less than comparable units without sacrificing performance. Installation is fast and simple with twist and tighten mounting 
			screw slots, 1/4" quick connect electrical terminals and dual size barbed pressure ports for 1/8" and 3/16" ID vinyl or rubber tubing.
			<br/><br/>For use with air and other non-combustible, non-corrosive gases, switches are UL and CSA recognized. Two models are offered with field 
			adjustable ranges from .15 to .55" w.c. and .50 to 7.5" w.c. Units are rated for 2 psig maximum system pressure, temperature limit is 190&deg;F 
			and weight is only 4 oz.
			<br/><br/><b>NOTE:</b> Minimum quantity order is 50 pieces. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/images/1700_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1700_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="19" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1800_Low_Differential_Pressure_Switch_for_General_Industrial_Serivce.jpg" alt="Dwyer Series 1800 Low Differential Pressure Switch for General Industrial Service" title="Dwyer Series 1800 Low Differential Pressure Switch for General Industrial Service"/></div>
			<div id="PartsContent"><h3>Series 1800 
			<br/><font color="#445678">Low Differential Pressure Switch for General Industrial Service</font></h3>
			<p>Essential for industrial environments, the Series 1800 combines small size and low price with 2% repeatability for enough accuracy for all but 
			the most demanding applications. Set point adjustment inside the mounting stud permits mounting switch on one side of a wall or panel with 
			adjustment easily accessible on the opposite side. UL and CSA listed, and FM approved.
			<br/><br/><b>CAUTION:</b> For use only with air or compatible gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1800_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1800_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1800_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="20" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1900_Compact_Low_Differential_Pressure_Switch.jpg" alt="Dwyer Series 1900 Compact Low Differential Pressure Switch" title="Dwyer Series 1900 Compact Low Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series 1900 
			<br/><font color="#445678">Compact Low Differential Pressure Switch</font></h3>
			<p>Our most popular series combines advanced design and precision construction to make these switches able to perform many of the tasks of larger, 
			costlier units. Designed for air conditioning service, they also serve many fluidics, refrigeration, oven and dryer applications. For air and 
			non-combustible compatible gases, Series 1900 switches have set points from 0.07 to 20" w.c. (1.8 to 508 mm). Set point adjustment is easy 
			with range screw located inside conduit enclosure. Internal location helps prevent tampering. UL, CE and CSA listed, and FM approved.
			<br/><br/><b>CAUTION:</b> For use only with air or compatible gases.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1900_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1900_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/1900_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="21" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1950G_Explosion_proof_Differential_Pressure_Switch.jpg" alt="Dwyer Series 1950G Explosion-proof Differential Pressure Switch" title="Dwyer Series 1950G Explosion-proof Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series 1950G 
			<br/><font color="#445678">Explosion-proof Differential Pressure Switch</font></h3>
			<p>The Model 1950G Explosion-Proof Switch combines the best features of the popular Dwyer&reg; Series 1950 Pressure Switch with the 
			benefit of natural gas compatibility. Units are rain-tight for outdoor installations, and are UL listed for use in Class I, 
			Groups A, B, C, & D; Class II, Groups E, F, & G and Class III atmospheres, Directive 94/9/EC (ATEX) Compliant for CE ATEX II 2G 
			Exd IIB+H2 T6, CSA & FM approved for Class I, Div. 1, Groups B, C, D; Class II, Div. 1, Groups E, F, G and Class III atmospheres 
			IECEx Ex d IIB+H2 T6 (-40&deg;C < Ta < +60&deg;C). The 1950G is very compact, about half the weight and bulk of equivalent conventional 
			explosion-proof switches.
			<br/><br/>Easy access to the SPDT relay and power supply terminals is provided by removing the top plate of the 
			aluminum housing. A supply voltage of 24 VDC, 120 or 240 VAC is required. A captive screw allows the cover to swing aside while 
			remaining attached to the unit. Adjustment to the set point of the switch can be made without disassembly of the housing.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1950g_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1950G_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_37i_low1.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="22" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_1996_Gas_Pressure_Switch.jpg" alt="Dwyer 1996 Gas Pressure Switch" title="Dwyer 1996 Gas Pressure Switch"/></div>
			<div id="PartsContent"><h3>1996 Gas Pressure Switch</h3>
			<p>Reliable and convenient, the 1996-5 and 1996-20 Gas Pressure Switches serve as a compact, low cost switch for gas fired 
			furnaces and equipment. Pressure ranges for both models are ideal for high or low gas pressure interlock. Visible set point 
			and on-off indicators add convenience in servicing. Use either NO or NC contacts on SPDT switch. Bottom connection has both 1/8" 
			female and 1/4" male threads for pipe nipple or coupling. Top connection vents diaphragm chamber to outside or to furnace 
			combustion chamber. Mount switch with diaphragm in a horizontal position and gas pressure connection at bottom. Used with natural, 
			manufactured or LP gas. Also see Model 1950G. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/1996_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/1996_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_36i.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="23" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_ADPS_HAVC_Differential_Pressure_Switch.jpg" alt="Dwyer Series ADPS HAVC Differential Pressure Switch" title="Dwyer Series ADPS HAVC Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series ADPS 
			<br/><font color="#445678">HAVC Differential Pressure Switch</font></h3>
			<p>The Series ADPS Adjustable Differential Pressure Switch is designed for pressure, vacuum, and differential 
			pressures. The dual scaled adjustment knob in inches water column and Pascals allows changes to the switching 
			pressure to be made without a pressure gage. The ADPS is available with settings from 0.08" w.c. (20 Pa) up to 
			16" w.c. (4000 Pa). The silicone diaphragm and PA 6.6 body make the series ADPS ideal for use with air and other 
			noncombustible gases. The compact size, adjustment knob and low cost make the ADPS the perfect choice for HVAC applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/ADPS_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/ADPS_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/ADPS_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="24" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_BYDS_Bypass_Damper_Switch.jpg" alt="Dwyer Series BYDS Bypass Damper Switch" title="Dwyer Series BYDS Bypass Damper Switch"/></div>
			<div id="PartsContent"><h3>Series BYDS 
			<br/><font color="#445678">Bypass Damper Switch</font></h3>
			<p>The Series BYDS Bypass Damper Switch is designed to control motorized bypass dampers. As individual zone dampers 
			open and close, the system static pressure will rise and fall. In order to maintain proper airflow and static pressure 
			through the HVAC system, a bypass system incorporating a floating type motorized damper and a static pressure control 
			should be incorporated. A typical on/off pressure switch cannot operate in this application due to the high cycle rate 
			that would result, eventually causing contact burnout. The BYDS static pressure control is equipped with a solid-state 
			switching and timing circuit to enhance operation and improve long-term reliability in this demanding application.
			<br/><br/><b>Control Operation</b>
			<br/><br/>The BYDS Bypass Damper Switch is designed for use with a three wire floating point damper actuator used to 
			control the static pressure on zone control systems requiring motorized bypass dampers. A bypass damper, bypasses 
			air from the discharge air side of the HVAC unit back in the return air side. The air flow is modulated in order 
			to maintain a constant static pressure in the system as individual zone dampers open and close. The static pressure 
			control is equipped with solid-state switching which dramatically increases the life of the control. On an increase 
			in static pressure the BYDS will send 24 volts out and start the actuator to drive open. When the static pressure 
			reaches set-point the actuator will stop. After a ten second delay the actuator will start to drive the damper 
			closed if there has not been an increase in static pressure. A green LED indicates when the damper is in the open 
			position or being driven open. A 24 VAC transformer is required to power the BYDS control and damper actuator. 
			The unit includes tubing and two static pressure probes.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/BYDS_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/BYDS_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/BYDS_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="25" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_H3_Explosion_Proof_Differential_Pressure_Switch.jpg" alt="Dwyer Series H3 Explosion-Proof Differential Pressure Switch" title="Dwyer Series H3 Explosion-Proof Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series H3  
			<br/><font color="#445678">Explosion-Proof Differential Pressure Switch</font></h3>
			<p>The Series DH3 Digihelic&reg; Differential Pressure Controller is a 3 in 1 instrument 
			possessing a digital display gage, control relay switches, and a transmitter with current 
			output all packed in the popular Photohelic&reg; gage style housing. Combining these 3 
			features allows the reduction of several instruments with one product, saving inventory, 
			installation time and money. The Digihelic&reg; controller is the ideal instrument for pressure, 
			velocity and flow applications, achieving a 1% full scale accuracy on ranges down to the extremely 
			low 0.25" w.c. to 2.5" w.c. full scale. Ranges of 5" w.c. and greater maintain 0.5% F.S. accuracy. 
			Bi-directional ranges are also available.
			<br/><br/>The Series DH3 Digihelic&reg; controller allows the selection of pressure, velocity or 
			volumetric flow operation in several commonly used engineering units. 2 SPDT control relays with 
			adjustable deadbands are provided along with a scalable 4-20 mA process output.
			<br/><br/>Programming is easy using the menu key to access 5 simplified menus which provide 
			access to: security level; selection of pressure, velocity or flow operation; selection 
			of engineering units; K-factor for use with flow sensors; rectangular or circular duct 
			for inputting area in flow applications; set point control or set point and alarm operation; 
			alarm operation as a high, low or high/low alarm; automatic or manual alarm reset; alarm delay; 
			view peak and valley process reading; digital damping for smoothing erratic process applications; 
			scaling the 4-20 mA process output to fit your applications range and field calibration.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/H3_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/H3_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_38i.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="26" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_3100_Explosion_proof_Differntial_Pressure_Transmitter.jpg" alt="Dwyer Series 3100D Explosion-proof Differential Pressure Transmitter" title="Dwyer Series 3100D Explosion-proof Differential Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series 3100D 
			<br/><font color="#445678">Explosion-proof Differential Pressure Transmitter</font></h3>
			<p>The Mercoid&reg; Series 3100D Smart Pressure Transmitter is a microprocessor-based high performance transmitter, 
			which has flexible pressure calibration, push button configuration, and programmable using HART&reg; Communication. 
			The Series 3100 is capable of being configured for differential pressure or level applications with the zero and 
			span buttons. A field calibrator is not required for configuration. The transmitter software compensates for 
			thermal effects, improving performance. EEPROM stores configuration settings and stores sensor correction 
			coefficients in the event of shutdowns or power loss. The Series 3100 is FM approved for use in hazardous 
			(classified) locations. The 100:1 rangeability allows the smart transmitter to be configured to fit any application.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/P_3100.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/3100_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/3100D_intl.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="27" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_603A_Differential_Pressure_Transmitter.jpg" alt="Series 603A Differential Pressure Indicating Transmitter" title="Series 603A Differential Pressure Indicating Transmitter"/></div>
			<div id="PartsContent"><h3>Series 603A
			<br/><font color="#445678">Differential Pressure Indicating Transmitter</font></h3>
			<p>The Series 603A Differential Pressure Transmitters combine ultra low ranges, low cost, high accuracy and rugged 
			construction - ideal for a wide range of HVAC and industrial applications. These transmitters provide a standard 4-20 mA analog 
			output signal in ranges as low as 0.2" w.c.
			<br/><br/>The transmitter can be surface mounted or flush mounted in a 4-13/16" (122 mm) diameter 
			panel hole. Hardware is included for either option. Duplicate 1/8" female NPT pressure connections on side and back ease installation. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/603a_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/603A_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/603A_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="28" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_604D_Minihelic_Differential_Pressure_Indicating_Transmitter.jpg" alt="Dwyer Series 604D Minihelic Differential Pressure Indicating Transmitter" title="Dwyer Series 604D Minihelic Differential Pressure Indicating Transmitter"/></div>
			<div id="PartsContent"><h3>Series 604D 
			<br/><font color="#445678">Minihelic&reg; Differential Pressure Indicating Transmitter</font></h3>
			<p>The Series 604D Minihelic&reg; differential pressure Indicating Transmitter combines visual monitoring with electronic control 
			of low differential air or compatible gas pressures. This versatile device is ideal for building HVAC systems where local 
			indication is needed during maintenance checks or when troubleshooting the system. The transmitter design employs the latest 
			strain gage technology and operates in 2-wire control loop circuits. Separate Zero and Span controls plus a 4-screw terminal 
			strip are protected in a gasketed side enclosure. Cable gland fits .10-.25" round cable. A 10-35 VDC power supply is required. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/604d_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/604D_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/604D_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="29" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_605_Magnehelic_Differential_Pressure_Indicating_Transmitter.jpg" alt="Dwyer Series 605 Magnehilc Differential Pressure Indicating Transmitter" title="Dwyer Series 605 Magnehilc Differential Pressure Indicating Transmitter"/></div>
			<div id="PartsContent"><h3>Series 605 
			<br/><font color="#445678">Magnehilc&reg; Differential Pressure Indicating Transmitter</font></h3>
			<p>The Series 605 Magnehelic&reg; Indicating Transmitter provides for both visual monitoring and electronic control of very 
			low differential pressure. The Series 605 is ideal for control applications in building HVAC systems where local indication is 
			desired during routine maintenance checks or necessary when trouble shooting the system. The easily read dial gage is complimented 
			by the two-wire, 4-20 mA control signal utilizing the time-proven Dwyer&reg; Magnehelic&reg; gage mechanical design and Series 600 
			transmitter technology. The two-wire design with terminal strip on the rear simplifies connection in any 4-20 mA control loop powered 
			by a 10-35 VDC supply.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/605_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/605_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_46i_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="30" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_631B_Wet_Wet_Differential_Pressure_Transmitter.jpg" alt="Dwyer Series 631B Wet/Wet Differential Pressure Transmitter" title="Dwyer Series 631B Wet/Wet Differential Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series 631B 
			<br/><font color="#445678">Wet/Wet Differential Pressure Transmitter</font></h3>
			<p>The Dwyer Series 631B Differential Pressure Transmitter monitors differential pressure of air and compatible gases and liquids 
			with accuracy. The design employs converting pressure changes into a standard 4-20 mA output signal for two wire circuits. Digital 
			push-button, zero and span adjustments are easily accessed on the front cover. The Series 631 Differential Pressure Transmitter is 
			designed to meet NEMA 4X (IP66) construction. Robust housing offers 500 psi static pressure rating on ranges down to 0.5" w.c. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/605_IOM.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/605_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_46i_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="31" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_636D_Fixed_Range_Differential_Pressure_Transmitter.jpg" alt="Dwyer Series 636D Fixed Range Differential Pressure Transmitter" title="Dwyer Series 636D Fixed Range Differential Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series 636D 
			<br/><font color="#445678">Fixed Range Differential Pressure Transmitter</font></h3>
			<p>The Series 636D Differential Pressure Transmitter can be used for measuring pressures of liquids, gases & vapors. All available 
			ranges have an excellent 0.5% F.S. accuracy with a 4-20 mA Output standard or optional 1-5 VDC output. The NEMA 4 housing is an all 
			316 welded construction that is designed to withstand the harshest environmental conditions. With all 316L wetted materials, 
			this transmitter is compatible with most media. These units are CSA approved explosion-proof for use in the specified hazardous 
			locations and meet NACE standards for off-shore applications. 
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/636D_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/636D_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/636D_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="32" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_655A_316_Wet_Wet_Differential_Pressure_Transmitter.jpg" alt="Dwyer Series 655A 316 Wet/Wet Differential Pressure Switch" title="Dwyer Series 655A 316 Wet/Wet Differential Pressure Switch"/></div>
			<div id="PartsContent"><h3>Series 655A 
			<br/><font color="#445678">316 Wet/Wet Differential Pressure Switch</font></h3>
			<p>Series 655A Differential Pressure Transmitters are designed for high static/low DP applications designed especially for the 
			End Users and OEM's where extreme overpressure and high performance of 0.25% accuracy and stability are required at ranges 
			down to 3" w.c.
			<br/><br/>Every 655A Series is built using advanced manufacturing techniques to ensure symmetry between the HI and 
			LO ports. Symmetry, along with automated welding procedures give these differential pressure transmitters excellent performance 
			and reliability over time at high elevated line pressure. Each unit includes a 6-point N.I.S.T certificate of calibration which 
			demonstrates the unit's high level of performance.
			<br/><br/>The 655A Series transmitter pressure ports that are all welded 316 SS with 
			an O-Ring free design making it ideal for high cyclical applications, capillary filled systems, or applications where a guaranteed 
			leak free system is needed.  
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/655A_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/655A_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/655A_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="33" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_DM_2000_Differential_Pressure_Transmitter.jpg" alt="Dwyer Series DM-2000 Differential Pressure Transmitter" title="Dwyer Series DM-2000 Differential Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series DM-2000 
			<br/><font color="#445678">Differential Pressure Transmitter</font></h3>
			<p>The Dwyer Series DM-2000 Differential Pressure Transmitter senses the pressure of air and 
			compatible gases and sends a standard 4-20 mA output signal. The DM-2000 housing is specifically 
			designed to mount in the same diameter cutout as a standard Magnehelic&reg; gage. 
			A wide range of models are available factory calibrated to specific ranges.
			<br/><br/>Pressure connections are inherent to the glass filled plastic molded housing 
			making installation quick and easy. Digital push-button zero and span simplify calibration 
			over typical turn-potentiometers. An optional 3.5 digit LCD shows process and engineering 
			units. A single bush button allows field selection of 4 to 6 engineering units depending on range on LCD models.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/DM-2000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/DM-2000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/DM-2000_cat.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="34" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_3200_Explosion_proof_Pressure_Transmitter.jpg" alt="Dwyer Series 3200G Explosion proof Pressure Transmitter" title="Dwyer Series 3200G Explosion proof Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series 3200G 
			<br/><font color="#445678">Explosion proof Pressure Transmitter</font></h3>
			<p>The Mercoid&reg; Series 3200G Smart Pressure Transmitter is a microprocessor-based high performance 
			transmitter, which has flexible pressure calibration, push button configuration, and programmable using HART&reg; 
			Communication. The Series 3200G is capable of being configured with the zero and span buttons, a field calibrator 
			is not required for configuration. The transmitter software compensates for thermal effects, improving performance. 
			EEPROM stores configuration settings and stores sensor correction coefficients in the event of shutdowns or power loss. 
			The Series 3200G is FM approved for use in hazardous (Classified) locations. The 100:1 rangeability allows the smart 
			transmitter to be configured to fit any application.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/P_3200.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/3200_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/3200G_intl.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div id="35" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_MS_Magnesense_Differential_Pressure_Transmitter.jpg" alt="Dwyer Series MS Magnesense Differential Pressure Transmitter" title="Dwyer Series MS Magnesense Differential Pressure Transmitter"/></div>
			<div id="PartsContent"><h3>Series MS 
			<br/><font color="#445678">Magnesense&reg; Differential Pressure Transmitter</font></h3>
			<p>The Series MS Magnesense&reg; Differential Pressure Transmitter is an extremely versatile transmitter 
			for monitoring pressure and air velocity. This compact package is loaded with features such as: field 
			selectable English or metric ranges, field upgradeable LCD display, adjustable dampening of output 
			signal (with optional display) and the ability to select a square root output for use with Pitot tubes 
			and other similar flow sensors.
			<br/><br/>Along with these features, the magnetic sensing technology provides exceptional long term 
			performance and enables the Magnesense&reg; transmitter to be the solution for a myriad of pressure and flow applications.
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/A_26P_low.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - ranges 10" and up</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/A_26_low.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual - ranges lower than 10"</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/MS-Duct_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings - Duct Mount</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/MS-Wall_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings - Wall Mount</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/MS-DIN-Bracket_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings - DIN Bracket</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/040i.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>

<div id="1" class="popup_block_Parts">
			<div id="fullsize"><img border="0" src="Parts_by_Man_OK_By_Jon/Dwyer/Series_2_5000_Minihelic_II_Differential_Pressure_Gage.jpg" alt="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage" title="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage"/></div>
			<div id="PartsContent"><h3>Series 2-5000 
			<br/><font color="#445678">Minihelic&reg; II Differential Pressure Gage</font></h3>
			<p>Combining clean design, small size and low cost with enough accuracy for all 
			but the most demanding applications our Minihelic&reg; II gage offers the latest 
			in design features for a dial type differential pressure gage. It is our most 
			compact gage but is easy to read and can safely operate at total pressures up 
			to 30 psig. The Minihelic&reg; II gage is designed for panel mounting in a 
			single 2-5/8" diameter hole. Standard pressure connections are barbed fittings 
			for 3/16" I.D. tubing; optional 1/8" male NPT connections are also available. 
			Over-pressure protection is built into the Minihelic&reg; II gage by means of 
			a blow-out membrane molded in conjunction with the diaphragm. Accidental 
			over-ranging up to the rated total pressure will not damage the gage. 
			With removable lens and rear housing, the gage may be easily serviced at 
			minimum cost.
			<br/><br/>With the housing molded from mineral and glass filled nylon and 
			the lens molded from acrylic, the gage will withstand rough use and exposure 
			as well as high total pressure. The 5% accuracy and low cost of the Minihelic&reg; II 
			gage make it well-suited for a wide variety of OEM and user applications. OEM applications 
			include cabinet air purging, medical respiratory therapy equipment, air samplers, laminar 
			flow hoods, and electronic air cooling systems. As an air filter gage, the Minihelic&reg; II 
			gage finds many end use applications on large stationary engines, compressors, ventilators, and air 
			handling units. The Minihelic&reg; II gage is suitable for many of the same applications as the 
			Magnehelic&reg; gage where the greater accuracy, sensitivity, and higher and lower differential 
			pressure ranges of the Magnehelic&reg; gage are not required.	
			<br/><br/><br/>
			<b><font color="#494A4A">Printable Literature</font></b>
			<br/><br/><a href="http://www.dwyer-inst.com/PDF_files/2-5000_iom.pdf" target="_blank"><font color="#ACB0C3"><b>Service Manual</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/images/2-5000_DD.pdf" target="_blank"><font color="#ACB0C3"><b>Dimensional Drawings</b></font></a>
			<br/><a href="http://www.dwyer-inst.com/PDF_files/Dwyer_8i_low.pdf" target="_blank"><font color="#ACB0C3"><b>Catalog Pages</b></font></a>
			</p>
			</div></div>
<div class="sliderGallery">
		<ul>
                <li><a href="#?w=400" rel="1" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_2_5000_Minihelic_II_Differential_Pressure_Gage_Thumbnail.gif" border="0" alt="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage" title="Dwyer Series 2-5000 Minihelic II Differential Pressure Gage" /></a></li>
		<li><a href="#?w=400" rel="2" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_2000_Magnehelic_Differential_Pressure_Gage_Thumbnail.gif" border="0" alt="Dwyer Series 2000 Magnehelic Differential Pressure Gage" title="Dwyer Series 2000 Magnehelic Differential Pressure Gage" /></a></li>
		<li><a href="#?w=400" rel="3" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_2000_SP_Magnehelic_Differential_Pressure_Gages_Thumbnail.gif" border="0" alt="Dwyer Series 2000-SP Magnehelic Differential Pressure Gage" title="Dwyer Series 2000-SP Magnehelic Differential Pressure Gage" /></a></li>
		<li><a href="#?w=400" rel="4" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_4000_Capsuhelic_Differential_Pressure_Gage_Thumbnail.gif" border="0" alt="Dwyer Series 4000 Capsuhelic Differential Pressure Gage" title="Dwyer Series 4000 Capsuhelic Differential Pressure Gage" /></a></li>
		<li><a href="#?w=400" rel="5" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_DM_1000_DigiMag_Digital_Differential_Pressure_and_Flow_Gages_Thumbnail.gif" border="0" alt="Dwyer Series DM-1000 DigiMag Digital Differential Pressure and Flow Gages" title="Dwyer Series DM-1000 DigiMag Digital Differential Pressure and Flow Gages" /></a></li>
		<li><a href="#?w=400" rel="6" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_PTGB_Differential_Pressure_Piston_Type_Gage_Thumbnail.gif" border="0" alt="Dwyer Series PTGB Differential Pressure Piston-Type Gage" title="Dwyer Series PTGB Differential Pressure Piston-Type Gage"/></a>
		<li><a href="#?w=400" rel="7" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_3000MR_3000MRS_Thumbnail.gif" border="0" alt="Dwyer Series 3000MR/3000MRS" title="Dwyer Series 3000MR/3000MRS" /></a></li>
		<li><a href="#?w=400" rel="8" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_3000SGT_Photohelic_Pressure_Switch_Gage_with_Integral_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 3000SGT Photohelic Pressure Switch Gage with Integral Transmitter" title="Dwyer Series 3000SGT Photohelic Pressure Switch Gage with Integral Transmitter" /></a></li>
		<li><a href="#?w=400" rel="9" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_43000_Capsu_Photohelic_Pressure_Switch_Gage_Thumbnail.gif" border="0" alt="Dwyer Series 43000 Capsu-Photohelic Pressure Switch/Gage" title="Dwyer Series 43000 Capsu-Photohelic Pressure Switch/Gage" /></a></li>
		<li><a href="#?w=400" rel="10" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_A3000_Photohelic_Pressure_Switch_Gage_Thumbnail.gif" border="0" alt="Dwyer Series A3000 Photohelic Pressure Switch/Gage" title="Dwyer Series A3000 Photohelic Pressure Switch/Gage" /></a></li>
		<li><a href="#?w=400" rel="11" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_MP_Mini_Photohelic_Differential_Pressure_Switch_Gage_Thumbnail.gif" border="0" alt="Dwyer Series MP Mini-Photohelic Differential Pressure Switch/Gage" title="Dwyer Series MP Mini-Photohelic Differential Pressure Switch/Gage" /></a></li>
		<li><a href="#?w=400" rel="12" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_DH_Digihelic_Differential_Pressure_Controller_Thumbnail.gif" border="0" alt="Dwyer Series DH Digihelic Differential Pressure Controller" title="Dwyer Series DH Digihelic Differential Pressure Controller"/></a></li>
		<li><a href="#?w=400" rel="13" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_DHII_Digihelic_II_Differential_Pressure_Controller_Thumbnail.gif" border="0" alt="Dwyer Series DHII Digihelic II Differential Pressure Controller" title="Dwyer Series DHII Digihelic II Differential Pressure Controller" /></a></li>
		<li><a href="#?w=400" rel="14" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_DH3_Digihelic_Differential_Pressure_Controller_Thumbnail.gif" border="0" alt="Dwyer Series DH3 Digihelic Differential Pressure Controller" title="Dwyer Series DH3 Digihelic Differential Pressure Controller" /></a></li>
		<li><a href="#?w=400" rel="15" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1620_Single_And_Dual_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series 1620 Single and Dual Pressure Switch" title="Dwyer Series 1620 Single and Dual Pressure Switch"/></a></li>
		<li><a href="#?w=400" rel="16" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1630_Large_Diaphragm_Differential_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series 1630 Large Diaphragm Differential Pressure Switch" title="Dwyer Series 1630 Large Diaphragm Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="17" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1640_Floating_contact_Null_Switch_for_High_and_Low_Actuation_Thumbnail.gif" border="0" alt="Dwyer Series 1640 Floating contact Null Switch for High and Low Actuation" title="Dwyer Series 1640 Floating contact Null Switch for High and Low Actuation"/></a></li>
		<li><a href="#?w=400" rel="18" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1700_Low_Differential_Pressure_Switch_Designed_For_OEM_Products_Thumbnail.gif" border="0" alt="Dwyer Series 1700 Low Differential Pressure Switch Designed for OEM Products" title="Dwyer Series 1700 Low Differential Pressure Switch Designed for OEM Products" /></a></li>
		<li><a href="#?w=400" rel="19" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1800_Low_Differential_Pressure_Switch_for_General_Industrial_Serivce_Thumbnail.gif" border="0" alt="Dwyer Series 1800 Low Differential Pressure Switch for General Industrial Service" title="Dwyer Series 1800 Low Differential Pressure Switch for General Industrial Service" /></a></li>
		<li><a href="#?w=400" rel="20" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1900_Compact_Low_Differential_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series 1900 Compact Low Differential Pressure Switch" title="Dwyer Series 1900 Compact Low Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="21" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1950G_Explosion_proof_Differential_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series 1950G Explosion-proof Differential Pressure Switch" title="Dwyer Series 1950G Explosion-proof Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="22" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_1996_Gas_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer 1996 Gas Pressure Switch" title="Dwyer 1996 Gas Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="23" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_ADPS_HAVC_Differential_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series ADPS HAVC Differential Pressure Switch" title="Dwyer Series ADPS HAVC Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="24" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_BYDS_Bypass_Damper_Switch_Thumbnail.gif" alt="Dwyer Series BYDS Bypass Damper Switch" title="Dwyer Series BYDS Bypass Damper Switch" border="0" /></a></li>
		<li><a href="#?w=400" rel="25" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_H3_Explosion_Proof_Differential_Pressure_Switch_Thumbnail.gif" border="0" alt="Dwyer Series H3 Explosion-Proof Differential Pressure Switch" title="Dwyer Series H3 Explosion-Proof Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="26" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_3100_Explosion_proof_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 3100D Explosion-proof Differential Pressure Transmitter" title="Dwyer Series 3100D Explosion-proof Differential Pressure Transmitter" /></a></li>
		<li><a href="#?w=400" rel="27" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_603A_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 603A Differential Pressure Indicating Transmitter" title="Dwyer Series 603A Differential Pressure Indicating Transmitter" /></a></li>
		<li><a href="#?w=400" rel="28" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_604D_Minihelic_Differential_Pressure_Indicating_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 604D Minihelic Differential Pressure Indicating Transmitter" title="Dwyer Series 604D Minihelic Differential Pressure Indicating Transmitter" /></a></li>
		<li><a href="#?w=400" rel="29" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_605_Magnehelic_Differential_Pressure_Indicating_Tansmitter_Thumbnail.gif" border="0" alt="Dwyer Series 605 Magnehilc Differential Pressure Indicating" title="Dwyer Series 605 Magnehilc Differential Pressure Indicating" /></a></li>
		<li><a href="#?w=400" rel="30" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_631B_Wet_Wet_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Combustion Parts" title="Dwyer Series 631B Wet/Wet Differential Pressure Transmitter" /></a></li>
		<li><a href="#?w=400" rel="31" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_636D_Fixed_Range_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 636D Fixed Range Differential Pressure Transmitter" title="Dwyer Series 636D Fixed Range Differential Pressure Transmitter" /></a></li>
		<li><a href="#?w=400" rel="32" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_655A_316_Wet_Wet_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 655A 316 Wet/Wet Differential Pressure Switch" title="Dwyer Series 655A 316 Wet/Wet Differential Pressure Switch" /></a></li>
		<li><a href="#?w=400" rel="33" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_DM_2000_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series DM-2000 Differential Pressure Transmitter" title="Dwyer Series DM-2000 Differential Pressure Transmitter" /></a></li>
		<li><a href="#?w=400" rel="34" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_3200_Explosion_proof_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series 3200G Explosion proof Pressure Transmitter" title="Dwyer Series 3200G Explosion proof Pressure Transmitter"/></a></li>
		<li><a href="#?w=400" rel="35" class="Product"><img src="Parts_by_Man_OK_By_Jon/Dwyer/thumbnails/Series_MS_Magnesense_Differential_Pressure_Transmitter_Thumbnail.gif" border="0" alt="Dwyer Series MS Magnesense Differential Pressure Transmitter" title="Dwyer Series MS Magnesense Differential Pressure Transmitter" /></a></li>
		</li>
		</ul>
		 <div class="slider">
		<div class="handle"></div>
            </div>
        </div>

<div id="ManBioWhtBkg"></div>
<div id="ManBioDropShadow"></div>
<div id="PartsByManDropShadow"></div>
<div id="PartsByManDropShadow2"></div>
<div id="LrgProductWhtBkg"></div>
<div id="LogoNavWhtBkg"></div>
<div id="WhiteNEWareaDrop"></div>
<div id="PartsWhiteBlockFooterTwo"></div>
<div id="PartsLightBlueBkgrd"></div>
<div id="PartsWhiteBlockFooterThree"></div>
<div id="PartsFooterBarTopDrop"></div>
<div id="PartsFooterBarBottomDrop"></div>
<div id="ENGBBoosterLeftInsideDrop"></div>
<div id="DropRightSolidWhiteBkgrd"></div>
<div id="PartsENGBLearnMore"><font size="2" color="#000000"><b>Learn More</b></font></div>
<div id="PartsLearnMoreFooterText" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:11px;color:#494A4A;">
View the ENGB video to learn &nbsp;
<br/>more about our natural gas&nbsp;&nbsp;
<br/>booster's advanced features.</div>
<div id="PartsENGBFooterSolidVideoBTN"><a href="#?w=850" rel="popup_ENGBSolid" class="SolidVideo"><input type="image" name="submit" id="submit" src="viewvideobluebuttonFooter.gif" alt="booster video"border="0" width="60px" height="60px"/></a></div>
	<div id="popup_ENGBSolid" class="ENGBSolid_block">
	<div class="ENGBSolidWorksVideo"> 
      	<table border='0' cellpadding='0' align="center">
        <tr><td>
        <object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000'
        codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0'
        width="803px" height="610px">
        <param name='movie' value="ENGB_Sample_ENGB.swf"/>
        <param name='quality' value="high"/>
        <param name='bgcolor' value='#FFFFFF'/>
        <param name='loop' value="true"/>
        <embed src="ENGB_Sample_ENGB.swf" quality='high' bgcolor='#FFFFFF' width="803px"
        height="610px" loop="true" type='application/x-shockwave-flash'
        pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash'>
        </embed>
        </object>
	</td></tr>
       </table>
	</div>
	</div>
<div id="PartsENews">
<!-- BEGIN: Constant Contact Stylish Email Newsletter Form --> 
<div align="left"> 
<div style="width:300px; background-color: #transparent;"> 
<form name="ccoptin" action="http://visitor.r20.constantcontact.com/d.jsp" target="_blank" method="post" style="margin-bottom:0;"></form>
<span style="background-color: transparent; float:right;margin-right:0;margin-top:0"></span>
</div></div>
<font style="font-weight: bold; font-family:Garamond; font-size:12px; color:#808080;">Sign up for the ETTER E-Newsletter</font><br />
<input type="text" name="ea" size="20" style="font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:10px; border:1px solid #999999;"/>
<input type="submit" name="go" value="GO" class="submit" style="font-family:Verdana,Arial,Helvetica,sans-serif; font-size:10px;"/>
<input type="hidden" name="llr" value="qksvr8cab"/> 
<input type="hidden" name="m" value="1102583613776"/> 
<input type="hidden" name="p" value="oi"/> 
<!-- END: Constant Contact Stylish Email Newsletter Form --> 
<!-- BEGIN: SafeSubscribe --> 
<div align="left" style="padding-top:0px;"> 
<a href="http://www.constantcontact.com/safesubscribe.jsp" target="_blank"><img src="https://imgssl.constantcontact.com/ui/images1/safe_subscribe_logo.gif" border="0" width="168" height="14" alt="safe subscribe"/>
</a> </div>
<!-- END: SafeSubscribe --> 
<!-- BEGIN: Email Marketing you can trust --> 
<div align="left" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;"> 
For <a href="http://www.constantcontact.com/jmml/email-marketing.jsp" style="text-decoration:none;font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;" target="_blank">Email Marketing</a> you can trust 
<!-- END: Email Marketing you can trust --> 
</div></div>
<div id="CompanyName">ETTER Engineering Company, Inc.</div>
<div id="Address">210 Century Drive, Bristol, CT 06010</div>
<div id="Phone">1-800-444-1962</div>
<a href="privacy_policy.php" id="PrivacyPolicy">Privacy Policy</a>
<a href="terms_of_service.php" id="TermsofService">Terms of Service</a>
<a href="site_map.php" id="SiteMap">Site Map</a>
</div>
</body>
</html>